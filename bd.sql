-- MySQL dump 10.13  Distrib 5.5.42-37.1, for Linux (x86_64)
--
-- Host: localhost    Database: siriarte_css
-- ------------------------------------------------------
-- Server version	5.5.42-37.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acos`
--

DROP TABLE IF EXISTS `acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_acos_lft_rght` (`lft`,`rght`),
  KEY `idx_acos_alias` (`alias`),
  KEY `idx_acos_model_foreign_key` (`model`,`foreign_key`)
) ENGINE=InnoDB AUTO_INCREMENT=226 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acos`
--

LOCK TABLES `acos` WRITE;
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;
INSERT INTO `acos` VALUES (1,NULL,NULL,NULL,'controllers',1,434),(2,1,NULL,NULL,'Actions',2,47),(3,2,NULL,NULL,'paramFilters',3,4),(4,2,NULL,NULL,'get_index',5,6),(5,2,NULL,NULL,'admin_index',7,8),(6,2,NULL,NULL,'post_add',9,10),(7,2,NULL,NULL,'get_add',11,12),(8,2,NULL,NULL,'admin_add',13,14),(9,2,NULL,NULL,'get_edit',15,16),(10,2,NULL,NULL,'post_edit',17,18),(11,2,NULL,NULL,'admin_edit',19,20),(12,2,NULL,NULL,'admin_deletemulti',21,22),(13,2,NULL,NULL,'admin_delete',23,24),(14,2,NULL,NULL,'recordsforpage',25,26),(15,2,NULL,NULL,'filterConfig',27,28),(16,2,NULL,NULL,'isRoot',29,30),(17,2,NULL,NULL,'setSidebarMenu',31,32),(18,2,NULL,NULL,'setHeaderMenu',33,34),(19,2,NULL,NULL,'is_Authorizate',35,36),(20,2,NULL,NULL,'getMethod',37,38),(21,2,NULL,NULL,'ajaxVariablesInit',39,40),(22,2,NULL,NULL,'installBase',41,42),(23,2,NULL,NULL,'syncACL',43,44),(24,2,NULL,NULL,'install_acl',45,46),(25,1,NULL,NULL,'Categories',48,93),(26,25,NULL,NULL,'paramFilters',49,50),(27,25,NULL,NULL,'get_index',51,52),(28,25,NULL,NULL,'admin_index',53,54),(29,25,NULL,NULL,'post_add',55,56),(30,25,NULL,NULL,'get_add',57,58),(31,25,NULL,NULL,'admin_add',59,60),(32,25,NULL,NULL,'get_edit',61,62),(33,25,NULL,NULL,'post_edit',63,64),(34,25,NULL,NULL,'admin_edit',65,66),(35,25,NULL,NULL,'admin_delete',67,68),(36,25,NULL,NULL,'admin_deletemulti',69,70),(37,25,NULL,NULL,'recordsforpage',71,72),(38,25,NULL,NULL,'filterConfig',73,74),(39,25,NULL,NULL,'isRoot',75,76),(40,25,NULL,NULL,'setSidebarMenu',77,78),(41,25,NULL,NULL,'setHeaderMenu',79,80),(42,25,NULL,NULL,'is_Authorizate',81,82),(43,25,NULL,NULL,'getMethod',83,84),(44,25,NULL,NULL,'ajaxVariablesInit',85,86),(45,25,NULL,NULL,'installBase',87,88),(46,25,NULL,NULL,'syncACL',89,90),(47,25,NULL,NULL,'install_acl',91,92),(48,1,NULL,NULL,'Groupactions',94,133),(49,48,NULL,NULL,'paramFilters',95,96),(50,48,NULL,NULL,'get_index',97,98),(51,48,NULL,NULL,'admin_index',99,100),(52,48,NULL,NULL,'post_add',101,102),(53,48,NULL,NULL,'admin_add',103,104),(54,48,NULL,NULL,'admin_delete',105,106),(55,48,NULL,NULL,'admin_deletemulti',107,108),(56,48,NULL,NULL,'admin_acl',109,110),(57,48,NULL,NULL,'recordsforpage',111,112),(58,48,NULL,NULL,'filterConfig',113,114),(59,48,NULL,NULL,'isRoot',115,116),(60,48,NULL,NULL,'setSidebarMenu',117,118),(61,48,NULL,NULL,'setHeaderMenu',119,120),(62,48,NULL,NULL,'is_Authorizate',121,122),(63,48,NULL,NULL,'getMethod',123,124),(64,48,NULL,NULL,'ajaxVariablesInit',125,126),(65,48,NULL,NULL,'installBase',127,128),(66,48,NULL,NULL,'syncACL',129,130),(67,48,NULL,NULL,'install_acl',131,132),(68,1,NULL,NULL,'Groups',134,179),(69,68,NULL,NULL,'paramFilters',135,136),(70,68,NULL,NULL,'get_index',137,138),(71,68,NULL,NULL,'admin_index',139,140),(72,68,NULL,NULL,'admin_delete',141,142),(73,68,NULL,NULL,'get_edit',143,144),(74,68,NULL,NULL,'post_edit',145,146),(75,68,NULL,NULL,'admin_edit',147,148),(76,68,NULL,NULL,'post_add',149,150),(77,68,NULL,NULL,'get_add',151,152),(78,68,NULL,NULL,'admin_add',153,154),(79,68,NULL,NULL,'admin_deletemulti',155,156),(80,68,NULL,NULL,'recordsforpage',157,158),(81,68,NULL,NULL,'filterConfig',159,160),(82,68,NULL,NULL,'isRoot',161,162),(83,68,NULL,NULL,'setSidebarMenu',163,164),(84,68,NULL,NULL,'setHeaderMenu',165,166),(85,68,NULL,NULL,'is_Authorizate',167,168),(86,68,NULL,NULL,'getMethod',169,170),(87,68,NULL,NULL,'ajaxVariablesInit',171,172),(88,68,NULL,NULL,'installBase',173,174),(89,68,NULL,NULL,'syncACL',175,176),(90,68,NULL,NULL,'install_acl',177,178),(91,1,NULL,NULL,'Modules',180,223),(92,91,NULL,NULL,'paramFilters',181,182),(93,91,NULL,NULL,'get_index',183,184),(94,91,NULL,NULL,'admin_index',185,186),(95,91,NULL,NULL,'post_add',187,188),(96,91,NULL,NULL,'admin_add',189,190),(97,91,NULL,NULL,'get_edit',191,192),(98,91,NULL,NULL,'post_edit',193,194),(99,91,NULL,NULL,'admin_edit',195,196),(100,91,NULL,NULL,'admin_delete',197,198),(101,91,NULL,NULL,'admin_deletemulti',199,200),(102,91,NULL,NULL,'recordsforpage',201,202),(103,91,NULL,NULL,'filterConfig',203,204),(104,91,NULL,NULL,'isRoot',205,206),(105,91,NULL,NULL,'setSidebarMenu',207,208),(106,91,NULL,NULL,'setHeaderMenu',209,210),(107,91,NULL,NULL,'is_Authorizate',211,212),(108,91,NULL,NULL,'getMethod',213,214),(109,91,NULL,NULL,'ajaxVariablesInit',215,216),(110,91,NULL,NULL,'installBase',217,218),(111,91,NULL,NULL,'syncACL',219,220),(112,91,NULL,NULL,'install_acl',221,222),(113,1,NULL,NULL,'Pages',224,249),(114,113,NULL,NULL,'display',225,226),(115,113,NULL,NULL,'recordsforpage',227,228),(116,113,NULL,NULL,'filterConfig',229,230),(117,113,NULL,NULL,'isRoot',231,232),(118,113,NULL,NULL,'setSidebarMenu',233,234),(119,113,NULL,NULL,'setHeaderMenu',235,236),(120,113,NULL,NULL,'is_Authorizate',237,238),(121,113,NULL,NULL,'getMethod',239,240),(122,113,NULL,NULL,'ajaxVariablesInit',241,242),(123,113,NULL,NULL,'installBase',243,244),(124,113,NULL,NULL,'syncACL',245,246),(125,113,NULL,NULL,'install_acl',247,248),(126,1,NULL,NULL,'Users',250,313),(127,126,NULL,NULL,'getConditionsRootGroup',251,252),(128,126,NULL,NULL,'getConditionsRootUser',253,254),(129,126,NULL,NULL,'admin_initDB',255,256),(130,126,NULL,NULL,'paramFilters',257,258),(131,126,NULL,NULL,'get_index',259,260),(132,126,NULL,NULL,'admin_index',261,262),(133,126,NULL,NULL,'post_add',263,264),(134,126,NULL,NULL,'admin_add',265,266),(135,126,NULL,NULL,'get_edit',267,268),(136,126,NULL,NULL,'post_edit',269,270),(137,126,NULL,NULL,'admin_edit',271,272),(138,126,NULL,NULL,'admin_delete',273,274),(139,126,NULL,NULL,'admin_deletemulti',275,276),(140,126,NULL,NULL,'post_resetpassword',277,278),(141,126,NULL,NULL,'admin_resetpassword',279,280),(142,126,NULL,NULL,'admin_home',281,282),(143,126,NULL,NULL,'get_login',283,284),(144,126,NULL,NULL,'post_login',285,286),(145,126,NULL,NULL,'admin_login',287,288),(146,126,NULL,NULL,'admin_logout',289,290),(147,126,NULL,NULL,'recordsforpage',291,292),(148,126,NULL,NULL,'filterConfig',293,294),(149,126,NULL,NULL,'isRoot',295,296),(150,126,NULL,NULL,'setSidebarMenu',297,298),(151,126,NULL,NULL,'setHeaderMenu',299,300),(152,126,NULL,NULL,'is_Authorizate',301,302),(153,126,NULL,NULL,'getMethod',303,304),(154,126,NULL,NULL,'ajaxVariablesInit',305,306),(155,126,NULL,NULL,'installBase',307,308),(156,126,NULL,NULL,'syncACL',309,310),(157,126,NULL,NULL,'install_acl',311,312),(158,1,NULL,NULL,'AclExtras',314,315),(159,1,NULL,NULL,'Companies',316,361),(160,159,NULL,NULL,'paramFilters',317,318),(161,159,NULL,NULL,'get_index',319,320),(162,159,NULL,NULL,'admin_index',321,322),(163,159,NULL,NULL,'post_add',323,324),(164,159,NULL,NULL,'get_add',325,326),(165,159,NULL,NULL,'admin_add',327,328),(166,159,NULL,NULL,'get_edit',329,330),(167,159,NULL,NULL,'post_edit',331,332),(168,159,NULL,NULL,'admin_edit',333,334),(169,159,NULL,NULL,'admin_delete',335,336),(170,159,NULL,NULL,'admin_deletemulti',337,338),(171,159,NULL,NULL,'recordsforpage',339,340),(172,159,NULL,NULL,'filterConfig',341,342),(173,159,NULL,NULL,'isRoot',343,344),(174,159,NULL,NULL,'setSidebarMenu',345,346),(175,159,NULL,NULL,'setHeaderMenu',347,348),(176,159,NULL,NULL,'is_Authorizate',349,350),(177,159,NULL,NULL,'getMethod',351,352),(178,159,NULL,NULL,'ajaxVariablesInit',353,354),(179,159,NULL,NULL,'installBase',355,356),(180,159,NULL,NULL,'syncACL',357,358),(181,159,NULL,NULL,'install_acl',359,360),(190,1,NULL,NULL,'Corporations',362,407),(191,190,NULL,NULL,'paramFilters',363,364),(192,190,NULL,NULL,'get_index',365,366),(193,190,NULL,NULL,'admin_index',367,368),(194,190,NULL,NULL,'post_add',369,370),(195,190,NULL,NULL,'get_add',371,372),(196,190,NULL,NULL,'admin_add',373,374),(197,190,NULL,NULL,'get_edit',375,376),(198,190,NULL,NULL,'post_edit',377,378),(199,190,NULL,NULL,'admin_edit',379,380),(200,190,NULL,NULL,'admin_delete',381,382),(201,190,NULL,NULL,'admin_deletemulti',383,384),(202,190,NULL,NULL,'recordsforpage',385,386),(203,190,NULL,NULL,'filterConfig',387,388),(204,190,NULL,NULL,'isRoot',389,390),(205,190,NULL,NULL,'setSidebarMenu',391,392),(206,190,NULL,NULL,'setHeaderMenu',393,394),(207,190,NULL,NULL,'is_Authorizate',395,396),(208,190,NULL,NULL,'getMethod',397,398),(209,190,NULL,NULL,'ajaxVariablesInit',399,400),(210,190,NULL,NULL,'installBase',401,402),(211,190,NULL,NULL,'syncACL',403,404),(212,190,NULL,NULL,'install_acl',405,406),(213,1,NULL,NULL,'Welcome',408,433),(214,213,NULL,NULL,'admin_index',409,410),(215,213,NULL,NULL,'recordsforpage',411,412),(216,213,NULL,NULL,'filterConfig',413,414),(217,213,NULL,NULL,'isRoot',415,416),(218,213,NULL,NULL,'setSidebarMenu',417,418),(219,213,NULL,NULL,'setHeaderMenu',419,420),(220,213,NULL,NULL,'is_Authorizate',421,422),(221,213,NULL,NULL,'getMethod',423,424),(222,213,NULL,NULL,'ajaxVariablesInit',425,426),(223,213,NULL,NULL,'installBase',427,428),(224,213,NULL,NULL,'syncACL',429,430),(225,213,NULL,NULL,'install_acl',431,432);
/*!40000 ALTER TABLE `acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actions`
--

DROP TABLE IF EXISTS `actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `order` int(2) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_actions_categories1_idx` (`category_id`),
  CONSTRAINT `fk_actions_categories1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actions`
--

LOCK TABLES `actions` WRITE;
/*!40000 ALTER TABLE `actions` DISABLE KEYS */;
INSERT INTO `actions` VALUES (1,'Grupos : Listado','/admin/groups/index',0,1,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(5,'Modulos : Listado','/admin/modules/index',0,2,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(9,'Categorias : Listado','/admin/categories/index',0,3,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(13,'Funciones : Listado','/admin/actions/index',0,4,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(17,'Permisos : Listado','/admin/groupactions/index',0,5,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(20,'Permisos : ACL','/admin/groupactions/acl',4,5,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(21,'Usuarios : Listado','/admin/users/index',0,6,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(25,'Usuarios : Sync','/admin/users/initDB',4,6,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(26,'Empresas : Listado','/admin/companies/index',1,7,'2015-06-06 12:39:06','2015-06-06 12:39:06'),(27,'Corporaciones : Listado','/admin/corporations/index ',99,8,'2015-09-08 10:20:48','2015-09-08 10:22:13'),(28,'Bienvenid@','/admin/welcome/index',99,9,'2015-09-08 16:36:58','2015-09-08 16:36:58');
/*!40000 ALTER TABLE `actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros`
--

DROP TABLE IF EXISTS `aros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aros_lft_rght` (`lft`,`rght`),
  KEY `idx_aros_alias` (`alias`),
  KEY `idx_aros_model_foreign_key` (`model`,`foreign_key`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros`
--

LOCK TABLES `aros` WRITE;
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;
INSERT INTO `aros` VALUES (1,NULL,'Group',1,NULL,1,6),(2,1,'User',1,NULL,2,3),(3,NULL,'Module',1,NULL,7,8),(4,NULL,'Category',1,NULL,9,10),(5,NULL,'Category',2,NULL,11,12),(6,NULL,'Category',3,NULL,13,14),(7,NULL,'Category',4,NULL,15,16),(8,NULL,'Category',5,NULL,17,18),(9,NULL,'Category',6,NULL,19,20),(10,NULL,'Module',2,NULL,21,22),(11,NULL,'Category',7,NULL,23,24),(12,NULL,'Company',1,NULL,25,26),(16,1,'User',3,NULL,4,5),(17,NULL,'Group',2,NULL,27,32),(20,NULL,'Module',3,NULL,33,34),(21,17,'User',6,NULL,28,29),(22,NULL,'Group',3,NULL,35,42),(23,NULL,'Company',4,NULL,43,44),(24,22,'User',7,NULL,36,37),(25,22,'User',8,NULL,38,39),(26,NULL,'Module',4,NULL,45,46),(27,NULL,'Category',8,NULL,47,48),(28,NULL,'Corporation',0,NULL,49,50),(30,NULL,'Corporation',3,NULL,51,52),(31,22,'User',9,NULL,40,41),(32,NULL,'Group',4,NULL,53,60),(33,32,'User',10,NULL,54,55),(34,NULL,'Corporation',4,NULL,61,62),(36,NULL,'Company',6,NULL,63,64),(37,NULL,'Company',7,NULL,65,66),(39,32,'User',11,NULL,56,57),(40,32,'User',12,NULL,58,59),(41,NULL,'Module',5,NULL,67,68),(42,NULL,'Category',9,NULL,69,70),(43,17,'User',13,NULL,30,31);
/*!40000 ALTER TABLE `aros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`),
  KEY `aco_id` (`aco_id`),
  CONSTRAINT `aros_acos_ibfk_1` FOREIGN KEY (`aro_id`) REFERENCES `aros` (`id`),
  CONSTRAINT `aros_acos_ibfk_2` FOREIGN KEY (`aco_id`) REFERENCES `acos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros_acos`
--

LOCK TABLES `aros_acos` WRITE;
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;
INSERT INTO `aros_acos` VALUES (1,2,1,'1','1','1','1'),(2,1,1,'1','1','1','1'),(3,17,126,'1','1','1','1'),(4,17,159,'-1','-1','-1','-1'),(5,22,142,'1','1','1','1'),(6,22,143,'1','1','1','1'),(7,22,144,'1','1','1','1'),(8,22,145,'1','1','1','1'),(9,22,146,'1','1','1','1'),(10,22,147,'1','1','1','1'),(11,22,148,'1','1','1','1'),(12,22,149,'1','1','1','1'),(13,22,150,'1','1','1','1'),(14,22,151,'1','1','1','1'),(15,22,152,'1','1','1','1'),(16,22,153,'1','1','1','1'),(17,22,154,'1','1','1','1'),(18,17,190,'-1','-1','-1','-1'),(19,17,191,'-1','-1','-1','-1'),(20,17,192,'-1','-1','-1','-1'),(21,17,193,'-1','-1','-1','-1'),(22,17,194,'-1','-1','-1','-1'),(23,17,195,'-1','-1','-1','-1'),(24,17,196,'-1','-1','-1','-1'),(25,17,197,'-1','-1','-1','-1'),(26,17,198,'-1','-1','-1','-1'),(27,17,199,'-1','-1','-1','-1'),(28,17,200,'-1','-1','-1','-1'),(29,17,201,'-1','-1','-1','-1'),(30,17,202,'-1','-1','-1','-1'),(31,17,203,'-1','-1','-1','-1'),(32,17,204,'-1','-1','-1','-1'),(33,17,205,'-1','-1','-1','-1'),(34,17,206,'-1','-1','-1','-1'),(35,17,207,'-1','-1','-1','-1'),(36,17,208,'-1','-1','-1','-1'),(37,17,209,'-1','-1','-1','-1'),(38,17,210,'-1','-1','-1','-1'),(39,17,211,'-1','-1','-1','-1'),(40,17,212,'-1','-1','-1','-1'),(41,32,126,'1','1','1','1'),(42,32,127,'1','1','1','1'),(43,32,128,'1','1','1','1'),(44,32,129,'1','1','1','1'),(45,32,130,'1','1','1','1'),(46,32,131,'1','1','1','1'),(47,32,132,'1','1','1','1'),(48,32,133,'1','1','1','1'),(49,32,134,'1','1','1','1'),(50,32,135,'1','1','1','1'),(51,32,136,'1','1','1','1'),(52,32,137,'1','1','1','1'),(53,32,138,'1','1','1','1'),(54,32,139,'1','1','1','1'),(55,32,140,'1','1','1','1'),(56,32,141,'1','1','1','1'),(57,32,142,'1','1','1','1'),(58,32,143,'1','1','1','1'),(59,32,144,'1','1','1','1'),(60,32,145,'1','1','1','1'),(61,32,146,'1','1','1','1'),(62,32,147,'1','1','1','1'),(63,32,148,'1','1','1','1'),(64,32,149,'1','1','1','1'),(65,32,150,'1','1','1','1'),(66,32,151,'1','1','1','1'),(67,32,152,'1','1','1','1'),(68,32,153,'1','1','1','1'),(69,32,154,'1','1','1','1'),(70,32,155,'1','1','1','1'),(71,32,156,'1','1','1','1'),(72,32,157,'1','1','1','1'),(73,32,158,'1','1','1','1'),(75,32,159,'1','1','1','1'),(76,32,160,'1','1','1','1'),(77,32,161,'1','1','1','1'),(78,32,162,'1','1','1','1'),(79,32,163,'1','1','1','1'),(80,32,164,'1','1','1','1'),(81,32,165,'1','1','1','1'),(82,32,166,'1','1','1','1'),(83,32,167,'1','1','1','1'),(84,32,168,'1','1','1','1'),(85,32,169,'1','1','1','1'),(86,32,170,'1','1','1','1'),(87,32,171,'1','1','1','1'),(88,32,172,'1','1','1','1'),(89,32,173,'1','1','1','1'),(90,32,174,'1','1','1','1'),(91,32,175,'1','1','1','1'),(92,32,176,'1','1','1','1'),(93,32,177,'1','1','1','1'),(94,32,178,'1','1','1','1'),(95,32,179,'1','1','1','1'),(96,32,180,'1','1','1','1'),(97,32,181,'1','1','1','1'),(98,32,190,'1','1','1','1'),(99,32,191,'1','1','1','1'),(100,32,192,'1','1','1','1'),(101,32,193,'1','1','1','1'),(102,32,194,'1','1','1','1'),(103,32,195,'1','1','1','1'),(104,32,196,'1','1','1','1'),(105,32,197,'1','1','1','1'),(106,32,198,'1','1','1','1'),(107,32,199,'1','1','1','1'),(108,32,200,'1','1','1','1'),(109,32,202,'1','1','1','1'),(110,32,201,'1','1','1','1'),(111,32,203,'1','1','1','1'),(112,32,204,'1','1','1','1'),(113,32,205,'1','1','1','1'),(114,32,206,'1','1','1','1'),(115,32,207,'1','1','1','1'),(116,32,208,'1','1','1','1'),(117,32,209,'1','1','1','1'),(118,32,210,'1','1','1','1'),(119,32,211,'1','1','1','1'),(120,32,212,'1','1','1','1'),(123,17,213,'1','1','1','1'),(124,22,213,'1','1','1','1'),(125,32,213,'1','1','1','1'),(126,32,214,'1','1','1','1'),(127,22,214,'1','1','1','1'),(128,17,214,'1','1','1','1'),(129,17,215,'1','1','1','1'),(130,22,215,'1','1','1','1'),(131,32,215,'1','1','1','1'),(132,32,216,'1','1','1','1'),(133,22,216,'1','1','1','1'),(134,17,216,'1','1','1','1'),(135,17,217,'1','1','1','1'),(136,22,217,'1','1','1','1'),(137,32,217,'1','1','1','1'),(138,32,218,'1','1','1','1'),(139,22,218,'1','1','1','1'),(140,17,218,'1','1','1','1'),(141,17,219,'1','1','1','1'),(142,22,219,'1','1','1','1'),(143,32,219,'1','1','1','1'),(144,32,220,'1','1','1','1'),(145,22,220,'1','1','1','1'),(146,17,220,'1','1','1','1'),(147,17,221,'1','1','1','1'),(148,22,221,'1','1','1','1'),(149,32,221,'1','1','1','1'),(150,32,222,'1','1','1','1'),(151,22,222,'1','1','1','1'),(152,17,222,'1','1','1','1'),(153,17,223,'1','1','1','1'),(154,22,223,'1','1','1','1'),(155,32,223,'1','1','1','1'),(156,32,224,'1','1','1','1'),(157,22,224,'1','1','1','1'),(158,17,224,'1','1','1','1'),(159,17,225,'1','1','1','1'),(160,22,225,'1','1','1','1'),(161,32,225,'1','1','1','1'),(162,17,160,'-1','-1','-1','-1'),(163,17,161,'-1','-1','-1','-1'),(164,17,162,'-1','-1','-1','-1'),(165,17,163,'-1','-1','-1','-1'),(166,17,164,'-1','-1','-1','-1'),(167,17,165,'-1','-1','-1','-1'),(168,17,166,'-1','-1','-1','-1'),(169,17,167,'-1','-1','-1','-1'),(170,17,168,'-1','-1','-1','-1'),(171,17,169,'-1','-1','-1','-1'),(172,17,170,'-1','-1','-1','-1'),(173,17,171,'-1','-1','-1','-1'),(174,17,172,'-1','-1','-1','-1'),(175,17,173,'-1','-1','-1','-1'),(176,17,175,'-1','-1','-1','-1'),(177,17,176,'-1','-1','-1','-1'),(178,17,174,'-1','-1','-1','-1'),(179,17,177,'-1','-1','-1','-1'),(180,17,178,'-1','-1','-1','-1'),(181,17,179,'-1','-1','-1','-1'),(182,17,180,'-1','-1','-1','-1'),(183,17,181,'-1','-1','-1','-1');
/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `order` int(2) NOT NULL,
  `module_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_categories_modules_idx` (`module_id`),
  CONSTRAINT `fk_categories_modules` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Grupos',0,1,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(2,'Modulos',1,1,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(3,'Categorias',2,1,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(4,'Funciones',3,1,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(5,'Permisos',4,1,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(6,'Administrar Usuarios',5,3,'2015-06-06 11:12:48','2015-06-07 02:07:38'),(7,'Administrar Empresas',1,2,'2015-06-06 12:20:56','2015-06-07 02:07:48'),(8,'Administrar Corporaciones',99,4,'2015-09-08 10:19:56','2015-09-08 10:19:56'),(9,'Bienvenid@',99,5,'2015-09-08 16:36:27','2015-09-08 16:36:27');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  `corporation_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'Empresa de Prueba','files/logos/1334525a361dced73950033516a2905ac2abb0.jpg','Empresa de prueba','1234567890','California','2015-09-08 10:56:21','2015-06-06 13:14:16',0),(4,'Google International Inc.','files/logos/10491399689ec46e4e86401558c861f690baae.png','Motor de bÃºsqueda','555555555','Silicon Valley','2015-09-08 16:01:34','2015-06-07 02:27:54',0),(6,'Florida Bebidas','files/logos/160031c82a9c2726863b49e6e851f7dd0016bf.png','Empresa de bebidas y alimentos','11112222','Cartago','2015-09-08 16:13:36','2015-09-08 16:00:31',4),(7,'Industrias Kern\'s','files/logos/16195767968118e3768f25b1c6a9fed5f4efd1.gif','Industrias Alimenticias Kern\'s y compaÃ±Ã­a.','9999966666','Puntarenas','2015-09-08 16:20:14','2015-09-08 16:19:57',4);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corporations`
--

DROP TABLE IF EXISTS `corporations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corporations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corporations`
--

LOCK TABLES `corporations` WRITE;
/*!40000 ALTER TABLE `corporations` DISABLE KEYS */;
INSERT INTO `corporations` VALUES (3,'ACME','files/logos/10571817b712a17afdff363e85155fcbe58210.jpg','CorporaciÃ³n de inventos y herramientas','9999999999','Texas','2015-09-08 10:57:18','2015-09-08 10:47:28'),(4,'FIFCO','files/logos/144621c82a9c2726863b49e6e851f7dd0016bf.png','FIFCO Costa Rica','1234567890','San JosÃ©','2015-09-08 14:46:21','2015-09-08 14:46:21');
/*!40000 ALTER TABLE `corporations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groupactions`
--

DROP TABLE IF EXISTS `groupactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groupactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_group_action` (`group_id`,`action_id`),
  KEY `fk_groupactions_groups1_idx` (`group_id`),
  KEY `fk_groupactions_actions1_idx` (`action_id`),
  CONSTRAINT `fk_groupactions_actions1` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_groupactions_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupactions`
--

LOCK TABLES `groupactions` WRITE;
/*!40000 ALTER TABLE `groupactions` DISABLE KEYS */;
INSERT INTO `groupactions` VALUES (1,1,1,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(2,1,5,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(3,1,9,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(4,1,13,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(5,1,17,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(6,1,20,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(7,1,21,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(8,1,25,'2015-06-06 11:12:48','2015-06-06 11:12:48'),(9,1,26,'2015-06-06 12:39:51','2015-06-06 12:39:51'),(13,1,27,'2015-09-08 10:29:36','2015-09-08 10:29:36'),(14,4,27,'2015-09-08 14:41:24','2015-09-08 14:41:24'),(15,4,26,'2015-09-08 14:41:30','2015-09-08 14:41:30'),(16,4,21,'2015-09-08 14:41:41','2015-09-08 14:41:41'),(17,2,28,'2015-09-08 16:37:17','2015-09-08 16:37:17'),(18,3,28,'2015-09-08 16:37:24','2015-09-08 16:37:24');
/*!40000 ALTER TABLE `groupactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'Root','2015-06-06 11:12:27','2015-06-06 11:12:27'),(2,'Administrador','2015-06-06 14:03:22','2015-06-07 02:01:49'),(3,'Perfil Usuario Normal','2015-06-07 02:24:22','2015-06-07 02:24:22'),(4,'Administrador Corporativo','2015-09-08 14:40:59','2015-09-08 14:40:59');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `i18n`
--

DROP TABLE IF EXISTS `i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `i18n` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`foreign_key`,`field`,`locale`,`model`),
  KEY `locale` (`locale`),
  KEY `model` (`model`),
  KEY `row_id` (`foreign_key`),
  KEY `field` (`field`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `i18n`
--

LOCK TABLES `i18n` WRITE;
/*!40000 ALTER TABLE `i18n` DISABLE KEYS */;
/*!40000 ALTER TABLE `i18n` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `masters`
--

DROP TABLE IF EXISTS `masters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masters`
--

LOCK TABLES `masters` WRITE;
/*!40000 ALTER TABLE `masters` DISABLE KEYS */;
/*!40000 ALTER TABLE `masters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `order` int(2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (1,'Sistema Base','icon-globe',99,'2015-06-06 11:12:48','2015-06-07 01:46:39'),(2,'Empresas','icon-th-list',2,'2015-06-06 12:16:39','2015-09-08 10:19:10'),(3,'Usuarios','icon-user',3,'2015-06-07 01:55:52','2015-09-08 10:19:18'),(4,'Corporaciones','',1,'2015-09-08 10:18:55','2015-09-08 10:19:27'),(5,'Bienvenid@','',99,'2015-09-08 16:36:00','2015-09-08 16:36:00');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `corporation_id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` char(40) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Andres','Rosales','andrewvergel@gmail.com',NULL,0,'andrewvergel','3c05d43299f54468f2395ebb768a14f052f5e8b0',1,'2015-06-06 11:12:44','2015-06-06 11:34:40'),(3,'Lisandro','Martinez','lisandrom@gmail.com',NULL,0,'lisandrom','253e6edb8139392550fff418bac6265d0d2cc934',1,'2015-06-06 14:02:07','2015-09-08 17:14:57'),(6,'Administrador','Empresa','admin@admin.com',NULL,0,'admin','253e6edb8139392550fff418bac6265d0d2cc934',2,'2015-06-07 02:15:39','2015-06-07 02:15:39'),(7,'Usuario ','Google','asdasd@google.com',4,0,'usuario1','253e6edb8139392550fff418bac6265d0d2cc934',3,'2015-06-07 02:28:34','2015-09-08 16:24:33'),(9,'adsf','asdf','asdf@asdf.com',NULL,0,'asdf','e861dd37532ef50a32162d7c2dfb78e9fd26d428',3,'2015-09-08 11:10:55','2015-09-08 11:10:55'),(10,'Pedro','Perez','pedrop@asdf.com',NULL,0,'pedrop','253e6edb8139392550fff418bac6265d0d2cc934',4,'2015-09-08 14:42:11','2015-09-08 14:42:11'),(11,'Daniela ','Prieto','danielap@siderysbsn.com',NULL,0,'danielap','253e6edb8139392550fff418bac6265d0d2cc934',4,'2015-09-08 16:23:08','2015-09-08 16:23:08'),(12,'Juan','Fernandez','juanf@siderysbsn.com',NULL,0,'juanf','253e6edb8139392550fff418bac6265d0d2cc934',4,'2015-09-08 16:23:30','2015-09-08 16:23:30'),(13,'Wile','Coyote','wile@acme.com',NULL,3,'coyote','253e6edb8139392550fff418bac6265d0d2cc934',2,'2015-09-08 17:07:50','2015-09-08 17:07:50');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `values`
--

DROP TABLE IF EXISTS `values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `optional` varchar(255) NOT NULL,
  `master_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_values_masters1_idx` (`master_id`),
  CONSTRAINT `fk_values_masters1` FOREIGN KEY (`master_id`) REFERENCES `masters` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `values`
--

LOCK TABLES `values` WRITE;
/*!40000 ALTER TABLE `values` DISABLE KEYS */;
/*!40000 ALTER TABLE `values` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-08 17:21:54
