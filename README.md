# MENU DE BACKEND

Las opciones del menú se dividen en varias secciones y se acceden en el backend desde la opción "Sistema Base"

- Grupos: son los perfiles de usuarios

- Módulos: son las opciones del menú superior

- Categorías: son las opciones que se desplegan cuando se hace clic en el menú superior

- Funciones: son los URL finales que accede el usuario y que se incluyen en la columna izquierda del backend

- Permisos: tiene tres secciones:

	- Permisos : Usuarios: Son las relaciones que tiene cada Grupo con las Funciones, y esto hace que se arme automáticamente el menú en el backend en base a las funciones que tiene asociado el usuario.

	- ACL: Sync: el ACL funciona a nivel de funciones y controladores, este módulo sirve para sincronizar las funciones del controlador con el sistema de ACL. Si esto no funciona en tu máquina puedes correr directamente el comando:

		./app/Console/cake AclExtras.AclExtras aco_sync

	- Permisos ACL: en esta sección se le da permiso a cada Grupo de usuario con las funciones de cada controlador, se debe hacer clic en cada botón "Activar" de cada función y cada grupo.

- Usuarios: creación de los usuarios asociados a un grupo