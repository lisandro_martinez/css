<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">


        <?php
          echo $this->Form->create('Company', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
        ?>

          <div class="form-group col-lg-6">
            <?php
              if($action == "admin_edit"){echo $this->Form->input('Company.id');}
            ?>
						   <label for="name"><?php echo __("Company name");?> *</label>
						   <input type="text" class="form-control" name="data[Company][name]" id="name" value="<?php echo isset($this->data['Company']['name']) ? $this->data['Company']['name'] : '';?>">

		</div>
		
		<div class="form-group col-lg-6">
			   <label for="name"><?php echo __("Slug");?> *</label>
			   <input type="text" class="form-control" name="data[Company][slug]" id="name" value="<?php echo isset($this->data['Company']['slug']) ? $this->data['Company']['slug'] : '';?>">
		</div>

          <div class="form-group col-lg-6">
						   <label for="name"><?php echo __("Phone");?> *</label>
						   <input type="text" class="form-control" name="data[Company][phone]" id="phone" value="<?php echo isset($this->data['Company']['phone']) ? $this->data['Company']['phone'] : '';?>">

					</div>
<?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-12 textareafield">
						   <label for="name"><?php echo __("Description");?> (ESP)*</label>
						   <textarea class="form-control" name="data[Company][description][esp]" id="name"><?php echo isset($this->data['Company']['description']['esp']) ? $this->data['Company']['description']['esp'] : '';?></textarea>
		  </div>
			<?php endif; ?>
<?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-lg-12 textareafield">
						   <label for="name"><?php echo __("Description");?> (ENG)*</label>
						   <textarea class="form-control" name="data[Company][description][eng]" id="name"><?php echo isset($this->data['Company']['description']['eng']) ? $this->data['Company']['description']['eng'] : '';?></textarea>
		  </div>
			<?php endif; ?>
		  <?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-6 textareafield">
						   <label for="name"><?php echo __("Address");?> (ESP)*</label>
						   <textarea class="form-control" name="data[Company][address][esp]" id="name"><?php echo isset($this->data['Company']['address']['esp']) ? $this->data['Company']['address']['esp'] : '';?></textarea>
			</div>
			<?php endif; ?>
		  <?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-lg-6 textareafield">
						   <label for="name"><?php echo __("Address");?> (ENG)*</label>
						   <textarea class="form-control" name="data[Company][address][eng]" id="name"><?php echo isset($this->data['Company']['address']['eng']) ? $this->data['Company']['address']['eng'] : '';?></textarea>
			</div>
			<?php endif; ?>

		<div class="form-group col-lg-6">
			   <label for="name"><?php echo __("URL");?></label>
			   <input type="text" class="form-control" name="data[Company][url]" id="name" value="<?php echo isset($this->data['Company']['url']) ? $this->data['Company']['url'] : '';?>">
		</div>

		<div class="form-group col-lg-6">
			   <label for="name"><?php echo __("Facebook");?></label>
			   <input type="text" class="form-control" name="data[Company][fb]" id="name" value="<?php echo isset($this->data['Company']['fb']) ? $this->data['Company']['fb'] : '';?>">
		</div>

		<div class="form-group col-lg-6">
			   <label for="name"><?php echo __("Twitter");?></label>
			   <input type="text" class="form-control" name="data[Company][t]" id="name" value="<?php echo isset($this->data['Company']['t']) ? $this->data['Company']['t'] : '';?>">
		</div>

		<div class="form-group col-lg-6">
			   <label for="name"><?php echo __("LinkedIn");?></label>
			   <input type="text" class="form-control" name="data[Company][ln]" id="name" value="<?php echo isset($this->data['Company']['ln']) ? $this->data['Company']['ln'] : '';?>">
		</div>

		<div class="form-group col-lg-6">
			   <label for="name"><?php echo __("Youtube");?></label>
			   <input type="text" class="form-control" name="data[Company][y]" id="name" value="<?php echo isset($this->data['Company']['y']) ? $this->data['Company']['y'] : '';?>">
		</div>

          <?php if($action == "admin_edit"){ ?>
          <div class="form-group col-lg-6">
              <div>
			  <?php
			  if (isset($this->data['Company']['logo']) and $this->data['Company']['logo']) {
			  ?>
               <img src="<?php echo '/'.$this->data['Company']['logo'];?>" width="150px" height="auto" />
			  <?php
			  }
			  ?>
               <input type="hidden" name="data[Company][logo]" class="form-control" value="<?php echo $this->data['Company']['logo'];?>">
              </div>
              <label for="name">Logo *</label>
              <?php
                   echo $this->Form->input('Company.logo_up_edit',array("class"=>"form-control","label"=>false,"required"=>false,'type' => 'file'));
               ?>
					</div>
          <?php }?>

		<div class="form-group col-lg-6">
			 <label for="name"><?php echo __("Associated Corporation");?></label>
			 <?php echo $this->Form->input('corporation_id',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>"Seleccione","type"=>"select","required"=>false)); ?>

		</div>
		  
          <div class="form-group col-lg-1 textareafield">
		  
          <label><?php echo __("Active");?></label>
				<select  class="form-control" name="data[Company][active]" style="min-width: 70px">
				<option value=1 
					<?php 
						if (isset($this->data['Company']['active']) and $this->data['Company']['active']==1) echo 'selected';
					?>
				><?php echo __("Yes");?></option>
				<option value=0 
					<?php 
						if (!isset($this->data['Company']['active']) or $this->data['Company']['active']==0) echo 'selected';
					?>
				><?php echo __("No");?></option>
				</select>
		  </div>

		  
          <?php if($action == "admin_add"){ ?>

          <div class="form-group col-lg-6">
						   <label for="name">Logo *</label>
               <?php
					          echo $this->Form->input('Company.logo_up',array("class"=>"form-control","label"=>false,"required"=>false,'type' => 'file'));
                ?>
					</div>

          <?php }?>
					<div class="form-group col-lg-6">
						<?php echo $this->Form->input('esp', array('type' => 'checkbox', 'label' => __('Spanish') )); ?>
					</div>
					<div class="form-group col-lg-6">
					<?php echo $this->Form->input('eng', array('type' => 'checkbox', 'label' => __('English') )); ?>
					</div>

          <div class="form-group form_button">

                <div class="form-group">
                  <label for="note"><?php echo __("All fields with * are mandatory");?></label>
                </div>
								<button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

					</div>
          <div class="form-group form_response">
            <div id="responseForm"></div>
          </div>

      </form>


  </div>
</div>

<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Companies.add");
</script>
<?php } ?>
