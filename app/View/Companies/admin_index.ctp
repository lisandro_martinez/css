<?php

    $headerstitles = array(
                            'Company.id' => '#',
                            'Corporation.name' => __('Corporation'),
                            'Company.name' => __('Name'),
                            'Company.logo' => 'Logo',
                            'Company.description' => __('Description'),
                            'Company.phone' => __('Phone'),
                            'Company.address' => __('Address')
                      );

?>

<div class="widget stacked">
    <?php  $this->Viewbase->panel_title(__('Company List'));   ?>
    <div class="widget-content">
        <?php include_once('filter.ctp');
            $this->Viewbase->Multi_form_create($this->form,'#');
           ?>

        <table class="table table-bordered">
           <?php
                // Encabezado de la tabla
                $this->Viewbase->table_Header($this->Paginator,$headerstitles);
            ?>
            <tbody>
                <?php foreach ($lists as $list): ?>
                <tr>
                    <?php
                        // Campo check de cada linea
                        $datarow=array(
                            'idModel' => $list['Company']['id'],
                            'textname' => $list['Company']['name'],
                            'inputname'=> 'data[Company][id][]',
                            'value' => $list['Company']['id']
                        );
                        $this->Viewbase->Multi_check_row($datarow);
                        // Campo check de cada linea
                    ?>
                    <td style="width: 10px;"><?php echo h($list['Company']['id']); ?>&nbsp;</td>
					<td><?php echo h($list['Corporation']['name']); ?>&nbsp;</td>
                    <td><?php echo h($list['Company']['name']); ?>&nbsp;</td>
                    <td>
					  <?php
					  if (isset($this->data['Company']['logo']) and $this->data['Company']['logo']) {
					  ?>
						<a href="/<?php echo h($list['Company']['logo']); ?>" target="_blank" ><img src="/<?php echo h($list['Company']['logo']); ?>" width="45" height="auto"/></a>&nbsp;
					  <?php
					  }
					  ?>
					</td>
                    <td><?php echo h($list['Company']['description']); ?>&nbsp;</td>
                    <td><?php echo h($list['Company']['phone']); ?>&nbsp;</td>
                    <td><?php echo h($list['Company']['address']); ?>&nbsp;</td>



                     <td class="actions">

                        <?php


                             $databutton_edit = array(
                                    'url'=> '/admin/companies/edit/'.$list['Company']['id']
                                );
                                $this->Viewbase->button_edit($this->Html,$databutton_edit);



                             $databutton_delete = array(
                                    'url'=> '/admin/companies/delete/'.$list['Company']['id'],
                                    'idModel' =>$list['Company']['id'],
                                    'name'    =>$list['Company']['name']);
                                $this->Viewbase->button_delete($this->Html,$databutton_delete);



                        ?>
                    </td>

        	   </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="8">
                        <?php

                            $databutton_add = array(
                                    'url'=> '/admin/companies/add/');
                                $this->Viewbase->button_add($this->Html,$databutton_add);

                        ?>
                    </td>
                </tr>
            </tbody>

        </table>

        <div class="checkalldiv col-md-6">
            <script>
                checkalltext = {
                    'empty' : {
                        'title' : '<?php echo __("Warning");?>',
                        'text' : '<?php echo __("You must select at least one register to use the multi-record option");?>'
                    },
                    'deleteall' :{
                        'title' : '<?php echo __("Multiple register deletion confirmation");?>',
                        'url' : '/admin/companies/deletemulti/',
                        'pretext' : '<?php echo __("Are you sure you want to delete the following registers?");?>'
                    }
                };
            </script>
            <input type="checkbox" class="checkallclick" title="Check All">
            <label for="checkall"><?php echo __("Check All"); ?></label>
            <select id="selectmulti" name="submit_mult" class="autosubmit" style="margin-left:10px;">
                <option value="0" selected="selected"><?php echo __("With selected:"); ?></option>
                <option value="deleteall"><?php echo __("Delete All"); ?></option>
            </select>
        </div>
        <?php echo $this->Form->end(); ?>
        <?php echo $this->element('paginado'); ?>
    </div>
</div>

<?php if($action == "admin_index"){ ?>
 <script type="text/javascript">
    Controllers.push("Companies.index");
</script>
<?php } ?>
