 <ul class="nav nav-tabs">
              <li class="active"><a href="#home" class="filter-tab" data-toggle="tab"><?php echo $form_config["title"]; ?></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane" id="home">

                <div class="panel panel-default">
                  <div class="panel-body">
                    <?php
                    echo $this->Form->create('Group', array('action' => $form_config["urlform"],'class'=>'form-horizontal','type' => 'get'));
                    ?>
                    <div class="col-md-12">
                        <div class="form-group col-lg-6">
                            <label for="SearchName">Nombre de Grupo</label>
                            <?php echo $this->Form->input('Search.name',array('class'=>'form-control',"label"=>false,"required"=>false));?>
                        </div>

                    </div>

                    <div class="filter_button">
                      <input class="btn btn-primary" type="submit" value="<?php echo $form_config["labelbutton"]; ?>">
                      </div>

                  </form>

                </div>

                </div>
            </div>
        </div>
