<div>
            <ul class="nav nav-tabs">
              <li class="active"><a href="#home" class="filter-tab" data-toggle="tab"><?php echo $form_config["title"]; ?></a></li>
              <?php if(!empty($this->data['Search'])){ ?>
              <li><a href="?" class="btn btn-info"><?php echo __('Quitar Filtro');?></a></li>
              <?php } ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane" id="home">

                <div class="panel panel-default">
                  <div class="panel-body">
                    <?php
                    echo $this->Form->create('Surveyquestion', array('action' => $form_config["urlform"],'class'=>'form-horizontal','type' => 'get'));
                    ?>
					<div class="form-group col-lg-6">
						<?php echo $this->Form->input('Search.node_id',array('class'=>'form-control', "label"=>__("Survey"),"empty"=>__("Select All"),"type"=>"select","required"=>false,'options'=>$surveys));
						?>
					</div>
                  <div class="filter_button">
                    <input class="btn btn-primary" type="submit" value="<?php echo $form_config["labelbutton"]; ?>">
                  </div>
                </div>
              </form>
                </div>
            </div>
        </div>
