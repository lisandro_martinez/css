<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">


        <?php
          echo $this->Form->create('Surveyquestion', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
        ?>

		
		<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-md-6">
			   <label for="name"><?php echo __("Question");?> (ESP)*</label>
			   <input type="text" class="form-control" name="data[Surveyquestion][name][esp]" id="name" value="<?php echo isset($this->data['Surveyquestion']['name']['esp']) ? $this->data['Surveyquestion']['name']['esp'] : '';?>">
          </div>
					<?php else : ?>
					<div class="form-group col-md-6">
					<?php
		  if($action == "admin_edit"){echo $this->Form->input('Surveyquestion.id');}
		?>
		</div>
					<?php endif; ?>
					<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-md-6">
			   <label for="name"><?php echo __("Question");?> (ENG)*</label>
			   <input type="text" class="form-control" name="data[Surveyquestion][name][eng]" id="name" value="<?php echo isset($this->data['Surveyquestion']['name']['eng']) ? $this->data['Surveyquestion']['name']['eng'] : '';?>">
          </div>
					<?php endif; ?>
		<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-12 textareafield">
			   <label for="name"><?php echo __("Description");?> (ESP)*</label>
			   <textarea class="form-control" name="data[Surveyquestion][description][esp]" id="name"><?php echo isset($this->data['Surveyquestion']['description']['esp']) ? $this->data['Surveyquestion']['description']['esp'] : '';?></textarea>
		  </div>
			<?php endif; ?>
<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-lg-12 textareafield">
			   <label for="name"><?php echo __("Description");?> (ENG)*</label>
			   <textarea class="form-control" name="data[Surveyquestion][description][eng]" id="name"><?php echo isset($this->data['Surveyquestion']['description']['eng']) ? $this->data['Surveyquestion']['description']['eng'] : '';?></textarea>
		  </div>
			<?php endif; ?>
		  <?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-md-6 textareafield">
			   <label for="name"><?php echo __("Possible Answers (one by line)");?> (ESP)*</label>
			   <textarea class="form-control" name="data[Surveyquestion][answers][esp]" id="name"><?php echo $this->data['Surveyquestion']['answers']['esp'];?></textarea>
			</div>
			<?php endif; ?>
		  <?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-md-6 textareafield">
			   <label for="name"><?php echo __("Possible Answers (one by line)");?> (ENG)*</label>
			   <textarea class="form-control" name="data[Surveyquestion][answers][eng]" id="name"><?php echo $this->data['Surveyquestion']['answers']['eng'];?></textarea>
			</div>
			<?php endif; ?>

		<div class="form-group col-md-6">
			 <label for="name"><?php echo __("Associated Survey");?></label>
			 <?php 
			 
			 $valor = $this->data['Surveyquestion']['node_id'];
			 
			 if(!$valor and $node_id) {
				$valor = $node_id;
			 }
			 
			 echo $this->Form->input('node_id',
					array(	"div"=>false,
							"class"=>"form-control",
							"label"=>false,
							"empty"=>__("Choose"),
							"type"=>"select",
							"required"=>false,
							'options'=>$surveys,
							'value'=>$valor)); ?>
		</div>
		  
		<div class="form-group col-md-6">
			 <label for="name"><?php echo __("Question type");?></label>
			 <?php echo $this->Form->input('question_type',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>__("Choose"),"type"=>"select","required"=>false, "options"=>$question_types)); ?>
		</div>
		  
          <div class="form-group col-md-6">
			   <label for="name"><?php echo __("Order");?> *</label>
			   <input type="text" class="form-control" name="data[Surveyquestion][order]" id="name" value="<?php echo isset($this->data['Surveyquestion']['order']) ? $this->data['Surveyquestion']['order'] : '';?>">
			</div>
		  
          <div class="form-group form_button">
                <div class="form-group">
                  <label for="note"><?php echo __("All fields with * are mandatory");?></label>
                </div>
				<button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>
		  </div>
          <div class="form-group form_response">
            <div id="responseForm"></div>
          </div>

      </form>


  </div>
</div>



<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Surveyquestions.add");
</script>
<?php } ?>
