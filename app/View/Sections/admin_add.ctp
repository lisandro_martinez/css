<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">


        <?php
          echo $this->Form->create('Section', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
        ?>
        <?php
              if($action == "admin_edit"){echo $this->Form->input('Section.id');}
            ?>
<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-6">
            
						   <label for="name"><?php echo __("Section name");?> (ESP)*</label>
						   <input type="text" class="form-control" name="data[Section][name][esp]" id="name" value="<?php echo isset($this->data['Section']['name']['esp']) ? $this->data['Section']['name']['esp'] : '';?>">

		</div>
    <?php endif; ?>
<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-lg-6">
						   <label for="name"><?php echo __("Section name");?> (ENG)*</label>
						   <input type="text" class="form-control" name="data[Section][name][eng]" id="name" value="<?php echo isset($this->data['Section']['name']['eng']) ? $this->data['Section']['name']['eng'] : '';?>">
		</div>
    <?php endif; ?>

		<div class="form-group col-lg-6">
			 <label for="name"><?php echo __("Associated Questionnaire");?></label>
			 <?php echo $this->Form->input('questionnaire_id',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>__("Choose"),"type"=>"select","required"=>false)); ?>

		</div>
		  
          <?php if($action == "admin_edit"){ ?>
          <div class="form-group col-lg-6">
              <div>
			  <?php
			  if (isset($this->data['Section']['logo']) and $this->data['Section']['logo']) {
			  ?>
               <img src="<?php echo '/'.$this->data['Section']['logo'];?>" width="150px" height="auto" />
			  <?php
			  }
			  ?>
               <input type="hidden" name="data[Section][logo]" class="form-control" value="<?php echo $this->data['Section']['logo'];?>">
              </div>
              <label for="name">Logo </label>
              <?php
                   echo $this->Form->input('Section.logo_up_edit',array("class"=>"form-control","label"=>false,"required"=>false,'type' => 'file'));
               ?>
					</div>
          <?php }?>

          <?php if($action == "admin_add"){ ?>

          <div class="form-group col-lg-6">
						   <label for="name">Logo </label>
               <?php
					          echo $this->Form->input('Section.logo_up',array("class"=>"form-control","label"=>false,"required"=>false,'type' => 'file'));
                ?>
					</div>

          <?php }?>

          <div class="form-group col-lg-6">
			   <label for="name"><?php echo __("Order");?> *</label>
			   <input type="text" class="form-control" name="data[Section][order]" id="name" value="<?php echo isset($this->data['Section']['order']) ? $this->data['Section']['order'] : '';?>">
		  </div>

          <div class="form-group form_button">

                <div class="form-group">
                  <label for="note"><?php echo __("All fields with * are mandatory");?></label>
                </div>
								<button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

					</div>
          <div class="form-group form_response">
            <div id="responseForm"></div>
          </div>

      </form>


  </div>
</div>



<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Sections.add");
</script>
<?php } ?>
