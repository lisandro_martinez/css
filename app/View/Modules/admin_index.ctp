<?php
    $headerstitles = array(
                            'Module.id' => '#',
                            'Module.name' => 'Nombre',
                            'Module.order' => 'Orden');

?>

<div class="widget stacked">
    <?php  $this->Viewbase->panel_title('Listado de Módulos');   ?>

    <div class="widget-content">
         <?php include_once('filter.ctp');
            $this->Viewbase->Multi_form_create($this->form,'#');
           ?>
        <table class="table table-bordered">
            <?php
                // Encabezado de la tabla
                $this->Viewbase->table_Header($this->Paginator,$headerstitles);
            ?>
            <tbody>
                <?php foreach ($lists as $list): ?>
                <tr>
                        <?php
                        // Campo check de cada linea
                        $datarow=array(
                            'idModel' => $list['Module']['id'],
                            'textname' => $list['Module']['name'],
                            'inputname'=> 'data[Module][id][]',
                            'value' => $list['Module']['id']
                        );
                        $this->Viewbase->Multi_check_row($datarow);
                        // Campo check de cada linea
                    ?>

                    <td style="width: 10px;"><?php echo h($list['Module']['id']); ?>&nbsp;</td>
                    <td><?php echo h($list['Module']['name']); ?>&nbsp;</td>
                    <td><?php echo h($list['Module']['order']); ?>&nbsp;</td>


                     <td class="actions">

                        <?php


                        $databutton_edit = array(
                                'url'=> '/admin/modules/edit/'.$list['Module']['id']
                            );
                            $this->Viewbase->button_edit($this->Html,$databutton_edit);



                        $databutton_delete = array(
                                'url'=> '/admin/modules/delete/'.$list['Module']['id'],
                                'idModel' =>$list['Module']['id'],
                                'name' =>$list['Module']['name']
                            );
                            $this->Viewbase->button_delete($this->Html,$databutton_delete);


                        ?>
                    </td>

        	   </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="5">
                        <?php

                            $databutton_add = array(
                                    'url'=> '/admin/modules/add/');
                                $this->Viewbase->button_add($this->Html,$databutton_add);

                        ?>
                    </td>
                </tr>
            </tbody>

        </table>
        <div class="checkalldiv col-md-6">
            <script>
                checkalltext = {
                    'empty' : {
                        'title' : 'Advertencia',
                        'text' : 'Debe seleccionar al menos un módulo para utilizar la opción sobre multiples registros'
                    },
                    'deleteall' :{
                        'title' : 'Confirmación para eliminar multiples registros',
                        'url' : '/admin/modules/deletemulti/',
                        'pretext' : 'Estas seguro que deseas eliminar los siguientes registros?'
                    }
                };
            </script>
            <input type="checkbox" class="checkallclick" title="Check All">
            <label for="checkall"><?php echo __('Check All'); ?></label>
            <select id="selectmulti" name="submit_mult" class="autosubmit" style="margin-left:10px;">
                <option value="0" selected="selected"><?php echo __('With selected:'); ?></option>
                <option value="deleteall"><?php echo __('Delete'); ?></option>
            </select>
        </div>
        <?php echo $this->element('paginado'); ?>
        <?php echo $this->Form->end(); ?>

    </div>
</div>
 <?php if($action == "admin_index"){ ?>
 <script type="text/javascript">
    Controllers.push("Modules.index");
</script>
<?php } ?>
