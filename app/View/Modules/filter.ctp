<div>
            <ul class="nav nav-tabs">
              <li class="active"><a href="#home" class="filter-tab" data-toggle="tab"><?php echo $form_config["title"]; ?></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane" id="home">

                <div class="panel panel-default">
                  <div class="panel-body">
                    <?php
                    echo $this->Form->create('Module', array('action' => $form_config["urlform"],'class'=>'form-horizontal','type' => 'get'));
                    ?>
                    <div class="col-md-12">
                        <div class="form-group col-lg-6">
                    <?php
                    if($action == "admin_edit"){echo $this->Form->input('id');}
                    ?>
                    <label for="SearchName">Nombre de Modulo</label>
                    <?php
                    echo $this->Form->input('Search.name',array('class'=>'form-control',"label"=>false,"required"=>false));?>
                        </div>

                        <div class="form-group col-lg-6">
                          <label for="SearchOrder">Orden en el menú</label>
                         <?php
                            echo $this->Form->input('Search.order',array('class'=>'form-control',"label"=>false,"required"=>false));?>
                        </div>



                      </div>

                      <div class="filter_button">
                        <input class="btn btn-primary" type="submit" value="<?php echo $form_config["labelbutton"]; ?>">
                      </div>
                    </form>

                </div>
            </div>
        </div>
