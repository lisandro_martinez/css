<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">


        <?php
          echo $this->Form->create('Module', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
        ?>
        <?php
              if($action == "admin_edit"){echo $this->Form->input('Module.id',array('value'=>$id));}
            ?>
        <?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-6">
            
						   <label for="name">Nombre del Modulo (ESP)*</label>
						   <input type="text" class="form-control" name="data[Module][name][esp]" id="name" value="<?php echo isset($this->data['Module']['name']['esp']) ? $this->data['Module']['name']['esp'] : '';?>">
		</div>
    <?php endif; ?>
        <?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>

          <div class="form-group col-lg-6">
						   <label for="name">Nombre del Modulo (ENG)*</label>
						   <input type="text" class="form-control" name="data[Module][name][eng]" id="name" value="<?php echo isset($this->data['Module']['name']['eng']) ? $this->data['Module']['name']['eng'] : '';?>">
		</div>
    <?php endif; ?>
		
          <div class="form-group col-lg-6">
						   <label for="name">Orden en el menu *</label>
						   <input type="text" class="form-control" name="data[Module][order]" id="name" value="<?php echo isset($this->data['Module']['order']) ? $this->data['Module']['order'] : '99';?>">

		</div>

          <div class="form-group col-lg-6">
						   <label for="name">Icono *</label>
						   <input type="text" class="form-control" name="data[Module][icon]" id="name" value="<?php echo isset($this->data['Module']['icon']) ? $this->data['Module']['icon'] : '99';?>">

		</div>





          <div class="form-group form_button">

            <div class="form-group">
              <label for="note">Nota: Todos los campos con * son obligatorios</label>
            </div>

								<button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

					</div>

          <div class="form-group form_response">
            <div id="responseForm"></div>
          </div>

      </form>


  </div>
</div>



<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Modules.add");
</script>
<?php } ?>
