﻿<?php

    $headerstitles = array(
                            'Corporation.id' => '#',
                            'Corporation.name' => __("Name"),
                            'Corporation.logo' => 'Logo',
                            'Corporation.description' => __("Description"),
                            'Corporation.phone' => __("Phone"),
                            'Corporation.address' => __("Address")
                      );

?>

<div class="widget stacked">
    <?php  $this->Viewbase->panel_title(__('Corporations List'));   ?>
    <div class="widget-content">
        <?php include_once('filter.ctp');
            $this->Viewbase->Multi_form_create($this->form,'#');
           ?>

        <table class="table table-bordered">
           <?php
                // Encabezado de la tabla
                $this->Viewbase->table_Header($this->Paginator,$headerstitles);
            ?>
            <tbody>
                <?php foreach ($lists as $list): ?>
                <tr>
                    <?php
                        // Campo check de cada linea
                        $datarow=array(
                            'idModel' => $list['Corporation']['id'],
                            'textname' => $list['Corporation']['name'],
                            'inputname'=> 'data[Corporation][id][]',
                            'value' => $list['Corporation']['id']
                        );
                        $this->Viewbase->Multi_check_row($datarow);
                        // Campo check de cada linea
                    ?>
                    <td style="width: 10px;"><?php echo h($list['Corporation']['id']); ?>&nbsp;</td>
                    <td><?php echo h($list['Corporation']['name']); ?>&nbsp;</td>
                    <td>
					<?php if (isset($list['Corporation']['logo']) and $list['Corporation']['logo']) {?>
						<a href="/<?php echo h($list['Corporation']['logo']); ?>" target="_blank" ><img src="/<?php echo h($list['Corporation']['logo']); ?>" width="45" height="auto"/></a>&nbsp;
					<?php } ?>
					</td>
                    <td><?php echo h($list['Corporation']['description']); ?>&nbsp;</td>
                    <td><?php echo h($list['Corporation']['phone']); ?>&nbsp;</td>
                    <td><?php echo h($list['Corporation']['address']); ?>&nbsp;</td>



                     <td class="actions">

                        <?php


                             $databutton_edit = array(
                                    'url'=> '/admin/corporations/edit/'.$list['Corporation']['id']
                                );
                                $this->Viewbase->button_edit($this->Html,$databutton_edit);



                             $databutton_delete = array(
                                    'url'=> '/admin/corporations/delete/'.$list['Corporation']['id'],
                                    'idModel' =>$list['Corporation']['id'],
                                    'name'    =>$list['Corporation']['name']);
                                $this->Viewbase->button_delete($this->Html,$databutton_delete);



                        ?>
                    </td>

        	   </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="8">
                        <?php

                            $databutton_add = array(
                                    'url'=> '/admin/corporations/add/');
                                $this->Viewbase->button_add($this->Html,$databutton_add);

                        ?>
                    </td>
                </tr>
            </tbody>

        </table>

        <div class="checkalldiv col-md-6">
            <script>
                checkalltext = {
                    'empty' : {
                        'title' : '<?php echo __("Warning");?>',
                        'text' : '<?php echo __("You must select at least one register to use the multi-record option");?>'
                    },
                    'deleteall' :{
                        'title' : '<?php echo __("Multiple register deletion confirmation");?>',
                        'url' : '/admin/corporations/deletemulti/',
                        'pretext' : '<?php echo __("Are you sure you want to delete the following registers?");?>'
                    }
                };
            </script>
            <input type="checkbox" class="checkallclick" title="Check All">
            <label for="checkall"><?php echo __("Check All"); ?></label>
            <select id="selectmulti" name="submit_mult" class="autosubmit" style="margin-left:10px;">
                <option value="0" selected="selected"><?php echo __("With selected:"); ?></option>
                <option value="deleteall"><?php echo __("Delete All"); ?></option>
            </select>
        </div>
        <?php echo $this->Form->end(); ?>
        <?php echo $this->element('paginado'); ?>
    </div>
</div>

<?php if($action == "admin_index"){ ?>
 <script type="text/javascript">
    Controllers.push("Corporations.index");
</script>
<?php } ?>
