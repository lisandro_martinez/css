﻿<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">


        <?php
          echo $this->Form->create('Corporation', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
        ?>

          <div class="form-group col-lg-6">
            <?php
              if($action == "admin_edit"){echo $this->Form->input('Corporation.id');}
            ?>
						   <label for="name"><?php echo __("Corporation name");?> *</label>
						   <input type="text" class="form-control" name="data[Corporation][name]" id="name" value="<?php echo isset($this->data['Corporation']['name']) ? $this->data['Corporation']['name'] : '';?>">

					</div>

          <div class="form-group col-lg-6">
						   <label for="name"><?php echo __("Phone");?> *</label>
						   <input type="text" class="form-control" name="data[Corporation][phone]" id="phone" value="<?php echo isset($this->data['Corporation']['phone']) ? $this->data['Corporation']['phone'] : '';?>">

					</div>
<?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-12 textareafield">
						   <label for="name"><?php echo __("Description");?> (ESP)*</label>
						   <textarea class="form-control" name="data[Corporation][description][esp]" id="name"><?php echo isset($this->data['Corporation']['description']['esp']) ? $this->data['Corporation']['description']['esp'] : '';?></textarea>
		  </div>
      <?php endif; ?>
      <?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-lg-12 textareafield">
						   <label for="name"><?php echo __("Description");?> (ENG)*</label>
						   <textarea class="form-control" name="data[Corporation][description][eng]" id="name"><?php echo isset($this->data['Corporation']['description']['eng']) ? $this->data['Corporation']['description']['eng'] : '';?></textarea>
		  </div>
      <?php endif; ?>
<?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-12 textareafield">
						   <label for="name"><?php echo __("Address");?> (ESP) *</label>
						   <textarea class="form-control" name="data[Corporation][address][esp]" id="name"><?php echo isset($this->data['Corporation']['address']['esp']) ? $this->data['Corporation']['address']['esp'] : '';?></textarea>
		  </div>
      <?php endif; ?>
      <?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-lg-12 textareafield">
						   <label for="name"><?php echo __("Address");?> (ENG) *</label>
						   <textarea class="form-control" name="data[Corporation][address][eng]" id="name"><?php echo isset($this->data['Corporation']['address']['eng']) ? $this->data['Corporation']['address']['eng'] : '';?></textarea>
		  </div>
      <?php endif; ?>


          <?php if($action == "admin_edit"){ ?>
          <div class="form-group col-lg-6">
              <div>
			   <?php
			   if (isset($this->data['Corporation']['logo']) and $this->data['Corporation']['logo']) {
			   ?>
               <img src="<?php echo '/'.$this->data['Corporation']['logo'];?>" width="150px" height="auto" />
			   <?php
			   }
			   ?>
               <input type="hidden" name="data[Corporation][logo]" class="form-control" value="<?php echo $this->data['Corporation']['logo'];?>">
              </div>
              <label for="name">Logo *</label>
              <?php
                   echo $this->Form->input('Corporation.logo_up_edit',array("class"=>"form-control","label"=>false,"required"=>false,'type' => 'file'));
               ?>
					</div>
          <?php }?>

          <?php if($action == "admin_add"){ ?>

          <div class="form-group col-lg-6">
						   <label for="name">Logo *</label>
               <?php
					          echo $this->Form->input('Corporation.logo_up',array("class"=>"form-control","label"=>false,"required"=>false,'type' => 'file'));
                ?>
					</div>

          <?php }?>



          <div class="form-group col-lg-1 textareafield">
		  
          <label><?php echo __("Active");?></label>
				<select  class="form-control" name="data[Corporation][active]" style="min-width: 70px">
				<option value=1 
					<?php 
						if (isset($this->data['Corporation']['active']) and $this->data['Corporation']['active']==1) echo 'selected';
					?>
				><?php echo __("Yes");?></option>
				<option value=0 
					<?php 
						if (!isset($this->data['Corporation']['active']) or $this->data['Corporation']['active']==0) echo 'selected';
					?>
				><?php echo __("No");?></option>
				</select>
		  </div>

          <div class="form-group form_button">

                <div class="form-group">
                  <label for="note"><?php echo __("All fields with * are mandatory");?></label>
                </div>
								<button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

					</div>
          <div class="form-group form_response">
            <div id="responseForm"></div>
          </div>

      </form>


  </div>
</div>



<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Corporations.add");
</script>
<?php } ?>
