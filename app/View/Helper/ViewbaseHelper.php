<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class ViewbaseHelper extends AppHelper {

	public $action = '';
	public $actionmultipleselect = array();
	public $actionlocate = array();

	public function getCurrentLocale(){
        return CakeSession::read('Config.language');
    }


	 function getlangdate($date){
        $lang_current=$this->getCurrentLocale();

        if($lang_current == 'esp'){

        	 $date = str_replace('January','Enero',$date);
					  $date = str_replace('February','Febrero',$date);
					  $date = str_replace('March','Marzo',$date);
            $date = str_replace('April','Abril',$date);
						$date = str_replace('May','Mayo',$date);
						$date = str_replace('June','Junio',$date);
						$date = str_replace('July','Julio',$date);
            $date = str_replace('August','Agosto',$date);
						$date = str_replace('September','Septiembre',$date);
						$date = str_replace('October','Octubre',$date);
						$date = str_replace('November','Noviembre',$date);
            $date = str_replace('December','Diciembre',$date);


            $date = str_replace('Jan','Ene',$date);
            $date = str_replace('Apr','Abr',$date);
            $date = str_replace('Aug','Ago',$date);
            $date = str_replace('Dec','Dic',$date);


        }

        return $date;

    }

	public function set($vars,$value){
		$this->{$vars} = $value;
	}

	public function table_Header($paginator, $titles){

		echo "<thead>
            <tr>";

		// Campo check para seleccionar todos desde arriba
        $this->Multi_checkall_up_create($this->actionmultipleselect);

		foreach ($titles as $field => $title) {
			echo "<th>".$paginator->sort($field,$title)."</th>";
		}

		// Encabezado de si tiene acciones
        $this->Actions_headers($this->actionlocate);


        echo "</tr>
            </thead>";

	}

	public function table_Header_order($paginator, $titles){

		echo "<thead>
            <tr>";



		foreach ($titles as $field => $title) {
			echo "<th>".$paginator->sort($field,$title)."</th>";
		}

		// Encabezado de si tiene acciones
        $this->Actions_headers($this->actionlocate);


        echo "</tr>
            </thead>";

	}

	public function panel_title($title){

		$panel_title = "
		<div class=\"widget-header\">
					<i class=\"icon-list\"></i>
	        <h3>
	            ".__($title)."
	        </h3>
	    </div>
		";

		echo $panel_title;

	}

	public function Multi_form_create($form,$target){
		//if(in_array($this->action, $this->actionmultipleselect)){
            echo $form->create('Action', array('action' => $target,'type' => 'post'));
        //}
	}

	public function Multi_checkall_up_create(){
		//if(in_array($this->action, $this->actionmultipleselect)){
            echo "<th><input type=\"checkbox\" class=\"checkallclick\" title=\"Check All\"></th>";
        //}
	}

	public function Multi_check_row($datarow){



        //if(in_array($this->action, $this->actionmultipleselect)){

	        echo "<td style=\"width: 10px;\">
	            <input type=\"checkbox\" class=\"actionsdelete-check\"
	                   multitext=\"#".$datarow['idModel']." - ".$datarow['textname']."\"
	                   name=\"".$datarow['inputname']."\"
	                   value=\"".$datarow['value']."\"/>
	        	</td>";
        //}

	}

	public function Actions_headers(){

        //if(in_array($this->action, $this->actionlocate)){

	        echo "<th class=\"actions\" align=\"center\">
	        		<div align=\"center\">".__('Actions')."</div>
	        	  </th>";
         //}
	}

	public function add_semaforo($color, $pct) {
		if ($color == 1) {
			echo '<span class="treegrid-red">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;
			<span style="color: black;">' . $pct . '%</span>';
		} elseif ($color == 2) {
			echo '<span class="treegrid-yellow">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;
			<span style="color: black;">' . $pct . '%</span>';
		} else {
			echo '<span class="treegrid-green">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;
			<span style="color: black;">' . $pct . '%</span>';
		}
	}

	public function add_semaforo_grande($color, $pct) {
		if ($color == 1) {
			$the_color='red';
		} elseif ($color == 2) {
			$the_color='yellow';
		} else {
			$the_color='green';
		}
		echo '
			<div class="row-fluid" style="text-align: center;">
				<img src="/img/'.$the_color.'.png" style="max-width: 70px">
			</div>
			<div class="row-fluid" style="text-align: center;">
				<span style="color: black; font-size: 18px; ">' 
					. $pct . '%
				</span>
			</div>';
	}

	public function button_add_user($html, $data)
	{
		 echo $html->link(__('New User'), $data['url'], array('class' => 'btn btn-success modalaction', 'escape' => false));
	}

	public function button_add_indicator($html, $data)
	{
		 echo $html->link(__('New Management Indicator'), $data['url'], array('class' => 'btn btn-success modalaction', 'escape' => false));
	}

	public function button_add($html,$data){

		 echo $html->link('<span class="icon-plus"></span> ', $data['url'], array('class' => 'btn btn-success modalaction', 'escape' => false));

	}

	public function button_edit($html,$data){

		 echo $html->link('<span class="icon-pencil"></span> ', $data['url'], array('class' => 'btn btn-success modalaction', 'escape' => false));

	}

	public function button_delete($html,$data){

		if (isset($data['name']) and $data['name']) {
			$name=$data['name'];
		} elseif (isset($data['title']) and $data['title']) {
			$name=$data['title'];
		}
	
		 echo $html->link('<span class="icon-trash"></span> ', $data['url'], array('class' => 'btn btn-danger deleteitem','data-confirm-title'=>__("Confirmación de Eliminación de Registro"),'data-confirm-msg'=>__("¿Está seguro que desea eliminar el registro #").$data['idModel']." bajo el nombre :".$name."?", 'escape' => false));

	}

	public function button_generate($html,$data){

		 echo $html->link('<span class="icon-plus"> '.__('Generate PDF').'</span> ', $data['url'], array('class' => 'btn btn-success modalaction', 'escape' => false));

	}

	public function button_Download($html,$data){

		if ($data['url']) {
			echo $html->link('<span class="icon-download"> PDF</span> ', $data['url'], array('class' => 'btn btn-info modalaction noload', 'escape' => false, 'target' => '_blank'));
			echo "<hr>";
			echo "<input type=text onClick=\"this.setSelectionRange(0, this.value.length)\" readonly value=\"{$data['url']}\" class=\"form-control\">";
		}

	}

	public function button_details($html,$data){

		 echo $html->link('<span class="glyphicon glyphicon-list-alt"></span>', $data['url'], array('class' => 'btn btn-default modalaction','data-toggle'=>'tooltip' ,'data-placement'=>'top','title'=>__('Details'), 'escape' => false));

	}

	public function button_cerrar($html,$data){

		 echo $html->link(__('Cerrar'), $data['url'], array('class' => 'btn btn-primary modalaction', 'escape' => false));

	}

	public function button_approved($html,$data){

		 echo $html->link('<span class="glyphicon glyphicon-ok"></span>', $data['url'], array('class' => 'btn btn-info deleteitem','data-toggle'=>'tooltip' ,'data-placement'=>'top','title'=>__('Approve'),'data-confirm-title'=>__("Confirmación"),'data-confirm-msg'=>__("¿Está seguro que desea aprobar la orden #").$data['name']."?", 'escape' => false));

	}

	public function button_tracking($html,$data){

		 echo $html->link('<span class="glyphicon glyphicon-plane"></span> ', $data['url'], array('class' => 'btn btn-warning modalaction','data-toggle'=>'tooltip' ,'data-placement'=>'top','title'=>__('Tracking'), 'escape' => false));

	}

	public function button_editinfo($html,$data){

		 echo $html->link('<span class="glyphicon glyphicon-pencil"></span> ', $data['url'], array('class' => 'btn btn-default modalaction','data-toggle'=>'tooltip' ,'data-placement'=>'top','title'=>__('Edit Info'), 'escape' => false));

	}

	public function button_fraud($html,$data){

		 echo $html->link('<span class="glyphicon glyphicon-bullhorn"></span> ', $data['url'], array('class' => 'btn btn-warning modalaction','data-toggle'=>'tooltip' ,'data-placement'=>'top','title'=>__('Fraud'),'escape' => false));

	}

	public function button_noapproved($html,$data){

		 echo $html->link('<span class="glyphicon glyphicon-bullhorn"></span> ', $data['url'], array('class' => 'btn btn-warning modalaction','data-toggle'=>'tooltip' ,'data-placement'=>'top','title'=>__('Not Aprroved'),'escape' => false));

	}

	public function button_nofraud($html,$data){

		 echo $html->link('<span class="glyphicon glyphicon-ok"></span>', $data['url'], array('class' => 'btn btn-info deleteitem','data-toggle'=>'tooltip' ,'data-placement'=>'top','title'=>__('No Fraud'),'data-confirm-title'=>__("Confirmación"),'data-confirm-msg'=>__("¿Está seguro que desea activar la orden #").$data['name']."?", 'escape' => false));

	}

	public function button_delete_order($html,$data){

		 echo $html->link('<span class="glyphicon glyphicon-remove"></span> ', $data['url'], array('class' => 'btn btn-danger deleteitem','data-toggle'=>'tooltip' ,'data-placement'=>'top','title'=>__('Delete'),'data-confirm-title'=>__("Confirmación"),'data-confirm-msg'=>__("¿Está seguro que desea eliminar la orden #").$data['name']."?", 'escape' => false));

	}

	public function button_shipped($html,$data){

		 echo $html->link('<span class="glyphicon glyphicon-ok"></span>'.__('Shipped'), $data['url'], array('class' => 'btn btn-success deleteitem','data-confirm-title'=>__("Confirmación"),'data-confirm-msg'=>__("¿Está seguro que desea colocar como enviada la orden #").$data['name']."?", 'escape' => false));

	}
}
