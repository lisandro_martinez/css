 <ul class="nav nav-tabs">
              <li class="active"><a href="#home" class="filter-tab" data-toggle="tab"><?php echo $form_config["title"]; ?></a></li>
              <?php if(!empty($this->data['Search'])){ ?>
              <li><a href="?" class="btn btn-info"><?php echo __('Quitar Filtro');?></a></li>
              <?php } ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane" id="home">

                <div class="panel panel-default">
                  <div class="panel-body">
                    <?php
                    echo $this->Form->create('User', array('action' => $form_config["urlform"],'class'=>'form-horizontal','type' => 'get'));
                    ?>
                    <div class="col-md-12">
                        <div class="form-group col-lg-6">
                    <?php echo $this->Form->input('fname',array("label"=>"Nombres","required"=>false,'class'=>'form-control'));?>
                        </div>
                        <div class="form-group col-lg-6">
                    <?php echo $this->Form->input('lname',array("label"=>"Apellidos","required"=>false,'class'=>'form-control'));?>
                        </div>
                        <div class="form-group col-lg-6">
                    <?php echo $this->Form->input('username',array("label"=>"Usuario","required"=>false,'class'=>'form-control'));?>
                        </div>
                        <div class="form-group col-lg-6">
                    <?php  echo $this->Form->input('group_id',array('label'=>'Grupos','type'=>'select','class'=>'form-control','empty'=>'Seleccione','required'=>false));?>
                        </div>
                        <div class="form-group col-lg-6">
                    <?php  echo $this->Form->input('corporation_id',array('label'=>'Corporaciones','type'=>'select','class'=>'form-control','empty'=>'Seleccione','required'=>false));?>
                        </div>
                        <div class="form-group col-lg-6">
                    <?php  echo $this->Form->input('company_id',array('label'=>'Empresas','type'=>'select','class'=>'form-control','empty'=>'Seleccione','required'=>false));?>
                        </div>
                    </div>

                    <div class="filter_button">
                      <input class="btn btn-primary" type="submit" value="<?php echo $form_config["labelbutton"]; ?>">
                    </div>
                </div>
              </form>

                </div>
            </div>
        </div>
