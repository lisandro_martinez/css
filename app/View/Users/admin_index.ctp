<?php
    $headerstitles = array(
                            'User.id' => '#',
                            'Corporation.name' => __('Corporation'),
                            'Company.name' => __('Company'),
                            'User.username' => __('User'),
                            'Group.name' => __('Group')

                    ); //order by


?>

<div class="widget stacked">
   <?php  $this->Viewbase->panel_title(__('User List'));   ?>
   <div class="widget-content">
        <?php include_once('filter.ctp');
            $this->Viewbase->Multi_form_create($this->form,'#');
           ?>
<table class="table table-bordered">
    <?php
                // Encabezado de la tabla
                $this->Viewbase->table_Header($this->Paginator,$headerstitles);
            ?>
    <tbody>
        <?php foreach ($lists as $list): ?>
        <tr>
            <?php
                        // Campo check de cada linea
                        $datarow=array(
                            'idModel' => $list['User']['id'],
                            'textname' => $list['User']['username'],
                            'inputname'=> 'data[User][id][]',
                            'value' => $list['User']['id']
                        );
                        $this->Viewbase->Multi_check_row($datarow);
                        // Campo check de cada linea
                    ?>
            <td style="width: 10px;"><?php echo h($list['User']['id']); ?>&nbsp;</td>
            <td><?php echo h($list['Corporation']['name']); ?>&nbsp;</td>
            <td><?php echo h($list['Company']['name']); ?>&nbsp;</td>
            <td><?php echo h($list['User']['username']); ?>&nbsp;</td>
            <td><?php echo h($list['Group']['name']); ?>&nbsp;</td>


                     <td class="actions">

                        <?php


                         $databutton_edit = array(
                                'url'=> '/admin/users/edit/'.$list['User']['id']
                            );
                            $this->Viewbase->button_edit($this->Html,$databutton_edit);



                         $databutton_delete = array(
                                'url'=> '/admin/users/delete/'.$list['User']['id'],
                                'idModel' =>$list['User']['id'],
                                'name' =>$list['User']['fname'].' '.$list['User']['lname']
                            );
                            $this->Viewbase->button_delete($this->Html,$databutton_delete);

                        ?>
                    </td>
	   </tr>
        <?php endforeach; ?>
        <tr>
                    <td colspan="7  ">
                        <?php

                            $databutton_add = array(
                                    'url'=> '/admin/users/add/');
                                $this->Viewbase->button_add_user($this->Html,$databutton_add);

                        ?>
                    </td>
                </tr>
    </tbody>

</table>

        <div class="checkalldiv col-md-6">
            <script>
                checkalltext = {
                    'empty' : {
                        'title' : '<?php echo __("Warning");?>',
                        'text' : '<?php echo __("You must select at least one register to use the multi-record option");?>'
                    },
                    'deleteall' :{
                        'title' : '<?php echo __("Multiple register deletion confirmation");?>',
                        'url' : '/admin/users/deletemulti/',
                        'pretext' : '<?php echo __("Are you sure you want to delete the following registers?");?>'
                    }
                };
            </script>
            <input type="checkbox" class="checkallclick" title="Check All">
            <label for="checkall"><?php __('Check All'); ?></label>
            <select id="selectmulti" name="submit_mult" class="autosubmit" style="margin-left:10px;">
                <option value="0" selected="selected"><?php echo __('With selected:'); ?></option>
                <option value="deleteall"><?php echo  __('Delete All'); ?></option>
            </select>
        </div>
        <?php echo $this->Form->end(); ?>

<?php echo $this->element('paginado'); ?>
</div>
</div>
<?php if($action == "admin_index"){ ?>
 <script type="text/javascript">
    Controllers.push("Users.index");
</script>
<?php } ?>
