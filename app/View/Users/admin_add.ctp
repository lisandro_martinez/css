<script>
function ShowHideIG() {
	var elval=$( "#UserGroupId" ).val();
	if (elval==11) {
		$("#elIG").fadeIn();
	} else {
		$("#elIG").fadeOut();
	}

}
</script>
<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>

  <div class="widget-content">

    <?php
    echo $this->Form->create('User', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
    ?>

    <div class="form-group col-lg-6">
      <?php
        if($action == "admin_edit"){echo $this->Form->input('User.id');}
      ?>
         <label for="name"><?php echo __('Names');?> *</label>
         <input type="text" class="form-control" name="data[User][fname]" id="fname" value="<?php echo isset($this->data['User']['fname']) ? $this->data['User']['fname'] : '';?>">

    </div>

    <div class="form-group col-lg-6">
         <label for="name"><?php echo __('Lastnames');?> *</label>
         <input type="text" class="form-control" name="data[User][lname]" id="lname" value="<?php echo isset($this->data['User']['lname']) ? $this->data['User']['lname'] : '';?>">
    </div>

    <div class="form-group col-lg-6">
         <label for="name"><?php echo __('email');?> *</label>
         <input type="text" class="form-control" name="data[User][email]" id="lname" value="<?php echo isset($this->data['User']['email']) ? $this->data['User']['email'] : '';?>">
    </div>

    <div class="form-group col-lg-6">
         <label for="name"><?php echo __('User');?> *</label>
         <input type="text" class="form-control" name="data[User][username]" id="username" value="<?php echo isset($this->data['User']['username']) ? $this->data['User']['username'] : '';?>">
    </div>

    <?php if($action == 'admin_add'): ?>
    <div class="form-group col-lg-6">
         <label for="name"><?php echo __('Password');?> *</label>
         <input type="password" class="form-control" name="data[User][password]" id="password" value="<?php echo isset($this->data['User']['password']) ? $this->data['User']['password'] : '';?>">
    </div>
   <?php endif;?>

    <div class="form-group col-lg-6">
         <label for="name"><?php echo __('Group');?> *</label>
         <?php echo $this->Form->input('group_id',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>"Seleccione","type"=>"select","required"=>false, 'onchange'=>'ShowHideIG()')); ?>
    </div>

    <div class="form-group col-lg-6" id="elIG">
         <label for="name"><?php echo __('Interest Group');?></label>
         <?php echo $this->Form->input('interestgroup_id',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>__("Choose"),"type"=>"select","required"=>false,'options'=>$interest_groups)); ?>
    </div>
	
    <div class="form-group col-lg-6">
         <label for="name"><?php echo __('Age');?></label>
         <input type="text" class="form-control" name="data[User][age]" id="age" value="<?php echo isset($this->data['User']['age']) ? $this->data['User']['age'] : '';?>">
    </div>

    <div class="form-group col-lg-6">
         <label for="name"><?php echo __('Location');?></label>
         <input type="text" class="form-control" name="data[User][location]" id="location" value="<?php echo isset($this->data['User']['location']) ? $this->data['User']['location'] : '';?>">
    </div>

    <div class="form-group col-lg-6">
         <label for="name"><?php echo __('Profession');?></label>
         <input type="text" class="form-control" name="data[User][profession]" id="Profession" value="<?php echo isset($this->data['User']['profession']) ? $this->data['User']['profession'] : '';?>">
    </div>

    <div class="form-group col-lg-6">
         <label for="name"><?php echo __('Sex');?></label>
         <?php echo $this->Form->input('sex',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>__("Choose"),"type"=>"select","required"=>false,'options'=>array(__("Male"), __("Female")))); ?>
    </div>

<?php
if ($is_root) {
?>

    <div class="form-group col-lg-6">
         <label for="name"><?php echo __('Corporation');?></label>
         <?php echo $this->Form->input('corporation_id',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>"Seleccione","type"=>"select","required"=>false)); ?>

    </div>

    <div class="form-group col-lg-6">
         <label for="name"><?php echo __('Company');?></label>
         <?php echo $this->Form->input('company_id',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>"Seleccione","type"=>"select","required"=>false)); ?>
    </div>

<?php
}
?>
   <?php if($action == 'admin_edit'): ?>
   <div class="form-group col-lg-6">
        <label for="name"><?php echo __('Password change');?></label>
        <input type="hidden" class="form-control" name="data[User][password]" id="password" value="<?php echo isset($this->data['User']['password']) ? $this->data['User']['password'] : '';?>">
        <input type="password" class="form-control" name="data[User][password_update]" id="password_update" value="">
   </div>
  <?php endif;?>


    <div class="form-group col-lg-6">
         <label for="name"><?php echo __('Role');?></label>
         <?php echo $this->Form->input('role',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>__("Choose"),"type"=>"select","required"=>false,'options'=>$roles)); ?>
    </div>

    <div class="form-group col-lg-6">
         <p><label for="name"><?php echo __('Active');?></label></p>
			<?php 
			echo $this->Form->checkbox('User.active',array("label"=>__("Active"),"required"=>false,'class'=>''));
			?>
	</div>

    <div class="form-group form_button">

          <div class="form-group">
            <label for="note"><?php echo __('All fields with * are mandatory');?></label>
          </div>
          <button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

    </div>
    <div class="form-group form_response">
      <div id="responseForm"></div>
    </div>

    </form>

    </div>
</div>

<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Users.add");
	ShowHideIG();
</script>
<?php } ?>
