<div class="account-container stacked">

	<div class="content clearfix">

		<form action="/admin/users/login" method="post">

			<h1><?php echo __("Login");?></h1>

			<div class="login-fields">

				<p><?php echo __("Login with a valid account");?></p>

				<div class="field">
					<label for="username"><?php echo __("User");?>:</label>
					<input type="text" id="username" name="data[User][username]" value="" placeholder="<?php echo __("User");?>" class="form-control input-lg username-field">
				</div> <!-- /field -->

				<div class="field">
					<label for="password"><?php echo __("Password");?>:</label>
					<input type="password" id="password" name="data[User][password]" value="" placeholder="<?php echo __("Password");?>" class="form-control input-lg password-field">
				</div> <!-- /password -->

			</div> <!-- /login-fields -->

      <div id="responseForm"></div>

			<div class="login-actions">

				<button class="login-action btn btn-primary"><?php echo __("Login");?></button>

			</div> <!-- .actions -->

		</form>

	</div> <!-- /content -->

</div>
<script type="text/javascript">
			Controllers.push("Users.login");
</script>
