<section class="panel panel-default">
        <div class="panel-heading">
        	<strong>
        		<span class="icon-user"></span>
				<?php echo $form_config["title"]; ?>
        	</strong>
        </div>
        <div class="panel-body">
<?php 
echo $this->Form->create('User', array('action' => $form_config["urlform"]));
?>

	<?php 
	echo $this->Form->input('current_password',array("label"=>__("Current Password"),"required"=>false,'class'=>'form-control','type'=>'password'));
	echo $this->Form->input('new_password',array("label"=>__("New Password"),"required"=>false,'class'=>'form-control','type'=>'password'));
	echo $this->Form->input('confirm_password',array("label"=>__("New Password Confirmation"),"required"=>false,'class'=>'form-control','type'=>'password'));
	?>

          <div class="form-group form_response">
            <div id="responseForm"></div>
          </div>

<?php
echo $this->Form->end(array('label'=>$form_config["labelbutton"],'class'=>'btn btn-primary','type'=>'submit'));
?>
</div>
 </section>
 <script type="text/javascript">
	Controllers.push("Users.resetpassword");
</script>

