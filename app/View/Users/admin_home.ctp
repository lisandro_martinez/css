<?php
/*
* Panel de control de usuarios de empresas
*/

if ($groupId==6 or $groupId==7 or $groupId==8) {

?>


<div class="col-md-6">

<div class="widget stacked ">

  <div class="widget-header">
  	<i class="icon-bookmark"></i>
  	<h3><?php echo __('Welcome');?></h3>
  </div> <!-- /widget-header -->

  <div class="widget-content">

	<?php

	if ($this->Session->read('TheCorporation')["Corporation"]["logo"]){
		echo "<p style=\"text-align:center;\"><img src=\"/".$this->Session->read('TheCorporation')["Corporation"]["logo"]."\" style=\"max-width: 150px;\"></p>";
	}
	if ($this->Session->read('TheCorporation')["Corporation"]["name"]){
		echo "<p style=\"text-align:center; font-size: small;\">".$this->Session->read('TheCorporation')["Corporation"]["name"]."</p>";
	}

	if ($this->Session->read('TheCompany')["Company"]["logo"]){
		echo "<p style=\"text-align:center;\"><img src=\"/".$this->Session->read('TheCompany')["Company"]["logo"]."\" style=\"max-width: 150px;\"></p>";
	}
	if ($this->Session->read('TheCompany')["Company"]["name"]){
		echo "<p style=\"text-align:center; font-size: small;\">".$this->Session->read('TheCompany')["Company"]["name"]."</p>";
	}

	?>

	<h2 style="text-align:center;"><?php echo __("Welcome");?></h2>
	<h3 style="text-align:center;"><?php echo __("Corporate Sustainability System");?></h3>
	<p><?php echo __("HELP-HOME");?></p>
	
  </div> <!-- /widget-content -->

</div> <!-- /widget -->

</div>

<div class="col-md-6">
			
<div class="widget stacked">
		
	<div class="widget-header">
		<i class="icon-user"></i>
		<h3><?php echo __("Options");?></h3>
	</div> <!-- /widget-header -->
	
	<div class="widget-content">
		
		<div class="shortcuts">
		
			<a class="shortcut" href="/admin/entities/wizard/?e=2&n=1">
				<i class="shortcut-icon icon-star"></i>
				<span class="shortcut-label"><?php echo __("Strategic Management Wizard");?></span>
			</a>
			
			<a class="shortcut" href="/admin/autoevaluations/index">
				<i class="shortcut-icon icon-check"></i>
				<span class="shortcut-label"><?php echo __("Self Assessments");?></span>
			</a>
			
			<a class="shortcut" href="/admin/entities/inventory">
				<i class="shortcut-icon icon-comment"></i>
				<span class="shortcut-label"><?php echo __("Weaknesses and Opportunities");?></span>								
			</a>
			
			<a class="shortcut" href="/admin/entities/maturity">
				<i class="shortcut-icon icon-list-alt"></i>
				<span class="shortcut-label"><?php echo __("Dashboard");?></span>	
			</a>
			
			<a class="shortcut" href="/admin/entities/bsc">
				<i class="shortcut-icon icon-th-large"></i>
				<span class="shortcut-label"><?php echo __("Balance ScoreCard");?></span>	
			</a>
			
			<a class="shortcut" href="<?php echo "/".$this->Session->read('TheCompany')["Company"]["slug"];?>">
				<i class="shortcut-icon icon-home"></i>
				<span class="shortcut-label"><?php echo __("Go to public portal");?></span>
			</a>
			
	</div> <!-- /shortcuts -->	
	
	</div> <!-- /widget-content -->
				
</div>

</div>

<!--

<div class="col-md-6">
			
<div class="widget stacked">
		
	<div class="widget-header">
		<i class="icon-pushpin"></i>
		<h3><?php echo __("Balance Scorecard");?></h3>
	</div> 
	
	<div class="widget-content">
		
		<div class="shortcuts">
			<a class="shortcut" href="javascript:;">
				<i class="shortcut-icon icon-usd"></i>
				<span class="shortcut-label">Perspectiva Finaciera</span>
			</a>
			
			<a class="shortcut" href="javascript:;">
				<i class="shortcut-icon icon-user"></i>
				<span class="shortcut-label">Perspectiva de Clientes</span>								
			</a>
			
			<a class="shortcut" href="javascript:;">
				<i class="shortcut-icon icon-arrow-down"></i>
				<span class="shortcut-label">Perspectiva Interna</span>	
			</a>
			
			<a class="shortcut" href="javascript:;">
				<i class="shortcut-icon icon-signal"></i>
				<span class="shortcut-label">Perspectiva Crecimiento</span>								
			</a>
			
		</div> 
	
	</div> 
				
</div>

</div>

-->

<?php } ?>