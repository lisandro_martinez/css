<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<title>Corporate Sustainability System</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
  <link href="/css/font-awesome.min.css" rel="stylesheet">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
  <link href="/css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">
  <link href="/css/base-admin-3.css" rel="stylesheet">
  <link href="/css/base-admin-3-responsive.css" rel="stylesheet">
  <link href="/css/pages/signin.css" rel="stylesheet" type="text/css">
  <link href="/css/custom.css" rel="stylesheet">
  <script type="text/javascript">
		var Controllers = new Array;
		var checkalltext ={};
	</script>

</head>
<body>
      <?php echo $this->element('menu_login'); ?>
      <?php echo $this->element('flash');?>
			<?php echo $this->fetch('content'); ?>


			<script src="/js/libs/jquery-3.1.1.min.js"></script>
			
			<script src="/js/libs/jquery-ui-1.10.0.custom.min.js"></script>
      <script src="/js/libs/jquery.form.min.js"></script>
			<script src="/js/libs/bootstrap.min.js"></script>
			<script src="/js/Application.js"></script>
      <script src="/js/Controllers/App.js"></script>
      <script src="/js/Controllers/Users.js"></script>



	    <?php echo $this->element('sql_dump'); ?>
</body>
</html>
