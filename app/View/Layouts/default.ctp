<!DOCTYPE html>
<html lang="en">
  <head>
	<meta charset="utf-8">
	<title>Corporate Sustainability System</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
  <link href="/css/font-awesome.min.css" rel="stylesheet">
  <link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600" rel="stylesheet">
  <link href="/css/ui-lightness/jquery-ui-1.10.0.custom.min.css" rel="stylesheet">
  <link href="/js/plugins/msgbox/jquery.msgbox.css" rel="stylesheet">
  <link href="/css/base-admin-3.css" rel="stylesheet">
  <link href="/css/base-admin-3-responsive.css" rel="stylesheet">
  <link href="/css/pages/signin.css" rel="stylesheet" type="text/css">
  <link href="/css/custom.css" rel="stylesheet">
  <script type="text/javascript">
		var Controllers = new Array;
		var checkalltext ={};
	</script>
	<script src="/js/libs/jquery-3.1.1.min.js"></script>
	<script src="/js/libs/jquery-ui-1.10.0.custom.min.js"></script>
	<script src="/js/libs/jquery.form.min.js"></script>
	<script src="/js/libs/bootstrap.min.js"></script>
	<script src="/js/plugins/msgbox/jquery.msgbox.min.js"></script>
	<script src="/js/Application.js"></script>
	<script src="/js/Controllers/App.js"></script>
	<script src="/js/Controllers/Users.js"></script>
	<script src="/js/Controllers/Groups.js"></script>
	<script src="/js/Controllers/Modules.js"></script>
	<script src="/js/Controllers/Categories.js"></script>
	<script src="/js/Controllers/Actions.js"></script>
	<script src="/js/Controllers/Groupactions.js"></script>
	<script src="/js/Controllers/Companies.js"></script>
	<script src="/js/Controllers/Corporations.js"></script>
	<script src="/js/Controllers/Questionnaires.js"></script>
	<script src="/js/Controllers/Values.js"></script>
	<script src="/js/Controllers/Sections.js"></script>
	<script src="/js/Controllers/Questions.js"></script>
	<script src="/js/Controllers/Answers.js"></script>
	<script src="/js/Controllers/Companyquestionnaires.js"></script>
	<script src="/js/Controllers/Autoevaluations.js"></script>
	<script src="/js/Controllers/Nodetypes.js"></script>
	<script src="/js/Controllers/Nodepriorities.js"></script>
	<script src="/js/Controllers/Reports.js"></script>
	<script src="/js/Controllers/Reportpages.js"></script>
	<script src="/js/Controllers/Nodesources.js"></script>
	<script src="/js/Controllers/Nodes.js"></script>
	<script src="/js/Controllers/Indicators.js"></script>
	<script src="/js/Controllers/Indicatorvalues.js"></script>
	<script src="/js/Controllers/Nodestatuses.js"></script>
	<script src="/js/Controllers/Surveyquestions.js"></script>

</head>
<body onunload=""> <!-- onunload clear the load when press the back button -->

<div id="load"></div>

<div class="app">

      <?php echo $this->element('menu'); ?>
      <?php echo $this->element('flash');?>
      <div class="main">

          <div class="container">

            <div class="row">

			<?php if($this->element('aside')) { ?>
			
              <div class="col-md-2 nav-content">

                <?php echo $this->element('aside');?>

              </div>

              <div class="col-md-10">

			<?php } else { ?>
			
              <div class="col-md-12">

			<?php } ?>
			  
                <?php echo $this->fetch('content'); ?>
              </div>


            </div>


          </div>

      </div>
	  
	  
<div class="footer">
		
	<div class="container">
		
		<div class="row">
			
			<div class="col-md-6" id="footer-copyright">
				&copy; <?php echo date('Y', time());?> Copyright Corporate Sustainability System.
			</div> <!-- /span6 -->
			
			<div class="col-md-6" id="footer-terms">
				<a target="_blank" href="http://SiderysBSN.com">SiderysBSN.com</a>
			</div> <!-- /.span6 -->
			
		</div> <!-- /row -->
		
	</div> <!-- /container -->
	
</div>	  

	    <?php echo $this->element('sql_dump'); ?>


<?php
if (isset($summernote) and $summernote) {
?>
<!-- Inicio de Summer Note -->
	
	<!-- include summernote -->
	<link rel="stylesheet" href="/summernote/dist/summernote.css">
	<script type="text/javascript" src="/summernote/dist/summernote.min.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
		  $('.summernote').summernote({
			height: 200,
			tabsize: 2,
			codemirror: {
			  theme: 'monokai'
			}
		  });
		});
	</script>

<!-- Fin de Summer Note -->
<?php
}
?>

</div>
</div>		
</body>
</html>
