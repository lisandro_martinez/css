<link rel="stylesheet" href="/css/faq.css">
      	
<?php
/*****************************************************************************************************************************
*
*
* Paso 1: selecciona el cuestionario
*
*
******************************************************************************************************************************/ 
if (!isset($paso) or $paso==1) {
?>

	<script>
	function dale(i) {
		document.getElementById("form"+i).submit();
	}
	</script>

	<div class="col-md-8">
		
		<div class="widget stacked">
				
			<div class="widget-header">
				<i class="icon-pencil"></i>
				<h3><?php echo __("Autoevaluation");?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				
				<div class="faq-container">
				
				<h3><?php echo __("Select the questionnaire you want to use");?></h3>
	 
					 <?php 
					 
					 $i=1;
					 
					 foreach($questionnaires as $q) { 
					 
					 ?>

						<form action="/admin/autoevaluations/add" method="post" id="form<?php echo $i;?>">
					 
						<?php
							echo "<input type=hidden name=data[Autoevaluation][questionnaire_id] value=".$q["Questionnaire"]["id"].">";
						?>
					 
							<ol class="faq-list">
								
								<li id="faq-1"><div class="faq-icon"><div class="faq-number"><?php echo $i;?></div></div><div class="faq-text">
								
		<?php 
			if($q["Questionnaire"]["logo"]) echo "<p><img src=\"/".$q["Questionnaire"]["logo"]."\" style=\"float: right; max-height: 150px;max-width: 300px;\"></p>";
		?>
								
										<h4><?php echo $q["Questionnaire"]["acronym"]; ?>: <?php echo $q["Questionnaire"]["title"]; ?></h4>
										<p><?php echo $q["Questionnaire"]["description"]; ?></p>
										
								</div></li>
								
								<a class="btn btn-primary btn-support-ask" href="javascript:dale(<?php echo $i++;?>);"><?php echo __("Start");?></a>
								
							</ol>
								
						</form>

					 <?php } ?>
	 
			</div>
				
				
			</div> <!-- /widget-content -->
				
		</div> <!-- /widget -->	
		
	</div> <!-- /span8 -->
	
	
	
	<div class="col-md-4">
				
		<div class="widget stacked widget-box">
			
			<div class="widget-header">	
				<i class="icon-info"></i>
				<h3><?php echo __("Helper"); ?></h3>			
			</div> <!-- /widget-header -->
			
			<div class="well">
				
				<p><?php echo __("HELP-AUTOEVAL-1"); ?></p>
				
			</div> <!-- /widget-content -->
			
		</div> <!-- /widget -->
		
	</div> <!-- /span4 -->
      	
<?php
/*****************************************************************************************************************************
*
*
* 	Paso 2: Mostrar los datos del cuestionario, las secciones, las preguntas
*
*
******************************************************************************************************************************/ 
} elseif ($paso==2) {

	echo $this->Form->create('Autoevaluation', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));

?>

	<script>
	function dale() {
		document.getElementById("AutoevaluationAdminAddForm").submit();
	}
	</script>




	<div class="col-md-8">
		
	<input type=hidden name="data[paso]" value="3">
	<input type=hidden name="data[questionnaire_id]" value="<?php echo $qid;?>">
	<input type=hidden name="data[autoeval_id]" value="<?php echo $autoeval_id;?>">
	
		<?php 
		$sec=1;
		$i=1;
		foreach($s as $section) { 
		?>
	
			<div class="widget stacked">
					
				<div class="widget-header">
					<i class="icon-pencil"></i>
					<h3><?php echo __("Section").": ".$sec++; ?></h3>
				</div> <!-- /widget-header -->

				
				<div class="widget-content">
					
					<div class="page-header">
						<?php if($section["Section"]["logo"]) 
							echo "<p><img src=\"/".$section["Section"]["logo"]."\" style=\"float: left; max-height: 50px; padding-bottom: 10px;\"></p>";?>
						<h3><?php echo $section["Section"]["name"]; ?></h3>
					</div>

					<div class="faq-container">
					
						<?php 
						foreach($section["Questions"] as $question) { 
						?>

							<ol class="faq-list">
								
								<li id="faq-1"><div class="faq-icon"><div class="faq-number"><?php echo $i++;?></div></div><div class="faq-text">

									<h4><?php echo $question["Question"]["name"]; ?></h4>
								
									<div style="margin-left: 20px; margin-top: 20px;">
									<?php foreach($question["Answers"] as $answer) { ?>
									
										<p><label>
											<input 	type="radio" 
													name="data[Answers][<?php echo $question["Question"]["id"]?>]" 
													value="<?php echo $answer["Answer"]["id"]?>"
													<?php 
													if (isset($respuestas_dadas2[$question["Question"]["id"]]) and
														$respuestas_dadas2[$question["Question"]["id"]]==$answer["Answer"]["id"]) 
														echo " checked ";
													?>
													>
										<?php echo $answer["Answer"]["name"]; ?></label></p>


									<?php } ?>
									</div>
									
								</div></li>
								
							</ol>

					<?php } ?>
		 
		 
					</div>
					
				</div>
				
			</div>
			
		<?php } ?>
	
		<a class="btn btn-primary btn-support-ask" href="javascript:dale();"><?php echo __("Continue");?></a>
		
	</div>


	<div class="col-md-4">
				
		<div class="widget stacked widget-box">
			
			<div class="widget-header">	
				<i class="icon-pencil"></i>
				<h3><?php echo $q["Questionnaire"]["acronym"]; ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				
				<?php if($q["Questionnaire"]["logo"]) 
					echo "<p style='text-align: center;'><img src=\"/".$q["Questionnaire"]["logo"]."\" class=\"logo-quest\"></p>";
				?>
				
				<h1 style='text-align: center;'><?php echo $q["Questionnaire"]["acronym"]; ?></h1>
				<h3 style='text-align: center;'><?php echo $q["Questionnaire"]["title"]; ?></h3>
				
			</div> <!-- /widget-content -->
			
		</div> <!-- /widget -->
		
		<div class="widget stacked widget-box">
			
			<div class="widget-header">	
				<i class="icon-info"></i>
				<h3><?php echo __("Helper"); ?></h3>			
			</div> <!-- /widget-header -->
			
			<div class="well">
				
				<p><?php echo __("HELP-AUTOEVAL-2"); ?></p>
				
			</div> <!-- /widget-content -->
			
		</div> <!-- /widget -->
		
	</div> <!-- /span4 -->


	
	<input type=hidden name="data[paso]" value="3">
	
<?php
/*****************************************************************************************************************************
*
*
* 	Paso 3: Muestra los resultados, pregunta si desea guardar, publicar o descartar
*
*
******************************************************************************************************************************/ 
} elseif ($paso==3) {


//	pr($laq);
//	pr($autoeval_id);

?>

	<div class="col-md-8">
	
		<div class="widget stacked">
				
			<div class="widget-header">
				<i class="icon-pencil"></i>
				<h3><?php echo __("Autoevaluation Result");?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">

				<?php
				
				echo "<h1>".$puntos."/".$puntaje_total." (".$porcentaje."%)</h1>";
				
				?>
	
				<div class="progress">
				  <div style="width: <?php echo $porcentaje;?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?php echo $porcentaje;?>" role="progressbar" class="progress-bar progress-bar-primary">
					<span class="sr-only"><?php echo $porcentaje;?>%</span>
				  </div>
				</div>
			
			</div> <!-- /widget-content -->
				
		</div> <!-- /widget -->		
	
		<div class="widget stacked">
				
			<div class="widget-header">
				<i class="icon-eye-open"></i>
				<h3><?php echo __("Feedback");?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				
				<div class="page-header">
					<h3><?php echo __("Weaknesses and Opportunities Inventory"); ?></h3>
				</div>

				<?php
				$i=1;
				foreach($las_respuestas as $r) {
					// echo "<h1>".$r["Answer"]["feedback_title"]."</h1>";
					// echo $r["Answer"]["feedback"];
				?>

					<div class="faq-container">
					
						<ol class="faq-list">
							
							<li id="faq-1"><div class="faq-icon"><div class="faq-number"><?php echo $i++;?></div></div>
							
							<div class="faq-text">

								<h4><?php echo $r["Answer"]["feedback_title"]; ?></h4>
							
								<?php echo $r["Answer"]["feedback"]; ?>

							</div></li>
							
						</ol>

					</div>




				<?php
					
				}
				
				?>
	
			</div> <!-- /widget-content -->
				
		</div> <!-- /widget -->		
	
		<div class="widget stacked">
			<div class="widget widget-plain">
				<div class="widget-content">
					<a class="btn btn-primary btn-support-ask" href="javascript:;"><?php echo __("Save Opportunities and Weakness to Strategic Management");?></a>	
				</div> <!-- /widget-content -->
			</div>		
		</div> <!-- /widget -->		
		
	</div>


	<div class="col-md-4">
				
		<div class="widget stacked widget-box">
			
			<div class="widget-header">	
				<i class="icon-pencil"></i>
				<h3><?php echo $q["Questionnaire"]["title"]; ?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				
				<?php if($q["Questionnaire"]["logo"]) 
					echo "<p style='text-align: center;'><img src=\"/".$q["Questionnaire"]["logo"]."\" style=\"max-height: 150px;\"></p>";
				?>
				
				<h1 style='text-align: center;'><?php echo $q["Questionnaire"]["acronym"]; ?></h1>
				<h3 style='text-align: center;'><?php echo $q["Questionnaire"]["title"]; ?></h3>
				
			</div> <!-- /widget-content -->
			
		</div> <!-- /widget -->
		
		<div class="widget stacked widget-box">
			
			<div class="widget-header">	
				<i class="icon-info"></i>
				<h3><?php echo __("Helper"); ?></h3>			
			</div> <!-- /widget-header -->
			
			<div class="well">
				
				<p><?php echo __("HELP-AUTOEVAL-3"); ?></p>
				
			</div> <!-- /widget-content -->
			
		</div> <!-- /widget -->
		
	</div> <!-- /span4 -->

<?php
}
?>


			


<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Autoevaluations.add");
</script>
<?php } ?>
