<script>
function dale() {
	document.getElementById("admin_view").submit();
}
</script>

<style>
input[type=checkbox] {
  margin: 15px;
}
</style>

<link rel="stylesheet" href="/css/faq.css">
      	
<form action="/admin/autoevaluations/view/<?php echo $autoeval_id;?>" id="admin_view" method="post">

<input type=hidden name=autoeval_id value="<?php echo $autoeval_id;?>">

<div class="col-md-8">

	<div class="widget stacked">
			
		<div class="widget-header">
			<i class="icon-pencil"></i>
			<h3><?php echo __("Autoevaluation Result");?></h3>
		</div> <!-- /widget-header -->
		
		<div class="widget-content">

			<?php
			
			echo "<h1>".$puntos."/".$puntaje_total." (".$porcentaje."%)</h1>";
			
			?>

			<div class="progress">
			  <div style="width: <?php echo $porcentaje;?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?php echo $porcentaje;?>" role="progressbar" class="progress-bar progress-bar-primary">
				<span class="sr-only"><?php echo $porcentaje;?>%</span>
			  </div>
			</div>
		
		</div> <!-- /widget-content -->
			
	</div> <!-- /widget -->		

	<div class="widget stacked">
			
		<div class="widget-header">
			<i class="icon-eye-open"></i>
			<h3><?php echo __("Feedback");?></h3>
		</div> <!-- /widget-header -->
		
		<div class="widget-content">
			
			<div class="page-header">
				<h3><?php echo __("Weaknesses and Opportunities Inventory"); ?></h3>
			</div>

			<?php
			$i=1;
			
			$button_save=false;
				
			foreach($las_respuestas as $r) {
				// echo "<h1>".$r["Answer"]["feedback_title"]."</h1>";
				// echo $r["Answer"]["feedback"];
				
				if ($r["Answer"]["feedback_title"] or $r["Answer"]["feedback"]) {
				
			?>

				<div class="faq-container">
				
					<ol class="faq-list">
						
						<li id="faq-1"><div class="faq-icon"><div class="faq-number"><?php echo $i++;?></div></div>
						
						<div class="faq-text">

							<h4><?php echo $r["Answer"]["feedback_title"]; ?></h4>
						
								<div class="checkbox">
									<label>
									<?php 
										if (!$r["Autoevaluationanswer"]["saved"]) {
											echo "<input type=checkbox name=\"data[AutoevaluationsToSave][".$r["Autoevaluationanswer"]["id"]."]\" value=\"".$r["Answer"]["id"]."\" checked>";
											
											$button_save=true;
				
										} else {
										
											echo '<i class="icon-ok"></i>';
										
										}
										echo $r["Answer"]["feedback"]; 
									?>
									</label>
									
								</div>						
								
						</div>
						
						</li>
						
					</ol>

				</div>

			<?php
			
				}
				
			}
			
			?>

		</div> <!-- /widget-content -->
			
	</div> <!-- /widget -->		

	<?php if($button_save) { ?>
	<div class="widget stacked">
		<div class="widget widget-plain">
			<div class="widget-content">
				<a class="btn btn-primary btn-support-ask" href="javascript:dale();"><?php echo __("Save Opportunities and Weakness to SM");?></a>	
			</div> <!-- /widget-content -->
		</div>		
	</div> <!-- /widget -->		
	<?php } ?>
	
</div>


<div class="col-md-4">
			
	<div class="widget stacked widget-box">
		
		<div class="widget-header">	
			<i class="icon-pencil"></i>
			<h3><?php echo $q["Questionnaire"]["acronym"]; ?></h3>
		</div> <!-- /widget-header -->
		
		<div class="widget-content">
			
			<?php if($q["Questionnaire"]["logo"]) 
				echo "<p style='text-align: center;'><img src=\"/".$q["Questionnaire"]["logo"]."\" class=\"logo-quest\"></p>";
			?>
			
			<h1 style='text-align: center;'><?php echo $q["Questionnaire"]["acronym"]; ?></h1>
			<h3 style='text-align: center;'><?php echo $q["Questionnaire"]["title"]; ?></h3>
			
		</div> <!-- /widget-content -->
		
	</div> <!-- /widget -->
	
	<div class="widget stacked widget-box">
		
		<div class="widget-header">	
			<i class="icon-info"></i>
			<h3><?php echo __("Helper"); ?></h3>			
		</div> <!-- /widget-header -->
		
		<div class="well">
			
			<p><?php echo __("HELP-AUTOEVAL-3"); ?></p>
			
		</div> <!-- /widget-content -->
		
	</div> <!-- /widget -->
	
</div> <!-- /span4 -->

</form>