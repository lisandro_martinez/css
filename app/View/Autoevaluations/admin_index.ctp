<link rel="stylesheet" href="/css/faq.css">
<script>
	function dale(i) {
		document.getElementById("form"+i).submit();
	}
</script>

<div class="page-header">
	<h1><i class="icon-check"></i> <?php echo __("Autoevaluation List");?></h1>
</div>

<?php
if (isset($auto) and is_array($auto)) {
	foreach($auto as $a) {
?>

	<div class="col-md-6">

		<div class="widget stacked ">

		  <div class="widget-header">
			<i class="icon-pencil"></i>
			<h3><?php echo $a["Questionnaire"]["acronym"];?></h3>
		  </div> <!-- /widget-header -->

		  <div class="widget-content">

			<?php if(isset($a["Questionnaire"]["logo"]) and $a["Questionnaire"]["logo"]) {?>
				<p style="text-align: center;"><img class="logo-quest" src="/<?php echo $a["Questionnaire"]["logo"];?>"></p>				
			<?php } ?>
			<h1 style="text-align: center;"><?php echo $a["Questionnaire"]["acronym"];?></h1>
			<h3 style="text-align: center;"><?php echo $a["Questionnaire"]["title"];?></h3>
			<h1 style="text-align: center;"><?php echo date('Y', time($a["Autoevaluation"]["date_time"]));?></h1>
			<p style="text-align: center;"><?php echo $a["Autoevaluation"]["date_time"];?></p>

			<center>
			<table style="border:0"><tbody><tr><td style="border:0">
				 <a href="/admin/autoevaluations/view/<?php echo $a["Autoevaluation"]["id"];?>" class="btn btn-info"><span class="icon-zoom-in"></span> </a>
			 </td><td style="border:0">
				 <form id="form<?php echo $a["Autoevaluation"]["id"];?>" method="post" action="/admin/autoevaluations/add/">
				 <input type="hidden" value="<?php echo $a["Questionnaire"]["id"];?>" name="data[Autoevaluation][questionnaire_id]">
				 <input type="hidden" value="2" name="data[paso]">
				 <input type="hidden" value="<?php echo $a["Autoevaluation"]["id"];?>" name="data[autoeval_id]">
				 </form>
				 <a href="javascript:dale('<?php echo $a["Autoevaluation"]["id"];?>');" class="btn btn-success modalaction"><span class="icon-pencil"></span> </a>
			 </td><td style="border:0">
					<a 	class="btn btn-danger deleteitem" 
						href="/admin/autoevaluations/delete/<?php echo $a["Autoevaluation"]["id"];?>"
						onclick="return confirm('<?php echo __("Are you sure you want to delete the following registers?");?>');"
					><span class="icon-remove"></span> </a> 					 
			</td></tr></tbody></table>
			</center>
			


		  </div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div>

<?php
	}
}
?>

	<div class="col-md-6">

		<div class="widget stacked ">

		  <div class="widget-header">
			<i class="icon-plus"></i>
			<h3><?php echo __("Autoevaluation");?></h3>
		  </div> <!-- /widget-header -->

		  <div class="widget-content">

			<p><a href="/admin/autoevaluations/add/" class="btn btn-primary btn-support-ask"><?php echo __("Conduct a Self-Assessment");?></a></p>
							
		  </div> <!-- /widget-content -->

		</div> <!-- /widget -->

	</div>

