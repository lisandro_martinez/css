<script>
function elscript() {
	$("#lalista2").html('<center><img src="/css/images/loader.gif" style="opacity: 0.4;filter: alpha(opacity=40);" /></center>');
	$.ajax({
		type: "POST",
		url: "/admin/strategicmanagement/selentities",
		data: { 
			id: $("#elselect").val(),
			entity_id: <?php if ($node["Node"]["id"]) echo $node["Node"]["id"]; else echo 0; ?>,
			redir: '<?php echo $redir;?>'
		},
		success: function(data, textStatus) {
			$("#lalista2").fadeOut(function() {
				$(this).html(data).fadeIn();
			});
		},
		error: function() {
			console.log('Error loading.');
		}
	});
}
</script>

<?php

function laVista($ntid) {
	switch($ntid) {
	case 1:
		return "weakness";
		break;
	case 2:
		return "perspective";
		break;
	case 3:
		return "strategy";
		break;
	case 5:
		return "mission";
		break;
	case 6:
		return "vision";
		break;
	case 7:
		return "objective";
		break;
	case 8:
		return "politics";
		break;
	default:
		return $ntid;
	}
}

if (isset($node["Node"]["id"])) {
?>
	<div class="col-md-12">
	
	
		<ol style="margin-top: 0px;" class="faq-list">
			
			<li id="faq-1"><div class="faq-icon"><div class="faq-number"><i class="icon-chevron-right"></i></div></div>
			
			<div class="faq-text">
				<div class="page-header">
					<h3><?php echo $node["Node"]["name"];?></h3>			 									
				</div>

				<p><?php echo $node["Node"]["description"];?></p>
				
				<div class="col-md-12">
					<p>
						<a 	href="/admin/nodes/edit/<?php echo $node["Node"]["id"];?>/<?php echo $redir;?>" 
							class="btn-edit">
							<i class="icon-edit"></i> 
							<?php echo __("Edit");?>
						</a>
						<a 	onclick="return confirm('<?php echo __("Are you sure do you want to delete this?");?>')" 
							href="/admin/nodes/delete/<?php echo $node["Node"]["id"];?>/<?php echo $redir;?>" 
							class="btn-delete">
							<i class="icon-trash"></i> 
							<?php echo __("Delete");?>
						</a>
					</p>
				</div>

				<?php
				if ($node["Node"]["inidate"] or
					$node["Node"]["enddate"] or
					($node["Node"]["cost"] and $node["Node"]["cost"]>0) or
					$node["Nodesource"]["name"] or
					$node["Nodestatus"]["name"] or
					$node["Node"]["assignedto"] or $node["Node"]["assignedto_email"] or
					$node["Node"]["priority"]) {
				?>
				<div class="widget big-stats-container stacked">
					
					<div class="widget-content" style="padding: 0px">
						
						<div class="cf" id="big_stats">
						
							<?php
							if ($node["Node"]["inidate"]) {
							?>
							<div class="stat">								
								<h4><?php echo __("Start date");?></h4>
								<span class="value"><?php echo date('d/m/Y', strtotime($node["Node"]["inidate"]))?></span>								
							</div>
							<?php
							}

							if ($node["Node"]["enddate"]) {
							?>
							<div class="stat">								
								<h4><?php echo __("End date");?></h4>
								<span class="value"><?php echo date('d/m/Y', strtotime($node["Node"]["enddate"]))?></span>								
							</div>
							<?php
							}

							if ($node["Node"]["cost"] and $node["Node"]["cost"]>0) { 
							?>
							<div class="stat">								
								<h4><?php echo __("Cost");?></h4>
								<span class="value"><?php echo $node["Node"]["cost"]." ".$node["Node"]["currency"]?></span>								
							</div>
							<?php
							}

							if ($node["Nodesource"]["name"]) { 
							?>
							<div class="stat">								
								<h4><?php echo __("Source");?></h4>
								<span class="value"><?php echo $node["Nodesource"]["name"];?></span>								
							</div>
							<?php
							}

							if ($node["Nodestatus"]["name"]) { 
							?>
							<div class="stat">								
								<h4><?php echo __("Status");?></h4>
								<span class="value"><?php echo $node["Nodestatus"]["name"];?></span>								
							</div>
							<?php
							}

							if ($node["Node"]["assignedto"] or $node["Node"]["assignedto_email"]) { 
							?>
							<div class="stat">								
								<h4><?php echo __("Status");?></h4>
								<span class="value"><?php echo $node["Node"]["assignedto"]." (".$node["Node"]["assignedto_email"].")";?></span>								
							</div>
							<?php
							}

							if ($node["Node"]["priority"]) { 
							?>
							<div class="stat">								
								<h4><?php echo __("Priority");?></h4>
								<span class="value"><?php echo $node["Node"]["priority"];?></span>								
							</div>
							<?php
							}

							?>
							
						</div>
					
					</div> <!-- /widget-content -->
					
				</div>
				
				<?php
				}
				?>
				
				<?php
				if (is_array($related) and sizeof($related)) {
				?>
						<div class="page-header">
							<h3><?php echo __("Related Entities");?></h3>
						</div>
						<?php
						$oldi=0;
						foreach($related as $i=>$r) {
							if($oldi<>$r["Node"]["nodetype_id"]) {
								echo "<h4><i class=\"icon-chevron-right\"></i> ".$nodetypes[$r["Node"]["nodetype_id"]]."</h4>";
								$oldi=$r["Node"]["nodetype_id"];
							}
							echo "<p style=\"margin-left: 30px\"><i class=\"icon-asterisk\"></i> ";
							echo "<a href=\"/admin/strategicmanagement/".laVista($r["Node"]["nodetype_id"])."/".$r["Node"]["id"]."\">".$r["Node"]["name"]."</a>";
							?>
							<a class="btn-delete" href="/admin/nodes/deleterelation/<?php echo $r["Node"]["id"];?>/<?php echo $node["Node"]["id"];?>/<?php echo $redir;?>" onclick="return confirm('<?php echo __("Are you sure do you want to delete this?");?>')">
							<i class="icon-trash"></i> <?php echo __("Delete relation");?></a>
							</p>
							<?php
						}
						?>
				<?php
				}
				?>
				
				<div class="page-header">
				</div>
				<div class="widget stacked ">
				  <div class="widget-header">
					<i class="icon-plus"></i>
					<h3><?php echo __("Add"). " " . __("Related Entities");?></h3>
				  </div> <!-- /widget-header -->
				  <div class="widget-content">
				
					<div class="col-md-6">
						<p><select id=elselect class="form-control" onchange="elscript()">
						<option value=0><?php echo __("Choose");?></option>
						<?php
						foreach($nodetypes as $nid=>$n) {
							echo "<option value=".$nid.">".$n."</option>";
						}
						?>
						</select></p>
					</div>
					<div class="col-md-6" id="lalista2">
					</div>
				
				  </div> <!-- /widget-content -->
				</div>
				
			</div>
			</li>
													
		</ol>	
			
	</div>

<?php
} 
?>
