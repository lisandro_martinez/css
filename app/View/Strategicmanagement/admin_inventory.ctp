<script>
function viewOW() {
	$("#lalista").html('<center><img src="/css/images/loader.gif" style="opacity: 0.4;filter: alpha(opacity=40);" /></center>');
	$.ajax({
		type: "POST",
		url: "/admin/strategicmanagement/hello",
		data: "id=" + $("#wosel").val(),
		success: function(data, textStatus) {
			$("#lalista").fadeOut(function() {
				$(this).html(data).fadeIn();
			});
		},
		error: function() {
			console.log('Error loading.');
		}
	});
}

function dale(j, i) {

	$.ajax({
		url: "/admin/strategicmanagement/woupdate/"+j+"/"+i
	}).done(function() {
	
		$("#div_"+j+"_1").hide();
		$("#div_"+j+"_2").hide();
		$("#div_"+j+"_3").hide();
		if (i==1){
			$("#div_"+j+"_1").show("slow");
		} 
		if (i==2){
			$("#div_"+j+"_2").show("slow");
		} 
		if (i==3){
			$("#div_"+j+"_3").show("slow");
		}
		
	});

}
</script>
<link rel="stylesheet" href="/css/faq.css">
<div class="widget stacked">
    <?php  
	$i=1;
	$this->Viewbase->panel_title(__("Weaknesses and Opportunities"));   
	?>
    <div class="widget-content">
	
			<div class="bs-example">
              <div class="tab-content">
	
						<br>
						<div class="col-md-8">
						
							<div class="page-header">
								<h3><i class="icon-comment"></i> <?php echo __("Weaknesses and Opportunities Inventory");?></h3>
							</div>					
						
						</div>
						<div class="col-md-4">
							<div class="widget stacked widget-box">
								
								<div class="widget-header">	
									<i class="icon-info"></i>
									<h3><?php echo __("Helper"); ?></h3>			
								</div> <!-- /widget-header -->
								
								<div class="well">
									
									<p><?php echo __("HELP-INVENTORY-1"); ?></p>
									
								</div> <!-- /widget-content -->
								
							</div> <!-- /widget -->
						</div>
						<div style="clear: both;"></div>
					
					
						<div class="faq-container">
						
							<ol class="faq-list" style="margin-top: 0px;">
								
									<?php
										if (isset($wo) and is_array($wo)) {
											foreach($wo as $woe) {
												?>
												<li id="faq-1"><div class="faq-icon"><div class="faq-number"><?php echo $i++;?></div></div><div class="faq-text">
												<?php
												echo "<h3>".$woe["Node"]["name"]."</h3>";
												
												echo $this->element('status', array('woe'=>$woe));
												
												echo "<p>".$woe["Node"]["description"]."</p>";
												?>
												
												<p>
													<a class="btn-edit" href="/admin/nodes/edit/<?php echo $woe["Node"]["id"];?>/1"><i class="icon-edit"></i> <?php echo __("Edit");?></a> 
													<a class="btn-delete" href="/admin/nodes/delete/<?php echo $woe["Node"]["id"];?>/1" onclick="return confirm('<?php echo __("Are you sure do you want to delete this?");?>')"><i class="icon-trash"></i> <?php echo __("Delete");?></a>
												</p>

												</div></li>
												<?php
											}
										}
									?>
								
							</ol>
					</div>				
				
             </div>
			  
            </div>	

		</div>

	</div>
	
	
			<div class="col-md-6">
				<div class="widget stacked ">
				  <div class="widget-header">
					<i class="icon-plus"></i>
					<h3><?php echo __("Autoevaluation");?></h3>
				  </div> <!-- /widget-header -->
				  <div class="widget-content">
					<p><a class="btn btn-primary btn-support-ask" href="/admin/autoevaluations/add/"><?php echo __("Conduct a Self-Assessment");?></a></p>
				  </div> <!-- /widget-content -->
				</div> <!-- /widget -->
			</div>
			
			<div class="col-md-6">
				<div class="widget stacked ">
				  <div class="widget-header">
					<i class="icon-plus"></i>
					<h3><?php echo __("Add");?></h3>
				  </div> <!-- /widget-header -->
				  <div class="widget-content">
					<p><a class="btn btn-primary btn-support-ask" href="/admin/nodes/add/1"><?php echo __("Add a W/O Manually");?></a></p>
				  </div> <!-- /widget-content -->
				</div> <!-- /widget -->
			</div>
			
		
