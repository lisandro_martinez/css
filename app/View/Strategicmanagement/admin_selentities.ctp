<script>
function addRelation(i) {
	document.location.href="/admin/strategicmanagement/addentityrel/"+i+"/"+$("#elid").val()+"/<?php echo $redir;?>";
}
</script>
<?php
if (is_array($allnodes) and sizeof($allnodes)) {

	echo "<p><select class=\"form-control\" id=\"elid\">";
	echo "<option value=0>".__("Choose")."</option>";
	foreach($allnodes as $n) {
		echo "<option value=".$n["Node"]["id"].">".$n["Node"]["name"]."</option>";
	}
	echo "</select></p>";
	
	?>
	<p style="text-align: right">
		<a href="javascript:void(0);" class="btn btn-primary noload" onclick="addRelation(<?php echo $entity_id;?>);">
			<?php echo __("Add");?>
		</a>
	</p>
	<?php
	
} else {

	echo __("There are no entities of this type");

} 
?>