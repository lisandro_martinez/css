﻿<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">


        <?php
          echo $this->Form->create('Questionnaire', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
        ?>
				<?php
              if($action == "admin_edit"){echo $this->Form->input('Questionnaire.id');}
            ?>
<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-6">
            
						   <label for="name"><?php echo __("Questionnaire title");?> (ESP) *</label>
						   <input type="text" class="form-control" name="data[Questionnaire][title][esp]" id="name" value="<?php echo isset($this->data['Questionnaire']['title']['esp']) ? $this->data['Questionnaire']['title']['esp'] : '';?>">

			</div>
			<?php endif; ?>
			<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
			
			<div class="form-group col-lg-6">
						   <label for="name"><?php echo __("Questionnaire title");?> (ENG) *</label>
						   <input type="text" class="form-control" name="data[Questionnaire][title][eng]" id="name" value="<?php echo isset($this->data['Questionnaire']['title']['eng']) ? $this->data['Questionnaire']['title']['eng'] : '';?>">
			</div>
			<?php endif; ?>
<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-12 textareafield">
						   <label for="name"><?php echo __("Description");?> (ESP)</label>
						   <textarea class="form-control" name="data[Questionnaire][description][esp]" id="name"><?php echo isset($this->data['Questionnaire']['description']['esp']) ? $this->data['Questionnaire']['description']['esp'] : '';?></textarea>
		  </div>
			<?php endif; ?>
			<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-lg-12 textareafield">
						   <label for="name"><?php echo __("Description");?> (ENG)</label>
						   <textarea class="form-control" name="data[Questionnaire][description][eng]" id="name"><?php echo isset($this->data['Questionnaire']['description']['eng']) ? $this->data['Questionnaire']['description']['eng'] : '';?></textarea>
		  </div>
			<?php endif; ?>
<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-12 textareafield">
						   <label for="name"><?php echo __("Acronym");?> (ESP)</label>
						   <textarea class="form-control" name="data[Questionnaire][acronym][esp]" id="name"><?php echo isset($this->data['Questionnaire']['acronym']['esp']) ? $this->data['Questionnaire']['acronym']['esp'] : '';?></textarea>
		  </div>
			<?php endif; ?>
			<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-lg-12 textareafield">
						   <label for="name"><?php echo __("Acronym");?> (ENG)</label>
						   <textarea class="form-control" name="data[Questionnaire][acronym][eng]" id="name"><?php echo isset($this->data['Questionnaire']['acronym']['eng']) ? $this->data['Questionnaire']['acronym']['eng'] : '';?></textarea>
		  </div>
			<?php endif; ?>
		  
		  
		  
		  
          <?php if($action == "admin_edit"){ ?>
          <div class="form-group col-lg-6">
              <div>
			  <?php
			  if (isset($this->data['Questionnaire']['logo']) and $this->data['Questionnaire']['logo']) {
			  ?>
               <img src="<?php echo '/'.$this->data['Questionnaire']['logo'];?>" width="150px" height="auto" />
			  <?php
			  }
			  ?>
               <input type="hidden" name="data[Questionnaire][logo]" class="form-control" value="<?php echo $this->data['Questionnaire']['logo'];?>">
              </div>
              <label for="name">Logo </label>
              <?php
                   echo $this->Form->input('Questionnaire.logo_up_edit',array("class"=>"form-control","label"=>false,"required"=>false,'type' => 'file'));
               ?>
					</div>
          <?php }?>

          <?php if($action == "admin_add"){ ?>

          <div class="form-group col-lg-6">
						   <label for="name">Logo </label>
               <?php
					          echo $this->Form->input('Questionnaire.logo_up',array("class"=>"form-control","label"=>false,"required"=>false,'type' => 'file'));
                ?>
					</div>

          <?php }?>

          <div class="form-group col-lg-1 textareafield">
				<label><?php echo __("Active");?></label>
				<select  class="form-control" name="data[Questionnaire][active]" style="min-width: 70px">
				<option value=1 
					<?php 
						if (isset($this->data['Questionnaire']['active']) and $this->data['Questionnaire']['active']==1) echo 'selected';
					?>
				><?php echo __("Yes");?></option>
				<option value=0 
					<?php 
						if (!isset($this->data['Questionnaire']['active']) or $this->data['Questionnaire']['active']==0) echo 'selected';
					?>
				><?php echo __("No");?></option>
				</select>
		  </div>

		  

          <div class="form-group form_button">

                <div class="form-group">
                  <label for="note"><?php echo __("All fields with * are mandatory");?></label>
                </div>
								<button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

					</div>
          <div class="form-group form_response">
            <div id="responseForm"></div>
          </div>

      </form>


  </div>
</div>



<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Questionnaires.add");
</script>
<?php } ?>
