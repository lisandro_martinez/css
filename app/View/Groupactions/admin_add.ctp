<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">

    <?php
    echo $this->Form->create('Groupaction', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
    ?>

    <div class="form-group col-lg-6">
    <?php
    echo $this->Form->input('group_id',array("class"=>"form-control","label"=>"Grupos *","empty"=>"Seleccione...","type"=>"select","required"=>false));?>
    </div>

    <div class="form-group col-lg-6">
    <?php
    echo $this->Form->input('action_id',array("class"=>"form-control","label"=>"Funciones *","empty"=>"Seleccione...","type"=>"select","required"=>false));
    ?>
    </div>

    <div class="form-group form_button">

          <div class="form-group">
            <label for="note">Nota: Todos los campos con * son obligatorios</label>
          </div>
          <button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

    </div>
    <div class="form-group form_response">
      <div id="responseForm"></div>
    </div>


  </div>
 </div>
 <?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
        Controllers.push("Groupactions.add");
</script>
<?php } ?>
