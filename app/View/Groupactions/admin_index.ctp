<?php
    $headerstitles = array(
                            'Groupaction.id' => '#',
                            'Group.name' => 'Grupos',
                            'Action.name' => 'Funciones');


?>

<div class="widget stacked">
    <?php  $this->Viewbase->panel_title('Listado de Permisos');   ?>

    <div class="widget-content">
    <?php
            include_once('filter.ctp');
            $this->Viewbase->Multi_form_create($this->form,'#');
    ?>
<table class="table table-bordered">
    <?php
                // Encabezado de la tabla
                $this->Viewbase->table_Header($this->Paginator,$headerstitles);
    ?>
    <tbody>
        <?php foreach ($lists as $list): ?>
        <tr>
            <?php
                    // Campo check de cada linea
                    $datarow=array(
                        'idModel' => $list['Groupaction']['id'],
                        'textname' => $list['Group']['name'].'-'.$list['Actions']['name'],
                        'inputname'=> 'data[Groupaction][id][]',
                        'value' => $list['Groupaction']['id']
                    );
                    $this->Viewbase->Multi_check_row($datarow);
                    // Campo check de cada linea
                ?>

            <td style="width: 10px;"><?php echo h($list['Groupaction']['id']); ?>&nbsp;</td>
            <td><?php echo h($list['Group']['name']); ?>&nbsp;</td>
            <td><?php 
			if (isset($categories[$list['Actions']['category_id']])) 
				echo h($categories[$list['Actions']['category_id']]);
			echo ' - ';
			if (isset($list['Actions']['name'])) 
				echo h($list['Actions']['name']); 
			?>&nbsp;</td>

                     <td class="actions">

                        <?php

                          $databutton_delete = array(
                                'url'=> '/admin/groupactions/delete/'.$list['Groupaction']['id'],
                                'idModel' =>$list['Groupaction']['id'],
                                'name' =>$categories[$list['Actions']['category_id']].' - '.$list['Actions']['name'].' - '.$list['Group']['name']
                            );
                            $this->Viewbase->button_delete($this->Html,$databutton_delete);

                        ?>
                    </td>

	   </tr>
        <?php endforeach; ?>
        <tr>
                    <td colspan="5">
                        <?php

                            $databutton_add = array(
                                    'url'=> '/admin/groupactions/add/');
                                $this->Viewbase->button_add($this->Html,$databutton_add);

                        ?>
                    </td>
                </tr>
    </tbody>

</table>

        <div class="checkalldiv col-md-6">
            <script>
                checkalltext = {
                    'empty' : {
                        'title' : 'Advertencia',
                        'text' : 'Debe seleccionar al menos un Permiso para utilizar la opción sobre multiples registros'
                    },
                    'deleteall' :{
                        'title' : 'Confirmación para eliminar multiples registros',
                        'url' : '/admin/groupactions/deletemulti/',
                        'pretext' : 'Estas seguro que deseas eliminar los siguientes registros?'
                    }
                };
            </script>
            <input type="checkbox" class="checkallclick" title="Check All">
            <label for="checkall"><?php echo __('Check All'); ?></label>
            <select id="selectmulti" name="submit_mult" class="autosubmit" style="margin-left:10px;">
                <option value="0" selected="selected"><?php echo __('With selected:'); ?></option>
                <option value="deleteall"><?php echo __('Delete All'); ?></option>
            </select>
        </div>
        <?php echo $this->Form->end(); ?>

<?php echo $this->element('paginado'); ?>
</div>
</div>
<?php if($action == "admin_index"){ ?>
 <script type="text/javascript">
    Controllers.push("Groupactions.index");
</script>
<?php } ?>
