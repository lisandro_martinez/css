<?php if (!$pdf) { ?>

<div class="row-fluid" id= "<?=$data['Indicator']['id']; ?>">
<div class="span12">
<div class="widget stacked">

	<div class="widget-header">

		<i class="icon-list"></i>
	<h3>
	    <?php echo __("Indicator");?>
	</h3>
    
    </div>
	<div class="widget-content" style="font-size: 12px !important;">

<?php } ?>

		<div class="col-md-12" >
		<?php

		echo "<h3>";
		if (!$pdf) {
			echo "<i class=\"".$data["Indicator"]["icon"]."\"></i> ";
		}
		echo $data["Indicator"]["name"]."</h3>";
		echo "<p>".$data["Indicator"]["description"]."</p>";
		
		?>
    </div>
    <div class="col-md-12" style="padding-left: 0px !important;  ">
     <?php
	$color = 1;
	$pct = 0;
	$color = $res[$data['Indicator']['id']]['color'];
	if ($data['Indicator']['goal']<>0) {
				$pct = number_format($res[$data['Indicator']['id']]['achieved']*100/$data['Indicator']['goal'],2);
			}
	echo '<br><p style="text-align: center">';
			echo '<span style="color: black; font-size: 18px; ">' . number_format($res[$data['Indicator']['id']]['achieved'], 2) . ' ' . $res[$data['Indicator']['id']]['metric'] . '</span>';
			echo '<br>';

	echo $this->Viewbase->add_semaforo_grande($color, $pct);
	echo '</p><br>';
    ?>
    </div>

<?php if (!$pdf) { ?>

     <div class="col-md-6"> <?php
		echo "<p><i class=\"icon-asterisk\"></i> ".__("Metric")."</p><h7 style='font-size: smaller; '>".$data["Indicator"]["metric"]."</h7>";
		
		?> </div><div class="col-md-6"> <?php
		echo "<p><i class=\"icon-asterisk\"></i> ".__("Goal")."</p><h7 style='font-size: smaller;'>".$data["Indicator"]["goal"]."</h7>";
		
		?> </div><div class="col-md-6"> <?php
		echo "<p><i class=\"icon-asterisk\"></i> ".__("Periodicity of measure")."</p><h7 style='font-size: smaller;'>".$meas[$data["Indicator"]["periodic_measure"]]."</h7>";
		
		?> </div><div class="col-md-6"> <?php
		echo "<p><i class=\"icon-asterisk\"></i> ".__("Calculation Type")."</p><h7 style='font-size: smaller;'>".$calc[$data["Indicator"]["calculation_type"]]."</h7>";

		?> </div><div class="col-md-6"> <?php
		echo "<p><i class=\"icon-asterisk\"></i> ".__("Responsible")."</p><h7 style='font-size: smaller;'>".$data["Indicator"]["responsible"]."</h7>";
		
		?> </div><div class="col-md-6"> <?php
		echo "<p><i class=\"icon-asterisk\"></i> ".__("Responsible (email)")."</p><h7 style='font-size: smaller;'>".$data["Indicator"]["responsible_email"]."</h7>";
		
		?> </div><div class="col-md-6"> <?php
		echo "<p><i class=\"icon-asterisk\"></i> ".__("Initial Date")."</p><h7 style='font-size: smaller;'>".$data["Indicator"]["inidate"]."</h7>";
		
		?> </div><div class="col-md-6"> <?php
		echo "<p><i class=\"icon-asterisk\"></i> ".__("End Date")."</p><h7 style='font-size: smaller;'>".$data["Indicator"]["enddate"]."</h7>";
		
		?> </div> 


    <div class="col-md-6">
	<div class="btn btn-primary"><?php echo $this->Html->Link('Ver Detalle', 
	'/admin/indicators/detail/' . $data["Indicator"]["id"], 
	array( 'escape' => false, 'class' => 'detalles', 
	'style' => 'color: white; text-decoration: none;')); ?> 
	</div>
    </div>

	</div>

</div>
</div>
<div class="span12">
<div>

<?php } ?>

<?php 
$colores = $res[$data['Indicator']['id']]['data_json'];        

$colors = array();
$colors['color1'] = $colores[0]['namecolor'];
$colors['color2'] = $colores[1]['namecolor'];
$colors['color3'] = $colores[2]['namecolor'];
$colors['from1']  = (strlen($colores[0]['from']) > 0)?$colores[0]['from']:0;
$colors['to1']    = (strlen($colores[0]['to']) > 0)?$colores[0]['to']:0;
$colors['from2']  = (strlen($colores[1]['from']) > 0)?$colores[1]['from']:0;
$colors['to2']    = (strlen($colores[1]['to']) > 0)?$colores[1]['to']:0;
$colors['from3']  = (strlen($colores[2]['from']) > 0)?$colores[2]['from']:0;
$colors['to3']    = (strlen($colores[2]['to']) > 0)?$colores[2]['to']:0;     

$inferior = $data["Indicator"]["cond_value_1"];
$superior = $data["Indicator"]["cond_value_2"];

// verde arriba
if ($data["Indicator"]["cut_order"]) {
	 $color1='ff0000';
	 $color2='ffff00';
	 $color3='00ff00';
 // rojo arriba
 } else {
	 $color1='00ff00';
	 $color2='ffff00';
	 $color3='ff0000';
 }

// odotutex11.php?title=Otro%20indicador&color1=ff0000&color2=ffff00&color3=00ff00&from1=0&to1=20&from2=20&to2=40&from3=40&to3=100&achieved=1244.6666666667&goal=2000.00

   echo '<img src="'.
			$this->base.'/odotutex11.php?'.
			'title='	. $data['Indicator']['name'] .
			'&color1='	. $color1 .
			'&color2='	. $color2 .
			'&color3='	. $color3 .
			'&from1='	. 0 .
			'&to1='		. $inferior .
			'&from2='	. $inferior .
			'&to2='		. $superior .
			'&from3='	. $superior .
			'&to3='		. 100 .
			'&achieved='. $res[$data['Indicator']['id']]['achieved'] .
			'&goal='	. $data['Indicator']['goal'] .
			'" />';

		?>

<?php if (!$pdf) { ?>
        </div>
</div>
<?php } ?>
