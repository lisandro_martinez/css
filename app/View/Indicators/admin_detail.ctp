<?php if (!$pdf) { ?>

<div class="col-md-10">

<?php } ?>

<?php

$metrica_label=null;
$metrica_graph=null;

foreach($metrica as $index => $value) {
	$numero = $index + 1;
	if ($metrica_label) $metrica_label.=",";
	$medida = $value["medida"];
	$metrica_label .= convierte($periodic_measure, $value["medida"]); // $pma[$periodic_measure] . " " . $numero;
}

foreach ($metrica as $m) {
	if ($metrica_graph) $metrica_graph.=",";
	$metrica_graph .= str_replace(',', '', $m['metrica']);
}

function convierte($pm, $val) {
	if ($pm==7) { // anual
		return substr($val, 0, 4);
	} elseif($pm==0 or $pm==1) { // diario, semanal
		return $val;
	} else { // otro
		return substr($val, 0, 7);
	}
}

?>

<?php if (!$pdf) { ?>

<div class="widget stacked">
    
		<div class="widget-header">
					<i class="icon-list"></i>
	        <h3>
	            <?php echo __("Indicator");?>
	        </h3>
	    </div>
		<div class="widget-content">

<?php } ?>

            <div class="row-fluid">
            <h4><?php echo $name; ?></h4>
            </div>
            
            <?php
			if ($data["Indicator"]["graph_id"]) {
			?>
				<img src="<?=Router::url('/', true)?>pchart/Example<?=$data["Indicator"]["graph_id"]?>.php?lang=<?=$lang?>&isget=1&s=<?=$metrica_label?>&d=<?=$metrica_graph?>&t=<?=$data["Indicator"]["name"]?>" style="max-width: 100%;">
			<?php
			}
			?>
            
            <div class="row-fluid">
            <table cellpadding=10 cellspacing=0>
                <thead>
                    <tr bgcolor=#ccc>    
                        <th style="font-weight: bold;"><?php /*echo __($pma[$periodic_measure]);*/ ?></th>
                        <?php foreach($metrica as $index => $value) : ?>
                        <?php $numero = $index + 1; ?>
                        <th style="font-weight: bold;"><?php echo convierte($periodic_measure, $value["medida"]) /*$pma[$periodic_measure] . " " . $numero*/; ?></th>
                        <?php endforeach; ?>
                    </tr>
                </thead>
                <tbody>
                    <tr bgcolor=#eee>
                        <td style="font-weight: bold;">Medida</td>
                        <?php foreach ($metrica as $m) : ?>
                        <td><?php echo $m['metrica']; ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr bgcolor=#ddd>
                        <td style="font-weight: bold;">Progreso</td>
                        <?php foreach ($metrica as $m) : ?>
                        <td><?php echo $m['progreso']; ?></td>
                        <?php endforeach; ?>
                    </tr>
                </tbody>
            </table>
            </div>

<?php if (!$pdf) { ?>

		</div>

    </div>
</div>

<?php } ?>
