<?php
function checkthis($a, $b) {
	if ($a==$b) return " checked ";
}

function pintaIcon($icon, $class) {
	if ($icon<>$class) {
		echo '<i class="'.$class.'"></i> ';
	}
}
?>

<script>

function cambiaSemaforo() {

	// verde arriba
	if ($('input[name="data[Indicator][cut_order]"]:checked', '#IndicatorAdminEditForm').val()==1) {
		
		$('#banda_superior').css("background-color", "green");
		$('#banda_inferior').css("background-color", "red");
		
	// rojo arriba
	} else {
		
		$('#banda_superior').css("background-color", "red");
		$('#banda_inferior').css("background-color", "green");		
		
	}

	if ($("#IndicatorCutValue2").is(':checked')) {
		var comp = ">= ";
	} else {
		var comp = "> ";
	}
	var text = '<p style="text-align: center;color: white; padding-top: 40px;">' + comp + $("#IndicatorCondValue2").val() + '</p>';
	$('#banda_superior').html(text);

	if ($("#IndicatorCutValue1").is(':checked')) {
		var comp = ">= ";
	} else {
		var comp = "> ";
	}
	var text = '<p style="text-align: center;color: gray; padding-top: 40px;">' + comp + $("#IndicatorCondValue1").val() + '</p>';
	$('#banda_media').html(text);

}

function onc() {
	var elval=$( "#entitype" ).val();
	if (elval>=10 && elval<=15) {
		$( "#divcost" ).show();
	} else {
		$( "#divcost" ).hide();
	}
}
function selFY(y) {
	document.getElementById("datepicker").value = '01-10-'+(y-1);
	document.getElementById("datepicker-2").value = "30-09-"+y;
}
</script>
<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">

        <?php
          echo $this->Form->create('Indicator', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
        ?>

		<div class="form-group col-lg-6">
			<label for="name"><?php echo __("Goal");?></label>
			<input type="text" class="form-control" name="data[Indicator][goal]" id="name" value="<?php echo isset($this->data['Indicator']['goal']) ? $this->data['Indicator']['goal'] : '';?>">
		</div>
		<div class="form-group col-lg-6">
			<label for="name"><?php echo __("Calculation Type");?></label>
			<?php
			
				echo $this->Form->input('calculation_type',
										array(
											"div"=>false,
											"class"=>"form-control",
											"label"=>false,
											"empty"=>__("Choose"),
											"type"=>"select",
											"required"=>false,
											'options'=>Indicator::calculationTypeArray(),
											'value'=>$this->data['Indicator']['calculation_type'], 
											'id'=>'entitype'));
			?>
		</div>
		<div class="form-group col-lg-6">
			<label for="name"><?php echo __("Periodicity of measure");?></label>
			<?php
			
				echo $this->Form->input('periodic_measure',
										array(
											"div"=>false,
											"class"=>"form-control",
											"label"=>false,
											"empty"=>__("Choose"),
											"type"=>"select",
											"required"=>false,
											'options'=>Indicator::periodicMeasureArray(),
											'value'=>$this->data['Indicator']['periodic_measure'], 
											'id'=>'entitype'));
			?>
		</div>
		
		
		
		<div class="form-group col-lg-6">
			<label for="name"><?php echo __("Indicator Type");?></label>
			<?php
			
				echo $this->Form->input('indicator_type',
										array(
											"div"=>false,
											"class"=>"form-control",
											"label"=>false,
											"empty"=>__("Choose"),
											"type"=>"select",
											"required"=>false,
											'options'=>Indicator::indicatorTypeArray(),
											'value'=>$this->data['Indicator']['indicator_type'], 
											'id'=>'entitype'));
			?>
		</div>

		

		<div class="form-group col-lg-6">

			<label for="name"><?php echo __("Associated Graph Type");?></label>
			
			<div style="height: 300px; overflow-y: scroll;">

				<?php
				foreach ($graph_list as $igl=>$gl) {
				?>

					<div class="col-md-6">
						<label>
						<img class="pgraph_ico" id="01_<?=$igl?>" src="/pchart/img/<?=$igl?>.png"> 
						<input type="radio" name="data[Indicator][graph_id]" value="<?=$igl?>" 
							<?php echo checkthis($this->data['Indicator']['graph_id'], $igl);?>> <?php echo $gl;?></label>
					</div>

				<?php
				}
				?>

		</div>
		</div>
		
		<div class="form-group col-lg-6">
			<label for="name"><?php echo __("Icon");?></label>
			
			<input type="hidden" value="<?php echo $this->data['Indicator']['icon'];?>" id="elicon" name="data[Indicator][icon]" class="form-control">
			
<style>
.los-iconos > i {
	padding: 10px;
	font-size:x-large;
}
.pgraph_ico {
	width: 75px;
}
.fa:hover {
    background-color: yellow;
} 
.resaltado {
    background-color: yellow;
    border-color: #f00;
    border-style: solid;
    border-width: thin;
}
</style>

<div class="los-iconos" style="height: 300px; overflow-y: scroll;">
<?php
if ($this->data['Indicator']['icon']) {
	echo '<i class="'.$this->data['Indicator']['icon'].' resaltado"></i> ';
}
pintaIcon($this->data['Indicator']['icon'], "fa fa-glass");
pintaIcon($this->data['Indicator']['icon'], "fa fa-music");
pintaIcon($this->data['Indicator']['icon'], "fa fa-search");
pintaIcon($this->data['Indicator']['icon'], "fa fa-envelope-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-heart");
pintaIcon($this->data['Indicator']['icon'], "fa fa-star");
pintaIcon($this->data['Indicator']['icon'], "fa fa-star-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-user");
pintaIcon($this->data['Indicator']['icon'], "fa fa-film");
pintaIcon($this->data['Indicator']['icon'], "fa fa-th-large");
pintaIcon($this->data['Indicator']['icon'], "fa fa-th");
pintaIcon($this->data['Indicator']['icon'], "fa fa-th-list");
pintaIcon($this->data['Indicator']['icon'], "fa fa-check");
pintaIcon($this->data['Indicator']['icon'], "fa fa-times");
pintaIcon($this->data['Indicator']['icon'], "fa fa-search-plus");
pintaIcon($this->data['Indicator']['icon'], "fa fa-search-minus");
pintaIcon($this->data['Indicator']['icon'], "fa fa-power-off");
pintaIcon($this->data['Indicator']['icon'], "fa fa-signal");
pintaIcon($this->data['Indicator']['icon'], "fa fa-cog");
pintaIcon($this->data['Indicator']['icon'], "fa fa-trash-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-home");
pintaIcon($this->data['Indicator']['icon'], "fa fa-file-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-clock-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-road");
pintaIcon($this->data['Indicator']['icon'], "fa fa-download");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrow-circle-o-down");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrow-circle-o-up");
pintaIcon($this->data['Indicator']['icon'], "fa fa-inbox");
pintaIcon($this->data['Indicator']['icon'], "fa fa-play-circle-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-repeat");
pintaIcon($this->data['Indicator']['icon'], "fa fa-refresh");
pintaIcon($this->data['Indicator']['icon'], "fa fa-list-alt");
pintaIcon($this->data['Indicator']['icon'], "fa fa-lock");
pintaIcon($this->data['Indicator']['icon'], "fa fa-flag");
pintaIcon($this->data['Indicator']['icon'], "fa fa-headphones");
pintaIcon($this->data['Indicator']['icon'], "fa fa-volume-off");
pintaIcon($this->data['Indicator']['icon'], "fa fa-volume-down");
pintaIcon($this->data['Indicator']['icon'], "fa fa-volume-up");
pintaIcon($this->data['Indicator']['icon'], "fa fa-qrcode");
pintaIcon($this->data['Indicator']['icon'], "fa fa-barcode");
pintaIcon($this->data['Indicator']['icon'], "fa fa-tag");
pintaIcon($this->data['Indicator']['icon'], "fa fa-tags");
pintaIcon($this->data['Indicator']['icon'], "fa fa-book");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bookmark");
pintaIcon($this->data['Indicator']['icon'], "fa fa-print");
pintaIcon($this->data['Indicator']['icon'], "fa fa-camera");
pintaIcon($this->data['Indicator']['icon'], "fa fa-font");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bold");
pintaIcon($this->data['Indicator']['icon'], "fa fa-italic");
pintaIcon($this->data['Indicator']['icon'], "fa fa-text-height");
pintaIcon($this->data['Indicator']['icon'], "fa fa-text-width");
pintaIcon($this->data['Indicator']['icon'], "fa fa-align-left");
pintaIcon($this->data['Indicator']['icon'], "fa fa-align-center");
pintaIcon($this->data['Indicator']['icon'], "fa fa-align-right");
pintaIcon($this->data['Indicator']['icon'], "fa fa-align-justify");
pintaIcon($this->data['Indicator']['icon'], "fa fa-list");
pintaIcon($this->data['Indicator']['icon'], "fa fa-outdent");
pintaIcon($this->data['Indicator']['icon'], "fa fa-indent");
pintaIcon($this->data['Indicator']['icon'], "fa fa-video-camera");
pintaIcon($this->data['Indicator']['icon'], "fa fa-picture-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-pencil");
pintaIcon($this->data['Indicator']['icon'], "fa fa-map-marker");
pintaIcon($this->data['Indicator']['icon'], "fa fa-adjust");
pintaIcon($this->data['Indicator']['icon'], "fa fa-tint");
pintaIcon($this->data['Indicator']['icon'], "fa fa-pencil-square-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-share-square-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-check-square-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrows");
pintaIcon($this->data['Indicator']['icon'], "fa fa-step-backward");
pintaIcon($this->data['Indicator']['icon'], "fa fa-fast-backward");
pintaIcon($this->data['Indicator']['icon'], "fa fa-backward");
pintaIcon($this->data['Indicator']['icon'], "fa fa-play");
pintaIcon($this->data['Indicator']['icon'], "fa fa-pause");
pintaIcon($this->data['Indicator']['icon'], "fa fa-stop");
pintaIcon($this->data['Indicator']['icon'], "fa fa-forward");
pintaIcon($this->data['Indicator']['icon'], "fa fa-fast-forward");
pintaIcon($this->data['Indicator']['icon'], "fa fa-step-forward");
pintaIcon($this->data['Indicator']['icon'], "fa fa-eject");
pintaIcon($this->data['Indicator']['icon'], "fa fa-chevron-left");
pintaIcon($this->data['Indicator']['icon'], "fa fa-chevron-right");
pintaIcon($this->data['Indicator']['icon'], "fa fa-plus-circle");
pintaIcon($this->data['Indicator']['icon'], "fa fa-minus-circle");
pintaIcon($this->data['Indicator']['icon'], "fa fa-times-circle");
pintaIcon($this->data['Indicator']['icon'], "fa fa-check-circle");
pintaIcon($this->data['Indicator']['icon'], "fa fa-question-circle");
pintaIcon($this->data['Indicator']['icon'], "fa fa-info-circle");
pintaIcon($this->data['Indicator']['icon'], "fa fa-crosshairs");
pintaIcon($this->data['Indicator']['icon'], "fa fa-times-circle-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-check-circle-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-ban");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrow-left");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrow-right");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrow-up");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrow-down");
pintaIcon($this->data['Indicator']['icon'], "fa fa-share");
pintaIcon($this->data['Indicator']['icon'], "fa fa-expand");
pintaIcon($this->data['Indicator']['icon'], "fa fa-compress");
pintaIcon($this->data['Indicator']['icon'], "fa fa-plus");
pintaIcon($this->data['Indicator']['icon'], "fa fa-minus");
pintaIcon($this->data['Indicator']['icon'], "fa fa-asterisk");
pintaIcon($this->data['Indicator']['icon'], "fa fa-exclamation-circle");
pintaIcon($this->data['Indicator']['icon'], "fa fa-gift");
pintaIcon($this->data['Indicator']['icon'], "fa fa-leaf");
pintaIcon($this->data['Indicator']['icon'], "fa fa-fire");
pintaIcon($this->data['Indicator']['icon'], "fa fa-eye");
pintaIcon($this->data['Indicator']['icon'], "fa fa-eye-slash");
pintaIcon($this->data['Indicator']['icon'], "fa fa-exclamation-triangle");
pintaIcon($this->data['Indicator']['icon'], "fa fa-plane");
pintaIcon($this->data['Indicator']['icon'], "fa fa-calendar");
pintaIcon($this->data['Indicator']['icon'], "fa fa-random");
pintaIcon($this->data['Indicator']['icon'], "fa fa-comment");
pintaIcon($this->data['Indicator']['icon'], "fa fa-magnet");
pintaIcon($this->data['Indicator']['icon'], "fa fa-chevron-up");
pintaIcon($this->data['Indicator']['icon'], "fa fa-chevron-down");
pintaIcon($this->data['Indicator']['icon'], "fa fa-retweet");
pintaIcon($this->data['Indicator']['icon'], "fa fa-shopping-cart");
pintaIcon($this->data['Indicator']['icon'], "fa fa-folder");
pintaIcon($this->data['Indicator']['icon'], "fa fa-folder-open");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrows-v");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrows-h");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bar-chart");
pintaIcon($this->data['Indicator']['icon'], "fa fa-twitter-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-facebook-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-camera-retro");
pintaIcon($this->data['Indicator']['icon'], "fa fa-key");
pintaIcon($this->data['Indicator']['icon'], "fa fa-cogs");
pintaIcon($this->data['Indicator']['icon'], "fa fa-comments");
pintaIcon($this->data['Indicator']['icon'], "fa fa-thumbs-o-up");
pintaIcon($this->data['Indicator']['icon'], "fa fa-thumbs-o-down");
pintaIcon($this->data['Indicator']['icon'], "fa fa-star-half");
pintaIcon($this->data['Indicator']['icon'], "fa fa-heart-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-sign-out");
pintaIcon($this->data['Indicator']['icon'], "fa fa-linkedin-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-thumb-tack");
pintaIcon($this->data['Indicator']['icon'], "fa fa-external-link");
pintaIcon($this->data['Indicator']['icon'], "fa fa-sign-in");
pintaIcon($this->data['Indicator']['icon'], "fa fa-trophy");
pintaIcon($this->data['Indicator']['icon'], "fa fa-github-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-upload");
pintaIcon($this->data['Indicator']['icon'], "fa fa-lemon-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-phone");
pintaIcon($this->data['Indicator']['icon'], "fa fa-square-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bookmark-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-phone-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-twitter");
pintaIcon($this->data['Indicator']['icon'], "fa fa-facebook");
pintaIcon($this->data['Indicator']['icon'], "fa fa-github");
pintaIcon($this->data['Indicator']['icon'], "fa fa-unlock");
pintaIcon($this->data['Indicator']['icon'], "fa fa-credit-card");
pintaIcon($this->data['Indicator']['icon'], "fa fa-rss");
pintaIcon($this->data['Indicator']['icon'], "fa fa-hdd-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bullhorn");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bell");
pintaIcon($this->data['Indicator']['icon'], "fa fa-certificate");
pintaIcon($this->data['Indicator']['icon'], "fa fa-hand-o-right");
pintaIcon($this->data['Indicator']['icon'], "fa fa-hand-o-left");
pintaIcon($this->data['Indicator']['icon'], "fa fa-hand-o-up");
pintaIcon($this->data['Indicator']['icon'], "fa fa-hand-o-down");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrow-circle-left");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrow-circle-right");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrow-circle-up");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrow-circle-down");
pintaIcon($this->data['Indicator']['icon'], "fa fa-globe");
pintaIcon($this->data['Indicator']['icon'], "fa fa-wrench");
pintaIcon($this->data['Indicator']['icon'], "fa fa-tasks");
pintaIcon($this->data['Indicator']['icon'], "fa fa-filter");
pintaIcon($this->data['Indicator']['icon'], "fa fa-briefcase");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrows-alt");
pintaIcon($this->data['Indicator']['icon'], "fa fa-users");
pintaIcon($this->data['Indicator']['icon'], "fa fa-link");
pintaIcon($this->data['Indicator']['icon'], "fa fa-cloud");
pintaIcon($this->data['Indicator']['icon'], "fa fa-flask");
pintaIcon($this->data['Indicator']['icon'], "fa fa-scissors");
pintaIcon($this->data['Indicator']['icon'], "fa fa-files-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-paperclip");
pintaIcon($this->data['Indicator']['icon'], "fa fa-floppy-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bars");
pintaIcon($this->data['Indicator']['icon'], "fa fa-list-ul");
pintaIcon($this->data['Indicator']['icon'], "fa fa-list-ol");
pintaIcon($this->data['Indicator']['icon'], "fa fa-strikethrough");
pintaIcon($this->data['Indicator']['icon'], "fa fa-underline");
pintaIcon($this->data['Indicator']['icon'], "fa fa-table");
pintaIcon($this->data['Indicator']['icon'], "fa fa-magic");
pintaIcon($this->data['Indicator']['icon'], "fa fa-truck");
pintaIcon($this->data['Indicator']['icon'], "fa fa-pinterest");
pintaIcon($this->data['Indicator']['icon'], "fa fa-pinterest-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-google-plus-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-google-plus");
pintaIcon($this->data['Indicator']['icon'], "fa fa-money");
pintaIcon($this->data['Indicator']['icon'], "fa fa-caret-down");
pintaIcon($this->data['Indicator']['icon'], "fa fa-caret-up");
pintaIcon($this->data['Indicator']['icon'], "fa fa-caret-left");
pintaIcon($this->data['Indicator']['icon'], "fa fa-caret-right");
pintaIcon($this->data['Indicator']['icon'], "fa fa-columns");
pintaIcon($this->data['Indicator']['icon'], "fa fa-sort");
pintaIcon($this->data['Indicator']['icon'], "fa fa-sort-desc");
pintaIcon($this->data['Indicator']['icon'], "fa fa-sort-asc");
pintaIcon($this->data['Indicator']['icon'], "fa fa-envelope");
pintaIcon($this->data['Indicator']['icon'], "fa fa-linkedin");
pintaIcon($this->data['Indicator']['icon'], "fa fa-undo");
pintaIcon($this->data['Indicator']['icon'], "fa fa-gavel");
pintaIcon($this->data['Indicator']['icon'], "fa fa-tachometer");
pintaIcon($this->data['Indicator']['icon'], "fa fa-comment-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-comments-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bolt");
pintaIcon($this->data['Indicator']['icon'], "fa fa-sitemap");
pintaIcon($this->data['Indicator']['icon'], "fa fa-umbrella");
pintaIcon($this->data['Indicator']['icon'], "fa fa-clipboard");
pintaIcon($this->data['Indicator']['icon'], "fa fa-lightbulb-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-exchange");
pintaIcon($this->data['Indicator']['icon'], "fa fa-cloud-download");
pintaIcon($this->data['Indicator']['icon'], "fa fa-cloud-upload");
pintaIcon($this->data['Indicator']['icon'], "fa fa-user-md");
pintaIcon($this->data['Indicator']['icon'], "fa fa-stethoscope");
pintaIcon($this->data['Indicator']['icon'], "fa fa-suitcase");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bell-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-coffee");
pintaIcon($this->data['Indicator']['icon'], "fa fa-cutlery");
pintaIcon($this->data['Indicator']['icon'], "fa fa-file-text-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-building-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-hospital-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-ambulance");
pintaIcon($this->data['Indicator']['icon'], "fa fa-medkit");
pintaIcon($this->data['Indicator']['icon'], "fa fa-fighter-jet");
pintaIcon($this->data['Indicator']['icon'], "fa fa-beer");
pintaIcon($this->data['Indicator']['icon'], "fa fa-h-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-plus-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-angle-double-left");
pintaIcon($this->data['Indicator']['icon'], "fa fa-angle-double-right");
pintaIcon($this->data['Indicator']['icon'], "fa fa-angle-double-up");
pintaIcon($this->data['Indicator']['icon'], "fa fa-angle-double-down");
pintaIcon($this->data['Indicator']['icon'], "fa fa-angle-left");
pintaIcon($this->data['Indicator']['icon'], "fa fa-angle-right");
pintaIcon($this->data['Indicator']['icon'], "fa fa-angle-up");
pintaIcon($this->data['Indicator']['icon'], "fa fa-angle-down");
pintaIcon($this->data['Indicator']['icon'], "fa fa-desktop");
pintaIcon($this->data['Indicator']['icon'], "fa fa-laptop");
pintaIcon($this->data['Indicator']['icon'], "fa fa-tablet");
pintaIcon($this->data['Indicator']['icon'], "fa fa-mobile");
pintaIcon($this->data['Indicator']['icon'], "fa fa-circle-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-quote-left");
pintaIcon($this->data['Indicator']['icon'], "fa fa-quote-right");
pintaIcon($this->data['Indicator']['icon'], "fa fa-spinner");
pintaIcon($this->data['Indicator']['icon'], "fa fa-circle");
pintaIcon($this->data['Indicator']['icon'], "fa fa-reply");
pintaIcon($this->data['Indicator']['icon'], "fa fa-github-alt");
pintaIcon($this->data['Indicator']['icon'], "fa fa-folder-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-folder-open-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-smile-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-frown-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-meh-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-gamepad");
pintaIcon($this->data['Indicator']['icon'], "fa fa-keyboard-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-flag-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-flag-checkered");
pintaIcon($this->data['Indicator']['icon'], "fa fa-terminal");
pintaIcon($this->data['Indicator']['icon'], "fa fa-code");
pintaIcon($this->data['Indicator']['icon'], "fa fa-reply-all");
pintaIcon($this->data['Indicator']['icon'], "fa fa-star-half-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-location-arrow");
pintaIcon($this->data['Indicator']['icon'], "fa fa-crop");
pintaIcon($this->data['Indicator']['icon'], "fa fa-code-fork");
pintaIcon($this->data['Indicator']['icon'], "fa fa-chain-broken");
pintaIcon($this->data['Indicator']['icon'], "fa fa-question");
pintaIcon($this->data['Indicator']['icon'], "fa fa-info");
pintaIcon($this->data['Indicator']['icon'], "fa fa-exclamation");
pintaIcon($this->data['Indicator']['icon'], "fa fa-superscript");
pintaIcon($this->data['Indicator']['icon'], "fa fa-subscript");
pintaIcon($this->data['Indicator']['icon'], "fa fa-eraser");
pintaIcon($this->data['Indicator']['icon'], "fa fa-puzzle-piece");
pintaIcon($this->data['Indicator']['icon'], "fa fa-microphone");
pintaIcon($this->data['Indicator']['icon'], "fa fa-microphone-slash");
pintaIcon($this->data['Indicator']['icon'], "fa fa-shield");
pintaIcon($this->data['Indicator']['icon'], "fa fa-calendar-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-fire-extinguisher");
pintaIcon($this->data['Indicator']['icon'], "fa fa-rocket");
pintaIcon($this->data['Indicator']['icon'], "fa fa-maxcdn");
pintaIcon($this->data['Indicator']['icon'], "fa fa-chevron-circle-left");
pintaIcon($this->data['Indicator']['icon'], "fa fa-chevron-circle-right");
pintaIcon($this->data['Indicator']['icon'], "fa fa-chevron-circle-up");
pintaIcon($this->data['Indicator']['icon'], "fa fa-chevron-circle-down");
pintaIcon($this->data['Indicator']['icon'], "fa fa-html5");
pintaIcon($this->data['Indicator']['icon'], "fa fa-css3");
pintaIcon($this->data['Indicator']['icon'], "fa fa-anchor");
pintaIcon($this->data['Indicator']['icon'], "fa fa-unlock-alt");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bullseye");
pintaIcon($this->data['Indicator']['icon'], "fa fa-ellipsis-h");
pintaIcon($this->data['Indicator']['icon'], "fa fa-ellipsis-v");
pintaIcon($this->data['Indicator']['icon'], "fa fa-rss-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-play-circle");
pintaIcon($this->data['Indicator']['icon'], "fa fa-ticket");
pintaIcon($this->data['Indicator']['icon'], "fa fa-minus-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-minus-square-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-level-up");
pintaIcon($this->data['Indicator']['icon'], "fa fa-level-down");
pintaIcon($this->data['Indicator']['icon'], "fa fa-check-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-pencil-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-external-link-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-share-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-compass");
pintaIcon($this->data['Indicator']['icon'], "fa fa-caret-square-o-down");
pintaIcon($this->data['Indicator']['icon'], "fa fa-caret-square-o-up");
pintaIcon($this->data['Indicator']['icon'], "fa fa-caret-square-o-right");
pintaIcon($this->data['Indicator']['icon'], "fa fa-eur");
pintaIcon($this->data['Indicator']['icon'], "fa fa-gbp");
pintaIcon($this->data['Indicator']['icon'], "fa fa-usd");
pintaIcon($this->data['Indicator']['icon'], "fa fa-inr");
pintaIcon($this->data['Indicator']['icon'], "fa fa-jpy");
pintaIcon($this->data['Indicator']['icon'], "fa fa-rub");
pintaIcon($this->data['Indicator']['icon'], "fa fa-krw");
pintaIcon($this->data['Indicator']['icon'], "fa fa-btc");
pintaIcon($this->data['Indicator']['icon'], "fa fa-file");
pintaIcon($this->data['Indicator']['icon'], "fa fa-file-text");
pintaIcon($this->data['Indicator']['icon'], "fa fa-sort-alpha-asc");
pintaIcon($this->data['Indicator']['icon'], "fa fa-sort-alpha-desc");
pintaIcon($this->data['Indicator']['icon'], "fa fa-sort-amount-asc");
pintaIcon($this->data['Indicator']['icon'], "fa fa-sort-amount-desc");
pintaIcon($this->data['Indicator']['icon'], "fa fa-sort-numeric-asc");
pintaIcon($this->data['Indicator']['icon'], "fa fa-sort-numeric-desc");
pintaIcon($this->data['Indicator']['icon'], "fa fa-thumbs-up");
pintaIcon($this->data['Indicator']['icon'], "fa fa-thumbs-down");
pintaIcon($this->data['Indicator']['icon'], "fa fa-youtube-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-youtube");
pintaIcon($this->data['Indicator']['icon'], "fa fa-xing");
pintaIcon($this->data['Indicator']['icon'], "fa fa-xing-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-youtube-play");
pintaIcon($this->data['Indicator']['icon'], "fa fa-dropbox");
pintaIcon($this->data['Indicator']['icon'], "fa fa-stack-overflow");
pintaIcon($this->data['Indicator']['icon'], "fa fa-instagram");
pintaIcon($this->data['Indicator']['icon'], "fa fa-flickr");
pintaIcon($this->data['Indicator']['icon'], "fa fa-adn");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bitbucket");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bitbucket-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-tumblr");
pintaIcon($this->data['Indicator']['icon'], "fa fa-tumblr-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-long-arrow-down");
pintaIcon($this->data['Indicator']['icon'], "fa fa-long-arrow-up");
pintaIcon($this->data['Indicator']['icon'], "fa fa-long-arrow-left");
pintaIcon($this->data['Indicator']['icon'], "fa fa-long-arrow-right");
pintaIcon($this->data['Indicator']['icon'], "fa fa-apple");
pintaIcon($this->data['Indicator']['icon'], "fa fa-windows");
pintaIcon($this->data['Indicator']['icon'], "fa fa-android");
pintaIcon($this->data['Indicator']['icon'], "fa fa-linux");
pintaIcon($this->data['Indicator']['icon'], "fa fa-dribbble");
pintaIcon($this->data['Indicator']['icon'], "fa fa-skype");
pintaIcon($this->data['Indicator']['icon'], "fa fa-foursquare");
pintaIcon($this->data['Indicator']['icon'], "fa fa-trello");
pintaIcon($this->data['Indicator']['icon'], "fa fa-female");
pintaIcon($this->data['Indicator']['icon'], "fa fa-male");
pintaIcon($this->data['Indicator']['icon'], "fa fa-gittip");
pintaIcon($this->data['Indicator']['icon'], "fa fa-sun-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-moon-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-archive");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bug");
pintaIcon($this->data['Indicator']['icon'], "fa fa-vk");
pintaIcon($this->data['Indicator']['icon'], "fa fa-weibo");
pintaIcon($this->data['Indicator']['icon'], "fa fa-renren");
pintaIcon($this->data['Indicator']['icon'], "fa fa-pagelines");
pintaIcon($this->data['Indicator']['icon'], "fa fa-stack-exchange");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrow-circle-o-right");
pintaIcon($this->data['Indicator']['icon'], "fa fa-arrow-circle-o-left");
pintaIcon($this->data['Indicator']['icon'], "fa fa-caret-square-o-left");
pintaIcon($this->data['Indicator']['icon'], "fa fa-dot-circle-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-wheelchair");
pintaIcon($this->data['Indicator']['icon'], "fa fa-vimeo-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-try");
pintaIcon($this->data['Indicator']['icon'], "fa fa-plus-square-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-space-shuttle");
pintaIcon($this->data['Indicator']['icon'], "fa fa-slack");
pintaIcon($this->data['Indicator']['icon'], "fa fa-envelope-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-wordpress");
pintaIcon($this->data['Indicator']['icon'], "fa fa-openid");
pintaIcon($this->data['Indicator']['icon'], "fa fa-university");
pintaIcon($this->data['Indicator']['icon'], "fa fa-graduation-cap");
pintaIcon($this->data['Indicator']['icon'], "fa fa-yahoo");
pintaIcon($this->data['Indicator']['icon'], "fa fa-google");
pintaIcon($this->data['Indicator']['icon'], "fa fa-reddit");
pintaIcon($this->data['Indicator']['icon'], "fa fa-reddit-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-stumbleupon-circle");
pintaIcon($this->data['Indicator']['icon'], "fa fa-stumbleupon");
pintaIcon($this->data['Indicator']['icon'], "fa fa-delicious");
pintaIcon($this->data['Indicator']['icon'], "fa fa-digg");
pintaIcon($this->data['Indicator']['icon'], "fa fa-pied-piper");
pintaIcon($this->data['Indicator']['icon'], "fa fa-pied-piper-alt");
pintaIcon($this->data['Indicator']['icon'], "fa fa-drupal");
pintaIcon($this->data['Indicator']['icon'], "fa fa-joomla");
pintaIcon($this->data['Indicator']['icon'], "fa fa-language");
pintaIcon($this->data['Indicator']['icon'], "fa fa-fax");
pintaIcon($this->data['Indicator']['icon'], "fa fa-building");
pintaIcon($this->data['Indicator']['icon'], "fa fa-child");
pintaIcon($this->data['Indicator']['icon'], "fa fa-paw");
pintaIcon($this->data['Indicator']['icon'], "fa fa-spoon");
pintaIcon($this->data['Indicator']['icon'], "fa fa-cube");
pintaIcon($this->data['Indicator']['icon'], "fa fa-cubes");
pintaIcon($this->data['Indicator']['icon'], "fa fa-behance");
pintaIcon($this->data['Indicator']['icon'], "fa fa-behance-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-steam");
pintaIcon($this->data['Indicator']['icon'], "fa fa-steam-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-recycle");
pintaIcon($this->data['Indicator']['icon'], "fa fa-car");
pintaIcon($this->data['Indicator']['icon'], "fa fa-taxi");
pintaIcon($this->data['Indicator']['icon'], "fa fa-tree");
pintaIcon($this->data['Indicator']['icon'], "fa fa-spotify");
pintaIcon($this->data['Indicator']['icon'], "fa fa-deviantart");
pintaIcon($this->data['Indicator']['icon'], "fa fa-soundcloud");
pintaIcon($this->data['Indicator']['icon'], "fa fa-database");
pintaIcon($this->data['Indicator']['icon'], "fa fa-file-pdf-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-file-word-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-file-excel-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-file-powerpoint-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-file-image-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-file-archive-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-file-audio-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-file-video-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-file-code-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-vine");
pintaIcon($this->data['Indicator']['icon'], "fa fa-codepen");
pintaIcon($this->data['Indicator']['icon'], "fa fa-jsfiddle");
pintaIcon($this->data['Indicator']['icon'], "fa fa-life-ring");
pintaIcon($this->data['Indicator']['icon'], "fa fa-circle-o-notch");
pintaIcon($this->data['Indicator']['icon'], "fa fa-rebel");
pintaIcon($this->data['Indicator']['icon'], "fa fa-empire");
pintaIcon($this->data['Indicator']['icon'], "fa fa-git-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-git");
pintaIcon($this->data['Indicator']['icon'], "fa fa-hacker-news");
pintaIcon($this->data['Indicator']['icon'], "fa fa-tencent-weibo");
pintaIcon($this->data['Indicator']['icon'], "fa fa-qq");
pintaIcon($this->data['Indicator']['icon'], "fa fa-weixin");
pintaIcon($this->data['Indicator']['icon'], "fa fa-paper-plane");
pintaIcon($this->data['Indicator']['icon'], "fa fa-paper-plane-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-history");
pintaIcon($this->data['Indicator']['icon'], "fa fa-circle-thin");
pintaIcon($this->data['Indicator']['icon'], "fa fa-header");
pintaIcon($this->data['Indicator']['icon'], "fa fa-paragraph");
pintaIcon($this->data['Indicator']['icon'], "fa fa-sliders");
pintaIcon($this->data['Indicator']['icon'], "fa fa-share-alt");
pintaIcon($this->data['Indicator']['icon'], "fa fa-share-alt-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bomb");
pintaIcon($this->data['Indicator']['icon'], "fa fa-futbol-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-tty");
pintaIcon($this->data['Indicator']['icon'], "fa fa-binoculars");
pintaIcon($this->data['Indicator']['icon'], "fa fa-plug");
pintaIcon($this->data['Indicator']['icon'], "fa fa-slideshare");
pintaIcon($this->data['Indicator']['icon'], "fa fa-twitch");
pintaIcon($this->data['Indicator']['icon'], "fa fa-yelp");
pintaIcon($this->data['Indicator']['icon'], "fa fa-newspaper-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-wifi");
pintaIcon($this->data['Indicator']['icon'], "fa fa-calculator");
pintaIcon($this->data['Indicator']['icon'], "fa fa-paypal");
pintaIcon($this->data['Indicator']['icon'], "fa fa-google-wallet");
pintaIcon($this->data['Indicator']['icon'], "fa fa-cc-visa");
pintaIcon($this->data['Indicator']['icon'], "fa fa-cc-mastercard");
pintaIcon($this->data['Indicator']['icon'], "fa fa-cc-discover");
pintaIcon($this->data['Indicator']['icon'], "fa fa-cc-amex");
pintaIcon($this->data['Indicator']['icon'], "fa fa-cc-paypal");
pintaIcon($this->data['Indicator']['icon'], "fa fa-cc-stripe");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bell-slash");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bell-slash-o");
pintaIcon($this->data['Indicator']['icon'], "fa fa-trash");
pintaIcon($this->data['Indicator']['icon'], "fa fa-copyright");
pintaIcon($this->data['Indicator']['icon'], "fa fa-at");
pintaIcon($this->data['Indicator']['icon'], "fa fa-eyedropper");
pintaIcon($this->data['Indicator']['icon'], "fa fa-paint-brush");
pintaIcon($this->data['Indicator']['icon'], "fa fa-birthday-cake");
pintaIcon($this->data['Indicator']['icon'], "fa fa-area-chart");
pintaIcon($this->data['Indicator']['icon'], "fa fa-pie-chart");
pintaIcon($this->data['Indicator']['icon'], "fa fa-line-chart");
pintaIcon($this->data['Indicator']['icon'], "fa fa-lastfm");
pintaIcon($this->data['Indicator']['icon'], "fa fa-lastfm-square");
pintaIcon($this->data['Indicator']['icon'], "fa fa-toggle-off");
pintaIcon($this->data['Indicator']['icon'], "fa fa-toggle-on");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bicycle");
pintaIcon($this->data['Indicator']['icon'], "fa fa-bus");
pintaIcon($this->data['Indicator']['icon'], "fa fa-ioxhost");
pintaIcon($this->data['Indicator']['icon'], "fa fa-angellist");
pintaIcon($this->data['Indicator']['icon'], "fa fa-cc");
pintaIcon($this->data['Indicator']['icon'], "fa fa-ils");
pintaIcon($this->data['Indicator']['icon'], "fa fa-meanpath");
?>
</div>

		</div>
		<div class="form-group col-lg-4">
		
			<p><?php echo __("Select dates based on Fiscal Year");?></p>
			
			<?php
			
			$y=date("Y");
			if (date("m")>9) $y++;
			for($i=5; $i>=0; $i--) {
				if ($i!=5) echo " | ";
				$fy=$y-$i;
				echo "<a href=\"javascript:void(0);\" class='noload' onclick='javascript:selFY($fy);'>$fy</a>";
			}
			
			?>
		
		</div>
		
		<div class="form-group col-lg-4">
			<label for="name"><?php echo __("Start date");?></label>
			<input type="text" class="form-control" name="data[Indicator][inidate]" value="<?php echo isset($this->data['Indicator']['inidate']) ? $this->data['Indicator']['inidate'] : '';?>" id="datepicker">
		</div>
		
		<div class="form-group col-lg-4">
			<label for="name"><?php echo __("End date");?></label>
			<input type="text" class="form-control" name="data[Indicator][enddate]" value="<?php echo isset($this->data['Indicator']['enddate']) ? $this->data['Indicator']['enddate'] : '';?>" id="datepicker-2">
		</div>
		
		
		
		        <div class="form-group col-lg-6">
            <?php
              if($action == "admin_edit"){echo $this->Form->input('Indicator.id');}
            ?>
			<label for="name"><?php echo __("Name");?> (ESP)*</label>
			<input type="text" class="form-control" name="data[Indicator][name][esp]" id="name" value="<?php echo isset($this->data['Indicator']['name']['esp']) ? $this->data['Indicator']['name']['esp'] : '';?>">

		</div>
<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
		<div class="form-group col-lg-6">
			<label for="name"><?php echo __("Name");?> (ENG)*</label>
			<input type="text" class="form-control" name="data[Indicator][name][eng]" id="name" value="<?php echo isset($this->data['Indicator']['name']['eng']) ? $this->data['Indicator']['name']['eng'] : '';?>">
		</div>
		<?php endif; ?>
<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
        <div class="form-group col-lg-6">
			<label for="name"><?php echo __("Indicator Metric");?> (ESP)*</label>
			<input type="text" class="form-control" name="data[Indicator][metric][esp]" id="name" value="<?php echo isset($this->data['Indicator']['metric']['esp']) ? $this->data['Indicator']['metric']['esp'] : '';?>">

		</div>
		<?php endif; ?>
		
		<div class="form-group col-lg-6">
			<label for="name"><?php echo __("Responsible (name)");?></label>
			<input type="text" class="form-control" name="data[Indicator][responsible]" id="name" value="<?php echo isset($this->data['Indicator']['responsible']) ? $this->data['Indicator']['responsible'] : '';?>">
		</div>
		<div class="form-group col-lg-6">
			<label for="name"><?php echo __("Responsible (email)");?></label>
			<input type="text" class="form-control" name="data[Indicator][responsible_email]" id="name" value="<?php echo isset($this->data['Indicator']['responsible_email']) ? $this->data['Indicator']['responsible_email'] : '';?>">
		</div>
		<?php if (! $this->Session->check('Config.LangVar') 
    			or ($this->Session->check('Config.LangVar') 
    			and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
		<div class="form-group col-lg-6">
			<label for="name"><?php echo __("Indicator Metric");?> (ENG)*</label>
			<input type="text" class="form-control" name="data[Indicator][metric][eng]" id="name" value="<?php echo isset($this->data['Indicator']['metric']['eng']) ? $this->data['Indicator']['metric']['eng'] : '';?>">
		</div>
		<?php endif; ?>
		<?php if (! $this->Session->check('Config.LangVar') 
    			or ($this->Session->check('Config.LangVar') 
    			and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
		<div class="form-group col-lg-12">
			<label for="name"><?php echo __("Description");?> (ESP)</label>
			<textarea class="summernote" name="data[Indicator][description][esp]" id="name"><?php echo isset($this->data['Indicator']['description']['esp']) ? $this->data['Indicator']['description']['esp'] : '';?></textarea>
		</div>
		<?php endif; ?>
		<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
		<div class="form-group col-lg-12">
			<label for="name"><?php echo __("Description");?> (ENG)</label>
			<textarea class="summernote" name="data[Indicator][description][eng]" id="name"><?php echo isset($this->data['Indicator']['description']['eng']) ? $this->data['Indicator']['description']['eng'] : '';?></textarea>
		</div>
		<?php endif; ?>

		<div class="form-group col-lg-12">
		<h2><?php echo __("Threshold"); ?></h2>

		<div class="form-group col-lg-6">

			<div class= "row-fluid">
			<label for="name"><?php echo __("Superior Cut Point (greater than)"); ?></label>
			
			<input 	name="data[Indicator][cond_value_2]" class="form-control" maxlength="11" type="text" onchange="cambiaSemaforo()"
					value="<?php echo $this->data['Indicator']['cond_value_2'];?>" id="IndicatorCondValue2">
			
				<input type="checkbox" name="data[Indicator][cut_value_2]" class="" value="1" id="IndicatorCutValue2" onclick="cambiaSemaforo()"
				<?php 
				if ( $this->data['Indicator']['cut_value_2'] ) {
					echo ' checked="checked" ';
				}
				?>
				> 
				<?php echo __("Greater than or equal");?>

			</div>

			<div class= "row-fluid">
			<label for="name"><?php echo __("Inferior Cut Point (greater than)"); ?></label>

			<input 	name="data[Indicator][cond_value_1]" class="form-control" maxlength="11" type="text" onchange="cambiaSemaforo()"
					value="<?php echo $this->data['Indicator']['cond_value_1'];?>" id="IndicatorCondValue1">
			
				<input type="checkbox" name="data[Indicator][cut_value_1]" class="" value="1" id="IndicatorCutValue1" onclick="cambiaSemaforo()"
				<?php 
				if ( $this->data['Indicator']['cut_value_1'] ) {
					echo ' checked="checked" ';
				}
				?>
				> 
				<?php echo __("Greater than or equal");?>

			</div>

			<div class= "row-fluid">
			<label for="name"><?php echo __("Semaphore order"); ?></label>
			<br />

			<label>
			<input type="radio" name="data[Indicator][cut_order]" value="0" onclick="cambiaSemaforo()"
				<?php echo checkthis($this->data['Indicator']['cut_order'], '0');?>> <?php echo __("Red above / Green below");?></label>
			<br />
			<label>
			<input type="radio" name="data[Indicator][cut_order]" value="1" onclick="cambiaSemaforo()"
				<?php echo checkthis($this->data['Indicator']['cut_order'], '1');?>> <?php echo __("Green above / Red below");?></label>

		</div>

			<div class= "row-fluid">
				<label for="name"><?php echo __("Baseline"); ?></label>				
				<?php echo $this->Form->input('baseline',
						array(	"div"=>false,
								"class"=>"form-control",
								"label"=>false,								
								"type"=>"text",
								"required"=>false								
								)); ?>			
			</div>
			<div class= "row-fluid">
				<label for="name"><?php echo __("Goal of period"); ?></label>
				<?php echo $this->Form->input('period_goal',
						array(	"div"=>false,
								"class"=>"form-control",
								"label"=>false,								
								"type"=>"text",
								"required"=>false								
								)); ?>			
			</div>			
			<div class= "row-fluid">	
				<div class="span3">			
					<label for="name"><?php echo __("Flag highlighted"); ?></label>
					<?php echo $this->Form->input('flag_hightlighted',
							array(	"div"=>false,
									"class"=>"form-control",
									"label"=>false,								
									"type"=>"checkbox",
									"required"=>false								
									)); ?>			
				</div>
			</div>
		</div>
		
		<div class="form-group col-lg-6">
			<div id=banda_superior 	style="background-color: red; 		min-height: 120px; 	max-width: 150px;">
				<?php
				echo "<p style=\"text-align: center;color: white; padding-top: 40px;\">".
					($this->data['Indicator']['cut_value_2']?'>= ':'> ').
					$this->data['Indicator']['cond_value_2']."</p>";
				?>
			</div>
			<div id=banda_media 	style="background-color: yellow; 	min-height: 120px; 	max-width: 150px;">
				<?php
				echo "<p style=\"text-align: center;color: gray; padding-top: 40px;\">".
					($this->data['Indicator']['cut_value_1']?'>= ':'> ').
					$this->data['Indicator']['cond_value_1']."</p>";
				?>
			</div>
			<div id=banda_inferior 	style="background-color: green;		min-height: 120px; 	max-width: 150px;">
				<?php
				echo "<p style=\"text-align: center;color: white; padding-top: 40px;\">".
					__("Otherwise")."</p>";
				?>
			</div>
		</div>
		
		
		
		
          <div class="form-group form_button">

                <div class="form-group">
                  <label for="note"><?php echo __("All fields with * are mandatory");?></label>
                </div>
								<button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

					</div>
          <div class="form-group form_response">
            <div id="responseForm"></div>
          </div>

      </form>


  </div>
</div>



<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Indicators.add");
</script>
<?php } ?>

<script>
$(".fa").click(function(){
	$('.resaltado').each(function(i, obj) {
		$(obj).removeClass('resaltado');
	});
	$("#elicon").val(this.className);
	$(this).addClass('resaltado');
});

$( document ).ready(function() {
  cambiaSemaforo();
});

</script>

