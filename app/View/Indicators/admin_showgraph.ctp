<div class="col-md-10">
                
<div class="widget stacked">
    
		<div class="widget-header">
					<i class="icon-list"></i>
	        <h3>
	            <?php echo __("Indicator");?>
	        </h3>
	    </div>
		<div class="widget-content">
			<?php

			echo "<h3><i class=\"".$indicator["Indicator"]["icon"]."\"></i> ".$indicator["Indicator"]["name"]."</h3>";
			echo "<p>".$indicator["Indicator"]["description"]."</p>";
			
			?> <div class="col-md-6"> <?php
			echo "<p><i class=\"icon-asterisk\"></i> ".__("Metric")."</p><h4>".$indicator["Indicator"]["metric"]."</h4>";
			
			?> </div><div class="col-md-6"> <?php
			echo "<p><i class=\"icon-asterisk\"></i> ".__("Goal")."</p><h4>".$indicator["Indicator"]["goal"]."</h4>";
			
			?> </div><div class="col-md-6"> <?php
			echo "<p><i class=\"icon-asterisk\"></i> ".__("Periodicity of measure")."</p><h4>".$meas[$indicator["Indicator"]["periodic_measure"]]."</h4>";
			
			?> </div><div class="col-md-6"> <?php
			echo "<p><i class=\"icon-asterisk\"></i> ".__("Calculation Type")."</p><h4>".$calc[$indicator["Indicator"]["calculation_type"]]."</h4>";

			?> </div><div class="col-md-6"> <?php
			echo "<p><i class=\"icon-asterisk\"></i> ".__("Responsible")."</p><h4>".$indicator["Indicator"]["responsible"]."</h4>";
			
			?> </div><div class="col-md-6"> <?php
			echo "<p><i class=\"icon-asterisk\"></i> ".__("Responsible (email)")."</p><h4>".$indicator["Indicator"]["responsible_email"]."</h4>";
			
			?> </div><div class="col-md-6"> <?php
			echo "<p><i class=\"icon-asterisk\"></i> ".__("Initial Date")."</p><h4>".$indicator["Indicator"]["inidate"]."</h4>";
			
			?> </div><div class="col-md-6"> <?php
			echo "<p><i class=\"icon-asterisk\"></i> ".__("End Date")."</p><h4>".$indicator["Indicator"]["enddate"]."</h4>";
			
			?> </div> <?php
/*
            [periodic_measure] => 2
            [calculation_type] => 1
            [indicator_type] => 1
            [responsible] => Pedro Perez
            [responsible_email] => pedrop@asdf.com
            [inidate] => 2015-10-01
            [enddate] => 2016-09-30
            [icon] => fa fa-glass
*/

//			pr($indicator);
			
			if (is_array($serie)) {
				
				$s=implode(",", $serie);
				
				if (is_array($data)) {
				
					$d=implode(",", $data);

					if ($graph_id) {
						echo '<img src="/pchart/Example'.$graph_id.'.php?isget=1&lang='.$lang.'&s='.$s.'&d='.$d.'&t='.urlencode($title).'">';
					}
					
				}
				
			}

			?>
		</div>

    </div>
</div>
