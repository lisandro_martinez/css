<div class="col-md-9">

	<div class="widget stacked ">

		<div class="widget-header">
			<i class="icon-circle-arrow-up"></i>
			<h3><?php echo __("title");?></h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">
		
			<div class="page-header">
				<h3><i class="icon-comment"></i> <?php echo __("Titulo de la sección");?></h3>
			</div>					
			
			Contenido
		
		</div> <!-- /widget-content -->

	</div>
	
</div>

<div class="col-md-3">

		<div class="widget stacked widget-box">
			
			<div class="widget-header">	
				<i class="icon-info"></i>
				<h3><?php echo __("Helper"); ?></h3>			
			</div> <!-- /widget-header -->
			
			<div class="well">
				
				<p><?php echo __("help"); ?></p>
				
			</div> <!-- /widget-content -->
			
		</div> <!-- /widget -->


</div>

