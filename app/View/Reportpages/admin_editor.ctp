<?php
$default_values = $this->params->data["default_values"];
?>

<!-- Bootstrap styles -->
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<!-- Generic page styles -->
<link rel="stylesheet" href="/upload/css/style.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="/upload/css/jquery.fileupload.css">


<?php

include_once( dirname(dirname(dirname(__FILE__))).'/webroot/css-pdf/code/html_generator/html_generator.php' );

if ($json = $json->params) {

	$params = json_decode(json_encode($json), true);

	if (! $this->Session->check('Config.MyLangVar') or ($this->Session->check('Config.MyLangVar') and $this->Session->read('Config.MyLangVar') == 'esp') ) { 
		$lang='esp';
	} else {
		$lang='eng';
	}
	
	foreach($params as $field=>$p) {

		// find loaded value
		$value_esp = $default_values['editor__text__'.$this->params->data["template"].'__'.$field.'__esp'];
		$value_eng = $default_values['editor__text__'.$this->params->data["template"].'__'.$field.'__eng'];

		switch($p["type"]) {
		case htmlGenerator::TYPE_TEXT_SIMPLE:
			echo '<div class="row">';
			if ($esp) echo $this->element('editor_text_simple', ['title' => $p["label"][$lang], 'field' => $field, 'lang' => 'esp', 'value' => $value_esp]);
			if ($eng) echo $this->element('editor_text_simple', ['title' => $p["label"][$lang], 'field' => $field, 'lang' => 'eng', 'value' => $value_eng]);
			echo '</div>';
			break;
		case htmlGenerator::TYPE_IMG:
			echo '<div class="row">';
			if ($esp) echo $this->element('editor_img', ['title' => $p["label"][$lang], 'field' => $field, 'lang' => 'esp', 'value' => $value_esp]);
			if ($eng) echo $this->element('editor_img', ['title' => $p["label"][$lang], 'field' => $field, 'lang' => 'eng', 'value' => $value_eng]);
			echo '</div>';
			break;
		case htmlGenerator::TYPE_TEXT:
			echo '<div class="row">';
			if ($esp) echo $this->element('editor_text', ['title' => $p["label"][$lang], 'field' => $field, 'lang' => 'esp', 'value' => $value_esp]);
			if ($eng) echo $this->element('editor_text', ['title' => $p["label"][$lang], 'field' => $field, 'lang' => 'eng', 'value' => $value_eng]);
			echo '</div>';
			break;
		case htmlGenerator::TYPE_WYSIWYG:
			echo '<div class="row">';
			if ($esp) echo $this->element('editor_wysiwyg', ['title' => $p["label"][$lang], 'field' => $field, 'lang' => 'esp', 'value' => $value_esp]);
			if ($eng) echo $this->element('editor_wysiwyg', ['title' => $p["label"][$lang], 'field' => $field, 'lang' => 'eng', 'value' => $value_eng]);
			echo '</div>';
			break;
		case htmlGenerator::TYPE_TABLE:

			$md = (int) (12 / ($p["cols"] + 1));
			$row = 1;
			
			echo '<div style="clear: both"></div>';

			echo "<h3>".$p["label"][$lang]."</h3>";

			// header
			echo '<div class="row">';
				echo '<div class="form-group col-md-1"></div>';
				$col="A";
				for ($j=1; $j<=$p["cols"]; $j++) {
					echo '<div class="form-group col-md-'.$md.'">';
						echo '<h3>'.$col.'</h3>';
					echo '</div>';
					$col++;
				}
			echo '</div>';

			// rows
			for($i=1; $i<=$p["rows"]; $i++) {
				$col="A";
				echo '<div class="row">';
					echo '<div class="form-group col-md-1"><h3>'.$row.'</h3></div>';
					for ($j=1; $j<=$p["cols"]; $j++) {

						$ind_esp = "editor__text__".$template."__".$field."_".$col."_".$row."__esp";
						$ind_eng = "editor__text__".$template."__".$field."_".$col."_".$row."__eng";

						echo '<div class="form-group col-md-'.$md.'">';
							if ($esp) echo $this->element('editor_text_cell', ['title' => '', 'field' => $field."_".$col."_".$row, 'lang' => 'esp', 'value' => $default_values[$ind_esp]]);
							if ($eng) echo $this->element('editor_text_cell', ['title' => '', 'field' => $field."_".$col."_".$row, 'lang' => 'eng', 'value' => $default_values[$ind_eng]]);
						echo '</div>';
						$col++;
					}
				echo '</div>';
				$row++;
			}
			
			?>


				<!-- Inicio de Summer Note -->
					
					<script type="text/javascript">
						$(document).ready(function() {
							$('.summernote_cell').summernote({
							  toolbar: [
								// [groupName, [list of button]]
								['style', ['bold', 'italic']],
								['fontsize', ['fontsize']],
								['color', ['color']],
							  ]
							});
						});
					</script>

				<!-- Fin de Summer Note -->

			<?php			
			
			break;
		}
	
	}

}

?>

<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
<script src="/upload/js/vendor/jquery.ui.widget.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="/upload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="/upload/js/jquery.fileupload.js"></script>

