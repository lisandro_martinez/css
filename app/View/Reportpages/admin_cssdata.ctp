<?php

// show node preview
if ($node and $nodetype) {
	$data_node = $nodes[$nodetype][$node]["Node"];
	echo '<div id="the_html" style="width: 100%; height: 150px; overflow-y: scroll;">';
	echo "<h3>".$data_node["name"]."</h3>";
	echo $data_node["description"];

	// es una gráfica?
	if($data_node["nodetype_id"]==21) {
		$graphdata = $graphdata->find('all', array("conditions"=>array('node_id'=>$node), 'limit'=>100));
		echo "<p><img src=\"$urlwebroot/pchart/Example".$graphdata[0]["Graphdata"]["type"].".php?g=".$node."&lang=".$lang."\"></p>";
	}

	// es una galería?
	if($data_node["nodetype_id"]==18) {

		$path = WWW_ROOT . "uploads/$company_id/$node";
		$url = "/uploads/$company_id/$node/";
		$dir = scandir($path);
		foreach ($dir as $key => $value) {
			if ($value!=="." and $value!=="..") {
			?>

				<div style="margin: 5px;border: 1px solid #ccc;float: left;width: 300px;">

					<div style="min-height: 200px;">
						<img src="<?php echo $url.$value;?>" width="600" height="400" style="overflow: hidden;max-width: 100%;height: auto;"> 
					</div>

				  <div style="padding: 15px;text-align: center;"><?php 
		 				if (is_array($imgs["imgs"])) {
							foreach ($imgs["imgs"] as $key2 => $value2) {
								if ($value2["file"]==$value) {
									echo $value2["desc"][$lang];
								}
							}
						}
				  ?></div>
				</div>

			<?php

			}
		}
	}

	// encuestas
	if ($data_node["nodetype_id"]==19) {
		echo $this->element('surveytotalize', array('totaliza'=>$totaliza, "pdf"=>true));
	}

	// Si es un buzon de mensajes
	if ($data_node["nodetype_id"]==Node::TYPE_MAILBOX) {
		echo $this->element('mailbox', array('node'=>$data_node, 'msgs'=>$msgs, 'id'=>$node, "pdf"=>1));
	} 


	echo '</div>';
	die;
}

// show IG preview
if ($ig) {

	foreach ($indicators as $key => $value) {
		if ($ig==$value["Indicator"]["id"]) {
			echo '<div id="the_html" style="width: 100%; height: 150px; overflow-y: scroll;">';
			echo '</div>';

			?>
			<script type="text/javascript">

				$("#the_html").html("");

			    $.ajax({
			        type: "POST",
			        url: "<?=$urlwebroot?>/admin/indicators/show",
			        data: {
			        	id: <?=$value["Indicator"]["id"]?>,
			        	pdf: true
			    	},
			        success: function(data)
			        {
			            $("#the_html").html(data);

					    $.ajax({
					        type: "GET",
					        url: "<?=$urlwebroot?>/admin/indicators/detail/<?=$value["Indicator"]["id"]?>",
					        success: function(data)
					        {
					            $("#the_html").append(data);
					        }
					    });

			        }
			    });
			    
			</script>
		    <?php

		}
	}
	die;

}

// show SA preview
if ($sa) {

	foreach ($auto as $key => $value) {
		if ($sa==$value["Autoevaluation"]["id"]) {
			echo '<div id="the_html" style="width: 100%; height: 150px; overflow-y: scroll;">';

			echo "<h2>".$value["Questionnaire"]["acronym"]."</h2>";
			echo "<p>".$value["Questionnaire"]["title"]."</p>";
			echo "<h4>".$value["Autoevaluation"]["total_points"]."/".$value["QuestionnaireTotalPoints"]."</h4>";
			if ($value["QuestionnaireTotalPoints"]) {
				$porc = $value["Autoevaluation"]["total_points"]/$value["QuestionnaireTotalPoints"]*100;
				$total_width = 300;
				$porcentaje = (int) $porc * $total_width / 100;
				echo '<div style="background-color: #f0f0f0; width: '.$total_width.'px; height: 25px;">';
					echo '<div style="background-color: #4ca64c; width: '.$porcentaje.'px; height: 25px;">';
					echo '</div>';
				echo '</div>';
			}
			echo "<p>".$value["Autoevaluation"]["date_time"]."</p>";

			/*
Array
(
    [Autoevaluation] => Array
        (
            [id] => 141
            [date_time] => 2016-12-03 10:09:19
            [user_id] => 23
            [questionnaire_id] => 10
            [total_points] => 15
            [company_id] => 16
            [corporation_id] => 7
        )

    [Questionnaire] => Array
        (
            [id] => 10
            [title] => Cuestionario de la Comisión Europea
            [description] => Empresas responsables Cuestionario de Concienciación de la Comisión Europea
            [active] => 1
            [acronym] => RSE CE
            [logo] => files/logos/110435577dffdd12e865380e2d9666b205f889.png
            [icon] => 
            [locale] => esp
        )

    [User] => Array
        (
            [id] => 23
            [fname] => Augusto
            [lname] => Gonzalez
            [email] => ag@asdf.com
            [company_id] => 16
            [corporation_id] => 
            [username] => kerns_company
            [password] => 253e6edb8139392550fff418bac6265d0d2cc934
            [group_id] => 6
            [created] => 2015-10-28 17:52:07
            [modified] => 2015-10-28 17:52:07
            [age] => 
            [sex] => 
            [location] => 
            [profession] => 
            [active] => 1
            [interestgroup_id] => 0
        )

    [Company] => Array
        (
            [id] => 16
            [name] => Kerns
            [logo] => files/logos/1437108d4a494af92c909eb34323b22f7ffce6.png
            [description] => 1
            [phone] => 12333333
            [address] => 2
            [modified] => 2016-11-30 17:10:55
            [created] => 2015-10-27 14:37:10
            [corporation_id] => 7
            [active] => 1
            [slug] => kerns
            [url] => 
            [fb] => 
            [t] => 
            [ln] => 
            [y] => 
            [esp] => 1
            [eng] => 0
        )

    [Corporation] => Array
        (
            [id] => 7
            [name] => Florida Ice & Farm Company
            [logo] => files/logos/172724c9c2bce82b74978a3e87079f2ce6e52b.png
            [description] => Es una empresa de bebidas y alimentos
            [phone] => 22224444
            [address] => San José
            [modified] => 2015-10-17 17:27:24
            [created] => 2015-10-14 19:08:23
            [active] => 1
        )

    [QuestionnaireTotalPoints] => 58
)
			*/
			echo '</div>';
		}
	}
	die;

}

?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="/css/font-awesome.min.css" rel="stylesheet">

<?php

	// merga data and instruments
	foreach($nodetypes as $nt) {
		$datins[$nt["Nodetype"]["id"]]=$nt["Nodetype"]["name"];
	}
	foreach($instruments as $ins=>$instrument) {
		$datins[$ins]=$instrument;
	}
	asort($datins);

	echo "<h3>".__("Data")."</h3>";
	echo "<select id=data onchange=\"updateNode()\" class=\"form-control\">";
	echo "<option value=\"0\">".__("Choose")."</option>";
	foreach($datins as $i=>$nt) {
		echo "<option value=\"".$i."\">".$nt."</option>";
	}
	echo "</select><hr>";

	// show all nodetypes title and descriptions in HTML
	foreach($nodetypes as $nt) {
		echo "<div id=div".$nt["Nodetype"]["id"]." class=\"selectores\" style=\"display: none;\">";
		if (is_array($nodes[$nt["Nodetype"]["id"]]) and sizeof($nodes[$nt["Nodetype"]["id"]])) {
			echo "<select id=data".$nt["Nodetype"]["id"]." onchange=\"selectNode('data".$nt["Nodetype"]["id"]."')\" class=\"form-control\">";
			echo "<option value=\"0\">".__("Choose")."</option>";
			foreach($nodes[$nt["Nodetype"]["id"]] as $nt2) {
				echo "<option value=\"".$nt2["Node"]["id"]."\">".$nt2["Node"]["name"]."</option>";
			}
			echo "</select>";
		} else {
			echo __("No entities found");
		}
		echo "</div>";
	}

	// show all indicators
	echo "<div id=divIG".$indicator["Indicator"]["id"]." class=\"selectores\" style=\"display: none;\">";
	echo "<select id=dataIG onchange=\"selectIG('dataIG')\" class=\"form-control\">";
	echo "<option value=\"0\">".__("Choose")."</option>";
	foreach($indicators as $indicator) {
		echo "<option value=\"".$indicator["Indicator"]["id"]."\">".$indicator["Indicator"]["name"]."</option>";
	}
	echo "</select>";
	echo "</div>";

/*
Array
(
    [0] => Array
        (
            [Autoevaluation] => Array
                (
                    [id] => 141
                    [date_time] => 2016-12-03 10:09:19
                    [user_id] => 23
                    [questionnaire_id] => 10
                    [total_points] => 15
                    [company_id] => 16
                    [corporation_id] => 7
                )

            [Questionnaire] => Array
                (
                    [id] => 10
                    [title] => Cuestionario de la Comisión Europea
                    [description] => Empresas responsables Cuestionario de Concienciación de la Comisión Europea
                    [active] => 1
                    [acronym] => RSE CE
                    [logo] => files/logos/110435577dffdd12e865380e2d9666b205f889.png
                    [icon] => 
                    [locale] => esp
                )

            [User] => Array
                (
                    [id] => 23
                    [fname] => Augusto
                    [lname] => Gonzalez
                    [email] => ag@asdf.com
                    [company_id] => 16
                    [corporation_id] => 
                    [username] => kerns_company
                    [password] => 253e6edb8139392550fff418bac6265d0d2cc934
                    [group_id] => 6
                    [created] => 2015-10-28 17:52:07
                    [modified] => 2015-10-28 17:52:07
                    [age] => 
                    [sex] => 
                    [location] => 
                    [profession] => 
                    [active] => 1
                    [interestgroup_id] => 0
                )

            [Company] => Array
                (
                    [id] => 16
                    [name] => Kerns
                    [logo] => files/logos/1437108d4a494af92c909eb34323b22f7ffce6.png
                    [description] => 1
                    [phone] => 12333333
                    [address] => 2
                    [modified] => 2016-11-30 17:10:55
                    [created] => 2015-10-27 14:37:10
                    [corporation_id] => 7
                    [active] => 1
                    [slug] => kerns
                    [url] => 
                    [fb] => 
                    [t] => 
                    [ln] => 
                    [y] => 
                    [esp] => 1
                    [eng] => 0
                )

            [Corporation] => Array
                (
                    [id] => 7
                    [name] => Florida Ice & Farm Company
                    [logo] => files/logos/172724c9c2bce82b74978a3e87079f2ce6e52b.png
                    [description] => Es una empresa de bebidas y alimentos
                    [phone] => 22224444
                    [address] => San José
                    [modified] => 2015-10-17 17:27:24
                    [created] => 2015-10-14 19:08:23
                    [active] => 1
                )

            [QuestionnaireTotalPoints] => 58
        )

*/

	// show self assesment
	echo "<div id=divSA".$auto["Autoevaluation"]["id"]." class=\"selectores\" style=\"display: none;\">";
	echo "<select id=dataSA onchange=\"selectSA('dataSA')\" class=\"form-control\">";
	echo "<option value=\"0\">".__("Choose")."</option>";
	foreach($auto as $a) {
		echo "<option value=\"".$a["Autoevaluation"]["id"]."\">".$a["Questionnaire"]["acronym"]." - ".$a["Autoevaluation"]["date_time"]."</option>";
	}
	echo "</select>";
	echo "</div>";

?>

<div id="preview_data"></div>
<br/>

<span class="btn btn-info fileinput-button" onclick="updateCSSData()">
	<i class="glyphicon glyphicon-plus"></i>
	<span><?=__("Choose")?></span>
</span>

<script>

	function updateNode() {

		$(".selectores").fadeOut();
		$("#div"+$("#data").val()).fadeIn("slow");
		$("#preview_data").fadeOut();

	}
	function selectNode(sel) {
		$("#preview_data").hide();
		
		$.ajax({
			type: "POST",
			url: "<?=$urlwebroot?>/admin/Reportpages/cssdata",
			data: { 
				nodetype: $("#data").val(),
				node: $("#"+sel).val()
			},
			success: function(data, textStatus) {
				$("#preview_data").fadeOut(function() {
					console.log(data);
					$(this).html(data).fadeIn();
				});
			},
			error: function() {
				console.log('Error loading.');
			}
		});

	}

	function selectIG(sel) {
		$("#preview_data").hide();
		
		$.ajax({
			type: "POST",
			url: "<?=$urlwebroot?>/admin/Reportpages/cssdata",
			data: { 
				ig: $("#"+sel).val()
			},
			success: function(data, textStatus) {
				$("#preview_data").fadeOut(function() {
					console.log(data);
					$(this).html(data).fadeIn();
				});
			},
			error: function() {
				console.log('Error loading.');
			}
		});

	}

	function selectSA(sel) {
		$("#preview_data").hide();
		
		$.ajax({
			type: "POST",
			url: "<?=$urlwebroot?>/admin/Reportpages/cssdata",
			data: { 
				sa: $("#"+sel).val()
			},
			success: function(data, textStatus) {
				$("#preview_data").fadeOut(function() {
					console.log(data);
					$(this).html(data).fadeIn();
				});
			},
			error: function() {
				console.log('Error loading.');
			}
		});

	}

	function updateCSSData() {
/*
		if (!window.opener.$("#<?=$this->params->query["upload_id"]?>").summernote('code') || 
			window.opener.$("#<?=$this->params->query["upload_id"]?>").summernote('code')=="<p><br></p>" || 
			confirm("<?php echo __("Are you sure you want to delete the current content?");?>")) {
*/
			window.opener.$("#<?=$this->params->query["upload_id"]?>").summernote('code', 
				window.opener.$("#<?=$this->params->query["upload_id"]?>").summernote('code') + 
				$("#the_html").html()
			);
			window.close();
//		}
	}

</script>

<script src="/js/libs/jquery-3.1.1.min.js"></script>
<script src="/js/libs/jquery-ui-1.10.0.custom.min.js"></script>
<script src="/js/libs/jquery.form.min.js"></script>
<script src="/js/libs/bootstrap.min.js"></script>
<script src="/js/plugins/msgbox/jquery.msgbox.min.js"></script><?php
