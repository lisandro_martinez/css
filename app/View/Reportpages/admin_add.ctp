<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">

        <?php
          echo $this->Form->create('Reportpage', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"], 'enctype'=>"multipart/form-data"));
        ?>
        
<!--
        <form action="/admin/reportpages/edit/14" class="col-lg-12" id="ReportpageAdminEditForm" method="post" accept-charset="utf-8">
-->

		<div class="form-group col-md-9">

			<div class="form-group col-md-12">

				<?php
				  if($action == "admin_edit"){echo $this->Form->input('Reportpage.id');}
				?>

				 <label for="name"><?php echo __("Report");?></label>
				 <?php echo $this->Form->input('report_id',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>__("Choose"),"type"=>"select","required"=>false, 'options'=>$reports, 'value'=>$this->data['Reportpage']['report_id'])); ?>

			</div>
			<div class="form-group col-md-12">
			
				<label for="name"><?php echo __("Order");?> *</label>
				<input type="number" style="width: 95%;" class="form-control" name="data[Reportpage][order]" id="name" value="<?php echo isset($this->data['Reportpage']['order']) ? $this->data['Reportpage']['order'] : '';?>">

			</div>
			<div class="form-group col-md-12">
			
				<label for="name"><?php echo __("Name");?> *</label>
				<input type="text" style="width: 95%;" class="form-control" name="data[Reportpage][name]" id="name" value="<?php echo isset($this->data['Reportpage']['name']) ? $this->data['Reportpage']['name'] : '';?>">

			</div>
			<div class="form-group col-md-12">
			

				<?php

				if (! $this->Session->check('Config.LangVar') or ($this->Session->check('Config.LangVar') and $this->Session->read('Config.LangVar') == 'esp') ) { 
					$lang='esp';
				} else {
					$lang='eng';
				}

				if ($action=="admin_add") {
				?>

					<label for="name"><?php echo __("Choose Template");?> *</label><br/>

					<input name="data[Reportpage][tag]" id="thumb_select" type="hidden" value="<?=$this->data['Reportpage']['tag']?>">

					<?php 

					echo '<div class="row">';

					foreach($templates as $tag=>$temp_data) {
						
						/*
						echo '<option value="'.$tag.'"';
						if ($this->data['Reportpage']['tag'] == $tag) echo ' selected ';
						echo '>'.($temp_data['names'][$lang]?$temp_data['names'][$lang]:$tag).'</option>';
						*/

						echo "<div class=\"form-group col-md-3\" style=\"height:250px;\">";

						if ($this->data['Reportpage']['tag'] == $tag) $style=" border-color: green; border-width: 5px;";
						else $style="";
						
						echo '<p style="text-align: center;">';
						echo '<img id="img_'.$tag.'" src="/img/report/'.$tag.'.png" class="thumbnail-editor" style="max-width: 120px;'.$style.'" onclick="selectTemplate(\''.$tag.'\')">';
						echo '<br/><br/>';
						echo $temp_data['names'][$lang]?$temp_data['names'][$lang]:$tag;
						echo '</p>';

						echo "</div>";
						
					}

					echo '</div class="row">';

			} else {
					echo '<input name="data[Reportpage][tag]" type="hidden" value="'.$this->data['Reportpage']['tag'].'" id="thumb_select" >';
					echo '<p>&nbsp;</p><h3>'.$templates[ $this->data['Reportpage']['tag'] ]['names'][$lang].'</h3>';
			}
			?>
			
			</div>

			<div class="form-group col-md-12" id="editor"></div>

			  <div class="form-group form_button">

					<div class="form-group">
					  <label for="note"><?php echo __("All fields with * are mandatory");?></label>
					</div>
					<button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

			  </div>
			  <div class="form-group form_response">
				<div id="responseForm"></div>
			  </div>

		</div>
        <div class="form-group col-md-3" id="thumb_preview"></div>

      </form>


  </div>
</div>



<?php if($action == "admin_add"){ ?>
<script type="text/javascript">
Controllers.push("Reportpages.add");
</script>
<?php } ?>

<script>
function ponThumb() {
		
	$('#thumb_preview').fadeOut("fast", function() {
		var template = $("#thumb_select").val();
		if (template) {
			$('#thumb_preview').html('<img src="/img/report/'+template+'.png" class="thumbnail-editor">');
		}
		$('#thumb_preview').fadeIn("fast");

		
		var templates = JSON.parse('<?php echo json_encode($templates);?>');

		<?php
		if ($action=="admin_edit") {
		?>
			$.ajax({
				type: "POST",
				url: "/admin/reportpages/editor",
				data: { 
					json: JSON.stringify(templates[template]),
					default_values: <?php echo json_encode($default_values);?>,
					template: template
				},
				success: function(data, textStatus) {
					$("#editor	").fadeOut(function() {
						$(this).html(data).fadeIn();
					});
				},
				error: function() {
					console.log('Error loading.');
				}
			});
		<?php
		}
		?>

	});
	
}
$( document ).ready(function() {
  ponThumb();
});

function selectTemplate(tmp) {
	// remove border color and border with from class: thumbnail-editor
	$(".thumbnail-editor").css('border-color','');
	$(".thumbnail-editor").css('border-width','');
	// add border-color: green; border-width: 5px; to id: img_tmp
	$("#img_"+tmp).css('border-color','green');
	$("#img_"+tmp).css('border-width','5px');
	// set hidden value
	$("#thumb_select").val(tmp);
	ponThumb();
}
</script>

<!-- include summernote -->
<link rel="stylesheet" href="/summernote/dist/summernote.css">
<script type="text/javascript" src="/summernote/dist/summernote.min.js"></script>


