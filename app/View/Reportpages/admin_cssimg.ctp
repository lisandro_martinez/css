<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="/css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<link href="/css/font-awesome.min.css" rel="stylesheet">

<?php
	if (is_array($graphs) and sizeof($graphs)) {
		echo "<h3>".__("Graph")."</h3>";
		echo "<select id=graphs onchange=\"updateGraph()\" class=\"form-control\">";
		echo "<option value=\"0\">".__("Choose")."</option>";
		foreach($graphs as $g) {
			echo "<option value=\"/pchart/Example".$g["Graphdata"]["type"].".php?g=".$g["Node"]["id"]."&lang=".$lang."\">".$g["Graphdata"]["name"]."</option>";
		}
		echo "</select>";
		$graph=true;
	} else {
		echo __("No graphs defined");
		$graph=false;
	}
?>

<div id="preview_img"></div>
<br/>

<?php if ($graph) { ?>
<span class="btn btn-info fileinput-button" onclick="updateCSSObject()">	
	<i class="glyphicon glyphicon-plus"></i>
	<span><?=__("Choose")?></span>
</span>
<?php } ?>

<script>
	function updateGraph() {
		$("#preview_img").html("<img src=\"<?=$urlwebroot?>/"+$("#graphs").val()+"\" style=\"max-width:100%;\">");
	}
	function updateCSSObject() {
		if ($("#graphs").val()!=0) {
			$("#value<?=$this->params->query["upload_id"]?>", opener.document).val("<?=$urlwebroot?>"+$("#graphs").val());
			opener.updateImage("<?=$this->params->query["upload_id"]?>", "<?=$urlwebroot?>/"+$("#graphs").val(), "<?=$urlwebroot?>/"+$("#graphs").val());
			window.opener.$("#controls<?=$this->params->query["upload_id"]?>").hide("slow");
			window.close();
		} else {
			alert("<?=__("Please choose a graph")?>");
		}
	}
</script>

<script src="/js/libs/jquery-3.1.1.min.js"></script>
<script src="/js/libs/jquery-ui-1.10.0.custom.min.js"></script>
<script src="/js/libs/jquery.form.min.js"></script>
<script src="/js/libs/bootstrap.min.js"></script>
<script src="/js/plugins/msgbox/jquery.msgbox.min.js"></script><?php
