<script>
function onc() {
	var elval=$( "#entitype" ).val();
	if (elval>=10 && elval<=15) {
		$( "#divcost" ).show();
	} else {
		$( "#divcost" ).hide();
	}
}
function selFY(y) {
	document.getElementById("datepicker").value = '01-10-'+(y-1);
	document.getElementById("datepicker-2").value = "30-09-"+y;
}
</script>
<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">

        <?php
          echo $this->Form->create('Indicatorvalue', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
        ?>

		<div class="form-group col-lg-6">
			 <label for="name"><?php /*echo __("Indicator");*/?></label>
			 <?php 
			 
			 if (!$ind_id_value = $this->data['Indicatorvalue']['indicator_id']) {
				 $ind_id_value = $indicator_id;
				 
			 }
			 
			 
			 echo "<h2>$name</h2>";
			 
			 echo $this->Form->input('indicator_id', array(
															"div"=>false,
															"class"=>"form-control",
															"label"=>false,
															"empty"=>__("Choose"),
															"type"=>"hidden",
															"required"=>false, 
															'value'=>$ind_id_value
			)); 
			 
			 ?>

		</div>
		
        <div class="form-group col-lg-6">
            <?php
              if($action == "admin_edit"){echo $this->Form->input('Indicatorvalue.id');}
            ?>
			<label for="name"><?php echo __("Value");?> *</label>
			<input type="text" class="form-control" name="data[Indicatorvalue][value]" id="name" value="<?php echo isset($this->data['Indicatorvalue']['value']) ? $this->data['Indicatorvalue']['value'] : '';?>">

		</div>

		<div class="form-group col-lg-4">
			<label for="name"><?php echo __("Date");?></label>
			<?php
			  switch($periodic_measure) {
				// 1 - Semanal
			  case 1:
				$day = date('w');
				$week_start = date('d-m-Y', strtotime('-'.$day.' days'));
				for ($resta=0; $resta<=156; $resta++) {
					$weeks[$semana = date('d-m-Y', strtotime($week_start)-$resta*7*24*60*60)]=__("Week of")." ".$semana;
					
				}
				echo $this->Form->input('date', [
											"div"		=> false,
											"class"		=> "form-control",
											"label"		=> false,
											"empty"		=> __("Choose"),
											"type"		=> "select",
											"required"	=> false, 
											"options"	=> $weeks, 
											"value"		=> $this->data['Indicatorvalue']['date']
				]);
				break;
				// 2 - Mensual
			  case 2:
			  
				$month_year = date('m-Y');
				$month_start = date('d-m-Y', strtotime('01-'.$month_year));
				for ($resta=0; $resta<=36; $resta++) {
					$month = date('m-Y', strtotime($month_start)-$resta*30*24*60*60);
					$months['01-'.$month] = $month;
				}
				echo $this->Form->input('date', [
											"div"		=> false,
											"class"		=> "form-control",
											"label"		=> false,
											"empty"		=> __("Choose"),
											"type"		=> "select",
											"required"	=> false, 
											"options"	=> $months, 
											"value"		=> $this->data['Indicatorvalue']['date']
				]);

				break;
				// 3 - Bimensual
			  case 3:

				$today=time();
				// usa meses pares solamente
				if (date('m', $today)%2<>0) {
					$today-=30*24*60*60;
				}

				$month_year = date('m-Y',$today);
				$month_start = date('d-m-Y', strtotime('01-'.$month_year));
				for ($resta=0; $resta<=18; $resta+=2) {
					$month = date('m-Y', strtotime($month_start)-$resta*30*24*60*60);
					$months['01-'.$month] = $month;
				}
				echo $this->Form->input('date', [
											"div"		=> false,
											"class"		=> "form-control",
											"label"		=> false,
											"empty"		=> __("Choose"),
											"type"		=> "select",
											"required"	=> false, 
											"options"	=> $months, 
											"value"		=> $this->data['Indicatorvalue']['date']
				]);

				break;
				// 4 - Trimestral
			  case 4:

				$today=time();
				// usa meses tri-pares solamente
				if (date('m', $today)%3<>0) {
					$today-=30*24*60*60;
					if (date('m', $today)%3<>0) {
						$today-=30*24*60*60;
					}
				}

				$month_year = date('m-Y',$today);
				$month_start = date('d-m-Y', strtotime('01-'.$month_year));
				for ($resta=0; $resta<=18; $resta+=3) {
					$month = date('m-Y', strtotime($month_start)-$resta*30*24*60*60);
					$months['01-'.$month] = $month;
				}
				echo $this->Form->input('date', [
											"div"		=> false,
											"class"		=> "form-control",
											"label"		=> false,
											"empty"		=> __("Choose"),
											"type"		=> "select",
											"required"	=> false, 
											"options"	=> $months, 
											"value"		=> $this->data['Indicatorvalue']['date']
				]);

				break;
				// 5 - Cuatrimestral
			  case 5:

				$today=time();
				// usa meses cuatri-pares solamente
				if (date('m', $today)%4<>0) {
					$today-=30*24*60*60;
					if (date('m', $today)%4<>0) {
						$today-=30*24*60*60;
						if (date('m', $today)%4<>0) {
							$today-=30*24*60*60;
						}
					}
				}

				$month_year = date('m-Y',$today);
				$month_start = date('d-m-Y', strtotime('01-'.$month_year));
				for ($resta=0; $resta<=18; $resta+=4) {
					$month = date('m-Y', strtotime($month_start)-$resta*30*24*60*60);
					$months['01-'.$month] = $month;
				}
				echo $this->Form->input('date', [
											"div"		=> false,
											"class"		=> "form-control",
											"label"		=> false,
											"empty"		=> __("Choose"),
											"type"		=> "select",
											"required"	=> false, 
											"options"	=> $months, 
											"value"		=> $this->data['Indicatorvalue']['date']
				]);

				break;
				// 6 - Semestral
			  case 6:

				$today=time();
				// usa semestres solamente
				if (date('m', $today)%6<>0) {
					$today-=30*24*60*60;
					if (date('m', $today)%6<>0) {
						$today-=30*24*60*60;
						if (date('m', $today)%6<>0) {
							$today-=30*24*60*60;
							if (date('m', $today)%6<>0) {
								$today-=30*24*60*60;
								if (date('m', $today)%6<>0) {
									$today-=30*24*60*60;
									if (date('m', $today)%6<>0) {
										$today-=30*24*60*60;
									}
								}
							}
						}
					}
				}

				$month_year = date('m-Y',$today);
				$month_start = date('d-m-Y', strtotime('01-'.$month_year));
				for ($resta=0; $resta<=24; $resta+=6) {
					$month = date('m-Y', strtotime($month_start)-$resta*30*24*60*60);
					$months['01-'.$month] = $month;
				}
				echo $this->Form->input('date', [
											"div"		=> false,
											"class"		=> "form-control",
											"label"		=> false,
											"empty"		=> __("Choose"),
											"type"		=> "select",
											"required"	=> false, 
											"options"	=> $months, 
											"value"		=> $this->data['Indicatorvalue']['date']
				]);

				break;
				// 7 - Anual
			  case 7:

				$today=time();
				// usa años solamente
				$ano = date('Y', $today);

				for ($resta=0; $resta<=4; $resta++) {
					$anos['31-12-'.$ano] = $ano;
					$ano--;
				}
				echo $this->Form->input('date', [
											"div"		=> false,
											"class"		=> "form-control",
											"label"		=> false,
											"empty"		=> __("Choose"),
											"type"		=> "select",
											"required"	=> false, 
											"options"	=> $anos, 
											"value"		=> $this->data['Indicatorvalue']['date']
				]);

				break;
				// 0 diario
			  default:
				?> <input	type="text" class="form-control" name="data[Indicatorvalue][date]" value="<?php echo isset($this->data['Indicatorvalue']['date']) ? $this->data['Indicatorvalue']['date'] : '';?>" id="datepicker"> <?php
				break;
			  }
			  
			?>
			 
		</div>
		
          <div class="form-group form_button">

                <div class="form-group">
                  <label for="note"><?php echo __("All fields with * are mandatory");?></label>
                </div>
								<button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

					</div>
          <div class="form-group form_response">
            <div id="responseForm"></div>
          </div>

      </form>


  </div>
</div>



<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Indicatorvalues.add");
</script>
<?php } ?>


