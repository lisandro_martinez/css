<?php

    $headerstitles = array(
                            'Indicatorvalues.id' => '#',
                            'Indicators.name' => __('Indicator'),
                            'Indicatorvalues.date' => __('Date'),
                            'Indicatorvalues.value' => __('Value'),
                      );

?>

<div class="widget stacked">
    <?php  $this->Viewbase->panel_title(__('Value List'));   ?>
    <div class="widget-content">
        <?php include_once('filter.ctp');
            $this->Viewbase->Multi_form_create($this->form,'#');
           ?>

        <table class="table table-bordered">
           <?php
                // Encabezado de la tabla
                $this->Viewbase->table_Header($this->Paginator,$headerstitles);
            ?>
            <tbody>
                <?php 
				foreach ($lists as $list): ?>
                <tr>
                    <?php
                        // Campo check de cada linea
                        $datarow=array(
                            'idModel' => $list['Indicatorvalue']['id'],
                            'textname' => $list['Indicatorvalue']['name'],
                            'inputname'=> 'data[Indicatorvalue][id][]',
                            'value' => $list['Indicatorvalue']['id']
                        );
                        $this->Viewbase->Multi_check_row($datarow);
                        // Campo check de cada linea
                    ?>
                    <td style="width: 10px;"><?php echo h($list['Indicatorvalue']['id']); ?>&nbsp;</td>
                    <td><?php echo h($list['Indicator']['name']); ?>&nbsp;</td>
                    <td><?php echo h($list['Indicatorvalue']['date']); ?>&nbsp;</td>
                    <td><?php echo h($list['Indicatorvalue']['value']); ?>&nbsp;</td>

                     <td class="actions">

                        <?php


                             $databutton_edit = array(
                                    'url'=> '/admin/indicatorvalues/edit/'.$list['Indicatorvalue']['id'].'?indicator_id='.$_GET["indicator_id"]
                                );
                                $this->Viewbase->button_edit($this->Html,$databutton_edit);



                             $databutton_delete = array(
                                    'url'=> '/admin/indicatorvalues/delete/'.$list['Indicatorvalue']['id'],
                                    'idModel' =>$list['Indicatorvalue']['id'],
                                    'name'    =>$list['Indicatorvalue']['name']);
                                $this->Viewbase->button_delete($this->Html,$databutton_delete);



                        ?>
                    </td>

        	   </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="8">
                        <?php

                            $databutton_add = array(
                                    'url'=> '/admin/indicatorvalues/add/?indicator_id='.$_GET["indicator_id"]);
                                $this->Viewbase->button_add($this->Html,$databutton_add);

                        ?>
                    </td>
                </tr>
            </tbody>

        </table>

        <div class="checkalldiv col-md-6">
            <script>
                checkalltext = {
                    'empty' : {
                        'title' : '<?php echo __("Warning");?>',
                        'text' : '<?php echo __("You must select at least one register to use the multi-record option");?>'
                    },
                    'deleteall' :{
                        'title' : '<?php echo __("Multiple register deletion confirmation");?>',
                        'url' : '/admin/indicatorvalues/deletemulti/',
                        'pretext' : '<?php echo __("Are you sure you want to delete the following registers?");?>'
                    }
                };
            </script>
            <input type="checkbox" class="checkallclick" title="Check All">
            <label for="checkall"><?php echo __("Check All"); ?></label>
            <select id="selectmulti" name="submit_mult" class="autosubmit" style="margin-left:10px;">
                <option value="0" selected="selected"><?php echo __("With selected:"); ?></option>
                <option value="deleteall"><?php echo __("Delete All"); ?></option>
            </select>
        </div>
        <?php echo $this->Form->end(); ?>
        <?php echo $this->element('paginado'); ?>
    </div>
</div>

<?php if($action == "admin_index"){ ?>
 <script type="text/javascript">
    Controllers.push("Indicatorvalues.index");
</script>
<?php } ?>
