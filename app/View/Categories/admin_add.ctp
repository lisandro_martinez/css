<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">


        <?php
          echo $this->Form->create('Category', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
        ?>
<?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-6">
            <?php
              if($action == "admin_edit"){echo $this->Form->input('Category.id');}
            ?>
						   <label for="name">Nombre del Categoria (ESP)*</label>
						   <input type="text" class="form-control" name="data[Category][name][esp]" id="name" value="<?php echo isset($this->data['Category']['name']['esp']) ? $this->data['Category']['name']['esp'] : '';?>">

          </div>
          <?php else : ?>
          <div class="form-group col-lg-6">
            <?php
              if($action == "admin_edit"){echo $this->Form->input('Category.id');}
            ?>
          </div>
          <?php endif; ?>
<?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-lg-6">
						   <label for="name">Nombre del Categoria (ENG)*</label>
						   <input type="text" class="form-control" name="data[Category][name][eng]" id="name" value="<?php echo isset($this->data['Category']['name']['eng']) ? $this->data['Category']['name']['eng'] : '';?>">
          </div>
          <?php endif; ?>
		  
        <div class="form-group col-lg-6">
             <label for="name">Modulo Asociado *</label>
             <?php echo $this->Form->input('module_id',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>"Seleccione","type"=>"select","required"=>false)); ?>
        </div>

          <div class="form-group col-lg-6">
						   <label for="name">Orden en el Menu *</label>
						   <input type="text" class="form-control" name="data[Category][order]" id="name" value="<?php echo isset($this->data['Category']['order']) ? $this->data['Category']['order'] : '99';?>">
		  </div>



          <div class="form-group form_button">

                <div class="form-group">
                  <label for="note">Nota: Todos los campos con * son obligatorios</label>
                </div>
								<button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

					</div>
          <div class="form-group form_response">
            <div id="responseForm"></div>
          </div>

      </form>


  </div>
</div>



<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Categories.add");
</script>
<?php } ?>
