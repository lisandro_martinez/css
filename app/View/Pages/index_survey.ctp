<script>
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function Comentar() {

	$("#div_name").hide();
	$("#div_email").hide();
	$("#div_email_format").hide();
	$("#div_card").hide();
	$("#div_coment").hide();
	
	if ($("#name").val()=="") $("#div_name").fadeIn("slow");
	if ($("#email").val()=="") $("#div_email").fadeIn("slow");
	if (!isEmail($("#email").val())) $("#div_email_format").fadeIn("slow");
	if ($("#card").val()=="") $("#div_card").fadeIn("slow");
	if ($("#coment").val()=="") $("#div_coment").fadeIn("slow");

	if ($("#name").val()!="" && $("#email").val()!="" && isEmail($("#email").val()) && $("#card").val()!="" && $("#coment").val()!="") {
	
		$("#el_form").submit();
	
	}
	
	return false;
	
}
</script>
<div class="row">
			
	<div class="col-md-12">

	<?php	
	echo $this->element('company', array('la_comp'=>$la_comp));
	?>
	
		<div class="col-md-8">
					
			<div class="widget stacked">
					
				<div class="widget-header">
					<i class="icon-envelope"></i>
					<h3><?php echo __("Survey");?></h3>
				</div> <!-- /widget-header -->
				
				<div class="widget-content">
					
					<h3><?php echo $la_encuesta[0]["Node"]["name"];?></h3>
					<p><?php echo $la_encuesta[0]["Node"]["description"];?></p>
					
					<form id=el_form method=post>
					
					<?php
					echo "<input type=hidden name=\"data[Surveyanswer][node_id]\" value=\"".$la_encuesta[0]["Node"]["id"]."\">";
					?>
					
					<!-- DATOS PERSONALES -->
					
					<div class="input-group" style="margin: 10px">
						<span class="input-group-addon"><i class="icon-user"></i></span>
						<input id=name name="data[Surveyanswer][name]" type="text" placeholder="<?php echo __("Write your name");?>" class="form-control">
					</div>					
					<div class="alert alert-danger alert-dismissable" style="display: none" id=div_name>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<strong>Ooops</strong> <?php echo __("This field is mandatory");?>
					</div>
					
					<div class="input-group" style="margin: 10px">
						<span class="input-group-addon">@</span>
						<input id=email name="data[Surveyanswer][email]" type="text" placeholder="<?php echo __("Write your email");?>" class="form-control">
					</div>					
					<div class="alert alert-danger alert-dismissable" style="display: none" id=div_email>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<strong>Ooops</strong> <?php echo __("This field is mandatory");?>
					</div>
					<div class="alert alert-danger alert-dismissable" style="display: none" id=div_email_format>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<strong>Ooops</strong> <?php echo __("Please write a valid email");?>
					</div>
					
					<div class="input-group" style="margin: 10px">
						<span class="input-group-addon"><i class="icon-credit-card"></i></span>
						<input id=card name="data[Surveyanswer][idcard]" type="text" placeholder="<?php echo __("Write your id card");?>" class="form-control">
					</div>					
					<div class="alert alert-danger alert-dismissable" style="display: none" id=div_card>
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<strong>Ooops</strong> <?php echo __("This field is mandatory");?>
					</div>
					
					<!-- PREGUNTAS -->
					
					<div style="margin: 10px">
					<?php
					if (is_array($las_preguntas)) {
					
						$i=0;
						
						foreach($las_preguntas as $p) {
							$i++;
							echo "<hr><h4><i class=icon-asterisk></i> ".$p["Surveyquestion"]["name"]."</h4>";
						
							// $question_types = array(1=>"Selección Simple", 2=>"Selección Múltiple", 3=>"Texto");
							switch ($p["Surveyquestion"]["question_type"]) {
							case 1: // selección simple
								$answers=explode(PHP_EOL, $p["Surveyquestion"]["answers"]);
								if (is_array($answers) and sizeof($answers)) {
									foreach($answers as $ia=>$a) {
										if ($a) echo "<p style=\"margin-left: 20px;\"><label><input type=radio name=\"data[Surveyanswer][data][".$p["Surveyquestion"]["id"]."]\" value=\"$ia\"> $a</label></p>";
									}
								}
								break;
							case 2: // selección múltiple
								$answers=explode(PHP_EOL, $p["Surveyquestion"]["answers"]);
								if (is_array($answers) and sizeof($answers)) {
									foreach($answers as $ia=>$a) {
										if ($a) echo "<p style=\"margin-left: 20px;\"><label><input type=checkbox name=\"data[Surveyanswer][data][".$p["Surveyquestion"]["id"]."][]\" value=\"$ia\"> $a</label></p>";
									}
								}
								break;
							case 3: // texto
								echo "<p><textarea 	id=q_".$p["Surveyquestion"]["id"]." 
													name=\"data[Surveyanswer][data][".$p["Surveyquestion"]["id"]."]\" 
													class=\"form-control\" 
													placeholder=\"".__("Write your answer here")."\"></textarea></p>";
								?>
								<div class="alert alert-danger alert-dismissable" style="display: none" id=div_answer_<?php echo $p["Surveyquestion"]["id"];?>>
									<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
									<strong>Ooops</strong> <?php echo __("This answer is mandatory");?>
								</div>
								<?php
								break;
							}
						}
					}
					?>
					</div>
				
					<div class="login-actions">
					<button class="login-action btn btn-primary" onclick="return Comentar();"><?php echo __("Continue");?></button>
					</div>
			</form>
				
					<hr>
					<p><i class="icon-home"></i> <a href="/<?php echo $la_comp["Company"]["slug"];?>"><?php echo __("Return to Home Page");?></a></p>
					
				</div> <!-- /widget-content -->
							
			</div>

		</div>

	</div>


</div>

