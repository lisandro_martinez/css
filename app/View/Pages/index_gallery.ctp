<!-- load Galleria -->
<style>
	#galleria{height:400px; max-width:900px; width: 100%;}
</style>
<script src="/js/galleria/galleria-1.4.2.min.js"></script>


<div class="row">
			
	<div class="col-md-12">

	<?php	
	echo $this->element('company', array('la_comp'=>$la_comp));
	?>
	
		<div class="col-md-8">
					
			<div class="widget stacked">
					
				<div class="widget-header">
					<i class="icon-picture"></i>
					<h3><?php echo __("Image Galleries") . ": " . $la_galeria[0]["Node"]["name"];?></h3>
				</div> <!-- /widget-header -->
				
				<div class="widget-content">
					
					<div id="galleria">

					<?php
					if (isset($la_galeria) and is_array($la_galeria)) {
					
						foreach($la_galeria[0]["Imgs"]["imgs"] as $img) {
							?>
								<img 
									src="<?php echo $la_galeria[0]["Imgs"]["dir"]."/".$img["file"]; ?>"
									data-title="<?php echo $img["desc"][$this->Session->read('Config.MyLangVar')];?>"
									data-description="<?php echo $la_galeria[0]["Node"]["name"];?>"
								>
							<?php
						}
					
					}
					?>
				
					</div>
					<?php
					if (isset($la_galeria) and is_array($la_galeria)) {
						if (isset($la_galeria[0]["Node"]["description"])) {
							echo "<p>".$la_galeria[0]["Node"]["description"]."</p>";
						}
					}
					?>
					<hr>
					<p><i class="icon-home"></i> <a href="/<?php echo $la_comp["Company"]["slug"];?>"><?php echo __("Return to Home Page");?></a></p>
					
				</div> <!-- /widget-content -->
							
			</div>

		</div>

	</div>


</div>

<script>
// Load the Azur theme
Galleria.loadTheme('/js/galleria/themes/azur/galleria.azur.min.js');
// Initialize Galleria
Galleria.run('#galleria', { trueFullscreen: true, _toggleInfo: false});
</script>
