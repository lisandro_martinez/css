<div class="row">
			
	<div class="col-md-12">

	<?php	
	echo $this->element('company', array('la_comp'=>$la_comp));
	?>

	</div>
	
	<div class="col-md-12">
				
		<div class="widget stacked">
				
			<div class="widget-header">
				<i class="icon-picture"></i>
				<h3><?php echo __("Image Galleries");?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				
				<?php
				
				if (isset($galerias) and is_array($galerias)) {
					foreach($galerias as $g) {
					
						echo '<div class="col-md-4">';
						echo "<p><a href=\"/".$la_comp["Company"]["slug"]."/gallery/".$g["Node"]["id"]."\">";
						echo "<p>";
						if (isset($g["Imgs"]["imgs"][0]["file"])) {
							echo "<img src=\"".$g["Imgs"]["dir"]."/".$g["Imgs"]["imgs"][0]["file"]."\" style=\"width: 100%\"></p>";
						}
						echo $g["Node"]["name"]."</a></p>";
						echo '</div>';
						
					}
				}
				
				?>
			
			</div> <!-- /widget-content -->
						
		</div>

	</div>

	<div class="col-md-12">
				
		<div class="widget stacked">
				
			<div class="widget-header">
				<i class="icon-inbox"></i>
				<h3><?php echo __("Suggestions mailbox");?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				
				<?php
				
				if (isset($mailboxes) and is_array($mailboxes)) {
				
					foreach($mailboxes as $g) {
					
						echo "<h3><i class=\"icon-envelope\"></i> ".$g["Node"]["name"]."</a></h3>";

						if (isset($g["ultimo_mensaje"]["Mailboxsuggestion"])) {
						
							$in = $g["ultimo_mensaje"]["Mailboxsuggestion"]["comment"];
						
							$out = strlen($in) > 150 ? substr($in,0,150)."..." : $in;
						
							$user = "<b><i class=\"icon-user\"></i> ".$g["ultimo_mensaje"]["Mailboxsuggestion"]["name"]."</b>";
						
							echo "<div style=\"margin-left: 40px\"><p class=\"pre\">$user<br>\"".$out."</p></div>"; 
						}

						echo "<p><i class=\"icon-chevron-right\"></i> <a href=\"/".$la_comp["Company"]["slug"]."/mailboxread/".$g["Node"]["id"]."\">";
						echo __("See messages")."</a></p>";
						
						echo "<p><i class=\"icon-pencil\"></i> <a href=\"/".$la_comp["Company"]["slug"]."/mailboxwrite/".$g["Node"]["id"]."\">";
						echo __("Leave a message")."</a></p>";
						
					}
				
				}
				
				?>
			
			</div> <!-- /widget-content -->
						
		</div>

	</div>

	<div class="col-md-12">
				
		<div class="widget stacked">
				
			<div class="widget-header">
				<i class="icon-th-list"></i>
				<h3><?php echo __("Surveys");?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				
				<?php
				
				if (isset($surveys) and is_array($surveys)) {
				
					foreach($surveys as $g) {
					
						echo "<h3><i class=\"icon-list-alt\"></i> ".$g["Node"]["name"]."</a></h3>";

						echo "<p><i class=\"icon-pencil\"></i> <a href=\"/".$la_comp["Company"]["slug"]."/survey/".$g["Node"]["id"]."\">";
						echo __("Fill in the survey")."</a></p>";
						
					}
				
				}
				
				?>
			
			</div> <!-- /widget-content -->
						
		</div>

	</div>

	<div class="col-md-12">
				
		<div class="widget stacked">
				
			<div class="widget-header">
				<i class="icon-th-list"></i>
				<h3><?php echo __("Graphs");?></h3>
			</div> <!-- /widget-header -->
			
			<div class="widget-content">
				
				<?php
				
				if (isset($graphs) and is_array($graphs)) {
				
					foreach($graphs as $g) {
					
						echo "<h3><i class=\"icon-signal\"></i> ".$g["Node"]["name"]."</a></h3>";
						echo "<p>".$g["Node"]["description"]."</a></h3>";

						echo "<p style=\"text-align: center;\"><img src=\"/pchart/Example".$g["Graphdata"]["type"].".php?g=".$g["Node"]["id"]."&lang=".$this->Session->read('Config.MyLangVar')."\" style=\"max-width: 100%;\"></p>";
						
						echo "<hr>";
						
					}
				
				}
				
				?>
			
			</div> <!-- /widget-content -->
						
		</div>

	</div>

</div>