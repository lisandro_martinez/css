<div class="row">
			
	<div class="col-md-12">

	<?php	
	echo $this->element('company', array('la_comp'=>$la_comp));
	?>
	
		<div class="col-md-8">
					
			<div class="widget stacked">
					
				<div class="widget-header">
					<i class="icon-envelope"></i>
					<h3><?php echo $el_buzon[0]["Node"]["name"];?></h3>
				</div> <!-- /widget-header -->
				
				<div class="widget-content">
					
					<?php

						if (is_array($los_mensajes) and sizeof($los_mensajes)) {
							foreach($los_mensajes as $m) { ?> 
								<div class="col-md-2">
									<p style="font-size: x-small"><i class="icon-calendar"></i><br><?php echo date('d/m/Y H:i', strtotime($m["Mailboxsuggestion"]["datetime"]));?></p>
									<img src="/img/quote.png" style="width: 100%">
								</div><div class="col-md-10"> 
									<h3><i class="icon-user"></i> <?php echo $m["Mailboxsuggestion"]["name"] . " " . __("wrote:");?></h3>
								<?php
									echo "<p class=\"pre\">".$m["Mailboxsuggestion"]["comment"]."</p>";
								?> 
								
								</div> 
								<div style="clear: both"></div>
												
								<?php
							}
						} else {
							echo __("No messages found, be the first to write a message");
						}
					
					?>
					
					<hr>
					<p><i class="icon-pencil"></i> <a href="/kerns/mailboxwrite/<?php echo $el_buzon[0]["Node"]["id"];?>"><?php echo __("Leave a message");?></a></p>
				
					<hr>
					<p><i class="icon-home"></i> <a href="/<?php echo $la_comp["Company"]["slug"];?>"><?php echo __("Return to Home Page");?></a></p>
					
				</div> <!-- /widget-content -->
							
			</div>

		</div>

	</div>


</div>

