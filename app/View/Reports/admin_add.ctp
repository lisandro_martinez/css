<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">


        <?php
          echo $this->Form->create('Report', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
        ?>


		<?php
		if($action == "admin_edit"){echo $this->Form->input('Report.id',array('value'=>$id));}
		?>

		<!-- Name -->

			<!-- ESP -->
			
			<?php if (! $this->Session->check('Config.LangVar') or ($this->Session->check('Config.LangVar') and $this->Session->read('Config.LangVar') == 'esp') ) { ?>

				<div class="form-group col-lg-6">
					<label for="name"><?php echo __("Report name");?>*</label>
					<input 	type="text" class="form-control" name="data[Report][name][esp]" id="name" 
							value="<?php echo isset($this->data['Report']['name']['esp']) ? $this->data['Report']['name']['esp'] : '';?>">
				</div>

			<?php } ?>

			<!-- ENG -->
			
			<?php if (! $this->Session->check('Config.LangVar') or ($this->Session->check('Config.LangVar') and $this->Session->read('Config.LangVar') == 'eng') ) { ?>
			
				<div class="form-group col-lg-6">
					<label for="name"><?php echo __("Report name");?> (ENG)*</label>
					<input 	type="text" class="form-control" name="data[Report][name][eng]" id="name" 
							value="<?php echo isset($this->data['Report']['name']['eng']) ? $this->data['Report']['name']['eng'] : '';?>">
				</div>
			
			<?php } ?>

		<!-- /Name -->
		
		<!-- Description -->
		
			<!-- ESP -->
			
			<?php if (! $this->Session->check('Config.LangVar') or ($this->Session->check('Config.LangVar') and $this->Session->read('Config.LangVar') == 'esp') ) { ?>

				<div class="form-group col-lg-12 textareafield">
					<label for="name"><?php echo __("Description");?></label>
					<textarea class="summernote" name="data[Report][description][esp]" id="name"><?php
						echo isset($this->data['Report']['description']['esp']) ? $this->data['Report']['description']['esp'] : '';
					?></textarea>
				</div>

			<?php } ?>

			<!-- ENG -->
			
			<?php if (! $this->Session->check('Config.LangVar') or ($this->Session->check('Config.LangVar') and $this->Session->read('Config.LangVar') == 'eng') ) { ?>
			
				<div class="form-group col-lg-12 textareafield">
					<label for="name"><?php echo __("Description");?> (ENG)</label>
					<textarea class="summernote" name="data[Report][description][eng]" id="name"><?php
						echo isset($this->data['Report']['description']['eng']) ? $this->data['Report']['description']['eng'] : '';
					?></textarea>
				</div>
			
			<?php } ?>

		<!-- /Description -->

		<!-- Year -->

			<!-- ESP -->
			
			<?php if (! $this->Session->check('Config.LangVar') or ($this->Session->check('Config.LangVar') and $this->Session->read('Config.LangVar') == 'esp') ) { ?>

				<div class="form-group col-lg-6">
					<label for="name"><?php echo __("Year");?>*</label>
					<input 	type="text" class="form-control" name="data[Report][year][esp]" id="name" 
							value="<?php echo isset($this->data['Report']['year']['esp']) ? $this->data['Report']['year']['esp'] : '';?>">
				</div>

			<?php } ?>

			<!-- ENG -->
			
			<?php if (! $this->Session->check('Config.LangVar') or ($this->Session->check('Config.LangVar') and $this->Session->read('Config.LangVar') == 'eng') ) { ?>
			
				<div class="form-group col-lg-6">
					<label for="name"><?php echo __("Year");?> (ENG)*</label>
					<input 	type="text" class="form-control" name="data[Report][year][eng]" id="name" 
							value="<?php echo isset($this->data['Report']['year']['eng']) ? $this->data['Report']['year']['eng'] : '';?>">
				</div>
			
			<?php } ?>

		<!-- /Year -->



          <div class="form-group form_button">

            <div class="form-group">
              <label for="note">Nota: Todos los campos con * son obligatorios</label>
            </div>

			<button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

		</div>

          <div class="form-group form_response">
            <div id="responseForm"></div>
          </div>

      </form>


  </div>
</div>



<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Reports.add");
</script>
<?php } ?>
