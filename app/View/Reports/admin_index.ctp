<?php
    $headerstitles = array(
                            'Report.id' => '#',
                            'Report.name' => __("Name"),
                            'Report.year' => __("Year"),
                            'label' => __("Pages"));

?>

<div class="widget stacked">
    <?php  $this->Viewbase->panel_title(__('Reports'));   ?>

    <div class="widget-content">
         <?php include_once('filter.ctp');
            $this->Viewbase->Multi_form_create($this->form,'#');
           ?>
        <table class="table table-bordered">
            <?php
                // Encabezado de la tabla
                $this->Viewbase->table_Header($this->Paginator,$headerstitles);
            ?>
            <tbody>
                <?php foreach ($lists as $list): ?>
                <tr>
                        <?php
                        // Campo check de cada linea
                        $datarow=array(
                            'idModel' => $list['Report']['id'],
                            'textname' => $list['Report']['name'],
                            'inputname'=> 'data[Report][id][]',
                            'value' => $list['Report']['id']
                        );
                        $this->Viewbase->Multi_check_row($datarow);
                        // Campo check de cada linea
                    ?>

                    <td style="width: 10px;"><?php echo h($list['Report']['id']); ?>&nbsp;</td>
                    <td><?php echo h($list['Report']['name']); ?>&nbsp;</td>
                    <td><?php echo h($list['Report']['year']); ?>&nbsp;</td>
                    <td><?php echo '<a href="/admin/reportpages/?report_id='.$list['Report']['id'].'">'.__('Edit').' '.__('Pages').'</a></td>';?>


                     <td class="actions">

                        <?php


                        $databutton_edit = array(
                                'url'=> '/admin/reports/edit/'.$list['Report']['id']
                            );
                            $this->Viewbase->button_edit($this->Html,$databutton_edit);



                        $databutton_delete = array(
                                'url'=> '/admin/reports/delete/'.$list['Report']['id'],
                                'idModel' =>$list['Report']['id'],
                                'name' =>$list['Report']['name']
                            );
                            $this->Viewbase->button_delete($this->Html,$databutton_delete);


						 $databutton_generate = array(
								'url'=> '/admin/reports/generate/'.$list['Report']['id'],
								'idModel' =>$list['Reportpage']['id'],
								'name'    =>$list['Reportpage']['name']);
							$this->Viewbase->button_generate($this->Html,$databutton_generate);

						 $databutton_generate = array(
								'url'=> $list['Report']['url'],
								'idModel' =>$list['Reportpage']['id'],
								'name'    =>$list['Reportpage']['name']);
							$this->Viewbase->button_download($this->Html,$databutton_generate);



                        ?>
                    </td>

        	   </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="6">
                        <?php

                            $databutton_add = array(
                                    'url'=> '/admin/reports/add/');
                                $this->Viewbase->button_add($this->Html,$databutton_add);

                        ?>
                    </td>
                </tr>
            </tbody>

        </table>
        <div class="checkalldiv col-md-6">
            <script>
                checkalltext = {
                    'empty' : {
                        'title' : 'Advertencia',
                        'text' : 'Debe seleccionar al menos un módulo para utilizar la opción sobre multiples registros'
                    },
                    'deleteall' :{
                        'title' : 'Confirmación para eliminar multiples registros',
                        'url' : '/admin/reports/deletemulti/',
                        'pretext' : 'Estas seguro que deseas eliminar los siguientes registros?'
                    }
                };
            </script>
            <input type="checkbox" class="checkallclick" title="Check All">
            <label for="checkall"><?php echo __('Check All'); ?></label>
            <select id="selectmulti" name="submit_mult" class="autosubmit" style="margin-left:10px;">
                <option value="0" selected="selected"><?php echo __('With selected:'); ?></option>
                <option value="deleteall"><?php echo __('Delete'); ?></option>
            </select>
        </div>
        <?php echo $this->element('paginado'); ?>
        <?php echo $this->Form->end(); ?>

    </div>
</div>

</div>
 <?php if($action == "admin_index"){ ?>
 <script type="text/javascript">
    Controllers.push("Reports.index");
</script>
<?php } ?>
