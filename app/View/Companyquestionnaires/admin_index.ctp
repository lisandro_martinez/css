<?php
    $headerstitles = array(
                            'Companyquestionnaire.id' => '#',
                            'Company.name' => __("Companies"),
                            'Questionnaire.name' => __("Questionnaire"));
?>

<div class="widget stacked">
    <?php  $this->Viewbase->panel_title(__("Assign Questionnaires to Companies"));   ?>

    <div class="widget-content">
    <?php
            include_once('filter.ctp');
            $this->Viewbase->Multi_form_create($this->form,'#');
    ?>
<table class="table table-bordered">
    <?php
                // Encabezado de la tabla
                $this->Viewbase->table_Header($this->Paginator,$headerstitles);
    ?>
    <tbody>
        <?php foreach ($lists as $list): ?>
        <tr>
            <?php
                    // Campo check de cada linea
                    $datarow=array(
                        'idModel' => $list['Companyquestionnaire']['id'],
                        'textname' => 	(isset($all_companies[$list['Companyquestionnaire']['company_id']]))?
											$all_companies[$list['Companyquestionnaire']['company_id']]:"".'-'.
										$all_questionnaires[$list['Companyquestionnaire']['questionnaire_id']],
                        'inputname'=> 'data[Companyquestionnaire][id][]',
                        'value' => $list['Companyquestionnaire']['id']
                    );
                    $this->Viewbase->Multi_check_row($datarow);
                    // Campo check de cada linea
                ?>

            <td style="width: 10px;"><?php echo h($list['Companyquestionnaire']['id']); ?>&nbsp;</td>
            <td><?php 
			if (isset($all_companies[$list['Companyquestionnaire']['company_id']]))
				echo $all_companies[$list['Companyquestionnaire']['company_id']]; ?>&nbsp;</td>
            <td><?php 
			if (isset($all_questionnaires[$list['Companyquestionnaire']['questionnaire_id']]))
				echo $all_questionnaires[$list['Companyquestionnaire']['questionnaire_id']]; ?>&nbsp;</td>

                     <td class="actions">

                        <?php

                          $databutton_delete = array(
                                'url'=> '/admin/companyquestionnaires/delete/'.$list['Companyquestionnaire']['id'],
                                'idModel' =>$list['Companyquestionnaire']['id'],
                                'name' =>$list['Companyquestionnaire']['id']
                            );
                            $this->Viewbase->button_delete($this->Html,$databutton_delete);

                        ?>
                    </td>

	   </tr>
        <?php endforeach; ?>
        <tr>
                    <td colspan="5">
                        <?php

                            $databutton_add = array(
                                    'url'=> '/admin/companyquestionnaires/add/');
                                $this->Viewbase->button_add($this->Html,$databutton_add);

                        ?>
                    </td>
                </tr>
    </tbody>

</table>

        <div class="checkalldiv col-md-6">
            <script>
                checkalltext = {
                    'empty' : {
                        'title' : '<?php echo __("Warning");?>',
                        'text' : '<?php echo __("You must select at least one register to use the multi-record option");?>'
                    },
                    'deleteall' :{
                        'title' : '<?php echo __("Multiple register deletion confirmation");?>',
                        'url' : '/admin/companyquestionnaires/deletemulti/',
                        'pretext' : '<?php echo __("Are you sure you want to delete the following registers?");?>'
                    }
                };
            </script>
            <input type="checkbox" class="checkallclick" title="Check All">
            <label for="checkall"><?php echo __('Check All'); ?></label>
            <select id="selectmulti" name="submit_mult" class="autosubmit" style="margin-left:10px;">
                <option value="0" selected="selected"><?php echo __('With selected:'); ?></option>
                <option value="deleteall"><?php echo __('Delete All'); ?></option>
            </select>
        </div>
        <?php echo $this->Form->end(); ?>

<?php echo $this->element('paginado'); ?>
</div>
</div>
<?php if($action == "admin_index"){ ?>
 <script type="text/javascript">
    Controllers.push("Companyquestionnaires.index");
</script>
<?php } ?>
