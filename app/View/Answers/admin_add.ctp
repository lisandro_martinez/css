<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">


        <?php
          echo $this->Form->create('Answer', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
        ?>
<?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-6">
            <?php
              if($action == "admin_edit"){echo $this->Form->input('Answer.id');}
            ?>
						   <label for="name"><?php echo __("Answer Option");?> (ESP)*</label>
						   <input type="text" class="form-control" name="data[Answer][name][esp]" id="name" value="<?php echo isset($this->data['Answer']['name']['esp']) ? $this->data['Answer']['name']['esp'] : '';?>">
		  </div>
			<?php else : ?>
			    <div class="form-group col-lg-6">
            <?php
              if($action == "admin_edit"){echo $this->Form->input('Answer.id');}
            ?>
					</div>
			<?php endif; ?>
<?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-lg-6">
						   <label for="name"><?php echo __("Answer Option");?> (ENG)*</label>
						   <input type="text" class="form-control" name="data[Answer][name][eng]" id="name" value="<?php echo isset($this->data['Answer']['name']['eng']) ? $this->data['Answer']['name']['eng'] : '';?>">
		  </div>
			<?php endif; ?>
<?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-6">
			   <label for="name"><?php echo __("Feedback Title");?> (ESP)*</label>
			   <input type="text" class="form-control" name="data[Answer][feedback_title][esp]" id="name" value="<?php echo isset($this->data['Answer']['feedback_title']['esp']) ? $this->data['Answer']['feedback_title']['esp'] : '';?>">
		  </div>
			<?php endif; ?>
<?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-lg-6">
			   <label for="name"><?php echo __("Feedback Title");?> (ENG)*</label>
			   <input type="text" class="form-control" name="data[Answer][feedback_title][eng]" id="name" value="<?php echo isset($this->data['Answer']['feedback_title']['eng']) ? $this->data['Answer']['feedback_title']['eng'] : '';?>">
		  </div>
			<?php endif; ?>

		  <?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-12">
			   <label for="name"><?php echo __("Feedback");?> (ESP)*</label>
			   <textarea class="summernote" name="data[Answer][feedback][esp]" id="name"><?php echo isset($this->data['Answer']['feedback']['esp']) ? $this->data['Answer']['feedback']['esp'] : '';?></textarea>
		  </div>
			<?php endif; ?>
			<?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'eng') ) : ?>

          <div class="form-group col-lg-12">
			   <label for="name"><?php echo __("Feedback");?> (ENG)*</label>
			   <textarea class="summernote" name="data[Answer][feedback][eng]" id="name"><?php echo isset($this->data['Answer']['feedback']['eng']) ? $this->data['Answer']['feedback']['eng'] : '';?></textarea>
		  </div>
			<?php endif; ?>

		  
          <div class="form-group col-lg-6">
			   <label for="name"><?php echo __("Points");?> *</label>
			   <input type="text" class="form-control" name="data[Answer][points]" id="name" value="<?php echo isset($this->data['Answer']['points']) ? $this->data['Answer']['points'] : '';?>">
		  </div>

          <div class="form-group col-lg-6">
			   <label for="name"><?php echo __("Order");?> *</label>
			   <input type="text" class="form-control" name="data[Answer][order]" id="name" value="<?php echo isset($this->data['Answer']['order']) ? $this->data['Answer']['order'] : '';?>">
		  </div>

		  <div class="form-group col-lg-6">
			 <label for="name"><?php echo __("Associated Question");?></label>
			 <?php echo $this->Form->input('question_id',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>__("Choose"),"type"=>"select","required"=>false)); ?>

		</div>
		  
          <div class="form-group form_button">

                <div class="form-group">
                  <label for="note"><?php echo __("All fields with * are mandatory");?></label>
                </div>
								<button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

					</div>
          <div class="form-group form_response">
            <div id="responseForm"></div>
          </div>

      </form>


  </div>
</div>



<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Answers.add");
</script>
<?php } ?>
