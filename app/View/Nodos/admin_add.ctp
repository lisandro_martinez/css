<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">


        <?php
          echo $this->Form->create('Nodo', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
        ?>
        <?php
              if($action == "admin_edit"){echo $this->Form->input('Nodo.id',array('value'=>$id));}
            ?>
        <?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-6">
            
						   <label for="name"><?php echo __("Name");?> (ESP)*</label>
						   <input type="text" class="form-control" name="data[Nodo][title][esp]" id="title" value="<?php echo isset($this->data['Nodo']['title']['esp']) ? $this->data['Nodo']['title']['esp'] : '';?>">
		</div>
    <?php endif; ?>
<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-lg-6">
						   <label for="name"><?php echo __("Name");?> (ENG)*</label>
						   <input type="text" class="form-control" name="data[Nodo][title][eng]" id="title" value="<?php echo isset($this->data['Nodo']['title']['eng']) ? $this->data['Nodo']['title']['eng'] : '';?>">
		</div>
    <?php endif; ?>
    <?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-12 textareafield">
						   <label for="name"><?php echo __("Description");?> (ESP)</label>
						   <textarea class="form-control" name="data[Nodo][description][esp]" id="name"><?php echo isset($this->data['Nodo']['description']['esp']) ? $this->data['Nodo']['description']['esp'] : '';?></textarea>
		  </div>
      <?php endif; ?>
      <?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-lg-12 textareafield">
						   <label for="name"><?php echo __("Description");?> (ENG)</label>
						   <textarea class="form-control" name="data[Nodo][description][eng]" id="name"><?php echo isset($this->data['Nodo']['description']['eng']) ? $this->data['Nodo']['description']['eng'] : '';?></textarea>
		  </div>
      <?php endif; ?>

        <div class="form-group col-lg-6">
             <label for="name">Tipo de entidad *</label>
             <?php 
			 echo $this->Form->input('nodetype_id',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>"Seleccione","type"=>"select","required"=>false)); ?>
        </div>


          <div class="form-group form_button">

            <div class="form-group">
              <label for="note">Nota: Todos los campos con * son obligatorios</label>
            </div>

								<button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

					</div>

          <div class="form-group form_response">
            <div id="responseForm"></div>
          </div>

      </form>


  </div>
</div>



<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Nodos.add");
</script>
<?php } ?>
