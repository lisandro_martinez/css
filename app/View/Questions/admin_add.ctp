<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">


        <?php
          echo $this->Form->create('Question', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
        ?>
        <?php
              if($action == "admin_edit"){echo $this->Form->input('Question.id');}
            ?>
<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
          <div class="form-group col-lg-6">
            
						   <label for="name"><?php echo __("Question name");?> (ESP)*</label>
						   <input type="text" class="form-control" name="data[Question][name][esp]" id="name" value="<?php echo isset($this->data['Question']['name']['esp']) ? $this->data['Question']['name']['esp'] : '';?>">
		</div>
    <?php endif; ?>
<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
          <div class="form-group col-lg-6">
						   <label for="name"><?php echo __("Question name");?> (ENG)*</label>
						   <input type="text" class="form-control" name="data[Question][name][eng]" id="name" value="<?php echo isset($this->data['Question']['name']['eng']) ? $this->data['Question']['name']['eng'] : '';?>">
		</div>
    <?php endif; ?>

          <div class="form-group col-lg-6">
			   <label for="name"><?php echo __("Order");?> *</label>
			   <input type="text" class="form-control" name="data[Question][order]" id="name" value="<?php echo isset($this->data['Question']['order']) ? $this->data['Question']['order'] : '';?>">
		  </div>

		<div class="form-group col-lg-6">
			 <label for="name"><?php echo __("Associated Section");?></label>
			 <?php echo $this->Form->input('section_id',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>__("Choose"),"type"=>"select","required"=>false)); ?>

		</div>
		  
          <div class="form-group form_button">

                <div class="form-group">
                  <label for="note"><?php echo __("All fields with * are mandatory");?></label>
                </div>
								<button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

					</div>
          <div class="form-group form_response">
            <div id="responseForm"></div>
          </div>

      </form>


  </div>
</div>



<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Questions.add");
</script>
<?php } ?>
