<?php

    $headerstitles =  array(
                        'Action.id' => '#',
                        'Category.name' => 'Categoría',
                        'Action.name' => 'Nombre',
                        'Action.url' => 'Url',
                        'Action.order' => 'Orden',
                        'Module.name' => 'Modulo'
                    );


?>
<div class="widget stacked">

    <?php $this->Viewbase->panel_title('Listado de Funciones');?>

    <div class="widget-content">

        <?php
        include_once('filters.ctp');
        $this->Viewbase->Multi_form_create($this->form,'#');
        ?>
        <table class="table table-bordered">

            <?php
                // Encabezado de la tabla
                $this->Viewbase->table_Header($this->Paginator,$headerstitles);
            ?>

            <tbody>
                <?php foreach ($lists as $list): ?>

                <tr>
                    <?php
                        // Campo check de cada linea
                        $datarow=array(
                            'idModel' => $list['Action']['id'],
                            'textname' => $list['Action']['name'],
                            'inputname'=> 'data[Action][id][]',
                            'value' => $list['Action']['id']
                        );
                        $this->Viewbase->Multi_check_row($datarow);
                        // Campo check de cada linea
                    ?>

                    <td style="width: 10px;"><?php echo h($list['Action']['id']); ?>&nbsp;</td>
                    <td><?php echo h($list['Categories']['name']); ?>&nbsp;</td>
                    <td><?php echo h($list['Action']['name']); ?>&nbsp;</td>
                    <td><?php echo h($list['Action']['url']); ?>&nbsp;</td>
                    <td><?php echo h($list['Action']['order']); ?>&nbsp;</td>
                    <td><?php echo h($list['Categories']['module_id']); ?>&nbsp;</td>


                     <td class="actions">

                        <?php


                            $databutton_edit = array(
                                'url'=> '/admin/actions/edit/'.$list['Action']['id']
                            );
                            $this->Viewbase->button_edit($this->Html,$databutton_edit);

                            $databutton_delete = array(
                                'url'=> '/admin/actions/delete/'.$list['Action']['id'],
                                'idModel' =>$list['Action']['id'],
                                'name'    =>$list['Action']['name']

                            );
                            $this->Viewbase->button_delete($this->Html,$databutton_delete);


                        ?>
                    </td>

        	   </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="8">
                        <?php

                            $databutton_add = array(
                                    'url'=> '/admin/actions/add/');
                                $this->Viewbase->button_add($this->Html,$databutton_add);

                        ?>
                    </td>
                </tr>
            </tbody>

        </table>

        <div class="checkalldiv col-md-6">
            <script>
                checkalltext = {
                    'empty' : {
                        'title' : 'Advertencia',
                        'text' : 'Debe seleccionar al menos una función para utilizar la opción sobre multiples registros'
                    },
                    'deleteall' :{
                        'title' : 'Confirmación para eliminar multiples registros',
                        'url' : '/admin/actions/deletemulti/',
                        'pretext' : 'Estas seguro que deseas eliminar los siguientes registros?'
                    }
                };
            </script>
            <input type="checkbox" class="checkallclick" title="Check All">
            <label for="checkall"><?php echo __("Check All"); ?></label>
            <select id="selectmulti" name="submit_mult" class="autosubmit" style="margin-left:10px;">
                <option value="0" selected="selected"><?php echo __("With selected:"); ?></option>
                <option value="deleteall"><?php echo __("Delete All"); ?></option>
            </select>
        </div>
        <?php echo $this->Form->end(); ?>

        <?php echo $this->element('paginado'); ?>
    </div>
</div>
<?php if($action == "admin_index"){ ?>
 <script type="text/javascript">
    Controllers.push("Actions.index");
</script>
<?php } ?>
