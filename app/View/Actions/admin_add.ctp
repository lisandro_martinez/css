<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo $form_config["title"]; ?></h3>
  </div>
  <div class="widget-content">
      <?php
      echo $this->Form->create('Action', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
      ?>
      
    <?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
      	<div class="form-group col-lg-6">
        <?php if($action == "admin_edit"){echo $this->Form->input('id');} ?>
      <?php      
      	echo $this->Form->input('Action.name.esp',array("class"=>"form-control","label"=>"Nombre de Función (ESP)*","required"=>false));
      	?>
      </div>
      <?php else: ?>
      	<div class="form-group col-lg-6">
        <?php if($action == "admin_edit"){echo $this->Form->input('id');} ?>
        </div>
      <?php endif; ?>
    <?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
      	<div class="form-group col-lg-6">
      <?php
      	echo $this->Form->input('Action.name.eng',array("class"=>"form-control","label"=>"Nombre de Función (ENG)*","required"=>false));
      	?>
      </div>
      <?php endif; ?>


      	<div class="form-group col-lg-6">

      <?php echo $this->Form->input('Action.url',array("class"=>"form-control","label"=>"Url de Función *","required"=>false));?>

      	</div>



      	<div class="form-group col-lg-6">
      <?php

      echo $this->Form->input('Action.order',array("class"=>"form-control","label"=>"Orden en el menú *","required"=>false,"value"=> (isset($this->data['Action']['order']) ? $this->data['Action']['order'] : '99')));?>
      	</div>

      	<div class="form-group col-lg-6">
      <?php echo $this->Form->input('Action.category_id',array("class"=>"form-control","label"=>"Categorías *","empty"=>"Seleccione","type"=>"select","required"=>false));
      ;?>
      	</div>




        <div class="form-group form_button">

              <div class="form-group">
                <label for="note">Nota: Todos los campos con * son obligatorios</label>
              </div>
              <button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

        </div>


        <div class="form-group form_response">
          <div id="responseForm"></div>
        </div>

    </form>



</div>
</div>
<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Actions.add");
</script>
<?php } ?>
