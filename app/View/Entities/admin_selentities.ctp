<script>
function addRelation(i) {
	<?php if ($predecessor) {?>
		document.location.href="/admin/entities/addpredecessor/"+i+"/"+$("#elid").val()+"/<?php echo $redir;?>";
	<?php } else {?>
		document.location.href="/admin/entities/addentityrel/"+i+"/"+$("#elid").val()+"/<?php echo $redir;?>";
	<?php }?>
}
</script>
<?php
if (is_array($allnodes) and sizeof($allnodes)) {

	echo "<p><select class=\"form-control\" id=\"elid\">";
	echo "<option value=0>".__("Choose")."</option>";
	foreach($allnodes as $n) {
		echo "<option value=".$n["Node"]["id"].">".$n["Node"]["name"]."</option>";
	}
	echo "</select></p>";
	
	?>
	<p style="text-align: right">
		<a href="javascript:void(0);" class="btn btn-primary noload" onclick="addRelation(<?php echo $entity_id;?>);">
			<?php 
				if ($predecessor) {
					echo __("Add Predecessor");
				} else {
					echo __("Add Relation");
				}
			?>
		</a>
	</p>
	<?php
	
} else {

	echo __("There are no entities of this type");

} 
?>