<?php
echo $this->element('sm', array('panel_title'=>__("BSC Perspective"),
								'tab_content_title'=>__("Please choose the BSC Perspective to see the details"),
								'help'=>__("HELP-INVENTORY-2"),
								'nodetype_id'=>Node::TYPE_PERSPECTIVE,
								'nodetype_add_text'=>__("New BSC Perspective"),
								'redir'=>$redir
								)
					);
?>
