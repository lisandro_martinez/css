<?php
echo $this->element('sm', array('panel_title'=>__("Survey"),
								'tab_content_title'=>__("Please choose the Survey to see the details"),
								'help'=>__("HELP-INVENTORY-2"),
								'nodetype_id'=>Node::TYPE_SURVEY,
								'nodetype_add_text'=>__("Add a Survey"),
								'redir'=>$redir
								)
					);
?>
