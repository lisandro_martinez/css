<link href="/css/dropzone.css" type="text/css" rel="stylesheet" />
<script src="/js/dropzone.js"></script> 
<script>
function laFuncionContinuar() {
	document.getElementById("continuar").style.display="";
}
function Continuar() {
	document.location.href="index2.php?c="+document.getElementById("company_id").value+"&g="+document.getElementById("gallery_id").value;
}
function updateImg(i, img) {
	
	$.ajax({
		type: "POST",
		url: "/admin/nodes/addgallery",
		data: { 
			imgname: img,
			esp: $("#"+i+"_esp").val(),
			eng: $("#"+i+"_eng").val()
		},
		success: function(data, textStatus) {
			$("#result_"+i).hide();
			$("#result_"+i).html('<div class="alert alert-success"><?php echo __("Saved");?></div>');
			$("#result_"+i).fadeIn("slow");
			$("#result_"+i).fadeOut("slow");
		},
		error: function() {
			console.log('Error loading.');
		}
	});
	
	return false;
	
}
function delImg(i, img) {
	
	if (confirm('<?php echo __("Are you sure do you want to delete this?");?>')==true) {
		$.ajax({
			type: "POST",
			url: "/admin/nodes/delgallery",
			data: { 
				imgname: img
			},
			success: function(data, textStatus) {
				document.location.href="/admin/entities/gallery/"+i;
			},
			error: function() {
				console.log('Error loading.');
			}
		});
	}
	
	return false;
	
}
</script>

<div class="col-md-12">
	<div class="widget stacked ">
	  <div class="widget-header">
		<i class="icon-picture"></i>
		<h3><?php echo __("Galleries");?></h3>
	  </div> <!-- /widget-header -->
	  <div class="widget-content">
	  
	  <?php
			echo $this->element('sm', array('panel_title'=>__("Gallery"),
								'tab_content_title'=>__("Please choose the Gallery to see the details"),
								'help'=>__("HELP-INVENTORY-2"),
								'nodetype_id'=>18,
								'nodetype_add_text'=>__("Add a Gallery"),
								'redir'=>$redir,
								'nobox'=>true
								)
					);
	  ?>
	  
		<div id="divupload" style="display: none;">
			<form action="/upload.php" class="dropzone">
			<input type=hidden name=company_id id=company_id value=<?php echo $this->Session->read('TheCompany')["Company"]["id"];?>>
			<input type=hidden name=gallery_id id=gallery_id value=<?php echo $node["Node"]["id"];?>>
			</form>

			<div id=continuar style="display: none;">
			<br>
			<p><a id="ela" class="btn btn-primary" href="/admin/entities/gallery/<?php echo $node["Node"]["id"];?>"><?php echo __("Continue");?></a></p>
			</div>
		</div>
	</div>
  </div>
</div>


<div class="col-md-6">
	<div class="widget stacked ">
	  <div class="widget-header">
		<i class="icon-plus"></i>
		<h3><?php echo __("Add");?></h3>
	  </div> <!-- /widget-header -->
	  <div class="widget-content">
		<p><a href="/admin/nodes/add/18" class="btn btn-primary btn-support-ask"><?php echo __("Add Gallery");?></a></p>
	  </div> <!-- /widget-content -->
	</div> <!-- /widget -->
</div>
