<?php
echo $this->element('sm', array('panel_title'=>__("Courses"),
								'tab_content_title'=>__("Please choose the Course to see the details"),
								'help'=>__("HELP-INVENTORY-2"),
								'nodetype_id'=>14,
								'nodetype_add_text'=>__("New Course"),
								'redir'=>$redir
								)
					);
?>