<div class="col-md-9">

	<div class="widget stacked ">

		<div class="widget-header">
			<i class="icon-plus"></i>
			<h3><?php echo __("Add");?></h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">
		
			<div class="page-header">
				<h3><i class="icon-leaf"></i> <?php echo __("Add Strategic Management Entities");?></h3>
			</div>

			
			
			
			<form method="post">
			
			<div class="form-group">

				<label class="col-md-4"><?php echo __("Type");?></label>

				<div class="col-md-8">

					<?php
					foreach($nodetypes as $nt) {					
					?>
					
						<div class="radio">
						  <label>
							<input type="radio" value="<?php echo $nt["Nodetype"]["id"];?>" name="data[Node][nodetype_id]" value=<?php echo $nt["Nodetype"]["id"];?>
							<?php
							if ($data["Node"]["nodetype_id"]==$nt["Nodetype"]["id"]) {
								echo " checked ";
							}
							?>>
							<?php echo $nt["Nodetype"]["name"];?>
						  </label>
						</div>
						
					<?php
					}
					?>
					
				</div>

			</div>			
			
			
			<div class="form-group">
				<label class="col-md-4"><?php echo __("Name");?> *</label>
				<div class="col-md-8">
				<?php if (! $this->Session->check('Config.LangVar') 
    				or ($this->Session->check('Config.LangVar') 
    				and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
					<p><input type="text" class="form-control" value="" name="data[Node][name][esp]" placeholder="Nombre en español"></p>
				<?php endif; ?>
				<?php if (! $this->Session->check('Config.LangVar') 
				    or ($this->Session->check('Config.LangVar') 
    				and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
					<p><input type="text" class="form-control" value="" name="data[Node][name][eng]" placeholder="Name in english"></p>
				<?php endif; ?>
				</div>
			</div>			

			<div class="form-group">
				<label class="col-md-4"><?php echo __("Description");?></label>
				<div class="col-md-8">
				<?php if (! $this->Session->check('Config.LangVar') 
				    or ($this->Session->check('Config.LangVar') 
    				and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
					<p><textarea rows="3" class="form-control" name="data[Node][description][esp]" placeholder="Descripción en español"></textarea></p>
					<?php endif; ?>
					<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
					<p><textarea rows="3" class="form-control" name="data[Node][description][eng]" placeholder="Description in english"></textarea></p>
					<?php endif; ?>
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-4"><?php echo __("Initial Date");?></label>
				<div class="col-md-8">
					<input type="text"  data-date-format="dd/mm/yyyy" class="form-control" value="" name="data[Node][inidate]" id="datepicker-inline">
				</div>
			</div>			

			<div class="form-group">
				<label class="col-md-4"><?php echo __("End Date");?></label>
				<div class="col-md-8">
					<input type="text"  data-date-format="dd/mm/yyyy" class="form-control" value="" name="data[Node][enddate]" id="datepicker-inline-2">
				</div>
			</div>			

			<div class="form-group">
				<div class="col-md-8" style="margin-top: 10px">
					<p><input type=submit class="btn btn-primary" value="<?php echo __("Add"); ?>"></p>
				</div>
			</div>
			
			</form>
			
		</div> <!-- /widget-content -->

	</div>
	
</div>

<div class="col-md-3">

		<div class="widget stacked widget-box">
			
			<div class="widget-header">	
				<i class="icon-info"></i>
				<h3><?php echo __("Helper"); ?></h3>			
			</div> <!-- /widget-header -->
			
			<div class="well">
				
				<p><?php echo __("help"); ?></p>
				
			</div> <!-- /widget-content -->
			
		</div> <!-- /widget -->


</div>

