<script>
function showEntities() {
	$.ajax({
		type: "POST",
		url: "/admin/entities/getentities",
		data: "nodetype=" + $("#type").val(),
		success: function(data, textStatus) {
			$("#ent").fadeOut(function() {
				$(this).html(data).fadeIn();
			});
		},
		error: function() {
			console.log('Error loading.');
		}
	});
}
</script>

<link rel="stylesheet" href="/css/faq.css">

<div class="col-md-12">

	<div class="widget stacked ">

		<div class="widget-header">
			<i class="icon-star"></i>
			<h3><?php echo __("Wizard");?></h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">
			<div class="col-md-2">
				<div class="numberCircle">9</div>
			</div>
			<div class="col-md-10">			
				<h2><?php echo __("Entities list"); ?></h2>
				<?php foreach($lista as $l) : ?>
					<?php if ($l['nt']['nodetype_id'] != 5) : ?>
						<div class="row-fluid btn btn-info btn-support-ask" style="padding: 0px;">
							<div class="span12">
								<a href="/admin/entities/wizard/step2/<?=$l['nt']['node_id']?>" 
								style='text-decoration:none; color: white;'>
								<?php echo $l['ntype']['name'] . ': ' . $l['node']['name']; ?>  
								</a>
							</div>
						</div>
					<?php endif; ?>
                <?php endforeach; ?>	
				<?php if (count($lista) == 0) : ?>
					<h3><?php echo __("There are no more related entities"); ?></h3>
				<?php endif; ?>							
			</div>
		</div> <!-- /widget-content -->

	</div>
	
</div>
