<script>
function mostrarInd(id_indicador){
     $("#mostrar").html("Cargando...");
		
    $.ajax({
        type: "POST",
        url: "/admin/indicators/show",
        data: { id: id_indicador},
        success: function(data)
        {
            $("#mostrar").html(data);
        }
    });

     /*$('.containerIndicadores').hide();
     $('#'+id).show();*/
 }

$(document).ready(function(){    
    $('a#uno').on('click', function (event) {
        //se previene que recargue y se elimina el load
        event.preventDefault();  
        $('#load').remove();        
    });

    $('a#dos').on('click', function (event) {
        //se previene que recargue y se elimina el load
        event.preventDefault();  
        $('#load').remove();    
    });

    $('.detalles').on('click', function (event) {
        //se previene que recargue y se elimina el load
        
        event.preventDefault();
        var ref = $(this).attr('href');
        console.log(ref);
        $('#load').remove();    
        window.open($(this).attr('href'),'_newtab');  
        
    });

});



</script>
<style>

.indicadores{
    cursor: pointer; 
    cursor: hand;
}

.table-bordered thead tr {
background-image: linear-gradient(to bottom, #fff 0%, #f5f5f5 100%);
}

.table-bordered>thead>tr>th, .table-bordered>thead>tr>td {
    border-bottom-width: 1px;
}

.table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
    border: 1px solid #eee;
}

</style>
<?php echo $this->Html->script('/js/plugins/highcharts/highcharts.js'); ?>
<?php echo $this->Html->script('/js/plugins/highcharts/highcharts-more.js'); ?>
<?php echo $this->Html->script('/js/plugins/highcharts/exporting.js'); ?>

<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home" id="uno" ><?php echo __("Main"); ?></a></li>
  <li><a data-toggle="tab" href="#menu1" id="dos"><?php echo __("Featured"); ?></a></li>
</ul>

<div class="tab-content">
  <div id="home" class="tab-pane fade in active">
    <h3>Balance Score Card</h3>
    <?php echo $this->Html->css('/css/jquery.treegrid.css'); ?>
    
    <?php echo $this->Html->script('/js/plugins/treegrid/jquery.treegrid.js'); ?>
    
    <?php echo $this->Html->script('/js/plugins/treegrid/jquery.treegrid.bootstrap3.js'); ?>
                
    <?php $inicio = 1; ?>
    <?php $rama = 1; ?>
    <div class="row-fluid">
    <div class="span8">







    <table class="tree table table-bordered">
    <thead>
    <tr>
        <td><?php echo __("Name"); ?></td>
        <td><?php echo __("Value"); ?></td>
        <td><?php echo __("Baseline"); ?></td>
        <td><?php echo __("Goal"); ?></td>
    </tr>
    <thead>
        <tr class="treegrid-1">
            <td><span class="treegrid-folder">&nbsp;&nbsp;&nbsp;</span>&nbsp;<?php echo __("Balance ScoreCard"); ?></td><td>&nbsp;</td>
        <td>&nbsp;</td><td>&nbsp;</td>
        </tr>
    <?php foreach($perspectives as $p) : ?>
    <?php if (count($p["Indicators_detail"]) > 0 or count($p['Strategies']) > 0) : ?>
    <?php 
        $inicio++;
        $ramap = $inicio; 
    ?>
        
        <tr class="treegrid-<?=$inicio; ?> treegrid-parent-1">
            <td><?php echo $p["Node"]["name"];?></td><td>&nbsp;</td>
        <td>&nbsp;</td><td>&nbsp;</td>
        </tr>
            <?php if (is_array($p['Indicators']) and count($p['Indicators_detail']) > 0) : ?>
                <?php $rama = $inicio; ?>
                <?php foreach($p['Indicators'] as $ind) :?>

                <?php $inicio++; ?>
                <tr class="treegrid-<?=$inicio; ?> treegrid-parent-<?=$rama; ?>">
                <td class="indicadores" onclick="mostrarInd(<?=$ind['Indicator']['id'];?>);"
                style="white-space: nowrap; ">
                <i class="<?=$ind["Indicator"]["icon"]; ?>"></i>
                <?=$ind['Indicator']['name'];?>
                </td>
                <?php 
                $color = 1;
                $pct = 0;
                $color = $p['Indicators_detail'][$ind['Indicator']['id']]['color'];
                $pct = number_format($p['Indicators_detail'][$ind['Indicator']['id']]['achieved']*100/$ind['Indicator']['goal'],2);
                ?>
                <td style="white-space: nowrap;">
					<?php 
					echo 	number_format($p['Indicators_detail'][$ind['Indicator']['id']]['achieved'], 2) . ' ' . 
							$p['Indicators_detail'][$ind['Indicator']['id']]['metric']; ?>
					<br>
					<?php echo $this->Viewbase->add_semaforo($color, $pct); ?>
				</td>
                <td><?=$ind['Indicator']['baseline']; ?></td>
                <td><?=$ind['Indicator']['goal']; ?></td>
                </tr>
                <?php endforeach; ?>
            <?php endif; ?>  
            <?php if (is_array($p['Strategies']) and count($p['Strategies']) > 0) : ?>
                <?php $ramae = $inicio; ?>
                <?php foreach($p['Strategies'] as $est) : ?>
                    <?php $inicio++; ?>
                    <tr class="treegrid-<?=$inicio; ?> treegrid-parent-<?=$ramap; ?>" >
                        <td><?php echo __("Strategy") . ": " . $est['nombre'];?></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>  
                    <?php if (is_array($est['objetivos']) and count($est['objetivos']) > 0) :?>
                        <?php $ramao = $inicio; ?>
                        <?php foreach($est['objetivos'] as $obj) : ?>
                        <?php if ((is_array($obj['Indicators']) and count($obj['Indicators_detail'])) 
                        or (is_array($obj['Commitments']) and count($obj['Commitments'])> 0)) : ?>
                            <?php $inicio++; ?>
                            <tr class="treegrid-<?=$inicio; ?> treegrid-parent-<?=$ramao; ?>" >
                                <td><?php echo __("Objective") . ": " . $obj['nombre']; ?></td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <?php if (is_array($obj['Indicators']) and count($obj['Indicators_detail'])) :  ?>
                            <?php $ramaind = $inicio; ?>
                            <?php foreach($obj['Indicators'] as $ind2) :?>
                                <?php $inicio++; ?>
                                <tr class="treegrid-<?=$inicio; ?> treegrid-parent-<?=$ramaind; ?>">
                                <td class="indicadores" onclick="mostrarInd(<?=$ind2['Indicator']['id'];?>);"
                                style="white-space: nowrap; ">
                                <i class="<?=$ind2["Indicator"]["icon"]; ?>"></i>
                                <?=$ind2['Indicator']['name'];?>
                                </td>
                                <?php 
                                $color = 1;
                                $pct = 0;
                                $color = $obj['Indicators_detail'][$ind2['Indicator']['id']]['color'];
                                if ($ind2['Indicator']['goal']<>0) {
									$pct = number_format($obj['Indicators_detail'][$ind2['Indicator']['id']]['achieved']*100/$ind2['Indicator']['goal'],2);
								}
                                ?>
                                <td style="white-space: nowrap;">
									<?php 
									echo 	number_format($obj['Indicators_detail'][$ind2['Indicator']['id']]['achieved'], 2) . ' ' . 
											$obj['Indicators_detail'][$ind2['Indicator']['id']]['metric']; ?>
									<br>
									<?php echo $this->Viewbase->add_semaforo($color, $pct); ?>
								</td>
                                <td><?=$ind2['Indicator']['baseline']; ?></td>
                                <td><?=$ind2['Indicator']['goal']; ?></td>
                                </tr>
                            <?php endforeach; ?>
                            <?php endif; //indicators objetives?>
                            
                            <?php if (is_array($obj['Commitments']) and count($obj['Commitments']) > 0) : ?>
                            <?php $ramacmm = $inicio; ?>
                            <?php foreach($obj['Commitments'] as $cmm) :?>
                                <?php $inicio++; ?>
                                <tr class="treegrid-<?=$inicio; ?> treegrid-parent-<?=$ramacmm; ?>" >
                                    <td><?php echo __("Commitment") . ": " . $cmm['nombre'];?></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>   
                                <?php if (is_array($cmm['Programs']) and count($cmm['Programs']) > 0) : ?>
                                <?php $ramapro = $inicio; ?>
                                <?php foreach($cmm['Programs'] as $pro) : ?>
                                    <?php $inicio++; ?>
                                    <tr class="treegrid-<?=$inicio; ?> treegrid-parent-<?=$ramapro; ?>" >
                                        <td><?php echo __("Program") . ": " . $pro['nombre'];?></td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <?php if (is_array($pro['Activities']) and count($pro['Activities']) > 0) : ?>
                                    <?php $ramaacti = $inicio; ?>
                                    <?php foreach($pro['Activities'] as $acti) :  ?>

                                        <?php $inicio++; ?>
                                        <?php $temporal = $inicio; ?>
                                        <tr class="treegrid-<?=$inicio; ?> treegrid-parent-<?=$ramaacti; ?>" >
                                            <td><?php echo __("Activity") . ": " . $acti['nombre'];?></td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        
                                        <?php if (is_array($acti['Tasks']) and count($acti['Tasks']) > 0) : ?>
                                        <?php $ramatask = $inicio; ?>
                                        <?php foreach($acti['Tasks'] as $task) : ?>
                                            <?php $inicio++; ?>
                                            <tr class="treegrid-<?=$inicio; ?> treegrid-parent-<?=$ramatask; ?>" >
                                                <td><?php echo __("Task") . ": " . $task['nombre'];?></td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <?php if (is_array($task['Indicators']) and count($task['Indicators_detail']) > 0) : ?>
                                            <?php $ramaindtask = $inicio; ?>
                                            <?php foreach($task['Indicators'] as $ind4) :?>
                                                <?php $inicio++; ?>
                                                <tr class="treegrid-<?=$inicio; ?> treegrid-parent-<?=$ramaindtask; ?>">
                                                <td class="indicadores" onclick="mostrarInd(<?=$ind4['Indicator']['id'];?>);"
                                                style="white-space: nowrap; ">
                                                <i class="<?=$ind4["Indicator"]["icon"]; ?>"></i>
                                                <?=$ind4['Indicator']['name'];?>
                                                </td>
                                                <?php 
                                                $color = 1;
                                                $pct = 0;
                                                $color = $task['Indicators_detail'][$ind4['Indicator']['id']]['color'];
                                                if ($the_goal=$ind4['Indicator']['goal']<>0) {
													$pct = number_format($task['Indicators_detail'][$ind4['Indicator']['id']]['achieved']*100/$the_goal,2);
												}
                                                ?>
                                                <td style="white-space: nowrap;">
													<?php 
													echo 	number_format($task['Indicators_detail'][$ind4['Indicator']['id']]['achieved'], 2) . ' ' . 
															$task['Indicators_detail'][$ind4['Indicator']['id']]['metric']; 
													?>
													<br>

													<?php echo $this->Viewbase->add_semaforo($color, $pct); ?>
												</td>
                                                <td><?=$ind4['Indicator']['baseline']; ?></td>
                                                <td><?=$ind4['Indicator']['goal']; ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            <?php endif; //indicators task ?>
                                        <?php endforeach; ?>
                                        <?php endif; //task?>
                                        <?php if (is_array($acti['Indicators']) and count($acti['Indicators_detail']) > 0) : ?>
                                        <?php $ramaindact = $inicio; ?>
                                        <?php foreach($acti['Indicators'] as $ind3) :?>
                                            <?php $inicio++; ?>
                                            <tr class="treegrid-<?=$inicio; ?> treegrid-parent-<?=$temporal; ?>">
                                            <td class="indicadores" onclick="mostrarInd(<?=$ind3['Indicator']['id'];?>);"
                                            style="white-space: nowrap; ">
                                            <i class="<?=$ind3["Indicator"]["icon"]; ?>"></i>
                                            <?=$ind3['Indicator']['name'];?>
                                            </td>
                                            <?php 
                                            $color = 1;
                                            $pct = 0;
                                            $color = $acti['Indicators_detail'][$ind3['Indicator']['id']]['color'];
                                            $pct = number_format($acti['Indicators_detail'][$ind3['Indicator']['id']]['achieved']*100/$ind3['Indicator']['goal'],2);
                                            ?>
                                            <td style="white-space: nowrap;">
												<?php 
												echo 	number_format($acti['Indicators_detail'][$ind3['Indicator']['id']]['achieved'], 2) . ' ' . 
														$acti['Indicators_detail'][$ind3['Indicator']['id']]['metric']; ?>
												<br>
												<?php echo $this->Viewbase->add_semaforo($color, $pct); ?>
											</td>
                                            <td><?=$ind3['Indicator']['baseline']; ?></td>
                                            <td><?=$ind3['Indicator']['goal']; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        <?php endif; //indicators activities ?>
                                <?php endforeach ?>     
                                <?php endif; //activities?>
                                                        
                            <?php endforeach; ?>
                            <?php endif; //program?>
                            
                        <?php endforeach; ?>
                    <?php endif;//commitments ?>    
                    <?php endif; ?>              
                <?php endforeach; ?>
            <?php endif; //objectives?> 
            <?php endforeach; ?>  
            <?php endif; ?>  
            <?php endif; ?> 
        <?php endforeach; ?>
    </table>
    </div>
    <div class="span4" >
    <!--abre-->
    <div id="mostrar"></div>    
    <!--cierra-->
    </div>
    </div>

    <script>
    $(document).ready(function() {
            $('.tree').treegrid();
    });
    </script>


  </div>
  <div id="menu1" class="tab-pane fade">


<?php if (is_array($indicators) and count($indicators) > 0) : ?>
    <h3>Indicadores Destacados</h3>
    <?php

   foreach($indicators as $index => $ind) :

    $colores = $ind['data_json'];
    
     
     $colors = array();
     $colors['color1'] = $colores[0]['namecolor'];
     $colors['color2'] = $colores[1]['namecolor'];
     $colors['color3'] = $colores[2]['namecolor'];
     $colors['from1']  = (strlen($colores[0]['from']) > 0)?$colores[0]['from']:0;
     $colors['to1']    = (strlen($colores[0]['to']) > 0)?$colores[0]['to']:0;
     $colors['from2']  = (strlen($colores[1]['from']) > 0)?$colores[1]['from']:0;
     $colors['to2']    = (strlen($colores[1]['to']) > 0)?$colores[1]['to']:0;
     $colors['from3']  = (strlen($colores[2]['from']) > 0)?$colores[2]['from']:0;
     $colors['to3']    = (strlen($colores[2]['to']) > 0)?$colores[2]['to']:0;
  ?>
  <div class="row-fluid">        
        <div class="span6">
            
    <?php 
        echo $this->element('iggraph', [
            'cond_value_1'=>$ind["Indicator"]["cond_value_1"],
            'cond_value_2'=>$ind["Indicator"]["cond_value_2"],
            'cut_order'=>$ind["Indicator"]["cut_order"],
            'name'=>$ind['Indicator']['name'],
            'achieved'=>$res[$index][$ind['Indicator']['id']]['achieved'],
            'goal'=>$ind['Indicator']['goal']
        ]);
    ?>         
            </div>
    </div>
   <!--  <div class="row-fluid">        
        <div class="span6">
            <div id="container_<?= $ind['id']; ?>" 
            style="min-width: 310px; max-width: 
            400px; height: 300px; margin: 0 auto;" ></div>
        </div>
        <script>
        $(function () {
            

            $('#container_<?=$ind['id'];?>').highcharts({

                chart: {
                    type: 'gauge',
                    plotBackgroundColor: null,
                    plotBackgroundImage: null,
                    plotBorderWidth: 0,
                    plotShadow: false
                },

                title: {
                    text: '<?=$ind['name'];?>'
                },

                pane: {
                    startAngle: -150,
                    endAngle: 150,
                    background: [{
                        backgroundColor: {
                            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                            stops: [
                                [0, '#FFF'],
                                [1, '#333']
                            ]
                        },
                        borderWidth: 0,
                        outerRadius: '109%'
                    }, {
                        backgroundColor: {
                            linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                            stops: [
                                [0, '#333'],
                                [1, '#FFF']
                            ]
                        },
                        borderWidth: 1,
                        outerRadius: '107%'
                    }, {
                        // default background
                    }, {
                        backgroundColor: '#DDD',
                        borderWidth: 0,
                        outerRadius: '105%',
                        innerRadius: '103%'
                    }]
                },

                // the value axis
                yAxis: {
                    min: 0,
                    max: <?= $ind['goal'];?>,

                    minorTickInterval: 'auto',
                    minorTickWidth: 1,
                    minorTickLength: 10,
                    minorTickPosition: 'inside',
                    minorTickColor: '#666',

                    tickPixelInterval: 30,
                    tickWidth: 2,
                    tickPosition: 'inside',
                    tickLength: 10,
                    tickColor: '#666',
                    labels: {
                        step: 2,
                        rotation: 'auto'
                    },
                    title: {
                        text: ' '
                    },
                    plotBands: <?=$ind['data_json'];?>
                },

                series: [{
                    name: <?php echo  "'".$ind['name']."'"; ?>,
                    data: [<?= $ind['achieved']; ?>]                    
                }]

            }
            // Add some life
            );
        });
        </script>
     </div>   
     -->



    
<?php             
        endforeach; 
        else :

    ?>
    
            <h3><?php echo __("There aren't highlighted indicators"); ?></h3>
    

    <?php endif; ?>
  </div>  
</div>

