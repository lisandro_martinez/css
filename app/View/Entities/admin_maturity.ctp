<?php
//pr($auto);
$nporc2 = 0;
$porc_total2 = 0;
$tot_company2 = array();
$ult_comp_id2 = 0;
foreach($auto as $d) :

	if ($d["Company"]["id"]!=$ult_comp_id2) :								
		$ult_comp_id=$d["Company"]["id"];			
		// inicializa totales para cálculo de promedio
		$tot_company2[$d["Company"]["id"]]["name"]=$d["Company"]["name"];
		$tot_company2[$d["Company"]["id"]]["total"]=0;
		$tot_company2[$d["Company"]["id"]]["n"]=0;				
	endif;

		$y=date("Y", strtotime($d["Autoevaluation"]["date_time"]));
		$porc2=0;

	if ($d["QuestionnaireTotalPoints"]) :
		$porc2 = round($d["Autoevaluation"]["total_points"]/$d["QuestionnaireTotalPoints"]*100, 0);
	endif;

	if (isset($d["Company"]["id"])) {
					$tot_company2[$d["Company"]["id"]]["total"]+=$d["Autoevaluation"]["total_points"];
					$tot_company2[$d["Company"]["id"]]["n"]++;
					// suma el total por años para el resumen
					$tot_company2[$d["Company"]["id"]]["years"][$y]["total"]+=$porc2;
					$tot_company2[$d["Company"]["id"]]["years"][$y]["n"]++;
				}
	$porc_total2+=$porc2;
	$nporc2++;


endforeach;
?>

<?php if (isset($auto) and is_array($auto)) { 
	if ($nporc2) {
	?>
<div class="col-md-9">

	<div class="widget stacked ">

	  <div class="widget-header">
		<i class="icon-globe"></i>
		<h3><?php echo __("Global Maturity Level");?></h3>
	  </div> <!-- /widget-header -->

	  <div class="widget-content">		
			
				<div class="page-header"><h2><i class="icon-globe"></i> <?php echo __("Global");?></h2></div>
				
				<?php
				
				foreach($tot_company2 as $tot) {
				
					?>
					<div class="col-md-12">
						<div class="page-header">
							<h3><i class="icon-asterisk"></i> <?php echo $tot["name"];?></h3>
						</div>
					</div>	
					<?php
					
					if (is_array($tot["years"])) {
					
						foreach($tot["years"] as $y=>$year) {

							$tot_year=0;
							if ($year["n"]) {
								$tot_year=round($year["total"]/$year["n"],0);
							}
							?>
							<div class="col-md-3">
								<br>
								<h4><i class="icon-chevron-right"></i> <?php echo $y;?></h4>
							</div>	
							<div class="col-md-9">
								<p><?php echo $tot_year;?>/<?php echo "100";?> (<?php echo $tot_year; ?>%)</p>
								<div class="progress">
								  <div style="width: <?php echo $tot_year;?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?php echo $tot_year;?>" role="progressbar" class="progress-bar progress-bar-success">
									<span class="sr-only"><?php echo $tot_year;?>% Complete (success)</span>
								  </div>
								</div>
							</div>	
							<?php
							
						}
					
					}

					$tot_company2=0;
					if ($tot["n"]) {
						$tot_company2=round($tot["total"]/$tot["n"], 0);
					}
					?>
					<!--
					<div class="col-md-6">
						<h4><i class="icon-asterisk"></i> <?php echo $tot["name"];?></h4>
					</div>	
					<div class="col-md-6">
						<p><?php echo $tot_company;?>/<?php echo "100";?> (<?php echo $tot_company; ?>%)</p>
						<div class="progress">
						  <div style="width: <?php echo $tot_company;?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?php echo $tot_company;?>" role="progressbar" class="progress-bar progress-bar-info">
							<span class="sr-only"><?php echo $tot_company;?>% Complete (success)</span>
						  </div>
						</div>
					</div>
					-->					
					<?php
					
				}
				
				?>
				<!--
				<div class="col-md-6">
					<h2><i class="icon-globe"></i> Global</h2>
				</div>	
				<div class="col-md-6">
					<p><?php echo $total_total;?>/<?php echo "100";?> (<?php echo $total_total; ?>%)</p>
					<div class="progress">
					  <div style="width: <?php echo $total_total;?>%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="<?php echo $total_total;?>" role="progressbar" class="progress-bar progress-bar-success">
						<span class="sr-only"><?php echo $total_total;?>% Complete (success)</span>
					  </div>
					</div>
				</div>	
				-->
			<?php
				
			}
			
		} else {
		
			echo __("No Autoevaluation has been found");
		
		}
		
		?>
		
	  </div> <!-- /widget-content -->

	</div>
	
</div>

<div class="col-md-3">

		<div class="widget stacked widget-box">
			
			<div class="widget-header">	
				<i class="icon-info"></i>
				<h3><?php echo __("Helper"); ?></h3>			
			</div> <!-- /widget-header -->
			
			<div class="well">
				
				<p><?php echo __("HELP-MATURITY-2"); ?></p>
				
			</div> <!-- /widget-content -->
			
		</div> <!-- /widget -->


</div>




<div class="col-md-9">

<div class="widget stacked ">

  <div class="widget-header">
  	<i class="icon-circle-arrow-up"></i>
  	<h3><?php echo __("Maturity Level by Companies");?></h3>
  </div> <!-- /widget-header -->

  <div class="widget-content">

	<?php
	$porc_total=0;
	$nporc=0;
	
	if (isset($auto) and is_array($auto)) {
	
		$ult_corp_id=0;
		$ult_comp_id=0;
	
		$tot_company=array();
	
		foreach($auto as $a) {

			?> 
			
			<div class="col-md-12">
			
			<?php
			
			if ($a["Corporation"]["id"]!=$ult_corp_id) {
				echo "<div class=\"page-header\">";
				echo "<h1>".$a["Corporation"]["name"]."</h1>";
				echo "</div>";
				$ult_corp_id=$a["Corporation"]["id"];
			}
			
			if ($a["Company"]["id"]!=$ult_comp_id) {
				
				echo "<div class=\"page-header\">";
				echo "<h3><i class=\"icon-asterisk\"></i> ".$a["Company"]["name"]."</h3>";
				echo "</div>";
				$ult_comp_id=$a["Company"]["id"];
				
				// inicializa totales para cálculo de promedio
				$tot_company[$a["Company"]["id"]]["name"]=$a["Company"]["name"];
				$tot_company[$a["Company"]["id"]]["total"]=0;
				$tot_company[$a["Company"]["id"]]["n"]=0;
				
			}
			
			?>

			</div>
		
			<div class="col-md-6">
				<h4><i class="icon-chevron-right"></i> 
					<?php 
					$y=date("Y", strtotime($a["Autoevaluation"]["date_time"]));
					echo date("Y", strtotime($a["Autoevaluation"]["date_time"]));
					?>
					<i class="icon-chevron-right"></i> 
					<?php echo $a["Questionnaire"]["acronym"];?></h4>
				<p><?php echo $a["Questionnaire"]["title"];?></p>
			</div>	
			<div class="col-md-6">
				<p><?php echo $a["Autoevaluation"]["total_points"];?>/<?php echo $a["QuestionnaireTotalPoints"];?> (<?php
				$porc=0;
				if ($a["QuestionnaireTotalPoints"]) {
					$porc = round($a["Autoevaluation"]["total_points"]/$a["QuestionnaireTotalPoints"]*100, 0);
				}
				echo $porc;
				
				// suma el total de la compañía para el resumen
				if (isset($a["Company"]["id"])) {
					$tot_company[$a["Company"]["id"]]["total"]+=$a["Autoevaluation"]["total_points"];
					$tot_company[$a["Company"]["id"]]["n"]++;
					// suma el total por años para el resumen
					$tot_company[$a["Company"]["id"]]["years"][$y]["total"]+=$porc;
					$tot_company[$a["Company"]["id"]]["years"][$y]["n"]++;
				}
				?>%)</p>
				<div class="progress">
				  <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="<?php echo $porc;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $porc;?>%">
					<span class="sr-only"><?php echo $porc;?>%</span>
				  </div>
				</div>
			</div>	
			
		<?php
			
			$porc_total+=$porc;
			$nporc++;
		
		}
		
		if ($nporc) {
		
			$total_total = round($porc_total/$nporc, 0);
			
		?>
	</div>
	
</div>

</div>
	

<div class="col-md-3">


		<div class="widget stacked widget-box">
			
			<div class="widget-header">	
				<i class="icon-info"></i>
				<h3><?php echo __("Helper"); ?></h3>			
			</div> <!-- /widget-header -->
			
			<div class="well">
				
				<p><?php echo __("HELP-MATURITY-1"); ?></p>
				<?php
	} 
	}
				else {
		
			echo __("No Autoevaluation has been found");
		
		}
				
				 ?>
			</div> <!-- /widget-content -->
			
		</div> <!-- /widget -->


</div>

<?php if (isset($indicators) && count($indicators) > 0) : ?>
	<div class="col-md-9">
		<div class="widget stacked">
			<div class="widget-header">
				<i class="icon-circle-arrow-up"></i>
				<h3><?php echo __("Highlighted indicators summary");?></h3>
			</div> <!-- /widget-header -->
			<div class="widget-content">
				<div class="col-md-12">
					<div class=\"page-header\">
					<h3><i class=\"icon-asterisk\"></i><?php echo __("Indicators"); ?></h3>
					</div>				
				</div>
				<?php foreach ($indicators as $ind) : ?>
				<div class="col-md-6">
					<h4><i class="icon-chevron-right"></i> 
						<?php 
						echo $ind['name'];
						?>
						</h4>
					<p>&nbsp; &nbsp;</p>
				</div>
				<div class="col-md-6">
					<p><?php echo $ind["achieved"];?>/<?php echo $ind["goal"];?> (<?php
					$porc=0;					
					$porc = round($ind['achieved']/$ind["goal"]*100, 0);					
					echo $porc;										
					?>%)</p>
					<div class="progress">
					<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="<?php echo $porc;?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $porc;?>%">
						<span class="sr-only"><?php echo $porc;?>%</span>
					</div>
					</div>
				</div>


				<?php endforeach; ?>

			</div>
		</div>
	</div>

<div class="col-md-3">


		<div class="widget stacked widget-box">
			
			<div class="widget-header">	
				<i class="icon-info"></i>
				<h3><?php echo __("Helper"); ?></h3>			
			</div> <!-- /widget-header -->
			
			<div class="well">
				
				<p><?php echo __("HELP-MATURITY-3"); ?></p>				
			</div> <!-- /widget-content -->
			
		</div> <!-- /widget -->


</div>

<?php endif; ?>

