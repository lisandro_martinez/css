<?php
echo $this->element('sm', array('panel_title'=>__("Interest Groups"),
								'tab_content_title'=>__("Please choose the Interest Group to see the details"),
								'help'=>__("HELP-INVENTORY-2"),
								'nodetype_id'=>16,
								'nodetype_add_text'=>__("New Interest Group"),
								'redir'=>$redir
								)
					);
?>