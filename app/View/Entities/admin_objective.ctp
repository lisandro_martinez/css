<?php
echo $this->element('sm', array('panel_title'=>__("Objective"),
								'tab_content_title'=>__("Please choose the Objective to see the details"),
								'help'=>__("HELP-INVENTORY-2"),
								'nodetype_id'=>Node::TYPE_OBJECTIVE,
								'nodetype_add_text'=>__("New Objective"),
								'redir'=>$redir
								)
					);
?>
