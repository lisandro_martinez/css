<script>

function RemoveMe(uuid) {

	if (confirm('<?php echo __("Are you sure do you want to delete this?");?>')==true) {
		$('span[id^="'+uuid+'"]').remove();
	}
	return false;
}

function generateUUID(){
    var d = new Date().getTime();
    if(window.performance && typeof window.performance.now === "function"){
        d += performance.now();; //use high-precision timer if available
    }
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
}

function AddEntity() {
	
	if ($("#ents").val()>0) {
	
		$("#ent_sel").fadeOut(function() {
		
			var html = '';
			
			// generar un wrapper con ID unico que luego se pueda borrar
			
			uuid = generateUUID();
			html = html + '<span id="'+uuid+'">';
			
			// campo hidden
			html = html + '<input type=hidden value="'+$("#ents").val()+'" name="related[]">';
			
			// nombre de la entidad
		
			html = html + '<h4><i class="icon-asterisk"></i> ' + $("#ents option[value='"+$("#ents").val()+"']").text() + '</h4>';

			// agrega la opción de eliminar
			
			html = html + '<a class="btn-delete" href="javascript: void(0);" onclick="return RemoveMe(\''+uuid+'\');"><i class="icon-trash"></i> <?php echo __("Delete");?></a>';

			// cierra el wrapper
			
			html = html + '</span>';

			$(this).append(html).fadeIn();
			
		});

	}
	
	return false;
}
</script>

<?php
if (isset($nodes) and is_array($nodes) and sizeof($nodes)) {
?>

<hr>
<select id=ents class="form-control" style="font-size: x-large; height: auto">
<?php 
echo "<option value=\"0\">".__("Choose one")."</option>";
foreach($nodes as $n) {
	echo "<option value=\"".$n["Node"]["id"]."\">".$n["Node"]["name"]."</option>";
}
?>
</select>

<br>
<p><button style="text-align: center;" class="btn btn-info" onclick="return AddEntity();"><?php echo __("Add"); ?></button></p>

<?php
} else {
	if($nodetype) {
		echo __("There are no entities of this type in the system");
	}
}
?>
