<?php
echo $this->element('sm', array('panel_title'=>__("Weaknesses and Opportunities"),
								'tab_content_title'=>__("Please choose the Opportunity/Weakness to see the details"),
								'help'=>__("HELP-INVENTORY-2"),
								'nodetype_id'=>1,
								'nodetype_add_text'=>__("New W/O Manually"),
								'redir'=>$redir
								)
					);
?>
			
			<div class="col-md-6">
				<div class="widget stacked ">
				  <div class="widget-header">
					<i class="icon-plus"></i>
					<h3><?php echo __("Autoevaluation");?></h3>
				  </div> <!-- /widget-header -->
				  <div class="widget-content">
					<p><a class="btn btn-primary btn-support-ask" href="/admin/autoevaluations/add/"><?php echo __("Conduct a Self-Assessment");?></a></p>
				  </div> <!-- /widget-content -->
				</div> <!-- /widget -->
			</div>
			
		
