<script>
function checkEmpty() {
	var todook=true;
	if ($("#txt1").val() != undefined 
	&& $("#txt1").val()=='') {
		$("#warning1").fadeIn();
		todook=false;
	}
	/*if ($("#txt2").val()=='') {
		$("#warning2").fadeIn();
		todook=false;
	}*/
	return todook;
}

function checkEmptyAndSave()
{
	var todook=true;
	var vacio = false;
	if ($("#txt1").val() != undefined 
	&& $("#txt1").val()=='' ) {		
		vacio = true;		
	}

	if ($("#txt2").val() != undefined && 
		$("#txt2").val()=='') {
		vacio = true;		
	}

	if (vacio) {
		console.log('ENTRAAA');
		$("#guardar").val(1); //siguiente sin guardar
	} else {
		console.log('NOOOOOOOO ENTRAAA');
		$("#guardar").val(2); //siguiente guardando
	}

	
	return todook;
}

function checkEmptyNext()
{
		var todook=true;	
		var vacio = false;
	if ($("#txt1").val() != undefined 
	&& $("#txt1").val()=='') {		
		vacio = true;		
	}

	if ($("#txt2").val() != undefined && 
		$("#txt2").val()=='') {
		vacio = true;		
	}

	if (vacio) {
		$("#guardar").val(1); //siguiente sin guardar
	} else {
		$("#guardar").val(2); //siguiente guardando
	}
	$("#nextstep").val(1);
	return todook;

}
</script>

<style>
@media (max-width: 700px) {
  #relleno {
    display: none;
  }
}

</style>

<link rel="stylesheet" href="/css/faq.css">

<div class="col-md-12">

	<div class="widget stacked ">

		<div class="widget-header">
			<i class="icon-star"></i>
			<h3><?php echo __("Wizard");?></h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">
				<?php
				if ($e) {
				?>

					<div class="col-md-2">
						<div class="numberCircle"><?=$e-1?></div>
					</div>
					<div class="col-md-10">
				
					<h2><?php 
					switch ($e) {
					case 2:
						echo __("BSC Perspective");
						break;
					case 3:
						echo __("Politics");					
						break;
					case 4:
						echo __("Objective");						
						break;
					case 5:					
						echo __("Strategy");							
						break;
					case 6:
						echo __("Mission");						
						break;					
					case 7:
						echo __("Vision");
						break;
					case 8:
						echo __("Commitment");
						break;
					}
					?></h2>

					<hr>
					<form action="/admin/entities/wizard/<?=$e?>" method="post">
					<input type=hidden name=nextstep  value=0 id=nextstep>
					<input type=hidden name=guardar  value=0 id=guardar>
					<input type=hidden name=node_id value=<?php echo $e;?>>
					<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
					<p><input id=txt1 type=text name="name_esp" class="form-control" style="font-size: x-large; height: auto" placeholder="<?php echo __("Write the name")."  (ESP)";?>"></p>
					<div id=warning1 style="display: none;" class="alert alert-danger alert-dismissable"><strong>Oops!</strong> <?php echo __("This field is mandatory");?></div>
					<?php endif; ?>
					<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
					<p><input id=txt2 type=text name="name_eng" class="form-control" style="font-size: x-large; height: auto" placeholder="<?php echo __("Write the name")."  (ENG)";?>"></p>
					<div id=warning2 style="display: none;" class="alert alert-danger alert-dismissable"><strong>Oops!</strong> <?php echo __("This field is mandatory");?></div>
					<?php endif; ?>
					<?php if (! $this->Session->check('Config.LangVar') 
					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp')) : ?>
					<h4><?php echo __("Description")."  (ESP)".":";?></p>
					
					<p><textarea id=txt3 class="summernote" name="description_esp"></textarea></p>
					<div id=warning3 style="display: none;" class="alert alert-danger alert-dismissable"><strong>Oops!</strong> <?php echo __("This field is mandatory");?></div>
					<?php endif; ?>
					<?php if (! $this->Session->check('Config.LangVar') 
					or ($this->Session->check('Config.LangVar') 
					and $this->Session->read('Config.LangVar') == 'eng')) : ?>
					<h4><?php echo __("Description")."  (ENG)".":";?></p>
					<div id=warning4 style="display: none;" class="alert alert-danger alert-dismissable"><strong>Oops!</strong> <?php echo __("This field is mandatory");?></div>					
					<p><textarea id=txt4 class="summernote" name="description_eng"></textarea></p>
					<?php endif; ?>
					<hr>
					<p>
					<div class="row-fluid" style="padding: 0px;">
					<?php if (in_array($e, array(3, 4, 5, 6, 7, 8))) : ?>
					<div class="span2" style="text-align: left;">
						<button class="btn btn-info  btn-support-ask" style="padding: 5px;">
						<a href="/admin/entities/wizard/?e=<?=$e-1;?>" 
						style="text-decoration: none; color: white; padding: 5px !important;">
						<i class="icon-arrow-left"></i>
						<?php echo __('Previous'); ?></a>
						</button>
					</div>		
					<div class="offset2 span1" id="relleno">&nbsp;</div>			
					<?php endif; ?>
					<?php if (in_array($e, array(3, 4, 5, 6, 7, 8))) : ?>
						<div class="span2 offset1 text-center" >					
					<?php elseif ($e == 2): ?>
						<div class="span2 offset5 text-center" >
					<?php endif; ?>

						<button  style="color: white; padding: 5px !important; margin-right:0px;" onclick="return checkEmpty();" class="btn btn-info btn-support-ask ">
						<?php 
						switch ($e) {
						case 2:
							echo '<i class="icon-plus-sign"></i> '.__("Create").' &nbsp;';
							//echo '<i class="icon-search"></i> '.__("Create a BSC Perspective");
							break;
						case 3:
							echo '<i class="icon-plus-sign"></i> '.__("Create").' &nbsp;';
							break;
						case 5:
							echo '<i class="icon-plus-sign"></i> '.__("Create").' &nbsp;';
							break;
						case 6:
							echo '<i class="icon-plus-sign"></i> '.__("Create").' &nbsp;';
							break;
						case 4:
							echo '<i class="icon-plus-sign"></i> '.__("Create").' &nbsp;';
							break;
						case 7:
							echo '<i class="icon-plus-sign"></i> '.__("Create").' &nbsp;';
							break;
						case 8:
							echo '<i class="icon-plus-sign"></i> '.__("Create").' &nbsp;';
							break;
						}
						?>
						</button>
					</div>
					<?php if (in_array($e, array(3,4,5,6,7))) : ?>
						<div class="span2 offset2 text-right" >
							<button onclick="return checkEmptyAndSave();" 
							style="color: white; padding: 5px !important;" class="btn btn-info btn-support-ask">
							<i class="icon-arrow-right"></i>
							<?php echo __('Next'); ?>
							</button>
						</div>
					<?php elseif ($e == 2) : ?>
						<div class="span1 offset3" style="text-align: right;">
							<button onclick="return checkEmptyAndSave();" 
							style="color: white; padding: 5px !important;" class="btn btn-info btn-support-ask ">
							<i class="icon-arrow-right"></i>
							<?php echo __('Next'); ?>
							</button>
						</div>
					<?php elseif ($e == 8) : ?>
						<div class="span2 offset2" style="text-align: right;">
							<button  
							class="btn btn-info btn-support-ask "
							style="color: white; padding: 5px !important;" onclick="return checkEmptyNext();">
							<i class="icon-arrow-right"></i>
							<?php echo __('Next'); ?>
							</button>
						</div>
					<?php endif; ?>
					</div>
					</p>
					</form>
					
				<?php
				} else {
				?>
				
				<div class="col-md-2">
					<div class="numberCircle">1</div>								
				</div>
				<div class="col-md-10">
					
				<h2><?php echo __("What type of entity do you want to create?"); ?></h2>
				<hr>
				
				<p><a href="/admin/entities/wizard/?e=2" class="btn btn-info btn-support-ask"><i class="icon-search"></i> <?php echo __("BSC Perspective"); ?></a></p>
				<p><a href="/admin/entities/wizard/?e=3" class="btn btn-info btn-support-ask"><i class="icon-leaf"></i> <?php echo __("Strategy"); ?></a></p>
				<p><a href="/admin/entities/wizard/?e=5" class="btn btn-info btn-support-ask"><i class="icon-ok"></i> <?php echo __("Mission"); ?></a></p>
				<p><a href="/admin/entities/wizard/?e=6" class="btn btn-info btn-support-ask"><i class="icon-eye-open"></i> <?php echo __("Vision"); ?></a></p>
				<p><a href="/admin/entities/wizard/?e=4" class="btn btn-info btn-support-ask"><i class="icon-heart"></i> <?php echo __("Commitment"); ?></a></p>
				<p><a href="/admin/entities/wizard/?e=7" class="btn btn-info btn-support-ask"><i class="icon-screenshot"></i> <?php echo __("Objective"); ?></a></p>
				<p><a href="/admin/entities/wizard/?e=8" class="btn btn-info btn-support-ask"><i class="icon-flag"></i> <?php echo __("Politics"); ?></a></p>
				
				<?php
				}
				?>
				
			</div>
		</div> <!-- /widget-content -->

	</div>
	
</div>

