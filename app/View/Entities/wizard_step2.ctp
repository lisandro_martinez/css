<script>
function showEntities() {
	$.ajax({
		type: "POST",
		url: "/admin/entities/getentities",
		data: "nodetype=" + $("#type").val(),
		success: function(data, textStatus) {
			$("#ent").fadeOut(function() {
				$(this).html(data).fadeIn();
			});
		},
		error: function() {
			console.log('Error loading.');
		}
	});
}
</script>

<link rel="stylesheet" href="/css/faq.css">

<div class="col-md-12">

	<div class="widget stacked ">

		<div class="widget-header">
			<i class="icon-star"></i>
			<h3><?php echo __("Wizard");?></h3>
		</div> <!-- /widget-header -->

		<div class="widget-content">
			<div class="col-md-2">
				<div class="numberCircle">10</div>
			</div>
			<div class="col-md-10">
			
				<h2><?php echo $message; ?></h2>
				<hr>
				<form action="/admin/entities/wizard/step5" method="post">
				<?php
				echo "<input type=hidden name=node_id value=\"".$node_id."\">";
				?>
				<select id=type class="form-control" style="font-size: x-large; height: auto" onchange="showEntities();">
				<?php 
				echo "<option value=\"0\">".__("Choose type")."</option>";
				foreach($nodetypes as $nt) {
					echo "<option value=\"".$nt["Nodetype"]["id"]."\">".$nt["Nodetype"]["name"]."</option>";
				}
				?>
				</select>
				<div id="ent"></div>
				<div id="ent_sel"></div>
				<hr>
				<p><button class="btn btn-info btn-support-ask" style="width: 100%"><?php echo __("Continue");?></button></p>
				
			</div>
		</div> <!-- /widget-content -->

	</div>
	
</div>
