<script>
function ShowHide(i) {
	$("#but_"+i).hide();
	$("#"+i).fadeIn();
}

function elscript() {
	$("#lalista2").html('<center><img src="/css/images/loader.gif" style="opacity: 0.4;filter: alpha(opacity=40);" /></center>');
	$.ajax({
		type: "POST",
		url: "/admin/entities/selentities",
		data: { 
			id: $("#elselect").val(),
			entity_id: <?php if ($node["Node"]["id"]) echo $node["Node"]["id"]; else echo 0; ?>,
			redir: '<?php echo $redir;?>'
		},
		success: function(data, textStatus) {
			$("#lalista2").fadeOut(function() {
				$(this).html(data).fadeIn();
			});
		},
		error: function() {
			console.log('Error loading.');
		}
	});
}

function elscript2() {
	$("#lalista3").html('<center><img src="/css/images/loader.gif" style="opacity: 0.4;filter: alpha(opacity=40);" /></center>');
	$.ajax({
		type: "POST",
		url: "/admin/entities/selentities",
		data: { 
			id: $("#elselect2").val(),
			entity_id: <?php if ($node["Node"]["id"]) echo $node["Node"]["id"]; else echo 0; ?>,
			redir: '<?php echo $redir;?>',
			predecessor: 1
		},
		success: function(data, textStatus) {
			$("#lalista3").fadeOut(function() {
				$(this).html(data).fadeIn();
			});
		},
		error: function() {
			console.log('Error loading.');
		}
	});
}
function elscript3(i) {
	if ($("#elselect3").val()>0) {
		document.location.href="/admin/entities/addindicator/"+$("#elselect3").val()+"/"+i+"/<?php echo $redir;?>";
	} else {
		alert("<?php echo __("Please choose an Indicator");?>");
	}
}
function elscript4(i) {
	if ($("#elselect4").val()>0) {
		document.location.href="/admin/entities/sendcall/"+i+"/"+$("#elselect4").val()+"/<?php echo $redir;?>";
	} else {
		alert("<?php echo __("Please choose an Interest Group");?>");
	}
}
</script>

<?php
if (isset($node["Node"]["id"])) {
?>
	<div class="col-md-12">
	
	
		<ol style="margin-top: 0px;" class="faq-list">
			
			<li id="faq-1"><div class="faq-icon"><div class="faq-number"><i class="icon-chevron-right"></i></div></div>
			
			<div class="faq-text">
				<div class="page-header">
					<h3><?php echo $node["Node"]["name"];?></h3>			 									
				</div>

				<p><?php echo $node["Node"]["description"];?></p>
				
				<div class="col-md-12">
					<p>
						<a 	href="/admin/nodes/edit/<?php echo $node["Node"]["id"];?>/<?php echo $redir;?>" 
							class="btn-edit">
							<i class="icon-edit"></i> 
							<?php echo __("Edit");?>
						</a>
						<a 	onclick="return confirm('<?php echo __("Are you sure do you want to delete this?");?>')" 
							href="/admin/nodes/delete/<?php echo $node["Node"]["id"];?>/<?php echo $redir;?>" 
							class="btn-delete">
							<i class="icon-trash"></i> 
							<?php echo __("Delete");?>
						</a>
					</p>
				</div>

				<?php
				if ($node["Node"]["inidate"] or
					$node["Node"]["enddate"] or
					($node["Node"]["cost"] and $node["Node"]["cost"]>0) or
					$node["Nodesource"]["name"] or
					$node["Nodestatus"]["name"] or
					$node["Node"]["assignedto"] or
					(
						$node["Node"]["nodetype_id"]==16 and 
						(
							$node["Node"]["power"] or 
							$node["Node"]["impact"] 
						)
					) or 
					$node["Node"]["assignedto_email"] or
					$node["Nodepriority"]["name"]) {
				?>
				<div class="widget big-stats-container stacked">
					
					<div class="widget-content" style="padding: 0px">
						
						<div class="cf" id="big_stats">
						
							<?php
							if ($node["Node"]["inidate"]) {
							?>
							<div class="stat">								
								<h4><?php echo __("Start date");?></h4>
								<span class="value"><?php echo date('d/m/Y', strtotime($node["Node"]["inidate"]))?></span>								
							</div>
							<?php
							}

							if ($node["Node"]["enddate"]) {
							?>
							<div class="stat">								
								<h4><?php echo __("End date");?></h4>
								<span class="value"><?php echo date('d/m/Y', strtotime($node["Node"]["enddate"]))?></span>								
							</div>
							<?php
							}

							if ($node["Node"]["cost"] and $node["Node"]["cost"]>0) { 
							?>
							<div class="stat">								
								<h4><?php echo __("Cost");?></h4>
								<span class="value"><?php echo $node["Node"]["cost"]." ".$node["Node"]["currency"]?></span>								
							</div>
							<?php
							}

							if ($node["Nodesource"]["name"]) { 
							?>
							<div class="stat">								
								<h4><?php echo __("Source");?></h4>
								<span class="value"><?php echo $node["Nodesource"]["name"];?></span>								
							</div>
							<?php
							}

							if ($node["Nodestatus"]["name"]) { 
							?>
							<div class="stat">								
								<h4><?php echo __("Status");?></h4>
								<span class="value"><?php echo $node["Nodestatus"]["name"];?></span>								
							</div>
							<?php
							}

							if ($node["Node"]["assignedto"] or $node["Node"]["assignedto_email"]) { 
							?>
							<div class="stat">								
								<h4><?php echo __("Assigned to");?></h4>
								<span class="value"><?php echo $node["Node"]["assignedto"]." (".$node["Node"]["assignedto_email"].")";?></span>								
							</div>
							<?php
							}

							if ($node["Nodepriority"]["name"]) { 
							?>
							<div class="stat">								
								<h4><?php echo __("Priority");?></h4>
								<span class="value"><?php echo $node["Nodepriority"]["name"];?></span>								
							</div>
							<?php
							}

							if ($node["Node"]["nodetype_id"]==16) {

								if ($node["Node"]["power"]) { 
								?>
								<div class="stat">								
									<h4><?php echo __("Power");?></h4>
									<span class="value"><?php echo $node["Node"]["power"];?></span>								
								</div>
								<?php
								}

								if ($node["Node"]["impact"]) {
								?>
								<div class="stat">								
									<h4><?php echo __("Impact");?></h4>
									<span class="value"><?php echo $node["Node"]["impact"];?></span>								
								</div>
								<?php
								}

							}
							?>
							
						</div>
					
					</div> <!-- /widget-content -->
					
				</div>
				
				<?php
				}
				
				// Si es una gráfica
				if ($node["Node"]["nodetype_id"]==Node::TYPE_GRAPH) {
					echo $this->element('graphdata', array('graphdata'=>$graphdata, 'graphtypes'=>$graphtypes, 'node'=>$node, 'redir'=>$redir));
				}
				
				// Si es una encuesta
				if ($node["Node"]["nodetype_id"]==Node::TYPE_SURVEY) {
					echo $this->element('surveyquestions', array('node'=>$node, 'questions'=>$questions));
					echo $this->element('surveyanswers', array('answers'=>$answers, 'questions'=>$questions, 'elnodeid'=>$node["Node"]["id"]));
					echo $this->element('surveytotalize', array('totaliza'=>$totaliza));
					echo "<hr>";
				} 
				
				// Entidades relacionadas
				echo $this->element('relatedentities', array('related'=>$related, 'node'=>$node, 'nodetypes'=>$nodetypes, 'redir'=>$redir));

				// INDICADORES DE GESTION RELACIONADOS	
				echo $this->element('relatedindicators', array('related_indicators'=>$related_indicators, 'indicators'=>$indicators, 'node'=>$node, 'nodetypes'=>$nodetypes));

				// Si es una galeria
				if ($node["Node"]["nodetype_id"]==Node::TYPE_GALLERY) {
					echo $this->element('gallery', array('node'=>$node, 'imgs'=>$imgs));					
				} 
				
				// Si es un buzon de mensajes
				if ($node["Node"]["nodetype_id"]==Node::TYPE_MAILBOX) {
					echo $this->element('mailbox', array('node'=>$node, 'msgs'=>$msgs, 'id'=>$node["Node"]["id"]));
				} 
				
				// Si es una convocatoria
				if ($node["Node"]["nodetype_id"]==Node::TYPE_CALL) {
					echo $this->element('call', array('interest_groups'=>$interest_groups, 'node'=>$node));

				}
				
				// PREDECESSORS for projects, etc...					
				if ($node["Node"]["nodetype_id"]>=11 and $node["Node"]["nodetype_id"]<=15 and 
					is_array($predecessors) and sizeof($predecessors)) {
					echo $this->element('predecessors', array('predecessors'=>$predecessors, 'node'=>$node, 'nodetypes'=>$nodetypes));
				}
				
				?>

			</div>
			</li>
													
		</ol>	
			
	</div>

<?php
} else {
	?>	
	<script>
		$("#divupload").hide();
	</script>
	<?php
}
?>
