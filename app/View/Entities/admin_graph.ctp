<?php
echo $this->element('sm', array('panel_title'=>__("Graph"),
								'tab_content_title'=>__("Please choose the Graph to see the details"),
								'help'=>__("HELP-INVENTORY-2"),
								'nodetype_id'=>Node::TYPE_GRAPH,
								'nodetype_add_text'=>__("New Graph"),
								'redir'=>$redir
								)
					);
?>