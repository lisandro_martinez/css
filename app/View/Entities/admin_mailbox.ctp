<?php
echo $this->element('sm', array('panel_title'=>__("Suggestions mailbox"),
								'tab_content_title'=>__("Please choose the suggestions mailbox to see the details"),
								'help'=>__("HELP-INVENTORY-2"),
								'nodetype_id'=>Node::TYPE_MAILBOX,
								'nodetype_add_text'=>__("New Mailbox"),
								'redir'=>$redir
								)
					);
?>