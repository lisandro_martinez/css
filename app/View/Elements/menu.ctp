<?php
$active_module=false;
if(isset($menu_category)){
  $active_module = $menu_category['Category']['module_id'];
}
?>

<nav class="navbar navbar-inverse" role="navigation">

	<div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a class="navbar-brand" href="/admin"><img src="/img/logo-css.png" class="logo-css"></a>
      </div>
	  
      <div class="" id="bs-example-navbar-collapse-1">

			<?php
			$ini='<ul class="nav navbar-nav navbar-right"><li class="">';
			$fin='</li></ul>';
			if ($this->Session->read('TheCorporation')["Corporation"]["logo"]){
				echo $ini."<img src=\"/".$this->Session->read('TheCorporation')["Corporation"]["logo"]."\" style=\"max-height: 40px;\">".$fin;
			} elseif ($this->Session->read('TheCompany')["Company"]["logo"]){
				echo $ini."<img src=\"/".$this->Session->read('TheCompany')["Company"]["logo"]."\" style=\"max-height: 40px;\">".$fin;
			}				
			?>				

			<ul class="nav navbar-nav navbar-right">
                  <?php if(isset($header_menu)){ ?>

                  <li class="dropdown">

              			<a href="javscript:;" class="dropdown-toggle" data-toggle="dropdown">
              				<i class="icon-user" style="color: white;"></i>
								<?php echo $username_menu; ?>
              				<b class="caret"></b>
              			</a>

              			<ul class="dropdown-menu">
              				<li><a href="/admin/users/resetpassword"><?php echo __("Password reset");?></a></li>
              				<li><a href="/admin/users/logout"><?php echo __("Logout");?></a></li>
              			</ul>
						
              		</li>

                  <?php } ?>
                </ul>


				<ul class="nav navbar-nav navbar-right">

                  <li class="">
				  
						<?php
						$elurl=null;
						// Si es un usuario logueado entonces coloca el redirector del cambio de idioma al home del usuario
						if ($this->Session->read('groupId')) {
							$elurl="/admin/users/home";
						// Si NO es un usuario logueado entonces coloca el redirector del cambio de idioma al home de la empresa
						} elseif ($this->params["slug"]) {
							$e=explode("/",$this->params["slug"]);
							$elurl="/".$e[0];
						}
						
						?>
						<?php if (! $this->Session->check('Config.LangVar')) : ?>
              			<a href="<?php echo $elurl;?>?lang=<?php
							
							if ($this->Session->read('Config.MyLangVar')=="esp"){
								echo "eng";
							} else {
								echo "esp";
							}
							
							?>">
							<?php
							if ($this->Session->read('Config.MyLangVar')=="esp"){
								echo "<img src=\"/img/en.png\">";
							} else {
								echo "<img src=\"/img/es.png\">";
							}
							?>
							</a>
						<?php endif; ?>
						
              		</li>

                </ul>

      </div><!-- /.navbar-collapse -->


	</div> <!-- /.container -->
</nav>


<?php if(isset($header_menu)){ //pr($header_menu); ?>

<div class="subnavbar">

	<div class="subnavbar-inner">
	
		<div class="container">
			
			<a data-target=".subnav-collapse" data-toggle="collapse" class="subnav-toggle" href="javascript:;">
		      <span class="sr-only">Toggle navigation</span>
		      <i class="icon-reorder"></i>
		      
		    </a>

			<div class="collapse subnav-collapse">
				<ul class="mainnav">
				
					<li class="<?php if ($isHome) echo "active";?>">
						<a href="/admin">
							<i class="icon-globe"></i>
							<span><?php echo __("Home");?></span>
						</a>	    				
					</li>

					<?php
                       foreach ($header_menu as $keymenu => $modules) {

                          foreach ($modules as $keymod => $module) {
						  
                    ?>

					
					<li class="dropdown <?php if(!$isHome and $keymod == $active_module){?>active<?php }?>">					
						<a data-toggle="dropdown" class="dropdown-toggle" href="javascript:;">
							<?php 
							if (isset($module['icon']) and $module['icon']) {
								echo "<i class=\"".$module['icon']."\"></i>";
							}
							?>
							<span><?php echo $module['name']; ?></span>
							<b class="caret"></b>
						</a>	    
					
						<ul class="dropdown-menu">
							<?php foreach ($module['categories'] as $keycat => $categories) {?>
								<li><a href="/admin/users/home/<?php echo $categories["id"];?>"><?php echo $categories['name'];?></a></li>
							<?php } ?>
						</ul> 				
					</li>

					<?php
							}

					}
					
                    ?>


					
				</ul>
			</div> <!-- /.subnav-collapse -->

		</div> <!-- /container -->
	
	</div> <!-- /subnavbar-inner -->

</div>

<?php } ?>

















