<?php
if (is_array($related_indicators) and sizeof($related_indicators)) {
?>

	<div class="page-header">
		<h3><?php echo __("Related Management Indicators");?></h3>
	</div>
	<?php
	$oldi=0;
	foreach($related_indicators as $i=>$r) {
		echo "<p style=\"margin-left: 30px\"><i class=\"icon-asterisk\"></i> ";
		echo "<a href=\"/admin/indicatorvalues?name=&indicator_id=".$r["Indicator"]["id"]."\">".$r["Indicator"]["name"]."</a>";
		?>
		<a class="btn-delete" href="/admin/nodes/deleteindicator/<?php echo $r["Indicator"]["id"];?>/<?php echo $node["Node"]["id"];?>/<?php echo $redir;?>" onclick="return confirm('<?php echo __("Are you sure do you want to delete this?");?>')">
		<i class="icon-trash"></i> <?php echo __("Delete relation");?></a>
		</p>
		<?php
	}
	
} else {
/*
	if ($node["Node"]["nodetype_id"] == Node::TYPE_COMMITMENT) {
		echo $this->element('warning', array('warning_message'=>__("This commitment has no management indicators associated, please add one.")));
	}
*/
}

if (is_array($indicators) and sizeof($indicators)) {
?>

<div id="but_add_ind"><p><a class="btn btn-primary noload" href="javascript:ShowHide('add_ind');">
	<i class="icon-plus"></i> <?php echo __("Add"). " " . __("Management Indicators");?></a></p></div>
			
<div id="add_ind" style="display:none"> 

	<div class="page-header">
	</div>
	<div class="widget stacked ">
	  <div class="widget-header">
		<i class="icon-plus"></i>
		<h3><?php echo __("Add"). " " . __("Related Management Indicators");?></h3>
	  </div> <!-- /widget-header -->
	  <div class="widget-content">
	
		<div class="col-md-6">
			<p><select id=elselect3 class="form-control">
			<option value=0><?php echo __("Choose");?></option>
			<?php
			foreach($indicators as $nid=>$n) {
				echo "<option value=".$nid.">".$n."</option>";
			}
			?>
			</select></p>
		</div>
		<div class="col-md-6" id="lalista4">
			<a onclick="elscript3(<?php echo $node["Node"]["id"];?>)" class="btn btn-primary noload" href="javascript:void(0);"> 
				<?php echo __("Add Relation");?>
			</a>
		</div>
	
	  </div> <!-- /widget-content -->
	</div>

</div>

<?php
}
?>
