<?php
App::uses('Graphdata', 'Model');

$graph_list = Graphdata::get_list();

?>

<script>
pos = 0; //row position
function nextStep(i) {

	$("#paso1").hide();
	$("#paso2").hide();
	$("#paso3").hide();
	$("#paso4").hide();

	window.location.href = "#inicio_la_data";
	$("#paso"+i).fadeIn();
	
}

function selGraph(id) {
	$('.pgraph_ico').each(function() {
		$this.removeClass('selected');
	});
	$('#'+id).addClass('selected');
	return false;
}

function guardaLaData(ns) {

	var series = [];
	
	var ladata = "";
	
	var serie_esp="";
	var serie_eng="";
	var serie1_data="";
	var serie2_data="";
	
	if ($("#serie_name1").val()=="" || $("#serie_name1_ang").val()=="") {
		alert("<?php echo __("The graph title cannot be empty");?>");
		return false;
	} else {
	
		$('.serie_label_esp').each(function() {
			if (serie_esp.length>0) {
				serie_esp = serie_esp+"|";
			}
			serie_esp = serie_esp+$(this).val();
		});
		
		$('.serie_label_eng').each(function() {
			if (serie_eng.length>0) {
				serie_eng = serie_eng+"|";
			}
			serie_eng = serie_eng+$(this).val();
		});
		
		$('.serie1_data').each(function() {
			if (serie1_data.length>0) {
				serie1_data = serie1_data+"|";
			}
			serie1_data = serie1_data+$(this).val();
		});
		
		$('.serie2_data').each(function() {
			if (serie2_data.length>0) {
				serie2_data = serie2_data+"|";
			}
			serie2_data = serie2_data+$(this).val();
		});
		
		ladata = ladata + serie_esp + "||" + serie_eng + "||" + serie1_data + "||" + serie2_data;
			
		var myRadio = $('input[name=type]');
		var checkedValue = myRadio.filter(':checked').val();
		
		$.ajax({
			type: "POST",
			url: "/admin/entities/savedatagraph",
			data: { 
				name: $("#name").val(),
				name_eng: $("#name_eng").val(),
				name_eng: $("#name_eng").val(),
				serie_name1: $("#serie_name1").val(),
				serie_name1_eng: $("#serie_name1_eng").val(),
				serie_name2: $("#serie_name2").val(),
				serie_name2_eng: $("#serie_name2_eng").val(),
				type: checkedValue,
				ladata: ladata,
				id: <?php if ($node["Node"]["id"]) echo $node["Node"]["id"]; else echo 0; ?>,
				redir: '<?php echo $redir;?>'
			},
			success: function(data, textStatus) {
				document.location.href='/admin/entities/graph/<?php if ($node["Node"]["id"]) echo $node["Node"]["id"]; else echo 0; ?>';
			},
			error: function() {
				console.log('Error loading.');
			}
		});
	}
}
</script>

<a name="inicio_la_data"></a>
<div id="la_data" style="display: none" class="alert alert-warning">
	<div class="col-md-6">
		<div class="page-header">
			<h3><?php echo __("Graph Setup");?></h3>			 									
		</div>
	</div>
	<div class="col-md-6" style="text-align: right;">
		<div id="el_boton_de_la_data_dentro">
			<p>
				<a href="javascript:escondeLaData();" class="btn btn-default noload">
					<i class="icon-collapse"></i> <?php echo __("Close");?>
				</a>
			</p>
		</div>
	</div>
	<div style="clear: both;"></div>

<div class="col-md-12" id="paso1">
	<h4><?php echo __("Graph Type");?> </h4> 

	<?php
	foreach ($graph_list as $key => $value) {
	?>		
	<div class="col-md-3">
		<img src="/pchart/img/<?=$key?>.png" class="pgraph_ico"> 
		<label><input type=radio name="type" value="<?=$key?>" 
		<?php if ($graphdata[0]["Graphdata"]["type"]==$key) echo "checked";?>
		> <?php echo $value; ?></label>
	</div>
	<?php
	}
	?>

	<div style="clear: both;"></div>

	<div align=right><input type=button onclick="nextStep(2);" value="<?php echo __("Next")." &raquo;";?>" class="btn btn-default noload"></div>

</div>

<div class="col-md-12" id="paso2" style="display: none;">
<?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
	<div class="col-md-6">
	<label><?php echo __("Graph Title - Not visible to the public");?> (ESP)</label> 
		<input type=text id="name" class="form-control" value="<?php echo $graphdata[0]["GraphdatanameTranslation"][0]["content"];?>">
	</div>
	<?php endif; ?>
	<?php if (! $this->Session->check('Config.LangVar') 
    or ($this->Session->check('Config.LangVar') 
    and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
	<div class="col-md-6">
	<label><?php echo __("Graph Title - Not visible to the public");?> (ENG)</label> 
		<input type=text id="name_eng" class="form-control" value="<?php echo $graphdata[0]["GraphdatanameTranslation"][1]["content"];?>">
	</div>
	<?php endif; ?>
	<div style="clear: both;"></div>
	<div class="col-md-6">
		<input type=button onclick="nextStep(1);" value="<?php echo "&laquo; ".__("Previous");?>" class="btn btn-default noload">
	</div>
	<div align=right><input type=button onclick="nextStep(3);" value="<?php echo __("Next")." &raquo;";?>" class="btn btn-default noload"></div>
</div>
<div class="col-md-12" id="paso3">
	<?php
	$data_del_grafico = unserialize($graphdata[0]["Graphdata"]["data"]);
	?>		
	<!--<div class="col-md-3">
		<h4><?php echo __("Data Serie")." 1";?></h4>
	</div>

	<div class="col-md-3">
		<h4><?php echo __("Data Serie")." 2 (".__("Optional").")";?></h4>
	</div>
	
	<div class="col-md-6">
	</div>-->
	
	<div class="col-md-3">
	<label><?php echo __("Serie Name")." 1 (ESP)";?></label> <input type=text id="serie_name1" class="form-control" 
		value="<?php echo $data_del_grafico[0]["serie_name1"];?>"
	>
	</div>	
	
	<div class="col-md-3">
	<label><?php echo __("Serie Name")." 2 (ESP)";?></label> <input type=text id="serie_name2" class="form-control" 
		value="<?php echo $data_del_grafico[0]["serie_name2"];?>"
	>
	</div>

	
	<div class="col-md-3">
	<label><?php echo __("Serie Name")." 1 (ENG)";?></label> <input type=text id="serie_name1_eng" class="form-control" 
		value="<?php echo $data_del_grafico[0]["serie_name1_eng"];?>"
	>
	</div>

	<div class="col-md-3">
	<label><?php echo __("Serie Name")." 2 (ENG)";?></label> <input type=text id="serie_name2_eng" class="form-control" 
		value="<?php echo $data_del_grafico[0]["serie_name2_eng"];?>"
	>
	</div>
	
	<div class="col-md-6">
		<input type=button onclick="nextStep(2);" value="<?php echo "&laquo; ".__("Previous");?>" class="btn btn-default noload">
	</div>
	<div align=right><input type=button onclick="nextStep(4);" value="<?php echo __("Next")." &raquo;";?>" class="btn btn-default noload"></div>
	<div style="clear: both;"></div>
</div>
<div class="col-md-12" id="paso4">	
	<div id="the_data_1">
		<div class="col-md-3"><label><?php echo __("Label")." (ESP)";?></label></div>
		<div class="col-md-3"><label><?php echo __("Label")." (ENG)";?></label></div>
		<div class="col-md-2"><label><?php echo __("Value");?></label></div>
		<div class="col-md-2"><label><?php echo __("Value");?></label></div>
		<div class="col-md-2"><label><?php echo __("Delete");?></label></div>
	</div>		
	<div class="col-md-11"><a href="javascript:void(0);" onclick="AddRow('','','','')"><i class="icon-plus"></i> <?php echo __("Add Row");?></a></div>
	
	
	<div class="col-md-6">
		<input type=button onclick="nextStep(3);" value="<?php echo "&laquo; ".__("Previous");?>" class="btn btn-default noload">
	</div>
	<div class="col-md-6" style="text-align: right;">
		<button onclick="javascript:guardaLaData(2);" class="btn btn-default">
			<i class="icon-save"></i> <?php echo __("Save")." &raquo;";?>
		</button>
	</div>
	
	<p>&nbsp;</p>

</div>

<div style="clear: both;"></div>

<div id=lalistadata></div>

</div>

<div id="el_boton_de_la_data">
	<p>
		<a href="javascript:muestraLaData();" class="btn btn-default noload">
			<i class="icon-collapse"></i> <?php echo __("Graph Manager");?>
		</a>
	</p>
</div>
<div id="el_boton_de_la_data_1">
	<p>
		<a href="javascript:muestraLaDataParcial(1);" class="btn btn-default noload">
			<i class="icon-edit"></i> <?php echo __("Change Graph Type");?>
		</a>
	</p>
</div>
<div id="el_boton_de_la_data_2">
	<p>
		<a href="javascript:muestraLaDataParcial(2);" class="btn btn-default noload">
			<i class="icon-edit"></i> <?php echo __("Change Title");?>
		</a>
	</p>
</div>
<div id="el_boton_de_la_data_3">
	<p>
		<a href="javascript:muestraLaDataParcial(3);" class="btn btn-default noload">
			<i class="icon-edit"></i> <?php echo __("Change Title of Data Serie");?>
		</a>
	</p>
</div>
<div id="el_boton_de_la_data_4">
	<p>
		<a href="javascript:muestraLaDataParcial(4);" class="btn btn-default noload">
			<i class="icon-edit"></i> <?php echo __("Change Values");?>
		</a>
	</p>
</div>

<script>
function escondeLaData() {
	$("#el_boton_de_la_data_dentro").hide();
	$("#la_data").hide();
	$("#el_boton_de_la_data").fadeIn();
	$("#el_boton_de_la_data_1").fadeIn();
	$("#el_boton_de_la_data_2").fadeIn();
	$("#el_boton_de_la_data_3").fadeIn();
	$("#el_boton_de_la_data_4").fadeIn();
}

function muestraLaData() {
	$("#el_boton_de_la_data").hide();
	$("#el_boton_de_la_data_1").hide();
	$("#el_boton_de_la_data_2").hide();
	$("#el_boton_de_la_data_3").hide();
	$("#el_boton_de_la_data_4").hide();
	$("#el_boton_de_la_data_dentro").fadeIn();
	$("#la_data").fadeIn();
	nextStep(1);
}

function muestraLaDataParcial(i) {
	$("#el_boton_de_la_data").hide();
	$("#el_boton_de_la_data_1").hide();
	$("#el_boton_de_la_data_2").hide();
	$("#el_boton_de_la_data_3").hide();
	$("#el_boton_de_la_data_4").hide();
	$("#el_boton_de_la_data_dentro").fadeIn();
	$("#la_data").fadeIn();
	if (i != 4) {
		$("#paso" + i).append(
			'<div class="col-md-11" style="text-align: left;">'+
		'<button onclick="javascript:guardaLaData(2);" class="btn btn-default">'+
			'<i class="icon-save"></i> <?php echo __("Save")." &raquo;";?>'+
		'</button>'+
	'</div>');		
	}
	nextStep(i);
}


function DeleteRow(position) 
{
	$("#pos"+position).remove();
} 

function AddRow(esp, eng, data, data2) 
{
	$("#the_data_1").append('<div id="pos'+pos+'" >'+
		'<div class="col-md-3"><input type=text class="serie_label_esp form-control" value="'+esp+'"></div>'+
		'<div class="col-md-3"><input type=text class="serie_label_eng form-control" value="'+eng+'"></div>'+
		'<div class="col-md-2"><input type=number class="serie1_data form-control" value="'+data+'"></div>'+
		'<div class="col-md-2"><input type=number class="serie2_data form-control" value="'+data2+'"></div>'+
		'<div class="col-md-2"><input type=button onclick="DeleteRow('+pos+');" value="x"></div>'+
		'</div>');
		pos++;
}
</script>

<?php

//pr($data_del_grafico);

if ($data_del_grafico["esp"]) {
	foreach($data_del_grafico["esp"] as $i=>$d) {
		echo "<script>AddRow('".$data_del_grafico["esp"][$i]."', '".$data_del_grafico["eng"][$i]."', '".$data_del_grafico["data"][$i]."', '".$data_del_grafico["data2"][$i]."');</script>";
	}
}

if (is_array($data_del_grafico[$this->Session->read('Config.MyLangVar')]) and is_array($data_del_grafico["data"])) {

	$s1 = implode(",",$data_del_grafico["data"]);
	$s2 = implode(",",$data_del_grafico[$this->Session->read('Config.MyLangVar')]);

	if ($this->Session->read('Config.MyLangVar')=="esp") {
		echo "<h3>".$graphdata[0]["GraphdatanameTranslation"][0]["content"]."</h3>";
	} else {
		echo "<h3>".$graphdata[0]["GraphdatanameTranslation"][1]["content"]."</h3>";
	}
	
	if (is_numeric($graphdata[0]["Graphdata"]["type"])) {
		echo "<p><img src=\"/pchart/Example".$graphdata[0]["Graphdata"]["type"].".php?g=".$node["Node"]["id"]."&lang=".$this->Session->read('Config.MyLangVar')."\" style=\"max-width: 100%;\"></p>";
	}
}

