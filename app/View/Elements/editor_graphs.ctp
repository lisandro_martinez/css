<?php

$pchart = $template.'_'.$field.'_pchart';

if (is_array($graphs_urls)) {

	echo "<select id=\"graph_urls\" onchange=\"updateGraph()\" class=\"form-control\" name='editor__text__".$template."__".$field."__graph'>";
	echo "<option value=0>".__("Upload Image")."</option>";
	foreach($graphs_urls as $i=>$gu) {
		echo "<option value=$i>{$gu["name"]}</option>";
	}
	echo "</select>";
	echo "<div id='$pchart'></div>";
}
echo "<script>
		function updateGraph() {
			if ($(\"#graph_urls\").val() == 0) {
				$(\".image-editor\").show(\"slow\");
				$(\"#".$pchart."\").hide(\"slow\");
			} else {
				$(\".image-editor\").hide(\"slow\");
				$(\"#".$pchart."\").show(\"slow\");
			}";

if (is_array($graphs_urls)) {
	foreach($graphs_urls as $i=>$gu) {
		echo "if ($(\"#graph_urls\").val() == $i) {";
			echo "$(\"#".$pchart."\").hide('slow');";
			echo "$(\"#".$pchart."\").html('');";
			if ($esp) {
				echo "$(\"#".$pchart."\").append('<p><img src=\"".$gu["url"]."esp\" style=\"max-width: 100%\"></p>');";
			}
			if ($eng) {
				echo "$(\"#".$pchart."\").append('<p><img src=\"".$gu["url"]."eng\" style=\"max-width: 100%\"></p>');";
			}
			echo "$(\"#".$pchart."\").show('slow');";
		echo "}";
	}
}
echo "}
	</script>";

?>
