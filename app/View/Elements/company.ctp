		<div class="col-md-4">

			<div class="widget stacked ">

			  <div class="widget-header">
				<i class="icon-bookmark"></i>
				<h3><?php echo $la_comp["Company"]["name"];?></h3>
			  </div> <!-- /widget-header -->

			  <div class="widget-content">
			  
				<?php if( $la_comp["Company"]["logo"] ) { ?>
					<p><img src="/<?php echo $la_comp["Company"]["logo"];?>" style="max-width: 250px;"></p>
				<?php } ?>

				<?php if( $la_comp["Company"]["description"] ) { ?>
					<p><?php echo $la_comp["Company"]["description"];?></p>
				<?php } ?>

				<?php if( $la_comp["Company"]["address"] ) { ?>
					<p><i class="icon-map-marker"></i> <?php echo $la_comp["Company"]["address"];?></p>
				<?php } ?>
					
				<?php if( $la_comp["Company"]["phone"] ) { ?>
					<p><i class="icon-phone"></i> <?php echo $la_comp["Company"]["phone"];?></p>
				<?php } ?>
				
				<div style="text-align: center; font-size: large;">
				<?php if( $la_comp["Company"]["url"] ) { ?>
					<a href="<?php echo $la_comp["Company"]["url"]; ?>" target="_blank" style="padding: 10px"><i class="icon-home"></i></a> 
				<?php } ?>
				<?php if( $la_comp["Company"]["fb"] ) { ?>
					<a href="//facebook.com/<?php echo $la_comp["Company"]["fb"]; ?>" target="_blank" style="padding: 10px"><i class="icon-facebook"></i></a> 
				<?php } ?>
				<?php if( $la_comp["Company"]["t"] ) { ?>
					<a href="//twitter.com/<?php echo $la_comp["Company"]["t"]; ?>" target="_blank" style="padding: 10px"><i class="icon-twitter"></i></a> 
				<?php } ?>
				<?php if( $la_comp["Company"]["ln"] ) { ?>
					<a href="//linkedin.com/<?php echo $la_comp["Company"]["ln"]; ?>" target="_blank" style="padding: 10px"><i class="icon-linkedin"></i></a> 
				<?php } ?>
				<?php if( $la_comp["Company"]["y"] ) { ?>
					<a href="//youtube.com/<?php echo $la_comp["Company"]["y"]; ?>" target="_blank" style="padding: 10px"><i class="icon-youtube"></i></a> 
				<?php } ?>
				</div>
				
			  </div> <!-- /widget-content -->

			</div> <!-- /widget -->

		</div>

