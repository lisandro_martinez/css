<div class="alert alert-danger alert-dismissable">
	<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
	<strong><i class="icon-warning-sign"></i> <?php echo __("Warning");?></strong><hr>
	<?php echo $warning_message;?>
</div>
