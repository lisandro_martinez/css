<div class="form-group col-lg-6">
	<label for="<?=$field?>">
	<?php 
	$field_name = 'editor__text__'.$template.'__'.$field.'__'.$lang;
	echo $title.' ('.strtoupper($lang).')';
	echo '<input id="'.$field_name.'" name="'.$field_name.'" type="text" class="form-control" value="000000" maxlength="6" size="6" />';
	?>
	</select>
	</label>
</div>

<link rel="stylesheet" media="screen" type="text/css" href="/js/colorpicker/css/colorpicker.css" />
<script type="text/javascript" src="/js/colorpicker/js/colorpicker.js"></script>

<script>

$('#<?=$field_name?>').ColorPicker({
	onSubmit: function(hsb, hex, rgb, el) {
		$(el).val(hex);
		$(el).ColorPickerHide();
	},
	onBeforeShow: function () {
		$(this).ColorPickerSetColor(this.value);
	}
})
.bind('keyup', function(){
	$(this).ColorPickerSetColor(this.value);
});

</script>
