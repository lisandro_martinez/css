<nav class="navbar navbar-inverse" role="navigation">

	<div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/admin"><img src="/img/logo-css.png" class="logo-css"></a>
		

      </div>

	<ul class="nav navbar-nav navbar-right">

	  <li class="">

			<a href="/admin/users/login?lang=<?php
				
				if ($this->Session->read('Config.MyLangVar')=="esp"){
					echo "eng";
				} else {
					echo "esp";
				}
				
				?>">
				<?php
				if ($this->Session->read('Config.MyLangVar')=="esp"){
					echo "<img src=\"/img/en.png\">";
				} else {
					echo "<img src=\"/img/es.png\">";
				}
				?>
				</a>
			
		</li>

	</ul>


  </div> <!-- /.container -->
  
</nav>
