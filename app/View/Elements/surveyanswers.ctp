<script>
function deleteSurveyAnswer(i, id) {
	if (confirm("<?php echo __("Are you sure do you want to delete this?");?>")==true) {
		$.ajax({
			type: "POST",
			url: "/admin/nodes/delsurveyanswer",
			data: { 
				id: id
			},
			success: function(data, textStatus) {
				document.location.href="/admin/entities/survey/"+i;
			},
			error: function() {
				console.log('Error loading.');
			}
		});
	}
	return false;
}
</script>

<div id="las_respuestas" style="display: none">

<div class="page-header">
	<h3><?php echo __("Answers Details");?></h3>			 									
</div>

<?php

if (is_array($answers) and sizeof($answers)) {

	if (is_array($answers) and sizeof($answers)) {

		foreach($answers as $a) {

//			pr($a);
			echo "<div class=\"col-md-3\"><i class=icon-calendar></i> ".$a["Surveyanswer"]["datetime"]."</div>";
			echo "<div class=\"col-md-3\"><i class=icon-user></i> ".$a["Surveyanswer"]["name"]."</div>";
			echo "<div class=\"col-md-3\"><i class=icon-envelope></i> ".$a["Surveyanswer"]["email"]."</div>";
			echo "<div class=\"col-md-3\"><i class=icon-credit-card></i> ".$a["Surveyanswer"]["idcard"]."</div>";
			echo "<div style=\"clear: both; margin-bottom: 20px;\"></div>";

			$data=unserialize($a["Surveyanswer"]["data"]);
			
			if (is_array($data) and is_array($questions)) {
				foreach($data as $id => $d) {
					// busca la pregunta...
					$la_preg=null;
					foreach($questions as $q) {
						if ($q["Surveyquestion"]["id"]==$id) {
							$la_preg=$q;
							break;
						}
					}
					if ($la_preg) {
						echo "<h4>".$la_preg["Surveyquestion"]["name"]."</h4>";
						switch($la_preg["Surveyquestion"]["question_type"]) {
						case 1:
						case 2:
							$las_resp = explode(PHP_EOL, $la_preg["Surveyquestion"]["answers"]);
							if ($la_preg["Surveyquestion"]["question_type"]==1) {
								echo "<p style=\"margin-left: 20px;\"><i class=\"icon-chevron-right\"></i> ".$las_resp[$d]."</p>";
							} else {
								if (is_array($d)) {
									foreach ($d as $d2) {
										echo "<p style=\"margin-left: 20px;\"><i class=\"icon-chevron-right\"></i> ".$las_resp[$d2]."</p>";
									}
								}
							}
							break;
						case 3:
							echo "<p class=\"pre\">".$d."</p>";
							break;
						}
					}
				}
			}
			
			echo "<a class=\"btn btn-danger\" onclick=\"deleteSurveyAnswer(".$elnodeid.", ".$a["Surveyanswer"]["id"].");\">".__("Delete")."</a> ";

			echo "<hr>";
				
		}

	}


} else {

	echo __("No answers were found");
	
}

?>

<div id="el_boton_de_las_respuestas_dentro">
	<p>
		<a href="javascript:escondeLasRespuestas();" class="btn btn-default noload">
			<i class="icon-collapse"></i> <?php echo __("Close Answers Detail");?>
		</a>
	</p>
</div>

</div>

<div id="el_boton_de_las_respuestas">
	<p>
		<a href="javascript:muestraLasRespuestas();" class="btn btn-default noload">
			<i class="icon-collapse"></i> <?php echo __("View Answers Detail");?>
		</a>
	</p>
</div>

<script>
function escondeLasRespuestas() {
	$("#el_boton_de_las_respuestas_dentro").hide();
	$("#las_respuestas").hide();
	$("#el_boton_de_las_respuestas").fadeIn();
}
function muestraLasRespuestas() {
	$("#el_boton_de_las_respuestas").hide();
	$("#el_boton_de_las_respuestas_dentro").fadeIn();
	$("#las_respuestas").fadeIn();
}
</script>

