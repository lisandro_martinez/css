<div id="las_preguntas" style="display: none">

<div class="page-header">
	<h3><?php echo __("Questions");?></h3>			 									
</div>

<?php

if (is_array($questions) and sizeof($questions)) {

	?>
	<div class="faq-container">
		<ol style="margin-top: 0px;" class="faq-list">
	<?php
	foreach($questions as $q) {
	?>
		<li id="faq-1">
			<div class="faq-icon">
				<div class="faq-number"><?php echo $q["Surveyquestion"]["order"];?></div>
			</div>
			<div class="faq-text">
				<h3><?php echo $q["Surveyquestion"]["name"];?></h3>
				<p><?php echo $q["Surveyquestion"]["description"];?></p>
				<?php 
				
				if ($this->Session->read('Config.MyLangVar')=="esp") {
					$question_types = array(1=>"Selección Simple", 2=>"Selección Múltiple", 3=>"Texto");
				} else {
					$question_types = array(1=>"Single Selection", 2=>"Multiple Selection", 3=>"Text");
				}
				
				echo "<p><b>".__("Type").": ".$question_types[$q["Surveyquestion"]["question_type"]]."</b></p>";
				
				$respuestas = explode(PHP_EOL, $q["Surveyquestion"]["answers"]);
				if (is_array($respuestas) and sizeof($respuestas)) {
					echo "<ul>";
					foreach($respuestas as $res) {
						if ($res) 
							echo "<li><i class=\"icon-chevron-right\"></i> $res</li>";
					}
					echo "</ul>";
				}
				?>
				<p>
				<a class="btn-edit" href="/admin/surveyquestions/edit/<?php echo $q["Surveyquestion"]["id"]."/$redir";?>">
				<i class="icon-edit"></i> <?php echo __("Edit");?></a>
				<a class="btn-delete" href="/admin/surveyquestions/delete/<?php echo $q["Surveyquestion"]["id"]."/$redir";?>" onclick="return confirm('<?php echo __("Are you sure do you want to delete this?");?>)">
				<i class="icon-trash"></i> <?php echo __("Delete");?></a>
				</p>
			</div>
		</li>
	<?php
	}
	?>
		</ol>
	</div>
	<?php
	
} else {
	echo __("No questions were found in the survey");
}

?>

<div id="but_add_rel_ent">
	<p>
		<a href="/admin/surveyquestions/add/<?php echo $node["Node"]["id"];?>" class="btn btn-primary noload">
			<i class="icon-plus"></i> <?php echo __("Add")." ".__("Question");?>
		</a>
	</p>
</div>

<div id="el_boton_de_las_preguntas_dentro">
	<p>
		<a href="javascript:escondeLasPreguntas();" class="btn btn-default noload">
			<i class="icon-collapse"></i> <?php echo __("Close Question Manager");?>
		</a>
	</p>
</div>

</div>

<div id="el_boton_de_las_preguntas">
	<p>
		<a href="javascript:muestraLasPreguntas();" class="btn btn-default noload">
			<i class="icon-collapse"></i> <?php echo __("Question Manager");?>
		</a>
	</p>
</div>

<script>
function escondeLasPreguntas() {
	$("#el_boton_de_las_preguntas_dentro").hide();
	$("#las_preguntas").hide();
	$("#el_boton_de_las_preguntas").fadeIn();
}
function muestraLasPreguntas() {
	$("#el_boton_de_las_preguntas").hide();
	$("#el_boton_de_las_preguntas_dentro").fadeIn();
	$("#las_preguntas").fadeIn();
}
</script>

