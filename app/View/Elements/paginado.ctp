
<p class="summarypaginator col-md-6">
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, Showing {:current} records(s) of {:count} in total')
	));
	?>	</p>

    <ul class="pagination">
        <li class='recordsforpage'>
            <p><?php echo __('Records per page'); ?></p>
            <select name="recordsforpage" id="recordsforpage">
                 <?php foreach ($optionsrecors as $key => $value) { ?>
                 <option value="<?php echo $value;?>" <?php if($value == $recordsforpage){?>selected<?php }?>>
                    <?php echo $value;?>
                </option>
                 <?php } ?>
            </select>
        </li>
				<?php
				$param_paginator= $this->Paginator->params();
				if(($param_paginator['prevPage'])||($param_paginator['nextPage'])){
				?>
            <?php
                echo $this->Paginator->prev(__('<<'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('>>'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
            ?>
				<?php }?>
     </ul>
