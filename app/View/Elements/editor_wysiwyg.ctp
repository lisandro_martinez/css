<div class="form-group col-lg-12">
	<label for="<?=$field?>">
	<?php 
	echo $title.' ('.strtoupper($lang).')';
	echo '</label>';
	
	$name = 'editor__text__'.$template.'__'.$field.'__'.$lang;
	
	?>
	<p>
		<span class="btn btn-info fileinput-button" onclick="useCSSData('<?=$name?>')">
			<i class="glyphicon glyphicon-plus"></i>
			<span><?php echo __("Use CSS Data");?>...</span>
		</span>
	</p>
	<?php
	echo '<textarea class="wysiwyg" id="'.$name.'" name="'.$name.'">'.$value.'</textarea>';
	
	?>
</div>

<script type="text/javascript">
	
	// Inicio de Summer Note
	$(document).ready(function() {
		$('.wysiwyg').summernote({
			height: 300,
			toolbar: [
				// [groupName, [list of button]]
				['style', ['style', 'bold', 'italic', 'underline', 'clear']],
				['fontsize', ['fontsize']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['height', ['height']],
				['picture', ['picture']],
				['codeview', ['codeview']]
			]
		});
	});
	// Fin de Summer Note

	
	function useCSSData(upload_id) {

		var w = 500;
		var h = 400;

		var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
		var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

		var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
		var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

		var left = ((width / 2) - (w / 2)) + dualScreenLeft;
		var top = ((height / 2) - (h / 2)) + dualScreenTop;
		var newWindow = window.open('/admin/Reportpages/cssdata/?upload_id='+upload_id, 'cssdata', 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

		// Puts focus on the newWindow
		if (window.focus) {
			newWindow.focus();
		}

	}
		
</script>

