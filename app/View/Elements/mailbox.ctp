<?php if (!$pdf) { ?>
<script>
function deleteComment(i, id) {
	if (confirm("<?php echo __("Are you sure do you want to delete this?");?>")==true) {
		$.ajax({
			type: "POST",
			url: "/admin/nodes/delmessage",
			data: { 
				id: id
			},
			success: function(data, textStatus) {
				document.location.href="/admin/entities/mailbox/"+i;
			},
			error: function() {
				console.log('Error loading.');
			}
		});
	}
	return false;
}
function addAsCommitment(i, id) {
	if (confirm("<?php echo __("Are you sure do you want to add this suggestion as a Commitment?");?>")==true) {
		document.location.href="/admin/nodes/addascommitment/"+id;
	}
	return false;
}
</script>

<?php } ?>

<div class="page-header">
	<h3><?php echo __("Suggestions mailbox");?></h3>			 									
</div>


<?php

if (is_array($msgs) and sizeof($msgs)) {

	foreach($msgs as $msg) {
	
		echo "<p><i class=icon-calendar></i> ".$msg["Mailboxsuggestion"]["datetime"]."</p>";
		echo "<p><i class=icon-user></i> ".$msg["Mailboxsuggestion"]["name"]."</p>";
		echo "<p><i class=icon-envelope></i> ".$msg["Mailboxsuggestion"]["email"]."</p>";
		echo "<p><i class=icon-credit-card></i> ".$msg["Mailboxsuggestion"]["idcard"]."</p>";
		echo "<p class=\"pre\">".$msg["Mailboxsuggestion"]["comment"]."</p>";

		if (!$pdf) {
			echo "<a class=\"btn btn-danger\" onclick=\"deleteComment(".$id.", ".$msg["Mailboxsuggestion"]["id"].");\">".__("Delete")."</a> ";
			echo "<a class=\"btn btn-primary\" onclick=\"addAsCommitment(".$id.", ".$msg["Mailboxsuggestion"]["id"].");\">".__("Add as Commitment")."</a>";
		}

		echo "<hr>";
	
	}

} else {
	if (!$pdf) {
		echo __("No messages were found");
	}
}
