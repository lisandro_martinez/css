<?php

// Selecciona el grupo de interés a enviar
if (is_array($interest_groups) and sizeof($interest_groups)) {
	?>
	<div class="col-md-12">
		<div class="page-header">
			<h4><?php echo __("Send Call");?></h4>
		</div>
		<h4><?php echo __("Choose Interest Group");?></h4>
		<p><select id=elselect4 class="form-control">
		<option value=0><?php echo __("Choose");?></option>
		<?php
		foreach($interest_groups as $ig) {
			echo "<option value=".$ig["Node"]["id"].">".$ig["Node"]["name"]."</option>";
		}
		?>
		</select>
		<div id="but_add_rel_ent">
			<a class="btn btn-primary noload" href="javascript:elscript4(<?php echo $node["Node"]["id"];?>);">
			<i class="icon-envelope"></i> <?php echo __("Send Call");?></a>
		</div>
		</p>
	</div>
	<div class="col-md-12">
		<?php
		$tit='<div class="page-header"><h4>'.__("Calls sent or in process").'</h4>';
		?>
		</div>
		<?php
		$titusado=false;
		foreach($interest_groups as $ig) {
			if (is_array($ig["Calls"]) and sizeof($ig["Calls"])) {
				foreach($ig["Calls"] as $c) {
				
					if (!$titusado) {
						echo $tit;
						$titusado=true;
					}
				
					echo "<p>".
						"<i class='icon-calendar'></i> " . 
							date("d/m/Y H:i", strtotime($c["Interestgroupcall"]["datetime"])) . 
							" &nbsp; ".
						"<i class='icon-user'></i> " . $ig["Node"]["name"]."</p>";
				}
			}
		}
		?>
	</div>
	<?php
}
