<div class="page-header">
	<h3><?php echo __("Predecessors");?></h3>
</div>
<?php
foreach($predecessors as $i=>$r) {
	echo "<p style=\"margin-left: 30px\"><i class=\"icon-asterisk\"></i> ";
	echo "<a href=\"/admin/entities/".laVista($r["Node"]["nodetype_id"])."/".$r["Node"]["id"]."\">".$r["Node"]["name"]."</a>";
	?>
	<a class="btn-delete" href="/admin/nodes/deletepredecessor/<?php echo $r["Node"]["id"];?>/<?php echo $node["Node"]["id"];?>/<?php echo $redir;?>" onclick="return confirm('<?php echo __("Are you sure do you want to delete this?");?>')">
	<i class="icon-trash"></i> <?php echo __("Delete relation");?></a>
	</p>
	<?php
}
?>
	
<div id="but_add_pre"><p><a class="btn btn-primary noload" href="javascript:ShowHide('add_pre');">
	<i class="icon-plus"></i> <?php echo __("Add Predecessor");?></a></p></div>
			
<div id="add_pre" style="display:none"> 

	<div class="page-header">
	</div>
	<div class="widget stacked ">
	  <div class="widget-header">
		<i class="icon-plus"></i>
		<h3><?php echo __("Add Predecessor");?></h3>
	  </div> <!-- /widget-header -->
	  <div class="widget-content">
	
		<div class="col-md-6">
			<p><select id=elselect2 class="form-control" onchange="elscript2()">
			<option value=0><?php echo __("Choose");?></option>
			<?php
			foreach($nodetypes as $nid=>$n) {
				if ($nid>=11 and $nid<=15) {
					echo "<option value=".$nid.">".$n."</option>";
				}
			}
			?>
			</select></p>
		</div>
		<div class="col-md-6" id="lalista3">
		</div>
	
	  </div> <!-- /widget-content -->
	</div>
</div>
