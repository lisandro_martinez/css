<?php
// prepara el upload de imágenes
?>
<script>
$("#divupload").show("slow");
$("#gallery_id").val(<?php echo $node["Node"]["id"];?>);
$("#ela").attr("href", "/admin/entities/gallery/<?php echo $node["Node"]["id"];?>")
</script>

<div class="page-header">
	<h3><?php echo __("Gallery Images");?></h3>			 									
</div>


<?php

// muestra las imágenes de la galería actual

if (is_array($imgs["imgs"]) and sizeof($imgs)) {
	$i=0;
	foreach($imgs["imgs"] as $f) {
		echo '<div class="col-md-3">';
		
		echo "<img src=\"".$imgs["dir"]."/".$f["file"]."\" style=\"width: 100%;\">";
		
		echo "<p><input type=text id=\"".$i."_esp\" class=\"form-control\" placeholder=\"Escriba una Descripción\" style=\"font-size: small\" value=\"".$f["desc"]["esp"]."\" onchange=\"return updateImg(".$i.", '".$imgs["dir"]."/".$f["file"]."');\"></p>";
		
		echo "<p><input type=text id=\"".$i."_eng\" class=\"form-control\" placeholder=\"Write a Description\" style=\"font-size: small\" value=\"".$f["desc"]["eng"]."\" onchange=\"return updateImg(".$i.", '".$imgs["dir"]."/".$f["file"]."');\"></p>";
		
//							echo "<p><a class=\"btn-edit noload\" href=\"javascript:void(0);\"><i class=\"icon-edit\"></i> ".__("Update")."</a>";
		
		echo "<a 	class=\"btn-delete\" 
					href=\"javascript:void(0);\" 
					onclick=\"return delImg(".$node["Node"]["id"].", '".$imgs["dir"]."/".$f["file"]."')\">
					<i class=\"icon-trash\"></i> ".__("Delete").
			"</a></p>";
		
		echo "<div id=\"result_".$i."\"></div>";
		
		echo "<hr>";
		
		echo '</div>';
		
		$i++;
		
	}
} else {
	echo __("No images were found in the gallery");
}
