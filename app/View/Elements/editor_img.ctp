<div class="form-group col-md-6">

	<div class="image-editor">
		
		<label for="<?=$field?>">
		<?php
		$upload_id = $template.'_'.$field.'_'.$lang;
		echo $title.' ('.strtoupper($lang).')';
		echo '</label>';
		?>
		
		<div>
			<!-- The fileinput-button span is used to style the file input field as button -->
			<!-- The container for controls -->
			<div id="controls<?=$upload_id?>"
				<?php
				if ($value) {
					echo ' style="display: none" ';
				}
				?>
			>
			
				<!-- form input for image url -->
				<?php
				echo '<input type=hidden id="value'.$upload_id.'" name="editor__text__'.$template.'__'.$field.'__'.$lang.'" value="'.$value.'">';
				?>
			
				<span class="btn btn-info fileinput-button" onclick="useCSSObject('<?=$upload_id?>')">
					
					<i class="glyphicon glyphicon-plus"></i>
					<span><?=__("Use Graph")?>...</span>
					
				</span>
				
				<br/>
				<br/>
				
				<span class="btn btn-success fileinput-button">
					
					<i class="glyphicon glyphicon-plus"></i>
					<span><?=__("Select files")?>...</span>
					
					<!-- The file input field used as target for the file upload widget -->
					<input id="<?=$upload_id?>" type="file" name="files[]">
					
				</span>

				<br>
				<br>
				<!-- The global progress bar -->
				<div id="progress<?=$upload_id?>" class="progress" style="width: 100%;">
					<div class="progress-bar progress-bar-success"></div>
				</div>
				<div id=message<?=$upload_id?> style="width: 100%; display: none;">
					<div class="alert alert-danger alert-dismissable">
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<strong><i class="icon-danger-sign"></i> <?php echo __("Warning");?></strong><hr>
						<?php echo __('An error ocurred uploading file. Please make sure it is an image: jpg, png or gif.');?>
					</div>
				</div>
			</div>
			<!-- The container for the uploaded files -->
			<div id="files<?=$upload_id?>" class="files">
			<?php
			if ($value) {
				
				$thumb='';
				$arr_value = explode('/', $value);
				
				foreach($arr_value as $i=>$av) {
					
					if ($i==sizeof($arr_value)-1) {
						$thumb.='thumbnail/'.$av;
					} else {
						$thumb.=$av.'/';
					}
				}
				
				// regla de negocio: si es un pchart quita thumbnail
				if (strpos($thumb, '/pchart/')!==false) {
					$thumb = str_replace('/thumbnail/', '/', $thumb);
				}
				
			?>
			
				<a href="<?=$value?>" target=_blank>
				<img src="<?=$thumb?>" style="max-width: 100%;">
				</a><br><br>
				<span class="btn btn-danger fileinput-button" onclick="deletefile('<?=$value?>', '<?=$upload_id?>');">
					<i class="glyphicon glyphicon-remove"></i>
					 <span><?=__("Remove")?></span>
				</span>
			
			<?php
			}
			?>
			</div>
		</div>
	
	</div>
</div>

<script>

$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url = '/upload/php/';

    $('#<?=$upload_id?>').fileupload({
		
        url: url,
        dataType: 'json',
        success: function (data) {
			
			$('#progress<?=$upload_id?> .progress-bar').css('width', '0%');
			
			if (typeof data.files[0].thumbnailUrl === 'undefined') {
				
				$("#message<?=$upload_id?>").show().delay(5000).fadeOut();
				
			} else {
				
				$('#controls<?=$upload_id?>').fadeOut( "slow", function() {
				
					$('#value<?=$upload_id?>').val(data.files[0].url);
			
					updateImage('<?=$upload_id?>', data.files[0].url, data.files[0].thumbnailUrl);
			
				});
					
			}
			
		},
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress<?=$upload_id?> .progress-bar').css(
                'width',
                progress + '%'
            );
        }
        
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
        
});

function deletefile(file, id) {

	if (confirm('<?php echo __('Are you sure do you want to delete this?');?>')) {
		
		// remove preview
		$('#files'+id).fadeOut( "slow", function() {
			$('#controls'+id).fadeIn( "slow" );
		});
		// remove value
		$('#value'+id).val('');
		
		console.log(file);
		console.log(id);
		
	}

}

function updateImage(upload_id, url, thumbnailUrl) {

	$('#files'+upload_id).html(
		'<a href="'+url+'" target=_blank>' +
		'<img src="'+thumbnailUrl+'" style="max-width: 100%">' + 
		'</a><br><br>' +
		'<span class="btn btn-danger fileinput-button" onclick="deletefile(\''+url+'\', \''+upload_id+'\');">' + 
			'<i class="glyphicon glyphicon-remove"></i>' + 
			' <span><?=__("Remove")?></span>' +
		'</span>').fadeIn('slow');

}

function useCSSObject(upload_id) {

	var w = 500;
	var h = 400;

    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open('/admin/Reportpages/cssimg/?upload_id='+upload_id, 'cssimg', 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }

}
</script>

