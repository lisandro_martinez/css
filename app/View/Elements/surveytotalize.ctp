<div id="el_total">

<?php

$cell=0;
if (!$pdf) {
	$cell=10;
}

if (is_array($totaliza) and sizeof($totaliza)) {

	?>
	<div class="page-header">
		<h3><?php echo __("Totals");?></h3>			 									
	</div>
	<?php
	
	foreach($totaliza as $total) {
	
		if ($total["question_type"]==1 or $total["question_type"]==2) {
		
			echo "<h3>".$total["question"]."</h3>";
		
			if (is_array($total["answer"])) {
			
				$s1=null;
				$s2=null;
			
				$tabla="<table cellpadding=$cell cellspacing=0 align=center><tr bgcolor=\"#f4ffb5\"><td></td><td align=right>".__("Value")."</td><td align=right>".__("Percentage")."</td></tr>";
				
				$color=array("#D0E460", "#E4F574");
				
				$i=0;
				$j=0;

				$hay_datos=false;

				foreach($total["answer"] as $i=>$a) {
				
					if ($a["text"] or $a["value"]) {

						if ($i) {
							$s1.=",";
							$s2.=",";
						}

						if ($j==0) $j=1;
						else $j=0;
						
						$tabla.="<tr bgcolor=\"".$color[$j]."\"><td>".$a["text"]."</td><td align=right>".$a["value"]."</td><td align=right>".$a["porc"]."%</td></tr>";
						
						$s1.=$a["value"];
						$s2.=$a["text"];

						if ($a["value"]) {
							$hay_datos=true;
						}

					}

				}

				$tabla.="</table>";
				
//				echo "<table cellpadding=0 cellspacing=0><tr><td width=50%>";
				echo $tabla;
//				echo "</td><td width=50%>";
				if ($hay_datos) {
					echo "<p><img src=\"/pchart/Example14.php?lang=esp&isget=1&s=$s2&d=$s1\" style=\"max-width: 100%;\"></p>";					
				}
//				echo "</td></tr></table>";
				
			}
		
		} 
	
	}

}

if (!$pdf) {
?>

<div id="el_boton_de_el_total_dentro">
	<p>
		<a href="javascript:escondeElTotal();" class="btn btn-default noload">
			<i class="icon-collapse"></i> <?php echo __("Close Totals");?>
		</a>
	</p>
</div>

</div>

<div id="el_boton_de_el_total" style="display: none">
	<p>
		<a href="javascript:muestraElTotal();" class="btn btn-default noload">
			<i class="icon-collapse"></i> <?php echo __("View Totals");?>
		</a>
	</p>
</div>

<script>
function escondeElTotal() {
	$("#el_boton_de_el_total_dentro").hide();
	$("#el_total").hide();
	$("#el_boton_de_el_total").fadeIn();
}
function muestraElTotal() {
	$("#el_boton_de_el_total").hide();
	$("#el_boton_de_el_total_dentro").fadeIn();
	$("#el_total").fadeIn();
}
</script>

<?php } ?>