<div class="form-group col-md-12">
	<label for="<?=$field?>">
	<?php 
	echo $title.' ('.strtoupper($lang).')';
	echo '</label>';
	echo '<textarea class="summernote" name="editor__text__'.$template.'__'.$field.'__'.$lang.'">'.$value.'</textarea>';
	?>
</div>

<!-- Inicio de Summer Note -->
	
	<script type="text/javascript">
		$(document).ready(function() {
			$('.summernote').summernote({
			  toolbar: [
				// [groupName, [list of button]]
				['style', ['bold', 'italic', 'underline', 'clear']],
				['fontsize', ['fontsize']],
				['color', ['color']],
				['para', ['ul', 'ol', 'paragraph']],
				['height', ['height']]
			  ]
			});
		});
	</script>

<!-- Fin de Summer Note -->

