<?php

$interest_group=false;

if (is_array($related) and sizeof($related)) {
?>
		<div class="page-header">
			<h3><?php echo __("Related Entities");?></h3>
		</div>
		<?php
		$oldi=0;
		foreach($related as $i=>$r) {
			
			if ($r["Node"]["nodetype_id"]==Node::TYPE_INTEREST) $interest_group=true;

			if($oldi<>$r["Node"]["nodetype_id"]) {
				echo "<h4><i class=\"icon-chevron-right\"></i> ".$all_nodetypes[$r["Node"]["nodetype_id"]] . "</h4>";
				$oldi=$r["Node"]["nodetype_id"];
			}
			echo "<p style=\"margin-left: 30px\"><i class=\"icon-asterisk\"></i> ";
			echo "<a href=\"/admin/entities/".laVista($r["Node"]["nodetype_id"])."/".$r["Node"]["id"]."\">".$r["Node"]["name"]."</a>";
			?>
			<a class="btn-delete" href="/admin/nodes/deleterelation/<?php echo $r["Node"]["id"];?>/<?php echo $node["Node"]["id"];?>/<?php echo $redir;?>" onclick="return confirm('<?php echo __("Are you sure do you want to delete this?");?>')">
			<i class="icon-trash"></i> <?php echo __("Delete relation");?></a>
			</p>
			<?php
		}
		?>
<?php
}

// Las expectativas y necesidades deben tener al menos un grupo de interés asociado
if ($node["Node"]["nodetype_id"]==Node::TYPE_EXPECTATIONS and !$interest_group) {
	echo $this->element('warning', array('warning_message'=>__("This Expectation has no Interest Groups associated, please add one.")));
}

if (is_array($nodetypes) and sizeof($nodetypes)) {
?>

<div id="but_add_rel_ent"><p><a class="btn btn-primary noload" href="javascript:ShowHide('add_rel_ent');">
	<i class="icon-plus"></i> <?php echo __("Add"). " " . __("Related Entities");?></a></p></div>
			
<div id="add_rel_ent" style="display:none"> 

	<div class="page-header">
	</div>
	<div class="widget stacked ">
	  <div class="widget-header">
		<i class="icon-plus"></i>
		<h3><?php echo __("Add"). " " . __("Related Entities");?></h3>
	  </div> <!-- /widget-header -->
	  <div class="widget-content">
	
		<div class="col-md-6">
			<p><select id=elselect class="form-control" onchange="elscript()">
			<option value=0><?php echo __("Choose");?></option>
			<?php
			$selected=" selected ";
			foreach($nodetypes as $nid=>$n) {
				echo "<option value=".$nid." $selected>".$n."</option>";
				$selected="";
			}
			?>
			</select></p>
		</div>
		<div class="col-md-6" id="lalista2">
		</div>
	
	  </div> <!-- /widget-content -->
	</div>
	
</div> <!-- add_red_ent -->

<?php
}
?>

<script>
$( document ).ready(function() {
  elscript();
});
</script>
