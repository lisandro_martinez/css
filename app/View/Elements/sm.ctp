<script>

$( document ).ready(function() {

	<?php
	if ($id) {
	?>
	
	$.ajax({
		type: "POST",
		url: "/admin/entities/hello",
		data: { id: <?php echo $id;?>, redir: '<?php echo $redir;?>' },
		success: function(data, textStatus) {
			$("#lalista").fadeOut(function() {
				$(this).html(data).fadeIn();
			});
		},
		error: function() {
			console.log('Error loading.');
		}
	});
	
	<?php
	}
	?>

});

function jump2first() {

<?php
if (isset($wo[0]["Node"]["id"])) {
?>
	if ($("#wosel").val()==0) {
		$("#wosel").val(<?php echo $wo[0]["Node"]["id"];?>);
		viewOW();
	}
<?php
}
?>

}

function viewOW() {
	$("#lalista").html('<center><img src="/css/images/loader.gif" style="opacity: 0.4;filter: alpha(opacity=40);" /></center>');
	$.ajax({
		type: "POST",
		url: "/admin/entities/hello",
		data: { id: $("#wosel").val(), redir: '<?php echo $redir;?>' },
		success: function(data, textStatus) {
			$("#lalista").fadeOut(function() {
				$(this).html(data).fadeIn();
			});
		},
		error: function() {
			console.log('Error loading.');
		}
	});
}

function dale(j, i) {

	$.ajax({
		url: "/admin/entities/woupdate/"+j+"/"+i
	}).done(function() {
	
		$("#div_"+j+"_1").hide();
		$("#div_"+j+"_2").hide();
		$("#div_"+j+"_3").hide();
		if (i==1){
			$("#div_"+j+"_1").show("slow");
		} 
		if (i==2){
			$("#div_"+j+"_2").show("slow");
		} 
		if (i==3){
			$("#div_"+j+"_3").show("slow");
		}
		
	});

}
</script>
<link rel="stylesheet" href="/css/faq.css">

<?php 

$i=1;
if (!$nobox) { 

?>



<div class="widget stacked">


<?php 

$this->Viewbase->panel_title($panel_title);   

} 

?>

    <?php  
	?>
	
	<?php if (!$nobox) { ?>
    <div class="widget-content">
	<?php } ?>
			<div class="bs-example">

			  <div class="tab-content">
			  
					<br>
					<div class="col-md-8">
					
						<?php
						if (isset($wo) and is_array($wo) and sizeof($wo)) {
						?>
							<h4><?php echo $tab_content_title;?></h4>
							<p><select class="form-control" id="wosel" onchange="viewOW();">
							<option value=0><?php echo __("Choose");?></option>
							<?php
									foreach($wo as $woe) {
										echo "<option value=".$woe["Node"]["id"].">".$woe["Node"]["name"]."</option>";
									}
							?>
						</select></p>
						<?php
						} else {
							echo "<p>".__("There are no entities of this type, you can add one using the bottoms below")."</p>";
						}
						?>
					
					</div>
					<div class="col-md-4">
						<div class="widget stacked widget-box">
							
							<div class="widget-header">	
								<i class="icon-info"></i>
								<h3><?php echo __("Helper"); ?></h3>			
							</div> <!-- /widget-header -->
							
							<div class="well">
								
								<p><?php echo $help; ?></p>
								
							</div> <!-- /widget-content -->
							
						</div> <!-- /widget -->
					</div>
					<div style="clear: both;"></div>
					<div class="col-md-12" id="lalista">
					</div>
				
              </div>
			  
            </div>	

<?php if (!$nobox) { ?>

		</div>

	</div>
	
	<div class="col-md-6">
		<div class="widget stacked ">
		  <div class="widget-header">
			<i class="icon-plus"></i>
			<h3><?php echo __("Add");?></h3>
		  </div> <!-- /widget-header -->
		  <div class="widget-content">
			<p><a class="btn btn-primary btn-support-ask" href="/admin/nodes/add/<?php echo $nodetype_id;?>"><?php echo $nodetype_add_text;?></a></p>
		  </div> <!-- /widget-content -->
		</div> <!-- /widget -->
	</div>

<?php } ?>


<?php if (!$id) { ?>
	<script>
		jump2first();
	</script>
<?php } ?>


