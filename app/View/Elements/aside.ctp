<?php 

if ($show_left_menu) {

	if(isset($menu_category) and is_array($menu_category) and sizeof($menu_category)) { 
		
	?>
	<ul class="nav-left nav nav-pills nav-stacked">
	  <li class="categoryname">
		  <b><?php echo $menu_category['Category']['name'] ; ?></b>
	  </li>

	  <?php
		  if(!empty($actions_category)){          
			  foreach ($actions_category as $keyact => $action) {
				$actual  = false;

				$funcionactual = str_replace("admin_", "", $funcionactual);

				$urlactual  = array('/admin/'.$controladoractual.'/'.$funcionactual.'/','/admin/'.$controladoractual.'/'.$funcionactual);
				//pr($urlactual);
				//pr($action["url"]);
				if(!$actual){
				  if(in_array(trim($action["url"]),$urlactual)){$actual = true;}
				}

			  ?>
			  <li <?php if($actual){ ?>class="active"<?php } ?>>
				<a href="<?php echo $action["url"]; ?>">
				  <?php echo $action["name"]; ?>
				</a>
			  </li>
		<?php }?>
	  <?php } ?>

	</ul>
	<?php 
	}

}
