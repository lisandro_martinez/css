<div id="div_<?php echo $woe["Node"]["id"];?>_1" style="width: 85px; float: right; text-align: center;
	<?php if ($woe["Node"]["status"] or $woe["Node"]["status"]==1) echo "display: none;"?>
">
	<a id="elbutton" class="btn btn-info noload" href="javascript:dale(<?php echo $woe["Node"]["id"];?>, 2);">
		<i id="elicon" class="btn-icon-only icon-off"></i>										
	</a>
	<div id="eltext"><?php echo __("Not Started");?></div>
</div>

<div id="div_<?php echo $woe["Node"]["id"];?>_2" style="width: 85px; float: right; text-align: center;
	<?php if ($woe["Node"]["status"]<>2) echo "display: none;"?>
">
	<a id="elbutton" class="btn btn-warning noload" href="javascript:dale(<?php echo $woe["Node"]["id"];?>, 3);">
		<i id="elicon" class="btn-icon-only icon-play"></i>										
	</a>
	<div id="eltext"><?php echo __("Started");?></div>
</div>

<div id="div_<?php echo $woe["Node"]["id"];?>_3" style="width: 85px; float: right; text-align: center;
	<?php if ($woe["Node"]["status"]<>3) echo "display: none;"?>
">
	<a id="elbutton" class="btn btn-success noload" href="javascript:dale(<?php echo $woe["Node"]["id"];?>, 1);">
		<i id="elicon" class="btn-icon-only icon-ok"></i>										
	</a>
	<div id="eltext"><?php echo __("Ended");?></div>
</div>
