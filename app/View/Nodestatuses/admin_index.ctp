<?php
    $headerstitles = array(
                            'Nodestatus.id' => '#',
                            'Nodestatus.name' => __("Name"));

?>

<div class="widget stacked">
    <?php  $this->Viewbase->panel_title('Listado de Tipos de Nodos');   ?>

    <div class="widget-content">
         <?php include_once('filter.ctp');
            $this->Viewbase->Multi_form_create($this->form,'#');
           ?>
        <table class="table table-bordered">
            <?php
                // Encabezado de la tabla
                $this->Viewbase->table_Header($this->Paginator,$headerstitles);
            ?>
            <tbody>
                <?php foreach ($lists as $list): ?>
                <tr>
                        <?php
                        // Campo check de cada linea
                        $datarow=array(
                            'idModel' => $list['Nodestatus']['id'],
                            'textname' => $list['Nodestatus']['name'],
                            'inputname'=> 'data[Nodestatus][id][]',
                            'value' => $list['Nodestatus']['id']
                        );
                        $this->Viewbase->Multi_check_row($datarow);
                        // Campo check de cada linea
                    ?>

                    <td style="width: 10px;"><?php echo h($list['Nodestatus']['id']); ?>&nbsp;</td>
                    <td><?php echo h($list['Nodestatus']['name']); ?>&nbsp;</td>


                     <td class="actions">

                        <?php


                        $databutton_edit = array(
                                'url'=> '/admin/nodestatuses/edit/'.$list['Nodestatus']['id']
                            );
                            $this->Viewbase->button_edit($this->Html,$databutton_edit);



                        $databutton_delete = array(
                                'url'=> '/admin/nodestatuses/delete/'.$list['Nodestatus']['id'],
                                'idModel' =>$list['Nodestatus']['id'],
                                'name' =>$list['Nodestatus']['name']
                            );
                            $this->Viewbase->button_delete($this->Html,$databutton_delete);


                        ?>
                    </td>

        	   </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="5">
                        <?php

                            $databutton_add = array(
                                    'url'=> '/admin/nodestatuses/add/');
                                $this->Viewbase->button_add($this->Html,$databutton_add);

                        ?>
                    </td>
                </tr>
            </tbody>

        </table>
        <div class="checkalldiv col-md-6">
            <script>
                checkalltext = {
                    'empty' : {
                        'title' : 'Advertencia',
                        'text' : 'Debe seleccionar al menos un módulo para utilizar la opción sobre multiples registros'
                    },
                    'deleteall' :{
                        'title' : 'Confirmación para eliminar multiples registros',
                        'url' : '/admin/nodestatuses/deletemulti/',
                        'pretext' : 'Estas seguro que deseas eliminar los siguientes registros?'
                    }
                };
            </script>
            <input type="checkbox" class="checkallclick" title="Check All">
            <label for="checkall"><?php echo __('Check All'); ?></label>
            <select id="selectmulti" name="submit_mult" class="autosubmit" style="margin-left:10px;">
                <option value="0" selected="selected"><?php echo __('With selected:'); ?></option>
                <option value="deleteall"><?php echo __('Delete'); ?></option>
            </select>
        </div>
        <?php echo $this->element('paginado'); ?>
        <?php echo $this->Form->end(); ?>

    </div>
</div>
</div>
 <?php if($action == "admin_index"){ ?>
 <script type="text/javascript">
    Controllers.push("nodestatuses.index");
</script>
<?php } ?>
