<script>
function onc() {
	
	var elval=$( "#entitype" ).val();
	
	// es una galeria/encuesta/buzón/gráfico, debe aparecer solo titulo y descripción
	if ((elval>=18) && (elval<=21)) {
		$( "#elactive" ).show();
	} else {
		$( "#elactive" ).hide();
	}

	// es una convocatoria/galeria/encuesta/buzón/gráfico, debe aparecer solo titulo y descripción
	if ( (elval>=17 && elval<=21) || elval==5 || elval==6 || elval==8 || elval==2) {
		$( "#sologi" ).hide();
		$( "#lasfechas" ).hide();
		$( "#elpriority" ).hide();
		$( "#divcost" ).hide();
	// es un grupo de interes... debe apagar todo menos prioridad y debe mostrar el poder y el impacto
	} else {
		if (elval==16) {
			$( "#sologi" ).show();
			$( "#lasfechas" ).hide();
			$( "#elpriority" ).show();
			$( "#divcost" ).hide();
		} else {
			$( "#sologi" ).hide();
			if (elval == 3 || elval == 4 || elval == 7) {
				$( "#lasfechas" ).show();
				$( "#elstatus" ).hide();	
			} else if (elval >= 1 && elval <= 9) {
				$( "#lasfechas" ).hide();
			}
			if (elval>=10 && elval<=15) {
				$( "#divcost" ).show();
				$( "#elpriority" ).show();
			} else {
				$( "#divcost" ).hide();
				$( "#elpriority" ).hide();
			}
		}
	}
}
function selFY(y) {
	document.getElementById("datepicker").value = '01-10-'+(y-1);
	document.getElementById("datepicker-2").value = "30-09-"+y;
}
</script>
<div class="widget stacked ">
  <div class="widget-header">
            <i class="icon-pencil"></i>
            <h3><?php echo __("Add"); ?></h3>
  </div>
  <div class="widget-content">

        <?php
          echo $this->Form->create('Node', array('action' => $form_config["urlform"],'class'=>'col-lg-12','type'=>$form_config["type"]));
        ?>

		<div class="form-group col-lg-12">
			 <label for="name"><?php echo __("Type");?> *</label>
			 
			 <?php 
			 if (isset($nodetype_id)) {
				 echo $this->Form->input('nodetype_id',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>__("Choose"),"type"=>"select","required"=>false, 'options'=>$nodetypes, 'value'=>$nodetype_id, 'onchange'=>'onc()', 'id'=>'entitype', 'style'=>"font-size: x-large; height: auto")); 
			 } else {
				 echo $this->Form->input('nodetype_id',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>__("Choose"),"type"=>"select","required"=>false, 'options'=>$nodetypes, 'onchange'=>'onc()', 'id'=>'entitype')); 
			 }
			?>

		</div>

		<?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp') ) : ?>	  
          <div class="form-group col-lg-12">
            <?php
              if($action == "admin_edit"){echo $this->Form->input('Node.id');}
            ?>
			<label for="name"><?php 
			if (isset($this->data['Node']['nodetype_id'])) :			
				if (in_array($this->data['Node']['nodetype_id'], array(2, 3, 4, 5, 6, 7, 8, 10, 22))) :
					echo __("Statement of") . ' ' . $nodetypes[$this->data['Node']['nodetype_id']];
				else :
					echo __("Name of") . ' ' . $nodetypes[$this->data['Node']['nodetype_id']];
				endif;
			else :
				echo __("Name");
			endif;
			?> (ESP)*</label>
			<input type="text" class="form-control" name="data[Node][name][esp]" id="name" value="<?php echo isset($this->data['Node']['name']['esp']) ? $this->data['Node']['name']['esp'] : '';?>">

		</div>
		<?php else : ?>
		<div class="form-group col-lg-12">
            <?php
              if($action == "admin_edit"){echo $this->Form->input('Node.id');}
            ?>
		</div>
		<?php endif; ?>
    <?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
		<div class="form-group col-lg-12">
			<label for="name"><?php 
			if (isset($this->data['Node']['nodetype_id'])) :
				if (in_array($this->data['Node']['nodetype_id'], array(2, 3, 4, 5, 6, 7, 8, 10, 22))) :
					echo __("Statement of") . ' ' . $nodetypes[$this->data['Node']['nodetype_id']];
				else :
					echo __("Name of") . ' ' . $nodetypes[$this->data['Node']['nodetype_id']];
				endif;
			else :
				echo __("Name");
			endif;
			?> (ENG)*</label>
			<input type="text" class="form-control" name="data[Node][name][eng]" id="name" value="<?php echo isset($this->data['Node']['name']['eng']) ? $this->data['Node']['name']['eng'] : '';?>">
		</div>
		<?php endif; ?>
    <?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'esp') ) : ?>
		<div class="form-group col-lg-12">
			<label for="name"><?php 
			if (in_array($this->data['Node']['nodetype_id'], array(2, 3, 4, 5, 6, 7, 8, 10, 22))) :
				echo __("Detail of") . ' ' . $nodetypes[$this->data['Node']['nodetype_id']];;
			else :
				echo __("Description");
			endif; ?> (ESP)</label>
			<textarea class="summernote" name="data[Node][description][esp]" id="name"><?php echo isset($this->data['Node']['description']['esp']) ? $this->data['Node']['description']['esp'] : '';?></textarea>
		</div>
		<?php endif; ?>
    <?php if (! $this->Session->check('Config.LangVar') 
    					or ($this->Session->check('Config.LangVar') 
    					and $this->Session->read('Config.LangVar') == 'eng') ) : ?>
		<div class="form-group col-lg-12">
			<label for="name"><?php 
			if (in_array($this->data['Node']['nodetype_id'], array(2, 3, 4, 5, 6, 7, 8, 10, 22))) :
				echo __("Detail of") . ' ' . $nodetypes[$this->data['Node']['nodetype_id']];;
			else :
				echo __("Description");
			endif; ?> (ENG)</label>
			<textarea class="summernote" name="data[Node][description][eng]" id="name"><?php echo isset($this->data['Node']['description']['eng']) ? $this->data['Node']['description']['eng'] : '';?></textarea>
		</div>
		<?php endif; ?>

		<div id="lasfechas">
			
			<div class="form-group col-lg-4">
			
				<p><?php echo __("Select dates based on Fiscal Year");?></p>
				
				<?php
				
				$y=date("Y");
				if (date("m")>9) $y++;
				for($i=5; $i>=0; $i--) {
					if ($i!=5) echo " | ";
					$fy=$y-$i;
					echo "<a href=\"javascript:void(0);\" class='noload' onclick='javascript:selFY($fy);'>$fy</a>";
				}
				
				?>
			
			</div>
			
			<div class="form-group col-lg-4">
				<label for="name"><?php echo __("Start date");?></label>
				<input type="text" class="form-control" name="data[Node][inidate]" value="<?php echo isset($this->data['Node']['inidate']) ? $this->data['Node']['inidate'] : '';?>" id="datepicker">
			</div>
			
			<div class="form-group col-lg-4">
				<label for="name"><?php echo __("End date");?></label>
				<input type="text" class="form-control" name="data[Node][enddate]" value="<?php echo isset($this->data['Node']['enddate']) ? $this->data['Node']['enddate'] : '';?>" id="datepicker-2">
			</div>
			

			
		
			<div class="form-group col-lg-6" id="elstatus">
				<label for="name"><?php echo __("Status");?></label>	 
				<?php 
				echo $this->Form->input('status',array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>__("Choose"),"type"=>"select","required"=>false, 'options'=>$nodestatuses)); 
				?>
			</div>
			
		
		</div>

	
		
		<div id="sologi" style="display: none;">
		
			<div class="form-group col-lg-4">
				<label for="name"><?php echo __("Power");?></label>				
				<select name="data[Node][power]" class="form-control">
				<option value=""><?php echo __("Choose");?></option>
				<option value="1" <?php echo (isset($this->data['Node']['power']) and $this->data['Node']['power']==1) ? " selected ":"";?>>1</option>
				<option value="2" <?php echo (isset($this->data['Node']['power']) and $this->data['Node']['power']==2) ? " selected ":"";?>>2</option>
				<option value="3" <?php echo (isset($this->data['Node']['power']) and $this->data['Node']['power']==3) ? " selected ":"";?>>3</option>
				<option value="4" <?php echo (isset($this->data['Node']['power']) and $this->data['Node']['power']==4) ? " selected ":"";?>>4</option>
				<option value="5" <?php echo (isset($this->data['Node']['power']) and $this->data['Node']['power']==5) ? " selected ":"";?>>5</option>
				</select>
			</div>
			
			<div class="form-group col-lg-4">
				<label for="name"><?php echo __("Impact");?></label>
				<select name="data[Node][impact]" class="form-control">
				<option value=""><?php echo __("Choose");?></option>
				<option value="1" <?php echo (isset($this->data['Node']['impact']) and $this->data['Node']['impact']==1) ? " selected ":"";?>>1</option>
				<option value="2" <?php echo (isset($this->data['Node']['impact']) and $this->data['Node']['impact']==2) ? " selected ":"";?>>2</option>
				<option value="3" <?php echo (isset($this->data['Node']['impact']) and $this->data['Node']['impact']==3) ? " selected ":"";?>>3</option>
				<option value="4" <?php echo (isset($this->data['Node']['impact']) and $this->data['Node']['impact']==4) ? " selected ":"";?>>4</option>
				<option value="5" <?php echo (isset($this->data['Node']['impact']) and $this->data['Node']['impact']==5) ? " selected ":"";?>>5</option>
				</select>
			</div>
			
		</div>
		
		<div id="divcost" style="display: none">
			<div class="form-group col-lg-6">
				<div class="col-lg-6">
					<label for="name"><?php echo __("Cost");?></label>
				</div>
				<div class="col-lg-6">
					<label for="name"><?php echo __("Currency");?></label>
				</div>
				<div class="col-lg-6">
					<input type="text" class="form-control" name="data[Node][cost]" id="cost" value="<?php echo isset($this->data['Node']['cost']) ? $this->data['Node']['cost'] : '0';?>">
				</div>
				<div class="col-lg-6">
					<select name="data[Node][currency]" class="form-control">
					<option value=""><?php echo __("Choose");?></option>
					<option value="₡" <?php echo (isset($this->data['Node']['currency']) and $this->data['Node']['currency']=="₡") ? " selected ":"";?>>₡</option>
					<option value="$" <?php echo (isset($this->data['Node']['currency']) and $this->data['Node']['currency']=='$') ? " selected ":"";?>>$</option>
					<option value="€" <?php echo (isset($this->data['Node']['currency']) and $this->data['Node']['currency']=='€') ? " selected ":"";?>>€</option>
					</select>
				</div>
			</div>
			
			<div class="form-group col-lg-4">
				<label for="name"><?php echo __("Attendant (name)");?></label>
				<input type="text" class="form-control" name="data[Node][assignedto]" id="name" value="<?php echo isset($this->data['Node']['assignedto']) ? $this->data['Node']['assignedto'] : '';?>">
			</div>
			<div class="form-group col-lg-4">
				<label for="name"><?php echo __("Attendant Mail");?></label>
				<input type="text" class="form-control" name="data[Node][assignedto_email]" id="name" value="<?php echo isset($this->data['Node']['assignedto_email']) ? $this->data['Node']['assignedto_email'] : '';?>">
			</div>
		</div>
		
		<div id="elpriority">
			<div class="form-group col-lg-4">
				<label for="name"><?php echo __("Priority");?></label>
				<?php
					echo $this->Form->input('priority',
											array(
												"div"=>false,
												"class"=>"form-control",
												"label"=>false,
												"empty"=>__("Choose"),
												"type"=>"select",
												"required"=>false,
												'options'=>$nodepriorities,
												'value'=>$this->data['Node']['priority'], 
												'id'=>'entitype'));
				?>
			</div>			
		</div>
					
		<div id="elactive">
			<div class="form-group col-lg-6">
				 <p><label for="name"><?php echo __('Active');?></label></p>
					<?php 
					echo $this->Form->checkbox('Node.active',array("label"=>__("Active"),"required"=>false,'class'=>''));
					?>
			</div>
		</div>
		<?php if ((isset($nodetype_id) && $nodetype_id == 11) 
		or (isset($this->data['Node']['nodetype_id']) && $this->data['Node']['nodetype_id'])) : ?>
		<div class="form-group col-lg-4">
			 <label for="name"><?php echo __("Type");?> *</label>			 
			 <?php 			 
			echo $this->Form->input('type', array("div"=>false,"class"=>"form-control","label"=>false,"empty"=>__("Choose"),"type"=>"select","required"=>false, 'options'=>$tipos)); 			 
			?>

		</div>
		<?php endif; ?>
		<?php if (isset($indicators)) :?>

		<div class="form-group col-lg-4">		
				<label for="name"><?php echo __("Indicator");?> *</label>
				<?php
					echo $this->Form->input('indicator_id',
											array(
												"div"=>false,
												"class"=>"form-control",
												"label"=>false,
												"empty"=>__("Choose"),
												"type"=>"select",
												"required"=>true,
												'options'=>$indicators,												 
												));
				?>
		</div>
		<?php endif; ?>
			
          <div class="form-group form_button">

                <div class="form-group">
                  <label for="note"><?php echo __("All fields with * are mandatory");?></label>
                </div>
								<button type="submit" class="btn btn-default"><?php echo $form_config["labelbutton"];?></button>

					</div>
          <div class="form-group form_response">
            <div id="responseForm"></div>
          </div>

      </form>


  </div>
</div>



<?php if($action == "admin_add"){ ?>
 <script type="text/javascript">
	Controllers.push("Nodes.add");
</script>
<?php } ?>


<script>
onc();
</script>
