<div>
            <ul class="nav nav-tabs">
              <li class="active"><a href="#home" class="filter-tab" data-toggle="tab"><?php echo $form_config["title"]; ?></a></li>
              <?php if(!empty($this->data['Search'])){ ?>
              <li><a href="?" class="btn btn-info"><?php echo __('Quitar Filtro');?></a></li>
              <?php } ?>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
              <div class="tab-pane" id="home">

                <div class="panel panel-default">
                  <div class="panel-body">
                    <?php
                    echo $this->Form->create('Node', array('action' => $form_config["urlform"],'class'=>'form-horizontal','type' => 'get'));
                    ?>
                    <div class="col-md-12">
                        <div class="form-group col-lg-6">
						<?php
						if($action == "admin_edit"){echo $this->Form->input('id');}
						echo $this->Form->input('Search.name',array('class'=>'form-control',"label"=>__("Name"),"required"=>false));?>
                        </div>
                    </div>
					
					<div class="form-group col-lg-6">
						<?php echo $this->Form->input('Search.section_id',array('class'=>'form-control', "label"=>__("Section"),"empty"=>__("Select All"),"type"=>"select","required"=>false,'options'=>$sections));
						?>
					</div>
					
					
                  <div class="filter_button">
                    <input class="btn btn-primary" type="submit" value="<?php echo $form_config["labelbutton"]; ?>">
                  </div>
                </div>
              </form>
                </div>
            </div>
        </div>
</div>