<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Answer $Answer
 */

class AnswersController extends AppController {

	/*----------------beforeFilter-----------------*/
    public function beforeFilter() {
        parent::beforeFilter();
    }
    /*----------------beforeFilter-----------------*/


        public function paramFilters($urlform){

            $form_config = array();
            $form_config["title"] = __("Search / Filter");
            $form_config["urlform"] = $urlform;
            $form_config["labelbutton"] = __("Search / Filter");
            $this->set('form_config',$form_config);

            $fields_char = array(
                        'name',
            );

            $conditions = $this->filterConfig('Answer',$fields_char);
            $this->recordsforpage();

            return $conditions;

        }


    /*----------------INDEX-----------------*/

        /*----------------get_index-----------------*/
        public function get_index($urlfilter = 'admin_index'){
            $conditions=$this->paramFilters($urlfilter);
            $limit = $this->Session->read('Filter.recordsforpage');

            $this->Answer->setLanguage();
            $this->Paginator->settings = array(
                'order' => 'Answer.id ASC',
                'conditions' => $conditions,
                'limit' => $limit,
                'recursive' => 1
            );

            $lists = $this->Paginator->paginate('Answer');
			
			$questions = $this->Question->find('list',array(
				  'fields'=>array(
					  'id','name'
				  )
			));

            $this->set(compact('lists', 'questions' ));
        }
        /*----------------get_index-----------------*/

        /*----------------index-----------------*/
        public function admin_index(){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if ($this->request->is('get')) {
                $this->get_index();
            }
        }
        /*----------------index-----------------*/

    /*----------------INDEX-----------------*/

    /*----------------ADD-----------------*/

        /*----------------post_add-----------------*/
        public function post_add(){

			$this->ajaxVariablesInit();

			$fieldslocales = array('Answer'=>array('name'));
			$validations = $this->validationLocale($fieldslocales);

			if(empty($validations)){
			
				$this->Answer->create();
				$this->Answer->set($this->data);

				try{
					if ($this->Answer->saveMany()) {
						$this->dataajax['response']['method']=$this->getMethod();
						$this->dataajax['response']['message_success']=__('Save Success',true);
					}
				}catch (Exception $e) {
					$this->dataajax['response']['message_error']=__('Save Error',true);
				}
			}else{
				$this->dataajax['response']["errors"]=$validations;
			}

			echo json_encode($this->dataajax);
			die();
			
        }
        /*----------------post_add-----------------*/

        /*----------------get_add-----------------*/
        public function get_add(){

        }
        /*----------------get_add-----------------*/

        /*----------------add-----------------*/
        public function admin_add() {
            $form_config = array();
            $form_config["title"] = __("Add")." ".__("Answer");
            $form_config["urlform"] = "admin_add";
            $form_config["labelbutton"] = __("Add");
            $form_config["type"] = 'file';
            $this->set('form_config',$form_config);

            if ($this->request->is('post')) {
                $this->post_add();
            }elseif ($this->request->is('get')){
                $this->get_add();
            }

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

			  $questions = $this->Question->find('list',array(
					  'fields'=>array(
						  'id','name'
					  )
			  ));

			  $is_root = $this->isRoot();

			  $summernote=true;
			  
			  $this->set(compact('questions','is_root', 'summernote'));
			  
        }
        /*----------------add-----------------*/

    /*----------------ADD-----------------*/

    /*----------------EDIT-----------------*/

        /*----------------get_edit-----------------*/
        public function get_edit($id){

            $this->Answer->id = $id;
            if (!$this->Answer->exists()) {
                $this->_flash(__('No-exist-record',true),'alert alert-warning');
                $this->redirect(array('action' => 'admin_edit'));
            }else{

            	$this->set(
                	array(
                		"modules" => $this->Module->find("list")
                	)
                );

                $datamodel = $this->Answer->read(null, $id);
                $this->request->data = $this->readWithLocale($datamodel); // se trae la informacion al editar.
				
                $this->set(compact('id'));
            }

        }
        /*----------------get_edit-----------------*/

        /*----------------post_edit-----------------*/
        public function post_edit($id){

                $this->ajaxVariablesInit();
				
				$fieldslocales = array('Answer'=>array('name'));
				$validations = $this->validationLocale($fieldslocales);

				if(empty($validations)){

                    $this->Answer->id = $id;
                    $this->Answer->set($this->data);

                    try{
                        if ($this->Answer->saveMany()) {
                            $this->dataajax['response']['method']=$this->getMethod();
                             $this->dataajax['response']['message_success']=__('Update Success',true);
                        }
                    }catch (Exception $e) {
                        $this->dataajax['response']['message_error']=__('Update Error',true);
                    }
                }else{
					$this->dataajax['response']["errors"]=$validations;
                }
                echo json_encode($this->dataajax);
                die();
        }
        /*----------------post_edit-----------------*/

        /*----------------edit-----------------*/
        public function admin_edit($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            $form_config = array();
            $form_config["title"] = __("Edit")." ".__("Answer");
            $form_config["urlform"] = "admin_edit";
            $form_config["labelbutton"] = __("Save");
            $form_config["type"] = 'put';
            $this->set('form_config',$form_config);

            if ($this->request->is('get')) {
                if(empty($id)){
                    $this->get_index('admin_edit');
                }else{
                    $this->get_edit($id);
                }
            }else{
                if ($this->request->is('put')) {
                    $this->post_edit($id);
                }
            }

			  $questions = $this->Question->find('list',array(
				  'fields'=>array(
					  'id','name'
				  )
			  ));

			  $is_root = $this->isRoot();

			  $summernote=true;
			  
			  $this->set(compact('questions','is_root', 'summernote'));
			  
        }
        /*----------------edit-----------------*/

    /*----------------EDIT-----------------*/


    /*----------------DELETE-----------------*/

        /*----------------delete-----------------*/
        public function admin_delete($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if(!empty($id)){
                $this->Answer->id = $id;
                if (!$this->Answer->exists()) {
                    $this->_flash(__('No-exist-record', true),'alert alert-danger');
                    $this->redirect(array('action' => 'admin_index'));
                }

                try{
                    if ($this->Answer->delete($id,true)) {
                        $this->_flash(__('Delete Success', true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                        $this->_flash(__('Delete Error', true),'alert alert-warning');
                        $this->redirect(array('action' => 'admin_index'));
                }
            }else{

                $this->get_index('admin_index');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------delete-----------------*/

        /*----------------delete-----------------*/
        public function admin_deletemulti(){

            if($this->request->is('post')){
                //pr($this->data);
                $dataids =  $this->data['Answer']['id'];

                try{
                    if ($this->Answer->deleteAll(array('Answerid' => $dataids))) {
                        $this->_flash(__('Delete Success',true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                    $this->_flash(__('Delete Error', true),'alert alert-warning');
                    $this->redirect(array('action' => 'admin_index'));
                }

            }else{
                $this->_flash(__('Delete Error', true),'alert alert-danger');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------delete-----------------*/

    /*----------------DELETE-----------------*/

}
