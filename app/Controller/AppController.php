<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
  
  public $uses = array('Aco', 'Aro', 'ArosAcos', 'User', 'Group', 'Groupaction', 
  'Companyquestionnaire', 'Action', 'Module', 'Node', 'Indicator', 'Indicatorvalue', 
  'Nodenode', 'Nodepredecessor', 'Nodetype', 'Nodepriority', 'Nodecompany', 'Nodetemporary', 
  'Nodestatus', 'Indicatornode', 'Nodesource', 'Category','Company','Corporation',
  'Questionnaire','Section', 'Question', 'Answer', 'Autoevaluation', 
  'Autoevaluationanswer', 'Interestgroupcall', 'Galleryimage', 
  'Mailboxsuggestion', 'Surveyquestion', 'Surveyanswer', 'Graphdata', 'Report', 'Reportpage' );
  
  public $helpers = array('Form', 'Html','Session','Time');

  public $components = array(
        'Acl',
        'Auth' => array(
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            )
        ),
        'Session',
        'Paginator',
    );

    public $dataajax=array();
    public $errorsajax = array();

	public function beforeFind() {
	}
	
    public function beforeFilter() {

        if (!$this->Session->read('Config.MyLangVar')) {
			$this->Session->write('Config.MyLangVar', 'esp');
		}

		Configure::write('Config.language', $this->Session->read('Config.MyLangVar'));
		$this->Session->write('Config.language', $this->Session->read('Config.MyLangVar'));
	
        $this->Auth->loginAction = array('controller' => 'users', 'action' => 'login','plugin' => false,'admin' => true);
        $this->Auth->loginRedirect = '/admin/users/home/';
        $this->Auth->logoutRedirect = '/admin/users/login/';

	    $this->Auth->allowedActions= array('admin_login','admin_logout','admin_home','index','admin_generate_html');

        $this->Auth->authenticate = array(
            AuthComponent::ALL => array(
                'fields' => array(
                    'username' => 'username',
                    'password' => 'password'),
                'userModel' => 'User'
            ), 'Form'
        );

        $appconfig= Configure::read('App');
        $this->set('appconfig',$appconfig);
        $this->set('action',$this->params["action"]);
		
        if(!CakeSession::read('Config.language')){
           CakeSession::write('Config.language','esp');
        }

        $this->is_Authorizate();

    }

    public function recordsforpage($cpp = 15){

        $optionsrecors=array(1,15,30,50,100,500,1000);

        $recordsforpage = $this->Session->read('Filter.recordsforpage');

        if(!isset($recordsforpage)){
            $recordsforpage = $cpp;
            $this->Session->write('Filter.recordsforpage', $recordsforpage);
        }

         $this->set('recordsforpage',$recordsforpage);
         $this->set('optionsrecors',$optionsrecors);

    }

    public function filterConfig($model,$fields_char){
        $conditions = array();

        //pr($this->request->query);

        if (!empty($this->request->query)) {
            foreach($this->request->query as $key => $record) {
                if($key!= 'rowspage'){
                    $this->request->data['Search'][$key] = $record;
                }else{
                    //pr($key);
                    //pr($record);
                    //pr($this->request);

                    $this->Session->write('Filter.recordsforpage', $record);
                    $this->redirect(array('controller'=>$this->request->params['controller'],'action' => $this->request->params['action']));
                }

            }

            if(isset($this->request->data['Search'])){
                foreach ($this->request->data['Search'] as $name => $record) {
                    if ((isset($record) && !empty($record)) || $record === "0") {
                        $this->request->query[$name] = $record;

                        if (in_array($name,$fields_char)) {
                            $conditions[$this->{$this->modelClass}->{$model}.$model.'.'.$name.' LIKE'] = '%' . $record . '%';
                        } else {
                            $conditions[$this->modelClass . '.' . $name] = $record;
                        }
                    }
                }
            }

        }

        //pr($conditions);
        return $conditions;
    }

    public function getlocalesValidates(){
        $locales = array(
            'eng','esp'
        );
        return $locales;
    }
	
    public function readWithLocale($requestdata){
                 //pr($requestdata);

                $datamodel = $requestdata;
                $pos=1;
                foreach ($requestdata as $keydata => $valuedata) {

                    if($pos == 1){

                        //pr($valuedata);
                        foreach ($valuedata as $keyfield => $valuefield) {
                            $withTranslation =  $keydata.$keyfield."Translation";
                            if(isset($datamodel[$withTranslation])){
                                //pr($datamodel[$withTranslation]);

                                $temp_array= array();
                                foreach ($datamodel[$withTranslation] as $keytranslation => $valuetranslation) {
                                    $temp_array[$valuetranslation['locale']]= $valuetranslation['content'];
                                }
                                //pr($temp_array);
                                $datamodel[$keydata][$keyfield]= $temp_array;
                            }
                        }
                        break;
                    }

                }

                //pr($datamodel);
                return $datamodel;
    }

    public function getlocales(){

        $locales = array(
            'esp'=>'<span class="flag flag-es" alt="Español"></span> Español',
            'eng'=>'<span class="flag flag-us" alt="English" ></span> English',
        );

        return $locales;
    }

    public function isRoot(){
        $sw=false;
        if(($this->Auth->user('group_id') == 1) || ($this->Auth->user('group_id')=="")){ $sw=true;}
        return $sw;
    }

    public function validationLocale($fieldslocales){
	
        $locales = $this->getlocalesValidates();
        $validations_errors_locales = array();
        $validations_errors = array();

        foreach ($fieldslocales as $modelfields => $fields) {

           $rulesvalidationsindexs = array();
           $rulesvalidations =  $this->{$modelfields}->validate;
           foreach ($rulesvalidations as $keyrules => $valuerules) {
               $rulesvalidationsindexs[$keyrules] = $keyrules;
           }

           //pr($rulesvalidationsindexs);
           //pr($this->data);

           $validations_errors_locales[$modelfields] = array();
		   if (isset($this->data[$modelfields])) {
            foreach ($this->data[$modelfields] as $keydata => $valuedata) {

                if(in_array($keydata, $fields)){

                    unset($rulesvalidationsindexs[$keydata]);
					
					if (isset($valuedata) and is_array($valuedata)) {
                      foreach ($valuedata as $locale => $valuelocale) {

                          if(in_array($locale, $locales)){

                              $this->{$modelfields}->set(array($modelfields=>array($keydata => $valuelocale)));
                              $valid = $this->{$modelfields}->validates(
                                          array(
                                              'fieldList' => array($keydata)
                                          )
                              );

                              if(!$valid){
                                  if(isset($this->{$modelfields}->validationErrors[$keydata])){
                                      $temp[$keydata][$locale] = $this->{$modelfields}->validationErrors[$keydata][0];
                                      $validations_errors_locales[$modelfields] = $temp;
                                  }

                              }
                          }

                      }
					}


                }
            }
			}

            if(!isset($validations_errors_locales[$modelfields])){
                $validations_errors_locales[$modelfields] = array();
            }

            //pr($validations_errors_locales);
            $validations_errors_nolocales = array();

            if(!empty($rulesvalidationsindexs)){
                $this->{$modelfields}->set($this->data);
                $valid = $this->{$modelfields}->validates(
                        array(
                            'fieldList' => $rulesvalidationsindexs
                        )
                );

                if(!$valid){

                    foreach ($this->{$modelfields}->validationErrors as $keyvalidation => $valuevalidation) {
                        $temparray[$keyvalidation] = $valuevalidation;
                        $validations_errors_nolocales[$modelfields]= $temparray;
                    }

                }else{
                    $validations_errors_nolocales[$modelfields] = array();
                }
            }else{
                 $validations_errors_nolocales[$modelfields] = array();
            }


            //pr($validations_errors_locales);
            //pr($validations_errors_nolocales);

            if((!empty($validations_errors_locales[$modelfields]))||(!empty($validations_errors_nolocales[$modelfields]))){
                $validations_errors[$modelfields] = array_merge($validations_errors_locales[$modelfields],$validations_errors_nolocales[$modelfields]);
            }

            if(empty($validations_errors[$modelfields])){
                $validations_errors= array();
            }

        }


        return $validations_errors;
    }

	
    public function setSidebarMenu($category_id=null){

        $actions = $this->Session->read('User.actions');
        //pr($actions);
        $this->set('idgrupo', $this->Auth->user('group_id'));

        $actions_category = array();
//        $actions_category = $this->Session->read('User.actions_category');

        $menu_category = array();
        $menu_category = $this->Session->read('User.menu_category');
		
        $category_id_session = $this->Session->read('User.menu_category_id');
        //$action['Action'] = array('name'=>__('homeMenu'),'url'=>'/admin/users/home/');
        //$actions_category[0]=$action['Action'];


        $redirect=true;
        if(!isset($category_id)){
          if(!empty($category_id_session)){
            $category_id = $category_id_session;
            $redirect=false;
          }
        }





		// dado el actual url del action dame el category_id
		// este debe ser el valor de: $category_id para evitar el bug del menu "pegado" cuando va a otra seccion
		$act = explode("_", $this->params['action']);

		$actions2 = $this->Action->find("all",array(
			'fields' => array('Action.id','Action.name','Action.url','Action.category_id'),
			'conditions'=>array(
				'Action.url' => "/".$act[0]."/".$this->params['controller']."/".$act[1]
			),
			'recursive'=>-1
		));

		// no mostrar el menu lateral a menos que se encuentre la funcion actual definida en una opcion del menu
		// CSS-HOTFIX-MENU-PEGADO
		$show_left_menu = false;
		if ($new_cate_id = $actions2[0]["Action"]["category_id"]) {
			if (isset($actions) and sizeof($actions)) {
				foreach ($actions as $keyaction => $action) {
					if($action['Action']['category_id'] == $new_cate_id){
						$show_left_menu = true;
					}
				}
			}
		}
		// fin bug fix





        if(isset($category_id)){

            $actions_category = array();

            $this->Category->id = $category_id;

            if (!$this->Category->exists()) {
                $this->_flash(__('msg-categorys-edit-noexist',true),'alert alert-warning');
                $this->redirect(array('action' => 'admin_home'));
            }else{

				if (isset($actions) and sizeof($actions)) {
					foreach ($actions as $keyaction => $action) {
						if($action['Action']['category_id'] == $category_id){
							$actions_category[$action['Action']['id']] = $action['Action'];
						}
					}
				}

            }

            $this->Session->write('User.actions_category', $actions_category);

            $this->Category->setLanguage();
            $menu_category = $this->Category->find('first',array(
                'conditions' => array(
                    'Category.id' => $category_id
                ),
                'recursive' => -1
            ));

            $this->Session->write('User.menu_category_id', $category_id);
            $this->Session->write('User.menu_category', $menu_category);

            reset($actions_category);
            $primeraopcion = current($actions_category);
            //echo $primeraopcion['url'];

            $this->set('actions_category',$actions_category);
            $this->set('menu_category',$menu_category);

            if($redirect){
              $this->redirect($primeraopcion['url']);
              die();
            }

        }

        $this->set('actions_category',$actions_category);
        $this->set('menu_category',$menu_category);        
        $controladoractual = $this->params['controller'];
        $funcionactual = $this->params['action'];

		$isHome=false;
		if ($controladoractual . "/" . $funcionactual == "users/admin_home") {
			$isHome=true;
		}
		
        $parametrosactuales = $this->params['pass'];
        $this->set(compact('controladoractual','funcionactual','parametrosactuales', 'isHome', 'show_left_menu'));

    }


    public function setHeaderMenu(){

        $header_menu = $this->Session->read('User.header_menu');
		
		if (CakeSession::read('Config.language')!=CakeSession::read('Config.lastLanguage')) {
			unset($header_menu);
		}
		
		CakeSession::write('Config.lastLanguage',CakeSession::read('Config.language'));

        if(empty($header_menu)){

            $group_id = $this->Auth->user('group_id');
            

            $this->Groupaction->setLanguage();
            $groupactions = $this->Groupaction->find("list",array(
                'fields'=>array('Groupaction.action_id'),
                'conditions'=>array(
                    'Groupaction.group_id' => $group_id
                ) ,
                'recursive' => -1
            ));

            //pr($groupactions);
            //die();

            if(!empty($groupactions)){

				$this->Action->setLanguage();
                $actions = $this->Action->find("all",array(
                    'fields' => array('Action.id','Action.name','Action.url','Action.category_id'),
                    'conditions'=>array(
                        'Action.id' => $groupactions
                    ),
                    'order'=>'Action.order ASC',
                    'recursive'=>-1
                ));

                //pr($actions);
                //die();

                $categories_actions = $this->Action->find("list",array(
                    'fields' => array('Action.category_id'),
                    'conditions'=>array(
                        'Action.id' => $groupactions
                    ),
                    'group' => 'Action.category_id',
                    'order'=>'Action.order ASC',
                    'recursive'=>-1
                ));

                //pr($categories_actions);
                //die();

				$this->Category->setLanguage();
                $categories = $this->Category->find("all",array(
                    'fields' => array('Category.id',"Category.name",'Category.module_id'),
                    'conditions'=>array(
                        'Category.id' => $categories_actions
                    ),
                    'order'=>'Category.order ASC',
                    'recursive'=>-1
                ));

                //pr($categories);
                //die();

				$this->Category->setLanguage();
                $modules_categories = $this->Category->find("list",array(
                    'fields' => array('Category.module_id'),
                    'conditions'=>array(
                        'Category.id' => $categories_actions
                    ),
                    'group' => 'Category.module_id',
                    'order'=>'Category.order ASC',
                    'recursive'=>-1
                ));

                //pr($modules_categories);
                //die();


				$this->Module->setLanguage();
                $modules = $this->Module->find("list",array(
                    'fields' => array('Module.id',"Module.name"),
                    'conditions'=>array(
                        'Module.id' => $modules_categories
                    ),
                    'order'=>'Module.order ASC',
                    'recursive'=>-1
                ));
                $icons = $this->Module->find("list",array(
                    'fields' => array('Module.id',"Module.icon"),
                    'conditions'=>array(
                        'Module.id' => $modules_categories
                    ),
                    'order'=>'Module.order ASC',
                    'recursive'=>-1
                ));

                //pr($modules);
                //die();

                $header_menu = array();

                foreach ($modules as $keymod => $module) {

                    $modulearray = array(
                        'name' => $module,
                        'icon' => $icons[$keymod],
                        'categories' => array()
                    );

                    //pr($categories);

                    foreach ($categories as $keycat => $category) {
                        if($keymod == $category['Category']['module_id']){
                            $modulearray["categories"][$category['Category']['id']]=$category["Category"];
                        }

                    }

                    $header_menu["modules"][$keymod]= $modulearray;

                }
                //pr($header_menu);
                //pr($actions);
                //die();
                $this->Session->write('User.actions', $actions);
                $this->Session->write('User.header_menu', $header_menu);
            }
        }

        $this->set('header_menu',$header_menu);

    }

    public function is_Authorizate(){

      if($this->Auth->user('id')){
            $this->setHeaderMenu();
            $this->setSidebarMenu();

            $username = $this->Auth->user('lname').' '.$this->Auth->user('fname');
            $this->set('username_menu',$username);

        }

      $cantidad_aros_acos = $this->Session->read('Login.cantidad_aros_acos');

        if(empty($cantidad_aros_acos[0][0])){

            $result_aros_acos = $this->Group->query("SELECT count(*) as contador FROM aros_acos;");
            $cantidad_aros_acos= $result_aros_acos[0][0];

            if($cantidad_aros_acos["contador"] == 0){

				$this->Module->setLanguage();
                $cont_modules = $this->Module->find("count",array("recursive"=>-1));

				$this->Group->setLanguage();
                $cont_group = $this->Group->find("count",array(
                "recursive" => "-1"
                ));
				$this->User->setLanguage();
                $cont_users = $this->User->find("count",array(
                    "recursive" => "-1"
                ));

                if(($cont_group == 0) || ($cont_group == 1) || ($cont_users == 0)){
                    $this->install_acl($cont_modules,$cont_group, $cont_users);
                }elseif ($cantidad_aros_acos["contador"] == 0) {
                    $this->install_acl($cont_modules,$cont_group, $cont_users);
                }

            }else{

                $this->Session->write('Login.cantidad_aros_acos', $cantidad_aros_acos);
            }

        }

    }

    public function getMethod(){
            if ($this->request->is('put')) {
                $method = 'put';
            }elseif ($this->request->is('post')) {
                $method = 'post';
            }
            return $method;
    }

    public function ajaxVariablesInit(){
        $this->dataajax['response']['message_error']="";
        $this->dataajax['response']['message_success']="";
        $this->dataajax['response']['redirect']="";
        $this->dataajax['response']['errors']=array();

        $this->layout = 'ajax';
        $this->autoRender = FALSE;
    }

    function _flash($message,$type='message')
        {
        $messages = (array)$this->Session->read('Message.multiFlash');
        $messages[] = array(
            'message'=>$message,
            'layout'=>'default',
            'element'=>'default',
            'params'=>array('class'=>$type),
            );
        $this->Session->write('Message.multiFlash', $messages);
    }


    public function installBase(){

      // SET foreign_key_checks = 0;
      // TRUNCATE `acos`;
      // TRUNCATE `actions`;
      // TRUNCATE `aros`;
      // TRUNCATE `aros_acos`;
      // TRUNCATE `categories`;
      // TRUNCATE `groupactions`;
      // TRUNCATE `groups`;
      // TRUNCATE `i18n`;
      // TRUNCATE `masters`;
      // TRUNCATE `modules`;
      // TRUNCATE `users`;
      // TRUNCATE `values`;
      // SET foreign_key_checks = 1;


        $data_modules = array(
            array(
                'Module'=> array(
                    'id' => 1,
                    'name' => 'Sistema Base',
                    'order' => 99
                )
            )
        );

        $data_categories = array(
            array(
                'Category'=> array(
                    'id' => 1,
                    'name' => 'Grupos',
                    'module_id' => 1,
                    'order' => 0
                )
            ),
            array(
                'Category'=> array(
                    'id' => 2,
                    'name' => 'Modulos',
                    'module_id' => 1,
                    'order' => 1
                )
            ),
            array(
                'Category'=> array(
                    'id' => 3,
                    'name' => 'Categorias',
                    'module_id' => 1,
                    'order' => 2
                )
            ),
            array(
                'Category'=> array(
                    'id' => 4,
                    'name' => 'Funciones',
                    'module_id' => 1,
                    'order' => 3
                )
            ),
            array(
                'Category'=> array(
                    'id' => 5,
                    'name' => 'Permisos',
                    'module_id' => 1,
                    'order' => 4
                )
            ),
            array(
                'Category'=> array(
                    'id' => 6,
                    'name' => 'Usuarios',
                    'module_id' => 1,
                    'order' => 5
                )
            ),
        );

$data_actions = array(
array(
'Action'=> array('id' => 1,'name' => 'Grupos : Listado','url' => '/admin/groups/index','category_id' => 1,'order' => 0)
),

array(
'Action'=> array('id' => 5,'name' => 'Modulos : Listado','url' => '/admin/modules/index','category_id' => 2,'order' => 0)
),

array(
'Action'=> array('id' => 9,'name' => 'Categorias : Listado','url' => '/admin/categories/index','category_id' => 3,'order' => 0)
),

array(
'Action'=> array('id' => 13,'name' => 'Funciones : Listado','url' => '/admin/actions/index','category_id' => 4,'order' => 0)
),

array(
'Action'=> array('id' => 17,'name' => 'Permisos : Listado','url' => '/admin/groupactions/index','category_id' => 5,'order' => 0)
),
array(
'Action'=> array('id' => 20,'name' => 'Permisos : ACL','url' => '/admin/groupactions/acl','category_id' => 5,'order' => 4)
),

array(
'Action'=> array('id' => 21,'name' => 'Usuarios : Listado','url' => '/admin/users/index','category_id' => 6,'order' => 0)
),
array(
'Action'=> array('id' => 25,'name' => 'Usuarios : Sync','url' => '/admin/users/initDB','category_id' => 6,'order' => 4)
),

);


        $data_groupactions=array();
        $id_groupactions = 1;
        $groupaction = array();
        foreach ($data_actions as $keyaction => $action) {

            $groupaction = array('Groupaction'=> array('id' => $id_groupactions,'group_id' => 1,'action_id' => $action['Action']['id']));
            array_push($data_groupactions, $groupaction);
            $id_groupactions ++;
        }



        if($this->Module->saveMany($data_modules)){


            $this->_flash(__('Instalación: Modulos Creados',true),'alert alert-success');

            if($this->Category->saveMany($data_categories)){

                $this->_flash(__('Instalación: Categorias Creadas',true),'alert alert-success');

                if($this->Action->saveMany($data_actions)){

                     $this->_flash(__('Instalación: Funciones Creadas',true),'alert alert-success');

                     if($this->Groupaction->saveMany($data_groupactions)){
                          $this->_flash(__('Instalación: Permisos Creados',true),'alert alert-success');
                     }

                }

            }

        }


    }

    public function syncACL($message = true) {

        $group = $this->User->Group;
        $group->id = 1;
        $this->Acl->allow(array( 'model' => 'User', 'foreign_key' => 1), 'controllers');

        App::uses('ShellDispatcher', 'Console');
        $command = '-app '.APP.' AclExtras.AclExtras aco_sync';
        $args = explode(' ', $command);
        $dispatcher = new ShellDispatcher($args, false);
        $dispatcher->dispatch();

        if($message){
            $this->_flash(__('msg-acl-syncSuccess',true),'alert alert-success');
        }

    }

    public function install_acl($cont_modules, $cont_group,$cont_users){

        //pr($this->params);

        $redirect = 1;

        if($cont_group == 0){

            if($this->params["controller"] == "groups"){
                $this->Auth->allow('admin_add');
                if($this->params["action"] == "admin_add"){
                    $redirect =0;
                }

            }

            if($redirect){
                $this->redirect('/admin/groups/add');
            }

        }else{

            if($cont_users == 0){
                if($this->params["controller"] == "users"){
                    $this->Auth->allow('admin_add');

                    if($this->params["action"] == "admin_add"){
                        $redirect =0;
                    }

                    if($redirect){
                        $this->redirect('/admin/users/add');
                    }

                }else{
                    if($this->params["controller"] == "groups"){
                        $this->redirect('/admin/users/add');
                    }
                }

            }else{

                if($cont_users >= 1){

                    if($cont_modules == 0){
                        $this->installBase();
                    }

                    $this->syncACL();

                    if($this->params["controller"] == "users"){
                        if(($this->params["action"] != "admin_login")||($this->params["action"] != "admin_logout")){
                          $redirect =0;
                          if($redirect){
                            $this->redirect('/admin/users/logout');
                          }

                        }
                    }
                }

            }

        }

    }

	public function ymd2dmy($var) {
		$var=substr($var, 0, 10);
		return implode("-", array_reverse(explode("-", $var)));
	}

	public function redirectCustom($params, $redir=0) {

		if (!$redir) {
			$redir=0;
			if (isset($params->params["pass"][1]) and $params->params["pass"][1]) { // campo de redirección custom
				$redir=$params->params["pass"][1];
			}
		}
		
		// verifica si el redireccionador tiene el codigo de la entidad
		$redir_id="";
		if  (strpos($redir, "_")!==false) {
			$ent=explode("_", $redir);
			$redir=$ent[0];
			$redir_id=$ent[1];
		}
		
		switch($redir) {
		case 0: // default
			$this->redirect(array('action' => 'admin_index'));
			break;
		case 1: // redirect to Entities/inventory
			$this->redirect('/admin/entities/inventory/'.$redir_id);
			break;
		case 2:
			$this->redirect('/admin/entities/weakness/'.$redir_id);
			break;
		case 3:
			$this->redirect('/admin/entities/perspective/'.$redir_id);
			break;
		case 4:
			$this->redirect('/admin/entities/strategy/'.$redir_id);
			break;
		case 5:
			$this->redirect('/admin/entities/mission/'.$redir_id);
			break;
		case 6:
			$this->redirect('/admin/entities/vision/'.$redir_id);
			break;
		case 7:
			$this->redirect('/admin/entities/objective/'.$redir_id);
			break;
		case 8:
			$this->redirect('/admin/entities/politics/'.$redir_id);
			break;
		case 9:
			$this->redirect('/admin/entities/commitment/'.$redir_id);
			break;
		case 10:
			$this->redirect('/admin/entities/expectation/'.$redir_id);
			break;
		case 11:
			$this->redirect('/admin/entities/program/'.$redir_id);
			break;
		case 12:
			$this->redirect('/admin/entities/activity/'.$redir_id);
			break;
		/*case 13:
			$this->redirect('/admin/entities/iniciative/'.$redir_id);
			break;*/
		/*case 14:
			$this->redirect('/admin/entities/project/'.$redir_id);
			break;*/
		/*case 15:
			$this->redirect('/admin/entities/course/'.$redir_id);
			break;*/
		case 16:
			$this->redirect('/admin/entities/task/'.$redir_id);
			break;
		case 17:
			$this->redirect('/admin/entities/interest/'.$redir_id);
			break;
		case 18:
			$this->redirect('/admin/entities/call/'.$redir_id);
			break;
		case 19:
			$this->redirect('/admin/entities/gallery/'.$redir_id);
			break;
		case 20:
			$this->redirect('/admin/entities/survey/'.$redir_id);
			break;
		case 21:
			$this->redirect('/admin/entities/mailbox/'.$redir_id);
			break;
		case 22:
			$this->redirect('/admin/entities/graph/'.$redir_id);
			break;
		}
		
	}
	
	public function getGalleryImgs($company_id, $node_id) {

		$pathto = dirname(dirname(__FILE__))."/webroot";
		$dir = "/uploads/".$company_id."/".$node_id;
		$scanned_directory = @array_diff(scandir($pathto.$dir), array('..', '.'));
		$imgs=array();
		if (is_array($scanned_directory)) {
			foreach($scanned_directory as $isd=>$sd) {
			
				$trad = $this->Galleryimage->find('all', array('conditions'=>array('imgname'=>$dir."/".$sd), 'recursive'=>1));
			
				$imgs[]=array(	"file"=>$sd,
								"desc"=>array	(
													$trad[0]["GalleryimagenameTranslation"][0]["locale"]=>
														$trad[0]["GalleryimagenameTranslation"][0]["content"],
													$trad[0]["GalleryimagenameTranslation"][1]["locale"]=>
														$trad[0]["GalleryimagenameTranslation"][1]["content"],
												)
				);
			}
		}
		
		return array("dir"=>$dir, "imgs"=>$imgs);
			
	}
	
}

function laVista($ntid) {
	switch($ntid) {
	case 1:
		return "weakness";
		break;
	case 2:
		return "perspective";
		break;
	case 3:
		return "strategy";
		break;
	case 4:
		return "commitment";
		break;
	case 5:
		return "mission";
		break;
	case 6:
		return "vision";
		break;
	case 7:
		return "objective";
		break;
	case 8:
		return "politics";
		break;
	case 9:
		return "expectation";
		break;
	case 10:
		return "program";
		break;
	case 11:
		return "activity";
		break;
	case 12:
		return "iniciative";
		break;
	case 13:
		return "project";
		break;
	case 14:
		return "course";
		break;
	case 15:
		return "task";
	case 16:
		return "interest";
	case 17:
		return "call";
	case 18:
		return "gallery";
	case 19:
		return "survey";
	case 20:
		return "mailbox";
	case 21:
		return "graph";
	default:
		return $ntid;
	}
}

