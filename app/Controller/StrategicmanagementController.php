<?php
App::uses('AppController', 'Controller');
/**
 * Strategicmanagement Controller
 *
 * @property Strategicmanagement $Strategicmanagement
 */

class StrategicmanagementController extends AppController {

	/*----------------beforeFilter-----------------*/
    public function beforeFilter() {
        parent::beforeFilter();
    }
    /*----------------beforeFilter-----------------*/

	/*----------------index-----------------*/
	public function admin_index(){

	}
	/*----------------index-----------------*/


	
	public function admin_woupdate(){

		$this->ajaxVariablesInit();

		$wo_id = $this->params->params["pass"][0];
		$status = $this->params->params["pass"][1];

		$this->Node->read(null, $wo_id);
		$this->Node->set("status", $status);
		$this->Node->saveMany();

		echo json_encode(array("wo_id"=>$wo_id,"status"=>$status));
		die();
		
	}
	
	

	public function admin_inventory(){
	
		$wo = $this->Node->getNodes(Node::TYPE_WEAKNESSES, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
	
		$this->set(compact('wo'));
		
	}
	
	
	public function admin_weakness(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_WEAKNESSES, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=2;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_perspective(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_PERSPECTIVE, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=3;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_strategy(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_STRATEGY, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=4;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_mission(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_MISSION, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=5;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_vision(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_VISION, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=6;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_objective(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_OBJECTIVE, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=7;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_politics(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_POLITICS, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=8;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	/*----------------index-----------------*/

	/*----------------index-----------------*/
	public function admin_maturity(){

		$this->Autoevaluation->setLanguage();
		$this->Questionnaire->setLanguage();
		$conditions=array();
		if ($this->Auth->user()["corporation_id"]) {
			$conditions['Autoevaluation.corporation_id']=$this->Auth->user()["corporation_id"];
		}
		if ($this->Auth->user()["company_id"]) {
			$conditions['Autoevaluation.company_id']=$this->Auth->user()["company_id"];
		}			
		$auto = $this->Autoevaluation->find('all',array(
			'conditions'=>$conditions,
			'order' => array('Corporation.name', 'Company.name', "Autoevaluation.date_time"),
			'recursive'=>1
		));
		
		if (isset($auto) and is_array($auto)) {

			foreach($auto as $i=>$a) {
		
				$questionnaire = $this->Questionnaire->find('first', 
														array(	'conditions'=>array('Questionnaire.id'=>$a["Autoevaluation"]["questionnaire_id"]), 
																'recursive'=>-1)
				);
				
				$auto[$i]["Questionnaire"]=$questionnaire["Questionnaire"];
				
				$auto[$i]["QuestionnaireTotalPoints"]=$this->Questionnaire->calculateTotalPoints($a["Autoevaluation"]["questionnaire_id"]);
				
			}
		
		}
		
		$this->set(compact('auto'));
		
	}
	/*----------------index-----------------*/

	public function admin_add() {
	
		if ($this->data) {
		
			$fieldslocales = array('Node'=>array('name', 'description'));
			$validations = $this->validationLocale($fieldslocales);

			if(empty($validations)){
			
				$this->Node->create();
				$this->Node->set($this->data);

				try{
					if ($this->Node->saveMany()) {
						$this->dataajax['response']['method']=$this->getMethod();
						$this->dataajax['response']['message_success']=__('Save Success',true);
					}
				}catch (Exception $e) {
					$this->dataajax['response']['message_error']=__('Save Error',true);
				}
				
			}else{
				$this->dataajax['response']["errors"]=$validations;
			}
			
		} else {
	
			$this->Nodetype->setLanguage();
			$nodetypes = $this->Nodetype->find('all',array(
				'conditions'=>array('type'=>1),
				'order' => array('Nodetype.name'),
				'recursive'=>-1
			));
			
			$data["Node"]["nodetype_id"]=0;
			if (isset($this->params->params["pass"][0])) 
				$data["Node"]["nodetype_id"] = $this->params->params["pass"][0];
			
			$this->set(compact('nodetypes', 'data'));
			
		}
		
	}

	public function admin_hello() {

		$this->layout = 'clean';

		$id=$this->data["id"];
		$redir=$this->data["redir"];

		$nodos=$node=array();
		
		// agrega al redireccionador el codigo de la entidad para que redireccione directo a la entidad
		if (is_numeric($id) and is_numeric($redir)) {
			$redir = $redir."_".$id;
		}
		
		if ($id) {
		
			// busca los tipos de nodos para agregar entidades relacionadas
			$this->Nodetype->setLanguage();
			$nodetypes = $this->Nodetype->find("list");

			$this->Node->setLanguage();
			$node = $this->Node->find('first', 
				array(	'conditions' => array('Node.id'=>$id), 'recursive'=>-1 )
			);
			
			$node["Nodesource"]=array();
			if ($node["Node"]["origin"]){
				$this->Nodesource->setLanguage();
				$node_source = $this->Nodesource->find('first', 
					array(	'conditions' => array('Nodesource.id'=>$node["Node"]["origin"]) )
				);
				$node["Nodesource"]=$node_source["Nodesource"];
			}
			
			$node["Nodestatus"]=array();
			if ($node["Node"]["status"]) {
				$this->Nodestatus->setLanguage();
				$node_status = $this->Nodestatus->find('first', 
					array(	'conditions' => array('Nodestatus.id'=>$node["Node"]["status"]) )
				);
				$node["Nodestatus"]=$node_status["Nodestatus"];
			}

			$related = $this->Node->getRelatedNodes($id);
			
			
		}
		
		$this->set(compact('id', 'node', 'nodetypes', 'related', 'redir'));

	}
	
	public function admin_selentities() {

		$this->layout = 'clean';

		$id=$this->data["id"];
		$entity_id=$this->data["entity_id"];
		$redir=$this->data["redir"];
		
		$allnodes = $this->Node->getNodes($id, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);

		$this->set(compact('id', 'entity_id', 'allnodes', 'redir'));
		
	}
	
	public function admin_addentityrel() {
	
		$this->layout = 'clean';
		
		$ent1 = $this->params->params["pass"][0];
		$ent2 = $this->params->params["pass"][1];
		$redir = $this->params->params["pass"][2];
	
		$auto=$this->Nodenode->find('all',array(
			'conditions'=>array("OR"=>array(
								array("Nodenode.node_id"=>$ent1,"Nodenode.node_id2"=>$ent2),
								array("Nodenode.node_id"=>$ent2,"Nodenode.node_id2"=>$ent1))),
			'recursive'=>-1
		));
		
		if (!isset($auto) or !is_array($auto) or !sizeof($auto)) {
			$this->Nodenode->create();
			$data= array("Nodenode" => array(
										"node_id" => $ent1,
										"node_id2" => $ent2
										)
						);
			$this->Nodenode->set($data);
			$this->Nodenode->saveMany();
		}
		
		$this->redirectCustom(null, $redir);
	
	}
	
	
}
