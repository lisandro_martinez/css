<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Surveyquestion $Surveyquestion
 */

class SurveyquestionsController extends AppController {

	/*----------------beforeFilter-----------------*/
    public function beforeFilter() {
        parent::beforeFilter();
    }
    /*----------------beforeFilter-----------------*/


        public function paramFilters($urlform){

            $form_config = array();
            $form_config["title"] = __("Search / Filter");
            $form_config["urlform"] = $urlform;
            $form_config["labelbutton"] = __("Search / Filter");
            $this->set('form_config',$form_config);

            $fields_char = array(
                        'node_id'
            );


            $conditions = $this->filterConfig('Surveyquestion',$fields_char);
            $this->recordsforpage();

            return $conditions;

        }


    /*----------------INDEX-----------------*/

        /*----------------get_index-----------------*/
        public function get_index($urlfilter = 'admin_index'){
            $conditions=$this->paramFilters($urlfilter);
            $limit = $this->Session->read('Filter.recordsforpage');

            $this->Surveyquestion->setLanguage();
            $this->Paginator->settings = array(
                'order' => 'Surveyquestion.id ASC',
                'conditions' => $conditions,
                'limit' => $limit,
                'recursive' => 1
            );

			$surveys = $this->Node->getNodes(Node::TYPE_SURVEY, $this->Auth->user('company_id'), $this->Auth->user('corporation_id'), true);
			
            $lists = $this->Paginator->paginate('Surveyquestion');
            $this->set(compact('lists', 'surveys'));
        }
        /*----------------get_index-----------------*/

        /*----------------index-----------------*/
        public function admin_index(){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if ($this->request->is('get')) {
                $this->get_index();
            }
        }
        /*----------------index-----------------*/

    /*----------------INDEX-----------------*/

    /*----------------ADD-----------------*/

        /*----------------post_add-----------------*/
        public function post_add(){


                $this->ajaxVariablesInit();

				$fieldslocales = array('Surveyquestion'=>array());
				$validations = $this->validationLocale($fieldslocales);

				if(empty($validations)){

                    $this->Surveyquestion->create();
                    $this->Surveyquestion->set($this->data);

                    try{
                        if ($this->Surveyquestion->saveMany()) {
                            $this->dataajax['response']['method']=$this->getMethod();
                            $this->dataajax['response']['message_success']=__('Save Success',true);
                        }
                    }catch (Exception $e) {
                        $this->dataajax['response']['message_error']=__('Save Error',true);
                    }
                }else{
					$this->dataajax['response']["errors"]=$validations;
                }

                echo json_encode($this->dataajax);
                die();
        }
        /*----------------post_add-----------------*/

        /*----------------get_add-----------------*/
        public function get_add(){

        }
        /*----------------get_add-----------------*/

        /*----------------add-----------------*/
        public function admin_add() {

			$node_id=$this->params->params["pass"][0];
		
            $form_config = array();
            $form_config["title"] = __("Add")." ".__("Survey Question");
            $form_config["urlform"] = "admin_add";
            $form_config["labelbutton"] = __("Add");
            $form_config["type"] = 'file';
            $this->set('form_config',$form_config);

            if ($this->request->is('post')) {
                $this->post_add();
            }elseif ($this->request->is('get')){
                $this->get_add();
            }

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

			  if ($this->Session->read('Config.MyLangVar')=="esp") {
				$question_types = array(1=>"Selección Simple", 2=>"Selección Múltiple", 3=>"Texto");
			  } else {
				$question_types = array(1=>"Single Selection", 2=>"Multiple Selection", 3=>"Text");
			  }
			  
			  $surveys = $this->Node->getNodes(Node::TYPE_SURVEY, $this->Auth->user('company_id'), $this->Auth->user('corporation_id'), true);

			  $this->set(compact('question_types', 'surveys', 'node_id'));
			  
        }
        /*----------------add-----------------*/

    /*----------------ADD-----------------*/

    /*----------------EDIT-----------------*/

        /*----------------get_edit-----------------*/
        public function get_edit($id){

            $this->Surveyquestion->id = $id;
            if (!$this->Surveyquestion->exists()) {
                $this->_flash(__('No-exist-record',true),'alert alert-warning');
                $this->redirect(array('action' => 'admin_edit'));
            }else{

            	$this->set(
                	array(
                		"modules" => $this->Module->find("list")
                	)
                );

                $datamodel = $this->Surveyquestion->read(null, $id);
                $this->request->data = $this->readWithLocale($datamodel); // se trae la informacion al editar.
				
                $this->set(compact('id'));
            }

        }
        /*----------------get_edit-----------------*/

        /*----------------post_edit-----------------*/
        public function post_edit($id){

                $this->ajaxVariablesInit();
				
				$fieldslocales = array('Surveyquestion'=>array());
				$validations = $this->validationLocale($fieldslocales);

				if(empty($validations)){

                    $this->Surveyquestion->id = $id;
                    $this->Surveyquestion->set($this->data);

                    try{
                        if ($this->Surveyquestion->saveMany()) {
                            $this->dataajax['response']['method']=$this->getMethod();
                             $this->dataajax['response']['message_success']=__('Update Success: '.$id,true);
                        }
                    }catch (Exception $e) {
                        $this->dataajax['response']['message_error']=__('Update Error',true);
                    }
                }else{
					$this->dataajax['response']["errors"]=$validations;
                }
                echo json_encode($this->dataajax);
                die();
        }
        /*----------------post_edit-----------------*/

        /*----------------edit-----------------*/
        public function admin_edit($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            $form_config = array();
            $form_config["title"] = __("Edit")." ".__("Survey question");
            $form_config["urlform"] = "admin_edit";
            $form_config["labelbutton"] = __("Save");
            $form_config["type"] = 'put';
            $this->set('form_config',$form_config);

            if ($this->request->is('get')) {
                if(empty($id)){
                    $this->get_index('admin_edit');
                }else{
                    $this->get_edit($id);
                }
            }else{
                if ($this->request->is('put')) {
                    $this->post_edit($id);
                }
            }

			  if ($this->Session->read('Config.MyLangVar')=="esp") {
				$question_types = array(1=>"Selección Simple", 2=>"Selección Múltiple", 3=>"Texto");
			  } else {
				$question_types = array(1=>"Single Selection", 2=>"Multiple Selection", 3=>"Text");
			  }
			  
				$surveys=array();
				App::uses('ConnectionManager', 'Model'); 
				$db = ConnectionManager::getDataSource('default');
				if ($db->isConnected()) { 
					$query = "SELECT `Node`.`id`, (`I18n__name`.`content`) AS `Node__i18n_name` FROM `nodes` AS `Node` INNER JOIN `i18n` AS `I18n__name` ON (`Node`.`id` = `I18n__name`.`foreign_key` AND `I18n__name`.`model` = 'Node' AND `I18n__name`.`field` = 'name' AND `I18n__name`.`locale` = 'esp') INNER JOIN nodecompanies ON Node.id=nodecompanies.node_id WHERE 1 = 1 AND Node.nodetype_id = 19 and nodecompanies.company_id=".$this->Auth->user('company_id');
					foreach($db->query($query) as $r) {
						$surveys[$r["Node"]["id"]]=$r[0]["Node__i18n_name"];
					}
				}
			  
			  $this->set(compact('question_types', 'surveys'));
			  
        }
        /*----------------edit-----------------*/

    /*----------------EDIT-----------------*/


    /*----------------DELETE-----------------*/

        /*----------------delete-----------------*/
        public function admin_delete($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if(!empty($id)){
                $this->Surveyquestion->id = $id;
                if (!$this->Surveyquestion->exists()) {
                    $this->_flash(__('No-exist-record', true),'alert alert-danger');
                    $this->redirect(array('action' => 'admin_index'));
                }

                try{
                    if ($this->Surveyquestion->delete($id,true)) {
                        $this->_flash(__('Delete Success', true),'alert alert-success');
						$this->redirectCustom($this->params);
//                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                        $this->_flash(__('Delete Error', true),'alert alert-warning');
                        $this->redirect(array('action' => 'admin_index'));
                }
            }else{

                $this->get_index('admin_index');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------delete-----------------*/

        /*----------------delete-----------------*/
        public function admin_deletemulti(){

            if($this->request->is('post')){
                //pr($this->data);
                $dataids =  $this->data['Surveyquestion']['id'];

                try{
                    if ($this->Surveyquestion->deleteAll(array('Surveyquestion.id' => $dataids))) {
                        $this->_flash(__('Delete Success',true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                    $this->_flash(__('Delete Error', true),'alert alert-warning');
                    $this->redirect(array('action' => 'admin_index'));
                }

            }else{
                $this->_flash(__('Delete Error', true),'alert alert-danger');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------delete-----------------*/

    /*----------------DELETE-----------------*/

}
