<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Corporation $Corporation
 */

class CorporationsController extends AppController {

	/*----------------beforeFilter-----------------*/
    public function beforeFilter() {
        parent::beforeFilter();
    }
    /*----------------beforeFilter-----------------*/


        public function paramFilters($urlform){

            $form_config = array();
            $form_config["title"] = __("Search / Filter");
            $form_config["urlform"] = $urlform;
            $form_config["labelbutton"] = __("Search / Filter");
            $this->set('form_config',$form_config);

            $fields_char = array(
                        'name',
                        'description',
                        'phone',
                        'address'
            );


            $conditions = $this->filterConfig('Corporation',$fields_char);
            $this->recordsforpage();

            return $conditions;

        }


    /*----------------INDEX-----------------*/

        /*----------------get_index-----------------*/
        public function get_index($urlfilter = 'admin_index'){
            $conditions=$this->paramFilters($urlfilter);
            $limit = $this->Session->read('Filter.recordsforpage');

			$this->Corporation->setLanguage();
            $this->Paginator->settings = array(
                'order' => 'Corporation.id ASC',
                'conditions' => $conditions,
                'limit' => $limit,
                'recursive' => 1
            );

            $lists = $this->Paginator->paginate('Corporation');
            $this->set(compact('lists'));
        }
        /*----------------get_index-----------------*/

        /*----------------index-----------------*/
        public function admin_index(){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if ($this->request->is('get')) {
                $this->get_index();
            }
        }
        /*----------------index-----------------*/

    /*----------------INDEX-----------------*/

    /*----------------ADD-----------------*/

        /*----------------post_add-----------------*/
        public function post_add(){

                $this->ajaxVariablesInit();

				$fieldslocales = array('Corporation'=>array());
				$validations = $this->validationLocale($fieldslocales);

				if(empty($validations)){

                    $folder = 'files/logos';
                    $uploadPath = WWW_ROOT;
                    $image = $this->data['Corporation']['logo_up'];

                    if(isset($image['name'])){
                                $imageName = $image['name'];
                                $ext = substr(strtolower(strrchr($image['name'], '.')), 1);
                                $imageName = date('His') . md5($imageName).'.'.$ext;
                                $only_patch_img = $folder . '/' . $imageName;
                                $full_image_path = $uploadPath . $only_patch_img;

                                try{
                                    if (move_uploaded_file($image['tmp_name'], $full_image_path)){
                                        $logo_url=$only_patch_img;
                                    }
                                }catch(Exception $e){
                                    $this->dataajax['response']['message_error']=__('Save-error-upload-files',true);
                                }
                    }

                    $this->Corporation->create();
                    $this->Corporation->set($this->data);
                    $this->Corporation->set('logo',$logo_url);

                    try{
                        if ($this->Corporation->saveMany()) {
                            $this->dataajax['response']['method']=$this->getMethod();
                            $this->dataajax['response']['message_success']=__('Save Success',true);
                        }
                    }catch (Exception $e) {
                        $this->dataajax['response']['message_error']=__('Save Error',true);
                    }
				}else{
					$this->dataajax['response']["errors"]=$validations;
				}
                echo json_encode($this->dataajax);
                die();
        }
        /*----------------post_add-----------------*/

        /*----------------get_add-----------------*/
        public function get_add(){

        }
        /*----------------get_add-----------------*/

        /*----------------add-----------------*/
        public function admin_add() {
		
            $form_config = array();
            $form_config["title"] = __("Add") . " " . __("Corporation");
            $form_config["urlform"] = "admin_add";
            $form_config["labelbutton"] = __("Add");
            $form_config["type"] = 'file';
            $this->set('form_config',$form_config);

            if ($this->request->is('post')) {
                $this->post_add();
            }

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

        }
        /*----------------add-----------------*/

    /*----------------ADD-----------------*/

    /*----------------EDIT-----------------*/

        /*----------------get_edit-----------------*/
        public function get_edit($id){

            $this->Corporation->id = $id;
            if (!$this->Corporation->exists()) {
                $this->_flash(__('No-exist-record',true),'alert alert-warning');
                $this->redirect(array('action' => 'admin_edit'));
            }else{

            	$this->set(
                	array(
                		"modules" => $this->Module->find("list")
                	)
                );

                $datamodel = $this->Corporation->read(null, $id);
                $this->request->data = $this->readWithLocale($datamodel); // se trae la informacion al editar.
                $this->set(compact('id'));
            }

        }
        /*----------------get_edit-----------------*/

        /*----------------post_edit-----------------*/
        public function post_edit($id){

                $this->ajaxVariablesInit();
				
				$fieldslocales = array('Corporation'=>array());
				$validations = $this->validationLocale($fieldslocales);

				if(empty($validations)){

                  $folder = 'files/logos';
                  $uploadPath = WWW_ROOT;
                  $image = $this->data['Corporation']['logo_up_edit'];

                  if(!empty($image)){

                    if(isset($image['name'])){
						$imageName = $image['name'];
						$ext = substr(strtolower(strrchr($image['name'], '.')), 1);
						$imageName = date('His') . md5($imageName).'.'.$ext;
						$only_patch_img = $folder . '/' . $imageName;
						$full_image_path = $uploadPath . $only_patch_img;

						try{
							if (move_uploaded_file($image['tmp_name'], $full_image_path)){
								$logo_url=$only_patch_img;
							}
						}catch(Exception $e){
							$this->dataajax['response']['message_error']=__('Save-error-upload-files',true);
						}
                    }

                  }
				  
                    $this->Corporation->id = $id;
                    $this->Corporation->set($this->data);

                    if(isset($logo_url)){
                      $this->Corporation->set('logo',$logo_url);
                    }

                    try{
                        if ($this->Corporation->saveMany()) {
                            $this->dataajax['response']['method']=$this->getMethod();
                             $this->dataajax['response']['message_success']=__('Update Success',true);
                        }
                    }catch (Exception $e) {
                        $this->dataajax['response']['message_error']=__('Update Error',true);
                    }
				}else{
					$this->dataajax['response']["errors"]=$validations;
				}
                echo json_encode($this->dataajax);
                die();
        }
        /*----------------post_edit-----------------*/

        /*----------------edit-----------------*/
        public function admin_edit($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            $form_config = array();
            $form_config["title"] = __("Edit") . " " . __("Corporation");
            $form_config["urlform"] = "admin_edit";
            $form_config["labelbutton"] = __("Save");
            $form_config["type"] = 'put';
            $this->set('form_config',$form_config);



            if ($this->request->is('get')) {
                if(empty($id)){
                    $this->get_index('admin_edit');
                }else{
                    $this->get_edit($id);
                }
            }else{
                if ($this->request->is('put')) {
                    $this->post_edit($id);
                }
            }

        }
        /*----------------edit-----------------*/

    /*----------------EDIT-----------------*/


    /*----------------DELETE-----------------*/

        /*----------------delete-----------------*/
        public function admin_delete($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if(!empty($id)){
                $this->Corporation->id = $id;
                if (!$this->Corporation->exists()) {
                    $this->_flash(__('No-exist-record', true),'alert alert-danger');
                    $this->redirect(array('action' => 'admin_index'));
                }

                try{
                    if ($this->Corporation->delete($id,true)) {
                        $this->_flash(__('Delete Success', true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                        $this->_flash(__('Delete Error', true),'alert alert-warning');
                        $this->redirect(array('action' => 'admin_index'));
                }
            }else{

                $this->get_index('admin_index');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------delete-----------------*/

        /*----------------delete-----------------*/
        public function admin_deletemulti(){

            if($this->request->is('post')){
                //pr($this->data);
                $dataids =  $this->data['Corporation']['id'];

                try{
                    if ($this->Corporation->deleteAll(array('Corporation.id' => $dataids))) {
                        $this->_flash(__('Delete Success',true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                    $this->_flash(__('Delete Error', true),'alert alert-warning');
                    $this->redirect(array('action' => 'admin_index'));
                }

            }else{
                $this->_flash(__('Delete Error', true),'alert alert-danger');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------delete-----------------*/

    /*----------------DELETE-----------------*/

}
