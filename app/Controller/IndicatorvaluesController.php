<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Indicatorvalue $Indicatorvalue
 */

class IndicatorvaluesController extends AppController {

	/*----------------beforeFilter-----------------*/
    public function beforeFilter() {
        parent::beforeFilter();
    }
    /*----------------beforeFilter-----------------*/


        public function paramFilters($urlform){

            $form_config = array();
            $form_config["title"] = __("Search / Filter");
            $form_config["urlform"] = $urlform;
            $form_config["labelbutton"] = __("Search / Filter");
            $this->set('form_config',$form_config);

            $fields_char = array(
                        'value',
            );
			
            $conditions = $this->filterConfig('Indicatorvalue',$fields_char);

            $this->recordsforpage();

			// Busca los indicadores de gestión de la compañía del usuario
			$conditions[] = $this->Indicator->conditionsListByCompany($this->Auth->user('company_id'), $this->Auth->user('corporation_id'));
			
			return $conditions;

        }


    /*----------------INDEX-----------------*/

        /*----------------get_index-----------------*/
        public function get_index($urlfilter = 'admin_index'){
            $conditions=$this->paramFilters($urlfilter);
            $limit = $this->Session->read('Filter.recordsforpage');

            $this->Indicatorvalue->setLanguage();
            $this->Paginator->settings = array(
                'order' => 'Indicatorvalue.id ASC',
                'conditions' => $conditions,
                'limit' => $limit,
                'recursive' => 1
            );

            $lists = $this->Paginator->paginate('Indicatorvalue');
			
            $this->set(compact('lists'));
        }
        /*----------------get_index-----------------*/

        /*----------------index-----------------*/
        public function admin_index(){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if ($this->request->is('get')) {
                $this->get_index();
            }
			
			$indicators=$this->Indicator->indicatorsByCompany($this->Auth->user('company_id'), $this->Auth->user('corporation_id'));
			
            $this->set(compact('indicators'));
			
			
        }
        /*----------------index-----------------*/

    /*----------------INDEX-----------------*/

    /*----------------ADD-----------------*/

        /*----------------post_add-----------------*/
        public function post_add(){

			$this->ajaxVariablesInit();

			$fieldslocales = array('Indicatorvalue'=>array('name'));
			$validations = $this->validationLocale($fieldslocales);

			if(empty($validations)){

				$data=self::preProcessData($this->data);
				
				$this->Indicatorvalue->create();
				$this->Indicatorvalue->set($data);

				try{
					if ($this->Indicatorvalue->saveMany()) {
						$this->dataajax['response']['method']=$this->getMethod();
						$this->dataajax['response']['message_success']=__('Save Success',true);
					}
				}catch (Exception $e) {
					$this->dataajax['response']['message_error']=__('Save Error',true);
				}

			}else{
				$this->dataajax['response']["errors"]=$validations;
			}
			
			echo json_encode($this->dataajax);
			die();
			
        }
        /*----------------post_add-----------------*/

        /*----------------get_add-----------------*/
        public function get_add(){

			if ($indicator_id = $_GET["indicator_id"]) {
				$indicator = $this->Indicator->read(null, $indicator_id);
				$periodic_measure = $indicator["Indicator"]["periodic_measure"];
				$name = $indicator["Indicator"]["name"];
			}
		
			$this->set(compact('id', 'indicator_id', 'periodic_measure', 'name'));

        }
        /*----------------get_add-----------------*/

        /*----------------add-----------------*/
        public function admin_add() {
		
            $form_config = array();
            $form_config["title"] = __("Add")." ".__("Indicator Value");
            $form_config["urlform"] = "admin_add";
            $form_config["labelbutton"] = __("Add");
            $form_config["type"] = 'file';
            $this->set('form_config',$form_config);

            if ($this->request->is('post')) {
                $this->post_add();
            }elseif ($this->request->is('get')){
                $this->get_add();
            }

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

			$is_root = $this->isRoot();

			// Busca los indicadores de gestión de la compañía del usuario
			$indicators = $this->Indicator->indicatorsByCompany($this->Auth->user('company_id'), $this->Auth->user('corporation_id'));
			
			$this->set(compact('is_root', 'indicators'));
			  
        }
        /*----------------add-----------------*/

    /*----------------ADD-----------------*/

    /*----------------EDIT-----------------*/

        /*----------------get_edit-----------------*/
        public function get_edit($id){

            $this->Indicatorvalue->id = $id;
            if (!$this->Indicatorvalue->exists()) {
                $this->_flash(__('No-exist-record',true),'alert alert-warning');
                $this->redirect(array('action' => 'admin_edit'));
            }else{

            	$this->set(
                	array(
                		"modules" => $this->Module->find("list")
                	)
                );

                $datamodel = $this->Indicatorvalue->read(null, $id);
                $this->request->data = self::preProcessData($this->readWithLocale($datamodel)); // se trae la informacion al editar.
				
				if ($indicator_id = $_GET["indicator_id"]) {
					$indicator = $this->Indicator->read(null, $indicator_id);
					$periodic_measure = $indicator["Indicator"]["periodic_measure"];
					$name = $indicator["Indicator"]["name"];
				}
		
				$this->set(compact('id', 'indicator_id', 'periodic_measure', 'name'));
			
            }

        }
        /*----------------get_edit-----------------*/

        /*----------------post_edit-----------------*/
        public function post_edit($id){

			$this->ajaxVariablesInit();
			
			$fieldslocales = array('Indicatorvalue'=>array('name'));
			$validations = $this->validationLocale($fieldslocales);

			if(empty($validations)){
			
				$data=self::preProcessData($this->data);
				
				$this->Indicatorvalue->id = $id;
				$this->Indicatorvalue->set($data);

				try{
					if ($this->Indicatorvalue->saveMany()) {

						$this->dataajax['response']['method']=$this->getMethod();
						$this->dataajax['response']['message_success']=__('Update Success',true);
					}
				}catch (Exception $e) {
					$this->dataajax['response']['message_error']=__('Update Error: '.$e->getMessage(),true);
				}
				
			}else{
				$this->dataajax['response']["errors"]=$validations;
			}
			
			echo json_encode($this->dataajax);
			die();

        }
        /*----------------post_edit-----------------*/

        /*----------------edit-----------------*/
        public function admin_edit($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            $form_config = array();
            $form_config["title"] = __("Edit")." ".__("Indicator Value");
            $form_config["urlform"] = "admin_edit";
            $form_config["labelbutton"] = __("Save");
            $form_config["type"] = 'put';
            $this->set('form_config',$form_config);

            if ($this->request->is('get')) {
                if(empty($id)){
                    $this->get_index('admin_edit');
                }else{
                    $this->get_edit($id);
                }
            }else{
                if ($this->request->is('put')) {
                    $this->post_edit($id);
                }
            }

			  $is_root = $this->isRoot();

			// Busca los indicadores de gestión de la compañía del usuario
/*
			$this->Indicator->setLanguage();			
			$indicators = $this->Indicator->indicatorsByCompany($this->Auth->user('company_id'), $this->Auth->user('corporation_id'));

			INTENTO DE SOLUCION

			App::uses('Indicator', 'Model');
			$Indicator=new Indicator();
			$Indicator->setLanguage();
			$indicators = $Indicator->indicatorsByCompany($this->Auth->user('company_id'), $this->Auth->user('corporation_id'));

/*
		SOLUCION DRASTICA
*/		
			App::uses('Indicator', 'Model');
			$Indicator = @new Indicator();
			$Indicator->setLanguage();
			$indicators = $Indicator->conditionsListByCompany($this->Auth->user('company_id'), $this->Auth->user('corporation_id'), true);
			
				// bug fix i18n
				$nodetypes=array();
				App::uses('ConnectionManager', 'Model'); 
				$db = ConnectionManager::getDataSource('default');
				
				if ($db->isConnected()) { 
					
					$query = "SELECT `Indicator`.`id`, (`I18n__name`.`content`) AS `Indicator__i18n_name` FROM `indicators` AS `Indicator` INNER JOIN `i18n` AS `I18n__name` ON (`Indicator`.`id` = `I18n__name`.`foreign_key` AND `I18n__name`.`model` = 'Indicator' AND `I18n__name`.`field` = 'name' AND `I18n__name`.`locale` = '".$this->Session->read('Config.MyLangVar')."') WHERE 1 = 1 AND Indicator.id IN (".implode(",", $indicators).")";
					
					foreach($db->query($query) as $r) {
						$indicators[$r["Indicator"]["id"]]=$r[0]["Indicator__i18n_name"];
					}
					
				}
			
			  $this->set(compact('is_root','indicators'));
			  
        }
        /*----------------edit-----------------*/

    /*----------------EDIT-----------------*/


    /*----------------DELETE-----------------*/

        /*----------------delete-----------------*/
        public function admin_delete($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if(!empty($id)){
                $this->Indicatorvalue->id = $id;
                if (!$this->Indicatorvalue->exists()) {
                    $this->_flash(__('No-exist-record', true),'alert alert-danger');
                    $this->redirectCustom($this->params);
                }

                try{
                    if ($this->Indicatorvalue->delete($id,true)) {
                        $this->_flash(__('Delete Success', true),'alert alert-success');
						$this->redirectCustom($this->params);
                    }
                }catch (Exception $e) {
                        $this->_flash(__('Delete Error', true),'alert alert-warning');
						$this->redirectCustom($this->params);
                }
            }else{

                $this->get_index('admin_index');
				$this->redirectCustom($this->params);
				
            }

        }
        /*----------------delete-----------------*/

        /*----------------delete-----------------*/
        public function admin_deletemulti(){

            if($this->request->is('post')){
                //pr($this->data);
                $dataids =  $this->data['Indicatorvalue']['id'];

                try{
                    if ($this->Indicatorvalue->deleteAll(array('Indicatorvalueid' => $dataids))) {
                        $this->_flash(__('Delete Success',true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                    $this->_flash(__('Delete Error', true),'alert alert-warning');
                    $this->redirect(array('action' => 'admin_index'));
                }

            }else{
                $this->_flash(__('Delete Error', true),'alert alert-danger');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------delete-----------------*/

    /*----------------DELETE-----------------*/

	
	private function preProcessData($data) {

		if (isset($data["Indicatorvalue"]["date"])) {
			$data["Indicatorvalue"]["date"]=self::ymd2dmy($data["Indicatorvalue"]["date"]);
		}
		
		return $data;
	
	}

	
}
