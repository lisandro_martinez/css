<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Autoevaluation $Autoevaluation
 */

class AutoevaluationsController extends AppController {

	/*----------------beforeFilter-----------------*/
    public function beforeFilter() {
        parent::beforeFilter();
    }
    /*----------------beforeFilter-----------------*/


        public function paramFilters($urlform){

            $form_config = array();
            $form_config["title"] = __("Search / Filter");
            $form_config["urlform"] = $urlform;
            $form_config["labelbutton"] = __("Search / Filter");
            $this->set('form_config',$form_config);

            $fields_char = array(
				'questionnaire_id',
            );

            $conditions = $this->filterConfig('Autoevaluation',$fields_char);
            $this->recordsforpage();

            return $conditions;

        }


    /*----------------INDEX-----------------*/

        /*----------------get_index-----------------*/
        public function get_index($urlfilter = 'admin_index'){
		
            $conditions=$this->paramFilters($urlfilter);
            $limit = $this->Session->read('Filter.recordsforpage');

            $this->Autoevaluation->setLanguage();
            $this->Questionnaire->setLanguage();
			$conditions=array();
			if ($this->Auth->user()["corporation_id"]) {
				$conditions['Autoevaluation.corporation_id']=$this->Auth->user()["corporation_id"];
			}
			if ($this->Auth->user()["company_id"]) {
				$conditions['Autoevaluation.company_id']=$this->Auth->user()["company_id"];
			}			
			$auto = $this->Autoevaluation->find('all',array(
				'conditions'=>$conditions,
				'recursive'=>-1
			));
			
			if (isset($auto) and is_array($auto)) {
				foreach($auto as $i=>$a) {
					$questionnaire = $this->Questionnaire->find('first', 
															array(	'conditions'=>array('Questionnaire.id'=>$a["Autoevaluation"]["questionnaire_id"]), 
																	'recursive'=>-1)
					);
					$auto[$i]["Questionnaire"]=$questionnaire["Questionnaire"];
				}
			}
			
            $this->set(compact('auto'));
			
        }
        /*----------------get_index-----------------*/

        /*----------------index-----------------*/
        public function admin_index(){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if ($this->request->is('get')) {
                $this->get_index();
            }
        }
        /*----------------index-----------------*/

    /*----------------INDEX-----------------*/

    /*----------------ADD-----------------*/

        /*----------------post_add-----------------*/
        public function post_add(){

			$this->ajaxVariablesInit();

			$fieldslocales = array('Autoevaluation'=>array('name'));
			$validations = $this->validationLocale($fieldslocales);

			if(empty($validations)){
			
				$this->Autoevaluation->create();
				$this->Autoevaluation->set($this->data);

				try{
					if ($this->Autoevaluation->saveMany()) {
						$this->dataajax['response']['method']=$this->getMethod();
						$this->dataajax['response']['message_success']=__('Save Success',true);
					}
				}catch (Exception $e) {
					$this->dataajax['response']['message_error']=__('Save Error',true);
				}
			}else{
				$this->dataajax['response']["errors"]=$validations;
			}

			echo json_encode($this->dataajax);
			die();
			
        }
        /*----------------post_add-----------------*/

        /*----------------get_add-----------------*/
        public function get_add(){

        }
        /*----------------get_add-----------------*/

        /*----------------add-----------------*/
        public function admin_add() {
		
			$form_config = array();
			$form_config["title"] = __("Autoevaluation");
			$form_config["urlform"] = "admin_add";
			$form_config["labelbutton"] = __("Start");
			$form_config["type"] = 'file';
			$this->set('form_config',$form_config);

			/**
			*
			* AUTOEVALUACION PASO 1
			* 
			*/
			
			if ($this->request->is('get')) {

				$this->get_add();

				if($this->request->is('ajax')){
					$this->layout = 'ajax';
				}

				$questionnaires = $this->Questionnaire->findByCo(
					'full',
					$this->Auth->user()["corporation_id"], 
					$this->Auth->user()["company_id"]
				);
				
				$is_root = $this->isRoot();

				$paso=1;
				$this->set(compact('questionnaires','is_root','paso'));
				
			} elseif ($this->request->is('post')) {

				$paso=2;
				if (isset($this->data["paso"]) and $this->data["paso"]) {
					$paso = $this->data["paso"];
				}

				switch($paso) {
				case 2:
				
					/**
					*
					* AUTOEVALUACION PASO 2
					* 
					*/
					
					$s=array();
					
					$qid=$this->data["Autoevaluation"]["questionnaire_id"];
					
					$this->Questionnaire->setLanguage();
					$q = $this->Questionnaire->find('all',array(
					  'conditions'=>array(
						  'Questionnaire.id'=>$qid
					  ),
					  'recursive'=>1
					));

					if (is_array($q) and sizeof($q)) {
						
						$q=current($q);
						
						$this->Section->setLanguage();
						$s = $this->Section->find('all',array(
						  'conditions'=>array(
							  'questionnaire_id'=>$qid
						  ),
						  'order'=>'order',
						  'recursive'=>-1
						));
						
						if (is_array($s) and sizeof($s)) {
							
							foreach ($s as $is => $es) {
							
								$this->Question->setLanguage();
								$ques = $this->Question->find('all',array(
								  'conditions'=>array(
									  'section_id'=>$es["Section"]["id"]
								  ),
								  'order'=>'order',
								  'recursive'=>-1
								));
							
								if (is_array($ques) and sizeof($ques)) {
									
									foreach ($ques as $iques => $question) {
									
										$this->Answer->setLanguage();
										$a = $this->Answer->find('all',array(
										  'conditions'=>array(
											  'question_id'=>$question["Question"]["id"]
										  ),
										  'order'=>'order',
										  'recursive'=>-1
										));
									
										$ques[$iques]['Answers']=$a;
									
									}
									
								}
						
								$s[$is]['Questions']=$ques;
							
							}
							
						}
						
					}

					$respuestas_dadas2=array();
					$autoeval_id=0;
					if (isset($this->data["autoeval_id"])) {
						$autoeval_id = $this->data["autoeval_id"];
						$this->Autoevaluationanswer->setLanguage();
						$respuestas_dadas = $this->Autoevaluationanswer->find('all',array(
						  'conditions'=>array(
							  'Autoevaluationanswer.autoevaluation_id'=>$this->data["autoeval_id"]
						  ),
						  'recursive'=>-1
						));
						
						foreach($respuestas_dadas as $rd) {
							$respuestas_dadas2[$rd["Autoevaluationanswer"]["question_id"]]=$rd["Autoevaluationanswer"]["answer_id"];
						}
						unset($respuestas_dadas);
						
					}
					
					$this->set(compact('qid', 'paso', 'q', 's', 'respuestas_dadas2', 'autoeval_id'));
				
					break;
					
				case 3:

					/**
					*
					* AUTOEVALUACION PASO 3
					* 
					*/
					
					// Agrega o edita la autoevaluación
					
					$qid=$this->data["questionnaire_id"];
					
					$autoeval_id=$this->data["autoeval_id"];
					
					$this->Questionnaire->setLanguage();
					$laq = $this->Questionnaire->find('all',array(
					  'conditions'=>array(
						  'Questionnaire.id'=>$qid
					  ),
					  'recursive'=>1
					));

					$this->Autoevaluation->setLanguage();
					
					if ($autoeval_id) {
						// es una modificación, entonces se borra para crear una nueva...
						$au = $this->Autoevaluation->read(null, $autoeval_id);
						$this->Autoevaluation->delete();
					} 
					
					// es una autoevaluación nueva... la crea
					$this->Autoevaluation->create();

					$corp_id=$this->Auth->user()["corporation_id"];
					
					// se debe agregar la corporación a la autoevaluación (si la hay)
					if (!$corp_id and $this->Auth->user()["company_id"]) {
						$the_corp = $this->Company->find('first', array(
							'conditions'=>array('Company.id'=>$this->Auth->user()["company_id"]),
							'recursive'=>-1
							)
						);
						if (isset($the_corp["Company"]["corporation_id"])) {
							$corp_id=$the_corp["Company"]["corporation_id"];
						}
					}
					
					$data=array("Autoevaluation"=>array(
						"user_id"=>$this->Auth->user()["id"],
						"questionnaire_id"=>$this->data["questionnaire_id"],
						"company_id"=>$this->Auth->user()["company_id"],
						"corporation_id"=>$corp_id,
					));
					
					if (!isset($data["Autoevaluation"]["company_id"]) or !$data["Autoevaluation"]["company_id"])
						$data["Autoevaluation"]["company_id"]=0;
					if (!isset($data["Autoevaluation"]["corporation_id"]) or !$data["Autoevaluation"]["corporation_id"])
						$data["Autoevaluation"]["corporation_id"]=0;
						
					$this->Autoevaluation->set($data);
					
					$autoeval=null;
					$las_respuestas=null;
					$puntos=0;
					
					try{
					
						if ($this->Autoevaluation->saveMany()) {

							// Agrega las respuestas

							if (isset($this->data["Answers"])) {
							
								foreach($this->data["Answers"] as $q => $a) {
								
									$this->Autoevaluationanswer->setLanguage();
									$this->Autoevaluationanswer->create();
									
									$autoeval=$this->Autoevaluation;
									
									$data=array("Autoevaluationanswer"=>array(
										"autoevaluation_id"=>$this->Autoevaluation->id,
										"question_id"=>$q,
										"answer_id"=>$a
										)
									);
									
									$this->Autoevaluationanswer->set($data);
									
									$this->Autoevaluationanswer->saveMany();
									
									// Evalua
									
									$an = $this->Answer->find('first',array(
									  'conditions'=>array(
										  'Answer.id'=>$a
									  ),
									  'recursive'=>-1
									));

									$puntos+=$an["Answer"]["points"];
									
								}
								
							}
							
							$this->Autoevaluation->set("total_points", $puntos);
							$this->Autoevaluation->save();
							
							// Muestra la confirmación

							$this->redirect("/admin/autoevaluations/view/".$this->Autoevaluation->id);
							
						} else {
						
							throw new Exception("Error al guardar autoevaluación");
						
						}
						
					} catch (Exception $e) {

							$this->Session->setFlash($e->getMessage());

					}
					
					$puntaje_total=0;
					
					// Busca el mayor puntaje posible de este cuestionario
					if ($puntos) {
						$puntaje_total=$this->Questionnaire->calculateTotalPoints($qid);
					}
					
					$porcentaje=0;
					if ($puntaje_total) {
						$porcentaje=round($puntos/$puntaje_total*100, 0);
					}
					
					$autoeval_id=$this->Autoevaluation->id;
					$q=current($laq);
					
					$this->set(compact('paso', 'q', 'puntos', 'porcentaje', 'puntaje_total', 'las_respuestas', 'autoeval_id'));
				
					break;
					
				}
				
			
			}
			  
        }
        /*----------------add-----------------*/

    /*----------------ADD-----------------*/

	
		public function admin_view() {

			// Si hay data en post
			if (isset($this->request->data["AutoevaluationsToSave"])) {
			
				App::uses('Nodecompany', 'Model');
				App::uses('Autoevaluationanswer', 'Model');
				
				// recorrer el arreglo: AutoevaluationsToSave
				foreach($this->request->data["AutoevaluationsToSave"] as $aea_id=>$answer_id) {
				
					// Buscar el autoevaluationanswers de codigo  AutoevaluationsToSave[i] : $aea_id
					// y luego la respuesta de código answer_id de esa misma tabla: $answer_id
					// tomar el título y el feedback de esa answer_id ... 
					
					$this->Answer->setLanguage();
					$answer = $this->Answer->find('first',array(
					  'conditions'=>array(
						  'Answer.id'=>$answer_id
					  ),
					  'recursive'=>1
					));
					
					// ... y guardarlo como entidad "Debilidad" en Nodes
					
					$data= array("Node" => array(
												"name" => array("esp" => $answer["Answerfeedback_titleTranslation"][0]["content"],
																"eng" => $answer["Answerfeedback_titleTranslation"][1]["content"]),
												"description" => array(	"esp" => $answer["AnswerfeedbackTranslation"][0]["content"],
																		"eng" => $answer["AnswerfeedbackTranslation"][1]["content"]),
												"nodetype_id" => 1,
												"origin" => 1,
												"corporation_id" => $this->Auth->user('corporation_id')
												)
								);

					$this->Node->create();
					$this->Node->set($data);
					$this->Node->saveMany();
					
					$this->Node->setLanguage();
					$el_node=$this->Node->find('first', array('order'=>'Node.id DESC'));

					// Guarda la relacion del nodo con la empresa
					if ($this->Auth->user('company_id')) {
						$Nodecompany = new Nodecompany();
						$data= array("Nodecompany" => array(
													"node_id" => $el_node["Node"]["id"],
													"company_id" => $this->Auth->user('company_id')
													)
									);
						$Nodecompany->set($data);
						$Nodecompany->saveMany();
					}

					// Marcar el autoevaluationanswers de codigo  AutoevaluationsToSave[i] como "saved"

					
					$Autoevaluationanswer = new Autoevaluationanswer();
					$Autoevaluationanswer->read(null, $aea_id);
					$Autoevaluationanswer->set("saved", 1);
					$Autoevaluationanswer->saveMany();
					
				}
			
			}
			
			$autoeval_id=$this->params->params["pass"][0];
			
			$this->Autoevaluation->setLanguage();
			$autoeval_list = $this->Autoevaluation->find('first',array(
			  'conditions'=>array(
				  'Autoevaluation.id'=>$autoeval_id
			  ),
			  'recursive'=>-1
			));
			
			$qid=$autoeval_list["Autoevaluation"]["questionnaire_id"];
			
			$this->Questionnaire->setLanguage();
			$laq = $this->Questionnaire->find('all',array(
			  'conditions'=>array(
				  'Questionnaire.id'=>$qid
			  ),
			  'recursive'=>1
			));
			
			$q=current($laq);

			$puntos=$autoeval_list["Autoevaluation"]["total_points"];
						
			// Busca el mayor puntaje posible de este cuestionario
			$puntaje_total = $this->Questionnaire->calculateTotalPoints($qid);
			
			$porcentaje=0;
			if ($puntaje_total) {
				$porcentaje=round($puntos/$puntaje_total*100, 0);
			}
			
			$las_respuestas=null;
			
			$this->Autoevaluationanswer->setLanguage();
			$las_respuestas = $this->Autoevaluationanswer->find('all',array(
			  'conditions'=>array(
				  'Autoevaluationanswer.autoevaluation_id'=>$autoeval_id
			  ),
			  'recursive'=>-1
			));
			
			$this->Answer->setLanguage();
			foreach($las_respuestas as $ilr=>$auxlr) {
				$respuesta = $this->Answer->find('first',array(
				  'conditions'=>array(
					  'Answer.id'=>$auxlr["Autoevaluationanswer"]["answer_id"]
				  ),
				  'recursive'=>-1
				));
				$las_respuestas[$ilr]["Answer"]=current($respuesta);
			}

			$this->set(compact('paso', 'q', 'puntos', 'porcentaje', 'puntaje_total', 'las_respuestas', 'autoeval_id'));
					
		}
	
    /*----------------EDIT-----------------*/

        /*----------------get_edit-----------------*/
        public function get_edit($id){

            $this->Autoevaluation->id = $id;
            if (!$this->Autoevaluation->exists()) {
                $this->_flash(__('No-exist-record',true),'alert alert-warning');
                $this->redirect(array('action' => 'admin_edit'));
            }else{

            	$this->set(
                	array(
                		"modules" => $this->Module->find("list")
                	)
                );

                $datamodel = $this->Autoevaluation->read(null, $id);
                $this->request->data = $this->readWithLocale($datamodel); // se trae la informacion al editar.

                $this->set(compact('id'));
            }

        }
        /*----------------get_edit-----------------*/

        /*----------------post_edit-----------------*/
        public function post_edit($id){

			$this->ajaxVariablesInit();
			
			$fieldslocales = array('Autoevaluation'=>array('name'));
			$validations = $this->validationLocale($fieldslocales);

			if(empty($validations)){

				$this->Autoevaluation->id = $id;
				$this->Autoevaluation->set($this->data);

				try{
					if ($this->Autoevaluation->saveMany()) {
						$this->dataajax['response']['method']=$this->getMethod();
						 $this->dataajax['response']['message_success']=__('Update Success',true);
					}
				}catch (Exception $e) {
					$this->dataajax['response']['message_error']=__('Update Error',true);
				}
			}else{
				$this->dataajax['response']["errors"]=$validations;
			}
			echo json_encode($this->dataajax);
			die();
        }
        /*----------------post_edit-----------------*/

        /*----------------edit-----------------*/
        public function admin_edit($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            $form_config = array();
            $form_config["title"] = __("Edit")." ".__("Autoevaluation");
            $form_config["urlform"] = "admin_edit";
            $form_config["labelbutton"] = __("Save");
            $form_config["type"] = 'put';
            $this->set('form_config',$form_config);

            if ($this->request->is('get')) {
                if(empty($id)){
                    $this->get_index('admin_edit');
                }else{
                    $this->get_edit($id);
                }
            }else{
                if ($this->request->is('put')) {
                    $this->post_edit($id);
                }
            }

			  $questionnaires = $this->Questionnaire->find('list',array(
				  'fields'=>array(
					  'id','title'
				  )
			  ));

			  $is_root = $this->isRoot();

			  $this->set(compact('questionnaires','is_root'));
			  
        }
        /*----------------edit-----------------*/

    /*----------------EDIT-----------------*/


    /*----------------DELETE-----------------*/

        /*----------------delete-----------------*/
        public function admin_delete($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if(!empty($id)){
                $this->Autoevaluation->id = $id;
                if (!$this->Autoevaluation->exists()) {
                    $this->_flash(__('No-exist-record', true),'alert alert-danger');
                    $this->redirect(array('action' => 'admin_index'));
                }

                try{
                    if ($this->Autoevaluation->delete($id,true)) {
                        $this->_flash(__('Delete Success', true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                        $this->_flash(__('Delete Error', true),'alert alert-warning');
                        $this->redirect(array('action' => 'admin_index'));
                }
            }else{

                $this->get_index('admin_index');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------delete-----------------*/

        /*----------------delete-----------------*/
        public function admin_deletemulti(){

            if($this->request->is('post')){
                //pr($this->data);
                $dataids =  $this->data['Autoevaluation']['id'];

                try{
                    if ($this->Autoevaluation->deleteAll(array('Autoevaluationid' => $dataids))) {
                        $this->_flash(__('Delete Success',true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                    $this->_flash(__('Delete Error', true),'alert alert-warning');
                    $this->redirect(array('action' => 'admin_index'));
                }

            }else{
                $this->_flash(__('Delete Error', true),'alert alert-danger');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------delete-----------------*/

    /*----------------DELETE-----------------*/

}
