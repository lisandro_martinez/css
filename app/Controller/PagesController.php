<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
	
	// manejador universal del portal publico
	function index() {	
	
		// cambio de idioma
		if (isset($this->params->query["lang"]) and in_array($this->params->query["lang"], $this->getlocalesValidates())) {
			$this->Session->write('Config.MyLangVar', $this->params->query["lang"]);
			$this->redirect($this->request->here);
		}
		
		$e=explode("/", $this->params["slug"]);
		
		// $e[1] = sección de la página
		// $e[2] = parámatros
		
		if ($e[0]) { // slug de la empresa
		
			$this->Company->setLanguage();
			$la_comp = $this->Company->find('first', array('conditions'=>array('slug'=>$e[0]), 'recursive'=>-1));

			if (is_array($la_comp) and sizeof($la_comp)) {
				
				if ($la_comp["Company"]["active"]) {

					// sección que desea ver
					switch($e[1]) {
					case "gallery":
						
						$this->Node->setLanguage();
						$la_galeria = $this->Node->getNodes(Node::TYPE_GALLERY, $la_comp["Company"]["id"], null, null, array("Node.active"=>1, "Node.id"=>$e[2]));
						
						foreach($la_galeria as $i=>$g) {
							$la_galeria[$i]["Imgs"]=$this->getGalleryImgs($la_comp["Company"]["id"], $g["Node"]["id"]);
						}
						
						$this->set(compact('la_comp', 'la_galeria'));
						$this->render('index_gallery');
						
						break;
						
					case "mailboxwrite":
						
						if (isset($this->data["Mailboxsuggestion"])) {
						
							$data = $this->data;
							
							$data["Mailboxsuggestion"]["comment"]=nl2br($data["Mailboxsuggestion"]["comment"]);
						
							$this->Mailboxsuggestion->create();
							$this->Mailboxsuggestion->set($data);
							$this->Mailboxsuggestion->saveMany();
							$this->redirect("/".$la_comp["Company"]["slug"]."/mailboxread/".$e[2]);
						
						} else {
						
							$this->Node->setLanguage();
							$el_buzon = $this->Node->getNodes(Node::TYPE_MAILBOX, $la_comp["Company"]["id"], null, null, array("Node.active"=>1, "Node.id"=>$e[2]));
							
							$this->set(compact('la_comp', 'el_buzon'));
							$this->render('index_mailboxwrite');
							
						}
						
						break;
						
					case "mailboxread":
						
						$this->Node->setLanguage();
						$el_buzon = $this->Node->getNodes(Node::TYPE_MAILBOX, $la_comp["Company"]["id"], null, null, array("Node.active"=>1, "Node.id"=>$e[2]));
						
						$los_mensajes = $this->Mailboxsuggestion->find('all', array('conditions'=>array('node_id', $e[2]), 'order'=>'datetime DESC'));
						
						$this->set(compact('la_comp', 'el_buzon', 'los_mensajes'));
						$this->render('index_mailboxread');
							
						break;
						
					case "survey":
						
						if (isset($this->data["Surveyanswer"])) {
						
							$data = $this->data;
							
							$data["Surveyanswer"]["data"]=serialize($data["Surveyanswer"]["data"]);
						
							$this->Surveyanswer->create();
							$this->Surveyanswer->set($data);
							$this->Surveyanswer->saveMany();

							$this->_flash(__('Thanks for your participation!', true),'alert alert-info');
							$this->redirect("/".$la_comp["Company"]["slug"]);
						
						} else {
						
							$this->Node->setLanguage();
							$la_encuesta = $this->Node->getNodes(Node::TYPE_SURVEY, $la_comp["Company"]["id"], null, null, array("Node.active"=>1, "Node.id"=>$e[2]));
							
							$las_preguntas = $this->Surveyquestion->find('all', array('conditions'=>array('node_id', $e[2]), 'order'=>'order'));
		
							$this->set(compact('la_comp', 'la_encuesta', 'las_preguntas'));
							$this->render('index_survey');
							
						}
						
						break;
						
					default: // home page
					
						$this->Node->setLanguage();
						
						// Listado de galerias
						$galerias = $this->Node->getNodes(Node::TYPE_GALLERY, $la_comp["Company"]["id"], null, null, array("Node.active"=>1));
						foreach($galerias as $i=>$g) {
							$galerias[$i]["Imgs"]=$this->getGalleryImgs($la_comp["Company"]["id"], $g["Node"]["id"]);
						}
						
						// Listado de buzones
						$mailboxes = $this->Node->getNodes(Node::TYPE_MAILBOX, $la_comp["Company"]["id"], null, null, array("Node.active"=>1));	
						foreach($mailboxes as $i=>$m) {
							$mailboxes[$i]["ultimo_mensaje"] = $this->Mailboxsuggestion->find('first', array('conditions'=>array('id', $m["Node"]["id"])));
						}
						
						// Listado de encuestas
						$surveys = $this->Node->getNodes(Node::TYPE_SURVEY, $la_comp["Company"]["id"], null, null, array("Node.active"=>1));
						
						// Listado de gráficas
						$graphs = $this->Node->getNodes(Node::TYPE_GRAPH, $la_comp["Company"]["id"], null, null, array("Node.active"=>1));
						
						if (is_array($graphs)) {
							$this->Graphdata->setLanguage();
							foreach($graphs as $i=>$g) {
								$theGraphdata = $this->Graphdata->find('all', array('conditions'=>array('Graphdata.node_id'=>$g["Node"]["id"])));
								if(isset($theGraphdata[0]["Graphdata"])) {
									$graphs[$i]["Graphdata"] = $theGraphdata[0]["Graphdata"];
								}
							}
						}
						
						$this->set(compact('la_comp', 'galerias', 'mailboxes', 'ultimo_mensaje', 'surveys', 'graphs'));
						break;
					}
				
				} else {
					$this->display();
				}				
				
			} else {
				$this->display();
			}
		
		} else {
			$this->display();
		}
		
	}

}
