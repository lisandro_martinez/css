<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Report $Report
 */

class ReportsController extends AppController {

	/*----------------beforeFilter-----------------*/
    public function beforeFilter() {
        parent::beforeFilter();
    }
    /*----------------beforeFilter-----------------*/


        public function paramFilters($urlform){

            $form_config = array();
            $form_config["title"] = __("Search / Filter");
            $form_config["urlform"] = $urlform;
            $form_config["labelbutton"] = __("Search / Filter");
            $this->set('form_config',$form_config);

            $fields_char = array('name');
			
            $conditions = $this->filterConfig('Report',$fields_char);
			
            $this->recordsforpage();

			// Busca los indicadores de gestión de la compañía del usuario
			if ($this->Auth->user('company_id')) {
				$conditions[]=array("company_id"=>$this->Auth->user('company_id'));
			} else {
				// Busca los indicadores de gestión de las compañías de la corporación del usuario
				if ($this->Auth->user('corporation_id')) {
					$conditions[]=array("company_id"=>$this->Company->find("list", array("conditions"=>array("corporation_id"=>$this->Auth->user('corporation_id')), 'fields'=>"id")));
				// Sino Busca todas...
				}
			}

            return $conditions;

        }


    /*----------------INDEX-----------------*/

        /*----------------get_index-----------------*/
        public function get_index($urlfilter = 'admin_index'){
            $conditions=$this->paramFilters($urlfilter);
            $limit = $this->Session->read('Filter.recordsforpage');

            $this->Report->setLanguage();
            $this->Paginator->settings = array(
                'order' => 'Report.id ASC',
                'conditions' => $conditions,
                'limit' => $limit,
                'recursive' => 1
            );

            $lists = $this->Paginator->paginate('Report');
			
            $this->set(compact('lists'));
        }
        /*----------------get_index-----------------*/

        /*----------------index-----------------*/
        public function admin_index(){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if ($this->request->is('get')) {
                $this->get_index();
            }
			
			// Busca los indicadores de gestión de la compañía del usuario
			if ($this->Auth->user('company_id')) {
				$companies=$this->Company->find("list", array("conditions"=>array("id"=>$this->Auth->user('company_id'))));
			} else {
				// Busca los indicadores de gestión de las compañías de la corporación del usuario
				if ($this->Auth->user('corporation_id')) {
					$companies=$this->Company->find("list", array("conditions"=>array("corporation_id"=>$this->Auth->user('corporation_id'))));
				// Sino no busca nada...
				} else {
					$companies=array();
				}
			}


            $this->set(compact('companies'));
			
			
        }
        /*----------------index-----------------*/

    /*----------------INDEX-----------------*/

    /*----------------ADD-----------------*/

        /*----------------post_add-----------------*/
        public function post_add(){

			$this->ajaxVariablesInit();

			$fieldslocales = array('Report'=>array('name'));
			$validations = $this->validationLocale($fieldslocales);

			if(empty($validations)){

				$data=self::preProcessData($this->data);
				$data["Report"]["company_id"]=$this->Auth->user('company_id');
				
				$this->Report->create();
				$this->Report->set($data);
				
				try{
					if ($this->Report->saveMany()) {
                        $this->dataajax['response']['method']=$this->getMethod();
						$this->dataajax['response']['message_success']=__('Save Success',true);						
					}
				}catch (Exception $e) {
					$this->dataajax['response']['message_error']=__('Save Error'.$e->getMessage(),true);
				}
			}else{
				$this->dataajax['response']["errors"]=$validations;
			}

			echo json_encode($this->dataajax);
			die();
			
        }
        /*----------------post_add-----------------*/

        /*----------------get_add-----------------*/
        public function get_add(){
            $treshold = array();
            $treshold['Mayor o igual que'] = __('Greater or equal than');
            $treshold['Mayor que'] = __('Greater than');
            $treshold['Menor o igual que'] = __('Lesser or equal than');
            $treshold['Menor que'] = __('Lesser than');
            $this->set(compact('treshold'));
        }
        /*----------------get_add-----------------*/

        /*----------------add-----------------*/
        public function admin_add() {
		
            $form_config = array();
            $form_config["title"] = __("Add")." ".__("Report");
            $form_config["urlform"] = "admin_add";
            $form_config["labelbutton"] = __("Add");
            $form_config["type"] = 'file';
            $this->set('form_config',$form_config);

            if ($this->request->is('post')) {
                $this->post_add();
            }elseif ($this->request->is('get')){
                $this->get_add();
            }

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

			  $is_root = $this->isRoot();

			  $summernote=true;

			  $this->set(compact('is_root','summernote'));
			  
        }
        /*----------------add-----------------*/

    /*----------------ADD-----------------*/

    /*----------------EDIT-----------------*/

        /*----------------get_edit-----------------*/
        public function get_edit($id){

            $this->Report->id = $id;

            if (!$this->Report->exists()) {
                $this->_flash(__('No-exist-record',true),'alert alert-warning');
                $this->redirect(array('action' => 'admin_edit'));
            }else{
                	$treshold = array();
					$treshold['Mayor o igual que'] = __('Greater or equal than');
					$treshold['Mayor que'] = __('Greater than');
					$treshold['Menor o igual que'] = __('Lesser or equal than');
					$treshold['Menor que'] = __('Lesser than');
					$this->set(compact('treshold'));

            	$this->set(
                	array(
                		"modules" => $this->Module->find("list")
                	)
                );

                $datamodel = $this->Report->read(null, $id);
                $this->request->data = self::preProcessData($this->readWithLocale($datamodel)); // se trae la informacion al editar.
				
                $this->set(compact('id'));
            }

        }
        /*----------------get_edit-----------------*/

        /*----------------post_edit-----------------*/
        public function post_edit($id){

			$this->ajaxVariablesInit();
			
			$fieldslocales = array('Report'=>array('name'));
			$validations = $this->validationLocale($fieldslocales);

			if(empty($validations)){

				$data=self::preProcessData($this->data);
				$data["Report"]["company_id"]=$this->Auth->user('company_id');
				
				$this->Report->id = $id;
				$this->Report->set($data);

				try{
					if ($this->Report->saveMany()) {
						$this->dataajax['response']['method']=$this->getMethod();
						$this->dataajax['response']['message_success']=__('Update Success',true);
					}
				}catch (Exception $e) {
					$this->dataajax['response']['message_error']=__('Update Error: '.$e->getMessage(),true);
				}
			}else{
				$this->dataajax['response']["errors"]=$validations;
			}
			echo json_encode($this->dataajax);
			die();

        }
        /*----------------post_edit-----------------*/

        /*----------------edit-----------------*/
        public function admin_edit($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            $form_config = array();
            $form_config["title"] = __("Edit")." ".__("Report");
            $form_config["urlform"] = "admin_edit";
            $form_config["labelbutton"] = __("Save");
            $form_config["type"] = 'put';
            $this->set('form_config',$form_config);

            if ($this->request->is('get')) {
                if(empty($id)){
                    $this->get_index('admin_edit');
                }else{
                    $this->get_edit($id);
                }
            }else{
                if ($this->request->is('put')) {
                    $this->post_edit($id);
                }
            }

			  $is_root = $this->isRoot();

			  $summernote=true;
	
			  $this->set(compact('is_root','summernote'));
			  
        }
        /*----------------edit-----------------*/

    /*----------------EDIT-----------------*/

        public function admin_generate($id=null){

			// get all pages from report $id order by order
			$this->Reportpage->setLanguage();
			$report = $this->Report->find('first', array('conditions'=>array('Report.id'=>$id)));

			if($_GET["lang"]=="eng") {
				$lang = "eng";
			} else {
				$lang = "esp";
			}
			
			// get data from report $id
			// calc pdf file name
			if ($report["Report"]["name"]) {
				$pdf_name = iconv('utf-8', 'us-ascii//TRANSLIT', $id."-".strtolower($report["Report"]["name"]));
				$pdf_name = strtolower(preg_replace('/[^A-Za-z0-9-]+/', '-', $pdf_name));
			} else {
				$pdf_name = $id."-report";
			}

			$url_html = str_replace('/reports/generate/', '/reports/generate_html/', Router::url( $this->here, true ))."?lang=".$lang;
			
			
			// produccion? o local?
			define('PRODUCCION', false);

			// save pdfs to:
			define('PATH_TO_LOCAL_PDF', dirname(dirname(__FILE__)).'/webroot/reports/');

			if (PRODUCCION) {

				 /*
				 * Ejemplo:
				 * http://css.red111.net/app/webroot/css-pdf/code/generate_pdf.php?url=http://css.red111.net/app/webroot/css-pdf/code/generate_html.php&pdf=test.pdf
				 */
			 
				// show pdfs in:
				define('URL_TO_LOCAL_PDF', 'http://css.red111.net/reports/');

				// wkhtmltopdf command location:
                define('WKHTMLTOPDF', dirname(dirname(__FILE__)).'/webroot/css-pdf/vendor/2017/wkhtmltopdf-amd64'); // new 2016/2017

				// define('WKHTMLTOPDF', dirname(dirname(__FILE__)).'/webroot/css-pdf/vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'); // Hostgator RedHat version

			} else {

				 /*
				 * Ejemplo:
				 * http://css.local/app/webroot/css-pdf/code/generate_pdf.php?url=http://css.local/app/webroot/css-pdf/code/generate_html.php&pdf=test.pdf
				 */
				 
				// show pdfs in:
				define('URL_TO_LOCAL_PDF', 'http://css.local/reports/');
				
				// wkhtmltopdf command location:
				define('WKHTMLTOPDF', 'xvfb-run wkhtmltopdf'); // ubuntu local version

			}

			$filename = $pdf_name.".pdf";

			// http://css.local/app/webroot/css-pdf/code/generate_html.php ./test.pdf  2>&1
# echo $url_html; die;
			$cmd = WKHTMLTOPDF . " --orientation Portrait " . escapeshellarg($url_html) . " ". escapeshellarg(PATH_TO_LOCAL_PDF . $filename) . " 2>&1";
			// $cmd = 'xvfb-run ' .WKHTMLTOPDF . " --orientation Portrait " . escapeshellarg($_GET["url"]) . " ". escapeshellarg(PATH_TO_LOCAL_PDF . $filename) . " 2>&1";

			$ini_time = time();

			# execute the command, and store the return value ($retval) and the last line
			exec($cmd, $retval, $last_line);

			# we expect the last line to be "Done" on success
			if($last_line == "Done") {
				$result = URL_TO_LOCAL_PDF . $filename;
				$secs = time() - $ini_time;
			}else{
				$error = "<p>command: $cmd</p><p>retval: " . var_dump($retval) . "</p><p>last_line" . var_dump($last_line) . "</p>";
			}

			$this->Report->id = $id;
			$report["Report"]["url"] = $result;
			$this->Report->set(["Report"=>$report["Report"]]);

			try{
				if ($this->Report->saveMany()) {
				}
			}catch (Exception $e) {
				$this->dataajax['response']['message_error']=__('Update Error: '.$e->getMessage(),true);
			}

			$this->redirect(array('action' => 'admin_index'));

//			$this->set(compact('result', 'secs', 'error', 'url_html'));
			
        }



        public function admin_generate_html($id=null){

			// clean layout
            $this->layout = 'clean';

			// get all pages from report $id order by order
			$this->Reportpage->setLanguage();
			$reportpages = $this->Reportpage->find('all', array('conditions'=>array('Report.id'=>$id), 'order' => 'Reportpage.order ASC'));

			if($_GET["lang"]=="esp") {
				$lang = "esp";
			} else {
				$lang = "eng";
			}
			
			// generate html
			include_once(dirname(dirname(__FILE__)).'/webroot/css-pdf/code/html_generator/html_generator.php');

			/*
			 * 01 es el diseño seleccionado tanto para el header como para el estilo general del sitio,
			 * luego viene el titulo y subtitulo del header
			 */

			$html = new htmlGenerator('01', strtoupper($reportpages[0]["Report"]["name"]), strtoupper($this->Session->read('TheCompany')["Company"]["name"]));

			/*
			 * Inicializar el header html con los estilos basicos
			 */
			 
			echo $html -> init();
			
			ob_start();

			foreach($reportpages as $page) {
				
				$tag = $page["Reportpage"]["tag"];
				
				$json = json_decode($page["Reportpage"]["json"], true);

				echo $html->renderTemplate($tag, $json[$tag], $lang);
			}

			$result_html = ob_get_contents();

			ob_end_clean();
			
			$this->set(compact('result_html'));
			
			
        }

    /*----------------DELETE-----------------*/

        /*----------------delete-----------------*/
        public function admin_delete($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if(!empty($id)){
                $this->Report->id = $id;
                if (!$this->Report->exists()) {
                    $this->_flash(__('No-exist-record', true),'alert alert-danger');
                    $this->redirectCustom($this->params);
                }

                try{
                    if ($this->Report->delete($id,true)) {
                        $this->_flash(__('Delete Success', true),'alert alert-success');
						$this->redirectCustom($this->params);
                    }
                }catch (Exception $e) {
                        $this->_flash(__('Delete Error', true),'alert alert-warning');
						$this->redirectCustom($this->params);
                }
            }else{

                $this->get_index('admin_index');
				$this->redirectCustom($this->params);
				
            }

        }
        /*----------------delete-----------------*/

        /*----------------delete-----------------*/
        public function admin_deletemulti(){

            if($this->request->is('post')){
                //pr($this->data);
                $dataids =  $this->data['Report']['id'];

                try{
                    if ($this->Indicator->deleteAll(array('Reportid' => $dataids))) {
                        $this->_flash(__('Delete Success',true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                    $this->_flash(__('Delete Error', true),'alert alert-warning');
                    $this->redirect(array('action' => 'admin_index'));
                }

            }else{
                $this->_flash(__('Delete Error', true),'alert alert-danger');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------delete-----------------*/

    /*----------------DELETE-----------------*/

	
	private function preProcessData($data) {

		return $data;
	
	}

	public function admin_values() {
		
		$id=$this->params->params["pass"][0];
		
		$report=array();
		if ($id) {
			$this->Report->setLanguage();
			$report = $this->Report->find('all', array('conditions'=>array('Report.id'=>$id)));
		}
		$this->set(compact('report'));
		
	}

    public function admin_detail() {
        $id = $this->params->params['pass'][0];
        $this->Report->setLanguage();
        $data = $this->Report->findById($id);
        $this->set('name', $data['Indicator']['name']);
        
    }

    public function admin_show() {
        $this->layout = "ajax";
        $data = $this->Report->findById($this->data['id']);
        $this->set('data', $data);
    }

}
