<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Reportpage $Reportpage
 */

class ReportpagesController extends AppController {

	/*----------------beforeFilter-----------------*/
    public function beforeFilter() {
        parent::beforeFilter();
    }
    /*----------------beforeFilter-----------------*/


        public function paramFilters($urlform){

            $form_config = array();
            $form_config["title"] = __("Search / Filter");
            $form_config["urlform"] = $urlform;
            $form_config["labelbutton"] = __("Search / Filter");
            $this->set('form_config',$form_config);

            $fields_char = array(
                        'value',
            );
			
            $conditions = $this->filterConfig('Reportpage',$fields_char);

            $this->recordsforpage();

			// Busca los indicadores de gestión de la compañía del usuario
//			$conditions[] = $this->Report->conditionsListByCompany($this->Auth->user('company_id'));
			
			return $conditions;

        }


    /*----------------INDEX-----------------*/

        /*----------------get_index-----------------*/
        public function get_index($urlfilter = 'admin_index'){
			
            $conditions=$this->paramFilters($urlfilter);
            $limit = $this->Session->read('Filter.recordsforpage');

			$conditions["Report.company_id"]=$this->Auth->user('company_id');

            $this->Reportpage->setLanguage();
            $this->Paginator->settings = array(
                'order' => 'Reportpage.order ASC',
                'conditions' => $conditions,
                'limit' => $limit,
                'recursive' => 1
            );

            $lists = $this->Paginator->paginate('Reportpage');
			
            $this->set(compact('lists'));
        }
        /*----------------get_index-----------------*/

        /*----------------index-----------------*/
        public function admin_index(){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if ($this->request->is('get')) {
                $this->get_index();
            }
            
			$reports=$this->Reportpage->reportsByCompany($this->Auth->user('company_id'));
			
            $this->set(compact('reports'));
			
			
        }
        /*----------------index-----------------*/

    /*----------------INDEX-----------------*/

    /*----------------ADD-----------------*/

        /*----------------post_add-----------------*/
        public function post_add(){

			$this->ajaxVariablesInit();

			$fieldslocales = array('Reportpage'=>array('name'));
			$validations = $this->validationLocale($fieldslocales);

			if(empty($validations)){

				$data=self::preProcessData($this->data);
				
				$this->Reportpage->create();
				$this->Reportpage->set($data);

				try{
					if ($this->Reportpage->saveMany()) {
						$this->dataajax['response']['method']=$this->getMethod();
						$this->dataajax['response']['message_success']=__('Save Success: '.
							'<script>document.location.href="/admin/Reportpages/edit/'.$this->Reportpage->getInsertID().'";</script>'
						,true);
					}
				}catch (Exception $e) {
					$this->dataajax['response']['message_error']=__('Save Error',true);
				}

			}else{
				$this->dataajax['response']["errors"]=$validations;
			}
			
			echo json_encode($this->dataajax);
			die();
			
        }
        /*----------------post_add-----------------*/

        /*----------------get_add-----------------*/
        public function get_add(){

			$templates = $this->Reportpage->templates();
			$this->set(compact('templates'));

        }
        /*----------------get_add-----------------*/

        /*----------------add-----------------*/
        public function admin_add() {
		
            $form_config = array();
            $form_config["title"] = __("Add")." ".__("page");
            $form_config["urlform"] = "admin_add";
            $form_config["labelbutton"] = __("Add");
            $form_config["type"] = 'file';
            $this->set('form_config',$form_config);

            if ($this->request->is('post')) {
                $this->post_add();
            }elseif ($this->request->is('get')){
                $this->get_add();
            }

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

			$is_root = $this->isRoot();

			// Busca los indicadores de gestión de la compañía del usuario
			$reports = $this->Reportpage->reportsByCompany($this->Auth->user('company_id'));

			$summernote=true;

			$this->set(compact('is_root', 'reports', 'summernote'));

        }
        /*----------------add-----------------*/

    /*----------------ADD-----------------*/

    /*----------------EDIT-----------------*/

        /*----------------get_edit-----------------*/
        public function get_edit($id){

            $this->Reportpage->id = $id;
            if (!$this->Reportpage->exists()) {
                $this->_flash(__('No-exist-record',true),'alert alert-warning');
                $this->redirect(array('action' => 'admin_edit'));
            }else{

            	$this->set(
                	array(
                		"modules" => $this->Module->find("list")
                	)
                );

                $datamodel = $this->Reportpage->read(null, $id);
                
                $this->request->data = self::preProcessData($this->readWithLocale($datamodel)); // se trae la informacion al editar.

				$default_values = [];

				if ($json = $this->request->data["Reportpage"]["json"]) {
					$arr_data = json_decode($json, true);

					foreach($arr_data as $template_tag=>$template_data) {

						if (is_array($template_data)) {
							foreach($template_data as $field_name=>$languages) {
								if (is_array($languages)) {
									foreach($languages as $lang_tag=>$lang_value) {
										if ($lang_tag and $lang_value) {
											$default_values[ 'editor__text__'.$template_tag.'__'.$field_name.'__'.$lang_tag ] = $lang_value;
										}
									}
								}
							}
						}
					}

				}
	
				$templates = $this->Reportpage->templates();
	
                $this->set(compact('id', 'templates', 'default_values'));
                
            }

        }
        /*----------------get_edit-----------------*/

        /*----------------post_edit-----------------*/
        public function post_edit($id){

			$this->ajaxVariablesInit();
			
			$fieldslocales = array('Reportpage'=>array('name'));
			$validations = $this->validationLocale($fieldslocales);

			if(empty($validations)){

				$data=self::preProcessData($this->data);
				
				/*
				 * find editor fields and add them to a json with the following format:
					[Reportpage] => Array
							(
								[id] => 14
								[report_id] => 9
								[order] => 1
								[tag] => portada01
								[json] =>
								* 		{
								* 			"portada01": {
								* 				"titulo": { 
								* 					"esp":"1<\/p>",
								* 					"eng":"2<\/p>"
								* 				},
								* 				"subtitulo": {
								* 					"esp":"3<\/p>",
								* 					"eng":"4<\/b><\/p>"
								* 				},
								* 				"titulo_pie": {
								* 					"esp":"5<\/b><\/span><\/p>",
								* 					"eng":"6<\/b><\/span><\/p>"
								*				}
								* 			}
								* 		}
							)

					)
				 */
				
				$arr_data = [];
				
				foreach($data as $field=>$value) {
					if (strpos($field, "editor__")!==false) {
						$arr_fields = explode("__", $field);
						$arr_data[$arr_fields[2]][$arr_fields[3]][$arr_fields[4]] = $value;
					}
				}
				
				$data["Reportpage"]["json"] = json_encode($arr_data);
				
				$this->Reportpage->id = $id;
				$this->Reportpage->set($data);

				try{
					if ($this->Reportpage->save()) {
						$this->dataajax['response']['method']=$this->getMethod();
						$this->dataajax['response']['message_success']=__('Update Success',true);
					}
				}catch (Exception $e) {
					$this->dataajax['response']['message_error']=__('Update Error: '.$e->getMessage(),true);
				}
				
			}else{
				$this->dataajax['response']["errors"]=$validations;
			}
			
			echo json_encode($this->dataajax);
			die();

        }
        /*----------------post_edit-----------------*/

        /*----------------edit-----------------*/
        public function admin_edit($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            $form_config = array();
            $form_config["title"] = __("Edit")." ".__("Page");
            $form_config["urlform"] = "admin_edit";
            $form_config["labelbutton"] = __("Save");
            $form_config["type"] = 'put';
            $this->set('form_config',$form_config);

            if ($this->request->is('get')) {
                if(empty($id)){
                    $this->get_index('admin_edit');
                }else{
                    $this->get_edit($id);
                }
            }else{
                if ($this->request->is('put')) {
                    $this->post_edit($id);
                }
            }

			  $is_root = $this->isRoot();

			// Busca los indicadores de gestión de la compañía del usuario

			$this->Reportpage->setLanguage();			
			$reports = $this->Reportpage->reportsByCompany($this->Auth->user('company_id'), $this->Auth->user('corporation_id'));

			$this->set(compact('is_root','reports'));
			  
        }
        /*----------------edit-----------------*/

    /*----------------EDIT-----------------*/


    /*----------------DELETE-----------------*/

        /*----------------delete-----------------*/
        public function admin_delete($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if(!empty($id)){
                $this->Reportpage->id = $id;
                if (!$this->Reportpage->exists()) {
                    $this->_flash(__('No-exist-record', true),'alert alert-danger');
                    $this->redirectCustom($this->params);
                }

                try{
                    if ($this->Reportpage->delete($id,true)) {
                        $this->_flash(__('Delete Success', true),'alert alert-success');
						$this->redirectCustom($this->params);
                    }
                }catch (Exception $e) {
                        $this->_flash(__('Delete Error', true),'alert alert-warning');
						$this->redirectCustom($this->params);
                }
            }else{

                $this->get_index('admin_index');
				$this->redirectCustom($this->params);
				
            }

        }
        /*----------------delete-----------------*/

        /*----------------delete-----------------*/
        public function admin_deletemulti(){

            if($this->request->is('post')){
                //pr($this->data);
                $dataids =  $this->data['Reportpage']['id'];

                try{
                    if ($this->Reportpage->deleteAll(array('Reportpageid' => $dataids))) {
                        $this->_flash(__('Delete Success',true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                    $this->_flash(__('Delete Error', true),'alert alert-warning');
                    $this->redirect(array('action' => 'admin_index'));
                }

            }else{
                $this->_flash(__('Delete Error', true),'alert alert-danger');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------delete-----------------*/

    /*----------------DELETE-----------------*/

	private function preProcessData($data) {
		return $data;
	}

	public function admin_editor() {
		
		$this->layout = 'clean';
		$json = json_decode($this->data["json"]);
		$template = $this->data["template"];
		
		if (!$this->Session->check('Config.LangVar')) {
			$esp=true;
			$eng=true;
		} else {
			if ($this->Session->read('Config.LangVar') == 'esp') {
				$esp=true;
			} else {
				$eng=true;
			}
		}

		$this->set(compact('json', 'template', 'eng', 'esp'));

	}

	public function admin_cssimg() {
		
		$this->layout = 'ajax';

		$graphs = $this->Node->getNodes(Node::TYPE_GRAPH, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);

		$this->Graphdata->setLanguage();
		foreach($graphs as $i=>$g) {
			$theGraphdata = $this->Graphdata->find('all', array('conditions'=>array('Graphdata.node_id'=>$g["Node"]["id"])));
			if(isset($theGraphdata[0]["Graphdata"])) {
				$graphs[$i]["Graphdata"] = $theGraphdata[0]["Graphdata"];
			}
		}

		$lang = $this->Session->read('Config.MyLangVar');
		$urlwebroot = dirname(dirname(dirname(Router::url( $this->here, true ))));
		$this->set(compact('graphs', 'lang', 'urlwebroot'));
		
	}

	public function admin_cssdata() {
		
		$this->layout = 'ajax';

		// get nodes

		$this->Nodetype->setLanguage();
		$nodetypes = $this->Nodetype->find('all',array(
			'order' => array('Nodetype.name'),
			'recursive'=>-1
		));

		foreach($nodetypes as $nt) {
			$all_nodes = $this->Node->getNodes($nt["Nodetype"]["id"], $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
			foreach($all_nodes as $an) {
				$nodes[$nt["Nodetype"]["id"]][$an["Node"]["id"]] = $an;
			}
		}

		// get indicators

		$instruments["SA"]=__("Self Assessments");
		$instruments["IG"]=__("Management Indicators");

		$this->Indicator->setLanguage();
		$indicators = $this->Indicator->find('all',array(
			'order' => array('Indicator.name'),
			'recursive'=>-1
		));

		foreach($indicators as $i=>$ind) {
			$summary = $this->Indicator->getSummaryIndicator2($ind["Indicator"]["id"]);
			$indicators[$i]["Summary"] = current($summary);
		}

		// get self assesment

		$this->Autoevaluation->setLanguage();
		$this->Questionnaire->setLanguage();
		$conditions=array();
		if ($this->Auth->user()["corporation_id"]) {
			$conditions['Autoevaluation.corporation_id']=$this->Auth->user()["corporation_id"];
		}
		if ($this->Auth->user()["company_id"]) {
			$conditions['Autoevaluation.company_id']=$this->Auth->user()["company_id"];
		}			
		$auto = $this->Autoevaluation->find('all',array(
			'conditions'=>$conditions,
			'order' => array('Corporation.name', 'Company.name', "Autoevaluation.date_time"),
			'recursive'=>1
		));

		if (isset($auto) and is_array($auto)) {

			foreach($auto as $i=>$a) {
		
				$questionnaire = $this->Questionnaire->find('first', 
														array(	'conditions'=>array('Questionnaire.id'=>$a["Autoevaluation"]["questionnaire_id"]), 
																'recursive'=>-1)
				);
				
				$auto[$i]["Questionnaire"]=$questionnaire["Questionnaire"];
				
				$auto[$i]["QuestionnaireTotalPoints"]=$this->Questionnaire->calculateTotalPoints($a["Autoevaluation"]["questionnaire_id"]);
				
			}
		
		}

		// render window

		$node = $this->params->data["node"];
		$nodetype = $this->params->data["nodetype"];
		$ig = $this->params->data["ig"];
		$sa = $this->params->data["sa"];

		$lang = $this->Session->read('Config.MyLangVar');
		$urlwebroot = dirname(dirname(dirname(Router::url( $this->here, true ))));
		
		$graphdata = $this->Graphdata;

		$company_id = $this->Auth->user()["company_id"];

		if ($nodetype==18) {
			$imgs = $this->getGalleryImgs($company_id, $node);
		} else {
			$imgs = null;
		}

		// encuestas
		if ($nodetype==19) {

			$this->Surveyquestion->setLanguage();
			$questions = $this->Surveyquestion->find('all', array("conditions"=>array('node_id'=>$node), 'order'=>'order'));
		
			$this->Surveyanswer->setLanguage();
			$answers = $this->Surveyanswer->find('all', array("conditions"=>array('node_id'=>$node), 'order'=>'datetime DESC'));
			
			$totaliza = $this->Surveyanswer->totalize($questions, $answers);
			
		} else {
			$totaliza=null;
		}

		// buzon de sugerencias
		if ($nodetype==Node::TYPE_MAILBOX) {
			$this->Mailboxsuggestion->setLanguage();
			$msgs = $this->Mailboxsuggestion->find('all', array("conditions"=>array('node_id'=>$node), 'order'=>'datetime DESC', 'limit'=>100));
		}

		$this->set(compact('nodetypes', 'nodes', 'lang', 'urlwebroot', 'node', 'nodetype', 'instruments', 'indicators', 'ig', 'sa', 'graphdata', 'auto', 'company_id', 'imgs', 'totaliza', 'msgs'));
		
	}

}
