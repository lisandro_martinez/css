<?php
App::uses('AppController', 'Controller');

class GroupsController extends AppController {

    /*----------------beforeFilter-----------------*/
    public function beforeFilter() {
        parent::beforeFilter();
    }
    /*----------------beforeFilter-----------------*/


    public function paramFilters($urlform){

            $form_config = array();
            $form_config["title"] = __("Search / Filter");
            $form_config["urlform"] = $urlform;
            $form_config["labelbutton"] = __("Search / Filter");
            $this->set('form_config',$form_config);

            $fields_char = array(
                        'name'
            );


            $conditions = $this->filterConfig('Group',$fields_char);
            $this->recordsforpage();

            return $conditions;

        }


    /*----------------INDEX-----------------*/

        /*----------------get_index-----------------*/
        public function get_index($urlfilter = 'admin_index'){
            $conditions = $this->paramFilters($urlfilter);

            if(!$this->isRoot()){
                $conditions['Group.id <>'] = 1;
            }

            //pr($conditions);
            $limit = $this->Session->read('Filter.recordsforpage');

            $this->Paginator->settings = array(
                'order' => 'Group.id ASC',
                'conditions' => $conditions,
                'limit' => $limit
            );
            $lists = $this->Paginator->paginate('Group');
			
            $this->set(compact('lists'));
        }
        /*----------------get_index-----------------*/

        /*----------------index-----------------*/
        public function admin_index(){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if ($this->request->is('get')) {
                $this->get_index();
            }
        }
        /*----------------index-----------------*/

    /*----------------INDEX-----------------*/


    /*----------------DELETE-----------------*/

        /*----------------delete-----------------*/
        public function admin_delete($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if(!empty($id)){
                $this->Group->id = $id;
                if (!$this->Group->exists()) {
                    $this->_flash(__('No-exist-record', true),'alert alert-danger');
                    $this->redirect(array('action' => 'admin_index'));
                }

                if(!$this->isRoot()){
                    if($id == 1){
                        $this->_flash(__('No-delete-root', true),'alert alert-danger');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }

                try{
                    if ($this->Group->delete($id,true)) {
                        $this->_flash(__('Delete Success', true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                        $this->_flash(__('Delete Error', true),'alert alert-warning');
                        $this->redirect(array('action' => 'admin_index'));
                }

            }else{
                $this->get_index('admin_index');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------delete-----------------*/

    /*----------------DELETE-----------------*/


    /*----------------EDIT-----------------*/

        /*----------------get_edit-----------------*/
        public function get_edit($id){

            $this->Group->id = $id;

            if(!$this->isRoot()){
                if($id == 1){
                    $this->_flash(__('No-edit-root', true),'alert alert-danger');
                    $this->redirect(array('action' => 'admin_edit'));
                }
            }

            if (!$this->Group->exists()) {
                $this->_flash(__('No-exist-record',true),'alert alert-warning');
                $this->redirect(array('action' => 'admin_edit'));
            }else{
                $datamodel = $this->Group->read(null, $id);
                $this->request->data = $this->readWithLocale($datamodel);

                $this->set(compact('id'));
            }

        }
        /*----------------get_edit-----------------*/

        /*----------------post_edit-----------------*/
        public function post_edit($id){

                $this->ajaxVariablesInit();

                $fieldslocales = array('Group'=>array('name'));
                $validations = $this->validationLocale($fieldslocales);

                //pr($validations);

                $checkeditroot=false;
                if(!$this->isRoot()){
                    if($id == 1){
                        $this->_flash(__('No-edit-root', true),'alert alert-danger');
                        $this->dataajax['response']['redirect']='/admin/groups/edit/';
                        $checkeditroot=true;
                    }
                }

                if(!$checkeditroot){
                    if(empty($validations)){
                        $this->Group->id = $id;
                        $this->Group->set($this->data);

                        try{
                            if ($this->Group->saveMany()) {
                                $this->dataajax['response']['method']=$this->getMethod();
                             $this->dataajax['response']['message_success']=__('Update Success',true);
                            }
                        }catch (Exception $e) {
                            $this->dataajax['response']['message_error']=__('Update Error',true);
                        }
                    }
                    else{
                         $this->dataajax['response']["errors"]=$validations;
                    }
                }

                echo json_encode($this->dataajax);
                die();
        }
        /*----------------post_edit-----------------*/

        /*----------------edit-----------------*/
        public function admin_edit($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            $form_config = array();
            $form_config["title"] = "Editar Grupo";
            $form_config["urlform"] = "admin_edit";
            $form_config["labelbutton"] = "Guardar";
            $form_config["type"] = 'put';
            $this->set('form_config',$form_config);



            if ($this->request->is('get')) {
                if(empty($id)){
                    $this->get_index('admin_edit');
                }else{
                    $this->get_edit($id);
                }
            }else{
                if ($this->request->is('put')) {
                    $this->post_edit($id);
                }
            }

        }
        /*----------------edit-----------------*/

    /*----------------EDIT-----------------*/

    /*----------------ADD-----------------*/

        /*----------------post_add-----------------*/
        public function post_add(){

                $this->ajaxVariablesInit();

                $fieldslocales = array('Group'=>array('name'));
                $validations = $this->validationLocale($fieldslocales);

                if(empty($validations)){
                    $this->Group->create();
                    $this->Group->set($this->data);
                    try{
                        if($this->Group->saveMany()) {
                            $this->dataajax['response']['method']=$this->getMethod();
                            $this->dataajax['response']['message_success']=__('Save Success',true);
                        }
                    }catch (Exception $e) {
                        $this->dataajax['response']['message_error']=__('Save Error',true);
                    }


                }else{
                    $this->dataajax['response']["errors"]=$validations;
                }


                echo json_encode($this->dataajax);
                die();
        }
        /*----------------post_add-----------------*/
        /*----------------get_add-----------------*/
        public function get_add(){
                $this->set(
                    array(
                        "modules" => $this->Group->find("list")
                    )
                );
        }
        /*----------------get_add-----------------*/

        /*----------------add-----------------*/
        public function admin_add() {
            $form_config = array();
            $form_config["title"] = "Agregar Grupo";
            $form_config["urlform"] = "admin_add";
            $form_config["labelbutton"] = "Guardar";
            $form_config["type"] = 'post';
            $this->set('form_config',$form_config);

            if ($this->request->is('post')) {
                $this->post_add();
            }

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }
        }
        /*----------------add-----------------*/

    /*----------------ADD-----------------*/


 /*----------------delete-----------------*/
        public function admin_deletemulti(){

            if($this->request->is('post')){
                //pr($this->data);
                $dataids =  $this->data['Group']['id'];

                if(!$this->isRoot()){
                    if(in_array(1,$dataids)){
                        $this->_flash(__('Delete-root-multi', true),'alert alert-warning');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }

                try{
                    if ($this->Group->deleteAll(array('Group.id' => $dataids))) {
                        $this->_flash(__('Delete Success',true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                    $this->_flash(__('Delete Error', true),'alert alert-warning');
                    $this->redirect(array('action' => 'admin_index'));
                }

            }else{
                $this->_flash(__('Delete Error Multi Request', true),'alert alert-danger');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------delete-----------------*/

}
