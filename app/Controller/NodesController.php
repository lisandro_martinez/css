<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Node $Node
 */

class NodesController extends AppController {

	/*----------------beforeFilter-----------------*/
    public function beforeFilter() {
        parent::beforeFilter();
    }
    /*----------------beforeFilter-----------------*/


        public function paramFilters($urlform){

            $form_config = array();
            $form_config["title"] = __("Search / Filter");
            $form_config["urlform"] = $urlform;
            $form_config["labelbutton"] = __("Search / Filter");
            $this->set('form_config',$form_config);

            $fields_char = array(
                        'name',
            );

            $conditions = $this->filterConfig('Node',$fields_char);
            $this->recordsforpage();

            return $conditions;

        }


    /*----------------INDEX-----------------*/

        /*----------------get_index-----------------*/
        public function get_index($urlfilter = 'admin_index'){
            $conditions=$this->paramFilters($urlfilter);
            $limit = $this->Session->read('Filter.recordsforpage');

            $this->Node->setLanguage();
            $this->Paginator->settings = array(
                'order' => 'Node.id ASC',
                'conditions' => $conditions,
                'limit' => $limit,
                'recursive' => 1
            );

            $lists = $this->Paginator->paginate('Node');
			
			$this->Nodetype->setLanguage();
			$nodetypes = $this->Nodetype->find('list',array(
				  'fields'=>array(
					  'id','name'
				  )
			));
			
            $this->set(compact('lists', 'nodetypes' ));
        }
        /*----------------get_index-----------------*/

        /*----------------index-----------------*/
        public function admin_index(){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if ($this->request->is('get')) {
                $this->get_index();
            }
        }
        /*----------------index-----------------*/

    /*----------------INDEX-----------------*/

    /*----------------ADD-----------------*/

        /*----------------post_add-----------------*/
        public function post_add(){

			$this->ajaxVariablesInit();

			$fieldslocales = array('Node'=>array('name'));
			$validations = $this->validationLocale($fieldslocales);

			if(empty($validations)){

				// modifica la fecha y almacena los codigos de corporacion y empresa
				$data2 = self::preProcessData($this->data);
				
				// los agregados por acá son siempre manuales
				$data["Node"]["origin"]=3;				

				$this->Node->create();
				if (strlen($data2['Node']['power']) == 0) {
					$data2['Node']['power'] = 1;
				}

				if (strlen($data2['Node']['impact']) == 0) {
					$data2['Node']['impact'] = 1;
				}
				
				$this->Node->set($data2);

				try{		
					if (isset($data2['Node']['indicator_id'])) {
						Debugger::log($data2['inidate']);
						$ind = $this->Indicator->findById($data2['Node']['indicator_id']);
						if (strlen($data2['Node']['inidate']) > 0 && strlen($data2['Node']['enddate']) > 0 
						&& $data2['Node']['inidate'] <= $ind['Indicator']['inidate'] 
						&& $data2['Node']['inidate'] <= $ind['Indicator']['enddate'] 
						&& $data2['Node']['enddate'] >= $ind['Indicator']['inidate'] 
						&& $data2['Node']['enddate'] >= $ind['Indicator']['enddate']) {
							if ($this->Node->saveMany()) {					
								// Si lo guardó entonces agrego la relación de empresa/corporación
								$this->AddIndicatorNode($data2['Node']['indicator_id'], $this->Node->id);
								$this->Node->setCompany($this->Auth->user('company_id'));					
								$this->dataajax['response']['method']=$this->getMethod();
								$this->dataajax['response']['message_success']=__('Save Success',true);
							}	
						} else {
							$this->dataajax['response']['message_error']=
							__('Save Error, Indicator date must be between Objective range',true);		
						}
					}
					else {
						if ($this->Node->saveMany()) {					
							// Si lo guardó entonces agrego la relación de empresa/corporación
							$this->Node->setCompany($this->Auth->user('company_id'));					
							$this->dataajax['response']['method']=$this->getMethod();
							$this->dataajax['response']['message_success']=__('Save Success',true);
						}
					}
					
						
					 
				}catch (Exception $e) {
					
					$this->dataajax['response']['message_error']=__('Save Error: ' . $e->getMessage(),true);
				}
			}else{
				$this->dataajax['response']["errors"]=$validations;
			}

			echo json_encode($this->dataajax);
			die();
			
        }
        /*----------------post_add-----------------*/

        /*----------------get_add-----------------*/
        public function get_add(){

			// Parámetros de agregado: [0]: nodetype_id
			if (isset($this->params->params["pass"][0]))
				$nodetype_id = $this->params->params["pass"][0];

			if ($nodetype_id == 11) {
				$tipos = array();
				$tipos['Actividades'] = __('Activities');
				$tipos['Proyectos'] = __('Projects');
				$tipos['Iniciativas'] = __('Iniciatives');
				$tipos['Cursos'] = __('Courses');
				$this->set(compact('tipos'));
			}

			if ($nodetype_id == 7) {
				$indicators = $this->Indicator->getIndicators($this->Auth->user()["company_id"], true);
				$this->set(compact('indicators'));
			}

			$this->set(compact('nodetype_id'));
			


        }
        /*----------------get_add-----------------*/

        /*----------------add-----------------*/
        public function admin_add() {

            $form_config = array();
            $form_config["title"] = __("Add")." ".__("Node");
            $form_config["urlform"] = "admin_add";
            $form_config["labelbutton"] = __("Add");
            $form_config["type"] = 'file';
            $this->set('form_config',$form_config);

            if ($this->request->is('post')) {
                $this->post_add();
            }elseif ($this->request->is('get')){
                $this->get_add();
            }

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

			  $this->Nodetype->setLanguage();
			  $nodetypes = $this->Nodetype->find('list',array(
					  'fields'=>array(
						  'id','name'
					  ),
					  'conditions'=>array("Nodetype.type"=>User::entities()),
					  'order'=>array('type', 'name')
			  ));

				$nodepriorities = $this->Nodepriority->setLanguage();
				$nodepriorities = $this->Nodepriority->find("list", array('order' => array('id' => 'ASC')));

			  $this->Nodestatus->setLanguage();
			  $nodestatuses = $this->Nodestatus->find('list',array(
					  'fields'=>array(
						  'id','name'
					  )
			  ));

			  $is_root = $this->isRoot();

			  $summernote=true;

			  $this->set(compact('nodetypes','nodestatuses', 'nodepriorities','is_root','summernote'));
			  
        }
        /*----------------add-----------------*/

    /*----------------ADD-----------------*/

    /*----------------EDIT-----------------*/

        /*----------------get_edit-----------------*/
        public function get_edit($id){

            $this->Node->id = $id;
            if (!$this->Node->exists()) {
                $this->_flash(__('No-exist-record',true),'alert alert-warning');
                $this->redirect(array('action' => 'admin_edit'));
            }else{

            	$this->set(
                	array(
                		"modules" => $this->Module->find("list")
                	)
                );

                $datamodel = $this->Node->read(null, $id);

				if ($datamodel['Node']['nodetype_id'] == 11) {
					$tipos = array();
					$tipos['Actividades'] = __('Activities');
					$tipos['Proyectos'] = __('Projects');
					$tipos['Iniciativas'] = __('Iniciatives');
					$tipos['Cursos'] = __('Courses');
					$this->set(compact('tipos'));
				}

				if ($datamodel['Node']['nodetype_id'] == 7) {
					$indicators = $this->Indicator->getIndicators($this->Auth->user()["company_id"], true);
					$this->set(compact('indicators'));
				}

                $this->request->data = self::preProcessData($this->readWithLocale($datamodel)); // se trae la informacion al editar.
				
                $this->set(compact('id'));
            }

        }
        /*----------------get_edit-----------------*/

        /*----------------post_edit-----------------*/
        public function post_edit($id){

                $this->ajaxVariablesInit();
				
				$fieldslocales = array('Node'=>array('name'));
				$validations = $this->validationLocale($fieldslocales);

				if(empty($validations)){

					// modifica la fecha y almacena los codigos de corporacion y empresa
					$data2 = self::preProcessData($this->data);
				
                    $this->Node->id = $id;
                    $this->Node->set($data2);

                    
					try{		
						if (isset($data2['Node']['indicator_id'])) {
							$ind = $this->Indicator->findById($data2['Node']['indicator_id']);
							if (strlen($data2['Node']['inidate']) > 0 && strlen($data2['Node']['enddate']) > 0 
							&& $data2['Node']['inidate'] <= $ind['Indicator']['inidate'] 
							&& $data2['Node']['inidate'] <= $ind['Indicator']['enddate'] 
							&& $data2['Node']['enddate'] >= $ind['Indicator']['inidate'] 
							&& $data2['Node']['enddate'] >= $ind['Indicator']['enddate']) {
								if ($this->Node->saveMany()) {					
									$this->AddIndicatorNode($data2['Node']['indicator_id'], $this->Node->id);
									// Si lo guardó entonces agrego la relación de empresa/corporación
									$this->Node->setCompany($this->Auth->user('company_id'));					
									$this->dataajax['response']['method']=$this->getMethod();
									$this->dataajax['response']['message_success']=__('Save Success',true);
								}	
							} else {
								$this->dataajax['response']['message_error']=
								__('Save Error, Indicator date must be between Objective range',true);		
							}
						} else {
							if ($this->Node->saveMany()) {					
								// Si lo guardó entonces agrego la relación de empresa/corporación
								$this->Node->setCompany($this->Auth->user('company_id'));					
								$this->dataajax['response']['method']=$this->getMethod();
								$this->dataajax['response']['message_success']=__('Save Success',true);
							}
						}                      
                    }catch (Exception $e) {
                        $this->dataajax['response']['message_error']=__('Update Error',true);
                    }
                }else{
					$this->dataajax['response']["errors"]=$validations;
                }
                echo json_encode($this->dataajax);
                die();
        }
        /*----------------post_edit-----------------*/

        /*----------------edit-----------------*/
        public function admin_edit($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            $form_config = array();
            $form_config["title"] = __("Edit")." ".__("Node");
            $form_config["urlform"] = "admin_edit";
            $form_config["labelbutton"] = __("Save");
            $form_config["type"] = 'put';
            $this->set('form_config',$form_config);

            if ($this->request->is('get')) {
                if(empty($id)){
                    $this->get_index('admin_edit');
                }else{
                    $this->get_edit($id);
                }
            }else{
                if ($this->request->is('put')) {
                    $this->post_edit($id);
                }
            }

			  $this->Nodestatus->setLanguage();
			  $nodestatuses = $this->Nodestatus->find('list',array(
				  'fields'=>array(
					  'id','name'
				  )
			  ));


				// bug fix i18n
				$nodetypes=array();
				App::uses('ConnectionManager', 'Model'); 
				$db = ConnectionManager::getDataSource('default');
				if ($db->isConnected()) { 
					$query = "SELECT `Nodetype`.`id`, (`I18n__name`.`content`) AS `Nodetype__i18n_name` FROM `nodetypes` AS `Nodetype` INNER JOIN `i18n` AS `I18n__name` ON (`Nodetype`.`id` = `I18n__name`.`foreign_key` AND `I18n__name`.`model` = 'Nodetype' AND `I18n__name`.`field` = 'name' AND `I18n__name`.`locale` = '".$this->Session->read('Config.MyLangVar')."') WHERE 1 = 1 AND Nodetype.type IN (".implode(",", User::entities()).") ORDER BY Nodetype.type, Nodetype.name";
					foreach($db->query($query) as $r) {
						$nodetypes[$r["Nodetype"]["id"]]=$r[0]["Nodetype__i18n_name"];
					}
				}

	
			  $is_root = $this->isRoot();

			  $summernote=true;
			  
				$nodepriorities = $this->Nodepriority->setLanguage();
				$nodepriorities = $this->Nodepriority->find("list", array('order' => array('id' => 'ASC')));
			  
			  $this->set(compact('nodetypes','nodestatuses','is_root','summernote', 'nodepriorities'));
			  
        }
        /*----------------edit-----------------*/

    /*----------------EDIT-----------------*/


    /*----------------DELETE-----------------*/

        /*----------------delete-----------------*/
        public function admin_delete($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if(!empty($id)){
                $this->Node->id = $id;
                if (!$this->Node->exists()) {
                    $this->_flash(__('No-exist-record', true),'alert alert-danger');
                    $this->redirectCustom($this->params);
                }

                try{
                    if ($this->Node->delete($id,true)) {
                        $this->_flash(__('Delete Success', true),'alert alert-success');
						$this->redirectCustom($this->params);
                    }
                }catch (Exception $e) {
                        $this->_flash(__('Delete Error', true),'alert alert-warning');
						$this->redirectCustom($this->params);
                }
            }else{

                $this->get_index('admin_index');
				$this->redirectCustom($this->params);
				
            }

        }
        /*----------------delete-----------------*/
		public function admin_deleteindicator($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

			try{
				if ($this->Indicatornode->deletecustom($this->params->params["pass"][0], $this->params->params["pass"][1])) {
					$this->_flash(__('Delete Success', true),'alert alert-success');
				}
				$this->redirectCustom(null, $this->params->params["pass"][2]);
			}catch (Exception $e) {
					$this->_flash(__('Delete Error', true),'alert alert-warning');
					$this->params->params["pass"][1]=$this->params->params["pass"][2];
					$this->redirectCustom(null, $this->params->params["pass"][2]);
			}

        }
		

        public function admin_deleterelation($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

			try{
				if ($this->Nodenode->deletecustom($this->params->params["pass"][0], $this->params->params["pass"][1])) {
					$this->_flash(__('Delete Success', true),'alert alert-success');
				}
				$this->redirectCustom(null, $this->params->params["pass"][2]);
			}catch (Exception $e) {
					$this->_flash(__('Delete Error', true),'alert alert-warning');
					$this->params->params["pass"][1]=$this->params->params["pass"][2];
					$this->redirectCustom(null, $this->params->params["pass"][2]);
			}

        }
		
        public function admin_deletepredecessor($id=null) {

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

			try{
				if ($this->Nodepredecessor->deletecustom($this->params->params["pass"][0], $this->params->params["pass"][1])) {
					$this->_flash(__('Delete Success', true),'alert alert-success');
				}
				$this->redirectCustom(null, $this->params->params["pass"][2]);
			}catch (Exception $e) {
					$this->_flash(__('Delete Error', true),'alert alert-warning');
					$this->params->params["pass"][1]=$this->params->params["pass"][2];
					$this->redirectCustom(null, $this->params->params["pass"][2]);
			}

        }
		
        /*----------------delete-----------------*/
        public function admin_deletemulti(){

            if($this->request->is('post')){
                //pr($this->data);
                $dataids =  $this->data['Node']['id'];

                try{
                    if ($this->Node->deleteAll(array('Nodeid' => $dataids))) {
                        $this->_flash(__('Delete Success',true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                    $this->_flash(__('Delete Error', true),'alert alert-warning');
                    $this->redirect(array('action' => 'admin_index'));
                }

            }else{
                $this->_flash(__('Delete Error', true),'alert alert-danger');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------delete-----------------*/

    /*----------------DELETE-----------------*/

	// modifica la fecha y almacena los codigos de corporacion y empresa
	private function preProcessData($data) {

		App::uses('Company', 'Model');
		
		if ($this->Auth->user('corporation_id')) {
			$data["Node"]["corporation_id"]=$this->Auth->user('corporation_id');
		} else {
		
			// Si la corporación del nodo no está seteada, pero la compañía tiene una corporación entonces setea tambien la corporación
		
			$Company = new Company();
			$res=$Company->find('first', array(	'conditions'=>array('Company.id'=>$this->Auth->user('company_id')), 
												'recursive'=>-1));
			$data["Node"]["corporation_id"]=$res["Company"]["corporation_id"];
			
		}

		if (isset($data["Node"]["inidate"])) {
			$data["Node"]["inidate"]=self::ymd2dmy($data["Node"]["inidate"]);
		}
		
		if (isset($data["Node"]["enddate"])) {
			$data["Node"]["enddate"]=self::ymd2dmy($data["Node"]["enddate"]);
		}
		
		return $data;
	
	}

	public function AddIndicatorNode($ent1, $ent2) {
		
		$auto=$this->Indicatornode->find('all',array(
			'conditions'=>array(
								"Indicatornode.indicator_id"=>$ent1,
								"Indicatornode.node_id"=>$ent2
								),
			'recursive'=>-1
		));
		
		if (!isset($auto) or !is_array($auto) or !sizeof($auto)) {
			$this->Indicatornode->create();
			$data= array("Indicatornode" => array(
										"indicator_id" => $ent1,
										"node_id" => $ent2
										)
			);
			$this->Indicatornode->set($data);
			$this->Indicatornode->saveMany();
		}
	
	}

	function admin_addgallery() {
		$this->layout = 'ajax';
		$data= array("Galleryimage" => array(
									"name" => array("esp" => $this->data["esp"],"eng" => $this->data["eng"]),
									"imgname" => $this->data["imgname"]
									)
					);
		try{
		
			$this->Galleryimage->deleteAll(array("Galleryimage.imgname"=>$this->data["imgname"]));

			$this->Galleryimage->imgname = $this->data["imgname"];
			$this->Galleryimage->set($data);

			if ($this->Galleryimage->saveMany()) {
				$this->dataajax['response']['message_success']=__('Update Success',true);
			}
		}catch (Exception $e) {
			$this->dataajax['response']['message_error']=__('Update Error: '.$e->getMessage(),true);
		}
		echo json_encode($this->dataajax);
		die();

	}
	
	
	function admin_delgallery() {
		$this->layout = 'ajax';
		try{
		
			$this->Galleryimage->deleteAll(array("Galleryimage.imgname"=>$this->data["imgname"]));
			
			// remove the file
			$file = dirname(dirname(__FILE__))."/webroot".$this->data["imgname"];
			unlink($file);
			
			$this->dataajax['response']['message_success']=__('Update Success',true);
			
		}catch (Exception $e) {
			$this->dataajax['response']['message_error']=__('Update Error: '.$e->getMessage(),true);
		}
		echo json_encode($this->dataajax);
		die();

	}
	
	function admin_delsurveyanswer() {
	
		$this->layout = 'ajax';
		try{
		
			$this->Surveyanswer->deleteAll(array("Surveyanswer.id"=>$this->data["id"]));
			
			$this->dataajax['response']['message_success']=__('Update Success',true);
			
		}catch (Exception $e) {
			$this->dataajax['response']['message_error']=__('Update Error: '.$e->getMessage(),true);
		}
		echo json_encode($this->dataajax);
		die();

	}
	
	function admin_delmessage() {
		$this->layout = 'ajax';
		try{
		
			$this->Mailboxsuggestion->deleteAll(array("Mailboxsuggestion.id"=>$this->data["id"]));
			
			$this->dataajax['response']['message_success']=__('Update Success',true);
			
		}catch (Exception $e) {
			$this->dataajax['response']['message_error']=__('Update Error: '.$e->getMessage(),true);
		}
		echo json_encode($this->dataajax);
		die();

	}
	
	function admin_addascommitment() {
	
		$this->layout = 'ajax';
		
		try{
		
			// Agregar el mensaje como Compromiso
			$sugerencia = $this->Mailboxsuggestion->find('first', array('conditions'=>array('id', $this->data["id"])));
			
			if ($sugerencia["Mailboxsuggestion"]["node_id"]) {
			
				$nombre = $sugerencia["Mailboxsuggestion"]["name"] . " (".$sugerencia["Mailboxsuggestion"]["email"].")";

				$mailbox = $this->Node->find('first', array('conditions'=>array('Node.id='.$sugerencia["Mailboxsuggestion"]["node_id"])));
				
				$this->Node->create();
				$data= array("Node" => array(
											"name" => array(
												"esp" => "Sugerencia de ".$nombre,
												"eng" => "Suggestion from ".$nombre),
											"description" => array(
												"esp" => $sugerencia["Mailboxsuggestion"]["comment"]."<hr>Tomado del buzón de sugerencias: <a href=\"/admin/entities/mailbox/".$sugerencia["Mailboxsuggestion"]["node_id"]."\">".$mailbox["NodenameTranslation"][0]["content"]."</a>",
												"eng" => $sugerencia["Mailboxsuggestion"]["comment"]."<hr>Taken from the suggestion mailbox: <a href=\"/admin/entities/mailbox/".$sugerencia["Mailboxsuggestion"]["node_id"]."\">".$mailbox["NodenameTranslation"][1]["content"]."</a>"),
											"nodetype_id" => Node::TYPE_COMMITMENT
											)
				);
				$this->Node->set($data);
				try{
					if ($this->Node->saveMany()) {
					
						// Si lo guardó entonces agrego la relación de empresa/corporación
						$this->Node->setCompany($this->Auth->user('company_id'));
					
						$this->redirect("/admin/nodes/edit/".$this->Node->id);
						
					}
				}catch (Exception $e) {
					$this->dataajax['response']['message_error']=__('Save Error',true);
				}

			}
			
			$this->dataajax['response']['message_success']=__('Update Success',true);
			
		}catch (Exception $e) {
			$this->dataajax['response']['message_error']=__('Update Error: '.$e->getMessage(),true);
		}
		echo json_encode($this->dataajax);
		die();

	}
	
}
