<?php
App::uses('AppController', 'Controller');
/**
 * Entities Controller
 *
 * @property Entities $Entities
 */

class EntitiesController extends AppController {

	/*----------------beforeFilter-----------------*/
    public function beforeFilter() {
        parent::beforeFilter();
    }
    /*----------------beforeFilter-----------------*/

	/*----------------index-----------------*/
	public function admin_index(){

	}
	/*----------------index-----------------*/


	
	public function admin_woupdate(){

		$this->ajaxVariablesInit();

		$wo_id = $this->params->params["pass"][0];
		$status = $this->params->params["pass"][1];

		$this->Node->read(null, $wo_id);
		$this->Node->set("status", $status);
		$this->Node->saveMany();

		echo json_encode(array("wo_id"=>$wo_id,"status"=>$status));
		die();
		
	}
	
	

	public function admin_inventory(){
	
		$wo = $this->Node->getNodes(Node::TYPE_WEAKNESSES, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
	
		$this->set(compact('wo'));
		
	}
	
	
	public function admin_weakness(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_WEAKNESSES, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=2;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_perspective(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_PERSPECTIVE, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=3;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_strategy(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_STRATEGY, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=4;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_mission(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_MISSION, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=5;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_vision(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_VISION, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=6;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_objective(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_OBJECTIVE, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=7;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_politics(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_POLITICS, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=8;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_commitment(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_COMMITMENT, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=9;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_expectation(){
		if (isset($this->params->params["pass"][0])) { $id = $this->params->params["pass"][0];}		
		$wo = $this->Node->getNodes(Node::TYPE_EXPECTATIONS, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=10;
		$this->set(compact('wo', 'id', 'redir'));
	}
	
	public function admin_program(){
		if (isset($this->params->params["pass"][0])) { $id = $this->params->params["pass"][0];}		
		$wo = $this->Node->getNodes(Node::TYPE_PROGRAMS, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=11;
		$this->set(compact('wo', 'id', 'redir'));
	}
	
	public function admin_activity(){
		if (isset($this->params->params["pass"][0])) { $id = $this->params->params["pass"][0];}		
		$wo = $this->Node->getNodes(Node::TYPE_ACTIVITIES, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=12;
		$this->set(compact('wo', 'id', 'redir'));
	}
	

/*	public function admin_iniciative(){
		if (isset($this->params->params["pass"][0])) { $id = $this->params->params["pass"][0];}		
		$wo = $this->Node->getNodes(Node::TYPE_INICIATIVES, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=13;
		$this->set(compact('wo', 'id', 'redir'));
	}
	
	public function admin_project(){
		if (isset($this->params->params["pass"][0])) { $id = $this->params->params["pass"][0];}		
		$wo = $this->Node->getNodes(Node::TYPE_PROJECTS, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=14;
		$this->set(compact('wo', 'id', 'redir'));
	}
	
	public function admin_course(){
		if (isset($this->params->params["pass"][0])) { $id = $this->params->params["pass"][0];}		
		$wo = $this->Node->getNodes(Node::TYPE_COURSES, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=15;
		$this->set(compact('wo', 'id', 'redir'));
	}

	*/
	
	public function admin_task(){
		if (isset($this->params->params["pass"][0])) { $id = $this->params->params["pass"][0];}		
		$wo = $this->Node->getNodes(Node::TYPE_TASKS, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=16;
		$this->set(compact('wo', 'id', 'redir'));
	}
	
	public function admin_interest(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_INTEREST, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=17;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_call(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_CALL, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=18;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_gallery(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_GALLERY, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=19;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_survey(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_SURVEY, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=20;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_mailbox(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_MAILBOX, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=21;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	
	public function admin_graph(){
	
		if (isset($this->params->params["pass"][0])) {
			$id = $this->params->params["pass"][0];
		}		
		$wo = $this->Node->getNodes(Node::TYPE_GRAPH, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$redir=22;
		$this->set(compact('wo', 'id', 'redir'));
		
	}
	

	/*----------------index-----------------*/

	/*----------------index-----------------*/
	public function admin_maturity(){

		$this->Autoevaluation->setLanguage();
		$this->Questionnaire->setLanguage();
		$conditions=array();
		if ($this->Auth->user()["corporation_id"]) {
			$conditions['Autoevaluation.corporation_id']=$this->Auth->user()["corporation_id"];
		}
		if ($this->Auth->user()["company_id"]) {
			$conditions['Autoevaluation.company_id']=$this->Auth->user()["company_id"];
		}			
		$auto = $this->Autoevaluation->find('all',array(
			'conditions'=>$conditions,
			'order' => array('Corporation.name', 'Company.name', "Autoevaluation.date_time"),
			'recursive'=>1
		));

		$indicators = $this->Indicator->getHightlightedIndicators($this->Auth->user()["company_id"], true);
		
		if (isset($auto) and is_array($auto)) {

			foreach($auto as $i=>$a) {
		
				$questionnaire = $this->Questionnaire->find('first', 
														array(	'conditions'=>array('Questionnaire.id'=>$a["Autoevaluation"]["questionnaire_id"]), 
																'recursive'=>-1)
				);
				
				$auto[$i]["Questionnaire"]=$questionnaire["Questionnaire"];
				
				$auto[$i]["QuestionnaireTotalPoints"]=$this->Questionnaire->calculateTotalPoints($a["Autoevaluation"]["questionnaire_id"]);
				
			}
		
		}
		
		$this->set(compact('auto', 'indicators'));
		
	}
	/*----------------index-----------------*/

	public function admin_add() {
	
		if ($this->data) {
		
			$fieldslocales = array('Node'=>array('name', 'description'));
			$validations = $this->validationLocale($fieldslocales);

			if(empty($validations)){
			
				$this->Node->create();
				$this->Node->set($this->data);

				try{
					if ($this->Node->saveMany()) {
						$this->dataajax['response']['method']=$this->getMethod();
						$this->dataajax['response']['message_success']=__('Save Success',true);
					}
				}catch (Exception $e) {
					$this->dataajax['response']['message_error']=__('Save Error',true);
				}
				
			}else{
				$this->dataajax['response']["errors"]=$validations;
			}
			
		} else {
	
			$this->Nodetype->setLanguage();
			$nodetypes = $this->Nodetype->find('all',array(
				'conditions'=>array('type'=>1),
				'order' => array('Nodetype.name'),
				'recursive'=>-1
			));
			
			$data["Node"]["nodetype_id"]=0;
			if (isset($this->params->params["pass"][0])) 
				$data["Node"]["nodetype_id"] = $this->params->params["pass"][0];
			
			$this->set(compact('nodetypes', 'data'));
			
		}
		
	}
	
	
	public function admin_graphsave() {

		$this->layout = 'clean';

		$filename="test.php";
		$folder="pChart2.1.4";
		
		$path = APP."webroot/$folder/$filename";
		
		$url = Router::fullBaseUrl($url = null)."/$folder/$filename";
		
		if (file_put_contents($path, $this->params->data["response"])) {
			$this->set(compact('url'));
		} else {
			$error = 1;
			$this->set(compact('error'));
		}

	}

	public function admin_getentities() {

		$this->layout = 'clean';

		$nodes = $this->Node->getNodes(	$this->data["nodetype"], 
										$this->Auth->user()["company_id"], 
										$this->Auth->user()["corporation_id"]);
		
		$nodetype = $this->data["nodetype"];
		
		$this->set(compact('nodes', 'nodetype'));
		
	}

	public function admin_hello() {

		$this->layout = 'clean';

		$id=$this->data["id"];
		$redir=$this->data["redir"];

		$nodos=$node=array();
		
		// agrega al redireccionador el codigo de la entidad para que redireccione directo a la entidad
		if (is_numeric($id) and is_numeric($redir)) {
			$redir = $redir."_".$id;
		}

		$este_nodo = $this->Node->find("first", array('conditions' => array('Node.id' => array($id))));

		$este_nodo_type_id = $este_nodo["Nodetype"]["id"];

		if ($este_nodo_type_id) {
		
			// busca los tipos de nodos para agregar entidades relacionadas
			$this->Nodetype->setLanguage();
			switch($este_nodo_type_id) {
				case 2: //perspectiva con politicas
					$nodetypes = $this->Nodetype->find("list", 
					array('conditions' => 
						array('Nodetype.id' => array(8))));
						break;					
				case 3: //estrategias con perspectivas
					$nodetypes = $this->Nodetype->find("list", 
					array('conditions' => 
						array('Nodetype.id' => array(2))));
						break;
				case 4: //compromisos con objetivos
					$nodetypes = $this->Nodetype->find("list", 
					array('conditions' => 
						array('Nodetype.id' => array(7))));
						break;
				case 5: //mision no se relaciona con nadie
					$nodetypes = array();
					break; 
				case 6: //vision con mision
					$nodetypes = $this->Nodetype->find("list", 
					array('conditions' => 
						array('Nodetype.id' => array(5))));
						break; 
				case 7: //objetivos con estrategias 
					$nodetypes = $this->Nodetype->find("list", 
					array('conditions' => 
						array('Nodetype.id' => array(3))));
						break;
				case 8: //politicas con vision
					$nodetypes = $this->Nodetype->find("list", 
					array('conditions' => 
						array('Nodetype.id' => array(6))));
						break;
				case 10: //programas con compromisos
					$nodetypes = $this->Nodetype->find("list", 
					array('conditions' => 
						array('Nodetype.id' => array(4))));
						break;
				case 11: //Actividades con proyectos, programas e indicadores de gestion
					$nodetypes = $this->Nodetype->find("list", 
					array('conditions' => 
						array('Nodetype.id' => array(10))));
						break;
				case 15: // tareas con: Proyectos y con indicadores de gestión.
					$nodetypes = $this->Nodetype->find("list", 
					array('conditions' => 
						array('Nodetype.id' => array(11))));
						break;
				case 16: //grupos de interes con Programas, Actividades, Proyectos, Cursos y Tareas
					$nodetypes = $this->Nodetype->find("list", 
					array('conditions' => 
						array('Nodetype.id' => array(10,11,15))));
						break;
				case 17: // Convocatorias sin relacion
					$nodetypes = array();
						break;
				case 18: //Galerias 
				case 19: // Encuestas
					$nodetypes = $this->Nodetype->find("list", 
					array('conditions' => 
						array('Nodetype.id' => array(10,11))));
						break;
				case 20: //Buzon con grupos de interes 
					$nodetypes = $this->Nodetype->find("list", 
					array('conditions' => 
						array('Nodetype.id' => array(16))));
						break;
				default:
					$nodetypes = $this->Nodetype->find("list");
			}
			
			$indicators = array();

//			echo '<h1>'.$este_nodo_type_id.'</h1>';

			if (in_array($este_nodo_type_id, array(7, 11, 15))) {
				$indicators = $this->Indicator->indicatorsByCompany($this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
			}
			
			$this->Node->setLanguage();
			$node = $this->Node->find('first', 
				array(	'conditions' => array('Node.id'=>$id), 'recursive'=>-1 )
			);

			$node["Nodesource"]=array();
			if ($node["Node"]["origin"]){
				$this->Nodesource->setLanguage();
				$node_source = $this->Nodesource->find('first', 
					array(	'conditions' => array('Nodesource.id'=>$node["Node"]["origin"]) )
				);
				$node["Nodesource"]=$node_source["Nodesource"];
			}
			
			$node["Nodestatus"]=array();
			if ($node["Node"]["status"]) {
				$this->Nodestatus->setLanguage();
				$node_status = $this->Nodestatus->find('first', 
					array(	'conditions' => array('Nodestatus.id'=>$node["Node"]["status"]) )
				);
				$node["Nodestatus"]=$node_status["Nodestatus"];
			}

			$node["Nodepriority"]=array();
			
			if ($node["Node"]["priority"]) {
			
				$this->Nodepriority->setLanguage();
				$node_priority = $this->Nodepriority->find('first', 
					array(	'conditions' => array('Nodepriority.id'=>$node["Node"]["priority"]) )
				);
				$node["Nodepriority"]=$node_priority["Nodepriority"];
			}

			$related = $this->Node->getRelatedNodes($id);
			$related_indicators = $this->Indicator->getRelatedIndicators($id);
			$predecessors = $this->Node->getPredecessors($id);
			
			// si es una convocatoria
			if ($node["Node"]["nodetype_id"]==17) {
			
				$interest_groups = $this->Node->getNodes(	Node::TYPE_INTEREST, 
															$this->Auth->user()["company_id"], 
															$this->Auth->user()["corporation_id"]);
				
				$this->Node->setLanguage();
				
				foreach($interest_groups as $i=>$ig) {
				
					$the_calls = $this->Interestgroupcall->find('all', 
																array(	'conditions'=>array('interestgroup_id'=>$ig["Node"]["id"]),
																		'order'=>'datetime DESC'));
				
					$interest_groups[$i]["Calls"] = $the_calls;

				}
				
			}

		}
		
		
		
		// Galeria
		if ($node["Node"]["nodetype_id"]==18) {

			$imgs = $this->getGalleryImgs($this->Session->read('TheCompany')["Company"]["id"], $node["Node"]["id"]);
		
			$this->set(compact('imgs'));
			
		}
		
		// Buzon de sugerencias
		if ($node["Node"]["nodetype_id"]==20) {

			$this->Mailboxsuggestion->setLanguage();
			$msgs = $this->Mailboxsuggestion->find('all', array("conditions"=>array('node_id'=>$id), 'order'=>'datetime DESC', 'limit'=>100));
		
			$this->set(compact('msgs'));
			
		}
		
		// Encuesta
		if ($node["Node"]["nodetype_id"]==19) {

			$this->Surveyquestion->setLanguage();
			$questions = $this->Surveyquestion->find('all', array("conditions"=>array('node_id'=>$id), 'order'=>'order'));
		
			$this->Surveyanswer->setLanguage();
			$answers = $this->Surveyanswer->find('all', array("conditions"=>array('node_id'=>$id), 'order'=>'datetime DESC'));
			
			$totaliza = $this->Surveyanswer->totalize($questions, $answers);
			
			$this->set(compact('questions', 'answers', 'totaliza'));
			
		}

		// gráfica
		if ($node["Node"]["nodetype_id"]==21) {

			$this->Graphdata->setLanguage();
			$graphdata = $this->Graphdata->find('all', array("conditions"=>array('node_id'=>$id), 'limit'=>100));
		
			$graphtypes=$this->Graphdata->getGraphTypes();
			$graphtypes=$graphtypes[$this->Session->read('Config.MyLangVar')];
			
			$this->set(compact('graphdata', 'graphtypes'));
			
		}
		
		$all_nodetypes = $this->Nodetype->find("list");

		$this->set(compact('id', 'node', 'nodetypes', 'related', 'related_indicators', 'predecessors', 'indicators', 'redir', 'interest_groups', 'all_nodetypes'));

	}
	
	public function admin_selentities() {

		$this->layout = 'clean';

		$id=$this->data["id"];
		$entity_id=$this->data["entity_id"];
		$redir=$this->data["redir"];
		$predecessor=$this->data["predecessor"];
		
		$allnodes = $this->Node->getNodes($id, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);

		$this->set(compact('id', 'entity_id', 'allnodes', 'redir', 'predecessor'));
		
	}
	
	public function saveNodeNodes($ent1, $ent2) {
		
		$auto=$this->Nodenode->find('all',array(
			'conditions'=>array("OR"=>array(
								array("Nodenode.node_id"=>$ent1,"Nodenode.node_id2"=>$ent2),
								array("Nodenode.node_id"=>$ent2,"Nodenode.node_id2"=>$ent1))),
			'recursive'=>-1
		));
		
		if (!isset($auto) or !is_array($auto) or !sizeof($auto)) {
			$this->Nodenode->create();
			$data= array("Nodenode" => array(
										"node_id" => $ent1,
										"node_id2" => $ent2
										)
						);
			$this->Nodenode->set($data);
			$this->Nodenode->saveMany();
		}
		
	}
		
	public function admin_addentityrel() {
	
		$this->layout = 'clean';
		
		$ent1 = $this->params->params["pass"][0];
		$ent2 = $this->params->params["pass"][1];
		$redir = $this->params->params["pass"][2];

		$this->saveNodeNodes($ent1, $ent2);
		
		$this->redirectCustom(null, $redir);
	
	}
	
	public function admin_sendcall() {
	
		$this->layout = 'clean';
		
		$ent1 = $this->params->params["pass"][0];
		$ent2 = $this->params->params["pass"][1];
		$redir = $this->params->params["pass"][2];
	
		$auto=$this->Interestgroupcall->find('all',array(
			'conditions'=>array(
								"Interestgroupcall.call_id"=>$ent1,
								"Interestgroupcall.interestgroup_id"=>$ent2,
								"Interestgroupcall.sent<>1"
								),
			'recursive'=>-1
		));
		
		if (!isset($auto) or !is_array($auto) or !sizeof($auto)) {
			$this->Interestgroupcall->create();
			$data= array("Interestgroupcall" => array(
										"call_id" => $ent1,
										"interestgroup_id" => $ent2,
										"sent" => 0
										)
			);
			$this->Interestgroupcall->set($data);
			$this->Interestgroupcall->saveMany();
		}

		$this->sendCall2IG($ent1, $ent2);
		
		$this->redirectCustom(null, $redir);
		
	}
	
	public function sendCall2IG($call_id, $ig_id) {
	
		$this->Node->setLanguage();
		$call	= $this->Node->find('first',array('conditions'=>array('Node.id'=>$call_id)));
		
		$titulo = $call["Node"]["name"];
		$mensaje = $call["Node"]["description"];
		// Para enviar un correo HTML, debe establecerse la cabecera Content-type
		$cabeceras  = 'MIME-Version: 1.0' . "\r\n";
		$cabeceras .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		if ($titulo or $desc) {
			$users	= $this->User->find('all',array('conditions'=>array('User.interestgroup_id'=>$ig_id)));
			foreach($users as $u) {
				$para=$u["User"]["email"];
				mail($para, $titulo, $mensaje, $cabeceras);				
			}
		}
	
	}
	
	public function AddIndicatorNode($ent1, $ent2) {
		
		$auto=$this->Indicatornode->find('all',array(
			'conditions'=>array(
								"Indicatornode.indicator_id"=>$ent1,
								"Indicatornode.node_id"=>$ent2
								),
			'recursive'=>-1
		));
		
		if (!isset($auto) or !is_array($auto) or !sizeof($auto)) {
			$this->Indicatornode->create();
			$data= array("Indicatornode" => array(
										"indicator_id" => $ent1,
										"node_id" => $ent2
										)
			);
			$this->Indicatornode->set($data);
			$this->Indicatornode->saveMany();
		}
	
	}
	
	public function admin_addindicator() {
	
		$this->layout = 'clean';
		
		$ent1 = $this->params->params["pass"][0];
		$ent2 = $this->params->params["pass"][1];
		$redir = $this->params->params["pass"][2];

		$this->AddIndicatorNode($ent1, $ent2);
		
		$this->redirectCustom(null, $redir);
	
	}
	
	// node_id es la entidad que tiene predecesor
	// node_id2 es el predecesor
	public function admin_addpredecessor() {
	
		$this->layout = 'clean';
		
		$ent1 = $this->params->params["pass"][0];
		$ent2 = $this->params->params["pass"][1];
		$redir = $this->params->params["pass"][2];
	
		$auto=$this->Nodepredecessor->find('all',array(
			'conditions'=>array(array("Nodepredecessor.node_id"=>$ent1,"Nodepredecessor.node_id2"=>$ent2)),
			'recursive'=>-1
		));
		
		if (!isset($auto) or !is_array($auto) or !sizeof($auto)) {
			$this->Nodepredecessor->create();
			$data= array("Nodepredecessor" => array(
										"node_id" => $ent1,
										"node_id2" => $ent2
										)
						);
			$this->Nodepredecessor->set($data);
			$this->Nodepredecessor->saveMany();
		}
		
		$this->redirectCustom(null, $redir);
	
	}
	
	public function admin_savedatagraph(){

		$this->layout = 'clean';
		
/*

$this->data

Array
(
    [name] => 1
    [name_eng] => 2
    [serie_name1] => 3
    [serie_name1_eng] => 5
    [serie_name2] => 4
    [serie_name2_eng] => 6
    [type] => 10
    [ladata] => 7|11||8|12||9|13||10|14
    [id] => 133
    [redir] => 22_133
)
*/		
		$data["node_id"]=$this->data["id"];
		$data["type"]=$this->data["type"];
		
		$ladatafinal[0]["serie_name1"]=$this->data["serie_name1"];
		$ladatafinal[0]["serie_name1_eng"]=$this->data["serie_name1_eng"];
		$ladatafinal[0]["serie_name2"]=$this->data["serie_name2"];
		$ladatafinal[0]["serie_name2_eng"]=$this->data["serie_name2_eng"];
		
		
		$auxiliar=explode("||", $this->data["ladata"]); // -> 0 esp / 1: eng / 2: data serie 1 / 3: data serie 2
		$ladatafinal["esp"]=explode("|", $auxiliar[0]);
		$ladatafinal["eng"]=explode("|", $auxiliar[1]);
		$ladatafinal["data"]=explode("|", $auxiliar[2]);
		$ladatafinal["data2"]=explode("|", $auxiliar[3]);

		$data["name"]["esp"]=$this->data["name"];
		$data["name"]["eng"]=$this->data["name_eng"];
		
		$data["data"]=serialize($ladatafinal);

		try{
		
			$this->Graphdata->deleteAll(array("Graphdata.node_id"=>$this->data["id"]));
			
			$this->Graphdata->create();
			$this->Graphdata->set($data);

			if ($this->Graphdata->saveMany()) {			
				$this->dataajax['response']['method']=$this->getMethod();
				$this->dataajax['response']['message_success']=__('Save Success',true);
				
				$this->redirectCustom(null, $this->data["redir"]);
				die;
			}
			
		}catch (Exception $e) {
			$this->dataajax['response']['message_error']=__('Save Error',true);
		}

		echo json_encode($this->dataajax);
		die();
		
	}
	
	function admin_wizard() {
	
		// paso 1 - b
		if ($this->params->query["e"]) {
			if ($this->params->query['n']) {
				$this->Nodetemporary->deleteAll();
			}
			
			$e = $this->params->query["e"];
			$summernote=true;
			$this->set(compact('e', 'summernote'));
			
		} elseif (is_numeric($this->params["pass"][0])) {
			
			// Guarda la entidad y muestra las entidades estratégicas asociadas
			if ($this->params['pass'][0] == 8 && $this->params['data']['nextstep'] == 1) {
					if (in_array($this->params['data']['guardar'], array(0,2))) {

					$save = true;
					$temporary = array();
				
					$this->Node->create();
					$data= array("Node" => array(
												"name" => array("esp" => $this->params["data"]["name_esp"],
																"eng" => $this->params["data"]["name_eng"]),
												"description" => array(	"esp" => $this->params["data"]["description_esp"],
																		"eng" => $this->params["data"]["description_eng"]),
												"nodetype_id" => $this->Nodetemporary->getSaveOrder($this->params["data"]["node_id"])
												)
								);
					$this->Node->set($data);
					if ($this->Node->saveMany()) {
						$this->_flash(__('Record Saved Successfully'),'alert alert-success');						
					} else {
						$save = false;				
						$this->_flash(__('Error: complete the fields'),'alert alert-warning');						
					}
					
					$node_id = $this->Node->id;
					$this->Nodetemporary->Create();
					$temporary['node_id'] = $node_id;
					$nodetype_id = $this->Nodetemporary->getSaveOrder($this->params["data"]["node_id"]);
					$temporary['nodetype_id'] = $nodetype_id;
					$temporary['order'] = $this->Nodetemporary->getListOrder($nodetype_id);
					$this->Nodetemporary->set($temporary);
					$this->Nodetemporary->save();

					
					// asocia el nodo a la empresa
					if ($save) {
						$this->Nodecompany->create();
						$data= array("Nodecompany" => array(
												"node_id" => $node_id,
												"company_id" => $this->Auth->user()["company_id"]
												)
						);
						$this->Nodecompany->set($data);
						$this->Nodecompany->saveMany();
					}
				
				} 

				$this->set('lista', $this->Nodetemporary->getList());
				$this -> render('wizard_step1');
			} else {

				if (in_array($this->params['data']['guardar'], array(0,2))) {

					$save = true;
					$temporary = array();
				
					$this->Node->create();
					$data= array("Node" => array(
												"name" => array("esp" => $this->params["data"]["name_esp"],
																"eng" => $this->params["data"]["name_eng"]),
												"description" => array(	"esp" => $this->params["data"]["description_esp"],
																		"eng" => $this->params["data"]["description_eng"]),
												"nodetype_id" => $this->Nodetemporary->getSaveOrder($this->params["data"]["node_id"])
												)
								);
					$this->Node->set($data);
					if ($this->Node->saveMany()) {						
						$this->_flash(__('Record Saved Successfully'),'alert alert-success');												
					} else {
						$save = false;				
						$this->_flash(__('Error: complete the fields'),'alert alert-warning');	
					}
					
					$node_id = $this->Node->id;
					$this->Nodetemporary->Create();
					$temporary['node_id'] = $node_id;
					$nodetype_id = $this->Nodetemporary->getSaveOrder($this->params["data"]["node_id"]);
					$temporary['nodetype_id'] = $nodetype_id;
					$temporary['order'] = $this->Nodetemporary->getListOrder($nodetype_id);
					$this->Nodetemporary->set($temporary);
					$this->Nodetemporary->save();

					
					// asocia el nodo a la empresa
					if ($save) {
						$this->Nodecompany->create();
						$data= array("Nodecompany" => array(
												"node_id" => $node_id,
												"company_id" => $this->Auth->user()["company_id"]
												)
						);
						$this->Nodecompany->set($data);
						$this->Nodecompany->saveMany();
					}
					if ($this->params['data']['guardar'] == 0) {
						$this->redirect('/admin/entities/wizard/?e=' . $this->params['pass'][0]);
					} else {
						$numero = $this->params['pass'][0];
						$numero++;
						$this->redirect('/admin/entities/wizard/?e=' . $numero);	
					}


				} else {
					$numero = $this->params['pass'][0];
					$numero++;
					$this->redirect('/admin/entities/wizard/?e=' . $numero);
				}	
			}
		} elseif ($this->params["pass"][0]=="step2") {
			$node_id = $this->params['pass'][1];
			if (strlen($node_id) > 0) {
				$data = $this->Node->findById($node_id);
				$this->Nodetemporary->deleteRecord($node_id);
				$this->Nodetype->setLanguage();
				switch($data['Node']['nodetype_id']) {
					case 4:
					$nodetypes = $this->Nodetype->find('all',array(
						'conditions'=>array('Nodetype.id'=>7),
						'order' => array('Nodetype.name'),
						'recursive'=>-1
					));
					break;
					case 6:
						$nodetypes = $this->Nodetype->find('all',array(
							'conditions'=>array('Nodetype.id'=>5),
							'order' => array('Nodetype.name'),
							'recursive'=>-1
						));
					break;	 
					case 5:
						$nodetypes = array();
					break;
					case 3:
					$nodetypes = $this->Nodetype->find('all',array(
						'conditions'=>array('Nodetype.id'=>2),
						'order' => array('Nodetype.name'),
						'recursive'=>-1
					));
					break;
					case 7:
					$nodetypes = $this->Nodetype->find('all',array(
						'conditions'=>array('Nodetype.id'=>3),
						'order' => array('Nodetype.name'),
						'recursive'=>-1
					));
					break;
					case 8:
					$nodetypes = $this->Nodetype->find('all',array(
						'conditions'=>array('Nodetype.id'=>6),
						'order' => array('Nodetype.name'),
						'recursive'=>-1
					));
					break;
					case 2:
					$nodetypes = $this->Nodetype->find('all',array(
						'conditions'=>array('Nodetype.id'=>8),
						'order' => array('Nodetype.name'),
						'recursive'=>-1
					));
					break;
				}
			} else {
				$this->Nodetype->setLanguage();
				$nodetypes = $this->Nodetype->find('all',array(
					'conditions'=>array('type'=>1),
					'order' => array('Nodetype.name'),
					'recursive'=>-1
				));
			}
			

			$next_step=10;			
			
			$message = __("How is related ") 
			. __($this->Nodetemporary->getNodetypeNameById($data['Node']['nodetype_id'])) 
			. ' ' . __($data['Node']['name']) 
			. __(" With...");
			
			$this->set(compact('node_id', 'name_esp', 'name_eng', 'description_esp', 
			'description_eng', 'nodetypes', 'node_id', 'next_step', 'message'));
			$this -> render('wizard_step2');
			
		} elseif ($this->params["pass"][0]=="step3") {

			// Guarda las relaciones estratégicas y muestra las entidades tacticas asociadas

			if (is_array($this->params["data"]["related"])) {

				foreach($this->params["data"]["related"] as $rel) {
				
					$this->saveNodeNodes($this->params["data"]["node_id"], $rel);
					
				}
				
			}
			
			$this->Nodetype->setLanguage();
			$nodetypes = $this->Nodetype->find('all',array(
				'conditions'=>array('type'=>2),
				'order' => array('Nodetype.name'),
				'recursive'=>-1
			));

			$next_step=11;
			$message = __("Do you want to associate this with a Tactical Planning entity?");
			
			$node_id=$this->params["data"]["node_id"];
		
			$this->set(compact('node_id', 'name_esp', 'name_eng', 
			'description_esp', 'description_eng', 'nodetypes', 'node_id', 'next_step', 'message'));
			$this -> render('wizard_step2');
			
		} elseif ($this->params["pass"][0]=="step4") {
		
			// Guarda las relaciones tacticas y muestra las entidades operativas asociadas

			if (is_array($this->params["data"]["related"])) {

				foreach($this->params["data"]["related"] as $rel) {
				
					$this->saveNodeNodes($this->params["data"]["node_id"], $rel);
					
				}
				
			}
			
			$this->Nodetype->setLanguage();
			$nodetypes = $this->Nodetype->find('all',array(
				'conditions'=>array('type'=>3),
				'order' => array('Nodetype.name'),
				'recursive'=>-1
			));

			$next_step=12;
			$message = __("Do you want to associate this with an Operation Management entity?");
			
			$node_id=$this->params["data"]["node_id"];
			
			$this->set(compact('node_id', 'name_esp', 'name_eng', 'description_esp', 'description_eng', 'nodetypes', 'node_id', 'next_step', 'message'));
			$this -> render('wizard_step2');
			
		} elseif ($this->params["pass"][0]=="step5") {
		
			// Guarda las relaciones operativas y muestra los indicadores de gestión asociados	

			if (is_array($this->params["data"]["related"])) {

				foreach($this->params["data"]["related"] as $rel) {
				
					$this->saveNodeNodes($this->params["data"]["node_id"], $rel);
					
				}
				
			}

			$data = $this->Node->findById($this->params['data']['node_id']);
			$indicators = array();
			if ($data['Node']['nodetype_id'] == 7) {				
				$indicators = $this->Indicator->getIndicators($this->Auth->user()["company_id"], true);
				$next_step=13;
				$message = __("Do you want to associate this with an Indicator Management?");
				
				$node_id=$this->params["data"]["node_id"];
				
				$this->set(compact('node_id', 'indicators', 'next_step', 'message'));
				$this -> render('wizard_step5');
			} else {
				$this->set('lista', $this->Nodetemporary->getList());
				$this -> render('wizard_step1');
			}
			
		} elseif ($this->params["pass"][0]=="step6") {
		
			// Guarda los indicadores de gestión y muestra la entidad creada

			if (is_array($this->params["data"]["related"])) {

				foreach($this->params["data"]["related"] as $rel) {
				
					$this->addIndicatorNode($rel, $this->params["data"]["node_id"]);
					
				}
				
			}

			//$n = $this->Node->find('first', array('conditions'=>array('Node.id'=>$this->params["data"]["node_id"])));
			
			$this->_flash(__('The entity has been created', true),'alert alert-info');
			$this->set('lista', $this->Nodetemporary->getList());
			$this -> render('wizard_step1');
			//$this->redirect("/admin/entities/".laVista($n["Node"]["nodetype_id"])."/".$this->params["data"]["node_id"]);
			
			//die;
		
		}
		
	}



	function admin_bsc() {
		
		$this->Node->setLanguage();
		$this->Indicator->setLanguage();
/*
		$bsc_data_raw = $this->Indicator->bsc($this->Auth->user()["company_id"]);

		if (is_array($bsc_data_raw)) {
			foreach($bsc_data_raw as $bsc) {
				$found_perspective = false;
				$cadena=null;
				for ($ibsc=7; $ibsc>0; $ibsc--) {
					if(!$found_perspective and $bsc['n'.$ibsc]['nt'.$ibsc]==2) {
						$found_perspective=true;
					}
					if ($found_perspective) {
						if ($cadena) $cadena.='|';
						$cadena .= $bsc['n'.$ibsc]['n'.$ibsc];
					}
				}
				$tree = explode('|', $cadena);
				if (is_array($tree)) {
					$summary = $this->Indicator->getSummaryIndicator($bsc['i']['id']);
					if (is_array($summary)) {
						$summary = array_pop($summary);
						$bsc_data[] = [	'tree' 		=> $tree, 
										'indicator' => $bsc['i'],
										'summary'	=> $summary
						];
					}
				}
			}
		}
*/

		$perspectives = $this->Node->getNodes(Node::TYPE_PERSPECTIVE, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"]);
		$indicators_temp = $this->Indicator->getHightlightedSummary($this->Auth->user()["company_id"], true);

		foreach($indicators_temp as $it) {
			$indicators[] = $this->Indicator->findById($it["id"]);
			$res[] = $this->Indicator->getSummaryIndicator2($it["id"]);
		}

		foreach($perspectives as $i=>$p) {
		
			$perspectives[$i]["Indicators"] = $this->Indicator->getRelatedIndicators($p["Node"]["id"]);
			$perspectives[$i]["Indicators_detail"] = $this->Indicator->getRelatedIndicatorsWithDetail($p["Node"]["id"]);
			$perspectives[$i]['Strategies'] = $this->Indicator->getRelatedStrategies($p["Node"]["id"]);
		
		}

		$calc = Indicator::calculationTypeArray();
		$meas = Indicator::periodicMeasureArray();
		
		$this->set(compact('perspectives', 'indicators', 'res', 'calc', 'meas', 'bsc_data'));
		
	
	}
	
}
