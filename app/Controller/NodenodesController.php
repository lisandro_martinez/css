<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Nodenode $Nodenode
 */

class NodenodesController extends AppController {

	/*----------------beforeFilter-----------------*/
    public function beforeFilter() {
        parent::beforeFilter();
    }
    /*----------------beforeFilter-----------------*/


        /*----------------delete-----------------*/
        public function admin_delete($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if(!empty($id)){
                $this->Nodenode->id = $id;
                if (!$this->Nodenode->exists()) {
                    $this->_flash(__('No-exist-record', true),'alert alert-danger');
                    $this->redirectCustom($this->params);
                }

                try{
                    if ($this->Nodenode->delete($id,true)) {
                        $this->_flash(__('Delete Success', true),'alert alert-success');
						$this->redirectCustom($this->params);
                    }
                }catch (Exception $e) {
                        $this->_flash(__('Delete Error', true),'alert alert-warning');
						$this->redirectCustom($this->params);
                }
            }else{

                $this->get_index('admin_index');
				$this->redirectCustom($this->params);
				
            }

        }
        /*----------------delete-----------------*/

}
