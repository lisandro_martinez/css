<?php
App::uses('AppController', 'Controller');
App::uses('Graphdata', 'Model');
/**
 * Categories Controller
 *
 * @property Indicator $Indicator
 */

class IndicatorsController extends AppController {

	/*----------------beforeFilter-----------------*/
    public function beforeFilter() {
        parent::beforeFilter();
    }
    /*----------------beforeFilter-----------------*/


        public function paramFilters($urlform){

            $form_config = array();
            $form_config["title"] = __("Search / Filter");
            $form_config["urlform"] = $urlform;
            $form_config["labelbutton"] = __("Search / Filter");
            $this->set('form_config',$form_config);

            $fields_char = array('name');
			
            $conditions = $this->filterConfig('Indicator',$fields_char);
			
            $this->recordsforpage();

			// Busca los indicadores de gestión de la compañía del usuario
			if ($this->Auth->user('company_id')) {
				$conditions[]=array("company_id"=>$this->Auth->user('company_id'));
			} else {
				// Busca los indicadores de gestión de las compañías de la corporación del usuario
				if ($this->Auth->user('corporation_id')) {
					$conditions[]=array("company_id"=>$this->Company->find("list", array("conditions"=>array("corporation_id"=>$this->Auth->user('corporation_id')), 'fields'=>"id")));
				// Sino Busca todas...
				}
			}

            return $conditions;

        }


    /*----------------INDEX-----------------*/

        /*----------------get_index-----------------*/
        public function get_index($urlfilter = 'admin_index'){
            $conditions=$this->paramFilters($urlfilter);
            $limit = $this->Session->read('Filter.recordsforpage');

            $this->Indicator->setLanguage();
            $this->Paginator->settings = array(
                'order' => 'Indicator.id ASC',
                'conditions' => $conditions,
                'limit' => $limit,
                'recursive' => 1
            );

            $lists = $this->Paginator->paginate('Indicator');
			
            $this->set(compact('lists'));
        }
        /*----------------get_index-----------------*/

        /*----------------index-----------------*/
        public function admin_index(){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if ($this->request->is('get')) {
                $this->get_index();
            }
			
			// Busca los indicadores de gestión de la compañía del usuario
			if ($this->Auth->user('company_id')) {
				$companies=$this->Company->find("list", array("conditions"=>array("id"=>$this->Auth->user('company_id'))));
			} else {
				// Busca los indicadores de gestión de las compañías de la corporación del usuario
				if ($this->Auth->user('corporation_id')) {
					$companies=$this->Company->find("list", array("conditions"=>array("corporation_id"=>$this->Auth->user('corporation_id'))));
				// Sino no busca nada...
				} else {
					$companies=array();
				}
			}


            $this->set(compact('companies'));
			
			
        }
        /*----------------index-----------------*/

    /*----------------INDEX-----------------*/

    /*----------------ADD-----------------*/

        /*----------------post_add-----------------*/
        public function post_add(){

			$this->ajaxVariablesInit();

			$fieldslocales = array('Indicator'=>array('name'));
			$validations = $this->validationLocale($fieldslocales);

			if(empty($validations)){

				$data=self::preProcessData($this->data);
				
				$data["Indicator"]["company_id"]=$this->Auth->user('company_id');

				$this->Indicator->create();
				$this->Indicator->set($data);

				try{
					if ($this->Indicator->saveMany()) {
					    if (! empty($data['Indicator']['baseline']) && is_numeric($data['Indicator']['baseline'])) {
                            $auxIndicatorValue = array();
                            $this->Indicatorvalue->create();                            
                            $auxIndicatorValue['Indicatorvalue']['indicator_id'] = $this->Indicator->getLastInsertId();
                            $auxIndicatorValue['Indicatorvalue']['value'] = $data['Indicator']['baseline'];
                            $auxIndicatorValue['Indicatorvalue']['date'] = $data['Indicator']['inidate'];
                            $this->Indicatorvalue->set($auxIndicatorValue);
                            $this->Indicatorvalue->save();
                        }
                        $this->dataajax['response']['method']=$this->getMethod();
						$this->dataajax['response']['message_success']=__('Save Success',true);						
					}
				}catch (Exception $e) {
					$this->dataajax['response']['message_error']=__('Save Error'.$e->getMessage(),true);
				}
			}else{
				$this->dataajax['response']["errors"]=$validations;
			}

			echo json_encode($this->dataajax);
			die();
			
        }
        /*----------------post_add-----------------*/

        /*----------------get_add-----------------*/
        public function get_add(){
            $treshold = array();
            $treshold['Mayor o igual que'] = __('Greater or equal than');
            $treshold['Mayor que'] = __('Greater than');
            $treshold['Menor o igual que'] = __('Lesser or equal than');
            $treshold['Menor que'] = __('Lesser than');

		$graph_list = Graphdata::get_list();


            $this->set(compact('treshold', 'graph_list'));
        }
        /*----------------get_add-----------------*/

        /*----------------add-----------------*/
        public function admin_add() {
		
            $form_config = array();
            $form_config["title"] = __("Add")." ".__("Indicator");
            $form_config["urlform"] = "admin_add";
            $form_config["labelbutton"] = __("Add");
            $form_config["type"] = 'file';
            $this->set('form_config',$form_config);

            if ($this->request->is('post')) {
                $this->post_add();
            }elseif ($this->request->is('get')){
                $this->get_add();
            }

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

			  $is_root = $this->isRoot();

			  $summernote=true;

			  $graphs = $this->Node->getNodes(Node::TYPE_GRAPH, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"], true);
			
			  $this->set(compact('is_root','summernote', 'graphs'));
			  
        }
        /*----------------add-----------------*/

    /*----------------ADD-----------------*/

    /*----------------EDIT-----------------*/

        /*----------------get_edit-----------------*/
        public function get_edit($id){

            $this->Indicator->id = $id;

            if (!$this->Indicator->exists()) {
                $this->_flash(__('No-exist-record',true),'alert alert-warning');
                $this->redirect(array('action' => 'admin_edit'));
            }else{

                $graph_list = Graphdata::get_list();

            	$treshold = array();
				$treshold['Mayor o igual que'] = __('Greater or equal than');
				$treshold['Mayor que'] = __('Greater than');
				$treshold['Menor o igual que'] = __('Lesser or equal than');
				$treshold['Menor que'] = __('Lesser than');
				$this->set(compact('treshold'));

            	$this->set(
                	array(
                		"modules" => $this->Module->find("list")
                	)
                );

                $datamodel = $this->Indicator->read(null, $id);
                $this->request->data = self::preProcessData($this->readWithLocale($datamodel)); // se trae la informacion al editar.
				
                $this->set(compact('id', 'graph_list'));
            }

        }
        /*----------------get_edit-----------------*/

        /*----------------post_edit-----------------*/
        public function post_edit($id){

			$this->ajaxVariablesInit();
			
			$fieldslocales = array('Indicator'=>array('name'));
			$validations = $this->validationLocale($fieldslocales);

			if(empty($validations)){

				$data=self::preProcessData($this->data);
				$data["Indicator"]["company_id"]=$this->Auth->user('company_id');
				
				$this->Indicator->id = $id;
				$this->Indicator->set($data);

				try{
					if ($this->Indicator->saveMany()) {

						$this->dataajax['response']['method']=$this->getMethod();
						$this->dataajax['response']['message_success']=__('Update Success',true);
					}
				}catch (Exception $e) {
					$this->dataajax['response']['message_error']=__('Update Error: '.$e->getMessage(),true);
				}
			}else{
				$this->dataajax['response']["errors"]=$validations;
			}
			echo json_encode($this->dataajax);
			die();

        }
        /*----------------post_edit-----------------*/

        /*----------------edit-----------------*/
        public function admin_edit($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            $form_config = array();
            $form_config["title"] = __("Edit")." ".__("Indicator");
            $form_config["urlform"] = "admin_edit";
            $form_config["labelbutton"] = __("Save");
            $form_config["type"] = 'put';
            $this->set('form_config',$form_config);

            if ($this->request->is('get')) {
                if(empty($id)){
                    $this->get_index('admin_edit');
                }else{
                    $this->get_edit($id);
                }
            }else{
                if ($this->request->is('put')) {
                    $this->post_edit($id);
                }
            }

			  $is_root = $this->isRoot();

			  $summernote=true;
			  
			  $graphs = $this->Node->getNodes(Node::TYPE_GRAPH, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"], true);

			  $this->set(compact('is_root','summernote', 'graphs'));
			  
        }
        /*----------------edit-----------------*/

    /*----------------EDIT-----------------*/


    /*----------------DELETE-----------------*/

        /*----------------delete-----------------*/
        public function admin_delete($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if(!empty($id)){
                $this->Indicator->id = $id;
                if (!$this->Indicator->exists()) {
                    $this->_flash(__('No-exist-record', true),'alert alert-danger');
                    $this->redirectCustom($this->params);
                }

                try{
                    if ($this->Indicator->delete($id,true)) {
                        $this->_flash(__('Delete Success', true),'alert alert-success');
						$this->redirectCustom($this->params);
                    }
                }catch (Exception $e) {
                        $this->_flash(__('Delete Error', true),'alert alert-warning');
						$this->redirectCustom($this->params);
                }
            }else{

                $this->get_index('admin_index');
				$this->redirectCustom($this->params);
				
            }

        }
        /*----------------delete-----------------*/

        /*----------------delete-----------------*/
        public function admin_deletemulti(){

            if($this->request->is('post')){
                //pr($this->data);
                $dataids =  $this->data['Indicator']['id'];

                try{
                    if ($this->Indicator->deleteAll(array('Indicatorid' => $dataids))) {
                        $this->_flash(__('Delete Success',true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                    $this->_flash(__('Delete Error', true),'alert alert-warning');
                    $this->redirect(array('action' => 'admin_index'));
                }

            }else{
                $this->_flash(__('Delete Error', true),'alert alert-danger');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------delete-----------------*/

    /*----------------DELETE-----------------*/

	
	private function preProcessData($data) {

		if (isset($data["Indicator"]["inidate"])) {
			$data["Indicator"]["inidate"]=self::ymd2dmy($data["Indicator"]["inidate"]);
		}
		
		if (isset($data["Indicator"]["enddate"])) {
			$data["Indicator"]["enddate"]=self::ymd2dmy($data["Indicator"]["enddate"]);
		}
		
		return $data;
	
	}

	public function admin_values() {
		
		$id=$this->params->params["pass"][0];
		
		$indicator=array();
		if ($id) {
			$this->Indicator->setLanguage();
			$indicator = $this->Indicator->find('all', array('conditions'=>array('Indicator.id'=>$id)));
		}
		$this->set(compact('indicator'));
		
	}

    public function admin_detail() {

        if($this->request->is('ajax')){
            $this->layout = 'ajax';
            $pdf=true;
        }

        $id = $this->params->params['pass'][0];
        $this->Indicator->setLanguage();
        $data = $this->Indicator->findById($id);
        $this->set('pma', Indicator::periodMeasureArray());        
        $this->set('periodic_measure', $data['Indicator']['periodic_measure']);
        $metrica = $this->Indicator->getAdvanceByPeriod(
            $id, 
            $data['Indicator']['calculation_type'], 
            $data['Indicator']['periodic_measure'], 
            $data['Indicator']['period_goal']
        );
        $this->set('metrica', $metrica);
        $this->set('name', $data['Indicator']['name']);

		$lang = $this->Session->read('Config.MyLangVar');
		
		$this->set(compact('data', 'lang', 'pdf'));
        
    }

    public function admin_show() {
        $this->layout = "ajax";
        $pdf = $this->data['pdf'];
        $data = $this->Indicator->findById($this->data['id']);
        $res = $this->Indicator->getSummaryIndicator2($this->data['id']);
        $this->set('data', $data);
        $this->set('res', $res);
        $this->set('pdf', $pdf);
        $this->set('meas', Indicator::periodicMeasureArray());
        $this->set('calc', Indicator::calculationTypeArray());
    }

	public function admin_showgraph() {

		$id=$this->params->params["pass"][0];
		
		$this->Indicator->setLanguage();
		$indicator=$this->Indicator->find("first", array("conditions"=>array("Indicator.id"=>$id), 'recursive'=>-1));
		$indicator_values=$this->Indicatorvalue->find("all", array("conditions"=>array("Indicatorvalue.indicator_id"=>$id), 'recursive'=>-1));
		
		$title=$indicator["Indicator"]["name"]." (".$indicator["Indicator"]["metric"].")";
		$graph_id=$indicator["Indicator"]["graph_id"];
		
		foreach($indicator_values as $iv) {
			$serie[]=$iv["Indicatorvalue"]["date"];
			$data[]=$iv["Indicatorvalue"]["value"];
		}
		
		$lang = $this->Session->read('Config.MyLangVar');
		
		$calc = Indicator::calculationTypeArray();
		$meas = Indicator::periodicMeasureArray();

		
		$this->set(compact('id', 'serie', 'data', 'title', 'graph_id', 'lang', 'indicator', 'calc', 'meas'));
	
	}
	
}
