<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class UsersController extends AppController {


  public function beforeFilter() {

	// cambio de idioma
	if (isset($this->params->url) and ($this->params->url=="admin/users/home" or $this->params->url=="admin/users/login") and
		isset($this->params->query["lang"]) and in_array($this->params->query["lang"], $this->getlocalesValidates())) {
		$this->Session->write('Config.MyLangVar', $this->params->query["lang"]);
	}
	
	parent::beforeFilter();
		
  }

  public function getConditionsRootGroup($conditions){
  
        if(!$this->isRoot()){
//                $conditions['Group.id <>'] = 1;

			$company_id=-1;
			if ($this->Auth->user('company_id')) {
				$company_id=$this->Auth->user('company_id');
			}
		
			// Si no es root y es un usuario de tipo gestion entones sólo permite crear perfiles de usuario de la compañía 
			// y de tipo grupo de interes: 11
			if (in_array($this->Session->read('groupId'), array(6,7,8))) {
			
				$conditions['Group.id'] = 11;
			
			// Si es un administrador entonces crea cualquier usuario de tipo 7,8,10,11
			} elseif ($this->Session->read('groupId')==6) {
			
				$conditions['Group.id'] = array(7,8,10,11);
				
			// Si es un administrador CSS entonces crea cualquier usuario menos root
			} elseif ($this->Session->read('groupId')==9) {

				$conditions['Group.id <>'] = 1;
				
			// En cualquier otro caso acceso no autorizado
			} else {
				throw new Exception ("Acceso no autorizado");
			}
		
        }
		
        return $conditions;
    }

    public function getConditionsRootUser($conditions){
	
        if(!$this->isRoot()){
		
			$company_id=-1;
			if ($this->Auth->user('company_id')) {
				$company_id=$this->Auth->user('company_id');
			}
		
			// Si no es root y es un usuario de tipo gestion entones sólo permite crear perfiles de usuario de la compañía 
			// y de tipo grupo de interes: 11
			if (in_array($this->Session->read('groupId'), array(6,7,8))) {
			
				$conditions['User.company_id'] = $company_id;
				$conditions['User.group_id'] = 11;
			
			// Si es un administrador entonces crea cualquier usuario de tipo 7,8,10,11
			} elseif ($this->Session->read('groupId')==6) {
			
				$conditions['User.company_id'] = $company_id;
				$conditions['User.group_id'] = array(7,8,10,11);
				
			// Si es un administrador CSS entonces crea cualquier usuario menos root
			} elseif ($this->Session->read('groupId')==9) {

				$conditions['User.group_id <>'] = 5;
				
			// En cualquier otro caso acceso no autorizado
			} else {
				throw new Exception ("Acceso no autorizado");
			}
			
        }

        return $conditions;
    }

    public function admin_initDB() {
        $this->syncACL();
        $this->redirect(array('action' => 'admin_home'));
    }


    public function paramFilters($urlform){

            $form_config = array();
            $form_config["title"] = __("Search / Filter");
            $form_config["urlform"] = $urlform;
            $form_config["labelbutton"] = __("Search / Filter");
            $this->set('form_config',$form_config);

            $fields_char = array(
                        'name'
            );


            $conditions = array();
            $conditions = $this->getConditionsRootGroup($conditions);

            $this->set(
                    array(
                        "groups" => $this->Group->find("list",array('conditions'=>$conditions)),
                        "companies" => $this->Company->find("list"),
                        "corporations" => $this->Corporation->find("list")
                    )
            );

            $conditions = $this->filterConfig('User',$fields_char);
            $this->recordsforpage();

            return $conditions;

        }


    /*----------------INDEX-----------------*/

          /*----------------get_index-----------------*/
          public function get_index($urlfilter = 'admin_index'){
		  
				$conditions = $this->paramFilters($urlfilter);

				$conditions = $this->getConditionsRootUser($conditions);

              //pr($conditions);
              $limit = $this->Session->read('Filter.recordsforpage');
              $this->Paginator->settings = array(
                  'limit' => $limit,
                  'order' => 'User.id ASC',
                  'conditions' => $conditions,
                  'recursive'=>1,
              );
              $lists = $this->Paginator->paginate('User');
              $this->set(compact('lists'));
          }
          /*----------------get_index-----------------*/

          /*----------------index-----------------*/
          public function admin_index(){

              if($this->request->is('ajax')){
                  $this->layout = 'ajax';
              }

              if ($this->request->is('get')) {
                  $this->get_index();
              }
          }
          /*----------------index-----------------*/

      /*----------------INDEX-----------------*/



      /*----------------ADD-----------------*/

          /*----------------post_add-----------------*/
          public function post_add(){

              $this->ajaxVariablesInit();

              $this->User->create();
              $this->User->set($this->data);

              if($this->User->validates())
              {
                      $this->User->create();

                      $data['User'] = $this->data['User'];
                      if(!empty($this->data['User']['password'])){
                          $data['User']['password']= AuthComponent::password($this->data['User']['password']);
                      }

                      $this->User->set($data);
					  
			  if ($this->data['User']['company_id'] or $this->data['User']['corporation_id']) {
        	                $this->User->set('company_id', $this->data['User']['company_id']);
                	        $this->User->set('corporation_id', $this->data['User']['corporation_id']);
                        	$this->User->set('interestgroup_id',$this->Auth->user('interestgroup_id'));
			  } elseif(!$this->isRoot()) {
                	        $this->User->set('company_id',$this->Auth->user('company_id'));
                        	$this->User->set('corporation_id',$this->Auth->user('corporation_id'));
	                        $this->User->set('interestgroup_id',$this->Auth->user('interestgroup_id'));
			  }
                      try{
                          if ($this->User->save()) {
                              $this->dataajax['response']['method']=$this->getMethod();
                              $this->dataajax['response']['message_success']=__('Save Success',true);
                          } 
                      }catch (Exception $e) {  
						  
						  pr($e);
						                         
                              $this->dataajax['response']['message_error']=__('Save Error',true);
                      }
              }else{
                   $this->errorsajax['User'] = $this->User->validationErrors;
                  $this->dataajax['response']["errors"]= $this->errorsajax;
              }

              echo json_encode($this->dataajax);
              die();


          }
          /*----------------post_add-----------------*/


          /*----------------add-----------------*/
          public function admin_add() {

              $form_config = array();
              $form_config["title"] = __("Add")." ".__("User");
              $form_config["urlform"] = "admin_add";
              $form_config["labelbutton"] = __("Add");
              $form_config["type"] = 'post';
               
              $this->set('form_config',$form_config);

              $roles = array();
              $roles['Colaborador'] = __("Colaborator");
              $roles['Editor'] = __("Editor");
              $roles['Visitantes'] = __("Visitor");

              $this->set('roles', $roles);

              if ($this->request->is('post')) {
                  $this->post_add();
              }

              if($this->request->is('ajax')){
                  $this->layout = 'ajax';
              }

              $conditions = array();
              $conditions = $this->getConditionsRootGroup($conditions);
			  
			  
              $groups = $this->Group->find('list',array(
                      'fields'=>array(
                          'id','name'
                      ),
                      'conditions'=>$conditions
              ));

              $companies = $this->Company->find('list',array(
                      'fields'=>array(
                          'id','name'
                      )
              ));
			  
              $corporations = $this->Corporation->find('list',array(
                      'fields'=>array(
                          'id','name'
                      )
              ));

              $is_root = ($this->isRoot() or $this->Auth->user()["Group"]["id"]==9);

			  $interest_groups = $this->Node->getNodes(Node::TYPE_INTEREST, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"], true);
			  
              $this->set(compact('groups','companies','corporations','is_root', 'interest_groups'));

          }
          /*----------------add-----------------*/

      /*----------------ADD-----------------*/


      /*----------------EDIT-----------------*/

          /*----------------get_edit-----------------*/
          public function get_edit($id){

              $this->User->id = $id;

              if (!$this->User->exists()) {
                  $this->_flash(__('No-exist-record',true),'alert alert-warning');
                  $this->redirect(array('action' => 'admin_edit'));

              }else{

                  if(!$this->isRoot()){

                      $user = $this->User->find('first',array(
                          'conditions'=>array(
                              'User.id'=>$id
                          ),
                          'recursive'=>-1
                      ));

                      if($user['User']['group_id'] == 1){
                          $this->_flash(__('No-delete-root', true),'alert alert-danger');
                          $this->redirect(array('action' => 'admin_index'));
                      }
                  }
                  
				$is_root = ($this->isRoot() or $this->Auth->user()["Group"]["id"]==9);

				$datamodel = $this->User->read(null, $id);
				$this->request->data =  $datamodel;

				$this->set(compact('id', 'is_root'));
				
              }

          }
          /*----------------get_edit-----------------*/

          /*----------------post_edit-----------------*/
          public function post_edit($id){

                  $this->ajaxVariablesInit();

                  $this->User->create();
                  $this->User->set($this->data);
                  $password_update = $this->data['User']['password_update'];
                  if(!empty($password_update)){
                      $this->User->set('password',$this->User->get_password_hash($password_update));
                  }

                  if($this->User->validates())
                  {
                      $this->User->id = $id;

                      if(!$this->isRoot()){

                          $user = $this->User->find('first',array(
                              'conditions'=>array(
                                  'User.id'=>$id
                              ),
                              'recursive'=>-1
                          ));

                          if($user['User']['group_id'] == 1){
                              $this->_flash(__('No-delete-root', true),'alert alert-danger');
                              $this->redirect(array('action' => 'admin_index'));
                          }
                      }

					  if ($this->data['User']['company_id'] or $this->data['User']['corporation_id']) {
                        $this->User->set('company_id', $this->data['User']['company_id']);
                        $this->User->set('corporation_id', $this->data['User']['corporation_id']);
                        $this->User->set('interestgroup_id',$this->Auth->user('interestgroup_id'));
					  } elseif(!$this->isRoot()) {
                        $this->User->set('company_id',$this->Auth->user('company_id'));
                        $this->User->set('corporation_id',$this->Auth->user('corporation_id'));
                        $this->User->set('interestgroup_id',$this->Auth->user('interestgroup_id'));
					  }

                      try{
                          if ($this->User->save()) {
                              $this->dataajax['response']['method']=$this->getMethod();
                               $this->dataajax['response']['message_success']=__('Update Success',true);
                          }
                      }catch (Exception $e) {
                          $this->dataajax['response']['message_error']=__('Update Error',true);
                      }
                  }else{
                        $this->errorsajax['User'] = $this->User->validationErrors;
                        $this->dataajax['response']["errors"]= $this->errorsajax;
                  }

                  echo json_encode($this->dataajax);
                  die();
          }
          /*----------------post_edit-----------------*/

          /*----------------edit-----------------*/
          public function admin_edit($id=null){

               if($this->request->is('ajax')){
                  $this->layout = 'ajax';
              }

              $form_config = array();
              $form_config["title"] = __("Edit")." ".__("User");
              $form_config["urlform"] = "admin_edit";
              $form_config["labelbutton"] = __("Save");
              $form_config["type"] = 'put';
              $this->set('form_config',$form_config);

              $roles = array();
              $roles['Colaborador'] = __("Colaborator");
              $roles['Editor'] = __("Editor");
              $roles['Visitantes'] = __("Visitor");
              $this->set('roles', $roles);

              $conditions = array();
              $conditions = $this->getConditionsRootGroup($conditions);
              $groups = $this->Group->find('list',array(
                      'fields'=>array(
                          'id','name'
                      ),
                      'conditions'=>$conditions
              ));

              $companies = $this->Company->find('list',array(
                      'fields'=>array(
                          'id','name'
                      )
              ));

              $corporations = $this->Corporation->find('list',array(
                      'fields'=>array(
                          'id','name'
                      )
              ));

              $is_root = $this->isRoot();

			  $interest_groups = $this->Node->getNodes(Node::TYPE_INTEREST, $this->Auth->user()["company_id"], $this->Auth->user()["corporation_id"], true);
			  
              $this->set(compact('groups','companies','corporations','is_root', 'interest_groups'));

              if ($this->request->is('get')) {
                  if(empty($id)){
                      $this->get_index('admin_edit');
                  }else{
                      $this->get_edit($id);
                  }
              }else{
                  if ($this->request->is('put')) {
                      $this->post_edit($id);
                  }
              }


          }
          /*----------------edit-----------------*/

      /*----------------EDIT-----------------*/


      /*----------------DELETE-----------------*/

          /*----------------delete-----------------*/
          public function admin_delete($id=null){

              if($this->request->is('ajax')){
                  $this->layout = 'ajax';
              }

              $groups = $this->Group->find('list',array(
                      'fields'=>array(
                          'id','name'
                      )
              ));

              $this->set(compact('groups'));

              if(!empty($id)){
                  $this->User->id = $id;
                  if (!$this->User->exists()) {
                      $this->_flash(__('No-exist-record', true),'alert alert-danger');
                      $this->redirect(array('action' => 'admin_index'));
                  }

                  if(!$this->isRoot()){

                          $user = $this->User->find('first',array(
                              'conditions'=>array(
                                  'User.id'=>$id
                              ),
                              'recursive'=>-1
                          ));

                          if($user['User']['group_id'] == 1){
                              $this->_flash(__('No-delete-root', true),'alert alert-danger');
                              $this->redirect(array('action' => 'admin_index'));
                          }
                  }

                  try{
                      if ($this->User->delete($id,true)) {
                          $this->_flash(__('Delete Success', true),'alert alert-success');
                          $this->redirect(array('action' => 'admin_index'));
                      }
                  }catch (Exception $e) {
                          $this->_flash(__('Delete Error', true),'alert alert-warning');
                          $this->redirect(array('action' => 'admin_index'));
                  }
              }else{
                  $this->get_index('admin_index');
                  $this->redirect(array('action' => 'admin_index'));
              }


          }
          /*----------------delete-----------------*/

          /*----------------delete-----------------*/
          public function admin_deletemulti(){

              if($this->request->is('post')){
                  //pr($this->data);
                  $dataids =  $this->data['User']['id'];

                  try{
                      if ($this->User->deleteAll(array('User.id' => $dataids))) {
                          $this->_flash(__('Delete-success-multi',true),'alert alert-success');
                          $this->redirect(array('action' => 'admin_index'));
                      }
                  }catch (Exception $e) {
                      $this->_flash(__('Delete-error-multi', true),'alert alert-warning');
                      $this->redirect(array('action' => 'admin_index'));
                  }

              }else{
                  $this->_flash(__('Delete-error-multi-request', true),'alert alert-danger');
                  $this->redirect(array('action' => 'admin_index'));
              }

          }
          /*----------------delete-----------------*/


      /*----------------DELETE-----------------*/


      public function post_resetpassword(){

          $this->ajaxVariablesInit();

          $this->User->create();
          $this->User->id = $this->Auth->user('id');
          $this->User->set($this->data);

          if($this->User->validates())
          {
                  $data['User']['password']= AuthComponent::password($this->data['User']['new_password']);
                  $this->User->id = $this->Auth->user('id');
                  $this->User->set($data);
                  try{
                      if ($this->User->save()) {
                          $this->dataajax['response']['message_success']=__('Update Success',true);
                      }
                  }catch (Exception $e) {
                          $this->dataajax['response']['message_error']=__('Save Error',true);
                  }


          }else{
              $this->errorsajax['User'] = $this->User->validationErrors;
              $this->dataajax['response']["errors"]= $this->errorsajax;
          }

          echo json_encode($this->dataajax);
          die();

      }

      public function admin_resetpassword(){

              $form_config = array();
              $form_config["title"] = __("Password Reset");
              $form_config["urlform"] = "admin_resetpassword";
              $form_config["labelbutton"] = __("Password Reset");
              $this->set('form_config',$form_config);

              if ($this->request->is('post')) {
                  $this->post_resetpassword();
              }

      }




    public function admin_home($category_id = null) {	
	
        if(isset($category_id)){
            $this->setSidebarMenu($category_id);
        }
		
		$groupId=$this->Session->read('groupId');
		$this->set(compact('groupId'));
		
    }



  public function get_login(){

        if($this->Auth->user('id')){
            $this->redirect(array('action' => 'admin_home'));
        }

  }

  public function post_login(){

		$this->ajaxVariablesInit();
		$this->User->set($this->data);

		// Validación de la empresa y corporación
		
		if($this->User->validates())
		{
			if( $this->Auth->login())
			{
			
				$result = $this->User->find('first', array(
					'conditions' => array(
						'User.id' => AuthComponent::user('id')
					),
					'fields' => 'User.group_id, User.active'
				));
			
				if (!$result['User']["active"]) {
					$this->Session->destroy();
					$this->dataajax['response']['message_error']=__('User / Password is not valid, please check! 1',true);
					echo json_encode($this->dataajax);
					die();
				}

                if (strlen($this->Auth->user('company_id')) > 0) {
                    $cmp = $this->Company->findById($this->Auth->user('company_id'));
                    if ($cmp['Company']['eng'] xor $cmp['Company']['esp']) {
                        if ($cmp['Company']['eng']) {
                            $this->Session->write('Config.MyLangVar', 'eng');
                            $this->Session->write('Config.LangVar', 'eng');            
                        } else {
                            $this->Session->write('Config.MyLangVar', 'esp');
                            $this->Session->write('Config.LangVar', 'esp');
                        }
                    }
                }
                
                
			
				$this->Session->write('groupId', $result['User']['group_id']);
				
				if ($this->Auth->user('company_id')) {
					
					$company = $this->Company->find('first',array(
					  'conditions'=>array(
						  'Company.id'=>$this->Auth->user('company_id')
					  ),
					  'recursive'=>-1
					));
					
					if (!$company["Company"]["active"]) {
						$this->dataajax['response']['message_error']=__('User / Password is not valid, please check! 2',true);
						echo json_encode($this->dataajax);
						die();
					}
					
					$this->Session->write('TheCompany', $company);
					
				}
					
				if ($this->Auth->user('corporation_id')) {
					
					$corporation = $this->Corporation->find('first',array(
					  'conditions'=>array(
						  'Corporation.id'=>$this->Auth->user('corporation_id')
					  ),
					  'recursive'=>-1
					));
					
					if (!$corporation["Corporation"]["active"]) {
						$this->dataajax['response']['message_error']=__('User / Password is not valid, please check! 3',true);
						echo json_encode($this->dataajax);
						die();
					}
					
					$this->Session->write('TheCorporation', $corporation);
					
				}
					

				$result = $this->User->find('first', array(
					'conditions' => array(
						'User.id' => AuthComponent::user('id')
					),
					'fields' => 'User.group_id' //CHANGE THIS
				));
				$this->Session->write('groupId', $result['User']['group_id']);
				$this->dataajax['response']['redirect']='/admin/users/home/';	
                if (strlen($this->Auth->redirect()) > 14) {
                    $this->dataajax['response']['redirect']=$this->Auth->redirect();
                }
				
			}
			else
			{
				$this->dataajax['response']['message_error']=__('User / Password is not valid, please check! 4',true);
			}
		}else{
			 $this->errorsajax['User'] = $this->User->validationErrors;
			 $this->dataajax['response']["errors"]= $this->errorsajax;
		}
			
		echo json_encode($this->dataajax);
		die();


    }

  public function admin_login() {
    $this->layout = 'login';
    if ($this->request->is('post')) {
          $this->post_login();
      }else{

          if ($this->request->is('get')) {
              $this-> get_login();
          }

    }

  }

  public function admin_logout() {
        $this->Session->destroy();
        $this->_flash(__('Close Session Success',true),'alert alert-success');
        $this->redirect($this->Auth->logout());
    }


}
