<?php
App::uses('AppController', 'Controller');
/**
 * Companyquestionnaires Controller
 *
 * @property Companyquestionnaire $Companyquestionnaire
 */
class CompanyquestionnairesController extends AppController {

	/*----------------beforeFilter-----------------*/
    public function beforeFilter() {
        parent::beforeFilter();
    }
    /*----------------beforeFilter-----------------*/

    public function paramFilters($urlform){

            $form_config = array();
            $form_config["title"] = __("Search / Filter");
            $form_config["urlform"] = $urlform;
            $form_config["labelbutton"] = __("Search / Filter");
            $this->set('form_config',$form_config);

            $fields_char = array();

            $conditions = $this->filterConfig('Companyquestionnaire',$fields_char);
            $this->recordsforpage();

            return $conditions;

        }


    /*----------------INDEX-----------------*/

        /*----------------get_index-----------------*/
        public function get_index($urlfilter = 'admin_index'){
            $conditions = $this->paramFilters($urlfilter);

            $limit = $this->Session->read('Filter.recordsforpage');

            $this->Paginator->settings = array(
                'conditions' => $conditions,
                'order' => 'Companyquestionnaire.id ASC',
                'limit' => $limit,
				'recursive' => 1
            );

            $lists = $this->Paginator->paginate('Companyquestionnaire');
	
            $all_companies = $this->Company->find("list");
            $all_questionnaires = $this->Questionnaire->find("list");
            $this->set(compact('lists', 'all_companies', 'all_questionnaires'));

        }
        /*----------------get_index-----------------*/

        /*----------------index-----------------*/
        public function admin_index(){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if ($this->request->is('get')) {
                $this->get_index();
            }
        }
        /*----------------index-----------------*/

    /*----------------INDEX-----------------*/


    /*----------------ADD-----------------*/

        /*----------------post_add-----------------*/
        public function post_add(){

                $this->ajaxVariablesInit();

                $this->Companyquestionnaire->create();
                $this->Companyquestionnaire->set($this->data);

                if($this->Companyquestionnaire->validates())
                {

					try{
						if ($this->Companyquestionnaire->save()) {
							 $this->dataajax['response']['method']=$this->getMethod();
							$this->dataajax['response']['message_success']=__('Save Success',true);
						}
					}catch (Exception $e) {
						$this->dataajax['response']['message_error']=__('Save Error',true);
					}

                }else{
                     $this->errorsajax['Companyquestionnaire'] = $this->Companyquestionnaire->validationErrors;
                     $this->dataajax['response']["errors"]= $this->errorsajax;
                }

                echo json_encode($this->dataajax);
                die();
        }
        /*----------------post_add-----------------*/

        /*----------------add-----------------*/
        public function admin_add() {
            $form_config = array();
            $form_config["title"] = __("Add Company/Questionnaire");
            $form_config["urlform"] = "admin_add";
            $form_config["labelbutton"] = __("Add");
            $form_config["type"] = 'post';
            $this->set('form_config',$form_config);

            $all_companies = $this->Company->find("list");
            $all_questionnaires = $this->Questionnaire->find("list");

            if ($this->request->is('post')) {
                $this->post_add();
            }

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            $this->set(compact('lists', 'all_companies', 'all_questionnaires'));

        }
        /*----------------add-----------------*/

    /*----------------ADD-----------------*/

    /*----------------DELETE-----------------*/

        /*----------------delete-----------------*/
        public function admin_delete($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if(!empty($id)){
                $this->Companyquestionnaire->id = $id;
                if (!$this->Companyquestionnaire->exists()) {
                    $this->_flash(__('No-exist-record', true),'alert alert-danger');
                    $this->redirect(array('action' => 'admin_index'));
                }

                if(!$this->isRoot()){
                    if($id == 1){
                        $this->_flash(__('No-delete-root', true),'alert alert-danger');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }

                try{
                    if ($this->Companyquestionnaire->delete($id,true)) {
                        $this->_flash(__('Delete Success', true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                        $this->_flash(__('Delete Error', true),'alert alert-warning');
                        $this->redirect(array('action' => 'admin_index'));
                }

            }else{
                $this->get_index('admin_index');
                $this->redirect(array('action' => 'admin_index'));

            }

        }
        /*----------------delete-----------------*/


        /*----------------deletemulti-----------------*/
        public function admin_deletemulti(){

            if($this->request->is('post')){
                //pr($this->data);
                $dataids =  $this->data['Companyquestionnaire']['id'];

                try{
                    if ($this->Companyquestionnaire->deleteAll(array('Companyquestionnaire.id' => $dataids))) {
                        $this->_flash(__('Delete-success-multi',true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                    $this->_flash(__('Delete-error-multi', true),'alert alert-warning');
                    $this->redirect(array('action' => 'admin_index'));
                }

            }else{
                $this->_flash(__('Delete-error-multi-request', true),'alert alert-danger');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------deletemulti-----------------*/

    /*----------------DELETE-----------------*/

}
