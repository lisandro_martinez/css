<?php
App::uses('AppController', 'Controller');
/**
 * Groupactions Controller
 *
 * @property Groupaction $Groupaction
 */
class GroupactionsController extends AppController {

	/*----------------beforeFilter-----------------*/
    public function beforeFilter() {
        parent::beforeFilter();
    }
    /*----------------beforeFilter-----------------*/

    public function paramFilters($urlform){

            $form_config = array();
            $form_config["title"] = __("Search / Filter");
            $form_config["urlform"] = $urlform;
            $form_config["labelbutton"] = __("Search / Filter");
            $this->set('form_config',$form_config);

            $fields_char = array();

            $conditions = $this->filterConfig('Groupaction',$fields_char);
            $this->recordsforpage();

            return $conditions;

        }


    /*----------------INDEX-----------------*/

        /*----------------get_index-----------------*/
        public function get_index($urlfilter = 'admin_index'){
            $conditions = $this->paramFilters($urlfilter);

            $conditionsGroups = array();
            if(!$this->isRoot()){
                $conditions['Group.id <>'] = 1;
                $conditionsGroups['Group.id <>'] = 1;
            }

            //pr($conditions);
            $limit = $this->Session->read('Filter.recordsforpage');

            $this->Paginator->settings = array(
                'conditions' => $conditions,
                'order' => 'Groupaction.id ASC',
                'limit' => $limit
            );

            $funciones= array();
            $funciones_all = $this->Action->find("all",array(
                'recursive'=>1
            ));

            foreach ($funciones_all as $keyfuncion => $valuefuncion) {
                $funciones[$valuefuncion['Action']['id']]= $valuefuncion['Categories']['name'] .' - '.$valuefuncion['Action']['name'];
            }

            $this->set(
                    array(
                        "groups" => $this->Group->find("list",array('conditions'=>$conditionsGroups)),
                        "actions" => $funciones
                    )
            );

            $lists = $this->Paginator->paginate('Groupaction');
            $categories = $this->Category->find("list");
            $this->set(compact('lists','categories'));
        }
        /*----------------get_index-----------------*/

        /*----------------index-----------------*/
        public function admin_index(){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if ($this->request->is('get')) {
                $this->get_index();
            }
        }
        /*----------------index-----------------*/

    /*----------------INDEX-----------------*/


    /*----------------ADD-----------------*/

        /*----------------post_add-----------------*/
        public function post_add(){

                $this->ajaxVariablesInit();

                $this->Groupaction->create();
                $this->Groupaction->set($this->data);

                if($this->Groupaction->validates())
                {

                    $checkaddroot=false;
                    if(!$this->isRoot()){
                        if($this->data['Groupaction']['group_id'] == 1){
                            $this->dataajax['response']['message_error']=__('Save Error Root',true);
                            $checkaddroot=true;
                        }
                    }

                    if(!$checkaddroot){
                        try{
                            if ($this->Groupaction->save()) {
                                 $this->dataajax['response']['method']=$this->getMethod();
                                $this->dataajax['response']['message_success']=__('Save Success',true);
                            }
                        }catch (Exception $e) {
                            $this->dataajax['response']['message_error']=__('Save Error',true);
                        }
                    }


                }else{
                     $this->errorsajax['Groupaction'] = $this->Groupaction->validationErrors;
                     $this->dataajax['response']["errors"]= $this->errorsajax;
                }

                echo json_encode($this->dataajax);
                die();
        }
        /*----------------post_add-----------------*/

        /*----------------add-----------------*/
        public function admin_add() {
            $form_config = array();
            $form_config["title"] = "Agregar Permisos";
            $form_config["urlform"] = "admin_add";
            $form_config["labelbutton"] = "Agregar";
            $form_config["type"] = 'post';
            $this->set('form_config',$form_config);

            $conditionsGroups= array();
            if(!$this->isRoot()){
                $conditionsGroups['Group.id <>'] = 1;
            }


            $funciones= array();
            $funciones_all = $this->Action->find("all",array(
                'recursive'=>1,
                'order'=>'Action.id ASC'
            ));

            foreach ($funciones_all as $keyfuncion => $valuefuncion) {
                $funciones[$valuefuncion['Action']['id']]= $valuefuncion['Categories']['name'] .' - '.$valuefuncion['Action']['name'];
            }

        	$this->set(
                	array(
                		"groups" => $this->Group->find("list",array('conditions'=>$conditionsGroups,'order'=>'Group.id ASC')),
                		"actions" => $funciones
                	)
            );

            if ($this->request->is('post')) {
                $this->post_add();
            }

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

        }
        /*----------------add-----------------*/

    /*----------------ADD-----------------*/

    /*----------------DELETE-----------------*/

        /*----------------delete-----------------*/
        public function admin_delete($id=null){

            if($this->request->is('ajax')){
                $this->layout = 'ajax';
            }

            if(!empty($id)){
                $this->Groupaction->id = $id;
                if (!$this->Groupaction->exists()) {
                    $this->_flash(__('No-exist-record', true),'alert alert-danger');
                    $this->redirect(array('action' => 'admin_index'));
                }

                if(!$this->isRoot()){
                    if($id == 1){
                        $this->_flash(__('No-delete-root', true),'alert alert-danger');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }

                try{
                    if ($this->Groupaction->delete($id,true)) {
                        $this->_flash(__('Delete Success', true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                        $this->_flash(__('Delete Error', true),'alert alert-warning');
                        $this->redirect(array('action' => 'admin_index'));
                }

            }else{
                $this->get_index('admin_index');
                $this->redirect(array('action' => 'admin_index'));

            }

        }
        /*----------------delete-----------------*/


        /*----------------deletemulti-----------------*/
        public function admin_deletemulti(){

            if($this->request->is('post')){
                //pr($this->data);
                $dataids =  $this->data['Groupaction']['id'];

                try{
                    if ($this->Groupaction->deleteAll(array('Groupaction.id' => $dataids))) {
                        $this->_flash(__('Delete-success-multi',true),'alert alert-success');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                }catch (Exception $e) {
                    $this->_flash(__('Delete-error-multi', true),'alert alert-warning');
                    $this->redirect(array('action' => 'admin_index'));
                }

            }else{
                $this->_flash(__('Delete-error-multi-request', true),'alert alert-danger');
                $this->redirect(array('action' => 'admin_index'));
            }

        }
        /*----------------deletemulti-----------------*/

    /*----------------DELETE-----------------*/


    /*----------------ACL-----------------*/
    public function admin_acl($access = null , $aro = null, $aco = null, $id =null){

        if((isset($access))&&(isset($aro))&&(isset($aco))){

            $this->ajaxVariablesInit();

            $this->ArosAcos->create();
            if(!empty($id)){
                $this->ArosAcos->id = $id;
            }

            if($access == 0){ $access ='1';}else{$access ='-1';}

            $this->ArosAcos->set('aro_id' , $aro);
            $this->ArosAcos->set('aco_id' , $aco);
            $this->ArosAcos->set('_create' , $access);
            $this->ArosAcos->set('_read', $access);
            $this->ArosAcos->set('_update', $access);
            $this->ArosAcos->set('_delete', $access);

            if ($this->ArosAcos->save()) {
                //$this->_flash(__('Save-success',true),'alert alert-success');
                //$this->redirect(array('action' => 'admin_acl'));
                $this->dataajax['response']['message_success']=__('Save-success',true);
            } else {
                //$this->_flash(__('Save-error',true),'alert alert-warning');
                $this->dataajax['response']['message_error']=__('Save-error',true);
            }

            echo json_encode($this->dataajax);
            die();

        }

        $acos = $this->Aco->find('all',array(
            'recursive'=>-1
        ));

        $aros = $this->Aro->find('all',array(
            'conditions'=>array(
                'model' =>'Group'
            )
        ));


        $accessgroup = array();
        foreach ($aros as $aro){
            $aroid = $aro['Aro']['id'];
            foreach ($aro['Aco'] as $aco){

                $idaco = $aco['id'];

                $acopermission= $aco['Permission'];

                if($acopermission['_create']){

                    $accessgroup[$aro['Aro']['foreign_key']]['idpermission-'.$idaco]=$acopermission['id'];
                    $accessgroup[$aro['Aro']['foreign_key']][$idaco]=$acopermission['_create'];
                }

            }
        }



        $groups = $this->Group->find('all',
            array(
                'joins' => array(
                    array(
                        'table' => 'aros',
                        'alias' => 'Aros',
                        'type' => 'inner',
                        'conditions' => array(
                            'Group.id = Aros.foreign_key',
                            'Aros.model' =>'Group'
                        )
                    )
                ),
                'fields' =>array('*')
            )
        );

        $this->set(compact('acos','groups','accessgroup'));

    }
     /*----------------ACL-----------------*/


}
