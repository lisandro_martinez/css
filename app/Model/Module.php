<?php
App::uses('AppModel', 'Model');
/**
 * Module Model
 *
 */


class Module extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
            'name' => 'ModulenameTranslation' /* Modelo+Campo+Translation */
        )
    );

    public $locale = 'esp';

    public $validate = array(
        'name' => array(
					'notempty' => array(
						'rule' => array('notempty'),
						'message' => 'This field can not be empty',
					),
		),
		'order' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty',
			),
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'This field can be numeric',
			),
		),
    );

    public function parentNode() {
        return null;
    }

}
