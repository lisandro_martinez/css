<?php
App::uses('AppModel', 'Model');
/**
 * Nodetype Model
 *
 */


class Nodetype extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
            'name' => 'NodetypenameTranslation' /* Modelo+Campo+Translation */
        )
    );

    public $locale = 'esp';

    public $validate = array(
        'name' => array(
					'notempty' => array(
						'rule' => array('notempty'),
						'message' => 'This field can not be empty',
					),
		),
    );

    public function parentNode() {
        return null;
    }

}
