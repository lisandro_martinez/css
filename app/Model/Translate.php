<?php
App::uses('AppModel', 'Model');
/**
 * Master Model
 *
 * @property Value $Value
 */
class Translate extends AppModel {
   public $useTable = 'i18n';
}