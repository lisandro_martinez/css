<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 * @property Modules $Modules
 */
class Section extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
            'name' => 'SectionnameTranslation', /* Modelo+Campo+Translation */
         )
    );

    public $locale = 'esp';
	
    public $belongsTo = array(
        'Questionnaire' => array(
            'className' => 'Questionnaires',
            'foreignKey' => 'questionnaire_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty',
			),
		),
		'order' => array(
				'rule' => 'numeric',
				'message' => 'Please suply a number',
		),
        'questionnaire_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty',
			),
        ),
	);

	 public function parentNode() {
	 	return null;
    }
}
