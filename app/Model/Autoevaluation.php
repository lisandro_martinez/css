<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 * @property Modules $Modules
 */
class Autoevaluation extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
    );

    public $locale = 'esp';

    public $belongsTo = array(
        'Questionnaire' => array(
            'className' => 'Questionnaires',
            'foreignKey' => 'questionnaire_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'Users',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Company' => array(
            'className' => 'Companies',
            'foreignKey' => 'company_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Corporation' => array(
            'className' => 'Corporations',
            'foreignKey' => 'corporation_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

	public $displayField = 'questionnaire.name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'user_id' => array(
				'rule' => 'numeric',
				'message' => 'Please suply a number',
		),
		'company_id' => array(
				'rule' => 'numeric',
				'message' => 'Please suply a number',
		),
		'questionnaire_id' => array(
				'rule' => 'numeric',
				'message' => 'Please suply a number',
		),
		'total_points' => array(
				'rule' => 'numeric',
				'message' => 'Please suply a number',
		),
	);

	 public function parentNode() {
	 	return null;
    }
}
