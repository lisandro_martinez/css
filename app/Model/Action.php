<?php
App::uses('AppModel', 'Model');
/**
 * Action Model
 *
 * @property Categories $Categories
 */
class Action extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public $actsAs = array(
         'Translate' => array(
            'name' => 'ActionnameTranslation'
        )
    );

	public $locale = 'esp';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty',
			),
		),
		'url' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty',
			),
		),
		'order' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty',
			),
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'This field can be numeric',
			),
		),
		'category_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Select one option in this list',
			),
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'This field can be numeric',
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Categories' => array(
			'className' => 'Categories',
			'foreignKey' => 'category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);


}
