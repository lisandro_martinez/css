<?php
App::uses('AppModel', 'Model');
/**
 * Interestgroupcall Model
 *
 */

class Interestgroupcall extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
    );

    public $validate = array(
        'interestgroup_id' => array(
					'notempty' => array(
						'rule' => array('notempty'),
						'message' => 'This field can not be empty',
					),
		),
        'call_id' => array(
					'notempty' => array(
						'rule' => array('notempty'),
						'message' => 'This field can not be empty',
					),
		),
    );

    public function parentNode() {
        return null;
    }
	
	public function deletecustom($i1, $i2) {
		if (is_numeric($i1) and is_numeric($i2)) {
			App::uses('ConnectionManager', 'Model'); 
			$db = ConnectionManager::getDataSource('default');
			if ($db->isConnected()) {
				$db->query("DELETE FROM interestgroupcalls WHERE call_id=$i1 and interestgroup_id=$i2");
			}
		}
	}
	
}
