	<?php
App::uses('AppModel', 'Model');
/**
 * Companyquestionnaire Model
 *
 * @property Group $Group
 * @property Actions $Actions
 */
class Companyquestionnaire extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

	public $locale = 'esp';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'company_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Select one option in this list',
			),
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'This field can be numeric',
			),
		),
		'questionnaire_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Select one option in this list',
			),
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'This field can be numeric',
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Company' => array(
			'className' => 'Companies',
			'foreignKey' => 'company_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Questionnaire' => array(
			'className' => 'Questionnaires',
			'foreignKey' => 'questionnaire_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
