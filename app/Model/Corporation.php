<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 * @property Modules $Modules
 */
class Corporation extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
            'description' => 'CorporationdescriptionTranslation', /* Modelo+Campo+Translation */
			'address' => 'CorporationaddressTranslation'
         )
    );

    public $locale = 'esp';

	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty',
			),
		)
	);

	 public function parentNode() {
	 	return null;
    }
}
