<?php
App::uses('AppModel', 'Model');
/**
 * Report Model
 *
 */

class Report extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
            'name' => 'ReportnameTranslation',
            'year' => 'ReportyearTranslation', 
            'description' => 'ReportdescriptionTranslation'
        )
    );

    public $locale = 'esp';

    public $validate = array(
        'name' => array(
					'notempty' => array(
						'rule' => array('notempty'),
						'message' => 'This field can not be empty',
					),
		),
        'metric' => array(
					'notempty' => array(
						'rule' => array('notempty'),
						'message' => 'This field can not be empty',
					),
		),
		'goal' => array(
				'rule' => 'numeric',
				'message' => 'Please suply a number',
		),
		'cond_value_1' => array(
            'lessThanMyOtherField' => array(
                'rule' => array('comparisonWithField', '<', 'cond_value_2'),
                'message' => 'Inferior Cut Point value has to be lower than Superior Cut Point value',
            ),
			'rule' => array('range', 0, 100),
			'message' => 'Please enter a number between 0 and 100'
        ),
		'cond_value_2' => array(
			'rule' => array('range', 0, 100),
			'message' => 'Please enter a number between 0 and 100'
        ),
    );


    public $belongsTo = array(
		'Company' => array(
			'className' => 'Companies',
			'foreignKey' => 'company_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
	
    public function parentNode() {
        return null;
    }


}
