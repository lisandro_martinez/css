<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 * @property Modules $Modules
 */
class Autoevaluationanswer extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
    );

    public $locale = 'esp';

    public $belongsTo = array(
        'Autoevaluation' => array(
            'className' => 'Autoevaluation',
            'foreignKey' => 'autoevaluation_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Question' => array(
            'className' => 'Questions',
            'foreignKey' => 'question_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Answer' => array(
            'className' => 'Answers',
            'foreignKey' => 'answer_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

	public $displayField = 'autoevaluation.id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'autoevaluation_id' => array(
				'rule' => 'numeric',
				'message' => 'Please suply a number',
		),
		'question_id' => array(
				'rule' => 'numeric',
				'message' => 'Please suply a number',
		),
		'answer_id' => array(
				'rule' => 'numeric',
				'message' => 'Please suply a number',
		),
	);

	 public function parentNode() {
	 	return null;
    }
}
