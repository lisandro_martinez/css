<?php
App::uses('AppModel', 'Model');
/**
 * Node Model
 *
 */


class Nodo extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
            'title' => 'NodotitleTranslation', /* Modelo+Campo+Translation */
			'description' => 'NododescriptionTranslation'
        )
    );

    public $locale = 'esp';

    public $validate = array(
        'title' => array(
					'notempty' => array(
						'rule' => array('notempty'),
						'message' => 'This field can not be empty',
					),
		),
    );

    public function parentNode() {
        return null;
    }

}
