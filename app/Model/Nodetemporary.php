<?php
App::uses('AppModel', 'Model');
/**
 * Nodetemporary Model
 *
 */


class Nodetemporary extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
 public $useTable = 'nodetemporary';

 public function deleteAll()
 {
     $this->query('delete from nodetemporary');
 }

 public function deleteRecord($node_id) 
 {
     if (strlen($node_id) > 0) {
         $this->query('delete from nodetemporary where node_id = ' . $node_id);
     }
     
 }

 public function getNodetypeNameById($nodetype_id)
 {
	 switch($nodetype_id)
	 {
		 case 4:
		 	return "The Commitment";
		 break;
		 case 6:
		 	return "The Vision";
		 break;
		 case 5:
		 	return "The Mission";
		 break;
		 case 3:
		 	return "The Strategy";
		 break;
		 case 7:
		 	return "The Objective";
		 break;
		 case 8:
		 	return "The Politics";
		 break;
		 case 2:
		 	return "The Perspective";
		 break;
	 }
	 return "";
 }

 public function getList()
 {
     $consulta = 'select nt.node_id, nt.nodetype_id, node.name, ntype.name from ' 
        . 'nodetemporary nt join nodes node on (nt.node_id = node.id) '
        . 'join nodetypes ntype on ntype.id=nt.nodetype_id '
        . 'order by nt.order';
     return $this->query($consulta);
 }

 public function getSaveOrder($nodetype)
 {     
     switch ($nodetype) {
					case 2:
						return 2;
						break;
					case 3:
						return 8;					
						break;
					case 4:
						return 7;						
						break;
					case 5:					
						return 3;							
						break;
					case 6:
						return 5;						
						break;					
					case 7:
						return 6;
						break;
					case 8:
						return 4;
						break;
					}
    return 0;
 }

 public function getListOrder($nodetype) {
     switch ($nodetype) {
					case 4:
						return 1;
						break;
					case 6:
						return 2;					
						break;
					case 5:
						return 3;						
						break;
					case 3:					
						return 4;							
						break;
					case 7:
						return 5;						
						break;					
					case 8:
						return 6;
						break;
					case 2:
						return 7;
						break;
					}
    return 0;
 }

	

}
