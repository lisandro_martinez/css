<?php
App::uses('AppModel', 'Model');
/**
 * Indicatorvalue Model
 *
 */


class Indicatorvalue extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
        )
    );

    public $validate = array(
        'value' => array(
			'rule' => 'numeric',
			'message' => 'Please suply a number',
		),
        'indicator_id' => array(
					'notempty' => array(
						'rule' => array('notempty'),
						'message' => 'This field can not be empty',
					),
		),
        'date' => array(
					'notempty' => array(
						'rule' => array('notempty'),
						'message' => 'This field can not be empty',
					),
		),
    );

	public $belongsTo = array(
		'Indicator' => array(
			'className' => 'Indicators',
			'foreignKey' => 'indicator_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
    public function parentNode() {
        return null;
    }

}
