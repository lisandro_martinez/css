<?php
App::uses('AppModel', 'Model');
/**
 * Value Model
 *
 * @property Master $Master
 */
class Value extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

	public $actsAs = array();

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty'
			),
		),
		'optional' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty'
			),
		),
		'master_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty',
			),
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'This field can be numeric',
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Master' => array(
			'className' => 'Master',
			'foreignKey' => 'master_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

	public function parentNode() {
        return null;
    }
}
