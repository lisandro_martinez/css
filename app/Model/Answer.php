<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 * @property Modules $Modules
 */
class Answer extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
            'name' => 'AnswernameTranslation', /* Modelo+Campo+Translation */
            'feedback_title' => 'Answerfeedback_titleTranslation',
            'feedback' => 'AnswerfeedbackTranslation',
         )
    );

    public $locale = 'esp';
	
    public $belongsTo = array(
        'Question' => array(
            'className' => 'Questions',
            'foreignKey' => 'question_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty',
			),
		),
		'points' => array(
				'rule' => 'numeric',
				'message' => 'Please suply a number',
		),
		'order' => array(
				'rule' => 'numeric',
				'message' => 'Please suply a number',
		),
        'question_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Select one option in this list',
            ),
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'This field can be numeric',
            ),
        ),
	);

	 public function parentNode() {
	 	return null;
    }
}
