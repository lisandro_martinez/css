<?php
App::uses('AppModel', 'Model');
/**
 * Indicator Model
 *
 */

class Indicator extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
            'name' => 'IndicatornameTranslation',
            'metric' => 'IndicatormetricTranslation', 
            'description' => 'IndicatordescriptionTranslation'
        )
    );

    public $locale = 'esp';

    public $validate = array(
        'name' => array(
					'notempty' => array(
						'rule' => array('notempty'),
						'message' => 'This field can not be empty',
					),
		),
        'metric' => array(
					'notempty' => array(
						'rule' => array('notempty'),
						'message' => 'This field can not be empty',
					),
		),
		'goal' => array(
				'rule' => 'numeric',
				'message' => 'Please suply a number',
		),
		'cond_value_1' => array(
            'lessThanMyOtherField' => array(
                'rule' => array('comparisonWithField', '<', 'cond_value_2'),
                'message' => 'Inferior Cut Point value has to be lower than Superior Cut Point value',
            ),
			'rule' => array('range', 0, 100),
			'message' => 'Please enter a number between 0 and 100'
        ),
		'cond_value_2' => array(
			'rule' => array('range', 0, 100),
			'message' => 'Please enter a number between 0 and 100'
        ),
    );


	public function bsc($company_id) {
		
		$query = <<<RAWQUERY
					SELECT 	n7.name as n7, n7.nodetype_id as nt7,
							n6.name as n6, n6.nodetype_id as nt6,
							n5.name as n5, n5.nodetype_id as nt5,
							n4.name as n4, n4.nodetype_id as nt4,
							n3.name as n3, n3.nodetype_id as nt3, 
							n2.name as n2, n2.nodetype_id as nt2,
							n1.name as n1, n1.nodetype_id as nt1,
							i.*
					FROM indicators i

						INNER JOIN indicatornodes inn 
						ON i.id=inn.indicator_id

							INNER JOIN nodes n1
							ON inn.node_id=n1.id

								LEFT JOIN nodenodes nn1
								ON n1.id=nn1.node_id
									LEFT JOIN nodes n2
									ON nn1.node_id2=n2.id

										LEFT JOIN nodenodes nn2
										ON n2.id=nn2.node_id
											LEFT JOIN nodes n3
											ON nn2.node_id2=n3.id

											LEFT JOIN nodenodes nn3
											ON n3.id=nn3.node_id
												LEFT JOIN nodes n4
												ON nn3.node_id2=n4.id

												LEFT JOIN nodenodes nn4
												ON n4.id=nn4.node_id
													LEFT JOIN nodes n5
													ON nn4.node_id2=n5.id

													LEFT JOIN nodenodes nn5
													ON n5.id=nn5.node_id
														LEFT JOIN nodes n6
														ON nn5.node_id2=n6.id

														LEFT JOIN nodenodes nn6
														ON n6.id=nn6.node_id
															LEFT JOIN nodes n7
															ON nn6.node_id2=n7.id
															
					WHERE i.company_id=$company_id
RAWQUERY;

		$db = ConnectionManager::getDataSource('default');
		return $db->query($query);		
		
	}


    /* Custom validation function */
    public function comparisonWithField($validationFields = array(), $operator = null, $compareFieldName = '') {
        if (!isset($this->data[$this->name][$compareFieldName])) {
            throw new CakeException(sprintf(__('Can\'t compare to the non-existing field "%s" of model %s.'), $compareFieldName, $this->name));
        }
        $compareTo = $this->data[$this->name][$compareFieldName];
        foreach ($validationFields as $key => $value) {
            if (!Validation::comparison($value, $operator, $compareTo)) {
                return false;
            }
        }
        return true;
    }
    
    public $belongsTo = array(
		'Company' => array(
			'className' => 'Companies',
			'foreignKey' => 'company_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
	
    public function parentNode() {
        return null;
    }

	public function periodicMeasureArray() {
		return array(__("Daily"), __("Weekly"), __("Monthly"), __("Bimonthly"), 
		__("Quarterly"), __("Four-monthly"), __("Biannual"), __("Yearly"));
	}

	public function periodMeasureArray() {
		return array(__("Day"), __("Week"), __("Month"), __("Bimonth"), 
		__("Quarter"), __("Four-month"), __("Year"));
	}
	
	public function calculationTypeArray() {
		return array(__("Weighted"), __("Sum"));
	}
	
	public function indicatorTypeArray() {
		return array(__("Effectiveness"), __("Optimization"));
	}

	public function conditionsListByCompany($company_id, $corporation_id, $solo_array=false) {
	
		// Busca los indicadores de gestión de la compañía del usuario
		
		// primero busco las compañías
		if ($company_id) {
			$companies=$this->Company->find("list", array(	"conditions"=>array("id"=>$company_id), 
															"fields"=>'id')
			);
		} else {
			// Busca los indicadores de gestión de las compañías de la corporación del usuario
			if ($corporation_id) {
				$companies=$this->Company->find("list", array(	"conditions"=>array("corporation_id"=>$corporation_id),
																"fields"=>'id'));
			// Sino no busca nada...
			} else {
				$companies=array();
			}
		}

		// ahora busco los indicadores de esas compañias y creo la condición con esos códigos de indicadores
		$indicators=$this->find("list", array(	"conditions"=>array("company_id"=>$companies),
												"fields"=>'id'));
		
		if ($solo_array) {
			return $indicators;
		} else {
			return array("indicator_id"=>$indicators);
		}

	}
	
	public function indicatorsByCompany($company_id, $corporation_id) {
	
		// Busca los indicadores de gestión de la compañía del usuario
		
		// primero busco las compañías
		if ($company_id) {
			$companies=$this->Company->find("list", array(	"conditions"=>array("id"=>$company_id), 
															"fields"=>'id')
			);
		} else {
			// Busca los indicadores de gestión de las compañías de la corporación del usuario
			if ($corporation_id) {
				$companies=$this->Company->find("list", array(	"conditions"=>array("corporation_id"=>$corporation_id),
																"fields"=>'id'));
			// Sino no busca nada...
			} else {
				$companies=array();
			}
		}
		
		// ahora busco los indicadores de esas compañias
		return $this->find("list", array(	"conditions"=>array("company_id"=>$companies)));
		
	}

	public function getCalculusByType($calculation_type, $text) 
	{		
		$texto = " sum(" . $text . ") ";
		if ($calculation_type == 0) {
			$texto = " avg(" . $text . ") ";
		}

		return $texto;

	}

	public function getCalculusByPeriod($periodic_measure, $text) 
	{
		
		if ($periodic_measure == 0) {
			$medida = " DAYOFYEAR(" . $text . ") ";
		} elseif($periodic_measure == 1) {
			$medida = " WEEk(" . $text . ") ";
		} elseif($periodic_measure == 2) {
			$medida = " MONTH(" . $text . ") ";
		} elseif($periodic_measure == 3) {
			$medida = " BYMONTH(" . $text . ") ";
		} elseif($periodic_measure == 4) {
			$medida = " QUARTER(" . $text . ") ";
		} elseif($periodic_measure == 5) {
			$medida = " CUATRIMESTRAL(" . $text . ") ";
		} elseif($periodic_measure == 6) {
			$medida = " SEMESTRAL(" . $text . ") ";
		} elseif($periodic_measure == 7) {
			$medida = " YEAR(" . $text . ") ";
		}

		return $medida;
	}

	public function getAdvanceByPeriod($id, $calculation_type, $periodic_measure, $period_goal) {
		$texto = $this->getCalculusByType($calculation_type,'iv.value');
		$medida = $this->getCalculusByPeriod($periodic_measure, 'iv.date');
/*
		$consulta = "select " 
			. $medida . "as medida, " 
			. $texto . "as metrica " 
			. "from indicatorvalues iv " 
			. "where iv.indicator_id = " . $id 
			. " group by 1" ;
*/

		$consulta = "select " 
			. 'concat(YEAR(date), "-", ' . $medida . ')' . " as medida, " 
			. $texto . "as metrica " 
			. "from indicatorvalues iv " 
			. "where iv.indicator_id = " . $id 
			. " group by YEAR(date), $medida" ;
			
		$data = $this->query($consulta);

		$datos = array();
		$cont = 0;
		foreach($data as $d) {
			$datos[$cont]['medida'] = $d['0']['medida'];
			$datos[$cont]['metrica'] = number_format($d[0]['metrica'],2);
			
			$datos[$cont]['progreso'] = number_format(($d[0]['metrica']*100)/$period_goal,2) . "%";
			$cont++;
		}
		return $datos;

	}
	
	public function getRelatedIndicators($node_id) {
	
		App::uses('Indicatornode', 'Model');
		
			$Indicatornode = new Indicatornode();
			$r = $Indicatornode->find('all',array(
				'conditions'=>array("node_id"=>$node_id),
				'recursive'=>-1
			));
			
			if (is_array($r)) {
				foreach($r as $r2) {
					$re[]=$r2["Indicatornode"]["indicator_id"];
				}
			}
			
			if (is_array($re)and sizeof($re)) {
				$this->setLanguage();
				$rel=$this->find('all', array("conditions"=>array("Indicator.id"=>$re), 'recursive'=>-1, 'order'=>"Indicator.name"));
				return $rel;
			}

		

		return array();
	
	}

	public function getRelatedTasks($node_id) {
		$consulta = "select Nodo.id, Nodo.name from nodes Nodo, nodenodes nn where nn.node_id2 = " 
		. $node_id . " and nn.node_id = Nodo.id and Nodo.nodetype_id = 15";
		$data = $this->query($consulta);
		$datos = array();
		$cont = 0;
		foreach ($data as $d) {
			$datos[$cont]['nombre'] = $d['Nodo']['name'];
			$datos[$cont]['Indicators'] = $this->getRelatedIndicators($d["Nodo"]["id"]);
			$datos[$cont]['Indicators_detail'] = $this->getRelatedIndicatorsWithDetail($d["Nodo"]["id"]);
			$cont++;
		}
		return $datos;

	}



	public function getRelatedObjectives($node_id) {		


		$consulta = "select Nodo.id, Nodo.name from nodes Nodo, nodenodes nn where nn.node_id2 = " 
		. $node_id . " and nn.node_id = Nodo.id and Nodo.nodetype_id = 7";
		$data = $this->query($consulta);
		$datos = array();
		$cont = 0;
		foreach ($data as $d) {
			$datos[$d['Nodo']['id']]['nombre'] = $d['Nodo']['name'];
			$datos[$d['Nodo']['id']]['Indicators'] = $this->getRelatedIndicators($d["Nodo"]["id"]);
			$datos[$d['Nodo']['id']]['Indicators_detail'] = $this->getRelatedIndicatorsWithDetail($d["Nodo"]["id"]);
			$cont++;
		}
		
		return $datos;

	}

	public function getValidObjectives() {
		$consulta = "select ind.node_id, count(ind.indicator_id) from indicatornodes ind,
		nodes nodo where ind.node_id = nodo.id and nodo.nodetype_id = 7 group by 1";
		$lista = array();
		$data = $this->query($consulta);
		foreach($data as $d) {
			$lista[] = $d['ind']['node_id'];
		}

		return $lista;
	}

	public function getValidTasks() {
		$consulta = "select ind.node_id, count(ind.indicator_id) from indicatornodes ind,
		nodes nodo where ind.node_id = nodo.id and nodo.nodetype_id = 15 group by 1";
		$lista = array();
		$data = $this->query($consulta);
		foreach($data as $d) {
			$lista[] = $d['ind']['node_id'];
		}

		return $lista;
	}

	public function getValidPrograms($actividades) {
		

		$consulta = "select Nodo.id from nodes Nodo, nodenodes nn where nn.node_id in ( " 
			. implode(',', $actividades) . ") and nn.node_id2 = Nodo.id and Nodo.nodetype_id = 10";
		$lista = array();
		if (! empty($actividades[0])) {
			$data = $this->query($consulta);
			foreach($data as $d) {
				$lista[] = $d['Nodo']['id'];
			}
		}
		

		return $lista;
	}

	public function getValidCommitments($programas) {
		$consulta = "select Nodo.id from nodes Nodo, nodenodes nn where nn.node_id in ( " 
			. implode(',', $programas) . ") and nn.node_id2 = Nodo.id and Nodo.nodetype_id = 4";
		$lista = array();
		$data = $this->query($consulta);
		foreach($data as $d) {
			$lista[] = $d['Nodo']['id'];
		}

		return $lista;
	}

	public function getValidObjectives2($compromisos) {
		$consulta = "select Nodo.id from nodes Nodo, 
		nodenodes nn where nn.node_id in ( " 
			. implode(',', $compromisos) . ") 
			and nn.node_id2 = Nodo.id and Nodo.nodetype_id = 7";
		$lista = array();
		$data = $this->query($consulta);
		foreach($data as $d) {
			$lista[] = $d['Nodo']['id'];
		}

		return $lista;
	}

	public function getValidActivities($tareas) {
		
		$consulta = "select Nodo.id from nodes Nodo, nodenodes nn where nn.node_id in ( " 
			. implode(',', $tareas) . ") and nn.node_id2 = Nodo.id and Nodo.nodetype_id = 11 ";
		$consulta2 = "select ind.node_id, count(ind.indicator_id) from indicatornodes ind,
		nodes nodo where ind.node_id = nodo.id and nodo.nodetype_id = 11 group by 1";
		$lista2 = array();
		$data  = $this->query($consulta);
		$data2 = $this->query($consulta2);
		if ($tareas[0] != 0) {
			foreach($data as $d) {
				$lista2[] = $d['Nodo']['id'];
			}
		}
		

		foreach($data2 as $d2) {
			$lista2[] = $d2['ind']['node_id'];
		}

		//Debugger::log($lista2);	
		return $lista2;
	}

	

	public function getValidStrategies($objetivos) {
		$consulta = "select Nodo.id from nodes Nodo, nodenodes nn where nn.node_id in ( " 
			. implode(',', $objetivos) . ") and nn.node_id2 = Nodo.id and Nodo.nodetype_id = 3";
		$lista = array();
		$data = $this->query($consulta);
		foreach($data as $d) {
			$lista[] = $d['Nodo']['id'];
		}

		return $lista;
	}

	public function getRelatedObjectives2($node_id)
	{		
		$consulta = "select Nodo.id, Nodo.name from nodes Nodo, nodenodes nn where nn.node_id2 = " 
		. $node_id . " and nn.node_id = Nodo.id and Nodo.nodetype_id = 7";
		$data = $this->query($consulta);
		$datos = array();
		$cont = 0;
		foreach ($data as $d) {
			$datos[$d["Nodo"]["id"]]['nombre'] = $d['Nodo']['name'];
			$datos[$d["Nodo"]["id"]]['Commitments'] = $this->getRelatedCommitments($d["Nodo"]["id"]);			
			$cont++;
		}		
		return $datos;

	}

	public function getRelatedCommitments($node_id)
	{		
		$tasks = $this->getValidTasks();
		if (count($tasks) > 0) {
			$activities = $this->getValidActivities($tasks);
		} else {
			$activities = $this->getValidActivities(array(0));
		}
		if (count($activities) > 0) {
			$programs = $this->getValidPrograms($activities);			
		}
		if (count($programs) > 0) {
			$commitments = $this->getValidCommitments($programs);
		}
		
		$consulta = "select Nodo.id, Nodo.name from nodes Nodo, nodenodes nn where nn.node_id2 = " 
		. $node_id . " and nn.node_id = Nodo.id and Nodo.nodetype_id = 4 and nn.node_id in (" . implode(',', $commitments) . ")";
		$datos = array();
		$cont = 0;
		$data = $this->query($consulta);
		foreach ($data as $d) {
			$datos[$cont]['nombre'] = $d['Nodo']['name']; 
			$datos[$cont]['Programs'] = $this->getRelatedPrograms($d['Nodo']['id']);
			$cont++;			
		}
		return $datos;

	}

	public function getRelatedPrograms($node_id)
	{		
		$tasks = $this->getValidTasks();
		if (count($tasks) > 0) {
			$activities = $this->getValidActivities($tasks);
		} else {
			$activities = $this->getValidActivities(array(0));
		}
		if (count($activities) > 0) {
			$programs = $this->getValidPrograms($activities);			
		}
		
		$consulta = "select Nodo.id, Nodo.name from nodes Nodo, nodenodes nn where nn.node_id2 = " 
		. $node_id . " and nn.node_id = Nodo.id and Nodo.nodetype_id = 10 and nn.node_id in (" . implode(',', $programs) . ")";
		$datos = array();
		$cont = 0;
		$data = $this->query($consulta);
		foreach ($data as $d) {
			$datos[$cont]['nombre'] = $d['Nodo']['name']; 
			$datos[$cont]['Activities'] = $this->getRelatedActivities($d['Nodo']['id']);
			$cont++;			
		}
		return $datos;

	}

	

	public function getRelatedActivities($node_id)
	{		
		$tasks = $this->getValidTasks();
		if (count($tasks) > 0) {
			$activities = $this->getValidActivities($tasks);
		} else {
			$activities = $this->getValidActivities(array(0));
		}
		
		
		$consulta = "select Nodo.id, Nodo.name from nodes Nodo, nodenodes nn where nn.node_id2 = " 
		. $node_id . " and nn.node_id = Nodo.id and Nodo.nodetype_id = 11 and nn.node_id in (" . implode(',', $activities) . ")";
		$datos = array();
		$cont = 0;
		$data = $this->query($consulta);
		foreach ($data as $d) {
			$datos[$cont]['nombre'] = $d['Nodo']['name']; 
			$datos[$cont]['Tasks'] = $this->getRelatedTasks($d['Nodo']['id']);
			$datos[$cont]['Indicators'] = $this->getRelatedIndicators($d["Nodo"]["id"]);
			$datos[$cont]['Indicators_detail'] = $this->getRelatedIndicatorsWithDetail($d["Nodo"]["id"]);
			$cont++;			
		}
		return $datos;

	}


	public function getRelatedStrategies($node_id) {
		$objetivos = $this->getValidObjectives();
		$datos = array();
		$cont = 0;
		if (count($objetivos) > 0) {
			$estrategias = $this->getValidStrategies($objetivos);
			if (count($estrategias) > 0) {
				$consulta = "select Nodo.id, Nodo.name from nodes Nodo, nodenodes nn where nn.node_id2 = " 
				. $node_id . " and nn.node_id = Nodo.id and Nodo.id in (" 
				. implode(',', $estrategias) . ") and Nodo.nodetype_id = 3";
				$data = $this->query($consulta);
				
				
				foreach ($data as $d) {
					$datos[$d['Nodo']['id']]['nombre'] = $d['Nodo']['name']; 
					$datos[$d['Nodo']['id']]['objetivos'] = $this->getRelatedObjectives($d['Nodo']['id']);					
					$cont++;			
				}
				
			}
			
		}

		$tasks = $this->getValidTasks();
		
		if (count($tasks) > 0) {
			$activities = $this->getValidActivities($tasks);
			
		} else {
			$activities = $this->getValidActivities(array(0));			
		}
		
		if (count($activities) > 0) {				
			$programs = $this->getValidPrograms($activities);						
		}

		if (count($programs) > 0) {
			$commitments = $this->getValidCommitments($programs);
		}
		if (count($commitments) > 0) {
			$objetives2 = $this->getValidObjectives2($commitments);
		}

		$datos2 = array();


		if (count($objetives2) > 0) {
			
			$estrategias2 = $this->getValidStrategies($objetives2);
			if (count($estrategias2) > 0) {
				$consulta2 = "select Nodo.id, Nodo.name from nodes Nodo, nodenodes nn where nn.node_id2 = " 
				. $node_id . " and nn.node_id = Nodo.id and Nodo.id in (" 
				. implode(',', $estrategias2) . ") and Nodo.nodetype_id = 3";
				$data2 = $this->query($consulta2);
								
				foreach ($data2 as $d2) {
					$datos[$d2['Nodo']['id']]['nombre'] = $d2['Nodo']['name'];
					if (count($datos[$d2['Nodo']['id']]['objetivos']) > 0) {
						$datos[$d2['Nodo']['id']]['objetivos'] = array_merge($this->getRelatedObjectives2($d2['Nodo']['id']), $datos[$d2['Nodo']['id']]['objetivos']);
					} else {
						$datos[$d2['Nodo']['id']]['objetivos'] = $this->getRelatedObjectives2($d2['Nodo']['id']);
					}					
					$cont++;			
				}				
			}
			
		}
		//$datos = array_merge($datos, $datos2);

		return $datos;

	}

	public function getRelatedIndicatorsWithDetail($node_id) {
	
		App::uses('Indicatornode', 'Model');
		
			$Indicatornode = new Indicatornode();
			$r = $Indicatornode->find('all',array(
				'conditions'=>array("node_id"=>$node_id),
				'recursive'=>-1
			));
			
			if (is_array($r)) {
				foreach($r as $r2) {
					$re[]=$r2["Indicatornode"]["indicator_id"];
				}
			}
			
			if (is_array($re)and sizeof($re)) {
				$this->setLanguage();
				$rel=$this->getSummaryIndicator($re);
				return $rel;
			}

		return array();
	
	}

	function getThresholdArray(
		$red_value, $red_condition, 
		$yellow_value, $yellow_condition, 
		$green_value, $green_condition,
		$goal) 
	{
		$data = array();
		$to3 = $goal;
		$from3 = 0; 
		$to2 = 0;
		$from2 = 0;
		$to1 = 0;
		$from1 = 0;	
		$ordenado = array();
		if (in_array($red_condition, array('Mayor que', 'Menor que'))) {
			if ($red_condition == "Mayor que") {
				$ordenado[1] = $red_value+0.01;	
			} else {
				$ordenado[1] = $red_value-0.01;
			}
		} else {
			$ordenado[1] = $red_value;
		}

		if (in_array($yellow_condition, array('Mayor que', 'Menor que'))) {
			if ($yellow_condition == "Mayor que") {
				$ordenado[2] = $yellow_value+0.01;	
			} else {
				$ordenado[2] = $yellow_value-0.01;
			}
		} else {
			$ordenado[2] = $yellow_value;
		}

		if (in_array($green_condition, array('Mayor que', 'Menor que'))) {
			if ($green_condition == "Mayor que") {
				$ordenado[3] = $green_value+0.01;	
			} else {
				$ordenado[3] = $green_value-0.01;
			}
		} else {
			$ordenado[3] = $green_value;
		}

		
		$paralelo = array();
		$paralelo[1] = $red_condition;
		$paralelo[2] = $yellow_condition;
		$paralelo[3] = $green_condition;
		$auxiliar = array();
		arsort($ordenado);
		$cont = 0;
		$llaves = array_keys($ordenado);
		foreach($ordenado as $index => $v) {
			
			if ($cont == 0) {
				$auxiliar[$index]['to'] = $goal;				
				if (in_array($paralelo[$index], array('Mayor que', 'Mayor o igual que'))) {					
					$auxiliar[$index]['from'] = $v;
					$auxiliar[$llaves[1]]['to'] = $v;
				} elseif(in_array($paralelo[$index], array('Menor que', 'Menor o igual que'))) {
					$auxiliar[$index]['to'] = $v;
				}
			}

			if ($cont == 1) {
				if (in_array($paralelo[$index], array('Mayor que', 'Mayor o igual que'))) {
					$auxiliar[$llaves[2]]['to'] = $v;
					$auxiliar[$index]['from'] = $v;
				} elseif(in_array($paralelo[$index], array('Menor que', 'Menor o igual que'))) {
					$auxiliar[$index]['to'] = $v;
					if (strlen($auxiliar[$llaves[0]]['from']) == 0)
					$auxiliar[$llaves[0]]['from'] = $v;
				}
			}

			if ($cont == 2) {
				if (in_array($paralelo[$index], array('Mayor que', 'Mayor o igual que'))) {					
					$auxiliar[$llaves[2]]['from'] = $v;
				} elseif(in_array($paralelo[$index], array('Menor que', 'Menor o igual que'))) {
					if (strlen($auxiliar[$llaves[1]]['from']) == 0)
					$auxiliar[$llaves[1]]['from'] = $v;
					$auxiliar[$llaves[2]]['to'] = $v;
					$auxiliar[$llaves[2]]['from'] = 0;
				}
			}

			$cont++;

			
		}



		$dataAux = array(
			'from' => $auxiliar[1]['from'],
			'to' => $auxiliar[1]['to'],
			'color' => '#55BF3B',
			'namecolor' => 'red',
			'colorvalue' => 1);
		
		$data[] = $dataAux;
		
		$dataAux = array(
			'from' => $auxiliar[2]['from'],
			'to' => $auxiliar[2]['to'],
			'color' => '#DDDF0D',
			'namecolor' => 'yellow',
			'colorvalue' => 2);

		$data[] = $dataAux;

		$dataAux = array(
			'from' => $auxiliar[3]['from'],
			'to' =>  $auxiliar[3]['to'],
			'color' => '#DF5353',
			'namecolor' => 'green',
			'colorvalue' => 3);

		$data[] = $dataAux;
		

		return $data;

	}

	public function getDetailByPeriod($id) {
		
	}

	public function testRange($int,$min,$max) {
		return ($min<=$int && $int<=$max);
	}

	public function getColorRange($from1, $to1, $from2, $to2, $from3, $to3, $achieved, $goal, $color1, $color2, $color3) {
		
		/*
		* 1= rojo
		* 2= amarillo
		* 3= verde
		*/

		if ($achieved > $goal) {
			$achieved = $goal-0.01;
		}
		if ($goal<>0) {
			$pct = $achieved * 100 / $goal;
		} else {
			$pct = 0;
		}
		if ($this->testRange($pct,$from1,$to1)) {
			return $color1;
		} elseif($this->testRange($pct,$from2,$to2)) {
			return $color2;
		} elseif($this->testRange($pct,$from3,$to3)) {
			return $color3;
		}
	} 

	private function translateCondition($cond) {
		switch ($cond) {
			case "Mayor o igual que": return ">=";				
			case "Mayor que": return ">";				
			case "Menor o igual que": return "<=";				
			case "Menor que": return "<";		
		}
	}

	public function getColorRangeV2($achieved, 
									$goal,
									$cond_value_inferior,
									$cond_value_superior,
									$corte_inferior,
									$corte_superior,
									$order) {

		// calcular porcentaje
		if ($goal<>0) {
			$pct = $achieved * 100 / $goal;
		} else {
			$pct = 0;
		}
		
		/*
		* return color here
		* 1= rojo
		* 2= amarillo
		* 3= verde
		*/
		
		// si rojo esta en el orden superior
		if (!$order) {
			
			// si el limite superior es >=
			if ($corte_superior) {
				
				// si el porcentaje es mayor al limite superior
				// devuelve rojo
				
				if ($pct >= $cond_value_superior) {
					return 1;
				}
					
			// si el limite superior no es >=
			} else {
			
				// si el porcentaje es mayor o igual al limite superior
				// devuelve rojo
				if ($pct > $cond_value_superior) {
					return 1;
				}
				
			}
			
			// si el limite inferior es >=
			if ($corte_inferior) {

				// si el porcentaje es mayor al limite inferior
				// devuelve amarillo
			
				if ($pct >= $cond_value_inferior) {
					return 2;
				}
				
			// si el limite inferior no es >=
			} else {
			
				// si el porcentaje es mayor o igual al limite inferior
				// devuelve amarillo
				if ($pct > $cond_value_inferior) {
					return 2;
				}
				
			}
			
			// en cualquier otros caso			
			// devuelve verde
			return 3;
				
		// si verde esta en el orden inferior
		} else {
			
			// si el limite superior es >=
			if ($corte_superior) {
				
				// si el porcentaje es mayor al limite superior
				// devuelve verde
				
				if ($pct >= $cond_value_superior) {
					return 3;
				}
					
			// si el limite superior no es >=
			} else {
			
				// si el porcentaje es mayor o igual al limite superior
				// devuelve verde
				if ($pct > $cond_value_superior) {
					return 3;
				}
				
			}
			
			// si el limite inferior es >=
			if ($corte_inferior) {

				// si el porcentaje es mayor al limite inferior
				// devuelve amarillo
			
				if ($pct >= $cond_value_inferior) {
					return 2;
				}
				
			// si el limite inferior no es >=
			} else {
			
				// si el porcentaje es mayor o igual al limite inferior
				// devuelve amarillo
				if ($pct > $cond_value_inferior) {
					return 2;
				}
				
			}
			
			// en cualquier otro caso			
			// devuelve rojo
			return 1;

		}
	} 

	public function getSummaryIndicator($id) {
	
		$wo = array();
	
		$this->setLanguage();
		
		$conditions = array();
		
		$conditions["Indicator.id"]=$id;		
		
		$wo = $this->find('all',array(
			'conditions'=>$conditions,
			'recursive'=>-1
		));

		
			foreach($wo as $ig2) {
				$wo2[$ig2["Indicator"]["id"]]['id'] = $ig2["Indicator"]["id"]; 
				$wo2[$ig2["Indicator"]["id"]]['name'] = $ig2["Indicator"]["name"];
				$wo2[$ig2["Indicator"]["id"]]['goal'] = $ig2['Indicator']['goal'];
				$wo2[$ig2["Indicator"]["id"]]['metric'] = $ig2['Indicator']['metric'];
				$wo2[$ig2["Indicator"]["id"]]['periodic_measure'] = $ig2['Indicator']['periodic_measure'];
				$wo2[$ig2["Indicator"]["id"]]['responsible'] = $ig2['Indicator']['responsible'];
				$wo2[$ig2["Indicator"]["id"]]['responsible_email'] = $ig2['Indicator']['responsible_email'];
				$wo2[$ig2["Indicator"]["id"]]['calculation_type'] = $ig2['Indicator']['calculation_type'];
				$wo2[$ig2["Indicator"]["id"]]['inidate'] = $ig2['Indicator']['inidate'];
				$wo2[$ig2["Indicator"]["id"]]['enddate'] = $ig2['Indicator']['enddate'];				
				$valor = 0;				
				$cont = 0;								
				
				$wo3 = $this->query('select iv.value from indicatorvalues iv where iv.indicator_id = ' 
				. $ig2["Indicator"]["id"]);
				foreach ($wo3 as $w) {
					$valor += $w['iv']['value'];
					$cont++;
				} 

				//Si es promedio que promedie caso contrario deje la sumatoria
				if ($ig2['Indicator']['calculation_type'] == 0) {
					if ($cont!= 0) {
						$valor /= $cont;
					}					 
				}

				$wo2[$ig2["Indicator"]["id"]]['achieved'] = $valor;				
				$wo2[$ig2["Indicator"]["id"]]['data_json'] = 
					$this->getThresholdArray(
						$ig2['Indicator']['threshold_red_value'], 
						$ig2['Indicator']['threshold_red_condition'],
						$ig2['Indicator']['threshold_yellow_value'], 
						$ig2['Indicator']['threshold_yellow_condition'],
						$ig2['Indicator']['threshold_green_value'],
						$ig2['Indicator']['threshold_green_condition'],
						$ig2['Indicator']['goal']);
				$data_aux = $this->getThresholdArray(
						$ig2['Indicator']['threshold_red_value'], 
						$ig2['Indicator']['threshold_red_condition'],
						$ig2['Indicator']['threshold_yellow_value'], 
						$ig2['Indicator']['threshold_yellow_condition'],
						$ig2['Indicator']['threshold_green_value'],
						$ig2['Indicator']['threshold_green_condition'],
						$ig2['Indicator']['goal']);
				$wo2[$ig2["Indicator"]["id"]]['color'] = 
					$this->getColorRangeV2(
						$valor, 
						$ig2['Indicator']['goal'], 
						$ig2['Indicator']['cond_value_1'], 
						$ig2['Indicator']['cond_value_2'], 
						$ig2['Indicator']['cut_value_1'], 
						$ig2['Indicator']['cut_value_2'], 
						$ig2['Indicator']['cut_order']);
			
			$wo=$wo2;
		}
		
		return $wo;

	}

	public function getSummaryIndicator2($id) {
	
		$wo = array();
	
		$this->setLanguage();
		
		$conditions = array();
		
		$conditions["Indicator.id"]=$id;		
		
		$wo = $this->find('all',array(
			'conditions'=>$conditions,
			'recursive'=>-1
		));

		
			foreach($wo as $ig2) {
				$wo2[$ig2["Indicator"]["id"]]['id'] = $ig2["Indicator"]["id"]; 
				$wo2[$ig2["Indicator"]["id"]]['name'] = $ig2["Indicator"]["name"];
				$wo2[$ig2["Indicator"]["id"]]['goal'] = $ig2['Indicator']['goal'];
				$wo2[$ig2["Indicator"]["id"]]['metric'] = $ig2['Indicator']['metric'];
				$wo2[$ig2["Indicator"]["id"]]['periodic_measure'] = $ig2['Indicator']['periodic_measure'];
				$wo2[$ig2["Indicator"]["id"]]['responsible'] = $ig2['Indicator']['responsible'];
				$wo2[$ig2["Indicator"]["id"]]['responsible_email'] = $ig2['Indicator']['responsible_email'];
				$wo2[$ig2["Indicator"]["id"]]['calculation_type'] = $ig2['Indicator']['calculation_type'];
				$wo2[$ig2["Indicator"]["id"]]['inidate'] = $ig2['Indicator']['inidate'];
				$wo2[$ig2["Indicator"]["id"]]['enddate'] = $ig2['Indicator']['enddate'];				
				$valor = 0;				
				$cont = 0;								
				
				$wo3 = $this->query('select iv.value from indicatorvalues iv where iv.indicator_id = ' 
				. $ig2["Indicator"]["id"]);
				foreach ($wo3 as $w) {
					$valor += $w['iv']['value'];
					$cont++;
				} 

				//Si es promedio que promedie caso contrario deje la sumatoria
				if ($ig2['Indicator']['calculation_type'] == 0) {
					if ($cont!= 0) {
						$valor /= $cont;
					}					 
				}

				$wo2[$ig2["Indicator"]["id"]]['achieved'] = $valor;				
				$wo2[$ig2["Indicator"]["id"]]['color'] = 
					$this->getColorRangeV2(
						$valor, 
						$ig2['Indicator']['goal'], 
						$ig2['Indicator']['cond_value_1'], 
						$ig2['Indicator']['cond_value_2'], 
						$ig2['Indicator']['cut_value_1'], 
						$ig2['Indicator']['cut_value_2'], 
						$ig2['Indicator']['cut_order']);
			$wo=$wo2;
		}
		
		return $wo;

	}

	public function getHightlightedSummary($company_id, $aslist=false) {
	
		$wo = array();
	
		$this->setLanguage();
		
		$conditions = array();
		
		$conditions["Indicator.company_id"]=$company_id;
		$conditions["Indicator.flag_hightlighted"]=1;
		
		$wo = $this->find('all',array(
			'conditions'=>$conditions,
			'recursive'=>-1
		));

		if ($aslist) {
			foreach($wo as $ig2) {
				$wo2[$ig2["Indicator"]["id"]]['id'] = $ig2["Indicator"]["id"]; 
				$wo2[$ig2["Indicator"]["id"]]['name'] = $ig2["Indicator"]["name"];
				$wo2[$ig2["Indicator"]["id"]]['goal'] = $ig2['Indicator']['goal']; 
				$valor = 0;				
				$cont = 0;								
				
				$wo3 = $this->query('select iv.value from indicatorvalues iv where iv.indicator_id = ' 
				. $ig2["Indicator"]["id"]);
				foreach ($wo3 as $w) {
					$valor += $w['iv']['value'];
					$cont++;
				} 

				//Si es promedio que promedie caso contrario deje la sumatoria
				if ($ig2['Indicator']['calculation_type'] == 0) {
					if ($cont<>0) {
						$valor /= $cont; 
					} else {
						$valor=0;
					}
				}

				$wo2[$ig2["Indicator"]["id"]]['achieved'] = $valor;
				$wo2[$ig2["Indicator"]["id"]]['data_json'] = 
					$this->getThresholdArray(
						$ig2['Indicator']['threshold_red_value'], 
						$ig2['Indicator']['threshold_red_condition'],
						$ig2['Indicator']['threshold_yellow_value'], 
						$ig2['Indicator']['threshold_yellow_condition'],
						$ig2['Indicator']['threshold_green_value'],
						$ig2['Indicator']['threshold_green_condition'],
						$ig2['Indicator']['goal']);

			}
			$wo=$wo2;
		}
		
		return $wo;

	}

	public function getHightlightedIndicators($company_id, $aslist=false) {
	
		$wo = array();
	
		$this->setLanguage();
		
		$conditions = array();
		
		$conditions["Indicator.company_id"]=$company_id;
		$conditions["Indicator.flag_hightlighted"]=1;
		
		$wo = $this->find('all',array(
			'conditions'=>$conditions,
			'recursive'=>-1
		));

		if ($aslist) {
			foreach($wo as $ig2) {
				$wo2[$ig2["Indicator"]["id"]]['name'] = $ig2["Indicator"]["name"];
				$wo2[$ig2["Indicator"]["id"]]['goal'] = $ig2['Indicator']['goal']; 
				$valor = 0;				
				$cont = 0;								
				
				$wo3 = $this->query('select iv.value from indicatorvalues iv where iv.indicator_id = ' 
				. $ig2["Indicator"]["id"]);
				foreach ($wo3 as $w) {
					$valor += $w['iv']['value'];
					$cont++;
				} 

				//Si es promedio que promedie caso contrario deje la sumatoria
				if ($ig2['Indicator']['calculation_type'] == 0) {
					$valor /= $cont; 
				}

				$wo2[$ig2["Indicator"]["id"]]['achieved'] = $valor;

			}
			$wo=$wo2;
		}
		
		return $wo;

	}
	
	public function getIndicators($company_id, $aslist=false) {
	
		$wo = array();
	
		$this->setLanguage();
		
		$conditions = array();
		
		$conditions["Indicator.company_id"]=$company_id;
		
		$wo = $this->find('all',array(
			'conditions'=>$conditions,
			'recursive'=>-1
		));

		if ($aslist) {
			foreach($wo as $ig2) {
				$wo2[$ig2["Indicator"]["id"]]=$ig2["Indicator"]["name"];
			}
			$wo=$wo2;
		}
		
		return $wo;

	}

}
