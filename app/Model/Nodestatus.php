<?php
App::uses('AppModel', 'Model');
/**
 * Nodestatus Model
 *
 */


class Nodestatus extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
            'name' => 'NodestatusnameTranslation' /* Modelo+Campo+Translation */
        )
    );

    public $locale = 'esp';

    public $validate = array(
        'name' => array(
					'notempty' => array(
						'rule' => array('notempty'),
						'message' => 'This field can not be empty',
					),
		),
    );

    public function parentNode() {
        return null;
    }

}
