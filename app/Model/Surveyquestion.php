<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 * @property Modules $Modules
 */
class Surveyquestion extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
            'name' => 'SurveyquestionnameTranslation',
            'description' => 'SurveyquestiondescriptionTranslation',
            'answers' => 'SurveyquestionanswersTranslation',
         )
    );

    public $locale = 'esp';
	
    public $belongsTo = array(
        'Node' => array(
            'className' => 'Nodes',
            'foreignKey' => 'node_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty',
			),
		),
		'order' => array(
				'rule' => 'numeric',
				'message' => 'Please suply a number',
		),
        'node_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty',
			),
        ),
	);

	 public function parentNode() {
	 	return null;
    }
}
