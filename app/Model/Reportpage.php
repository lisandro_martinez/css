<?php
App::uses('AppModel', 'Model');
/**
 * Reportpage Model
 *
 */


class Reportpage extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
        )
    );

    public $validate = array(
        'order' => array(
			'rule' => 'numeric',
			'message' => 'Please suply a number',
		),
        'report_id' => array(
					'notempty' => array(
						'rule' => array('notempty'),
						'message' => 'This field can not be empty',
					),
		),
    );

	public $belongsTo = array(
		'Report' => array(
			'className' => 'Reports',
			'foreignKey' => 'report_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
    public function parentNode() {
        return null;
    }

	public function reportsByCompany($company_id) {
		// busco los reportes de esas compañias
		return $this->Report->find("list", array("conditions"=>array("company_id"=>$company_id)));
	}

	public function templates() {
		include_once(dirname(dirname(__FILE__)).'/webroot/css-pdf/code/html_generator/html_generator.php');
		return htmlGenerator::reflection();
	}
	
}
