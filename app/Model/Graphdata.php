<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 * @property Modules $Modules
 */
class Graphdata extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
            'name' => 'GraphdatanameTranslation',
         )
    );

    public $locale = 'esp';
	
    public $belongsTo = array(
        'Node' => array(
            'className' => 'Nodes',
            'foreignKey' => 'node_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty',
			),
		),
        'node_id' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty',
			),
        ),
	);

	 public function parentNode() {
	 	return null;
    }
	
	function getGraphTypes() {
	
		return array(	"esp"=>array("Torta", "Líneas", "Barras"),
						"eng"=>array("Cake", "Lines", "Bars"));
	
	}

	static function get_list() {

		return [
			10 => __("Cake") . "1",
			13 => __("Cake") . "2",
			14 =>  __("Cake") . "3",
			50 =>  __("Cake") . "4",
			51 =>  __("Cake") . "5",
			52 =>  __("Cake") . "6",
			53 =>  __("Cake") . "7",
			54 =>  __("Cake") . "8",
			55 =>  __("Cake") . "9",
			2 =>  __("Curves") . "1",
			7 =>  __("Curves") . "2",
			67 =>  __("Curves") . "3",
			3 =>  __("Bars") . "1",
			12 =>  __("Bars") . "2",
			20 =>  __("Bars") . "3",
			23 =>  __("Bars") . "4",
			58 =>  __("Bars") . "5",
			59 =>  __("Bars") . "6",
			60 =>  __("Bars") . "7",
			61 =>  __("Bars") . "8",
			62 =>  __("Bars") . "9",
			63 =>  __("Bars") . "10",
			64 =>  __("Bubbles") . "1",
			65 =>  __("Bubbles") . "2",
			66 =>  __("Bubbles") . "3",
			17 =>  __("Lines") . "1",
			21 =>  __("Lines") . "2",
			25 =>  __("Lines") . "3",
			56 =>  __("Lines") . "4",
			57 =>  __("Lines") . "5",
			5 =>  __("Range") . "1"
		];


	}

}
