<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 * @property Modules $Modules
 */
class Questionnaire extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
            'title' => 'QuestionnairetitleTranslation', /* Modelo+Campo+Translation */
            'description' => 'QuestionnairedescriptionTranslation', 
            'acronym' => 'QuestionnaireacronymTranslation', 
         )
    );

    public $locale = 'esp';

	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty',
			),
		),
	);

	 public function parentNode() {
	 	return null;
    }
	
	public function findByCo($type="list", $corp=null, $comp=null) {
	
		App::uses('Company', 'Model');
		$Company = new Company();
		$Company->setLanguage();
		
		App::uses('Companyquestionnaire', 'Model');
		$CQ = new Companyquestionnaire();
		$CQ->setLanguage();
		
		if ($corp) {
			$cods = $Company->find('list',array(
					  'fields'=>array(
						  'id'
					  ),
					  'conditions'=>array(
							'active'=>1,
							'corporation_id'=>$corp
					  ),
			));
		} elseif ($comp) {
			$cods = $Company->find('list',array(
					  'fields'=>array(
						  'id'
					  ),
					  'conditions'=>array(
							'active'=>1,
							'id'=>$comp
					  ),
			));
		}
		
		$questionnaires=array();

		if (isset($cods)) {
		
			$q = $CQ->find('all', array(
					'conditions'=>array(
						'company_id'=>$cods
					),
					'recursive'=>-1
					)
			);
			
			if (is_array($q)) {
			
				foreach($q as $quest) {
				
					$this->setLanguage();
					$q2 = $this->find('all',array(
						'conditions'=>array(
							'active'=>1,
							'Questionnaire.id'=>$quest["Companyquestionnaire"]["questionnaire_id"]
						)
					));

					if (isset($q2[0]))	{
					
						$q2=$q2[0];
					
						$questionnaires[$q2["Questionnaire"]["id"]]=$q2;
						
					}
					
				}
				
			}
			
		}

		return $questionnaires;
	
	}
	
	// Busca el mayor puntaje posible de un cuestionario
	public function calculateTotalPoints($qid) {
	
		$puntaje_total=0;
		
		App::uses('ConnectionManager', 'Model'); 
		$db = ConnectionManager::getDataSource('default');
		
		if ($db->isConnected()) {
			$res = $db->query("SELECT sum(m) as suma FROM (SELECT a.question_id, max(a.points) as m FROM questionnaires q INNER JOIN sections s ON q.id=s.questionnaire_id INNER JOIN questions u ON s.id=u.section_id INNER JOIN answers a ON u.id=a.question_id WHERE q.id=$qid GROUP BY a.question_id) s");
			
			if (isset($res[0][0]["suma"])) {
				$puntaje_total=$res[0][0]["suma"];
			}
			
		}
		
		return $puntaje_total;
	}
	
}
