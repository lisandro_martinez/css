<?php

App::uses('AppModel', 'Model');

/**
 * Surveyanswer Model
 *
 */

class Surveyanswer extends AppModel {

	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
    );

    public function parentNode() {
        return null;
    }

	public function totalize($questions, $answers) {
		$totalize=array();
		if (is_array($questions)) {
			foreach($questions as $q) {
				$totalize[$q["Surveyquestion"]["id"]]["question"]=$q["Surveyquestion"]["name"];
				$totalize[$q["Surveyquestion"]["id"]]["question_type"]=$q["Surveyquestion"]["question_type"];
				// inicializa las posibles respuestas de cada tipo de pregunta
				switch($q["Surveyquestion"]["question_type"]) {
				case 1: // es una selección simple entonces lo que hace es contar la cantidad seleccionada de cada valor 
				case 2: // es una selección multiple entonces lo que hace es contar la cantidad seleccionada de cada valor 
					$possible_answers=explode(PHP_EOL, $q["Surveyquestion"]["answers"]);
					foreach($possible_answers as $ipa => $pa) {
						$totalize[$q["Surveyquestion"]["id"]]["answer"][$ipa]["text"]=$pa;
						$totalize[$q["Surveyquestion"]["id"]]["answer"][$ipa]["value"]=0;
					}
					$totalize[$q["Surveyquestion"]["id"]]["total"]=0; // inicializa el valor total para porcentajes
					break;
					break;
				case 3: // es una pregunta abierta entonces lo que hace es copiar todos los mensajes 
					$totalize[$q["Surveyquestion"]["id"]]["answer"]="";
					break;
				}
			}
//			pr($totalize);
			foreach($answers as $a) {
				$data = unserialize($a["Surveyanswer"]["data"]);
				foreach($data as $idq=>$d) {
//				pr($d);
					if ($totalize[$idq]["question_type"]==1) { 						// es una respuesta de selección simple
						$totalize[$idq]["answer"][$d]["value"]++; 					// suma 1 a la respuesta $d
						$totalize[$idq]["total"]++; 								// suma 1 al total
					} 
					if ($totalize[$idq]["question_type"]==2) { 						// es una respuesta de selección multiple
						if (is_array($d)) { 										// son multiples respuestas
							foreach($d as $opc_sel) {
								$totalize[$idq]["answer"][$opc_sel]["value"]++; 	// suma 1 a la respuesta $d
							}
							$totalize[$idq]["total"]++; 							// suma 1 al total
						}
					} 
					if ($totalize[$idq]["question_type"]==3) { 						// es una respuesta abierta
						$totalize[$idq]["answer"][]=nl2br($d);
					} 					
				}
			}
//			pr($totalize);

			// todo bien, ahora calcula los porcentajes de cada respuesta que sea de selección múltiple o simple
			
			if (is_array($totalize)) {
				foreach($totalize as $idq => $total) {
					if ($totalize[$idq]["question_type"]==1 or $totalize[$idq]["question_type"]==2) {
						if (is_array($totalize[$idq]["answer"]) and $totalize[$idq]["total"]<>0) {
							foreach($totalize[$idq]["answer"] as $ia=>$a) {
								$totalize[$idq]["answer"][$ia]["porc"] = round($totalize[$idq]["answer"][$ia]["value"]/$totalize[$idq]["total"]*100);
							}
						}
					}
				}
			}

		}
		return $totalize;
	}
	
}
