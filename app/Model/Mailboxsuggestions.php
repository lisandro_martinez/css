<?php
App::uses('AppModel', 'Model');
/**
 * Mailboxsuggestion Model
 *
 */

class Mailboxsuggestion extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
    );

    public function parentNode() {
        return null;
    }
	
}
