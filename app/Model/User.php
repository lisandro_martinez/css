<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppModel', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
App::uses('AuthComponent', 'Controller/Component');
class User extends AppModel {

    public $actsAs = array(
          'Acl' => array('type' => 'requester'),
    );

    public $belongsTo = array(
        'Group' => array(
            'className' => 'Groups',
            'foreignKey' => 'group_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Company' => array(
            'className' => 'Companies',
            'foreignKey' => 'company_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Corporation' => array(
            'className' => 'Corporations',
            'foreignKey' => 'corporation_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

    public function password_verifies() {

        $user = $this->find('first', array(
                'conditions'=>array(
                    'User.id'=>AuthComponent::user('id')
                ),
                'recursive' => -1
            )
        );

        return AuthComponent::password($this->data[$this->alias]['current_password']) == $user['User']['password'];
    }

    public function password_confirm() {

        return $this->data[$this->alias]['new_password'] == $this->data[$this->alias]['confirm_password'];
    }

    public function get_password_hash($password){
      return AuthComponent::password($password);
    }

    public $validate = array(
        'fname' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'This field can not be empty',
            ),
        ),
        'lname' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'This field can not be empty',
            ),
        ),
        'email' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'This field can not be empty',
            ),
            'email' => array(
                'rule' => array('email'),
                'message' => 'No tiene un formato email',
            ),
        ),
        'username' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'This field can not be empty',
            ),
        ),
        'password' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'This field can not be empty',
            ),
        ),
        'current_password' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'This field can not be empty',
            ),
            'old_password' => array(
                'rule' => 'password_verifies',
                'message' => 'Current Password is wrong'
            )
        ),
        'new_password' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'This field can not be empty',
            ),
            'confirm_password' => array(
                'rule' => 'password_confirm',
                'message' => 'Password confirmation is wrong'
            )
        ),
        'confirm_password' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'This field can not be empty',
            ),
        ),
        'group_id' => array(
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Select one option in this list',
            ),
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'This field can be numeric',
            ),
        ),
        'company_id' => array(
/*
            'notempty' => array(
                'rule' => array('notempty'),
                'message' => 'Select one option in this list',
            ),
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'This field can be numeric',
            ),
*/
        ),
        'corporation_id' => array(
        ),
    );

    public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['User']['group_id'])) {
            $groupId = $this->data['User']['group_id'];
        } else {
            $groupId = $this->field('group_id');
        }
        if (!$groupId) {
            return null;
        } else {
            return array('Group' => array('id' => $groupId));
        }
    }

	public function entities() {

		switch ($this->Session->read('groupId')) {
		case 8: // tactical
			return array(2,3);
		case 10: // operational
			return array(3);
		default:
			return array(1,2,3);
		}

	}
	
}
