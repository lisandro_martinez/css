<?php
App::uses('AppModel', 'Model');
/**
 * Node Model
 *
 */

class Node extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
            'name' => 'NodenameTranslation', /* Modelo+Campo+Translation */
            'description' => 'NodedescriptionTranslation'
        )
    );

	const TYPE_WEAKNESSES=1;
	const TYPE_PERSPECTIVE=2;
	const TYPE_STRATEGY=3;
	const TYPE_COMMITMENT=4;
	const TYPE_MISSION=5;
	const TYPE_VISION=6;
	const TYPE_OBJECTIVE=7;
	const TYPE_POLITICS=8;
	const TYPE_EXPECTATIONS=9;
	const TYPE_PROGRAMS=10;
	const TYPE_ACTIVITIES=11;
	const TYPE_INICIATIVES=12;
	const TYPE_PROJECTS=13;
	const TYPE_COURSES=14;
	const TYPE_TASKS=15;
	const TYPE_INTEREST=16;
	const TYPE_CALL=17;
	const TYPE_GALLERY=18;
	const TYPE_SURVEY=19;
	const TYPE_MAILBOX=20;
	const TYPE_GRAPH=21;
	

	const SOURCE_AUTOEVALUATION=1;
	const SOURCE_SYSTEM=2;
	const SOURCE_MANUAL=3;

    public $locale = 'esp';

    public $validate = array(
        'name' => array(
					'notempty' => array(
						'rule' => array('notempty'),
						'message' => 'This field can not be empty',
					),
		),
        'nodetype_id' => array(
					'notempty' => array(
						'rule' => array('notempty'),
						'message' => 'This field can not be empty',
					),
		),
    );

	public $belongsTo = array(
		'Nodetype' => array(
			'className' => 'Nodetypes',
			'foreignKey' => 'nodetype_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Nodepriority' => array(
			'className' => 'Nodepriorities',
			'foreignKey' => 'nodetype_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
    public function parentNode() {
        return null;
    }
	
	public function getRelatedNodes($id) {
	
		App::uses('Nodenode', 'Model');
		App::uses('Nodetype', 'Model');
		
		$Nodenode = new Nodenode();
		$r = $Nodenode->find('all',array(
			'conditions'=>array("OR"=>array(
				array("node_id"=>$id),
				array("node_id2"=>$id)
			)),
			'recursive'=>-1
		));
		
		if (is_array($r)) {
			foreach($r as $r2) {
				if ($r2["Nodenode"]["node_id"]!=$id) {
					$re[]=$r2["Nodenode"]["node_id"];
				} else {
					$re[]=$r2["Nodenode"]["node_id2"];
				}
			}
		}
				
		
		if (is_array($re)and sizeof($re)) {
			$this->setLanguage();
			$rel=$this->find('all', array("conditions"=>array("Node.id"=>$re), 'recursive'=>-1, 
			'order'=>"nodetype_id"));
			return $rel;
		}
		
		return array();
	
	}

	
	// node_id es la entidad que tiene predecesor
	// node_id2 es el predecesor
	public function getPredecessors($id) {
	
		App::uses('Nodepredecessor', 'Model');
		App::uses('Nodetype', 'Model');
		
		$Nodepredecessor = new Nodepredecessor();
		$r = $Nodepredecessor->find('all',array(
			'conditions'=>array(
				array("node_id"=>$id)
			),
			'recursive'=>-1
		));

		if (is_array($r)) {
			foreach($r as $r2) {
				$re[]=$r2["Nodepredecessor"]["node_id2"];
			}
		}
		
		if (is_array($re)and sizeof($re)) {
			$this->setLanguage();
			$rel=$this->find('all', array("conditions"=>array("Node.id"=>$re), 'recursive'=>-1, 'order'=>"nodetype_id"));
			return $rel;
		}
		
		return array();
	
	}

	
	
	public function getNodes($nodetype_id, $company_id, $corporation_id, $aslist=false, $extra_conds=null) {
	
		$wo = array();
	
		$this->setLanguage();
		
		$conditions = array('nodetype_id'=>$nodetype_id);
		
		if (is_array($extra_conds)) {
			$conditions = array_merge($conditions, $extra_conds);
		}
		
		if ($company_id) {
			
			$conditions["nc.company_id"]=$company_id;
			
			$joins = array(
						array(
							'table' => 'nodecompanies',
							'alias' => 'nc',
							'type' => 'LEFT',
							'conditions' => array(
								'nc.node_id = Node.id',
								$extra_conds
							),
						)
			);
			
			$wo = $this->find('all',array(
				'conditions'=>$conditions,
				'joins'=>$joins,
				'order' => 'Node.name',
				'recursive'=>-1
			));

		} elseif ($corporation_id) {
			
			$conditions["corporation_id"]=$corporation_id;
			
			$wo = $this->find('all',array(
				'conditions'=>$conditions,
				'recursive'=>-1
			));
			
		}
		
		if ($aslist) {
			foreach($wo as $ig2) {
				$wo2[$ig2["Node"]["id"]]=$ig2["Node"]["name"];
			}
			$wo=$wo2;
		}
		return $wo;

	}

	// setea la compañía en función del usuario
	// si la corporación es vacía entonces setea la corporación (si existe) en base a la compañía o corporación del usuario
	public function setCompany($company_id) {
		
		App::uses('Nodecompany', 'Model');
		
		if ($company_id) {
		
			// Si el usuario tiene una compañía seteada entonces setea la relación con el nodo y la compañía (si no existe)
			
			$Nodecompany = new Nodecompany();
			
			$existe=$Nodecompany->find('first', array(
												'conditions'=>array(
																	"Nodecompany.node_id" => $this->id,
																	"Nodecompany.company_id" => $company_id),
												'recursive'=>-1));
			// Si no existe
			if (sizeof($existe)==0) {
			
				$data= array("Nodecompany" => array(
											"node_id" => $this->id,
											"company_id" => $company_id
											)
							);
				$Nodecompany->set($data);
				$Nodecompany->saveMany();
				
			}
			
		}
		
	}
	
}
