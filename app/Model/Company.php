<?php
App::uses('AppModel', 'Model');
/**
 * Category Model
 *
 * @property Modules $Modules
 */
class Company extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $actsAs = array(
         'Acl' => array('type' => 'requester'),
         'Translate' => array(
            'description' => 'CompanydescriptionTranslation', /* Modelo+Campo+Translation */
			'address' => 'CompanyaddressTranslation'
         )
    );

    public $locale = 'esp';
	
    public $belongsTo = array(
        'Corporation' => array(
            'className' => 'Corporations',
            'foreignKey' => 'corporation_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );

	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'This field can not be empty',
			),
		),
		'slug' => array(
			'notempty' => array(
				'rule' => array('custom', '/^[a-z0-9\-\_]*$/i'),
				'message' => "This field can not be empty, and it only accepts numbers, letters and dashes (without spaces)",
			),
		),
	);

	 public function parentNode() {
	 	return null;

    }
}
