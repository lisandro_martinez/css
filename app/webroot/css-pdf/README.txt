General

https://madalgo.au.dk/~jakobt/wkhtmltoxdoc/wkhtmltopdf-0.9.9-doc.html

------------------

Opcion 1 (ubuntu 14)

install wkhtmltopdf on ubuntu

http://askubuntu.com/questions/725369/how-to-install-wkhtmltopdf-0-12-1-on-ubuntu-server-14-04

Comando para generar el PDF:

./wkhtmltopdf --orientation Portrait http://css.local/app/webroot/css-pdf/code/example.php  ./test.pdf  2>&1

------------------

Opcion 2 (hostgator)

http://stackoverflow.com/questions/10938908/wkhtmltopdf-installation-on-web-server-hostgator

package: http://download.gna.org/wkhtmltopdf/obsolete/linux/wkhtmltopdf-0.11.0_rc1-static-amd64.tar.bz2

------------------

Opcion 3 composer

https://github.com/h4cc/wkhtmltopdf-amd64

This worked in hostgator with composer version:

../vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64  --orientation Portrait http://css.local/app/webroot/css-pdf/code/example.php  ./test.pdf  2>&1

