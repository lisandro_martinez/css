<?php

$numpag=0; // numero de pagina en el encabezado
($npags=$_GET["repeat"])?"":$npags=1; // numero de p�ginas a generar en cada seccion de prueba

function ponTitulo() {
	global $numpag;
?>

	<div class="col1_3">
		<?php $numpag++; echo $numpag;?>
	</div>

	<div class="col2_tercios" style="text-align: right">
		<span style="color: #afb932">| REPORTE DE SOSTENIBILIDAD 2016 |</span> <span style="color: #0c4a92">FLORIDA ICE & FARM Co.</span>
	</div>
	<div class="fin_col">
	</div>

<?php
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
	
<style>

.page {
// perfect fit for ubuntu 14.04
//	height: 90.57em;
//	width: 100%;
// end ubuntu 14.04
	
// perfect fir for redhat / hostgator
	height: 1500px;
	width: 1000px;
	page-break-inside: avoid;
//
}

ul, p {
	line-height: 200%;
	text-align: justify;
	font-family: Arial;
	color: #888;
	font-size: 13px;
}

h1 {
    color: white;
    font-family: sans-serif;
    font-size: x-large;
    padding-top: 35px;
}

h2 {
  color: white;
  font-family: Georgia;
  font-size: xx-large;
  width: 300px;
  padding-left: 300px;
  padding-top: 350px;
}

h3 {
  color: #0189c3;
  font-size: x-large;
  margin: 20px;
  text-transform: uppercase;
}

h4 {
  color: #000099;
  font-size: 5em;
  margin-top: 100px;
  max-width: 10em;
  margin: auto;
  text-align: center;
}

h5 {
  color: #000099;
  font-size: 1em;
  margin: auto;
}

h6 {
    color: white;
    font-family: sans-serif;
    font-size: x-large;
    text-align: center;
    padding: 20px;
}

.col1_2 {
	float: left;
	width: 48%;
}

.col2_2 {
	float: right;
	width: 48%;
}


.col1_3 {
	float: left;
	width: 33%;
}

.col2_3 {
	float: left;
	width: 33%;
}

.col3_3 {
	float: right;
	width: 33%;
}

.col2_tercios {
	float: left;
	width: 66%;
}


.fin_col {
	clear: both;
}

.texto1 {
	padding: 10px; font-family: Verdana; font-size: large; color: #999;
}

.first_letter {
	font-size: 90px;
	float: left;
	padding-right: 10px;
}

.titulo {
  text-align: center;
}

.titulo_parte1 {
  color: blue;
  font-size: 38px;
  font-weight: bold;
}

.titulo_parte2 {
  color: green;
  font-family: Arial;
  font-size: 34px;
  font-weight: lighter;
}

.titulo2 {
  color: #007bbc;
  font-family: Georgia;
  font-size: 24px;
  line-height: 120%;
}

</style>

</head>

<body>
	
<?php

// http://red111.net/florida-prototipos/html2ps_v2043/public_html/demo/html2ps.php?process_mode=single&URL=http%3A%2F%2Fred111.net%2Fflorida-prototipos%2Fhtml2ps_v2043%2Fdemos%2F01.php&proxy=&pixels=1024&scalepoints=1&renderimages=1&renderlinks=1&renderfields=1&media=Letter&cssmedia=Screen&leftmargin=30&rightmargin=15&topmargin=15&bottommargin=15&encoding=&headerhtml=&footerhtml=&watermarkhtml=&toc-location=before&smartpagebreak=1&pslevel=3&method=fpdf&pdfversion=1.3&output=0&convert=Convert+File

// Modelos de pagina 1: 50 p�ginas: 256M

// Modelos de 3 columnas: 15 p�ginas 256M


for($i=1; $i<=$npags; $i++) {

?>

<div class="page">
	<p><img src="./img/logo.jpg"></p>
	<div style="width: 932px; height: 743px; background-image: url('./img/custom-image-0.jpg');">
		<h1 style="padding-left: 150px;">Reporte Anual de Sostenibilidad | 2016</h1>
		<h2>Crecemos sosteniblemente</h2>
	</div>
	<h3>Certificado A+ por el Global Reporting Initiative</h3>
</div>

<div class="page">

	<?php
	ponTitulo();
	?>
	
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	
	<h4>Cuarto Reporte de Sostenibilidad</h4>

	<p>&nbsp;</p>

	<div class="col1_3">
		<div style="height: 918px; background-image: url('./img/fondo-02.jpg'); background-repeat: no-repeat; padding-top: 50px;">
			<div style="margin-left: 20px; margin-right: 20px;">
				<h5>Nuestros Reportes Anteriores</h5>
				<p>&nbsp;</p>
				<p align="center"><a href="http://google.com"><img src="./img/portada1.jpg"><BR>2009 | Nivel B</a></p>
				<p>&nbsp;</p>
				<p align="center"><a href="http://google.com"><img src="./img/portada2.jpg"><BR>2010 | Nivel A+</a></p>
				<p>&nbsp;</p>
				<p align="center"><a href="http://google.com"><img src="./img/portada3.jpg"><BR>2011 | Nivel A+</a></p>
			</div>
		</div>
	</div>
	<div class="col2_3">
	
		<div class="texto1">

		<p>Por cuarto a�o consecutivo, <strong>Florida Ice & Farm Co. (FIFCO)</strong> presenta su Reporte de Sostenibilidad, con el fin de informar a sus p�blicos de inter�s y a la sociedad en general sobre sus avances en el camino hacia el desarrollo sostenible. </p>
		
		<p>Para FIFCO, la <strong>sostenibilidad</strong> es un tema estrat�gico; es una forma de hacer negocios que permea todas sus esferas de acci�n y que permite su crecimiento a largo plazo, en armon�a con las personas, las comunidades y el medio ambiente.</p>
		
		<p>Guiada por las expectativas de sus <strong>p�blicos de inter�s</strong>, a quienes se consulta cada a�o, la empresa trabaja con rigurosidad no solo para reducir sus huellas, sino tambi�n para crear valor en <strong>tres dimensiones</strong> (ambiental, social y econ�mica) con un enfoque de medici�n de progreso y de rendici�n de cuentas.</p>
		
		<p>Es por ello que el presente reporte anual recopila m�s de <strong>70 indicadores</strong> ambientales, sociales y econ�micos, relacionados directamente con el impacto de la empresa, correspondientes al per�odo fiscal <strong>octubre 2011-setiembre 2012</strong>, de todas las operaciones de FIFCO en Costa Rica, Guatemala y El Salvador: Florida Bebidas, Industrias Alimenticias Kern�s y Florida Inmobiliaria (Reserva Conchal).</p>
		
		</div>
		
	</div>
	<div class="col3_3">
	
		<div class="texto1">
		<p>Se a�ade, adem�s, las nuevas divisiones de negocio que se incorporaron durante el per�odo reportado: Florida L�cteos, Florida Vinos y Destilados, y Musmanni, divisi�n dedicada a la elaboraci�n de productos de panader�a y su distribuci�n mediante el modelo de franquicia.</p>
		<p>Dada la reciente incorporaci�n de estas divisiones a FIFCO, se reportar�n �nicamente aquellos indicadores que se encuentran ya cuantificados y se establecer� un cronograma para aquellos en proceso de construcci�n.</p>
		<p>Para recopilar esta informaci�n, se utilizaron los Protocolos de Indicadores del GRI. Adem�s, al ser FIFCO una empresa p�blica, se emplean sistemas robustos de informaci�n y de medici�n de resultados en sus tres dimensiones.</p>
		<p>No se incluye en este reporte a la empresa cervecera estadounidense North American Breweries (NAB), que FIFCO adquiri� en octubre del 2012, pues la transacci�n se encuentra fuera del per�odo reportado.</p>
		<p>Al presentar este cuarto Reporte de Sostenibilidad, no solo cumplimos con nuestro objetivo de la dimensi�n Social Externa de informar en forma �tica y transparente de nuestro progreso, sino que esperamos que todos nuestros p�blicos puedan llegar a conocer y utilizar este reporte y la informaci�n que contiene.</p>
		</div>
	</div>
	<div class="fin_col"></div>

</div>

<div class="page">

	<?php
	ponTitulo();
	?>
	
	<center>
	<img src="./img/calificacion.png">
	</center>

</div>


<div class="page"	>

	<?php
	ponTitulo();
	?>

	<center>
		<div style="width: 932px; height: 943px; background-image: url('./img/fondo-04.jpg'); vertical-align: center; margin-top: 150px; padding: 50px 0;">
			<div style="width: 600px; height: 843px; background-color: rgba(255, 255, 255, 0.8); margin-top: 50px;">
				<h1>Contenido</h1>
				<table>
					<tr>
						<td>
							<h3>Mensaje del Presidente de la Junta Directiva</h3>
						</td>
						<td>
							<h3>6</h3>
						</td>
					</tr>
					<tr>
						<td>
							<h3>Nuestra Estrategia: Crecer Sosteniblemente</h3>
						</td>
						<td>
							<h3>8</h3>
						</td>
					</tr>
					<tr>
						<td>
							<h3>Lorem ipsum atdolor sim asimet.</h3>
						</td>
						<td>
							<h3>12</h3>
						</td>
					</tr>
					<tr>
						<td>
							<h3>Lorem ipsum atdolor sim asimet.</h3>
						</td>
						<td>
							<h3>16</h3>
						</td>
					</tr>
					<tr>
						<td>
							<h3>Lorem ipsum atdolor sim asimet.</h3>
						</td>
						<td>
							<h3>18</h3>
						</td>
					</tr>
					<tr>
						<td>
							<h3>Lorem ipsum atdolor sim asimet.</h3>
						</td>
						<td>
							<h3>19</h3>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</center>

</div>


<div class="page">

	<?php
	ponTitulo();
	?>

	<center>
		
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<h3>Mensaje del Presidente de la Corporaci�n</h3>
		<p>&nbsp;</p>
		
		<div style="width: 640px; height: 400px; background-image: url('./img/fondo-05.png');">
			<div style="height: 40px"></div>
			<div style="width: 459px; height: 263px; background-image: url('./img/custom-image-1.jpg');">
				<div style="height: 180px"></div>
				<p style="color: white; text-align: right; margin-right: 10px;"><strong>Pedro Perez</strong></p>
				<p style="color: white; text-align: right; margin-right: 10px;">Presidente</p>
			</div>
		</div>
		
		<p>&nbsp;</p>
		<p>&nbsp;</p>

		<div style="width: 900px;">
			<div class="col1_2">
				<div class="first_letter" style="color: blue;">N</div>
				<p>os enorgullece presentar el cuarto Reporte de Sostenibilidad consecutivo que realiza Florida Ice & Farm Co. (FIFCO), en un esfuerzo por medir y dar a conocer las acciones que emprende cada a�o, buscando la excelencia en tres dimensiones: ambiental, social y econ�mica.
				</p><p>
				Este es tambi�n el cuarto a�o consecutivo en que realizamos una consulta a nuestros p�blicos de inter�s, con el fin de conocer sus expectativas y percepciones sobre el papel de FIFCO en el desarrollo sostenible de los pa�ses donde operamos.
				</p><p>
				Esta evoluci�n en nuestra estrategia nos ha permitido profundizar en la minimizaci�n de nuestras huellas y la promoci�n de la sostenibilidad en nuestra cadena de valor, al mismo tiempo que desarrollamos una estrategia de inversi�n social e incursionamos en nuevos modelos de creaci�n de valor social y ambiental.
				</p><p>
				Esta consulta nos confirma, una vez m�s, que los retos de promover el consumo responsable de bebidas alcoh�licas, el manejo adecuado de los residuos s�lidos y del agua, contin�an siendo los m�s relevantes para el �xito de nuestra empresa en el largo plazo. 
				</p>
			</div>
			<div class="col2_2">
				<p>
				En las dimensiones ambiental y social, el per�odo reportado se caracteriz� por la ruptura del paradigma de que la �nica posibilidad como empresa es la reducci�n de sus huellas. Es as� como iniciamos con la construcci�n de un nuevo paradigma en el que podemos crear valor, no solo econ�mico, sino tambi�n ambiental y social.
				</p><p>
				En la dimensi�n social interna, que tiene como fin el desarrollo integral de nuestros colaboradores, logramos completar m�s de 123.000 horas de capacitaci�n, mejoramos significativamente nuestros �ndices de salud y seguridad ocupacional, y desarrollamos un programa de formaci�n de mandos medios denominado "L�deres Florida", que nos permiti� dar pasos firmes hacia convertirnos en un empleador de preferencia.
				</p><p>
				En la dimensi�n social externa, continuamos la implementaci�n de nuestra estrategia para promover el consumo responsable de alcohol, a trav�s de campa�as masivas que impulsan la moderaci�n entre los adultos sanos que deciden tomar y rogramas para evitar el consumo de alcohol por parte de poblaciones sensibles (menores de edad, mujeres embarazadas o con condiciones especiales de salud y personas que van a manejar).
				</p>
			</div>

			<p style="text-align: right"><strong>Wilhem Steinvorth Herrera</strong><br>
			Presidente de la Junta Directiva<br>
			Florida Ice & Farm Co.</p>

		</div>

	</center>

</div>


<div class="page">

	<?php
	ponTitulo();
	?>

	<center>
		
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p class="titulo"><span class="titulo_parte1">Nuestra estrategia: </span><span class="titulo_parte2">Crecer sosteniblemente</span></p>
		<p>&nbsp;</p>
		
		<div style="width: 759px; height: 456px; background-image: url('./img/custom-image-2.jpg');"></div>
		
		<p>&nbsp;</p>
		<p>&nbsp;</p>

		<div style="width: 900px;">
			<div class="col1_3">
				<p>El a�o 2012 estuvo marcado por un importante crecimiento en todas las dimensiones de la empresa. Al mismo tiempo que FIFCO incursion� en nuevos negocios, logr� evolucionar en su modelo de sostenibilidad para acercarse m�s a la visi�n de ser una empresa total de bebidas, que crea valor econ�mico, social y ambiental para sus p�blicos de inter�s. </p>
			</div>
			<div class="col2_3">
				<ul>
					<li>Adquisici�n del negocio de Musmanni (diciembre 2011)</li>
					<li>Alianza con Coopeleche y creaci�n de Florida L�cteos (marzo 2012)</li>
					<li>Alianza con Diageo y creaci�n de Florida Vinos y Destilados (julio 2012)</li>
					<li>Adquisici�n de North American Breweries (hecho subsecuente - Noviembre 2012)</li>
					<li>Separaci�n de activos en Pen�nsula Papagayo (enero 2012)</li>
				</ul>
			</div>
			<div class="col3_3"></div>
			<div class="fin_col"></div>
		</div>

	</center>

</div>


<div class="page">

	<?php
	ponTitulo();
	?>

	<center>
		
		<p>&nbsp;</p>

		<div style="width: 900px;">
			
			<div class="col1_2">
				<p><img src="./img/line-1.jpg"></p>
				<p><span class="titulo2">1. Minimizaci�n de huellas y b�squeda de la neutralidad ambiental y social</span></p>
			</div>
			<div class="col2_2"></div>
			<div class="fin_col"></div>
			
			<p>&nbsp;</p>

			<div class="col1_3">
				<p>De acuerdo con la consulta realizada a nuestros p�blicos de inter�s, el consumo nocivo de alcohol, los residuos s�lidos, el consumo de agua y energ�a, y las emisiones de carbono contin�an siendo las principales huellas sociales y ambientales de FIFCO. Con el fin de reducirlas al m�nimo posible y, al mismo tiempo, trabajar en su compensaci�n, se desarrollaron 5 iniciativas estrat�gicas:
				</p>
				<p><span class="titulo2">a. Moderaci�n y consumo responsable de alcohol</span></p>
				<p>
				Para promover la moderaci�n en aquellos adultos sanos que deciden consumir alcohol, se realizaron durante este a�o campa�as masivas de informaci�n y concientizaci�n. Al mismo tiempo, se impuls� el no consumo de alcohol por parte de poblaciones sensibles (especialmente menores de edad y personas que van a conducir) a trav�s de iniciativas como "Se Busca el que vende alcohol a menores", Operativos Peat�n, y "Yo vs. Yo", un programa avalado por el Ministerio de Educaci�n P�blica.
				</p><p>
				Adicionalmente, se colaboradores como capacit� a embajadores los del consumo responsable y a 450 clientes para convertirlos en guardianes del consumo moderado en el punto de venta, as� como con l�deres de opini�n y medios de comunicaci�n, que pueden tener un impacto significativo en la promoci�n del consumo responsable de alcohol.			
				</p>	
			</div>
			<div class="col2_tercios">
				<img src="./img/custom-image-3.jpg" style="width: 100%; padding: 20px 35px;">
			</div>
			<div class="fin_col"></div>
			
		</div>

	</center>

</div>



<div class="page">

	<?php
	ponTitulo();
	?>

	<center>
		
		<p>&nbsp;</p>

		<div style="width: 900px;">
			
			<div class="col1_3">
				<p><img src="./img/custom-image-4.jpg" style="width: 90%"></p>
				<p><img src="./img/custom-image-5.jpg" style="width: 90%"></p>
			</div>
			<div class="col2_tercios">
				
				<p><img src="./img/custom-image-6.jpg" style="width: 100%"></p>
				
				<div class="col1_2">
					<p><span class="titulo2">b. Cero Residuos S�lidos</span></p>
					<p>El manejo adecuado de los residuos s�lidos es una de las prioridades ambientales de FIFCO. Es por esto que trabajamos en dos iniciativas claves: Cero Residuos y Reciclaje.
					</p><p>
					Al cierre del a�o 2012, el 99% de todos los residuos s�lidos generados en nuestras instalaciones (plantas de producci�n, centros de distribuci�n, etc.) se reutilizaron, reciclaron o se usaron para alimentaci�n animal o producci�n de energ�a. Ello significa que solamente el 1% de los residuos se envi� a un relleno sanitario. Para compensar estos residuos no valorizables, se realizaron 21 jornadas de recolecci�n de residuos en comunidades, playas y r�os de Costa Rica. En cuanto a los residuos s�lidos post-consumo, la meta de Florida es llegar a reciclar el 100% de los envases que se colocan en el mercado. En el a�o 2012, el porcentaje fue de 52%; ello equivale</p>	
					</p>
				</div>
				<div class="col2_2">
					<p>a 3,6 millones de kilogramos, 760 toneladas m�tricas m�s que lo reciclado el a�o anterior. Espec�ficamente, se logr� recuperar m�s del 40% del pl�stico PET, 50% de las latas de aluminio, 61% de los empaques de tetrapak y un 186% del pl�stico HDPE que Florida coloc� en el mercado durante el 2012.
					</p>
					<p><span class="titulo2">c. Agua Neutral 2012</span></p>
					<p>
					A finales del a�o 2012, Florida Bebidas se convirti� en la primera empresa Agua Neutral de la regi�n, al lograr medir su huella operativa de agua (33,9 millones de hectolitros que equivale a 5,46 hectolitros de agua por cada hectolitro de bebida producido), reducirla significativamente (en 1,1 millones de hectolitros) y compensar externamente por medio de la protecci�n de zonas de recarga acu�fera (777 hect�reas en Costa Rica) y la construcci�n de soluciones de agua para poblaciones que no tienen acceso a este recurso vital (mejoras significativas al acueducto de Brasilito en Guanacaste).
					</p>
				</div>

			</div>
			<div class="fin_col"></div>
			
		</div>

	</center>

</div>

<?php
}
?>

</body>
</html>


<?php

/*

	$cmd = "wkhtmltopdf  --viewport-size 1024x768 --orientation Portrait http://localhost/generate.php --post p " . escapeshellarg($param) . " /var/www/pdf/public/" . $pdf_file_name . "  2>&1";

	# execute the command, and store the return value ($retval) and the last line
	exec($cmd, $retval, $last_line);

	
	
	echo "<pre>command 33:<br>" . $cmd . "<br><br>";
	var_dump($retval);
	var_dump($last_line);
	
	# we expect the last line to be "Done" on success
	if($last_line == "Done"){
		# return the URL of the PDF
		echo json_encode($public_output_path . $pdf_file_name);
	}else{
		#lame error reporting
		#echo "Hmm. Something went wrong.";
		echo json_encode("0");
	}


 */
?>
