<pre>
<?php

/*
 * Script de generación de PDFs
 * 
 */

// Constants to define:

// produccion? o local?
define('PRODUCCION', true);

// save pdfs to:
define('PATH_TO_LOCAL_PDF', dirname(__FILE__).'/pdf/');

if (PRODUCCION) {

	 /*
	 * Ejemplo:
	 * http://css.red111.net/app/webroot/css-pdf/code/generate_pdf.php?url=http://css.red111.net/app/webroot/css-pdf/code/generate_html.php&pdf=test.pdf
	 */
 
	// show pdfs in:
	define('URL_TO_LOCAL_PDF', 'http://css.red111.net/app/webroot/css-pdf/code/pdf/');

	// wkhtmltopdf command location:
	define('WKHTMLTOPDF', '../vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64'); // Hostgator RedHat version

} else {

	 /*
	 * Ejemplo:
	 * http://css.local/app/webroot/css-pdf/code/generate_pdf.php?url=http://css.local/app/webroot/css-pdf/code/generate_html.php&pdf=test.pdf
	 */
	 
	// show pdfs in:
	define('URL_TO_LOCAL_PDF', 'http://css.local/app/webroot/css-pdf/code/pdf/');
	
	// wkhtmltopdf command location:
	define('WKHTMLTOPDF', 'wkhtmltopdf'); // ubuntu local version

}

// solo se permiten numeros, letras, puntos y underscore en el nombre
if($filename = $_GET["pdf"] and preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $filename))
{
	// http://css.local/app/webroot/css-pdf/code/generate_html.php ./test.pdf  2>&1

	$cmd = WKHTMLTOPDF . " --orientation Portrait " . escapeshellarg($_GET["url"]) . " ". escapeshellarg(PATH_TO_LOCAL_PDF . $filename) . " 2>&1";
	// $cmd = 'xvfb-run ' .WKHTMLTOPDF . " --orientation Portrait " . escapeshellarg($_GET["url"]) . " ". escapeshellarg(PATH_TO_LOCAL_PDF . $filename) . " 2>&1";

	$ini_time = time();

	# execute the command, and store the return value ($retval) and the last line
	exec($cmd, $retval, $last_line);

	# we expect the last line to be "Done" on success
	if($last_line == "Done"){
		# return the URL of the PDF
		$result = URL_TO_LOCAL_PDF . $filename;
		echo "<p><a href=\"$result\">$result</a></p>";
		$secs = time() - $ini_time;
		echo "<p>Generated in $secs secs</p>";
	}else{
		echo "<p>command: $cmd</p><p>retval: " . var_dump($retval) . "</p><p>last_line" . var_dump($last_line) . "</p>";
	}

}
else
{
    echo "Incorrect filename";
}

?>
</pre>
