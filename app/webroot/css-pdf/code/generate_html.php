<?php

// parametro para pruebas de generacion:
if (isset($_GET['n'])) {
	$repetir=$_GET["n"];
if (!$repetir and !is_numeric($repetir)) {
	$repetir=1;
}
} else {
	$repetir = 2;
}


/****************************************************************************
 *
 * Inicialización
 *  
 ***************************************************************************/
 
/*
 *  Load CSS Library / class
 */
 
include_once("./html_generator/html_generator.php");

/*
 * 01 es el diseño seleccionado tanto para el header como para el estilo general del sitio,
 * luego viene el titulo y subtitulo del header
 */

$pdf = new htmlGenerator('01', "REPORTE DE SOSTENIBILIDAD 2016", "FLORIDA ICE & FARM Co.");

/*
 * Inicializar el header html con los estilos basicos
 */
 
echo $pdf -> init();


for ($i=1; $i<$repetir; $i++) { // parametro de pruebas (repetir)

/****************************************************************************
 * Luego viene la lógica de todo el PDF, 
 * se debe iterar por todas las páginas del PDF y mostrarlas una a una.
 * A continuacion algunas muestras de paginas con sus templates
 ***************************************************************************/


/*
 * 
 * TEMPLATE: Portada 01
 * 
 * Datos solicitados al usuario:
 * 
 * 		img (superior)
 * 
 * 		bg_image
 * 		titulo
 * 		titulo_align
 * 		titulo_color
 * 		subtitulo
 * 		sub_titulo_align
 * 		sub_titulo_color
 * 
 * 		titulo (inferior)
 * 
 * Datos calculados por el sistema:
 * 
 * 		bg_image_width (tomado del bg_image)
 * 		bg_image_height
 * 
 */

echo $pdf->renderTemplate('portada01', [

	"img"				=> "./img/logo.jpg",
	
	"bg_image"			=> "./img/custom-image-0.jpg", 
	"bg_image_width"	=> "932px",
	"bg_image_height"	=> "743px",
	"titulo"			=> "Reporte Anual de Sostenibilidad | 2016",
	"titulo_align"		=> "center",
	"titulo_color"		=> "white",
	"subtitulo"			=> "Crecemos sosteniblemente",
	"sub_titulo_align"	=> "center",
	"sub_titulo_color"	=> "white",	
	
	"titulo_pie"		=> "Certificado A+ por el Global Reporting Initiative"

]);

/*
 * 
 * TEMPLATE: Galeria de imágenes con tres columnas
 * 
 * Datos solicitados al usuario:
 * 		
 * 		titulo (de la pagina)
 * 
 * 		titulo de la galeria columna 1
 * 		bg_image
 * 
 * 		images: imgs, textos, links
 * 
 * 		column2, column3: texto escrito por el usuario (WYSIWYG)
 * 		
 * Datos calculados por el sistema:
 * 
 * 		bg_image_width (tomado del bg_image)
 * 		bg_image_height
 * 
 */
 
echo $pdf->renderTemplate('galeria_3_columnas', [
	
	"titulo"			=> "Cuarto Reporte de Sostenibilidad",
	
	"subtitulo"			=>	"Nuestros Reportes Anteriores",
	"bg_image"			=>	"./img/fondo-02.jpg",
	"bg_image_height"	=>	"918px",
	"bg_image_width"	=>	"280px",
	"images"			=>	[
		[	"img"	=>	"./img/portada1.jpg",
			"txt"	=>	"2009 | Nivel B",
			"link"	=>	"http://google.com"
		],
		[	"img"	=>	"./img/portada2.jpg",
			"txt"	=>	"2010 | Nivel A+",
			"link"	=>	"http://google.com"
		],
		[	"img"	=>	"./img/portada3.jpg",
			"txt"	=>	"2011 | Nivel A",
			"link"	=>	"http://google.com"
		],
	],
	"column2"	=> "<p>Por cuarto año consecutivo, <strong>Florida Ice & Farm Co. (FIFCO)</strong> presenta su Reporte de Sostenibilidad, con el fin de informar a sus públicos de inter�s y a la sociedad en general sobre sus avances en el camino hacia el desarrollo sostenible. </p>
					<p>Para FIFCO, la <strong>sostenibilidad</strong> es un tema estrat�gico; es una forma de hacer negocios que permea todas sus esferas de acci�n y que permite su crecimiento a largo plazo, en armon�a con las personas, las comunidades y el medio ambiente.</p>
					<p>Guiada por las expectativas de sus <strong>p�blicos de inter�s</strong>, a quienes se consulta cada a�o, la empresa trabaja con rigurosidad no solo para reducir sus huellas, sino tambi�n para crear valor en <strong>tres dimensiones</strong> (ambiental, social y econ�mica) con un enfoque de medici�n de progreso y de rendici�n de cuentas.</p>
					<p>Es por ello que el presente reporte anual recopila m�s de <strong>70 indicadores</strong> ambientales, sociales y econ�micos, relacionados directamente con el impacto de la empresa, correspondientes al per�odo fiscal <strong>octubre 2011-setiembre 2012</strong>, de todas las operaciones de FIFCO en Costa Rica, Guatemala y El Salvador: Florida Bebidas, Industrias Alimenticias Kern�s y Florida Inmobiliaria (Reserva Conchal).</p>",
	"column3"	=> "<p>Se a�ade, adem�s, las nuevas divisiones de negocio que se incorporaron durante el per�odo reportado: Florida L�cteos, Florida Vinos y Destilados, y Musmanni, divisi�n dedicada a la elaboraci�n de productos de panader�a y su distribuci�n mediante el modelo de franquicia.</p>
					<p>Dada la reciente incorporaci�n de estas divisiones a FIFCO, se reportar�n �nicamente aquellos indicadores que se encuentran ya cuantificados y se establecer� un cronograma para aquellos en proceso de construcci�n.</p>
					<p>Para recopilar esta informaci�n, se utilizaron los Protocolos de Indicadores del GRI. Adem�s, al ser FIFCO una empresa p�blica, se emplean sistemas robustos de informaci�n y de medici�n de resultados en sus tres dimensiones.</p>
					<p>No se incluye en este reporte a la empresa cervecera estadounidense North American Breweries (NAB), que FIFCO adquiri� en octubre del 2012, pues la transacci�n se encuentra fuera del per�odo reportado.</p>
					<p>Al presentar este cuarto Reporte de Sostenibilidad, no solo cumplimos con nuestro objetivo de la dimensi�n Social Externa de informar en forma �tica y transparente de nuestro progreso, sino que esperamos que todos nuestros p�blicos puedan llegar a conocer y utilizar este reporte y la informaci�n que contiene.</p>",
	
	
]);

/*
 * 
 * TEMPLATE: imagen a p�gina completa
 * 
 * Datos solicitados al usuario:
 * 
 * 		img
 * 		text-align
 * 
 */

echo $pdf->renderTemplate('imagen_pagina', ["img" => "./img/calificacion.png", "text-align" => "center"]);

/* 
 * 
 * TEMPLATE: Tabla 01, dos columnas
 * 
 * Datos solicitados al usuario:
 * 
 * 		bg_image
 * 		titulo
 * 		filas a dos columnas
 * 
 */

echo $pdf->renderTemplate('tabla_01', [
	"bg_image"	=> "./img/fondo-04.jpg",
	"titulo"	=> "Contenido",
	"tabla"		=> [
		["Mensaje del Presidente de la Junta Directiva", 6],
		["Nuestra Estrategia: Crecer Sosteniblemente", 8],
		["Lorem ipsum atdolor sim asimet.", 10],
		["Lorem ipsum atdolor sim asimet.", 11],
		["Lorem ipsum atdolor sim asimet.", 13],
		["Lorem ipsum atdolor sim asimet.", 16],
	]
]);

/* 
 * 
 * TEMPLATE: Titulo, imagen y texto a dos columnas
 * 
 * Datos solicitados al usuario:
 * 
 * 		titulo
 * 		subtitulo
 * 		subtitulo2
 * 
 * 		bg_image
 * 		text-align (img)
 * 
 * 		column1, column2 (WYSIWYG)
 * 
 */

echo $pdf->renderTemplate('imagen_2_columnas', [
	"bg_image"			=> "./img/custom-image-7.jpg",
	"bg_image_width"	=> "548px",
	"bg_image_height"	=> "400px",
	"titulo"			=> "Mensaje del Presidente de la Corporaci�n",
	"subtitulo"			=> "Pedro Perez",
	"subtitulo_align"	=> "right",
	"subtitulo_color"	=> "blue",
	"subtitulo2"		=> "Presidente",
	"subtitulo2_align"	=> "right",
	"subtitulo2_color"	=> "blue",
	"column1"	=>	"<p>Nos enorgullece presentar el cuarto Reporte de Sostenibilidad consecutivo que realiza Florida Ice & Farm Co. (FIFCO), en un esfuerzo por medir y dar a conocer las acciones que emprende cada a�o, buscando la excelencia en tres dimensiones: ambiental, social y econ�mica.
					</p><p>
					Este es tambi�n el cuarto a�o consecutivo en que realizamos una consulta a nuestros p�blicos de inter�s, con el fin de conocer sus expectativas y percepciones sobre el papel de FIFCO en el desarrollo sostenible de los pa�ses donde operamos.
					</p><p>
					Esta evoluci�n en nuestra estrategia nos ha permitido profundizar en la minimizaci�n de nuestras huellas y la promoci�n de la sostenibilidad en nuestra cadena de valor, al mismo tiempo que desarrollamos una estrategia de inversi�n social e incursionamos en nuevos modelos de creaci�n de valor social y ambiental.
					</p><p>
					Esta consulta nos confirma, una vez m�s, que los retos de promover el consumo responsable de bebidas alcoh�licas, el manejo adecuado de los residuos s�lidos y del agua, contin�an siendo los m�s relevantes para el �xito de nuestra empresa en el largo plazo. 
					</p>",
	"column2"	=> "<p>
					En las dimensiones ambiental y social, el per�odo reportado se caracteriz� por la ruptura del paradigma de que la �nica posibilidad como empresa es la reducci�n de sus huellas. Es as� como iniciamos con la construcci�n de un nuevo paradigma en el que podemos crear valor, no solo econ�mico, sino tambi�n ambiental y social.
					</p><p>
					En la dimensi�n social interna, que tiene como fin el desarrollo integral de nuestros colaboradores, logramos completar m�s de 123.000 horas de capacitaci�n, mejoramos significativamente nuestros �ndices de salud y seguridad ocupacional, y desarrollamos un programa de formaci�n de mandos medios denominado \"L�deres Florida\", que nos permiti� dar pasos firmes hacia convertirnos en un empleador de preferencia.
					</p><p>
					En la dimensi�n social externa, continuamos la implementaci�n de nuestra estrategia para promover el consumo responsable de alcohol, a trav�s de campa�as masivas que impulsan la moderaci�n entre los adultos sanos que deciden tomar y rogramas para evitar el consumo de alcohol por parte de poblaciones sensibles (menores de edad, mujeres embarazadas o con condiciones especiales de salud y personas que van a manejar).
					</p>
					<p style=\"text-align: right\"><strong>Wilhem Steinvorth Herrera</strong><br>
					Presidente de la Junta Directiva<br>
					Florida Ice & Farm Co.</p>"
]);


/* 
 * 
 * TEMPLATE: Titulo, imagen y texto a tres columnas
 * 
 * Datos solicitados al usuario:
 * 
 * 		titulo
 * 		subtitulo
 * 
 * 		img
 * 		text-align (img)
 * 
 * 		column1, column2, column3 (WYSIWYG)
 * 
 */

echo $pdf->renderTemplate('imagen_3_columnas', [
	"bg_image"			=> "./img/custom-image-2.jpg",
	"titulo"			=> "Nuestra estrategia:",
	"subtitulo"			=> "Crecer sosteniblemente",
	"column1"	=> "<p>El a�o 2012 estuvo marcado por un importante crecimiento en todas las dimensiones de la empresa. Al mismo tiempo que FIFCO incursion� en nuevos negocios, logr� evolucionar en su modelo de sostenibilidad para acercarse m�s a la visi�n de ser una empresa total de bebidas, que crea valor econ�mico, social y ambiental para sus p�blicos de inter�s. </p>",
	"column2"	=> "<ul>
					<li>Adquisici�n del negocio de Musmanni (diciembre 2011)</li>
					<li>Alianza con Coopeleche y creaci�n de Florida L�cteos (marzo 2012)</li>
					<li>Alianza con Diageo y creaci�n de Florida Vinos y Destilados (julio 2012)</li>
					<li>Adquisici�n de North American Breweries (hecho subsecuente - Noviembre 2012)</li>
					<li>Separaci�n de activos en Pen�nsula Papagayo (enero 2012)</li>
					</ul>",
	"column3"	=> ""
]);


/* 
 * 
 * TEMPLATE: Titulo, texto e imagen
 * 
 * Datos solicitados al usuario:
 * 
 * 		titulo
 * 
 * 		column1(WYSIWYG)
 * 
 * 		img
 * 
 */

echo $pdf->renderTemplate('titulo_texto_imagen', [
	"titulo"			=> "1. Minimizaci�n de huellas y b�squeda de la neutralidad ambiental y social",
	"column1"	=> "<p>De acuerdo con la consulta realizada a nuestros p�blicos de inter�s, el consumo nocivo de alcohol, los residuos s�lidos, el consumo de agua y energ�a, y las emisiones de carbono contin�an siendo las principales huellas sociales y ambientales de FIFCO. Con el fin de reducirlas al m�nimo posible y, al mismo tiempo, trabajar en su compensaci�n, se desarrollaron 5 iniciativas estrat�gicas:</p>
															<p>
															Para promover la moderaci�n en aquellos adultos sanos que deciden consumir alcohol, se realizaron durante este a�o campa�as masivas de informaci�n y concientizaci�n. Al mismo tiempo, se impuls� el no consumo de alcohol por parte de poblaciones sensibles (especialmente menores de edad y personas que van a conducir) a trav�s de iniciativas como \"Se Busca el que vende alcohol a menores\", Operativos Peat�n, y \"Yo vs. Yo\", un programa avalado por el Ministerio de Educaci�n P�blica.
															</p><p>
															Adicionalmente, se colaboradores como capacit� a embajadores los del consumo responsable y a 450 clientes para convertirlos en guardianes del consumo moderado en el punto de venta, as� como con l�deres de opini�n y medios de comunicaci�n, que pueden tener un impacto significativo en la promoci�n del consumo responsable de alcohol.			
															</p>",

	"bg_image"			=> "./img/custom-image-3.jpg"
]);

/* 
 * 
 * TEMPLATE: Titulo, texto e imagen
 * 
 * Datos solicitados al usuario:
 * 
 * 		titulo
 * 
 * 		column1(WYSIWYG)
 * 
 * 		img
 * 
 */

echo $pdf->renderTemplate('texto_3_imagenes', [
	"column1"	=> "<p>b. Cero Residuos S�lidos</p>
					<p>El manejo adecuado de los residuos s�lidos es una de las prioridades ambientales de FIFCO. Es por esto que trabajamos en dos iniciativas claves: Cero Residuos y Reciclaje.
					</p><p>
					Al cierre del a�o 2012, el 99% de todos los residuos s�lidos generados en nuestras instalaciones (plantas de producci�n, centros de distribuci�n, etc.) se reutilizaron, reciclaron o se usaron para alimentaci�n animal o producci�n de energ�a. Ello significa que solamente el 1% de los residuos se envi� a un relleno sanitario. Para compensar estos residuos no valorizables, se realizaron 21 jornadas de recolecci�n de residuos en comunidades, playas y r�os de Costa Rica. En cuanto a los residuos s�lidos post-consumo, la meta de Florida es llegar a reciclar el 100% de los envases que se colocan en el mercado. En el a�o 2012, el porcentaje fue de 52%; ello equivale</p>	
					</p>",
	"column2"	=> "<p>a 3,6 millones de kilogramos, 760 toneladas m�tricas m�s que lo reciclado el a�o anterior. Espec�ficamente, se logr� recuperar m�s del 40% del pl�stico PET, 50% de las latas de aluminio, 61% de los empaques de tetrapak y un 186% del pl�stico HDPE que Florida coloc� en el mercado durante el 2012.
					</p><p>c. Agua Neutral 2012</p>
					<p>A finales del a�o 2012, Florida Bebidas se convirti� en la primera empresa Agua Neutral de la regi�n, al lograr medir su huella operativa de agua (33,9 millones de hectolitros que equivale a 5,46 hectolitros de agua por cada hectolitro de bebida producido), reducirla significativamente (en 1,1 millones de hectolitros) y compensar externamente por medio de la protecci�n de zonas de recarga acu�fera (777 hect�reas en Costa Rica) y la construcci�n de soluciones de agua para poblaciones que no tienen acceso a este recurso vital (mejoras significativas al acueducto de Brasilito en Guanacaste).</p>",
	"image1"	=> "./img/custom-image-4.jpg",
	"image2"	=> "./img/custom-image-5.jpg",
	"image3"	=> "./img/custom-image-6.jpg",
]);


/****************************************************************************
 * END
 ***************************************************************************/

} // parametro de pruebas (repetir)
 
echo $pdf -> end();
