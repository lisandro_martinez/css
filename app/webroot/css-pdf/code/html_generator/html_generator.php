<?php

/*
 * 
 * Esta clase tiene la lógica para generar todo el html que será usado como fuente para generar un pdf
 * 
 * Los métodos: tpl_pag_* son los templates de cada página
 * El metodo: renderTemplate es el que renderiza esas páginas y les manda los parámetros recibidos
 * init y end se usan para generar el inicio y el fin del html
 * encabezado se usa al inicio de cada template si el template incluye encabezado
 * 
 */

class htmlGenerator {
	
	const TYPE_IMG 			= 'img';
	const TYPE_TEXT 		= 'txt';
	const TYPE_WYSIWYG		= 'wysiwyg';
	const TYPE_TEXT_SIMPLE	= 'raw_text';
	const TYPE_TABLE 		= 'table';

	private $design, $title, $subtitle, $numpag, $m, $template_dir, $lang;
	
	function __construct($design, $title, $subtitle) {

		/*
		 *  Load Mustache Library
		 */

		$this->template_dir = dirname(dirname(__FILE__)).'/templates/';
		
		require dirname(dirname(__FILE__)) . '/lib/Mustache/Autoloader.php';
		Mustache_Autoloader::register();
		$this->m = new Mustache_Engine;

		/*
		 * Ini vars
		 */

		$this->design = $design;
		$this->title = $title;
		$this->subtitle = $subtitle;
	}

	function encabezado() {

		return $this->m->render(	
							file_get_contents($this->template_dir.'encabezado_'.$this->design.'.html'), 
							[	"numero_pagina"=>$this->numpag, 
								"titulo"=>$this->title, 
								"subtitulo"=>$this->subtitle
							]);

	}
	
	public function init() {

		return <<<INIT

<!DOCTYPE html>
<meta charset="UTF-8">
<html lang="en">

<head>
<link href="/css-pdf/code/templates/style.css" media="all" rel="stylesheet" />
<link href="/css-pdf/code/templates/style_{$this->design}.css" media="all" rel="stylesheet" />
</head>

<body>
			
INIT;
			
	}

	public function end() {
		
		return <<<END
</body>
</html>
END;

	}
	
	/*
	 * Esta función renderiza un template ($template) llamando a la función respectiva: tpl_pag_$template
	 * pasandole $params como parámetro
	 */
	 
	public function renderTemplate($template, $params, $lang) {
		
		$this->lang = $lang;
		
		$this->numpag++; // incrementa el numero de página en cada renderización
		
		$function = 'tpl_pag_'.$template;
		if (method_exists($this, $function)) {
			return $this->$function($params);
		} else {
			return "<pre>Template: $template not found</pre>";
		}
	}
	
	/*
	 * 
	 * 		helper function to return all templates: all function names that start with: tpl_pag_XXXX return XXXX
	 * 		With a given url for thumbnail
	 * 			
	 * 			Example: 
	 * 
	 * 				{
	 * 					"portada01": {
	 *			 						"tag": "portada01",
	 * 									"name": "Portrait 01",
	 * 									"thumbnail": "path/to/thumbnail.png"
	 * 					},
	 * 					"portada02": {
	 *			 						"tag": "portada02",
	 * 									"name": "Portrait 02",
	 * 									"thumbnail": "path/to/thumbnail.png"
	 * 					}
	 * 				}
	 *
	 * 		2.- Add helper function to return json definition for a given template: {field:data_type:label_esp:label_eng}
	 * 
	 * 			Available data types:
	 * 
	 * 				- img: asks for a file
	 * 				- text: simple text (any length)
	 * 				- dropdown: list of predefined options
	 * 				- color: a color picker
	 * 				- wysiwyg: summernote text field
	 * 				- css_object: TBD are going to be objects taken from CSS system (graphs, indicators, etc)
	 * 
	 * 			Example for portada01, it must return:
	 * 
	 * 				notes: 
	 * 
	 * 					label is in english, must be translated in locale
	 * 					value can be a default value or a dropdown list
	 * 
	 * 				{
	 * 					"img": {
	 * 								"type": "file",
	 * 								"label": "Superior Image",
	 * 					},
	 * 					"bg_image": {
	 * 								"type": "file",
	 * 								"label": "Background image"
	 *					}
	 * 					"titulo": {
	 * 								"type": "text",
	 * 								"label": "Main title",
	 * 					}
	 * 				}
	 */
	 
	 public function reflection() {
		 
		 // find tags
		 $tags = [];
		 $reflector = new ReflectionClass('htmlGenerator');

		 foreach($reflector->getMethods() as $method) {
			 
			 if (substr($method->name, 0, 8) == 'tpl_pag_') {
				 
				$tag=substr($method->name, 8);

				$name = $method->name;
				
				$tags[$tag]=self::$name(null, true);

			 }

		 }

		 ksort($tags);
		 
		 // ask each method for his data
		 
		 return $tags;
		 
	 }		
	 
	 public function response(	$names, $params) {
		 return [	'names'		=> ['eng'=>$names[0], 'esp'=>$names[1]],
					'params'	=> $params];
	 }
	 
	/*
	 * 
	 * TEMPLATE: Portada 01
	 * 
	 * Datos solicitados al usuario:
	 * 
	 * 		img (superior)
	 * 
	 * 		bg_image
	 * 		titulo
	 * 		titulo_align
	 * 		titulo_color
	 * 		subtitulo
	 * 		sub_titulo_align
	 * 		sub_titulo_color
	 * 
	 * 		titulo (inferior)
	 * 
	 * Datos calculados por el sistema:
	 * 
	 * 		bg_image_width (tomado del bg_image)
	 * 		bg_image_height
	 * 
	 */

	private function tpl_pag_portada01($params, $ask=false) {

		if ($ask) return self::response(	['Portrait', 'Portada'], 
											[
												'img' 				=> 	[
																			'type'	=> self::TYPE_IMG,
																			'label'	=> ['esp' => 'Logo', 'eng' => 'Logo']
																		],
											 	'bg_image'			=> 	[
																			'type'	=> self::TYPE_IMG,
																			'label'	=> ['esp' => 'Imagen de fondo', 'eng' => 'Background image']
																		],
												'titulo'			=> 	[
																			'type'	=> self::TYPE_TEXT,
																			'label'	=> ['esp' => 'Título', 'eng' => 'Title']
																		],
												'subtitulo'			=> 	[
																			'type'	=> self::TYPE_TEXT,
																			'label'	=> ['esp' => 'Subtítulo', 'eng' => 'Subtitle']
																		],
												'titulo_pie'		=> 	[
																			'type'	=> self::TYPE_TEXT,
																			'label'	=> ['esp' => 'Texto al pie', 'eng' => 'Footer text']
																		],
											]
		);

		return $this->m->render(
				file_get_contents($this->template_dir.'page.html'), 
				[	"html"		=> 
						$this->m->render(	
									file_get_contents($this->template_dir.'img_01.html'), 
									[	"img"				=> $params["img"][$this->lang]
						]) .
						$this->m->render(	
									file_get_contents($this->template_dir.'img_txt_01.html'), 
									[	"extra"				=> "min-height: 1000px;",
										"bg_image"			=> $params["bg_image"][$this->lang], 
										"titulo"			=> $params["titulo"][$this->lang],
										"titulo_align"		=> $params["titulo_align"][$this->lang],
										"titulo_color"		=> $params["titulo_color"][$this->lang],
										"subtitulo"			=> $params["subtitulo"][$this->lang],
						]) .
						$this->m->render(	
									file_get_contents($this->template_dir.'titulo_03.html'), 
									[	"titulo"			=> $params["titulo_pie"][$this->lang]
						])
		]);
		
	}
	
	
	/*
	 * 
	 * TEMPLATE: Galeria de imágenes con tres columnas
	 * 
	 * Datos solicitados al usuario:
	 * 		
	 * 		titulo (de la pagina)
	 * 
	 * 		titulo de la galeria columna 1
	 * 		bg_image
	 * 
	 * 		images: imgs, textos, links
	 * 
	 * 		column2, column3: texto escrito por el usuario (WYSIWYG)
	 * 		
	 * Datos calculados por el sistema:
	 * 
	 * 		bg_image_width (tomado del bg_image)
	 * 		bg_image_height
	 * 
	 */
	 
	private function tpl_pag_galeria_3_columnas($params, $ask=false) {

		if ($ask) return self::response(	['Image Index three columns', 'Imágenes en indice a tres columnas'], 
											[
												'titulo'			=> 	[
																			'type'	=> self::TYPE_TEXT,
																			'label'	=> ['esp' => 'Título', 'eng' => 'Title']
																		],
												'subtitulo'			=> 	[
																			'type'	=> self::TYPE_TEXT,
																			'label'	=> ['esp' => 'Subtítulo', 'eng' => 'Subtitle']
																		],
												'bg_image'			=> 	[
																			'type'	=> self::TYPE_IMG,
																			'label'	=> ['esp' => 'Imagen de fondo', 'eng' => 'Background image']
																		],
												'img1'				=> 	[
																			'type'	=> self::TYPE_IMG,
																			'label'	=> ['esp' => 'Imagen 1', 'eng' => 'Image 1']
																		],
												'txt1'				=> 	[
																			'type'	=> self::TYPE_TEXT_SIMPLE,
																			'label'	=> ['esp' => 'Text 1', 'eng' => 'Text 1']
																		],
												'link1'				=> 	[
																			'type'	=> self::TYPE_TEXT_SIMPLE,
																			'label'	=> ['esp' => 'URL 1', 'eng' => 'URL 1']
																		],
												'img2'				=> 	[
																			'type'	=> self::TYPE_IMG,
																			'label'	=> ['esp' => 'Imagen 2', 'eng' => 'Image 2']
																		],
												'txt2'				=> 	[
																			'type'	=> self::TYPE_TEXT_SIMPLE,
																			'label'	=> ['esp' => 'Text 2', 'eng' => 'Text 2']
																		],
												'link2'				=> 	[
																			'type'	=> self::TYPE_TEXT_SIMPLE,
																			'label'	=> ['esp' => 'URL 2', 'eng' => 'URL 2']
																		],
												'img3'				=> 	[
																			'type'	=> self::TYPE_IMG,
																			'label'	=> ['esp' => 'Imagen 3', 'eng' => 'Image 3']
																		],
												'txt3'				=> 	[
																			'type'	=> self::TYPE_TEXT_SIMPLE,
																			'label'	=> ['esp' => 'Text 3', 'eng' => 'Text 3']
																		],
												'link3'				=> 	[
																			'type'	=> self::TYPE_TEXT_SIMPLE,
																			'label'	=> ['esp' => 'URL 3', 'eng' => 'URL 3']
																		],
												'column2'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Texto segunda columna', 'eng' => 'Second column text']
																		],
												'column3'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Texto tercera columna', 'eng' => 'Third column text']
																		],
											]);

		return $this->m->render(
			file_get_contents($this->template_dir.'page.html'), 
			[	"header"	=> $this->encabezado(),	// algunas páginas pueden tener o no encabezado
				"html"		=> 
					$this->m->render(	
								file_get_contents($this->template_dir.'titulo_04.html'), 
								[	"titulo"	=> $params["titulo"][$this->lang]
					]) .
					$this->m->render(
								file_get_contents($this->template_dir.'content_3_columns.html'), 
								[	"column1"	=>	@$this->m->render(	
																file_get_contents($this->template_dir.'galeria_01.html'), 
																[	"titulo"			=>	$params["subtitulo"][$this->lang],
																	"bg_image"			=>	$params["bg_image"][$this->lang],
																	"images"			=>	[
																								[
																									'link'	=> $params["link1"][$this->lang],
																									'img'	=> $params["img1"][$this->lang],
																									'txt'	=> $params["txt1"][$this->lang]
																								],
																								[
																									'link'	=> $params["link2"][$this->lang],
																									'img'	=> $params["img2"][$this->lang],
																									'txt'	=> $params["txt2"][$this->lang]
																								],
																								[
																									'link'	=> $params["link3"][$this->lang],
																									'img'	=> $params["img3"][$this->lang],
																									'txt'	=> $params["txt3"][$this->lang]
																								]
																							]
													]),
									"column2"	=> $params["column2"][$this->lang],
									"column3"	=> $params["column3"][$this->lang],
									"extra_col"	=> "max-height: 1200px",
					])
		]);
		
	}

	/*
	 * 
	 * TEMPLATE: imagen a página completa
	 * 
	 * Datos solicitados al usuario:
	 * 
	 * 		img
	 * 		text-align
	 * 
	 */

	private function tpl_pag_imagen_pagina($params, $ask=false) {

		if ($ask) return self::response( ['Image full page', 'Imagen a toda página'], [
												'img' 				=> 	[
																			'type'	=> self::TYPE_IMG,
																			'label'	=> ['esp' => 'Imagen', 'eng' => 'Image']
																		]
											]);
		
		return $this->m->render(
						file_get_contents($this->template_dir.'page.html'), 
						[	"header"	=>	$this->encabezado(),
							"html"		=>	$this->m->render(
														file_get_contents($this->template_dir.'img_01.html'), 
														[
															"img" 			=> $params["img"][$this->lang],
															"extra_ini"		=> "<div style=\"min-height: 1400px;\">",
															"extra_fin"		=> "</div>",
															"text-align"	=> "center",
															"width"			=> "100%"
														])
		]);
		
	}
	
	/* 
	 * 
	 * TEMPLATE: Tabla 01, dos columnas
	 * 
	 * Datos solicitados al usuario:
	 * 
	 * 		bg_image
	 * 		titulo
	 * 		filas a dos columnas
	 * 
	 */

	private function tpl_pag_tabla_01($params, $ask=false) {

		$rows = 10;
		$cols = 2;

		if ($ask) return self::response( ['Table of contents', 'Tabla de contenidos'], [
											 	'bg_image'			=> 	[
																			'type'	=> self::TYPE_IMG,
																			'label'	=> ['esp' => 'Imagen de fondo', 'eng' => 'Background image']
																		],
												'titulo'			=> 	[
																			'type'	=> self::TYPE_TEXT,
																			'label'	=> ['esp' => 'Título', 'eng' => 'Title']
																		],
												'table'				=> 	[
																			'type'	=> self::TYPE_TABLE,
																			'cols'	=> $cols,
																			'rows'	=> $rows,
																			'label'	=> ['esp' => 'Tabla de contenido', 'eng' => 'Table of contents']
																		],
											]);

		$tabla_datos = [];
		for($row=0; $row<$rows; $row++) {
			$pcol = "A";
			for($col=0; $col<$cols; $col++) {
				$the_row = $row + 1;
				$tabla_datos[$row][$col] = $params["table_".$pcol."_".$the_row][$this->lang];
				$pcol++;
			}
		}

		return $this->m->render(
							file_get_contents($this->template_dir.'page.html'), 
							[	"header"	=> 	$this->encabezado(),
								"html"		=>	@$this->m->render(
															file_get_contents($this->template_dir.'table_01.html'), 
															[
																"bg_image"	=> $params["bg_image"][$this->lang],
																"titulo"	=> $params["titulo"][$this->lang],
																"tabla"		=> $tabla_datos
															]
												)
		]);
		
	}

	/* 
	 * 
	 * TEMPLATE: Titulo, imagen y texto a dos columnas
	 * 
	 * Datos solicitados al usuario:
	 * 
	 * 		titulo
	 * 		subtitulo
	 * 		subtitulo2
	 * 
	 * 		bg_image
	 * 		text-align (img)
	 * 
	 * 		column1, column2 (WYSIWYG)
	 * 
	 */

	private function str_replace_first($from, $to, $subject)
	{
		$from = '/'.preg_quote($from, '/').'/';
		return preg_replace($from, $to, $subject, 1);
	}

	private function tpl_pag_imagen_2_columnas($params, $ask=false) {

		if ($ask) return self::response( 	['Imagen and two columns text', 'Imagen y texto a dos columnas'], [
												'titulo'			=> 	[
																			'type'	=> self::TYPE_TEXT,
																			'label'	=> ['esp' => 'Título superior', 'eng' => 'Superior title']
																		],
											 	'bg_image'			=> 	[
																			'type'	=> self::TYPE_IMG,
																			'label'	=> ['esp' => 'Imagen superior', 'eng' => 'Superior image']
																		],
												'subtitulo'			=> 	[
																			'type'	=> self::TYPE_TEXT,
																			'label'	=> ['esp' => 'Título inferior', 'eng' => 'Inferior title']
																		],
												'subtitulo2'			=> 	[
																			'type'	=> self::TYPE_TEXT,
																			'label'	=> ['esp' => 'Subtítulo inferior', 'eng' => 'Inferior subtitle']
																		],
												'column1'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Texto de la columna 1', 'eng' => 'Column 1 text']
																		],
												'column2'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Texto de la columna 2', 'eng' => 'Column 2 text']
																		],
											]);

		// reemplaza la primera letra con mayuscula
		$column1 = trim(strip_tags($params["column1"][$this->lang]));
		if ($column1) {
			$first10 = substr($column1, 0, 10);
			$first = substr($column1, 0, 1);
			$rest9 = substr($column1, 1, 9);
			$params["column1"][$this->lang] = self::str_replace_first(	$first10, '<span class="first_letter" style="color: blue;">'.$first.'</span>'.$rest9, $params["column1"][$this->lang]);
		}

		return  $this->m->render(
			file_get_contents($this->template_dir.'page.html'), 
			[	"header"	=> 	$this->encabezado(),
				"html"		=> 
					$this->m->render(	file_get_contents($this->template_dir.'titulo_03.html'), 
								[ 	"titulo" 		=> $params["titulo"][$this->lang] ] ) .
					$this->m->render(	file_get_contents($this->template_dir.'img_txt_02.html'), 
								[	"bg_image"			=> $params["bg_image"][$this->lang], 
									"bg_image_height"	=> "400px",
									"titulo"			=> $params["subtitulo"][$this->lang],
									"subtitulo"			=> $params["subtitulo2"][$this->lang],
					]) .
					"<p>&nbsp;</p><p>&nbsp;</p>" . 
					$this->m->render(	file_get_contents($this->template_dir.'content_2_columns.html'), 
								[	"column1"	=>	$params["column1"][$this->lang],
									"column2"	=> $params["column2"][$this->lang],
									"extra_col"	=> "max-height: 720px",
					])
		]);
		
	}
	
	/* 
	 * 
	 * TEMPLATE: Titulo, imagen y texto a tres columnas
	 * 
	 * Datos solicitados al usuario:
	 * 
	 * 		titulo
	 * 		subtitulo
	 * 
	 * 		img
	 * 		text-align (img)
	 * 
	 * 		column1, column2, column3 (WYSIWYG)
	 * 
	 */


	private function tpl_pag_imagen_3_columnas($params, $ask=false) {

		if ($ask) return self::response( ['Image and three columns text', 'Imagen a tres columnas'], [
												'titulo'			=> 	[
																			'type'	=> self::TYPE_TEXT,
																			'label'	=> ['esp' => 'Título', 'eng' => 'Title']
																		],
												'subtitulo'			=> 	[
																			'type'	=> self::TYPE_TEXT,
																			'label'	=> ['esp' => 'Subtítulo', 'eng' => 'Subtitle']
																		],
											 	'bg_image'			=> 	[
																			'type'	=> self::TYPE_IMG,
																			'label'	=> ['esp' => 'Imagen', 'eng' => 'Image']
																		],
												'column1'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Texto de la columna 1', 'eng' => 'Column 1 text']
																		],
												'column2'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Texto de la columna 2', 'eng' => 'Column 2 text']
																		],
												'column3'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Texto de la columna 3', 'eng' => 'Column 3 text']
																		],
											]);
		
		return $this->m->render(	file_get_contents($this->template_dir.'page.html'), 
			[	"header"	=> $this->encabezado(),
				"html"		=> 
					$this->m->render(	file_get_contents($this->template_dir.'titulo_02.html'), 
								[	"titulo"	=> $params["titulo"][$this->lang],
									"subtitulo"	=> $params["subtitulo"][$this->lang]
					]) .
					$this->m->render(	file_get_contents($this->template_dir.'img_01.html'), 
											[	"img"			=> $params["bg_image"][$this->lang],
												"text-align"	=> "center"
								]) . 
					"<p>&nbsp;</p>" .
					$this->m->render(	file_get_contents($this->template_dir.'content_3_columns.html'), 
								[	"column1"	=> $params["column1"][$this->lang],
									"column2"	=> $params["column2"][$this->lang],
									"column3"	=> $params["column3"][$this->lang],
									"extra_col"	=> "max-height: 650px",
					])
		]);
		
	}

	/* 
	 * 
	 * TEMPLATE: Titulo, texto e imagen
	 * 
	 * Datos solicitados al usuario:
	 * 
	 * 		titulo
	 * 
	 * 		column1(WYSIWYG)
	 * 
	 * 		img
	 * 
	 */

	private function tpl_pag_titulo_texto_imagen($params, $ask=false) {

		if ($ask) return self::response( ['One image and one column text', 'Una imagen y una columna de texto'], [
												'titulo'			=> 	[
																			'type'	=> self::TYPE_TEXT,
																			'label'	=> ['esp' => 'Título', 'eng' => 'Title']
																		],
											 	'bg_image'			=> 	[
																			'type'	=> self::TYPE_IMG,
																			'label'	=> ['esp' => 'Imagen', 'eng' => 'Image']
																		],
												'column1'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Texto de la columna', 'eng' => 'Column text']
																		],
											]);
		
		return $this->m->render(
			file_get_contents($this->template_dir.'page.html'), 
			[	"header"	=> $this->encabezado(),
				"html"		=> 
					"<p>&nbsp;</p>" .
					$this->m->render(	file_get_contents($this->template_dir.'content_2_columns.html'), 
								[	"column1"	=> $this->m->render(	file_get_contents($this->template_dir.'titulo_05.html'), 
																[	"titulo"	=> $params["titulo"][$this->lang]
													]),
									"column2"	=> ""
					]) .
					"<p>&nbsp;</p>" .
					$this->m->render(	file_get_contents($this->template_dir.'content_2_tercios_columns.html'), 
								[	"column1"	=> 	$params["column1"][$this->lang],
									"column2"	=> $this->m->render(	file_get_contents($this->template_dir.'img_01.html'), 
																[	"img"			=> $params["bg_image"][$this->lang],
																	"width"			=> "100%"
																]),
									"extra_col"	=> "max-height: 1100px"
					])
		]);

	}

	/* 
	 * 
	 * TEMPLATE: Titulo, texto e imagen
	 * 
	 * Datos solicitados al usuario:
	 * 
	 * 		titulo
	 * 
	 * 		column1(WYSIWYG)
	 * 
	 * 		img
	 * 
	 */

	private function tpl_pag_texto_3_imagenes($params, $ask=false) {

		if ($ask) return self::response( ['Three images and two columns text', 'Tres imágenes y texto a dos columnas'], [
											 	'image1'			=> 	[
																			'type'	=> self::TYPE_IMG,
																			'label'	=> ['esp' => 'Imagen 1', 'eng' => 'Image 1']
																		],
											 	'image2'			=> 	[
																			'type'	=> self::TYPE_IMG,
																			'label'	=> ['esp' => 'Imagen 2', 'eng' => 'Image 2']
																		],
											 	'image3'			=> 	[
																			'type'	=> self::TYPE_IMG,
																			'label'	=> ['esp' => 'Imagen 3', 'eng' => 'Image 3']
																		],
												'column1'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Texto de la columna 1', 'eng' => 'Column 1 text']
																		],
												'column2'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Texto de la columna 2', 'eng' => 'Column 2 text']
																		],
											]);
		
		return $this->m->render(	file_get_contents($this->template_dir.'page.html'), 
							[	"header"	=> $this->encabezado(),
								"html"		=> 
									"<p>&nbsp;</p>" .
									$this->m->render(	file_get_contents($this->template_dir.'content_2_tercios_columns.html'), 
												[	"column1"	=> 	$this->m->render(
																				file_get_contents($this->template_dir.'img_01.html'), 
																				[	"img"			=> $params["image1"][$this->lang],
																					"width"			=> "90%"
																				]) .
																	$this->m->render(
																				file_get_contents($this->template_dir.'img_01.html'), 
																				[	"img"			=> $params["image2"][$this->lang],
																					"width"			=> "90%"
																				]),
													"column2"	=> 	$this->m->render(
																				file_get_contents($this->template_dir.'img_01.html'), 
																				[	"img"			=> $params["image3"][$this->lang],
																					"width"			=> "100%"
																				]) . 
																	$this->m->render(
																				file_get_contents($this->template_dir.'content_2_columns.html'), 
																				[	"column1"	=> $params["column1"][$this->lang],
																					"column2"	=> $params["column2"][$this->lang],
																	]),
													"extra_col"	=> "max-height: 1300px"
									])
		]);

	}

	/* 
	 * 
	 * TEMPLATE: Pagina completa
	 * 
	 * Datos solicitados al usuario:
	 * 
	 * 		content(WYSIWYG)
	 * 
	 */

	private function tpl_pag_completa($params, $ask=false) {

		if ($ask) return self::response( ['Full page', 'Página completa'], [
												'content'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Contenido', 'eng' => 'Content']
																		],
											]);
		
		return $this->m->render(	file_get_contents($this->template_dir.'page.html'), 
							[	"header"	=> $this->encabezado(),
								"html"		=> 
									$this->m->render(	file_get_contents($this->template_dir.'content.html'), 
												[	"content"	=> 	$params["content"][$this->lang] ]
									)
							]
		);

	}
	
	/* 
	 * 
	 * TEMPLATE: Pagina a dos columnas
	 * 
	 * Datos solicitados al usuario:
	 * 
	 * 		column1(WYSIWYG)
	 * 		column2(WYSIWYG)
	 * 
	 */

	private function tpl_pag_dos_columnas($params, $ask=false) {

		if ($ask) return self::response( ['Two column page', 'Página a dos columnas'], [
												'column1'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Columna 1', 'eng' => 'Column 1']
																		],
												'column2'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Columna 2', 'eng' => 'Column 2']
																		],
											]);
		
		return $this->m->render(	file_get_contents($this->template_dir.'page.html'), 
							[	"header"	=> $this->encabezado(),
								"html"		=> 
									$this->m->render(	file_get_contents($this->template_dir.'content_2_columns.html'), 
												[	"column1"	=> 	$params["column1"][$this->lang],
													"column2"	=> 	$params["column2"][$this->lang]
													]
									)
							]
		);

	}
	
	/* 
	 * 
	 * TEMPLATE: Pagina a tres columnas
	 * 
	 * Datos solicitados al usuario:
	 * 
	 * 		column1(WYSIWYG)
	 * 		column2(WYSIWYG)
	 * 		column3(WYSIWYG)
	 * 
	 */

	private function tpl_pag_tres_columnas($params, $ask=false) {

		if ($ask) return self::response( ['Three column page', 'Página a tres columnas'], [
												'column1'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Columna 1', 'eng' => 'Column 1']
																		],
												'column2'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Columna 2', 'eng' => 'Column 2']
																		],
												'column3'			=> 	[
																			'type'	=> self::TYPE_WYSIWYG,
																			'label'	=> ['esp' => 'Columna 3', 'eng' => 'Column 3']
																		],
											]);
		
		return $this->m->render(	file_get_contents($this->template_dir.'page.html'), 
							[	"header"	=> $this->encabezado(),
								"html"		=> 
									$this->m->render(	file_get_contents($this->template_dir.'content_3_columns.html'), 
												[	"column1"	=> 	$params["column1"][$this->lang],
													"column2"	=> 	$params["column2"][$this->lang],
													"column3"	=> 	$params["column3"][$this->lang]
													]
									)
							]
		);

	}
	
}

