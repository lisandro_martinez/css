<?php

require_once ('jpgraph/jpgraph.php');
require_once ('jpgraph/jpgraph_odo.php');

$achieved = round($_GET['achieved']);
$goal = round($_GET['goal']);

if ($goal<>0) {
	$pct = round($achieved * 100 / $goal, 2);
} else {
	$pct=0;
}

// Create a new odometer graph (width=250, height=200 pixels)
$graph = new OdoGraph(250,250);

// Setup titles
$graph->title->Set(utf8_decode($_GET['title']));
// $graph->subtitle->Set($pct."%");

// Now we need to create an odometer to add to the graph.
// By default the scale will be 0 to 100
$odo = new Odometer(ODO_FULL);

$odo->SetBorder('#ffffff', 0);
 
// Set display value for the odometer
if ($pct>100) $pct=100;
if ($pct<0) $pct=0;
$odo->needle->Set($pct);

$odo->AddIndication($_GET['from1'], $_GET['to1'], '#'.$_GET['color1']);
$odo->AddIndication($_GET['from2'], $_GET['to2'], '#'.$_GET['color2']);
$odo->AddIndication($_GET['from3'], $_GET['to3'], '#'.$_GET['color3']);

// Add the odometer to the graph
$graph->Add($odo);
 
// ... and finally stroke and stream the image back to the client
$graph->Stroke();

?>
