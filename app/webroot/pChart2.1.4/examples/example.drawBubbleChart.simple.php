<?php
 /* CAT:Bubble chart */

 function normaliza($data) {
	 if (is_array($data)) {
		 foreach($data as $d) {
			 $data2[]=(int) $d/4;
		 }
		 return $data2;
	 }
	 
 }
 
 // DE LA BD
 include("../../pchart/getdb.php");
 $dataDB = getData($_GET["g"]);
 
 if (!is_array($dataDB)) die;
 $serie1 = $dataDB["data"];
 $serie2 = $dataDB["data2"];
 $labels = $dataDB[$_GET["lang"]];
 if ($_GET["lang"]=="eng") $titulo1=$dataDB[0]["serie_name1_eng"];
 else $titulo1=$dataDB[0]["serie_name1"]; 
 if ($_GET["lang"]=="eng") $titulo2=$dataDB[0]["serie_name2_eng"];
 else $titulo2=$dataDB[0]["serie_name2"];
 if (!isset($serie2[0]) or $serie2[0]=="") {
	$serie2=false;
 }
 /////////////////////////////////////////////////////////

 /* pChart library inclusions */
 include("../class/pData.class.php");
 include("../class/pDraw.class.php");
 include("../class/pImage.class.php");
 include("../class/pBubble.class.php");

 /* Create and populate the pData object */
 $MyData = new pData();  
 $MyData->addPoints($serie1,"Probe1");
 $MyData->addPoints(normaliza($serie1),"Probe1Weight");
 if ($serie2) $MyData->addPoints($serie2,"Probe2");
 if ($serie2) $MyData->addPoints(normaliza($serie2),"Probe2Weight");
 $MyData->setSerieDescription("Probe1",$titulo1);
 $MyData->setSerieDescription("Probe2",$titulo2);
//	 $MyData->setAxisName(0,"Current stock");
 $MyData->addPoints($labels,"Product");
 $MyData->setAbscissa("Product");
// $MyData->setAbscissaName("Selected Products");

 /* Create the pChart object */
 $myPicture = new pImage(700,230,$MyData);

 /* Turn of AAliasing */
 $myPicture->Antialias = FALSE;

 /* Draw the border */
 $myPicture->drawRectangle(0,0,699,229,array("R"=>0,"G"=>0,"B"=>0));

 $myPicture->setFontProperties(array("FontName"=>"../fonts/pf_arma_five.ttf","FontSize"=>9));

 /* Define the chart area */
 $myPicture->setGraphArea(60,30,650,190);

 /* Draw the scale */
 $scaleSettings = array("GridR"=>200,"GridG"=>200,"GridB"=>200,"DrawSubTicks"=>TRUE,"CycleBackground"=>TRUE);
 $myPicture->drawScale($scaleSettings);

 /* Create the Bubble chart object and scale up */
 $myPicture->Antialias = TRUE;
 $myBubbleChart = new pBubble($myPicture,$MyData);

 /* Scale up for the bubble chart */
 $bubbleDataSeries   = array("Probe1","Probe2");
 $bubbleWeightSeries = array("Probe1Weight","Probe2Weight");
 $myBubbleChart->bubbleScale($bubbleDataSeries,$bubbleWeightSeries);

 /* Draw the bubble chart */
 $myBubbleChart->drawBubbleChart($bubbleDataSeries,$bubbleWeightSeries,array("BorderWidth"=>4,"BorderAlpha"=>50,"Surrounding"=>20));

 /* Write the chart legend */
 $myPicture->drawLegend(570,13,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_HORIZONTAL));

 /* Render the picture (choose the best way) */
 $myPicture->autoOutput("pictures/example.drawBubbleChart.simple.png");
?>
