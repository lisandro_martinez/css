<?php
 /* CAT:Bubble chart */

 function normaliza($data) {
	 if (is_array($data)) {
		 foreach($data as $d) {
			 $data2[]=(int) $d/4;
		 }
		 return $data2;
	 }
	 
 }
 
 // DE LA BD
 include("../../pchart/getdb.php");
 $dataDB = getData($_GET["g"]);
 
 if (!is_array($dataDB)) die;
 $serie1 = $dataDB["data"];
 $serie2 = $dataDB["data2"];
 $labels = $dataDB[$_GET["lang"]];
 if ($_GET["lang"]=="eng") $titulo1=$dataDB[0]["serie_name1_eng"];
 else $titulo1=$dataDB[0]["serie_name1"]; 
 if ($_GET["lang"]=="eng") $titulo2=$dataDB[0]["serie_name2_eng"];
 else $titulo2=$dataDB[0]["serie_name2"];
 if (!isset($serie2[0]) or $serie2[0]=="") {
	$serie2=false;
 }
 /////////////////////////////////////////////////////////

 /* pChart library inclusions */
 include("../class/pData.class.php");
 include("../class/pDraw.class.php");
 include("../class/pImage.class.php");
 include("../class/pBubble.class.php");

 /* Create and populate the pData object */
 $MyData = new pData();
 $MyData->loadPalette("../palettes/summer.color",TRUE);
 $MyData->addPoints($serie1,"Probe1");
 $MyData->addPoints(normaliza($serie1),"Probe1Weight");
 if ($serie2) $MyData->addPoints($serie2,"Probe2");
 if ($serie2) $MyData->addPoints(normaliza($serie2),"Probe2Weight");
 $MyData->setSerieDescription("Probe1","This year");
 if ($serie2) $MyData->setSerieDescription("Probe2","Last year");
// $MyData->setAxisName(0,"Current stock");
 $MyData->addPoints($labels,"Product");
 $MyData->setAbscissa("Product");

 /* Create the pChart object */
 $myPicture = new pImage(500,230,$MyData);

 /* Draw the background */
 $Settings = array("R"=>170, "G"=>183, "B"=>87, "Dash"=>1, "DashR"=>190, "DashG"=>203, "DashB"=>107);
 $myPicture->drawFilledRectangle(0,0,500,230,$Settings);

 /* Overlay with a gradient */
 $Settings = array("StartR"=>219, "StartG"=>231, "StartB"=>139, "EndR"=>1, "EndG"=>138, "EndB"=>68, "Alpha"=>50);
 $myPicture->drawGradientArea(0,0,500,230,DIRECTION_VERTICAL,$Settings);
// $myPicture->drawGradientArea(0,0,500,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>80));

 /* Add a border to the picture */
 $myPicture->drawRectangle(0,0,499,229,array("R"=>0,"G"=>0,"B"=>0));
 
 /* Write the picture title */ 
 $myPicture->setFontProperties(array("FontName"=>"../fonts/Silkscreen.ttf","FontSize"=>8));
 $myPicture->drawText(10,13,$titulo,array("R"=>255,"G"=>255,"B"=>255));

 /* Write the title */
 $myPicture->setFontProperties(array("FontName"=>"../fonts/Forgotte.ttf","FontSize"=>11));
// $myPicture->drawText(40,55,"Current Stock / Needs chart",array("FontSize"=>14,"Align"=>TEXT_ALIGN_BOTTOMLEFT));

 /* Change the default font */
 $myPicture->setFontProperties(array("FontName"=>"../fonts/pf_arma_five.ttf","FontSize"=>8));

 /* Create the Bubble chart object and scale up */
 $myBubbleChart = new pBubble($myPicture,$MyData);

 /* Scale up for the bubble chart */
 $bubbleDataSeries   = array("Probe1","Probe2");
 $bubbleWeightSeries = array("Probe1Weight","Probe2Weight");
 $myBubbleChart->bubbleScale($bubbleDataSeries,$bubbleWeightSeries);

 /* Draw the 1st chart */
 $myPicture->setGraphArea(60,30,430,190);
 $myPicture->drawFilledRectangle(60,30,430,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-50,"Alpha"=>10));
 $myPicture->drawScale(array("DrawSubTicks"=>TRUE,"CycleBackground"=>TRUE));
 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>30));
 $myBubbleChart->drawBubbleChart($bubbleDataSeries,$bubbleWeightSeries);
/*
 $myPicture->setShadow(FALSE);
 $myPicture->setGraphArea(500,60,670,190);
 $myPicture->drawFilledRectangle(500,60,670,190,array("R"=>255,"G"=>255,"B"=>255,"Surrounding"=>-200,"Alpha"=>10));
 $myPicture->drawScale(array("Pos"=>SCALE_POS_TOPBOTTOM,"DrawSubTicks"=>TRUE));
*/

 $myPicture->setShadow(TRUE,array("X"=>1,"Y"=>1,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>30));
 $myBubbleChart->drawbubbleChart($bubbleDataSeries,$bubbleWeightSeries,array("DrawBorder"=>TRUE,"Surrounding"=>60,"BorderAlpha"=>100));

 /* Write the chart legend */
// $myPicture->drawLegend(550,215,array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_HORIZONTAL));

 /* Render the picture (choose the best way) */
 $myPicture->autoOutput();
?>
