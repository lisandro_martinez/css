<?php
 /*
     Example10 : A 3D exploded pie graph
 */

 // DE LA BD
 include("getdb.php");
 $dataDB = getData($_GET["g"]);
 if (!is_array($dataDB)) die;
 $as1 = $dataDB["data"];
 $labels = $dataDB[$_GET["lang"]];
 if ($_GET["lang"]=="eng") $titulo_serie1=$dataDB["serie_name1_eng"];
 else $titulo_serie1=$dataDB["serie_name1"];
 /////////////////////////////////////////////

 // Standard inclusions   
 include("pChart/pData.class");
 include("pChart/pChart.class");

 // Dataset definition 
 $DataSet = new pData;
 $DataSet->AddPoint($as1,"Serie1");
 $DataSet->AddPoint($labels,"Labels");
 $DataSet->AddAllSeries();
 $DataSet->SetAbsciseLabelSerie("Labels");

 // Initialise the graph
 $Test = new pChart(900,500);
// $Test->drawFilledRoundedRectangle(7,7,413,243,5,240,240,240);
// $Test->drawRoundedRectangle(5,5,415,245,5,230,230,230);
 $Test->createColorGradientPalette(195,204,56,223,110,41,5);

 // Draw the pie chart
 $Test->setFontProperties("Fonts/tahoma.ttf",14);
 $Test->AntialiasQuality = 0;
 $Test->drawPieGraph($DataSet->GetData(),$DataSet->GetDataDescription(),420,250,300,PIE_PERCENTAGE_LABEL,FALSE,50,20,5);
 $Test->drawPieLegend(660,30,$DataSet->GetData(),$DataSet->GetDataDescription(),250,250,250);

 // Write the title
 $Test->setFontProperties("Fonts/MankSans.ttf",20);
 $Test->drawTitle(10,20,$titulo,100,100,100);

 $Test->Stroke();
?>