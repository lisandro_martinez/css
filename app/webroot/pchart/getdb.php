<?php

include_once(dirname(dirname(dirname(__FILE__)))."/Config/database.php");

function getData($id) {

	// caso 1:
	// Example25.php?lang=esp&isget=1&s=2015-10-01,2015-10-22,2015-11-04&d=123.00,123.00,213456.00&t=Consumo+Energético+(Voltios%2Faño)
	if ($_GET["isget"]) {

		$dataDB["data"]=explode(",", $_GET["d"]);
		$dataDB["data2"]=array("0"=>null);
		$dataDB["eng"]=$dataDB["esp"]=explode(",", $_GET["s"]);
		$dataDB[0]["serie_name1_eng"]=$dataDB[0]["serie_name1"]=$_GET["t"]; 
		$dataDB[0]["serie_name2_eng"]=$dataDB[0]["serie_name2"]=""; 

		return $dataDB;
	
	//caso 2: (DB)
	} else {
		if (is_numeric($id)) {
	 
			$config = new DATABASE_CONFIG();
			// Array ( [datasource] => Database/Mysql [persistent] => [host] => localhost [login] => siriarte_css [password] => asdf123@ [database] => siriarte_css2 [prefix] => ) 
			$mysqli = new mysqli($config->default["host"], $config->default["login"], $config->default["password"], $config->default["database"]);
			if (!$mysqli->connect_errno) {
				$resultado = $mysqli->query("SELECT * FROM graphdata WHERE node_id=".$id);
				$fila = $resultado->fetch_assoc();
				
				$data = unserialize($fila["data"]);
				
				if (isset($data[0])) {
					return $data;
				}
				
			}
			
		}
	}	
}
