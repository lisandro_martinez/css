<?php
 /*
     Example13: A 2D exploded pie graph
 */

 // Standard inclusions   
 include("pChart/pData.class");
 include("pChart/pChart.class");

 // DE LA BD
 include("getdb.php");
 $dataDB = getData($_GET["g"]);
 if (!is_array($dataDB)) die;
 $as1 = $dataDB["data"];
 $labels = $dataDB[$_GET["lang"]];
 if ($_GET["lang"]=="eng") $titulo_serie1=$dataDB["serie_name1_eng"];
 else $titulo_serie1=$dataDB["serie_name1"];
 /////////////////////////////////////////////
 
 // Dataset definition 
 $DataSet = new pData;
 $DataSet->AddPoint($as1,"Serie1");
 $DataSet->AddPoint($labels,"Labels");

 $DataSet->AddAllSeries();
 $DataSet->SetAbsciseLabelSerie("Labels");

 // Initialise the graph
 $Test = new pChart(700,400);
 $Test->setFontProperties("Fonts/tahoma.ttf",8);
// $Test->drawFilledRoundedRectangle(7,7,293,193,5,240,240,240);
// $Test->drawRoundedRectangle(5,5,295,195,5,230,230,230);

 // Draw the pie chart
 $Test->AntialiasQuality = 0;
 $Test->setShadowProperties(2,2,200,200,200);
 $Test->drawFlatPieGraphWithShadow($DataSet->GetData(),$DataSet->GetDataDescription(),240,200,150,PIE_PERCENTAGE,8);
 $Test->clearShadow();

 $Test->drawPieLegend(460,15,$DataSet->GetData(),$DataSet->GetDataDescription(),250,250,250);

 $Test->Stroke();
?>