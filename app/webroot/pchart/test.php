<?php   
 /*
     Example1 : A simple line chart
 */

 // Standard inclusions      
 include("pChart/pData.class");   
 include("pChart/pChart.class");   
  
$Data[] = array("Name"=>"A","Serie1"=>0);  
$Data[] = array("Name"=>"B","Serie1"=>4);  
$Data[] = array("Name"=>"C","Serie1"=>2);  
$Data[] = array("Name"=>"D","Serie1"=>7);  
$Data[] = array("Name"=>"E","Serie1"=>3);  
$Data[] = array("Name"=>"F","Serie1"=>5);  

// Set the serie Name as abscissa data  
$DataDescription["Position"] = "Name";  
  
// Display Serie1 when calling graphs functions  
$DataDescription["Values"][] = "Serie1";  
  
// Set the description of Serie1 to "Year 2007"  
$DataDescription["Description"]["Serie1"] = "Year 2007";  

  
 // Initialise the graph   
 $Test = new pChart(700,230);
 $Test->setFontProperties("Fonts/tahoma.ttf",8);   
 $Test->setGraphArea(70,30,680,200);   
// $Test->drawGraphArea(255,255,255,TRUE);
 $Test->drawScale($Data,$DataDescription,SCALE_NORMAL,150,150,150,TRUE,0,2);   
 $Test->drawGrid(4,TRUE,230,230,230,50);
  
 // Draw the 0 line   
 $Test->setFontProperties("Fonts/tahoma.ttf",6);   
 $Test->drawTreshold(0,143,55,72,TRUE,TRUE);   
  
 // Draw the line graph
 $Test->drawLineGraph($Data,$DataDescription);   
 $Test->drawPlotGraph($Data,$DataDescription,3,2,255,255,255);   
  
 // Finish the graph   
 $Test->setFontProperties("Fonts/tahoma.ttf",8);   
 $Test->drawLegend(75,35,$DataDescription,255,255,255);   
 $Test->setFontProperties("Fonts/tahoma.ttf",10);   
 $Test->drawTitle(60,22,"example 1",50,50,50,585);   
 $Test->Stroke();
?>