<?php   
 /*
     Example17 : Playing with axis
 */

 // Standard inclusions
 include("pChart/pData.class");
 include("pChart/pChart.class");
  
 // DE LA BD
 include("getdb.php");
 $dataDB = getData($_GET["g"]);
 if (!is_array($dataDB)) die;

 $serie1 = $dataDB["data"];
 $serie2 = $dataDB["data2"];
 $labels = $dataDB[$_GET["lang"]];
 if ($_GET["lang"]=="eng") $titulo1=$dataDB[0]["serie_name1_eng"];
 else $titulo1=$dataDB[0]["serie_name1"]; 
 if ($_GET["lang"]=="eng") $titulo2=$dataDB[0]["serie_name2_eng"];
 else $titulo2=$dataDB[0]["serie_name2"];

 if (!isset($serie2[0]) or $serie2[0]=="") {
	$serie2=false;
 }

 /////////////////////////////////////////////////////////
 
 // Dataset definition
 $DataSet = new pData;
 $DataSet->AddPoint($serie1,"Serie1");
 if ($serie2) $DataSet->AddPoint($serie2,"Serie2");
 $DataSet->AddSerie("Serie1");
  if ($serie2) $DataSet->AddSerie("Serie2");
 $DataSet->AddPoint($labels,"Labels"); // Labels: paso 2
 $DataSet->SetAbsciseLabelSerie("Labels"); // Labels: paso 3
 $DataSet->SetSerieName($titulo1,"Serie1");
 if ($serie2) $DataSet->SetSerieName($titulo2,"Serie2");
// $DataSet->SetYAxisName("Call duration");
// $DataSet->SetYAxisFormat("time");
// $DataSet->SetXAxisFormat("date");

 // Initialise the graph   
 $Test = new pChart(700,230);
 $Test->setFontProperties("Fonts/tahoma.ttf",8);
 $Test->setGraphArea(85,30,650,200);
// $Test->drawFilledRoundedRectangle(7,7,693,223,5,240,240,240);
// $Test->drawRoundedRectangle(5,5,695,225,5,230,230,230);
 $Test->drawGraphArea(255,255,255,TRUE);
 $Test->drawScale($DataSet->GetData(),$DataSet->GetDataDescription(),SCALE_ADDALLSTART0,150,150,150,TRUE,0,2);
 $Test->drawGrid(4,TRUE,230,230,230,50);
 
 // Draw the 0 line   
 $Test->setFontProperties("Fonts/tahoma.ttf",6);
 $Test->drawTreshold(0,143,55,72,TRUE,TRUE);
  
 // Draw the line graph
 $Test->drawLineGraph($DataSet->GetData(),$DataSet->GetDataDescription());
 $Test->drawPlotGraph($DataSet->GetData(),$DataSet->GetDataDescription(),3,2,255,255,255);
  
 // Finish the graph
 $Test->setFontProperties("Fonts/tahoma.ttf",8);
 $Test->drawLegend(90,35,$DataSet->GetDataDescription(),255,255,255);
 $Test->setFontProperties("Fonts/tahoma.ttf",10);
 $Test->drawTitle(60,22,$titulo,50,50,50,585);
 $Test->Stroke();
