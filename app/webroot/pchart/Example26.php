<?php
 /*
     Example26 : Two Y axis / shadow demonstration
 */

 // Standard inclusions   
 include("pChart/pData.class");
 include("pChart/pChart.class");

 // DE LA BD
 $serie1 = array(1,4,3,2,3,3,2,1,3,7,4,3,2,3,3,5,1,0,7);  
 $serie2 = array(1,4,2,6,2,3,1,1,5,1,2,4,5,2,1,0,6,4,2);
 $titulo="TITULO";
 $titulo1="El titulo 1";
 $titulo2="El titulo 2";
 $serie3 = array("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s"); // Labels: paso 1
 /////////////////////////////////////////////////////////
 
 // Dataset definition 
 $DataSet = new pData;
 $DataSet->AddPoint($serie1,"Serie1");
 $DataSet->AddPoint($serie2,"Serie2");
 $DataSet->AddPoint($serie3,"Serie3");
 $DataSet->AddSerie("Serie1");
 $DataSet->RemoveSerie("Serie3");
 $DataSet->SetAbsciseLabelSerie("Serie3");
 $DataSet->SetSerieName($titulo1,"Serie1");
 $DataSet->SetSerieName($titulo2,"Serie2");

 // Initialise the graph
 $Test = new pChart(660,230);
 $Test->drawGraphAreaGradient(90,90,90,90,TARGET_BACKGROUND);

 // Prepare the graph area
 $Test->setFontProperties("Fonts/tahoma.ttf",8);
 $Test->setGraphArea(60,40,595,190);

 // Initialise graph area
 $Test->setFontProperties("Fonts/tahoma.ttf",8);

 // Draw the SourceForge Rank graph
// $DataSet->SetYAxisName("Sourceforge Rank");
 $Test->drawScale($DataSet->GetData(),$DataSet->GetDataDescription(),SCALE_NORMAL,213,217,221,TRUE,0,0);
 $Test->drawGraphAreaGradient(40,40,40,-50);
 $Test->drawGrid(4,TRUE,230,230,230,10);
 $Test->setShadowProperties(3,3,0,0,0,30,4);
 $Test->drawCubicCurve($DataSet->GetData(),$DataSet->GetDataDescription());
 $Test->clearShadow();
 $Test->drawFilledCubicCurve($DataSet->GetData(),$DataSet->GetDataDescription(),.1,30);
 $Test->drawPlotGraph($DataSet->GetData(),$DataSet->GetDataDescription(),3,2,255,255,255);

 // Clear the scale
 $Test->clearScale();

 // Draw the 2nd graph
 $DataSet->RemoveSerie("Serie1");
 $DataSet->AddSerie("Serie2");
// $DataSet->SetYAxisName("Web Hits");
 $Test->drawRightScale($DataSet->GetData(),$DataSet->GetDataDescription(),SCALE_NORMAL,213,217,221,TRUE,0,0);
 $Test->drawGrid(4,TRUE,230,230,230,10);
 $Test->setShadowProperties(3,3,0,0,0,30,4);
 $Test->drawCubicCurve($DataSet->GetData(),$DataSet->GetDataDescription());
 $Test->clearShadow();
 $Test->drawFilledCubicCurve($DataSet->GetData(),$DataSet->GetDataDescription(),.1,30);
 $Test->drawPlotGraph($DataSet->GetData(),$DataSet->GetDataDescription(),3,2,255,255,255);

 // Write the legend (box less)
 $Test->setFontProperties("Fonts/tahoma.ttf",8);
 $Test->drawLegend(530,5,$DataSet->GetDataDescription(),0,0,0,0,0,0,255,255,255,FALSE);

 // Write the title
 $Test->setFontProperties("Fonts/MankSans.ttf",18);
 $Test->setShadowProperties(1,1,0,0,0);
 $Test->drawTitle(0,0,$titulo,255,255,255,660,30,TRUE);
 $Test->clearShadow();

 // Render the picture
 $Test->Stroke();
