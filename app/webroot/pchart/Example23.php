<?php
 /*
     Example23 : Playing with background bis
 */

 // Standard inclusions   
 include("pChart/pData.class");
 include("pChart/pChart.class");

 // DE LA BD
 include("getdb.php");
 $dataDB = getData($_GET["g"]);
 if (!is_array($dataDB)) die;
 $serie1 = $dataDB["data"];
 $serie2 = $dataDB["data2"];
 $labels = $dataDB[$_GET["lang"]];
 if ($_GET["lang"]=="eng") $titulo1=$dataDB[0]["serie_name1_eng"];
 else $titulo1=$dataDB[0]["serie_name1"]; 
 if ($_GET["lang"]=="eng") $titulo2=$dataDB[0]["serie_name2_eng"];
 else $titulo2=$dataDB[0]["serie_name2"];

 if (!isset($serie2[0]) or $serie2[0]=="") {
	$serie2=false;
 }

 /////////////////////////////////////////////////////////
 
 // Dataset definition 
 $DataSet = new pData;
 $DataSet->AddPoint($serie1,"Serie1");
 if ($serie2) $DataSet->AddPoint($serie2,"Serie2");
 $DataSet->AddPoint($labels,"Labels");
 $DataSet->AddAllSeries();
 $DataSet->RemoveSerie("Labels");
 $DataSet->SetAbsciseLabelSerie("Labels");
 $DataSet->SetSerieName($titulo1,"Serie1");
 if ($serie2) $DataSet->SetSerieName($titulo2,"Serie2");
 
/*
 $DataSet->SetYAxisName("Temperature");
 $DataSet->SetYAxisUnit("�C");
 $DataSet->SetXAxisUnit("h");
*/
 // Initialise the graph
 $Test = new pChart(750,230);
 $Test->drawGraphAreaGradient(132,173,131,50,TARGET_BACKGROUND);

 $Test->setFontProperties("Fonts/tahoma.ttf",8);
 $Test->setGraphArea(120,20,675,190);
 $Test->drawGraphArea(213,217,221,FALSE);
 $Test->drawScale($DataSet->GetData(),$DataSet->GetDataDescription(),SCALE_ADDALLSTART0,213,217,221,TRUE,0,2,TRUE);
 $Test->drawGraphAreaGradient(163,203,167,50);
 $Test->drawGrid(4,TRUE,230,230,230,20);

 // Draw the bar chart
 $Test->drawStackedBarGraph($DataSet->GetData(),$DataSet->GetDataDescription(),70);

 // Draw the title
 $Title = $titulo;
 $Test->drawTextBox(0,0,50,230,$Title,90,255,255,255,ALIGN_BOTTOM_CENTER,TRUE,0,0,0,30);

 // Draw the legend
 $Test->setFontProperties("Fonts/tahoma.ttf",8);
 $Test->drawLegend(610,10,$DataSet->GetDataDescription(),236,238,240,52,58,82);

 // Render the picture
 $Test->addBorder(2);
 $Test->Stroke();
