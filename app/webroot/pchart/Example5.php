<?php
 /*
     Example5 : A limits graph
 */

 // Standard inclusions   
 include("pChart/pData.class");
 include("pChart/pChart.class");


 // DE LA BD
 include("getdb.php");
 $dataDB = getData($_GET["g"]);
 if (!is_array($dataDB)) die;
 $serie1 = $dataDB["data"];
 $serie2 = $dataDB["data2"];
 $labels = $dataDB[$_GET["lang"]];
 if ($_GET["lang"]=="eng") $titulo1=$dataDB[0]["serie_name1_eng"];
 else $titulo1=$dataDB[0]["serie_name1"]; 
 if ($_GET["lang"]=="eng") $titulo2=$dataDB[0]["serie_name2_eng"];
 else $titulo2=$dataDB[0]["serie_name2"];

 if (!isset($serie2[0]) or $serie2[0]=="") {
	$serie2=false;
 }

 /////////////////////////////////////////////////////////
 
 // Dataset definition 
 $DataSet = new pData;
 $DataSet->AddPoint($serie1,"Serie1");
 if ($serie2) $DataSet->AddPoint($serie2,"Serie2");
 $DataSet->AddAllSeries();
 $DataSet->AddPoint($labels,"labels"); // Labels: paso 2
 $DataSet->SetAbsciseLabelSerie("labels"); // Labels: paso 3
 $DataSet->SetSerieName($titulo1,"Serie1");
 if ($serie2) $DataSet->SetSerieName($titulo2,"Serie2");

 // Initialise the graph
 $Test = new pChart(700,230);
 $Test->setFontProperties("Fonts/tahoma.ttf",8);
 $Test->setGraphArea(50,30,585,200);
// $Test->drawFilledRoundedRectangle(7,7,693,223,5,240,240,240);
// $Test->drawRoundedRectangle(5,5,695,225,5,230,230,230);
 $Test->drawGraphArea(255,255,255,TRUE);
 $Test->drawScale($DataSet->GetData(),$DataSet->GetDataDescription(),SCALE_ADDALLSTART0,150,150,150,TRUE,0,2,TRUE);
 $Test->drawGrid(4,TRUE,230,230,230,50);

 // Draw the 0 line
 $Test->setFontProperties("Fonts/tahoma.ttf",6);
 $Test->drawTreshold(0,143,55,72,TRUE,TRUE);

 // Draw the limit graph
 $Test->drawLimitsGraph($DataSet->GetData(),$DataSet->GetDataDescription(),180,180,180);

 // Finish the graph
 $Test->setFontProperties("Fonts/tahoma.ttf",8);
 $Test->drawLegend(600,30,$DataSet->GetDataDescription(),255,255,255);
 $Test->setFontProperties("Fonts/tahoma.ttf",10);
 $Test->drawTitle(50,22,$titulo,50,50,50,585);
 $Test->Stroke();
