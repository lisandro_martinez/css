<?php
 /*
     Example21 : Playing with background
 */

 // Standard inclusions   
 include("pChart/pData.class");
 include("pChart/pChart.class");

 // DE LA BD
 include("getdb.php");
 $dataDB = getData($_GET["g"]);
 if (!is_array($dataDB)) die;
 $serie1 = $dataDB["data"];
 $serie2 = $dataDB["data2"];
 $labels = $dataDB[$_GET["lang"]];
 if ($_GET["lang"]=="eng") $titulo1=$dataDB[0]["serie_name1_eng"];
 else $titulo1=$dataDB[0]["serie_name1"]; 
 if ($_GET["lang"]=="eng") $titulo2=$dataDB[0]["serie_name2_eng"];
 else $titulo2=$dataDB[0]["serie_name2"];

 if (!isset($serie2[0]) or $serie2[0]=="") {
	$serie2=false;
 }

 /////////////////////////////////////////////////////////
 
 // Dataset definition 
 $DataSet = new pData;
 $DataSet->AddPoint($serie1,"Serie1");
 if ($serie2) $DataSet->AddPoint($serie2,"Serie2");
 $DataSet->AddAllSeries();
 $DataSet->RemoveSerie("Labels");
 $DataSet->AddPoint($labels,"Labels"); // Labels: paso 2
 $DataSet->SetAbsciseLabelSerie("Labels"); // Labels: paso 3
 $DataSet->SetSerieName($titulo1,"Serie1");
 if ($serie2) $DataSet->SetSerieName($titulo2,"Serie2");

 // Initialise the graph
 $Test = new pChart(800,230);
 $Test->drawGraphAreaGradient(132,153,172,50,TARGET_BACKGROUND);

 // Graph area setup
 $Test->setFontProperties("Fonts/tahoma.ttf",8);
 $Test->setGraphArea(60,20,585,180);
 $Test->drawGraphArea(213,217,221,FALSE);
 $Test->drawScale($DataSet->GetData(),$DataSet->GetDataDescription(),SCALE_ADDALLSTART0,213,217,221,TRUE,0,2);
 $Test->drawGraphAreaGradient(162,183,202,50);
 $Test->drawGrid(4,TRUE,230,230,230,20);

 // Draw the line chart
 $Test->setShadowProperties(3,3,0,0,0,30,4);
 $Test->drawLineGraph($DataSet->GetData(),$DataSet->GetDataDescription());
 $Test->clearShadow();
 $Test->drawPlotGraph($DataSet->GetData(),$DataSet->GetDataDescription(),4,2,-1,-1,-1,TRUE);

 // Draw the legend
 $Test->setFontProperties("Fonts/tahoma.ttf",8);
 $Test->drawLegend(605,142,$DataSet->GetDataDescription(),236,238,240,52,58,82);

 // Draw the title
 $Title = $titulo;
 $Test->drawTextBox(0,210,800,230,$Title,0,255,255,255,ALIGN_RIGHT,TRUE,0,0,0,30);

 // Render the picture
 $Test->addBorder(2);
 $Test->Stroke();
