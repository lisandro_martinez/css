<?php
 /*
     Example25 : Playing with shadow
 */

 // Standard inclusions   
 include("pChart/pData.class");
 include("pChart/pChart.class");

 // DE LA BD
 include("getdb.php");
 $dataDB = getData($_GET["g"]);
 if (!is_array($dataDB)) die;
 $serie1 = $dataDB["data"];
 $serie2 = $dataDB["data2"];
 $labels = $dataDB[$_GET["lang"]];
 if ($_GET["lang"]=="eng") $titulo1=$dataDB[0]["serie_name1_eng"];
 else $titulo1=$dataDB[0]["serie_name1"]; 
 if ($_GET["lang"]=="eng") $titulo2=$dataDB[0]["serie_name2_eng"];
 else $titulo2=$dataDB[0]["serie_name2"];

 if (!isset($serie2[0]) or $serie2[0]=="") {
	$serie2=false;
 }

 /////////////////////////////////////////////////////////
 
 // Dataset definition 
 $DataSet = new pData;
 $DataSet->AddPoint($serie1,"Serie1");
 if ($serie2) $DataSet->AddPoint($serie2,"Serie2");
 $DataSet->AddPoint($labels,"Labels");
 $DataSet->AddAllSeries();
 $DataSet->RemoveSerie("Labels");
 $DataSet->SetAbsciseLabelSerie("Labels");
 $DataSet->SetSerieName($titulo1,"Serie1");
 if ($serie2) $DataSet->SetSerieName($titulo2,"Serie2");
/*
 $DataSet->SetYAxisName("Temperature");
 $DataSet->SetYAxisUnit("�C");
 $DataSet->SetXAxisUnit("h");
*/
 // Initialise the graph
 $Test = new pChart(700,230);
 $Test->drawGraphAreaGradient(90,90,90,90,TARGET_BACKGROUND);
// $Test->setFixedScale(0,40,4);

 // Graph area setup
 $Test->setFontProperties("Fonts/pf_arma_five.ttf",6);
 $Test->setGraphArea(60,40,680,200);
 $Test->drawGraphArea(200,200,200,FALSE);
 $Test->drawScale($DataSet->GetData(),$DataSet->GetDataDescription(),SCALE_ADDALLSTART0,200,200,200,TRUE,0,2);
 $Test->drawGraphAreaGradient(40,40,40,-50);
 $Test->drawGrid(4,TRUE,230,230,230,10);

 // Draw the line chart
 $Test->setShadowProperties(3,3,0,0,0,30,4);
 $Test->drawCubicCurve($DataSet->GetData(),$DataSet->GetDataDescription());
 $Test->clearShadow();
 $Test->drawPlotGraph($DataSet->GetData(),$DataSet->GetDataDescription(),3,0,-1,-1,-1,TRUE);

 // Write the title
 $Test->setFontProperties("Fonts/MankSans.ttf",18);
 $Test->setShadowProperties(1,1,0,0,0);
 $Test->drawTitle(0,0,$titulo,255,255,255,700,30,TRUE);
 $Test->clearShadow();

 // Draw the legend
 $Test->setFontProperties("Fonts/tahoma.ttf",8);
 $Test->drawLegend(560,5,$DataSet->GetDataDescription(),0,0,0,0,0,0,255,255,255,FALSE);

 // Render the picture
 $Test->Stroke();
