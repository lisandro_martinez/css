<?php
 /*
     Example14: A smooth flat pie graph
 */

 // DE LA BD
 include("getdb.php");
 $dataDB = getData($_GET["g"]);
 if (!is_array($dataDB)) die;
 $as1 = $dataDB["data"];
 $labels = $dataDB[$_GET["lang"]];
 if ($_GET["lang"]=="eng") $titulo_serie1=$dataDB["serie_name1_eng"];
 else $titulo_serie1=$dataDB["serie_name1"];
 /////////////////////////////////////////////
 
 // Standard inclusions   
 include("pChart/pData.class");
 include("pChart/pChart.class");

 // Dataset definition 
 $DataSet = new pData;
 $DataSet->AddPoint($as1,"Serie1");
 $DataSet->AddPoint($labels,"Labels");
 $DataSet->AddAllSeries();
 $DataSet->SetAbsciseLabelSerie("Labels");

 // Initialise the graph
 $Test = new pChart(600,400);
 $Test->loadColorPalette("Sample/softtones.txt");
 //$Test->drawFilledRoundedRectangle(7,7,493,193,5,240,240,240);
 //$Test->drawRoundedRectangle(5,5,495,195,5,230,230,230);

 // This will draw a shadow under the pie chart
 $Test->drawFilledCircle(242,202,140,200,200,200);

 // Draw the pie chart
 $Test->setFontProperties("Fonts/tahoma.ttf",14);
 $Test->AntialiasQuality = 0;
 $Test->drawBasicPieGraph($DataSet->GetData(),$DataSet->GetDataDescription(),240,200,140,PIE_PERCENTAGE,255,255,218);
 $Test->drawPieLegend(400,30,$DataSet->GetData(),$DataSet->GetDataDescription(),250,250,250);

 $Test->Stroke();
?>