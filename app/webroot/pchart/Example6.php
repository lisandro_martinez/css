<?php
 /*
     Example6 : A simple filled line graph
 */

 // Standard inclusions   
 include("pChart/pData.class");
 include("pChart/pChart.class");

 // de la BD
 $Data[] = array("Name"=>"A","Serie1"=>1,"Serie2"=>0);  
 $Data[] = array("Name"=>"B","Serie1"=>2,"Serie2"=>1);  
 $Data[] = array("Name"=>"C","Serie1"=>4,"Serie2"=>4);  
 $Data[] = array("Name"=>"D","Serie1"=>7,"Serie2"=>8);  
 $Data[] = array("Name"=>"E","Serie1"=>9,"Serie2"=>10);  
 $Data[] = array("Name"=>"F","Serie1"=>11,"Serie2"=>14);  
 $titulo="TITULO";
 $titulo1="El titulo 1";
 $titulo2="El titulo 2";
 //////////////////////////////////////////////////////////
 
 // Dataset definition 
/*
 $DataSet = new pData;
 $DataSet->ImportFromCSV("Sample/datawithtitle.csv",",",array(1,2,3),TRUE,0);
 $DataSet->AddAllSeries();
 $DataSet->SetAbsciseLabelSerie();
*/

 // Initialise the graph
 $Test = new pChart(700,230);
 $Test->setFontProperties("Fonts/tahoma.ttf",8);
 $Test->setGraphArea(60,30,680,200);
// $Test->drawFilledRoundedRectangle(7,7,693,223,5,240,240,240);
// $Test->drawRoundedRectangle(5,5,695,225,5,230,230,230);
 $Test->drawGraphArea(255,255,255,TRUE);
 $Test->drawScale($Data,$DataSet->GetDataDescription(),SCALE_NORMAL,150,150,150,TRUE,0,2);
 $Test->drawGrid(4,TRUE,230,230,230,50);

 // Draw the 0 line
 $Test->setFontProperties("Fonts/tahoma.ttf",6);
 $Test->drawTreshold(0,143,55,72,TRUE,TRUE);

 // Draw the filled line graph
 $Test->drawFilledLineGraph($Data,$DataSet->GetDataDescription(),50,TRUE);

 // Finish the graph
 $Test->setFontProperties("Fonts/tahoma.ttf",8);
 $Test->drawLegend(65,35,$DataSet->GetDataDescription(),255,255,255);
 $Test->setFontProperties("Fonts/tahoma.ttf",10);
 $Test->drawTitle(60,22,"Example 6",50,50,50,585);
 $Test->Stroke();
