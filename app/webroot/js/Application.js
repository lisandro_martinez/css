$(function () {
	
	/*--------------------------------------------------
	Plugin: Date Picker
	--------------------------------------------------*/
	
    $( "#datepicker" ).datepicker({ dateFormat: 'dd-mm-yy' });
    $( "#datepicker-2" ).datepicker({ dateFormat: 'dd-mm-yy' });
	
	$( "#datepicker-multi" ).datepicker({
		numberOfMonths: 2,
		showButtonPanel: true
	});
	
});

var DEBUGJS = true;

function capitalize(s)
{
    return s[0].toUpperCase() + s.slice(1);
}

function loadingScreen() {
	var html = '<div id="load"></div>';
	$('body').append(html);
}

function removeLoadScreen(){
	$('#load').fadeOut(100,function() {
    		$(this).remove();
 	 });
	$('[data-toggle="tooltip"]').tooltip();
}

function showApp(){
	$('.app').fadeIn();
}


function clearAlerts() {
	$(".alert").fadeOut(300, function() { $(this).remove(); });
}

function consolelog(msg){

 	if(DEBUGJS){
 		console.log(msg);
 	}

}

function invokeMethod(object, method,data) {
  data = typeof data !== 'undefined' ? data : "";
  object[method](data);
}


$(document).ready(function(){

	removeLoadScreen();
	showApp();
	App.clickBlockScreen();

	Controllers.forEach(function(entry) {
			dataarray = entry.split(".");
			Obj = window[dataarray[0]];
			invokeMethod(Obj,dataarray[1]);
	});

});



$(function () {

	Application.init ();

});



var Application = function () {

	var validationRules = getValidationRules ();

	return { init: init, validationRules: validationRules };

	function init () {

		enableBackToTop ();
		enableLightbox ();
		enableCirque ();
		enableEnhancedAccordion ();


		$('.ui-tooltip').tooltip();
	    $('.ui-popover').popover();


	}

	function enableCirque () {
		if ($.fn.lightbox) {
			$('.ui-lightbox').lightbox ();
		}
	}

	function enableLightbox () {
		if ($.fn.cirque) {
			$('.ui-cirque').cirque ({  });
		}
	}

	function enableBackToTop () {
		var backToTop = $('<a>', { id: 'back-to-top', href: '#top' });
		var icon = $('<i>', { class: 'icon-chevron-up' });

		backToTop.appendTo ('body');
		icon.appendTo (backToTop);

	    backToTop.hide();

	    $(window).scroll(function () {
	        if ($(this).scrollTop() > 150) {
	            backToTop.fadeIn ();
	        } else {
	            backToTop.fadeOut ();
	        }
	    });

	    backToTop.click (function (e) {
	    	e.preventDefault ();

	        $('body, html').animate({
	            scrollTop: 0
	        }, 600);
	    });
	}

	function enableEnhancedAccordion () {
		$('.accordion-toggle').on('click', function (e) {
	         $(e.target).parent ().parent ().parent ().addClass('open');
	    });

	    $('.accordion-toggle').on('click', function (e) {
	        $(this).parents ('.panel').siblings ().removeClass ('open');
	    });

	}

	function getValidationRules () {
		var custom = {
	    	focusCleanup: false,

			wrapper: 'div',
			errorElement: 'span',

			highlight: function(element) {
				$(element).parents ('.form-group').removeClass ('success').addClass('error');
			},
			success: function(element) {
				$(element).parents ('.form-group').removeClass ('error').addClass('success');
				$(element).parents ('.form-group:not(:has(.clean))').find ('div:last').before ('<div class="clean"></div>');
			},
			errorPlacement: function(error, element) {
				error.prependTo(element.parents ('.form-group'));
			}

	    };

	    return custom;
	}

}();
