var Autoevaluations = {
	index :  function (){
		consolelog("Load Autoevaluations.index");
		App.actualmodel='Autoevaluations';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.modalactioncrud();
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Autoevaluations.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Autoevaluations.beforeSubmit");
	},

	add : function(){
		consolelog("Load Autoevaluations.add");
/*
		App.actualmodel='Autoevaluations';
		App.actualaction='add';
		App.formsubmit();
*/
	},
	edit : function(){
		consolelog("Load Autoevaluations.edit");
		App.actualmodel='Autoevaluations';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Autoevaluations.delete");
	}
};
