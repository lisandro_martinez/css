var Categories = {
	index :  function (){
		consolelog("Load Categories.index");
		App.actualmodel='Categories';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.modalactioncrud();
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Productcategories.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Productcategories.beforeSubmit");
	},
	add : function(){
		consolelog("Load Categories.add");
		App.actualmodel='Categories';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Categories.edit");
		App.actualmodel='Categories';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Categories.delete");
	}
};
