var Groupactions = {
	index :  function (){
		consolelog("Load Groupactions.index");
		App.actualmodel='Groupactions';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	add : function(){
		consolelog("Load Groupactions.add");
		App.actualmodel='Groupactions';
		App.actualaction='add';
		App.formsubmit();
	},
	formdetails : function(){
		consolelog("Load Groupactions.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Groupactions.beforeSubmit");
	},
	acl : function(){
		consolelog("Load Groupactions.acl");

		App.actualmodel='Groupactions';
		App.actualaction='acl';

		$('.btn.btn-success').unbind("click").bind( "click",function (e) {
				  consolelog("Click success");
				  e.preventDefault();

				  urlaction = $(this).attr("href");
				  urlactionnew = urlaction.replace("acl/0","acl/1");
				  consolelog(urlaction);
				  consolelog(urlactionnew);

				  var options = {
				        beforeSubmit:  App.showRequest,  // pre-submit callback
				        success:       App.showResponse,  // post-submit callback
				     	url:       urlaction,
				  };

				  $(this).ajaxSubmit(options);

				  $(this).removeClass('btn-success').addClass('btn-warning');
				  relactual = $(this).attr('alttitle');
				  valactual = $(this).text();

				  consolelog(relactual);
				  consolelog(valactual);

				  $(this).text(relactual);
				  $(this).attr('alttitle',valactual);
				  $(this).attr('href',urlactionnew);
				  Groupactions.acl();
		});

		$('.btn.btn-warning').unbind("click").bind( "click",function (e) {
				  consolelog("Click warning");
				  e.preventDefault();

				  urlaction = $(this).attr("href");
				  urlactionnew = urlaction.replace("acl/1","acl/0");
				  consolelog(urlaction);
				  consolelog(urlactionnew);


				  var options = {
				        beforeSubmit:  App.showRequest,  // pre-submit callback
				        success:       App.showResponse,  // post-submit callback
				     	url:       urlaction,
				  };

				  $(this).ajaxSubmit(options);

				  $(this).removeClass('btn-warning').addClass('btn-success');
				  relactual = $(this).attr('alttitle');
				  valactual = $(this).text();

				  consolelog(relactual);
				  consolelog(valactual);

				  $(this).text(relactual);
				  $(this).attr('alttitle',valactual);
				  $(this).attr('href',urlactionnew);
				  Groupactions.acl();
		});

	},
	delete : function(){
		consolelog("Load Groupactions.delete");
	}
};
