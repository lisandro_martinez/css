var Nodetypes = {
	index :  function (){
		consolelog("Load Nodetypes.index");
		App.actualmodel='Nodetypes';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);

		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Nodetypes.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Nodetypes.beforeSubmit");
	},
	add : function(){
		consolelog("Load Nodetypes.add");
		App.actualmodel='Nodetypes';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Nodetypes.edit");
		App.actualmodel='Nodetypes';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Nodetypes.delete");
	}
};
