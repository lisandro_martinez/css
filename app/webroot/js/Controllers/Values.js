var Values = {
	index :  function (){
		consolelog("Load Values.index");
		App.actualmodel='Values';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.modalactioncrud();
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Values.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Values	.beforeSubmit");
	},
	add : function(){
		consolelog("Load Values.add");
	},
	edit : function(){
		consolelog("Load Values.edit");
	},
	delete : function(){
		consolelog("Load Values.delete");
	}
};
