var Modules = {
	index :  function (){
		consolelog("Load Modules.index");
		App.actualmodel='Modules';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);

		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Modules.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Modules.beforeSubmit");
	},
	add : function(){
		consolelog("Load Modules.add");
		App.actualmodel='Modules';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Modules.edit");
		App.actualmodel='Modules';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Modules.delete");
	}
};
