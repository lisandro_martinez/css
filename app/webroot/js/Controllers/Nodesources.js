var Nodesources = {
	index :  function (){
		consolelog("Load Nodesources.index");
		App.actualmodel='Nodesources';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);

		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Nodesources.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Nodesources.beforeSubmit");
	},
	add : function(){
		consolelog("Load Nodesources.add");
		App.actualmodel='Nodesources';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Nodesources.edit");
		App.actualmodel='Nodesources';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Nodesources.delete");
	}
};
