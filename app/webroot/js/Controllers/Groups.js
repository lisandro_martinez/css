var Groups = {
	index :  function (){
		consolelog("Load Groups.index");

		App.actualmodel='Groups';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);

		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Groups.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Groups.beforeSubmit");
	},
	add : function(){
		consolelog("Load Groups.add");

		App.actualmodel='Groups';
		App.actualaction='add';
		App.formsubmit();

	},
	edit : function(){
		consolelog("Load Groups.edit");

		App.actualmodel='Groups';
		App.actualaction='edit';
		App.formsubmit();
		this.index();

	},
	delete : function(){
		consolelog("Load Groups.delete");
	}
};
