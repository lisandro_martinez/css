var Indicatorvalues = {
	index :  function (){
		consolelog("Load Indicatorvalues.index");
		App.actualmodel='Indicatorvalues';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.modalactioncrud();
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Indicatorvalues.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Indicatorvalues.beforeSubmit");
	},
	add : function(){
		consolelog("Load Indicatorvalues.add");
		App.actualmodel='Indicatorvalues';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Indicatorvalues.edit");
		App.actualmodel='Indicatorvalues';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Indicatorvalues.delete");
	}
};
