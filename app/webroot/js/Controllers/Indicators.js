var Indicators = {
	index :  function (){
		consolelog("Load Indicators.index");
		App.actualmodel='Indicators';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.modalactioncrud();
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Indicators.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Indicators.beforeSubmit");
	},
	add : function(){
		consolelog("Load Indicators.add");
		App.actualmodel='Indicators';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Indicators.edit");
		App.actualmodel='Indicators';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Indicators.delete");
	}
};
