var Questions = {
	index :  function (){
		consolelog("Load Questions.index");
		App.actualmodel='Questions';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.modalactioncrud();
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Questions.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Questions.beforeSubmit");
	},
	add : function(){
		consolelog("Load Questions.add");
		App.actualmodel='Questions';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Questions.edit");
		App.actualmodel='Questions';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Questions.delete");
	}
};
