var Corporations = {
	index :  function (){
		consolelog("Load Corporations.index");
		App.actualmodel='Corporations';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.modalactioncrud();
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Corporations.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Corporations.beforeSubmit");
	},
	add : function(){
		consolelog("Load Corporations.add");
		App.actualmodel='Corporations';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Corporations.edit");
		App.actualmodel='Corporations';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Corporations.delete");
	}
};
