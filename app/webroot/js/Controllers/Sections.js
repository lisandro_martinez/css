var Sections = {
	index :  function (){
		consolelog("Load Sections.index");
		App.actualmodel='Sections';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.modalactioncrud();
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Sections.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Sections.beforeSubmit");
	},
	add : function(){
		consolelog("Load Sections.add");
		App.actualmodel='Sections';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Sections.edit");
		App.actualmodel='Sections';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Sections.delete");
	}
};
