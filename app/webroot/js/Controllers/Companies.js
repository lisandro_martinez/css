var Companies = {
	index :  function (){
		consolelog("Load Companies.index");
		App.actualmodel='Companies';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.modalactioncrud();
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Companies.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Companies.beforeSubmit");
	},
	add : function(){
		consolelog("Load Companies.add");
		App.actualmodel='Companies';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Companies.edit");
		App.actualmodel='Companies';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Companies.delete");
	}
};
