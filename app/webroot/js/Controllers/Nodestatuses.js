var Nodestatuses = {
	index :  function (){
		consolelog("Load Nodestatuses.index");
		App.actualmodel='Nodestatuses';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);

		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Nodestatuses.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Nodestatuses.beforeSubmit");
	},
	add : function(){
		consolelog("Load Nodestatuses.add");
		App.actualmodel='Nodestatuses';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Nodestatuses.edit");
		App.actualmodel='Nodestatuses';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Nodestatuses.delete");
	}
};
