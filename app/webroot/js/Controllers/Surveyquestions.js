var Surveyquestions = {
	index :  function (){
		consolelog("Load Surveyquestions.index");
		App.actualmodel='Surveyquestions';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.modalactioncrud();
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Surveyquestions.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Surveyquestions.beforeSubmit");
	},
	add : function(){
		consolelog("Load Surveyquestions.add");
		App.actualmodel='Surveyquestions';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Surveyquestions.edit");
		App.actualmodel='Surveyquestions';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Surveyquestions.delete");
	}
};
