var Nodos = {
	index :  function (){
		consolelog("Load Nodos.index");
		App.actualmodel='Nodos';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);

		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Nodos.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Nodos.beforeSubmit");
	},
	add : function(){
		consolelog("Load Nodos.add");
		App.actualmodel='Nodos';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Nodos.edit");
		App.actualmodel='Nodos';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Nodos.delete");
	}
};
