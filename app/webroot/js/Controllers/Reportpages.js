var Reportpages = {
	index :  function (){
		consolelog("Load Reportpages.index");
		App.actualmodel='Reportpages';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.modalactioncrud();
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Reportpages.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Reportpages.beforeSubmit");
	},
	add : function(){
		consolelog("Load Reportpages.add");
		App.actualmodel='Reportpages';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Reportpages.edit");
		App.actualmodel='Reportpages';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Reportpages.delete");
	}
};
