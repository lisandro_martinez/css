var Reports = {
	index :  function (){
		consolelog("Load Reports.index");
		App.actualmodel='Reports';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);

		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Reports.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Reports.beforeSubmit");
	},
	add : function(){
		consolelog("Load Reports.add");
		App.actualmodel='Reports';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Reports.edit");
		App.actualmodel='Reports';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Reports.delete");
	}
};
