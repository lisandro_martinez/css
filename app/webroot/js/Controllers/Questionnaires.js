var Questionnaires = {
	index :  function (){
		consolelog("Load Questionnaires.index");
		App.actualmodel='Questionnaires';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.modalactioncrud();
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Questionnaires.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Questionnaires.beforeSubmit");
	},
	add : function(){
		consolelog("Load Questionnaires.add");
		App.actualmodel='Questionnaires';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Questionnaires.edit");
		App.actualmodel='Questionnaires';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Questionnaires.delete");
	}
};
