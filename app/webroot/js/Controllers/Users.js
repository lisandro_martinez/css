var Users = {
	index :  function (){
		consolelog("Load Users.index");
		App.actualmodel='Users';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	login :  function (){
		consolelog("Load Users.login");
       	App.formsubmit();
	},
	beforeSubmit : function(data){
		consolelog("Load Users.beforeSubmit");
	},
	add : function(){
		consolelog("Load Users.add");
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Users.edit");
		App.actualmodel='Users';
		App.actualaction='edit';
		App.formsubmit();
		this.index();

	},
	resetpassword : function(){
		consolelog("Load Users.resetpassword");
		App.actualmodel='Users';
		App.actualaction='resetpassword';
		App.formsubmit();
		this.index();

	},
	delete : function(){
		consolelog("Load Users.delete");
	}
};
