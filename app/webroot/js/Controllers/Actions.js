var Actions = {
	index :  function (){
		consolelog("Load Actions.index");
		App.actualmodel='Actions';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.modalactioncrud();
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Actions.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Actions.beforeSubmit");
	},
	add : function(){
		consolelog("Load Actions.add");
		App.actualmodel='Actions';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Actions.edit");
		App.actualmodel='Actions';
		App.actualaction='edit';
		App.formsubmit();
		this.index();

	},
	delete : function(){
		consolelog("Load Actions.delete");
	}
};
