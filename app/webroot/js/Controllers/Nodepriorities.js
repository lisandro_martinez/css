var Nodepriorities = {
	index :  function (){
		consolelog("Load Nodepriorities.index");
		App.actualmodel='Nodepriorities';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);

		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Nodepriorities.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Nodepriorities.beforeSubmit");
	},
	add : function(){
		consolelog("Load Nodepriorities.add");
		App.actualmodel='Nodepriorities';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Nodepriorities.edit");
		App.actualmodel='Nodepriorities';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Nodepriorities.delete");
	}
};
