var Answers = {
	index :  function (){
		consolelog("Load Answers.index");
		App.actualmodel='Answers';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.modalactioncrud();
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Answers.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Answers.beforeSubmit");
	},
	add : function(){
		consolelog("Load Answers.add");
		App.actualmodel='Answers';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Answers.edit");
		App.actualmodel='Answers';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Answers.delete");
	}
};
