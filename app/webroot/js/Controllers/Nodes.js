var Nodes = {
	index :  function (){
		consolelog("Load Nodes.index");
		App.actualmodel='Nodes';
		App.actualaction='index';

		App.actionslist(App.actualmodel,App.actualaction);
		App.modalactioncrud();
		App.actionselectall('actionsdelete-check');
		App.deleteItems();
		App.multiactions();
	},
	formdetails : function(){
		consolelog("Load Nodes.formdetails");
	},
	beforeSubmit : function(data){
		consolelog("Load Nodes.beforeSubmit");
	},
	add : function(){
		consolelog("Load Nodes.add");
		App.actualmodel='Nodes';
		App.actualaction='add';
		App.formsubmit();
	},
	edit : function(){
		consolelog("Load Nodes.edit");
		App.actualmodel='Nodes';
		App.actualaction='edit';
		App.formsubmit();
		this.index();
	},
	delete : function(){
		consolelog("Load Nodes.delete");
	}
};
