CREATE DEFINER=`root`@`%` FUNCTION `BYMONTH`(`Param` date) RETURNS int(11)
BEGIN
declare v_mes int(11);
select month(Param) into v_mes;
if v_mes in (1,2) then
   return 1;
elseif v_mes in(3,4) then
   return 2;
elseif v_mes in(5,6) then
       return 3;
elseif v_mes in(7,8) then
       return 4;
elseif v_mes in(9,10) then
       return 5;
else 
     return 6;
end if;
     
  RETURN Param;
END;
