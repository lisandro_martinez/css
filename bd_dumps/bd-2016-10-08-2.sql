﻿delete from nodetypes where id in (12,13,14);
delete from nodes where nodetype_id in (12,13,14);
delete from groupactions where action_id in (63,65,66);
delete from actions where id in (63,65,66);

ALTER TABLE `nodes`
  ADD COLUMN `type` varchar(50) NULL DEFAULT NULL;
