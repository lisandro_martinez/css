CREATE DEFINER=`root`@`%` FUNCTION `CUATRIMESTRAL`(`Param` date) RETURNS int(11)
BEGIN
declare v_mes int(11);
select month(Param) into v_mes;
if v_mes in (1,2,3,4) then
   return 1;
elseif v_mes in(5,6,7,8) then
   return 2;
else 
     return 3;
end if;
     
  RETURN Param;
END;
