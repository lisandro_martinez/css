ALTER TABLE `indicators`
  ADD COLUMN `baseline` numeric(10,2) NULL DEFAULT NULL;

ALTER TABLE `indicators`
  ADD COLUMN `period_goal` numeric(10,2) NULL DEFAULT NULL;

ALTER TABLE `indicators`
  ADD COLUMN `flag_hightlighted` int(11) NULL DEFAULT NULL;



