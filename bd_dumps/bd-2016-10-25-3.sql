CREATE DEFINER=`root`@`%` FUNCTION `SEMESTRAL`(`Param` date) RETURNS int(11)
BEGIN
declare v_mes int(11);
select month(Param) into v_mes;
if v_mes in (1,2,3,4,5,6) then
   return 1;
else 
     return 2;
end if;
  RETURN Param;
END;


DELIMITER $$

CREATE FUNCTION `SEMESTRAL`(`Param` date) RETURNS int(11)
BEGIN
declare v_mes int(11);
select month(Param) into v_mes;
if v_mes in (1,2,3,4,5,6) then
   return 1;
else 
     return 2;
end if;
  RETURN Param;

END $$

DELIMITER ;
