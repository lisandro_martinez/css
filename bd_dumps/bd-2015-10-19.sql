-- MySQL dump 10.13  Distrib 5.5.42-37.1, for Linux (x86_64)
--
-- Host: localhost    Database: siriarte_css2
-- ------------------------------------------------------
-- Server version	5.5.42-37.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acos`
--

DROP TABLE IF EXISTS `acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_acos_lft_rght` (`lft`,`rght`),
  KEY `idx_acos_alias` (`alias`),
  KEY `idx_acos_model_foreign_key` (`model`,`foreign_key`)
) ENGINE=InnoDB AUTO_INCREMENT=452 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acos`
--

LOCK TABLES `acos` WRITE;
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;
INSERT INTO `acos` VALUES (1,NULL,NULL,NULL,'controllers',1,832),(2,1,NULL,NULL,'Actions',2,55),(3,2,NULL,NULL,'paramFilters',3,4),(4,2,NULL,NULL,'get_index',5,6),(5,2,NULL,NULL,'admin_index',7,8),(6,2,NULL,NULL,'post_add',9,10),(7,2,NULL,NULL,'get_add',11,12),(8,2,NULL,NULL,'admin_add',13,14),(9,2,NULL,NULL,'get_edit',15,16),(10,2,NULL,NULL,'post_edit',17,18),(11,2,NULL,NULL,'admin_edit',19,20),(12,2,NULL,NULL,'admin_deletemulti',21,22),(13,2,NULL,NULL,'admin_delete',23,24),(14,2,NULL,NULL,'recordsforpage',25,26),(15,2,NULL,NULL,'filterConfig',27,28),(16,2,NULL,NULL,'isRoot',29,30),(17,2,NULL,NULL,'setSidebarMenu',31,32),(18,2,NULL,NULL,'setHeaderMenu',33,34),(19,2,NULL,NULL,'is_Authorizate',35,36),(20,2,NULL,NULL,'getMethod',37,38),(21,2,NULL,NULL,'ajaxVariablesInit',39,40),(22,2,NULL,NULL,'installBase',41,42),(23,2,NULL,NULL,'syncACL',43,44),(24,2,NULL,NULL,'install_acl',45,46),(25,1,NULL,NULL,'Categories',56,109),(26,25,NULL,NULL,'paramFilters',57,58),(27,25,NULL,NULL,'get_index',59,60),(28,25,NULL,NULL,'admin_index',61,62),(29,25,NULL,NULL,'post_add',63,64),(30,25,NULL,NULL,'get_add',65,66),(31,25,NULL,NULL,'admin_add',67,68),(32,25,NULL,NULL,'get_edit',69,70),(33,25,NULL,NULL,'post_edit',71,72),(34,25,NULL,NULL,'admin_edit',73,74),(35,25,NULL,NULL,'admin_delete',75,76),(36,25,NULL,NULL,'admin_deletemulti',77,78),(37,25,NULL,NULL,'recordsforpage',79,80),(38,25,NULL,NULL,'filterConfig',81,82),(39,25,NULL,NULL,'isRoot',83,84),(40,25,NULL,NULL,'setSidebarMenu',85,86),(41,25,NULL,NULL,'setHeaderMenu',87,88),(42,25,NULL,NULL,'is_Authorizate',89,90),(43,25,NULL,NULL,'getMethod',91,92),(44,25,NULL,NULL,'ajaxVariablesInit',93,94),(45,25,NULL,NULL,'installBase',95,96),(46,25,NULL,NULL,'syncACL',97,98),(47,25,NULL,NULL,'install_acl',99,100),(48,1,NULL,NULL,'Groupactions',110,157),(49,48,NULL,NULL,'paramFilters',111,112),(50,48,NULL,NULL,'get_index',113,114),(51,48,NULL,NULL,'admin_index',115,116),(52,48,NULL,NULL,'post_add',117,118),(53,48,NULL,NULL,'admin_add',119,120),(54,48,NULL,NULL,'admin_delete',121,122),(55,48,NULL,NULL,'admin_deletemulti',123,124),(56,48,NULL,NULL,'admin_acl',125,126),(57,48,NULL,NULL,'recordsforpage',127,128),(58,48,NULL,NULL,'filterConfig',129,130),(59,48,NULL,NULL,'isRoot',131,132),(60,48,NULL,NULL,'setSidebarMenu',133,134),(61,48,NULL,NULL,'setHeaderMenu',135,136),(62,48,NULL,NULL,'is_Authorizate',137,138),(63,48,NULL,NULL,'getMethod',139,140),(64,48,NULL,NULL,'ajaxVariablesInit',141,142),(65,48,NULL,NULL,'installBase',143,144),(66,48,NULL,NULL,'syncACL',145,146),(67,48,NULL,NULL,'install_acl',147,148),(68,1,NULL,NULL,'Groups',158,211),(69,68,NULL,NULL,'paramFilters',159,160),(70,68,NULL,NULL,'get_index',161,162),(71,68,NULL,NULL,'admin_index',163,164),(72,68,NULL,NULL,'admin_delete',165,166),(73,68,NULL,NULL,'get_edit',167,168),(74,68,NULL,NULL,'post_edit',169,170),(75,68,NULL,NULL,'admin_edit',171,172),(76,68,NULL,NULL,'post_add',173,174),(77,68,NULL,NULL,'get_add',175,176),(78,68,NULL,NULL,'admin_add',177,178),(79,68,NULL,NULL,'admin_deletemulti',179,180),(80,68,NULL,NULL,'recordsforpage',181,182),(81,68,NULL,NULL,'filterConfig',183,184),(82,68,NULL,NULL,'isRoot',185,186),(83,68,NULL,NULL,'setSidebarMenu',187,188),(84,68,NULL,NULL,'setHeaderMenu',189,190),(85,68,NULL,NULL,'is_Authorizate',191,192),(86,68,NULL,NULL,'getMethod',193,194),(87,68,NULL,NULL,'ajaxVariablesInit',195,196),(88,68,NULL,NULL,'installBase',197,198),(89,68,NULL,NULL,'syncACL',199,200),(90,68,NULL,NULL,'install_acl',201,202),(91,1,NULL,NULL,'Modules',212,263),(92,91,NULL,NULL,'paramFilters',213,214),(93,91,NULL,NULL,'get_index',215,216),(94,91,NULL,NULL,'admin_index',217,218),(95,91,NULL,NULL,'post_add',219,220),(96,91,NULL,NULL,'admin_add',221,222),(97,91,NULL,NULL,'get_edit',223,224),(98,91,NULL,NULL,'post_edit',225,226),(99,91,NULL,NULL,'admin_edit',227,228),(100,91,NULL,NULL,'admin_delete',229,230),(101,91,NULL,NULL,'admin_deletemulti',231,232),(102,91,NULL,NULL,'recordsforpage',233,234),(103,91,NULL,NULL,'filterConfig',235,236),(104,91,NULL,NULL,'isRoot',237,238),(105,91,NULL,NULL,'setSidebarMenu',239,240),(106,91,NULL,NULL,'setHeaderMenu',241,242),(107,91,NULL,NULL,'is_Authorizate',243,244),(108,91,NULL,NULL,'getMethod',245,246),(109,91,NULL,NULL,'ajaxVariablesInit',247,248),(110,91,NULL,NULL,'installBase',249,250),(111,91,NULL,NULL,'syncACL',251,252),(112,91,NULL,NULL,'install_acl',253,254),(113,1,NULL,NULL,'Pages',264,297),(114,113,NULL,NULL,'display',265,266),(115,113,NULL,NULL,'recordsforpage',267,268),(116,113,NULL,NULL,'filterConfig',269,270),(117,113,NULL,NULL,'isRoot',271,272),(118,113,NULL,NULL,'setSidebarMenu',273,274),(119,113,NULL,NULL,'setHeaderMenu',275,276),(120,113,NULL,NULL,'is_Authorizate',277,278),(121,113,NULL,NULL,'getMethod',279,280),(122,113,NULL,NULL,'ajaxVariablesInit',281,282),(123,113,NULL,NULL,'installBase',283,284),(124,113,NULL,NULL,'syncACL',285,286),(125,113,NULL,NULL,'install_acl',287,288),(126,1,NULL,NULL,'Users',298,369),(127,126,NULL,NULL,'getConditionsRootGroup',299,300),(128,126,NULL,NULL,'getConditionsRootUser',301,302),(129,126,NULL,NULL,'admin_initDB',303,304),(130,126,NULL,NULL,'paramFilters',305,306),(131,126,NULL,NULL,'get_index',307,308),(132,126,NULL,NULL,'admin_index',309,310),(133,126,NULL,NULL,'post_add',311,312),(134,126,NULL,NULL,'admin_add',313,314),(135,126,NULL,NULL,'get_edit',315,316),(136,126,NULL,NULL,'post_edit',317,318),(137,126,NULL,NULL,'admin_edit',319,320),(138,126,NULL,NULL,'admin_delete',321,322),(139,126,NULL,NULL,'admin_deletemulti',323,324),(140,126,NULL,NULL,'post_resetpassword',325,326),(141,126,NULL,NULL,'admin_resetpassword',327,328),(142,126,NULL,NULL,'admin_home',329,330),(143,126,NULL,NULL,'get_login',331,332),(144,126,NULL,NULL,'post_login',333,334),(145,126,NULL,NULL,'admin_login',335,336),(146,126,NULL,NULL,'admin_logout',337,338),(147,126,NULL,NULL,'recordsforpage',339,340),(148,126,NULL,NULL,'filterConfig',341,342),(149,126,NULL,NULL,'isRoot',343,344),(150,126,NULL,NULL,'setSidebarMenu',345,346),(151,126,NULL,NULL,'setHeaderMenu',347,348),(152,126,NULL,NULL,'is_Authorizate',349,350),(153,126,NULL,NULL,'getMethod',351,352),(154,126,NULL,NULL,'ajaxVariablesInit',353,354),(155,126,NULL,NULL,'installBase',355,356),(156,126,NULL,NULL,'syncACL',357,358),(157,126,NULL,NULL,'install_acl',359,360),(158,1,NULL,NULL,'AclExtras',370,371),(159,1,NULL,NULL,'Companies',372,425),(160,159,NULL,NULL,'paramFilters',373,374),(161,159,NULL,NULL,'get_index',375,376),(162,159,NULL,NULL,'admin_index',377,378),(163,159,NULL,NULL,'post_add',379,380),(164,159,NULL,NULL,'get_add',381,382),(165,159,NULL,NULL,'admin_add',383,384),(166,159,NULL,NULL,'get_edit',385,386),(167,159,NULL,NULL,'post_edit',387,388),(168,159,NULL,NULL,'admin_edit',389,390),(169,159,NULL,NULL,'admin_delete',391,392),(170,159,NULL,NULL,'admin_deletemulti',393,394),(171,159,NULL,NULL,'recordsforpage',395,396),(172,159,NULL,NULL,'filterConfig',397,398),(173,159,NULL,NULL,'isRoot',399,400),(174,159,NULL,NULL,'setSidebarMenu',401,402),(175,159,NULL,NULL,'setHeaderMenu',403,404),(176,159,NULL,NULL,'is_Authorizate',405,406),(177,159,NULL,NULL,'getMethod',407,408),(178,159,NULL,NULL,'ajaxVariablesInit',409,410),(179,159,NULL,NULL,'installBase',411,412),(180,159,NULL,NULL,'syncACL',413,414),(181,159,NULL,NULL,'install_acl',415,416),(190,1,NULL,NULL,'Corporations',426,479),(191,190,NULL,NULL,'paramFilters',427,428),(192,190,NULL,NULL,'get_index',429,430),(193,190,NULL,NULL,'admin_index',431,432),(194,190,NULL,NULL,'post_add',433,434),(195,190,NULL,NULL,'get_add',435,436),(196,190,NULL,NULL,'admin_add',437,438),(197,190,NULL,NULL,'get_edit',439,440),(198,190,NULL,NULL,'post_edit',441,442),(199,190,NULL,NULL,'admin_edit',443,444),(200,190,NULL,NULL,'admin_delete',445,446),(201,190,NULL,NULL,'admin_deletemulti',447,448),(202,190,NULL,NULL,'recordsforpage',449,450),(203,190,NULL,NULL,'filterConfig',451,452),(204,190,NULL,NULL,'isRoot',453,454),(205,190,NULL,NULL,'setSidebarMenu',455,456),(206,190,NULL,NULL,'setHeaderMenu',457,458),(207,190,NULL,NULL,'is_Authorizate',459,460),(208,190,NULL,NULL,'getMethod',461,462),(209,190,NULL,NULL,'ajaxVariablesInit',463,464),(210,190,NULL,NULL,'installBase',465,466),(211,190,NULL,NULL,'syncACL',467,468),(212,190,NULL,NULL,'install_acl',469,470),(213,1,NULL,NULL,'Welcome',480,513),(214,213,NULL,NULL,'admin_index',481,482),(215,213,NULL,NULL,'recordsforpage',483,484),(216,213,NULL,NULL,'filterConfig',485,486),(217,213,NULL,NULL,'isRoot',487,488),(218,213,NULL,NULL,'setSidebarMenu',489,490),(219,213,NULL,NULL,'setHeaderMenu',491,492),(220,213,NULL,NULL,'is_Authorizate',493,494),(221,213,NULL,NULL,'getMethod',495,496),(222,213,NULL,NULL,'ajaxVariablesInit',497,498),(223,213,NULL,NULL,'installBase',499,500),(224,213,NULL,NULL,'syncACL',501,502),(225,213,NULL,NULL,'install_acl',503,504),(226,2,NULL,NULL,'getlocalesValidates',47,48),(227,2,NULL,NULL,'readWithLocale',49,50),(228,2,NULL,NULL,'getlocales',51,52),(229,2,NULL,NULL,'validationLocale',53,54),(230,25,NULL,NULL,'getlocalesValidates',101,102),(231,25,NULL,NULL,'readWithLocale',103,104),(232,25,NULL,NULL,'getlocales',105,106),(233,25,NULL,NULL,'validationLocale',107,108),(234,159,NULL,NULL,'getlocalesValidates',417,418),(235,159,NULL,NULL,'readWithLocale',419,420),(236,159,NULL,NULL,'getlocales',421,422),(237,159,NULL,NULL,'validationLocale',423,424),(238,190,NULL,NULL,'getlocalesValidates',471,472),(239,190,NULL,NULL,'readWithLocale',473,474),(240,190,NULL,NULL,'getlocales',475,476),(241,190,NULL,NULL,'validationLocale',477,478),(242,48,NULL,NULL,'getlocalesValidates',149,150),(243,48,NULL,NULL,'readWithLocale',151,152),(244,48,NULL,NULL,'getlocales',153,154),(245,48,NULL,NULL,'validationLocale',155,156),(246,68,NULL,NULL,'getlocalesValidates',203,204),(247,68,NULL,NULL,'readWithLocale',205,206),(248,68,NULL,NULL,'getlocales',207,208),(249,68,NULL,NULL,'validationLocale',209,210),(250,91,NULL,NULL,'getlocalesValidates',255,256),(251,91,NULL,NULL,'readWithLocale',257,258),(252,91,NULL,NULL,'getlocales',259,260),(253,91,NULL,NULL,'validationLocale',261,262),(254,113,NULL,NULL,'getlocalesValidates',289,290),(255,113,NULL,NULL,'readWithLocale',291,292),(256,113,NULL,NULL,'getlocales',293,294),(257,113,NULL,NULL,'validationLocale',295,296),(258,126,NULL,NULL,'getlocalesValidates',361,362),(259,126,NULL,NULL,'readWithLocale',363,364),(260,126,NULL,NULL,'getlocales',365,366),(261,126,NULL,NULL,'validationLocale',367,368),(262,213,NULL,NULL,'getlocalesValidates',505,506),(263,213,NULL,NULL,'readWithLocale',507,508),(264,213,NULL,NULL,'getlocales',509,510),(265,213,NULL,NULL,'validationLocale',511,512),(266,1,NULL,NULL,'Questionnaires',514,567),(267,266,NULL,NULL,'paramFilters',515,516),(268,266,NULL,NULL,'get_index',517,518),(269,266,NULL,NULL,'admin_index',519,520),(270,266,NULL,NULL,'post_add',521,522),(271,266,NULL,NULL,'get_add',523,524),(272,266,NULL,NULL,'admin_add',525,526),(273,266,NULL,NULL,'get_edit',527,528),(274,266,NULL,NULL,'post_edit',529,530),(275,266,NULL,NULL,'admin_edit',531,532),(276,266,NULL,NULL,'admin_delete',533,534),(277,266,NULL,NULL,'admin_deletemulti',535,536),(278,266,NULL,NULL,'recordsforpage',537,538),(279,266,NULL,NULL,'filterConfig',539,540),(280,266,NULL,NULL,'getlocalesValidates',541,542),(281,266,NULL,NULL,'readWithLocale',543,544),(282,266,NULL,NULL,'getlocales',545,546),(283,266,NULL,NULL,'isRoot',547,548),(284,266,NULL,NULL,'validationLocale',549,550),(285,266,NULL,NULL,'setSidebarMenu',551,552),(286,266,NULL,NULL,'setHeaderMenu',553,554),(287,266,NULL,NULL,'is_Authorizate',555,556),(288,266,NULL,NULL,'getMethod',557,558),(289,266,NULL,NULL,'ajaxVariablesInit',559,560),(290,266,NULL,NULL,'installBase',561,562),(291,266,NULL,NULL,'syncACL',563,564),(292,266,NULL,NULL,'install_acl',565,566),(320,1,NULL,NULL,'Sections',568,621),(321,320,NULL,NULL,'paramFilters',569,570),(322,320,NULL,NULL,'get_index',571,572),(323,320,NULL,NULL,'admin_index',573,574),(324,320,NULL,NULL,'post_add',575,576),(325,320,NULL,NULL,'get_add',577,578),(326,320,NULL,NULL,'admin_add',579,580),(327,320,NULL,NULL,'get_edit',581,582),(328,320,NULL,NULL,'post_edit',583,584),(329,320,NULL,NULL,'admin_edit',585,586),(330,320,NULL,NULL,'admin_delete',587,588),(331,320,NULL,NULL,'admin_deletemulti',589,590),(332,320,NULL,NULL,'recordsforpage',591,592),(333,320,NULL,NULL,'filterConfig',593,594),(334,320,NULL,NULL,'getlocalesValidates',595,596),(335,320,NULL,NULL,'readWithLocale',597,598),(336,320,NULL,NULL,'getlocales',599,600),(337,320,NULL,NULL,'isRoot',601,602),(338,320,NULL,NULL,'validationLocale',603,604),(339,320,NULL,NULL,'setSidebarMenu',605,606),(340,320,NULL,NULL,'setHeaderMenu',607,608),(341,320,NULL,NULL,'is_Authorizate',609,610),(342,320,NULL,NULL,'getMethod',611,612),(343,320,NULL,NULL,'ajaxVariablesInit',613,614),(344,320,NULL,NULL,'installBase',615,616),(345,320,NULL,NULL,'syncACL',617,618),(346,320,NULL,NULL,'install_acl',619,620),(347,1,NULL,NULL,'Questions',622,675),(348,347,NULL,NULL,'paramFilters',623,624),(349,347,NULL,NULL,'get_index',625,626),(350,347,NULL,NULL,'admin_index',627,628),(351,347,NULL,NULL,'post_add',629,630),(352,347,NULL,NULL,'get_add',631,632),(353,347,NULL,NULL,'admin_add',633,634),(354,347,NULL,NULL,'get_edit',635,636),(355,347,NULL,NULL,'post_edit',637,638),(356,347,NULL,NULL,'admin_edit',639,640),(357,347,NULL,NULL,'admin_delete',641,642),(358,347,NULL,NULL,'admin_deletemulti',643,644),(359,347,NULL,NULL,'recordsforpage',645,646),(360,347,NULL,NULL,'filterConfig',647,648),(361,347,NULL,NULL,'getlocalesValidates',649,650),(362,347,NULL,NULL,'readWithLocale',651,652),(363,347,NULL,NULL,'getlocales',653,654),(364,347,NULL,NULL,'isRoot',655,656),(365,347,NULL,NULL,'validationLocale',657,658),(366,347,NULL,NULL,'setSidebarMenu',659,660),(367,347,NULL,NULL,'setHeaderMenu',661,662),(368,347,NULL,NULL,'is_Authorizate',663,664),(369,347,NULL,NULL,'getMethod',665,666),(370,347,NULL,NULL,'ajaxVariablesInit',667,668),(371,347,NULL,NULL,'installBase',669,670),(372,347,NULL,NULL,'syncACL',671,672),(373,347,NULL,NULL,'install_acl',673,674),(374,1,NULL,NULL,'Answers',676,729),(375,374,NULL,NULL,'paramFilters',677,678),(376,374,NULL,NULL,'get_index',679,680),(377,374,NULL,NULL,'admin_index',681,682),(378,374,NULL,NULL,'post_add',683,684),(379,374,NULL,NULL,'get_add',685,686),(380,374,NULL,NULL,'admin_add',687,688),(381,374,NULL,NULL,'get_edit',689,690),(382,374,NULL,NULL,'post_edit',691,692),(383,374,NULL,NULL,'admin_edit',693,694),(384,374,NULL,NULL,'admin_delete',695,696),(385,374,NULL,NULL,'admin_deletemulti',697,698),(386,374,NULL,NULL,'recordsforpage',699,700),(387,374,NULL,NULL,'filterConfig',701,702),(388,374,NULL,NULL,'getlocalesValidates',703,704),(389,374,NULL,NULL,'readWithLocale',705,706),(390,374,NULL,NULL,'getlocales',707,708),(391,374,NULL,NULL,'isRoot',709,710),(392,374,NULL,NULL,'validationLocale',711,712),(393,374,NULL,NULL,'setSidebarMenu',713,714),(394,374,NULL,NULL,'setHeaderMenu',715,716),(395,374,NULL,NULL,'is_Authorizate',717,718),(396,374,NULL,NULL,'getMethod',719,720),(397,374,NULL,NULL,'ajaxVariablesInit',721,722),(398,374,NULL,NULL,'installBase',723,724),(399,374,NULL,NULL,'syncACL',725,726),(400,374,NULL,NULL,'install_acl',727,728),(401,1,NULL,NULL,'Companyquestionnaires',730,775),(402,401,NULL,NULL,'paramFilters',731,732),(403,401,NULL,NULL,'get_index',733,734),(404,401,NULL,NULL,'admin_index',735,736),(405,401,NULL,NULL,'post_add',737,738),(406,401,NULL,NULL,'admin_add',739,740),(407,401,NULL,NULL,'admin_delete',741,742),(408,401,NULL,NULL,'admin_deletemulti',743,744),(409,401,NULL,NULL,'recordsforpage',745,746),(410,401,NULL,NULL,'filterConfig',747,748),(411,401,NULL,NULL,'getlocalesValidates',749,750),(412,401,NULL,NULL,'readWithLocale',751,752),(413,401,NULL,NULL,'getlocales',753,754),(414,401,NULL,NULL,'isRoot',755,756),(415,401,NULL,NULL,'validationLocale',757,758),(416,401,NULL,NULL,'setSidebarMenu',759,760),(417,401,NULL,NULL,'setHeaderMenu',761,762),(418,401,NULL,NULL,'is_Authorizate',763,764),(419,401,NULL,NULL,'getMethod',765,766),(420,401,NULL,NULL,'ajaxVariablesInit',767,768),(421,401,NULL,NULL,'installBase',769,770),(422,401,NULL,NULL,'syncACL',771,772),(423,401,NULL,NULL,'install_acl',773,774),(424,1,NULL,NULL,'Autoevaluations',776,831),(425,424,NULL,NULL,'paramFilters',777,778),(426,424,NULL,NULL,'get_index',779,780),(427,424,NULL,NULL,'admin_index',781,782),(428,424,NULL,NULL,'post_add',783,784),(429,424,NULL,NULL,'get_add',785,786),(430,424,NULL,NULL,'admin_add',787,788),(431,424,NULL,NULL,'get_edit',789,790),(432,424,NULL,NULL,'post_edit',791,792),(433,424,NULL,NULL,'admin_edit',793,794),(434,424,NULL,NULL,'admin_delete',795,796),(435,424,NULL,NULL,'admin_deletemulti',797,798),(436,424,NULL,NULL,'recordsforpage',799,800),(437,424,NULL,NULL,'filterConfig',801,802),(438,424,NULL,NULL,'getlocalesValidates',803,804),(439,424,NULL,NULL,'readWithLocale',805,806),(440,424,NULL,NULL,'getlocales',807,808),(441,424,NULL,NULL,'isRoot',809,810),(442,424,NULL,NULL,'validationLocale',811,812),(443,424,NULL,NULL,'setSidebarMenu',813,814),(444,424,NULL,NULL,'setHeaderMenu',815,816),(445,424,NULL,NULL,'is_Authorizate',817,818),(446,424,NULL,NULL,'getMethod',819,820),(447,424,NULL,NULL,'ajaxVariablesInit',821,822),(448,424,NULL,NULL,'installBase',823,824),(449,424,NULL,NULL,'syncACL',825,826),(450,424,NULL,NULL,'install_acl',827,828),(451,424,NULL,NULL,'admin_view',829,830);
/*!40000 ALTER TABLE `acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actions`
--

DROP TABLE IF EXISTS `actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `order` int(2) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_actions_categories1_idx` (`category_id`),
  CONSTRAINT `fk_actions_categories1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actions`
--

LOCK TABLES `actions` WRITE;
/*!40000 ALTER TABLE `actions` DISABLE KEYS */;
INSERT INTO `actions` VALUES (30,'CategorÃ­as : Listado','/admin/categories/index',0,16,'2015-10-14 15:52:17','2015-10-14 15:52:17'),(31,'Corporaciones : Listado','/admin/corporations/index',0,10,'2015-10-14 15:52:37','2015-10-14 15:52:37'),(32,'Empresas : Listado','/admin/companies/index',0,11,'2015-10-14 15:52:59','2015-10-14 15:52:59'),(33,'Funciones : Listado','/admin/actions/index',0,17,'2015-10-14 15:53:55','2015-10-14 15:53:55'),(34,'Grupos : Listado','/admin/groups/index',0,14,'2015-10-14 15:54:19','2015-10-14 15:54:19'),(35,'Modulos : Listado','/admin/modules/index',0,15,'2015-10-14 15:54:40','2015-10-14 15:54:40'),(36,'Permisos : ACL','/admin/groupactions/acl',1,18,'2015-10-14 15:55:11','2015-10-14 15:55:11'),(37,'Permisos : Listado','/admin/groupactions/index',0,18,'2015-10-14 15:55:33','2015-10-14 15:55:33'),(38,'Usuarios : Listado','/admin/users/index',0,12,'2015-10-14 15:55:58','2015-10-14 15:55:58'),(39,'Usuarios : Sync','/admin/users/initDB',1,12,'2015-10-14 15:56:20','2015-10-14 15:56:20'),(40,'Administrar Cuestionarios','/admin/questionnaires/index/',0,19,'2015-10-16 11:09:31','2015-10-16 11:09:31'),(41,'Administrar Secciones','/admin/sections/index/',1,19,'2015-10-16 13:59:32','2015-10-17 11:07:24'),(42,'Administrar Preguntas','/admin/questions/index/ ',2,19,'2015-10-16 19:43:49','2015-10-17 11:07:30'),(43,'Administrar Respuestas','/admin/answers/index/',5,19,'2015-10-16 21:01:39','2015-10-17 11:07:34'),(44,'Asignar Cuestionarios a Empresas','/admin/companyquestionnaires/index',1,23,'2015-10-17 15:32:08','2015-10-17 15:35:34'),(45,'GestiÃ³n EstratÃ©gica','/admin/users/home/',0,24,'2015-10-19 10:29:33','2015-10-19 10:29:33'),(46,'PlanificaciÃ³n TÃ¡ctica','/admin/users/home/',0,27,'2015-10-19 10:29:54','2015-10-19 10:29:54'),(47,'GestiÃ³n Operativa','/admin/users/home/',0,25,'2015-10-19 10:30:16','2015-10-19 10:30:16'),(48,'Memoria y Cuenta','/admin/users/home/',0,26,'2015-10-19 10:30:29','2015-10-19 10:30:29'),(49,'Instrumentos','/admin/users/home/ ',0,28,'2015-10-19 10:43:50','2015-10-19 10:43:50'),(50,'Realizar una AutoevaluaciÃ³n','/admin/autoevaluations/index',1,29,'2015-10-19 23:24:27','2015-10-19 23:27:27');
/*!40000 ALTER TABLE `actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `points` int(11) NOT NULL,
  `feedback_title` text COLLATE utf8_unicode_ci,
  `feedback` text COLLATE utf8_unicode_ci,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `question_id` (`question_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,3,'sqwe rqwerqewr qwer ',12,'3','<div style=\"text-align: center;\"><span style=\"font-weight: bold; font-size: 36px;\">5</span></div>',0),(2,4,'1',7,'La retro','RetroalimentaciÃ³n en espaÃ±ol<br>',2),(3,3,'1',7,'3','<h1>5</h1><p>asdfasdf</p><p><img style=\"width: 25%;\" alt=\"http://www.bsntest.com/files/blocks/111006d69ff1c301b70e270c450a31e435ce91.jpg\" src=\"http://www.bsntest.com/files/blocks/111006d69ff1c301b70e270c450a31e435ce91.jpg\"></p>',0);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros`
--

DROP TABLE IF EXISTS `aros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aros_lft_rght` (`lft`,`rght`),
  KEY `idx_aros_alias` (`alias`),
  KEY `idx_aros_model_foreign_key` (`model`,`foreign_key`)
) ENGINE=InnoDB AUTO_INCREMENT=401 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros`
--

LOCK TABLES `aros` WRITE;
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;
INSERT INTO `aros` VALUES (1,NULL,'Group',1,NULL,1,6),(2,1,'User',1,NULL,2,3),(3,NULL,'Module',1,NULL,7,8),(4,NULL,'Category',1,NULL,9,10),(5,NULL,'Category',2,NULL,11,12),(6,NULL,'Category',3,NULL,13,14),(7,NULL,'Category',4,NULL,15,16),(8,NULL,'Category',5,NULL,17,18),(9,NULL,'Category',6,NULL,19,20),(10,NULL,'Module',2,NULL,21,22),(11,NULL,'Category',7,NULL,23,24),(12,NULL,'Company',1,NULL,25,26),(16,1,'User',3,NULL,4,5),(17,NULL,'Group',2,NULL,27,32),(20,NULL,'Module',3,NULL,33,34),(21,17,'User',6,NULL,28,29),(22,NULL,'Group',3,NULL,35,42),(23,NULL,'Company',4,NULL,43,44),(24,22,'User',7,NULL,36,37),(25,22,'User',8,NULL,38,39),(26,NULL,'Module',4,NULL,45,46),(27,NULL,'Category',8,NULL,47,48),(28,NULL,'Corporation',0,NULL,49,50),(30,NULL,'Corporation',3,NULL,51,52),(31,22,'User',9,NULL,40,41),(32,NULL,'Group',4,NULL,53,60),(33,32,'User',10,NULL,54,55),(34,NULL,'Corporation',4,NULL,61,62),(36,NULL,'Company',6,NULL,63,64),(37,NULL,'Company',7,NULL,65,66),(39,32,'User',11,NULL,56,57),(40,32,'User',12,NULL,58,59),(41,NULL,'Module',5,NULL,67,68),(42,NULL,'Category',9,NULL,69,70),(43,17,'User',13,NULL,30,31),(44,NULL,'Module',6,NULL,71,72),(45,NULL,'Module',7,NULL,73,74),(46,NULL,'Module',8,NULL,75,76),(48,NULL,'Group',5,NULL,77,80),(49,NULL,'Group',6,NULL,81,86),(50,NULL,'Group',7,NULL,87,92),(51,NULL,'Group',8,NULL,93,94),(53,NULL,'Module',11,NULL,95,96),(55,NULL,'Module',13,NULL,97,98),(57,NULL,'Category',10,NULL,99,100),(58,NULL,'Category',11,NULL,101,102),(59,NULL,'Category',12,NULL,103,104),(61,NULL,'Category',14,NULL,105,106),(62,NULL,'Category',15,NULL,107,108),(63,NULL,'Category',16,NULL,109,110),(64,NULL,'Category',17,NULL,111,112),(65,NULL,'Category',18,NULL,113,114),(66,48,'User',14,NULL,78,79),(67,NULL,'Corporation',5,NULL,115,116),(68,NULL,'Company',9,NULL,117,118),(69,NULL,'Company',10,NULL,119,120),(70,49,'User',15,NULL,82,83),(71,50,'User',16,NULL,88,89),(73,NULL,'Corporation',7,NULL,121,122),(74,NULL,'Company',11,NULL,123,124),(76,49,'User',17,NULL,84,85),(77,50,'User',18,NULL,90,91),(79,NULL,'Group',9,NULL,125,130),(80,79,'User',19,NULL,126,127),(81,79,'User',20,NULL,128,129),(82,NULL,'Questionnaire',0,NULL,131,132),(86,NULL,'Questionnaire',5,NULL,133,134),(88,NULL,'Module',15,NULL,135,136),(89,NULL,'Category',19,NULL,137,138),(90,NULL,'Section',0,NULL,139,140),(91,NULL,'Section',2,NULL,141,142),(92,NULL,'Section',1,NULL,143,144),(93,NULL,'Section',2,NULL,145,146),(95,NULL,'Questionnaire',7,NULL,147,148),(96,NULL,'Section',4,NULL,149,150),(99,NULL,'Question',3,NULL,151,152),(100,NULL,'Question',4,NULL,153,154),(101,NULL,'Question',5,NULL,155,156),(102,NULL,'Answer',1,NULL,157,158),(103,NULL,'Corporation',9,NULL,159,160),(104,NULL,'Company',13,NULL,161,162),(106,NULL,'Section',5,NULL,163,164),(107,NULL,'Section',6,NULL,165,166),(108,NULL,'Question',6,NULL,167,168),(109,NULL,'Answer',2,NULL,169,170),(110,NULL,'Answer',3,NULL,171,172),(114,NULL,'Questionnaire',9,NULL,173,174),(115,NULL,'Category',23,NULL,175,176),(116,NULL,'Module',16,NULL,177,178),(117,NULL,'Module',17,NULL,179,180),(118,NULL,'Module',18,NULL,181,182),(119,NULL,'Module',19,NULL,183,184),(120,NULL,'Category',24,NULL,185,186),(121,NULL,'Category',25,NULL,187,188),(122,NULL,'Category',26,NULL,189,190),(123,NULL,'Category',27,NULL,191,192),(124,NULL,'Module',20,NULL,193,194),(125,NULL,'Category',28,NULL,195,196),(126,NULL,'Company',14,NULL,197,198),(127,NULL,'Autoevaluation',1,NULL,199,200),(128,NULL,'Autoevaluation',2,NULL,201,202),(129,NULL,'Autoevaluation',3,NULL,203,204),(130,NULL,'Autoevaluation',4,NULL,205,206),(131,NULL,'Autoevaluation',5,NULL,207,208),(132,NULL,'Autoevaluation',6,NULL,209,210),(133,NULL,'Autoevaluation',7,NULL,211,212),(134,NULL,'Autoevaluation',8,NULL,213,214),(135,NULL,'Autoevaluation',9,NULL,215,216),(136,NULL,'Autoevaluation',10,NULL,217,218),(137,NULL,'Autoevaluation',11,NULL,219,220),(138,NULL,'Autoevaluation',12,NULL,221,222),(139,NULL,'Autoevaluation',13,NULL,223,224),(140,NULL,'Autoevaluation',14,NULL,225,226),(141,NULL,'Autoevaluation',15,NULL,227,228),(142,NULL,'Autoevaluation',16,NULL,229,230),(143,NULL,'Autoevaluation',17,NULL,231,232),(144,NULL,'Autoevaluation',18,NULL,233,234),(145,NULL,'Autoevaluation',19,NULL,235,236),(146,NULL,'Autoevaluationanswer',1,NULL,237,238),(147,NULL,'Autoevaluationanswer',2,NULL,239,240),(148,NULL,'Autoevaluation',20,NULL,241,242),(149,NULL,'Autoevaluationanswer',3,NULL,243,244),(150,NULL,'Autoevaluationanswer',4,NULL,245,246),(151,NULL,'Autoevaluation',21,NULL,247,248),(152,NULL,'Autoevaluationanswer',5,NULL,249,250),(153,NULL,'Autoevaluationanswer',6,NULL,251,252),(154,NULL,'Autoevaluation',22,NULL,253,254),(155,NULL,'Autoevaluationanswer',7,NULL,255,256),(156,NULL,'Autoevaluationanswer',8,NULL,257,258),(157,NULL,'Autoevaluation',23,NULL,259,260),(158,NULL,'Autoevaluationanswer',9,NULL,261,262),(159,NULL,'Autoevaluationanswer',10,NULL,263,264),(160,NULL,'Autoevaluation',24,NULL,265,266),(161,NULL,'Autoevaluationanswer',11,NULL,267,268),(162,NULL,'Autoevaluationanswer',12,NULL,269,270),(163,NULL,'Autoevaluation',25,NULL,271,272),(164,NULL,'Autoevaluationanswer',13,NULL,273,274),(165,NULL,'Autoevaluationanswer',14,NULL,275,276),(166,NULL,'Autoevaluation',26,NULL,277,278),(167,NULL,'Autoevaluationanswer',15,NULL,279,280),(168,NULL,'Autoevaluationanswer',16,NULL,281,282),(169,NULL,'Autoevaluation',27,NULL,283,284),(170,NULL,'Autoevaluation',28,NULL,285,286),(171,NULL,'Autoevaluationanswer',17,NULL,287,288),(172,NULL,'Autoevaluationanswer',18,NULL,289,290),(173,NULL,'Autoevaluation',29,NULL,291,292),(174,NULL,'Autoevaluation',30,NULL,293,294),(175,NULL,'Autoevaluationanswer',19,NULL,295,296),(176,NULL,'Autoevaluationanswer',20,NULL,297,298),(177,NULL,'Autoevaluation',31,NULL,299,300),(178,NULL,'Autoevaluation',32,NULL,301,302),(179,NULL,'Autoevaluationanswer',21,NULL,303,304),(180,NULL,'Autoevaluationanswer',22,NULL,305,306),(181,NULL,'Autoevaluation',5,NULL,307,308),(182,NULL,'Autoevaluation',17,NULL,309,310),(183,NULL,'Autoevaluation',11,NULL,311,312),(184,NULL,'Autoevaluation',7,NULL,313,314),(185,NULL,'Autoevaluation',33,NULL,315,316),(186,NULL,'Autoevaluationanswer',23,NULL,317,318),(187,NULL,'Autoevaluationanswer',24,NULL,319,320),(188,NULL,'Autoevaluation',34,NULL,321,322),(189,NULL,'Autoevaluationanswer',25,NULL,323,324),(190,NULL,'Autoevaluationanswer',26,NULL,325,326),(191,NULL,'Autoevaluation',35,NULL,327,328),(192,NULL,'Autoevaluationanswer',27,NULL,329,330),(193,NULL,'Autoevaluationanswer',28,NULL,331,332),(194,NULL,'Autoevaluation',36,NULL,333,334),(195,NULL,'Autoevaluationanswer',29,NULL,335,336),(196,NULL,'Autoevaluationanswer',30,NULL,337,338),(197,NULL,'Autoevaluation',37,NULL,339,340),(198,NULL,'Autoevaluationanswer',31,NULL,341,342),(199,NULL,'Autoevaluationanswer',32,NULL,343,344),(200,NULL,'Autoevaluation',38,NULL,345,346),(201,NULL,'Autoevaluationanswer',33,NULL,347,348),(202,NULL,'Autoevaluationanswer',34,NULL,349,350),(203,NULL,'Autoevaluation',39,NULL,351,352),(204,NULL,'Autoevaluationanswer',35,NULL,353,354),(205,NULL,'Autoevaluationanswer',36,NULL,355,356),(206,NULL,'Autoevaluation',40,NULL,357,358),(207,NULL,'Autoevaluationanswer',37,NULL,359,360),(208,NULL,'Autoevaluationanswer',38,NULL,361,362),(209,NULL,'Autoevaluation',41,NULL,363,364),(210,NULL,'Autoevaluationanswer',39,NULL,365,366),(211,NULL,'Autoevaluationanswer',40,NULL,367,368),(212,NULL,'Autoevaluation',42,NULL,369,370),(213,NULL,'Autoevaluationanswer',41,NULL,371,372),(214,NULL,'Autoevaluationanswer',42,NULL,373,374),(215,NULL,'Autoevaluation',43,NULL,375,376),(216,NULL,'Autoevaluationanswer',43,NULL,377,378),(217,NULL,'Autoevaluationanswer',44,NULL,379,380),(218,NULL,'Autoevaluation',44,NULL,381,382),(219,NULL,'Autoevaluationanswer',45,NULL,383,384),(220,NULL,'Autoevaluationanswer',46,NULL,385,386),(221,NULL,'Autoevaluation',45,NULL,387,388),(222,NULL,'Autoevaluationanswer',47,NULL,389,390),(223,NULL,'Autoevaluationanswer',48,NULL,391,392),(224,NULL,'Autoevaluation',46,NULL,393,394),(225,NULL,'Autoevaluationanswer',49,NULL,395,396),(226,NULL,'Autoevaluationanswer',50,NULL,397,398),(228,NULL,'Autoevaluationanswer',51,NULL,399,400),(229,NULL,'Autoevaluationanswer',52,NULL,401,402),(230,NULL,'Autoevaluation',48,NULL,403,404),(231,NULL,'Autoevaluationanswer',53,NULL,405,406),(232,NULL,'Autoevaluationanswer',54,NULL,407,408),(233,NULL,'Autoevaluation',49,NULL,409,410),(234,NULL,'Autoevaluationanswer',55,NULL,411,412),(235,NULL,'Autoevaluationanswer',56,NULL,413,414),(236,NULL,'Autoevaluation',50,NULL,415,416),(237,NULL,'Autoevaluationanswer',57,NULL,417,418),(238,NULL,'Autoevaluationanswer',58,NULL,419,420),(239,NULL,'Autoevaluation',51,NULL,421,422),(240,NULL,'Autoevaluationanswer',59,NULL,423,424),(241,NULL,'Autoevaluationanswer',60,NULL,425,426),(242,NULL,'Autoevaluation',52,NULL,427,428),(243,NULL,'Autoevaluationanswer',61,NULL,429,430),(244,NULL,'Autoevaluationanswer',62,NULL,431,432),(245,NULL,'Autoevaluation',53,NULL,433,434),(246,NULL,'Autoevaluationanswer',63,NULL,435,436),(247,NULL,'Autoevaluationanswer',64,NULL,437,438),(248,NULL,'Autoevaluation',54,NULL,439,440),(249,NULL,'Autoevaluationanswer',65,NULL,441,442),(250,NULL,'Autoevaluationanswer',66,NULL,443,444),(251,NULL,'Autoevaluation',55,NULL,445,446),(252,NULL,'Autoevaluationanswer',67,NULL,447,448),(253,NULL,'Autoevaluationanswer',68,NULL,449,450),(254,NULL,'Autoevaluation',56,NULL,451,452),(255,NULL,'Autoevaluationanswer',69,NULL,453,454),(256,NULL,'Autoevaluationanswer',70,NULL,455,456),(257,NULL,'Autoevaluation',57,NULL,457,458),(258,NULL,'Autoevaluationanswer',71,NULL,459,460),(259,NULL,'Autoevaluationanswer',72,NULL,461,462),(260,NULL,'Autoevaluation',58,NULL,463,464),(261,NULL,'Autoevaluationanswer',73,NULL,465,466),(262,NULL,'Autoevaluationanswer',74,NULL,467,468),(263,NULL,'Autoevaluation',59,NULL,469,470),(264,NULL,'Autoevaluationanswer',75,NULL,471,472),(265,NULL,'Autoevaluationanswer',76,NULL,473,474),(266,NULL,'Autoevaluation',60,NULL,475,476),(267,NULL,'Autoevaluationanswer',77,NULL,477,478),(268,NULL,'Autoevaluationanswer',78,NULL,479,480),(269,NULL,'Autoevaluation',61,NULL,481,482),(270,NULL,'Autoevaluationanswer',79,NULL,483,484),(271,NULL,'Autoevaluationanswer',80,NULL,485,486),(272,NULL,'Autoevaluation',62,NULL,487,488),(273,NULL,'Autoevaluationanswer',81,NULL,489,490),(274,NULL,'Autoevaluationanswer',82,NULL,491,492),(275,NULL,'Autoevaluation',63,NULL,493,494),(276,NULL,'Autoevaluationanswer',83,NULL,495,496),(277,NULL,'Autoevaluationanswer',84,NULL,497,498),(278,NULL,'Autoevaluation',64,NULL,499,500),(279,NULL,'Autoevaluationanswer',85,NULL,501,502),(280,NULL,'Autoevaluationanswer',86,NULL,503,504),(281,NULL,'Autoevaluation',65,NULL,505,506),(282,NULL,'Autoevaluationanswer',87,NULL,507,508),(283,NULL,'Autoevaluationanswer',88,NULL,509,510),(284,NULL,'Autoevaluation',66,NULL,511,512),(285,NULL,'Autoevaluationanswer',89,NULL,513,514),(286,NULL,'Autoevaluationanswer',90,NULL,515,516),(287,NULL,'Autoevaluation',67,NULL,517,518),(288,NULL,'Autoevaluationanswer',91,NULL,519,520),(289,NULL,'Autoevaluationanswer',92,NULL,521,522),(290,NULL,'Autoevaluation',68,NULL,523,524),(291,NULL,'Autoevaluationanswer',93,NULL,525,526),(292,NULL,'Autoevaluationanswer',94,NULL,527,528),(293,NULL,'Autoevaluation',69,NULL,529,530),(294,NULL,'Autoevaluationanswer',95,NULL,531,532),(295,NULL,'Autoevaluationanswer',96,NULL,533,534),(296,NULL,'Autoevaluation',70,NULL,535,536),(297,NULL,'Autoevaluationanswer',97,NULL,537,538),(298,NULL,'Autoevaluationanswer',98,NULL,539,540),(299,NULL,'Autoevaluation',71,NULL,541,542),(300,NULL,'Autoevaluationanswer',99,NULL,543,544),(301,NULL,'Autoevaluationanswer',100,NULL,545,546),(302,NULL,'Autoevaluation',72,NULL,547,548),(303,NULL,'Autoevaluationanswer',101,NULL,549,550),(304,NULL,'Autoevaluationanswer',102,NULL,551,552),(305,NULL,'Autoevaluation',73,NULL,553,554),(306,NULL,'Autoevaluationanswer',103,NULL,555,556),(307,NULL,'Autoevaluationanswer',104,NULL,557,558),(308,NULL,'Autoevaluation',74,NULL,559,560),(309,NULL,'Autoevaluationanswer',105,NULL,561,562),(310,NULL,'Autoevaluationanswer',106,NULL,563,564),(312,NULL,'Autoevaluationanswer',107,NULL,565,566),(313,NULL,'Autoevaluationanswer',108,NULL,567,568),(314,NULL,'Autoevaluation',76,NULL,569,570),(315,NULL,'Autoevaluationanswer',109,NULL,571,572),(316,NULL,'Autoevaluationanswer',110,NULL,573,574),(317,NULL,'Autoevaluation',77,NULL,575,576),(318,NULL,'Autoevaluationanswer',111,NULL,577,578),(319,NULL,'Autoevaluationanswer',112,NULL,579,580),(320,NULL,'Autoevaluation',78,NULL,581,582),(321,NULL,'Autoevaluationanswer',113,NULL,583,584),(322,NULL,'Autoevaluationanswer',114,NULL,585,586),(323,NULL,'Autoevaluation',79,NULL,587,588),(324,NULL,'Autoevaluationanswer',115,NULL,589,590),(325,NULL,'Autoevaluationanswer',116,NULL,591,592),(326,NULL,'Autoevaluation',80,NULL,593,594),(327,NULL,'Autoevaluationanswer',117,NULL,595,596),(328,NULL,'Autoevaluationanswer',118,NULL,597,598),(329,NULL,'Autoevaluation',81,NULL,599,600),(330,NULL,'Autoevaluationanswer',119,NULL,601,602),(331,NULL,'Autoevaluationanswer',120,NULL,603,604),(332,NULL,'Autoevaluation',82,NULL,605,606),(333,NULL,'Autoevaluationanswer',121,NULL,607,608),(334,NULL,'Autoevaluationanswer',122,NULL,609,610),(335,NULL,'Autoevaluation',83,NULL,611,612),(336,NULL,'Autoevaluationanswer',123,NULL,613,614),(337,NULL,'Autoevaluationanswer',124,NULL,615,616),(338,NULL,'Autoevaluation',84,NULL,617,618),(339,NULL,'Autoevaluationanswer',125,NULL,619,620),(340,NULL,'Autoevaluationanswer',126,NULL,621,622),(341,NULL,'Autoevaluation',85,NULL,623,624),(342,NULL,'Autoevaluationanswer',127,NULL,625,626),(343,NULL,'Autoevaluationanswer',128,NULL,627,628),(344,NULL,'Autoevaluation',86,NULL,629,630),(345,NULL,'Autoevaluationanswer',129,NULL,631,632),(346,NULL,'Autoevaluationanswer',130,NULL,633,634),(347,NULL,'Autoevaluation',87,NULL,635,636),(348,NULL,'Autoevaluationanswer',131,NULL,637,638),(349,NULL,'Autoevaluationanswer',132,NULL,639,640),(350,NULL,'Autoevaluation',88,NULL,641,642),(351,NULL,'Autoevaluationanswer',133,NULL,643,644),(352,NULL,'Autoevaluationanswer',134,NULL,645,646),(353,NULL,'Autoevaluation',89,NULL,647,648),(354,NULL,'Autoevaluationanswer',135,NULL,649,650),(355,NULL,'Autoevaluationanswer',136,NULL,651,652),(356,NULL,'Autoevaluation',90,NULL,653,654),(357,NULL,'Autoevaluationanswer',137,NULL,655,656),(358,NULL,'Autoevaluationanswer',138,NULL,657,658),(359,NULL,'Autoevaluation',91,NULL,659,660),(360,NULL,'Autoevaluationanswer',139,NULL,661,662),(361,NULL,'Autoevaluationanswer',140,NULL,663,664),(362,NULL,'Autoevaluation',92,NULL,665,666),(363,NULL,'Autoevaluationanswer',141,NULL,667,668),(364,NULL,'Autoevaluationanswer',142,NULL,669,670),(365,NULL,'Autoevaluation',93,NULL,671,672),(366,NULL,'Autoevaluationanswer',143,NULL,673,674),(367,NULL,'Autoevaluationanswer',144,NULL,675,676),(368,NULL,'Autoevaluation',94,NULL,677,678),(369,NULL,'Autoevaluationanswer',145,NULL,679,680),(370,NULL,'Autoevaluationanswer',146,NULL,681,682),(371,NULL,'Autoevaluation',95,NULL,683,684),(372,NULL,'Autoevaluationanswer',147,NULL,685,686),(373,NULL,'Autoevaluationanswer',148,NULL,687,688),(374,NULL,'Autoevaluation',96,NULL,689,690),(375,NULL,'Autoevaluationanswer',149,NULL,691,692),(376,NULL,'Autoevaluationanswer',150,NULL,693,694),(378,NULL,'Autoevaluationanswer',151,NULL,695,696),(379,NULL,'Autoevaluationanswer',152,NULL,697,698),(381,NULL,'Autoevaluationanswer',153,NULL,699,700),(382,NULL,'Autoevaluationanswer',154,NULL,701,702),(384,NULL,'Autoevaluationanswer',155,NULL,703,704),(385,NULL,'Autoevaluationanswer',156,NULL,705,706),(387,NULL,'Autoevaluationanswer',157,NULL,707,708),(388,NULL,'Autoevaluationanswer',158,NULL,709,710),(390,NULL,'Autoevaluationanswer',159,NULL,711,712),(391,NULL,'Autoevaluationanswer',160,NULL,713,714),(393,NULL,'Autoevaluationanswer',161,NULL,715,716),(394,NULL,'Autoevaluationanswer',162,NULL,717,718),(395,NULL,'Category',29,NULL,719,720),(397,NULL,'Autoevaluation',104,NULL,721,722),(398,NULL,'Autoevaluation',105,NULL,723,724),(399,NULL,'Autoevaluationanswer',163,NULL,725,726),(400,NULL,'Autoevaluationanswer',164,NULL,727,728);
/*!40000 ALTER TABLE `aros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`),
  KEY `aco_id` (`aco_id`),
  CONSTRAINT `aros_acos_ibfk_1` FOREIGN KEY (`aro_id`) REFERENCES `aros` (`id`),
  CONSTRAINT `aros_acos_ibfk_2` FOREIGN KEY (`aco_id`) REFERENCES `acos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=755 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros_acos`
--

LOCK TABLES `aros_acos` WRITE;
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;
INSERT INTO `aros_acos` VALUES (1,2,1,'1','1','1','1'),(2,1,1,'1','1','1','1'),(3,17,126,'1','1','1','1'),(4,17,159,'-1','-1','-1','-1'),(5,22,142,'1','1','1','1'),(6,22,143,'1','1','1','1'),(7,22,144,'1','1','1','1'),(8,22,145,'1','1','1','1'),(9,22,146,'1','1','1','1'),(10,22,147,'1','1','1','1'),(11,22,148,'1','1','1','1'),(12,22,149,'1','1','1','1'),(13,22,150,'1','1','1','1'),(14,22,151,'1','1','1','1'),(15,22,152,'1','1','1','1'),(16,22,153,'1','1','1','1'),(17,22,154,'1','1','1','1'),(18,17,190,'-1','-1','-1','-1'),(19,17,191,'-1','-1','-1','-1'),(20,17,192,'-1','-1','-1','-1'),(21,17,193,'-1','-1','-1','-1'),(22,17,194,'-1','-1','-1','-1'),(23,17,195,'-1','-1','-1','-1'),(24,17,196,'-1','-1','-1','-1'),(25,17,197,'-1','-1','-1','-1'),(26,17,198,'-1','-1','-1','-1'),(27,17,199,'-1','-1','-1','-1'),(28,17,200,'-1','-1','-1','-1'),(29,17,201,'-1','-1','-1','-1'),(30,17,202,'-1','-1','-1','-1'),(31,17,203,'-1','-1','-1','-1'),(32,17,204,'-1','-1','-1','-1'),(33,17,205,'-1','-1','-1','-1'),(34,17,206,'-1','-1','-1','-1'),(35,17,207,'-1','-1','-1','-1'),(36,17,208,'-1','-1','-1','-1'),(37,17,209,'-1','-1','-1','-1'),(38,17,210,'-1','-1','-1','-1'),(39,17,211,'-1','-1','-1','-1'),(40,17,212,'-1','-1','-1','-1'),(41,32,126,'1','1','1','1'),(42,32,127,'1','1','1','1'),(43,32,128,'1','1','1','1'),(44,32,129,'1','1','1','1'),(45,32,130,'1','1','1','1'),(46,32,131,'1','1','1','1'),(47,32,132,'1','1','1','1'),(48,32,133,'1','1','1','1'),(49,32,134,'1','1','1','1'),(50,32,135,'1','1','1','1'),(51,32,136,'1','1','1','1'),(52,32,137,'1','1','1','1'),(53,32,138,'1','1','1','1'),(54,32,139,'1','1','1','1'),(55,32,140,'1','1','1','1'),(56,32,141,'1','1','1','1'),(57,32,142,'1','1','1','1'),(58,32,143,'1','1','1','1'),(59,32,144,'1','1','1','1'),(60,32,145,'1','1','1','1'),(61,32,146,'1','1','1','1'),(62,32,147,'1','1','1','1'),(63,32,148,'1','1','1','1'),(64,32,149,'1','1','1','1'),(65,32,150,'1','1','1','1'),(66,32,151,'1','1','1','1'),(67,32,152,'1','1','1','1'),(68,32,153,'1','1','1','1'),(69,32,154,'1','1','1','1'),(70,32,155,'1','1','1','1'),(71,32,156,'1','1','1','1'),(72,32,157,'1','1','1','1'),(73,32,158,'1','1','1','1'),(75,32,159,'1','1','1','1'),(76,32,160,'1','1','1','1'),(77,32,161,'1','1','1','1'),(78,32,162,'1','1','1','1'),(79,32,163,'1','1','1','1'),(80,32,164,'1','1','1','1'),(81,32,165,'1','1','1','1'),(82,32,166,'1','1','1','1'),(83,32,167,'1','1','1','1'),(84,32,168,'1','1','1','1'),(85,32,169,'1','1','1','1'),(86,32,170,'1','1','1','1'),(87,32,171,'1','1','1','1'),(88,32,172,'1','1','1','1'),(89,32,173,'1','1','1','1'),(90,32,174,'1','1','1','1'),(91,32,175,'1','1','1','1'),(92,32,176,'1','1','1','1'),(93,32,177,'1','1','1','1'),(94,32,178,'1','1','1','1'),(95,32,179,'1','1','1','1'),(96,32,180,'1','1','1','1'),(97,32,181,'1','1','1','1'),(98,32,190,'1','1','1','1'),(99,32,191,'1','1','1','1'),(100,32,192,'1','1','1','1'),(101,32,193,'1','1','1','1'),(102,32,194,'1','1','1','1'),(103,32,195,'1','1','1','1'),(104,32,196,'1','1','1','1'),(105,32,197,'1','1','1','1'),(106,32,198,'1','1','1','1'),(107,32,199,'1','1','1','1'),(108,32,200,'1','1','1','1'),(109,32,202,'1','1','1','1'),(110,32,201,'1','1','1','1'),(111,32,203,'1','1','1','1'),(112,32,204,'1','1','1','1'),(113,32,205,'1','1','1','1'),(114,32,206,'1','1','1','1'),(115,32,207,'1','1','1','1'),(116,32,208,'1','1','1','1'),(117,32,209,'1','1','1','1'),(118,32,210,'1','1','1','1'),(119,32,211,'1','1','1','1'),(120,32,212,'1','1','1','1'),(123,17,213,'1','1','1','1'),(124,22,213,'1','1','1','1'),(125,32,213,'1','1','1','1'),(126,32,214,'1','1','1','1'),(127,22,214,'1','1','1','1'),(128,17,214,'1','1','1','1'),(129,17,215,'1','1','1','1'),(130,22,215,'1','1','1','1'),(131,32,215,'1','1','1','1'),(132,32,216,'1','1','1','1'),(133,22,216,'1','1','1','1'),(134,17,216,'1','1','1','1'),(135,17,217,'1','1','1','1'),(136,22,217,'1','1','1','1'),(137,32,217,'1','1','1','1'),(138,32,218,'1','1','1','1'),(139,22,218,'1','1','1','1'),(140,17,218,'1','1','1','1'),(141,17,219,'1','1','1','1'),(142,22,219,'1','1','1','1'),(143,32,219,'1','1','1','1'),(144,32,220,'1','1','1','1'),(145,22,220,'1','1','1','1'),(146,17,220,'1','1','1','1'),(147,17,221,'1','1','1','1'),(148,22,221,'1','1','1','1'),(149,32,221,'1','1','1','1'),(150,32,222,'1','1','1','1'),(151,22,222,'1','1','1','1'),(152,17,222,'1','1','1','1'),(153,17,223,'1','1','1','1'),(154,22,223,'1','1','1','1'),(155,32,223,'1','1','1','1'),(156,32,224,'1','1','1','1'),(157,22,224,'1','1','1','1'),(158,17,224,'1','1','1','1'),(159,17,225,'1','1','1','1'),(160,22,225,'1','1','1','1'),(161,32,225,'1','1','1','1'),(162,17,160,'-1','-1','-1','-1'),(163,17,161,'-1','-1','-1','-1'),(164,17,162,'-1','-1','-1','-1'),(165,17,163,'-1','-1','-1','-1'),(166,17,164,'-1','-1','-1','-1'),(167,17,165,'-1','-1','-1','-1'),(168,17,166,'-1','-1','-1','-1'),(169,17,167,'-1','-1','-1','-1'),(170,17,168,'-1','-1','-1','-1'),(171,17,169,'-1','-1','-1','-1'),(172,17,170,'-1','-1','-1','-1'),(173,17,171,'-1','-1','-1','-1'),(174,17,172,'-1','-1','-1','-1'),(175,17,173,'-1','-1','-1','-1'),(176,17,175,'-1','-1','-1','-1'),(177,17,176,'-1','-1','-1','-1'),(178,17,174,'-1','-1','-1','-1'),(179,17,177,'-1','-1','-1','-1'),(180,17,178,'-1','-1','-1','-1'),(181,17,179,'-1','-1','-1','-1'),(182,17,180,'-1','-1','-1','-1'),(183,17,181,'-1','-1','-1','-1'),(184,22,140,'1','1','1','1'),(185,22,141,'1','1','1','1'),(186,48,1,'1','1','1','1'),(187,48,2,'1','1','1','1'),(188,48,3,'1','1','1','1'),(189,48,4,'1','1','1','1'),(190,48,5,'1','1','1','1'),(191,48,6,'1','1','1','1'),(192,48,7,'1','1','1','1'),(193,48,8,'1','1','1','1'),(194,48,9,'1','1','1','1'),(195,48,10,'1','1','1','1'),(196,48,11,'1','1','1','1'),(197,48,12,'1','1','1','1'),(198,48,13,'1','1','1','1'),(199,48,14,'1','1','1','1'),(200,48,15,'1','1','1','1'),(201,48,16,'1','1','1','1'),(202,48,17,'1','1','1','1'),(203,48,18,'1','1','1','1'),(204,48,19,'1','1','1','1'),(205,48,20,'1','1','1','1'),(206,48,21,'1','1','1','1'),(207,48,22,'1','1','1','1'),(208,48,23,'1','1','1','1'),(209,48,24,'1','1','1','1'),(210,48,25,'1','1','1','1'),(211,48,26,'1','1','1','1'),(212,48,27,'1','1','1','1'),(213,48,28,'1','1','1','1'),(214,48,29,'1','1','1','1'),(215,48,30,'1','1','1','1'),(216,48,31,'1','1','1','1'),(217,48,32,'1','1','1','1'),(218,48,33,'1','1','1','1'),(219,48,34,'1','1','1','1'),(220,48,35,'1','1','1','1'),(221,48,36,'1','1','1','1'),(222,48,37,'1','1','1','1'),(223,48,38,'1','1','1','1'),(224,48,39,'1','1','1','1'),(225,48,40,'1','1','1','1'),(226,48,41,'1','1','1','1'),(227,48,42,'1','1','1','1'),(228,48,43,'1','1','1','1'),(229,48,44,'1','1','1','1'),(230,48,45,'1','1','1','1'),(231,48,46,'1','1','1','1'),(232,48,47,'1','1','1','1'),(233,48,48,'1','1','1','1'),(234,48,49,'1','1','1','1'),(235,48,50,'1','1','1','1'),(236,48,51,'1','1','1','1'),(237,48,52,'1','1','1','1'),(238,48,53,'1','1','1','1'),(239,48,54,'1','1','1','1'),(240,48,55,'1','1','1','1'),(241,48,56,'1','1','1','1'),(242,48,57,'1','1','1','1'),(243,48,58,'1','1','1','1'),(244,48,59,'1','1','1','1'),(245,48,60,'1','1','1','1'),(246,48,61,'1','1','1','1'),(247,48,62,'1','1','1','1'),(248,48,63,'1','1','1','1'),(249,48,64,'1','1','1','1'),(250,48,65,'1','1','1','1'),(251,48,66,'1','1','1','1'),(252,48,67,'1','1','1','1'),(253,48,68,'1','1','1','1'),(254,48,69,'1','1','1','1'),(255,48,70,'1','1','1','1'),(256,48,71,'1','1','1','1'),(257,48,72,'1','1','1','1'),(258,48,73,'1','1','1','1'),(259,48,74,'1','1','1','1'),(260,48,75,'1','1','1','1'),(261,48,76,'1','1','1','1'),(262,48,77,'1','1','1','1'),(263,48,78,'1','1','1','1'),(264,48,79,'1','1','1','1'),(265,48,80,'1','1','1','1'),(266,48,81,'1','1','1','1'),(267,48,82,'1','1','1','1'),(268,48,83,'1','1','1','1'),(269,48,84,'1','1','1','1'),(270,48,85,'1','1','1','1'),(271,48,86,'1','1','1','1'),(272,48,87,'1','1','1','1'),(273,48,88,'1','1','1','1'),(274,48,89,'1','1','1','1'),(275,48,90,'1','1','1','1'),(276,48,91,'1','1','1','1'),(277,48,92,'1','1','1','1'),(280,48,93,'1','1','1','1'),(281,48,94,'1','1','1','1'),(282,48,95,'1','1','1','1'),(283,48,96,'1','1','1','1'),(284,48,97,'1','1','1','1'),(285,48,98,'1','1','1','1'),(286,48,99,'1','1','1','1'),(287,48,100,'1','1','1','1'),(288,48,101,'1','1','1','1'),(289,48,102,'1','1','1','1'),(290,48,103,'1','1','1','1'),(291,48,104,'1','1','1','1'),(292,48,105,'1','1','1','1'),(293,48,106,'1','1','1','1'),(294,48,107,'1','1','1','1'),(295,48,108,'1','1','1','1'),(296,48,109,'1','1','1','1'),(297,48,110,'1','1','1','1'),(298,48,111,'1','1','1','1'),(299,48,112,'1','1','1','1'),(300,48,113,'1','1','1','1'),(301,48,114,'1','1','1','1'),(302,48,115,'1','1','1','1'),(303,48,116,'1','1','1','1'),(304,48,117,'1','1','1','1'),(305,48,118,'1','1','1','1'),(306,48,119,'1','1','1','1'),(307,48,120,'1','1','1','1'),(308,48,121,'1','1','1','1'),(309,48,122,'1','1','1','1'),(310,48,123,'1','1','1','1'),(311,48,124,'1','1','1','1'),(312,48,125,'1','1','1','1'),(313,48,126,'1','1','1','1'),(314,48,127,'1','1','1','1'),(315,48,128,'1','1','1','1'),(316,48,129,'1','1','1','1'),(317,48,130,'1','1','1','1'),(318,48,131,'1','1','1','1'),(319,48,132,'1','1','1','1'),(320,48,133,'1','1','1','1'),(321,48,134,'1','1','1','1'),(322,48,135,'1','1','1','1'),(323,48,136,'1','1','1','1'),(324,48,137,'1','1','1','1'),(325,48,138,'1','1','1','1'),(326,48,139,'1','1','1','1'),(327,48,140,'1','1','1','1'),(328,48,141,'1','1','1','1'),(329,48,142,'1','1','1','1'),(330,48,143,'1','1','1','1'),(331,48,144,'1','1','1','1'),(332,48,145,'1','1','1','1'),(333,48,146,'1','1','1','1'),(334,48,147,'1','1','1','1'),(335,48,148,'1','1','1','1'),(336,48,149,'1','1','1','1'),(337,48,150,'1','1','1','1'),(338,48,151,'1','1','1','1'),(339,48,152,'1','1','1','1'),(340,48,153,'1','1','1','1'),(341,48,154,'1','1','1','1'),(342,48,155,'1','1','1','1'),(343,48,156,'1','1','1','1'),(344,48,157,'1','1','1','1'),(345,48,158,'1','1','1','1'),(346,48,159,'1','1','1','1'),(347,48,160,'1','1','1','1'),(348,48,161,'1','1','1','1'),(349,48,162,'1','1','1','1'),(350,48,163,'1','1','1','1'),(351,48,164,'1','1','1','1'),(352,48,165,'1','1','1','1'),(353,48,166,'1','1','1','1'),(354,48,167,'1','1','1','1'),(355,48,168,'1','1','1','1'),(356,48,169,'1','1','1','1'),(357,48,170,'1','1','1','1'),(358,48,171,'1','1','1','1'),(359,48,172,'1','1','1','1'),(360,48,173,'1','1','1','1'),(361,48,174,'1','1','1','1'),(362,48,175,'1','1','1','1'),(363,48,176,'1','1','1','1'),(364,48,177,'1','1','1','1'),(365,48,178,'1','1','1','1'),(366,48,179,'1','1','1','1'),(367,48,180,'1','1','1','1'),(368,48,181,'1','1','1','1'),(369,48,190,'1','1','1','1'),(370,48,191,'1','1','1','1'),(371,48,192,'1','1','1','1'),(372,48,193,'1','1','1','1'),(373,48,194,'1','1','1','1'),(374,48,195,'1','1','1','1'),(375,48,196,'1','1','1','1'),(376,48,197,'1','1','1','1'),(377,48,198,'1','1','1','1'),(378,48,199,'1','1','1','1'),(379,48,200,'1','1','1','1'),(380,48,201,'1','1','1','1'),(381,48,202,'1','1','1','1'),(382,48,203,'1','1','1','1'),(383,48,204,'1','1','1','1'),(384,48,205,'1','1','1','1'),(385,48,206,'1','1','1','1'),(386,48,207,'1','1','1','1'),(387,48,208,'1','1','1','1'),(388,48,209,'1','1','1','1'),(389,48,210,'1','1','1','1'),(390,48,211,'1','1','1','1'),(391,48,212,'1','1','1','1'),(392,48,213,'1','1','1','1'),(393,48,214,'1','1','1','1'),(394,48,215,'1','1','1','1'),(395,48,216,'1','1','1','1'),(396,48,217,'1','1','1','1'),(397,48,218,'1','1','1','1'),(398,48,219,'1','1','1','1'),(399,48,220,'1','1','1','1'),(400,48,221,'1','1','1','1'),(401,48,222,'1','1','1','1'),(402,48,223,'1','1','1','1'),(403,48,224,'1','1','1','1'),(404,48,225,'1','1','1','1'),(405,49,213,'1','1','1','1'),(406,50,213,'1','1','1','1'),(407,51,213,'1','1','1','1'),(408,51,214,'1','1','1','1'),(409,50,214,'1','1','1','1'),(410,49,214,'1','1','1','1'),(411,49,215,'1','1','1','1'),(412,50,215,'1','1','1','1'),(413,51,215,'1','1','1','1'),(414,51,216,'1','1','1','1'),(415,50,216,'1','1','1','1'),(416,49,216,'1','1','1','1'),(417,49,217,'1','1','1','1'),(418,50,217,'1','1','1','1'),(419,51,217,'1','1','1','1'),(420,51,218,'1','1','1','1'),(421,50,218,'1','1','1','1'),(422,49,218,'1','1','1','1'),(423,49,219,'1','1','1','1'),(424,50,219,'1','1','1','1'),(425,51,219,'1','1','1','1'),(426,49,225,'1','1','1','1'),(427,50,225,'1','1','1','1'),(428,51,225,'1','1','1','1'),(429,51,224,'1','1','1','1'),(430,50,224,'1','1','1','1'),(431,49,224,'1','1','1','1'),(432,49,223,'1','1','1','1'),(433,50,223,'1','1','1','1'),(434,51,222,'1','1','1','1'),(435,50,222,'1','1','1','1'),(436,49,222,'1','1','1','1'),(437,49,221,'1','1','1','1'),(438,49,220,'1','1','1','1'),(439,51,220,'1','1','1','1'),(440,50,220,'1','1','1','1'),(441,51,221,'1','1','1','1'),(442,50,221,'1','1','1','1'),(443,51,223,'1','1','1','1'),(444,49,141,'1','1','1','1'),(445,50,141,'1','1','1','1'),(446,51,141,'1','1','1','1'),(447,51,140,'1','1','1','1'),(448,50,140,'1','1','1','1'),(449,49,140,'1','1','1','1'),(450,49,142,'1','1','1','1'),(451,50,142,'1','1','1','1'),(452,51,142,'1','1','1','1'),(453,49,143,'1','1','1','1'),(454,50,143,'1','1','1','1'),(455,51,143,'1','1','1','1'),(456,50,144,'1','1','1','1'),(457,51,144,'1','1','1','1'),(458,49,144,'1','1','1','1'),(459,49,145,'1','1','1','1'),(460,50,145,'1','1','1','1'),(461,51,146,'1','1','1','1'),(462,51,145,'1','1','1','1'),(463,50,146,'1','1','1','1'),(464,49,146,'1','1','1','1'),(465,79,126,'1','1','1','1'),(466,79,127,'1','1','1','1'),(467,79,128,'1','1','1','1'),(468,79,129,'1','1','1','1'),(469,79,130,'1','1','1','1'),(470,79,131,'1','1','1','1'),(471,79,132,'1','1','1','1'),(472,79,133,'1','1','1','1'),(473,79,134,'1','1','1','1'),(474,79,135,'1','1','1','1'),(475,79,136,'1','1','1','1'),(476,79,137,'1','1','1','1'),(477,79,138,'1','1','1','1'),(478,79,139,'1','1','1','1'),(479,79,140,'1','1','1','1'),(480,79,141,'1','1','1','1'),(481,79,142,'1','1','1','1'),(482,79,143,'1','1','1','1'),(483,79,144,'1','1','1','1'),(484,79,145,'1','1','1','1'),(485,79,146,'1','1','1','1'),(486,79,147,'1','1','1','1'),(487,79,148,'1','1','1','1'),(488,79,149,'1','1','1','1'),(489,79,150,'1','1','1','1'),(490,79,151,'1','1','1','1'),(491,79,152,'1','1','1','1'),(492,79,153,'1','1','1','1'),(493,79,154,'1','1','1','1'),(494,79,155,'1','1','1','1'),(495,79,156,'1','1','1','1'),(496,79,157,'1','1','1','1'),(497,79,159,'1','1','1','1'),(498,79,181,'1','1','1','1'),(499,79,190,'1','1','1','1'),(500,79,212,'1','1','1','1'),(501,79,211,'1','1','1','1'),(502,79,210,'1','1','1','1'),(503,79,209,'1','1','1','1'),(504,79,208,'1','1','1','1'),(505,79,207,'1','1','1','1'),(506,79,206,'1','1','1','1'),(507,79,205,'1','1','1','1'),(508,79,160,'1','1','1','1'),(509,79,161,'1','1','1','1'),(510,79,162,'1','1','1','1'),(511,79,163,'1','1','1','1'),(512,79,164,'1','1','1','1'),(513,79,165,'1','1','1','1'),(514,79,166,'1','1','1','1'),(515,79,167,'1','1','1','1'),(516,79,168,'1','1','1','1'),(517,79,169,'1','1','1','1'),(518,79,170,'1','1','1','1'),(519,79,171,'1','1','1','1'),(520,79,172,'1','1','1','1'),(521,79,173,'1','1','1','1'),(522,79,174,'1','1','1','1'),(523,79,175,'1','1','1','1'),(524,79,176,'1','1','1','1'),(525,79,177,'1','1','1','1'),(526,79,178,'1','1','1','1'),(527,79,179,'1','1','1','1'),(528,79,180,'1','1','1','1'),(529,79,204,'1','1','1','1'),(530,79,203,'1','1','1','1'),(531,79,202,'1','1','1','1'),(532,79,201,'1','1','1','1'),(533,79,200,'1','1','1','1'),(534,79,199,'1','1','1','1'),(535,79,198,'1','1','1','1'),(536,79,197,'1','1','1','1'),(537,79,196,'1','1','1','1'),(538,79,195,'1','1','1','1'),(539,79,194,'1','1','1','1'),(540,79,193,'1','1','1','1'),(541,79,192,'1','1','1','1'),(542,79,191,'1','1','1','1'),(543,79,266,'1','1','1','1'),(544,79,267,'1','1','1','1'),(545,79,268,'1','1','1','1'),(546,79,269,'1','1','1','1'),(547,79,270,'1','1','1','1'),(548,79,271,'1','1','1','1'),(549,79,272,'1','1','1','1'),(550,79,273,'1','1','1','1'),(551,79,274,'1','1','1','1'),(552,79,275,'1','1','1','1'),(553,79,276,'1','1','1','1'),(554,79,277,'1','1','1','1'),(555,79,278,'1','1','1','1'),(556,79,279,'1','1','1','1'),(557,79,280,'1','1','1','1'),(558,79,281,'1','1','1','1'),(559,79,282,'1','1','1','1'),(560,79,283,'1','1','1','1'),(561,79,284,'1','1','1','1'),(562,79,285,'1','1','1','1'),(563,79,286,'1','1','1','1'),(564,79,287,'1','1','1','1'),(565,79,288,'1','1','1','1'),(566,79,289,'1','1','1','1'),(567,79,290,'1','1','1','1'),(568,79,291,'1','1','1','1'),(569,79,292,'1','1','1','1'),(570,79,347,'1','1','1','1'),(571,79,373,'1','1','1','1'),(572,79,372,'1','1','1','1'),(573,79,371,'1','1','1','1'),(574,79,370,'1','1','1','1'),(575,79,369,'1','1','1','1'),(576,79,368,'1','1','1','1'),(577,79,367,'1','1','1','1'),(578,79,366,'1','1','1','1'),(579,79,365,'1','1','1','1'),(580,79,364,'1','1','1','1'),(581,79,363,'1','1','1','1'),(582,79,362,'1','1','1','1'),(583,79,361,'1','1','1','1'),(584,79,360,'1','1','1','1'),(585,79,359,'1','1','1','1'),(586,79,358,'1','1','1','1'),(587,79,357,'1','1','1','1'),(588,79,356,'1','1','1','1'),(589,79,355,'1','1','1','1'),(590,79,354,'1','1','1','1'),(591,79,353,'1','1','1','1'),(592,79,352,'1','1','1','1'),(593,79,351,'1','1','1','1'),(594,79,350,'1','1','1','1'),(595,79,349,'1','1','1','1'),(596,79,348,'1','1','1','1'),(597,79,374,'1','1','1','1'),(598,79,375,'1','1','1','1'),(599,79,376,'1','1','1','1'),(600,79,377,'1','1','1','1'),(601,79,378,'1','1','1','1'),(602,79,379,'1','1','1','1'),(603,79,380,'1','1','1','1'),(604,79,381,'1','1','1','1'),(605,79,382,'1','1','1','1'),(606,79,383,'1','1','1','1'),(607,79,384,'1','1','1','1'),(608,79,385,'1','1','1','1'),(609,79,386,'1','1','1','1'),(610,79,387,'1','1','1','1'),(611,79,388,'1','1','1','1'),(612,79,389,'1','1','1','1'),(613,79,390,'1','1','1','1'),(614,79,391,'1','1','1','1'),(615,79,392,'1','1','1','1'),(616,79,393,'1','1','1','1'),(617,79,394,'1','1','1','1'),(618,79,395,'1','1','1','1'),(619,79,396,'1','1','1','1'),(620,79,397,'1','1','1','1'),(621,79,398,'1','1','1','1'),(622,79,399,'1','1','1','1'),(623,79,400,'1','1','1','1'),(624,79,320,'1','1','1','1'),(625,79,321,'1','1','1','1'),(626,79,346,'1','1','1','1'),(627,79,345,'1','1','1','1'),(628,79,344,'1','1','1','1'),(629,79,343,'1','1','1','1'),(630,79,342,'1','1','1','1'),(631,79,341,'1','1','1','1'),(632,79,340,'1','1','1','1'),(633,79,338,'1','1','1','1'),(634,79,339,'1','1','1','1'),(635,79,337,'1','1','1','1'),(636,79,336,'1','1','1','1'),(637,79,335,'1','1','1','1'),(638,79,334,'1','1','1','1'),(639,79,333,'1','1','1','1'),(640,79,332,'1','1','1','1'),(641,79,331,'1','1','1','1'),(642,79,330,'1','1','1','1'),(643,79,329,'1','1','1','1'),(644,79,328,'1','1','1','1'),(645,79,327,'1','1','1','1'),(646,79,323,'1','1','1','1'),(647,79,325,'1','1','1','1'),(648,79,326,'1','1','1','1'),(649,79,324,'1','1','1','1'),(650,79,322,'1','1','1','1'),(651,79,401,'1','1','1','1'),(652,79,402,'1','1','1','1'),(653,79,403,'1','1','1','1'),(654,79,404,'1','1','1','1'),(655,79,405,'1','1','1','1'),(656,79,406,'1','1','1','1'),(657,79,407,'1','1','1','1'),(658,79,408,'1','1','1','1'),(659,79,409,'1','1','1','1'),(660,79,410,'1','1','1','1'),(661,79,411,'1','1','1','1'),(662,79,412,'1','1','1','1'),(663,79,413,'1','1','1','1'),(664,79,414,'1','1','1','1'),(665,79,415,'1','1','1','1'),(666,79,416,'1','1','1','1'),(667,79,417,'1','1','1','1'),(668,79,418,'1','1','1','1'),(669,79,419,'1','1','1','1'),(670,79,420,'1','1','1','1'),(671,79,421,'1','1','1','1'),(672,79,422,'1','1','1','1'),(673,79,423,'1','1','1','1'),(674,49,424,'1','1','1','1'),(675,50,424,'1','1','1','1'),(676,51,424,'1','1','1','1'),(677,51,425,'1','1','1','1'),(678,50,425,'1','1','1','1'),(679,49,425,'1','1','1','1'),(680,49,426,'1','1','1','1'),(681,50,426,'1','1','1','1'),(682,51,426,'1','1','1','1'),(683,51,427,'1','1','1','1'),(684,50,427,'1','1','1','1'),(685,49,427,'1','1','1','1'),(686,49,428,'1','1','1','1'),(687,50,428,'1','1','1','1'),(688,51,428,'1','1','1','1'),(689,51,429,'1','1','1','1'),(690,50,429,'1','1','1','1'),(691,49,430,'1','1','1','1'),(692,50,430,'1','1','1','1'),(693,51,430,'1','1','1','1'),(694,49,429,'1','1','1','1'),(695,49,431,'1','1','1','1'),(696,50,431,'1','1','1','1'),(697,51,431,'1','1','1','1'),(698,51,432,'1','1','1','1'),(699,50,432,'1','1','1','1'),(700,49,432,'1','1','1','1'),(701,49,433,'1','1','1','1'),(702,49,434,'1','1','1','1'),(703,49,435,'1','1','1','1'),(704,49,436,'1','1','1','1'),(705,49,437,'1','1','1','1'),(706,50,437,'1','1','1','1'),(707,50,436,'1','1','1','1'),(708,50,435,'1','1','1','1'),(709,50,434,'1','1','1','1'),(710,50,433,'1','1','1','1'),(711,51,433,'1','1','1','1'),(712,51,434,'1','1','1','1'),(713,51,435,'1','1','1','1'),(714,51,436,'1','1','1','1'),(715,51,437,'1','1','1','1'),(716,51,438,'1','1','1','1'),(717,50,438,'1','1','1','1'),(718,49,438,'1','1','1','1'),(719,49,439,'1','1','1','1'),(720,49,440,'1','1','1','1'),(721,49,441,'1','1','1','1'),(722,49,442,'1','1','1','1'),(723,49,443,'1','1','1','1'),(724,50,443,'1','1','1','1'),(725,50,442,'1','1','1','1'),(726,50,441,'1','1','1','1'),(727,50,440,'1','1','1','1'),(728,50,439,'1','1','1','1'),(729,51,439,'1','1','1','1'),(730,51,440,'1','1','1','1'),(731,51,441,'1','1','1','1'),(732,51,442,'1','1','1','1'),(733,51,443,'1','1','1','1'),(734,49,444,'1','1','1','1'),(735,50,444,'1','1','1','1'),(736,51,444,'1','1','1','1'),(737,51,445,'1','1','1','1'),(738,51,446,'1','1','1','1'),(739,51,447,'1','1','1','1'),(740,51,448,'1','1','1','1'),(741,51,449,'1','1','1','1'),(742,51,450,'1','1','1','1'),(743,50,450,'1','1','1','1'),(744,49,450,'1','1','1','1'),(745,49,449,'1','1','1','1'),(746,49,448,'1','1','1','1'),(747,49,447,'1','1','1','1'),(748,49,446,'1','1','1','1'),(749,49,445,'1','1','1','1'),(750,50,445,'1','1','1','1'),(751,50,446,'1','1','1','1'),(752,50,447,'1','1','1','1'),(753,50,448,'1','1','1','1'),(754,50,449,'1','1','1','1');
/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autoevaluationanswers`
--

DROP TABLE IF EXISTS `autoevaluationanswers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autoevaluationanswers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `autoevaluation_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`question_id`),
  KEY `fk_table1_AutoEvaluacion1_idx` (`id`),
  KEY `fk_table1_Pregunta1_idx` (`question_id`),
  KEY `fk_table1_Respuesta1_idx` (`answer_id`),
  KEY `question_id` (`question_id`),
  KEY `answer_id` (`answer_id`),
  KEY `autoevaluation_id` (`autoevaluation_id`)
) ENGINE=MyISAM AUTO_INCREMENT=165 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autoevaluationanswers`
--

LOCK TABLES `autoevaluationanswers` WRITE;
/*!40000 ALTER TABLE `autoevaluationanswers` DISABLE KEYS */;
INSERT INTO `autoevaluationanswers` VALUES (164,105,4,2),(163,105,3,1),(162,102,4,2),(161,102,3,3),(160,101,4,2),(159,101,3,1),(158,100,4,2),(157,100,3,1),(156,99,4,2),(155,99,3,3),(154,98,4,2),(153,98,3,3),(152,97,4,2),(151,97,3,3);
/*!40000 ALTER TABLE `autoevaluationanswers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autoevaluations`
--

DROP TABLE IF EXISTS `autoevaluations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autoevaluations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `questionnaire_id` int(11) NOT NULL,
  `total_points` int(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `corporation_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`questionnaire_id`),
  KEY `fk_AutoEvaluacion_Usuario1_idx` (`user_id`),
  KEY `fk_AutoEvaluacion_Cuestionario1_idx` (`questionnaire_id`)
) ENGINE=MyISAM AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autoevaluations`
--

LOCK TABLES `autoevaluations` WRITE;
/*!40000 ALTER TABLE `autoevaluations` DISABLE KEYS */;
INSERT INTO `autoevaluations` VALUES (105,'2015-10-20 04:30:32',17,5,19,11,7),(104,'2015-10-20 04:30:14',17,5,0,11,7);
/*!40000 ALTER TABLE `autoevaluations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `order` int(2) NOT NULL,
  `module_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_categories_modules_idx` (`module_id`),
  CONSTRAINT `fk_categories_modules` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (10,'Corporaciones',0,13,'2015-10-14 15:48:38','2015-10-17 11:04:31'),(11,'Empresas',1,13,'2015-10-14 15:48:51','2015-10-17 11:04:42'),(12,'Usuarios',2,13,'2015-10-14 15:49:02','2015-10-17 11:04:51'),(14,'Grupos',0,11,'2015-10-14 15:49:53','2015-10-14 15:49:53'),(15,'Modulos',1,11,'2015-10-14 15:50:07','2015-10-14 15:50:07'),(16,'Categorias',2,11,'2015-10-14 15:50:31','2015-10-14 15:50:31'),(17,'Funciones',3,11,'2015-10-14 15:50:48','2015-10-14 15:50:48'),(18,'Permisos',5,11,'2015-10-14 15:51:07','2015-10-14 15:51:07'),(19,'Cuestionarios',0,15,'2015-10-16 11:08:38','2015-10-17 11:05:02'),(23,'Asignar Cuestionarios a Empresas',1,15,'2015-10-17 15:31:04','2015-10-17 15:31:04'),(24,'Gestion EstratÃ©gica',0,16,'2015-10-19 10:27:42','2015-10-19 10:27:42'),(25,'GestiÃ³n Operativa',0,18,'2015-10-19 10:28:18','2015-10-19 10:28:18'),(26,'Memoria y Cuenta',0,19,'2015-10-19 10:28:35','2015-10-19 10:28:35'),(27,'PlanificaciÃ³n TÃ¡ctica',0,17,'2015-10-19 10:28:50','2015-10-19 10:28:50'),(28,'Instrumentos',0,20,'2015-10-19 10:42:26','2015-10-19 10:42:26'),(29,'Realizar una autoevaluaciÃ³n',0,20,'2015-10-19 23:26:41','2015-10-19 23:26:41');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `description` text,
  `phone` varchar(255) DEFAULT NULL,
  `address` text,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `corporation_id` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `corporation_id` (`corporation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (11,'Musmanni','files/logos/172554405ca27e59a8c042ce0fdc804ba1a830.png','PanaderÃ­a y pastelerÃ­a','66665555','La direcciÃ³n','2015-10-17 17:25:54','2015-10-14 19:38:08',7,1),(13,'ffffffffffffff','','','','','2015-10-16 22:11:36','2015-10-16 22:10:03',NULL,0),(14,'Florida','files/logos/145324550e106237930dbc966c24ff6abbfef7.jpg','1','123','1','2015-10-19 14:53:24','2015-10-19 14:53:24',7,1);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companyquestionnaires`
--

DROP TABLE IF EXISTS `companyquestionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companyquestionnaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `questionnaire_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `questionnaire_id` (`questionnaire_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companyquestionnaires`
--

LOCK TABLES `companyquestionnaires` WRITE;
/*!40000 ALTER TABLE `companyquestionnaires` DISABLE KEYS */;
INSERT INTO `companyquestionnaires` VALUES (1,11,5,'2015-10-17 15:20:26','2015-10-17 15:20:26'),(3,11,7,'2015-10-17 15:40:51','2015-10-17 15:40:51'),(4,14,9,'2015-10-19 15:08:24','2015-10-19 15:08:24');
/*!40000 ALTER TABLE `companyquestionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corporations`
--

DROP TABLE IF EXISTS `corporations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corporations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `description` text,
  `phone` varchar(255) DEFAULT NULL,
  `address` text,
  `modified` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corporations`
--

LOCK TABLES `corporations` WRITE;
/*!40000 ALTER TABLE `corporations` DISABLE KEYS */;
INSERT INTO `corporations` VALUES (7,'Florida Ice & Farm Company','files/logos/172724c9c2bce82b74978a3e87079f2ce6e52b.png','Es una empresa de bebidas y alimentos','22224444','San JosÃ©','2015-10-17 17:27:24','2015-10-14 19:08:23',1),(9,'asdf',NULL,'','','','2015-10-16 22:03:01','2015-10-16 22:03:01',0);
/*!40000 ALTER TABLE `corporations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groupactions`
--

DROP TABLE IF EXISTS `groupactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groupactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_group_action` (`group_id`,`action_id`),
  KEY `fk_groupactions_groups1_idx` (`group_id`),
  KEY `fk_groupactions_actions1_idx` (`action_id`),
  CONSTRAINT `fk_groupactions_actions1` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_groupactions_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupactions`
--

LOCK TABLES `groupactions` WRITE;
/*!40000 ALTER TABLE `groupactions` DISABLE KEYS */;
INSERT INTO `groupactions` VALUES (19,5,30,'2015-10-14 15:57:08','2015-10-14 15:57:08'),(20,5,31,'2015-10-14 15:57:13','2015-10-14 15:57:13'),(21,5,32,'2015-10-14 15:57:27','2015-10-14 15:57:27'),(22,5,33,'2015-10-14 15:57:33','2015-10-14 15:57:33'),(23,5,34,'2015-10-14 15:57:39','2015-10-14 15:57:39'),(24,5,35,'2015-10-14 15:57:50','2015-10-14 15:57:50'),(25,5,36,'2015-10-14 15:57:55','2015-10-14 15:57:55'),(26,5,37,'2015-10-14 15:58:01','2015-10-14 15:58:01'),(27,5,38,'2015-10-14 15:58:09','2015-10-14 15:58:09'),(28,5,39,'2015-10-14 15:58:15','2015-10-14 15:58:15'),(35,9,31,'2015-10-15 21:50:35','2015-10-15 21:50:35'),(36,9,32,'2015-10-15 21:50:41','2015-10-15 21:50:41'),(37,9,38,'2015-10-15 21:50:54','2015-10-15 21:50:54'),(38,5,40,'2015-10-16 11:09:59','2015-10-16 11:09:59'),(39,9,40,'2015-10-16 11:10:06','2015-10-16 11:10:06'),(40,5,41,'2015-10-16 14:00:17','2015-10-16 14:00:17'),(41,9,41,'2015-10-16 14:00:24','2015-10-16 14:00:24'),(42,5,42,'2015-10-16 19:44:50','2015-10-16 19:44:50'),(43,9,42,'2015-10-16 19:44:56','2015-10-16 19:44:56'),(44,5,43,'2015-10-16 21:03:22','2015-10-16 21:03:22'),(45,9,43,'2015-10-16 21:03:28','2015-10-16 21:03:28'),(49,5,44,'2015-10-17 15:33:04','2015-10-17 15:33:04'),(50,9,44,'2015-10-17 15:33:09','2015-10-17 15:33:09'),(52,6,46,'2015-10-19 10:31:15','2015-10-19 10:31:15'),(53,6,45,'2015-10-19 10:32:11','2015-10-19 10:32:11'),(55,6,47,'2015-10-19 10:32:25','2015-10-19 10:32:25'),(56,6,48,'2015-10-19 10:32:31','2015-10-19 10:32:31'),(57,7,45,'2015-10-19 10:32:39','2015-10-19 10:32:39'),(58,7,46,'2015-10-19 10:32:44','2015-10-19 10:32:44'),(59,7,47,'2015-10-19 10:32:50','2015-10-19 10:32:50'),(60,7,48,'2015-10-19 10:32:56','2015-10-19 10:32:56'),(61,8,45,'2015-10-19 10:33:03','2015-10-19 10:33:03'),(62,8,46,'2015-10-19 10:33:08','2015-10-19 10:33:08'),(63,8,47,'2015-10-19 10:33:17','2015-10-19 10:33:17'),(64,8,48,'2015-10-19 10:33:24','2015-10-19 10:33:24'),(65,6,49,'2015-10-19 10:44:09','2015-10-19 10:44:09'),(66,7,49,'2015-10-19 10:44:15','2015-10-19 10:44:15'),(67,8,49,'2015-10-19 10:44:22','2015-10-19 10:44:22'),(68,6,50,'2015-10-19 23:24:50','2015-10-19 23:24:50'),(69,7,50,'2015-10-19 23:24:57','2015-10-19 23:24:57'),(70,8,50,'2015-10-19 23:25:04','2015-10-19 23:25:04');
/*!40000 ALTER TABLE `groupactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (5,'Root','2015-10-14 15:46:28','2015-10-19 19:14:30'),(6,'Administrador Corporativo','2015-10-14 15:46:38','2015-10-14 15:46:38'),(7,'Administrador','2015-10-14 15:46:48','2015-10-14 15:46:48'),(8,'Perfil Usuario Normal','2015-10-14 15:46:56','2015-10-14 15:46:56'),(9,'Administrador CSS','2015-10-15 21:50:04','2015-10-15 21:50:04');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `i18n`
--

DROP TABLE IF EXISTS `i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `i18n` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`foreign_key`,`field`,`locale`,`model`),
  KEY `locale` (`locale`),
  KEY `model` (`model`),
  KEY `row_id` (`foreign_key`),
  KEY `field` (`field`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `i18n`
--

LOCK TABLES `i18n` WRITE;
/*!40000 ALTER TABLE `i18n` DISABLE KEYS */;
INSERT INTO `i18n` VALUES (9,'esp','Group',5,'name','Root'),(10,'eng','Group',5,'name','Root'),(11,'esp','Group',6,'name','Administrador Corporativo'),(12,'eng','Group',6,'name','Corporation Administrator'),(13,'esp','Group',7,'name','Administrador'),(14,'eng','Group',7,'name','Administrator'),(15,'esp','Group',8,'name','Perfil Usuario Normal'),(16,'eng','Group',8,'name','Normal User Profile'),(19,'esp','Module',11,'name','Sistema Base'),(20,'eng','Module',11,'name','Base System'),(23,'esp','Module',13,'name','Empresas'),(24,'eng','Module',13,'name','Companies'),(27,'esp','Category',10,'name','Corporaciones'),(28,'eng','Category',10,'name','Corporations '),(29,'esp','Category',11,'name','Empresas'),(30,'eng','Category',11,'name','Companies'),(31,'esp','Category',12,'name','Usuarios'),(32,'eng','Category',12,'name','User '),(35,'esp','Category',14,'name','Grupos'),(36,'eng','Category',14,'name','Groups'),(37,'esp','Category',15,'name','Modulos'),(38,'eng','Category',15,'name','Modules'),(39,'esp','Category',16,'name','Categorias'),(40,'eng','Category',16,'name','Categories'),(41,'esp','Category',17,'name','Funciones'),(42,'eng','Category',17,'name','Functions'),(43,'esp','Category',18,'name','Permisos'),(44,'eng','Category',18,'name','Permission'),(47,'esp','Action',30,'name','CategorÃ­as : Listado'),(48,'eng','Action',30,'name','Categories : List'),(49,'esp','Action',31,'name','Corporaciones : Listado'),(50,'eng','Action',31,'name','Corporations : List'),(51,'esp','Action',32,'name','Empresas : Listado'),(52,'eng','Action',32,'name','Companies : List'),(53,'esp','Action',33,'name','Funciones : Listado'),(54,'eng','Action',33,'name','Functions : List'),(55,'esp','Action',34,'name','Grupos : Listado'),(56,'eng','Action',34,'name','Groups : List'),(57,'esp','Action',35,'name','Modulos : Listado'),(58,'eng','Action',35,'name','Modules : List'),(59,'esp','Action',36,'name','Permisos : ACL'),(60,'eng','Action',36,'name','Permissions : ACL'),(61,'esp','Action',37,'name','Permisos : Listado'),(62,'eng','Action',37,'name','Permissions : List'),(63,'esp','Action',38,'name','Usuarios : Listado'),(64,'eng','Action',38,'name','Users : List'),(65,'esp','Action',39,'name','Usuarios : Sync'),(66,'eng','Action',39,'name','Users : Sync'),(67,'esp','Corporation',7,'description','Es una empresa de bebidas y alimentos'),(68,'eng','Corporation',7,'description','A food and drinks company'),(69,'esp','Corporation',7,'address','San JosÃ©'),(70,'eng','Corporation',7,'address','Saint Joseph.'),(71,'esp','Company',11,'description','PanaderÃ­a y pastelerÃ­a'),(72,'eng','Company',11,'description','Bakery '),(73,'esp','Company',11,'address','La direcciÃ³n'),(74,'eng','Company',11,'address','The address'),(83,'esp','Group',9,'name','Administrador CSS'),(84,'eng','Group',9,'name','CSS Administrator'),(85,'esp','Questionnaire',0,'title','1'),(86,'eng','Questionnaire',0,'title','2'),(87,'esp','Questionnaire',0,'description','3'),(88,'eng','Questionnaire',0,'description','4'),(89,'esp','Questionnaire',0,'acronym','5'),(90,'eng','Questionnaire',0,'acronym','6'),(109,'esp','Questionnaire',5,'title','Cuestionario 1'),(110,'eng','Questionnaire',5,'title','Questionnaire 1'),(111,'esp','Questionnaire',5,'description','9'),(112,'eng','Questionnaire',5,'description','9'),(113,'esp','Questionnaire',5,'acronym','9'),(114,'eng','Questionnaire',5,'acronym','9'),(121,'esp','Module',15,'name','Instrumentos'),(122,'eng','Module',15,'name','Instruments'),(123,'esp','Category',19,'name','Cuestionarios'),(124,'eng','Category',19,'name','Questionnaires'),(125,'esp','Action',40,'name','Administrar Cuestionarios'),(126,'eng','Action',40,'name','Questionnaires Management'),(127,'esp','Section',0,'name','SecciÃ³n 1'),(128,'eng','Section',0,'name','Section 1'),(129,'esp','Action',41,'name','Administrar Secciones'),(130,'eng','Action',41,'name','Sections Management'),(131,'esp','Section',2,'name','SecciÃ³n dos'),(132,'eng','Section',2,'name','Section two'),(133,'esp','Section',1,'name','SecciÃ³n 1'),(134,'eng','Section',1,'name','Section 1'),(137,'esp','Questionnaire',7,'title','Cuestionario 2'),(138,'eng','Questionnaire',7,'title','two'),(139,'esp','Questionnaire',7,'description','3'),(140,'eng','Questionnaire',7,'description','4'),(141,'esp','Questionnaire',7,'acronym','5'),(142,'eng','Questionnaire',7,'acronym','6'),(143,'esp','Section',4,'name','Otra secciÃ³n'),(144,'eng','Section',4,'name','Another section'),(147,'esp','Action',42,'name','Administrar Preguntas'),(148,'eng','Action',42,'name','Questions Management'),(151,'esp','Question',3,'name','Â¿asdfasdf asdf asdf asdf asdf asdf asdf asdf ?'),(152,'eng','Question',3,'name','qwerqwe rqwer qwer qwer qwer qwerqwer?'),(153,'esp','Question',4,'name','pregunta en esp'),(154,'eng','Question',4,'name','question in english'),(155,'esp','Question',5,'name','zxcvzxcv'),(156,'eng','Question',5,'name','asdfasdf'),(157,'esp','Answer',1,'name','sqwe rqwerqewr qwer '),(158,'eng','Answer',1,'name','21413241234 12341234'),(159,'esp','Answer',1,'feedback_title','3'),(160,'eng','Answer',1,'feedback_title','4'),(161,'esp','Answer',1,'feedback','<div style=\"text-align: center;\"><span style=\"font-weight: bold; font-size: 36px;\">5</span></div>'),(162,'eng','Answer',1,'feedback','<div style=\"text-align: right;\"><span style=\"font-family: Comic Sans MS;\"><span style=\"color: rgb(255, 255, 255);\"><span style=\"background-color: rgb(0, 255, 255); font-size: 36px;\">6</span></span></span></div>'),(163,'esp','Action',43,'name','Administrar Respuestas'),(164,'eng','Action',43,'name','Answers Management'),(165,'esp','Corporation',9,'description',''),(166,'eng','Corporation',9,'description',''),(167,'esp','Corporation',9,'address',''),(168,'eng','Corporation',9,'address',''),(169,'esp','Company',13,'description',''),(170,'eng','Company',13,'description',''),(171,'esp','Company',13,'address',''),(172,'eng','Company',13,'address',''),(179,'esp','Section',5,'name','a'),(180,'eng','Section',5,'name','b'),(181,'esp','Section',6,'name','1'),(182,'eng','Section',6,'name','2'),(183,'esp','Question',6,'name','1'),(184,'eng','Question',6,'name','2'),(185,'esp','Answer',2,'name','1'),(186,'eng','Answer',2,'name','2'),(187,'esp','Answer',2,'feedback_title','La retro'),(188,'eng','Answer',2,'feedback_title','The feedback'),(189,'esp','Answer',2,'feedback','RetroalimentaciÃ³n en espaÃ±ol<br>'),(190,'eng','Answer',2,'feedback','<h1>Feedback in english<br></h1>'),(191,'esp','Answer',3,'name','1'),(192,'eng','Answer',3,'name','2'),(193,'esp','Answer',3,'feedback_title','3'),(194,'eng','Answer',3,'feedback_title','4'),(195,'esp','Answer',3,'feedback','<h1>5</h1><p>asdfasdf</p><p><img style=\"width: 25%;\" alt=\"http://www.bsntest.com/files/blocks/111006d69ff1c301b70e270c450a31e435ce91.jpg\" src=\"http://www.bsntest.com/files/blocks/111006d69ff1c301b70e270c450a31e435ce91.jpg\"></p>'),(196,'eng','Answer',3,'feedback','<pre>6</pre><h1>asdfasdfasdf asdfadsf</h1><h2>qwerqwer qwer <span style=\"background-color: yellow;\"><span style=\"font-weight: bold;\">qewr </span></span>qwer</h2><h3>zXCvzxcvzxcvzxcv<br></h3>'),(203,'esp','Questionnaire',9,'title','1'),(204,'eng','Questionnaire',9,'title','2'),(205,'esp','Questionnaire',9,'description',''),(206,'eng','Questionnaire',9,'description',''),(207,'esp','Questionnaire',9,'acronym',''),(208,'eng','Questionnaire',9,'acronym',''),(209,'esp','Category',23,'name','Asignar Cuestionarios a Empresas'),(210,'eng','Category',23,'name','Assign Questionnaires to Companies'),(211,'esp','Action',44,'name','Asignar Cuestionarios a Empresas'),(212,'eng','Action',44,'name','Assign Questionnaires to Companies'),(213,'esp','Module',16,'name','GestiÃ³n EstratÃ©gica'),(214,'eng','Module',16,'name','Strategic Management'),(215,'esp','Module',17,'name','PlanificaciÃ³n TÃ¡ctica'),(216,'eng','Module',17,'name','Tactical Planning'),(217,'esp','Module',18,'name','GestiÃ³n Operativa'),(218,'eng','Module',18,'name','Operational Management'),(219,'esp','Module',19,'name','Memoria y Cuenta'),(220,'eng','Module',19,'name','Report and Accounts'),(221,'esp','Category',24,'name','Gestion EstratÃ©gica'),(222,'eng','Category',24,'name','Strategic Management'),(223,'esp','Category',25,'name','GestiÃ³n Operativa'),(224,'eng','Category',25,'name','Operational Management'),(225,'esp','Category',26,'name','Memoria y Cuenta'),(226,'eng','Category',26,'name','Reports and Accounts'),(227,'esp','Category',27,'name','PlanificaciÃ³n TÃ¡ctica'),(228,'eng','Category',27,'name','Tactical Planning'),(229,'esp','Action',45,'name','GestiÃ³n EstratÃ©gica'),(230,'eng','Action',45,'name','Strategic Management'),(231,'esp','Action',46,'name','PlanificaciÃ³n TÃ¡ctica'),(232,'eng','Action',46,'name','Tactical Planning'),(233,'esp','Action',47,'name','GestiÃ³n Operativa'),(234,'eng','Action',47,'name','Operational Management'),(235,'esp','Action',48,'name','Memoria y Cuenta'),(236,'eng','Action',48,'name','Reports and Accounts'),(237,'esp','Module',20,'name','Instrumentos'),(238,'eng','Module',20,'name','Instruments'),(239,'esp','Category',28,'name','Instrumentos'),(240,'eng','Category',28,'name','Instruments'),(241,'esp','Action',49,'name','Instrumentos'),(242,'eng','Action',49,'name','Instruments'),(243,'esp','Company',14,'description','1'),(244,'eng','Company',14,'description','1'),(245,'esp','Company',14,'address','1'),(246,'eng','Company',14,'address','1'),(247,'esp','Action',50,'name','Realizar una AutoevaluaciÃ³n'),(248,'eng','Action',50,'name','Conduct a Self-Assessment'),(249,'esp','Category',29,'name','Realizar una autoevaluaciÃ³n'),(250,'eng','Category',29,'name','Conduct a Self-Assessment');
/*!40000 ALTER TABLE `i18n` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `masters`
--

DROP TABLE IF EXISTS `masters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masters`
--

LOCK TABLES `masters` WRITE;
/*!40000 ALTER TABLE `masters` DISABLE KEYS */;
/*!40000 ALTER TABLE `masters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `order` int(2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (11,'Sistema Base','icon-check',1,'2015-10-14 15:47:35','2015-10-15 18:43:52'),(13,'Empresas','icon-leaf',4,'2015-10-14 15:47:59','2015-10-17 10:52:01'),(15,'Instrumentos','icon-eye-open',7,'2015-10-16 11:08:08','2015-10-16 11:11:47'),(16,'GestiÃ³n EstratÃ©gica','icon-leaf',1,'2015-10-19 10:23:49','2015-10-19 10:23:49'),(17,'PlanificaciÃ³n TÃ¡ctica','icon-screenshot',2,'2015-10-19 10:24:57','2015-10-19 10:24:57'),(18,'GestiÃ³n Operativa','icon-dashboard',3,'2015-10-19 10:25:31','2015-10-19 10:41:23'),(19,'Memoria y Cuenta','icon-book',4,'2015-10-19 10:26:49','2015-10-19 10:41:30'),(20,'Instrumentos','icon-eye-open',5,'2015-10-19 10:41:57','2015-10-19 10:43:16');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaires`
--

DROP TABLE IF EXISTS `questionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `active` tinyint(1) DEFAULT NULL,
  `acronym` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaires`
--

LOCK TABLES `questionnaires` WRITE;
/*!40000 ALTER TABLE `questionnaires` DISABLE KEYS */;
INSERT INTO `questionnaires` VALUES (7,'Cuestionario 2','3',1,'5','',''),(5,'Cuestionario 1','9',1,'9','files/logos/133027550e106237930dbc966c24ff6abbfef7.jpg',''),(9,'1','',0,'','files/logos/133201550e106237930dbc966c24ff6abbfef7.jpg','');
/*!40000 ALTER TABLE `questionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `section_id` int(11) NOT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `section_id` (`section_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
INSERT INTO `questions` VALUES (3,'Â¿asdfasdf asdf asdf asdf asdf asdf asdf asdf ?',2,0),(4,'pregunta en esp',1,2),(5,'zxcvzxcv',4,0),(6,'1',1,0);
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `questionnaire_id` int(11) NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `questionnaire_id` (`questionnaire_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES (1,'SecciÃ³n 1',5,'files/logos/141718895603f5c478c808c6a4dba05c9f4a72.png','',1),(2,'SecciÃ³n dos',5,'','',NULL),(4,'Otra secciÃ³n',7,'','',NULL),(5,'a',5,'','',NULL),(6,'1',7,'','',NULL);
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `corporation_id` int(11) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` char(40) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (14,'Lisandro','MartÃ­nez','lisandrom@gmail.com',NULL,NULL,'lisandrom','a56547131290384e62cd0937e9a1281c3e10d824',5,'2015-10-14 16:22:29','2015-10-15 21:47:22'),(17,'Pedro','Perez','pedrop@red111.net',11,7,'pedrop','253e6edb8139392550fff418bac6265d0d2cc934',6,'2015-10-14 19:49:27','2015-10-18 10:32:00'),(18,'MarÃ­a','Perez','mariap@red111.net',11,NULL,'mariap','253e6edb8139392550fff418bac6265d0d2cc934',7,'2015-10-14 19:49:50','2015-10-15 21:48:46'),(19,'Daniela','Prieto','danielap@siderysbsn.com',NULL,NULL,'danielap','253e6edb8139392550fff418bac6265d0d2cc934',9,'2015-10-15 21:58:04','2015-10-15 21:58:04'),(20,'Juan','Fernandez','juanf@siderysbsn.com',NULL,NULL,'juanf','253e6edb8139392550fff418bac6265d0d2cc934',9,'2015-10-15 21:59:22','2015-10-15 21:59:22');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `values`
--

DROP TABLE IF EXISTS `values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `optional` varchar(255) NOT NULL,
  `master_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_values_masters1_idx` (`master_id`),
  CONSTRAINT `fk_values_masters1` FOREIGN KEY (`master_id`) REFERENCES `masters` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `values`
--

LOCK TABLES `values` WRITE;
/*!40000 ALTER TABLE `values` DISABLE KEYS */;
/*!40000 ALTER TABLE `values` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-19 23:39:42
