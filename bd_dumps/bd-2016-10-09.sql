ALTER TABLE `companies`
  ADD COLUMN `esp` int(11) NULL DEFAULT NULL;

ALTER TABLE `companies`
  ADD COLUMN `eng` int(11) NULL DEFAULT NULL;

update companies set eng=1, esp=1;
