-- MySQL dump 10.13  Distrib 5.5.42-37.1, for Linux (x86_64)
--
-- Host: localhost    Database: siriarte_css2
-- ------------------------------------------------------
-- Server version	5.5.42-37.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `acos`
--

DROP TABLE IF EXISTS `acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_acos_lft_rght` (`lft`,`rght`),
  KEY `idx_acos_alias` (`alias`),
  KEY `idx_acos_model_foreign_key` (`model`,`foreign_key`)
) ENGINE=InnoDB AUTO_INCREMENT=266 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acos`
--

LOCK TABLES `acos` WRITE;
/*!40000 ALTER TABLE `acos` DISABLE KEYS */;
INSERT INTO `acos` VALUES (1,NULL,NULL,NULL,'controllers',1,514),(2,1,NULL,NULL,'Actions',2,55),(3,2,NULL,NULL,'paramFilters',3,4),(4,2,NULL,NULL,'get_index',5,6),(5,2,NULL,NULL,'admin_index',7,8),(6,2,NULL,NULL,'post_add',9,10),(7,2,NULL,NULL,'get_add',11,12),(8,2,NULL,NULL,'admin_add',13,14),(9,2,NULL,NULL,'get_edit',15,16),(10,2,NULL,NULL,'post_edit',17,18),(11,2,NULL,NULL,'admin_edit',19,20),(12,2,NULL,NULL,'admin_deletemulti',21,22),(13,2,NULL,NULL,'admin_delete',23,24),(14,2,NULL,NULL,'recordsforpage',25,26),(15,2,NULL,NULL,'filterConfig',27,28),(16,2,NULL,NULL,'isRoot',29,30),(17,2,NULL,NULL,'setSidebarMenu',31,32),(18,2,NULL,NULL,'setHeaderMenu',33,34),(19,2,NULL,NULL,'is_Authorizate',35,36),(20,2,NULL,NULL,'getMethod',37,38),(21,2,NULL,NULL,'ajaxVariablesInit',39,40),(22,2,NULL,NULL,'installBase',41,42),(23,2,NULL,NULL,'syncACL',43,44),(24,2,NULL,NULL,'install_acl',45,46),(25,1,NULL,NULL,'Categories',56,109),(26,25,NULL,NULL,'paramFilters',57,58),(27,25,NULL,NULL,'get_index',59,60),(28,25,NULL,NULL,'admin_index',61,62),(29,25,NULL,NULL,'post_add',63,64),(30,25,NULL,NULL,'get_add',65,66),(31,25,NULL,NULL,'admin_add',67,68),(32,25,NULL,NULL,'get_edit',69,70),(33,25,NULL,NULL,'post_edit',71,72),(34,25,NULL,NULL,'admin_edit',73,74),(35,25,NULL,NULL,'admin_delete',75,76),(36,25,NULL,NULL,'admin_deletemulti',77,78),(37,25,NULL,NULL,'recordsforpage',79,80),(38,25,NULL,NULL,'filterConfig',81,82),(39,25,NULL,NULL,'isRoot',83,84),(40,25,NULL,NULL,'setSidebarMenu',85,86),(41,25,NULL,NULL,'setHeaderMenu',87,88),(42,25,NULL,NULL,'is_Authorizate',89,90),(43,25,NULL,NULL,'getMethod',91,92),(44,25,NULL,NULL,'ajaxVariablesInit',93,94),(45,25,NULL,NULL,'installBase',95,96),(46,25,NULL,NULL,'syncACL',97,98),(47,25,NULL,NULL,'install_acl',99,100),(48,1,NULL,NULL,'Groupactions',110,157),(49,48,NULL,NULL,'paramFilters',111,112),(50,48,NULL,NULL,'get_index',113,114),(51,48,NULL,NULL,'admin_index',115,116),(52,48,NULL,NULL,'post_add',117,118),(53,48,NULL,NULL,'admin_add',119,120),(54,48,NULL,NULL,'admin_delete',121,122),(55,48,NULL,NULL,'admin_deletemulti',123,124),(56,48,NULL,NULL,'admin_acl',125,126),(57,48,NULL,NULL,'recordsforpage',127,128),(58,48,NULL,NULL,'filterConfig',129,130),(59,48,NULL,NULL,'isRoot',131,132),(60,48,NULL,NULL,'setSidebarMenu',133,134),(61,48,NULL,NULL,'setHeaderMenu',135,136),(62,48,NULL,NULL,'is_Authorizate',137,138),(63,48,NULL,NULL,'getMethod',139,140),(64,48,NULL,NULL,'ajaxVariablesInit',141,142),(65,48,NULL,NULL,'installBase',143,144),(66,48,NULL,NULL,'syncACL',145,146),(67,48,NULL,NULL,'install_acl',147,148),(68,1,NULL,NULL,'Groups',158,211),(69,68,NULL,NULL,'paramFilters',159,160),(70,68,NULL,NULL,'get_index',161,162),(71,68,NULL,NULL,'admin_index',163,164),(72,68,NULL,NULL,'admin_delete',165,166),(73,68,NULL,NULL,'get_edit',167,168),(74,68,NULL,NULL,'post_edit',169,170),(75,68,NULL,NULL,'admin_edit',171,172),(76,68,NULL,NULL,'post_add',173,174),(77,68,NULL,NULL,'get_add',175,176),(78,68,NULL,NULL,'admin_add',177,178),(79,68,NULL,NULL,'admin_deletemulti',179,180),(80,68,NULL,NULL,'recordsforpage',181,182),(81,68,NULL,NULL,'filterConfig',183,184),(82,68,NULL,NULL,'isRoot',185,186),(83,68,NULL,NULL,'setSidebarMenu',187,188),(84,68,NULL,NULL,'setHeaderMenu',189,190),(85,68,NULL,NULL,'is_Authorizate',191,192),(86,68,NULL,NULL,'getMethod',193,194),(87,68,NULL,NULL,'ajaxVariablesInit',195,196),(88,68,NULL,NULL,'installBase',197,198),(89,68,NULL,NULL,'syncACL',199,200),(90,68,NULL,NULL,'install_acl',201,202),(91,1,NULL,NULL,'Modules',212,263),(92,91,NULL,NULL,'paramFilters',213,214),(93,91,NULL,NULL,'get_index',215,216),(94,91,NULL,NULL,'admin_index',217,218),(95,91,NULL,NULL,'post_add',219,220),(96,91,NULL,NULL,'admin_add',221,222),(97,91,NULL,NULL,'get_edit',223,224),(98,91,NULL,NULL,'post_edit',225,226),(99,91,NULL,NULL,'admin_edit',227,228),(100,91,NULL,NULL,'admin_delete',229,230),(101,91,NULL,NULL,'admin_deletemulti',231,232),(102,91,NULL,NULL,'recordsforpage',233,234),(103,91,NULL,NULL,'filterConfig',235,236),(104,91,NULL,NULL,'isRoot',237,238),(105,91,NULL,NULL,'setSidebarMenu',239,240),(106,91,NULL,NULL,'setHeaderMenu',241,242),(107,91,NULL,NULL,'is_Authorizate',243,244),(108,91,NULL,NULL,'getMethod',245,246),(109,91,NULL,NULL,'ajaxVariablesInit',247,248),(110,91,NULL,NULL,'installBase',249,250),(111,91,NULL,NULL,'syncACL',251,252),(112,91,NULL,NULL,'install_acl',253,254),(113,1,NULL,NULL,'Pages',264,297),(114,113,NULL,NULL,'display',265,266),(115,113,NULL,NULL,'recordsforpage',267,268),(116,113,NULL,NULL,'filterConfig',269,270),(117,113,NULL,NULL,'isRoot',271,272),(118,113,NULL,NULL,'setSidebarMenu',273,274),(119,113,NULL,NULL,'setHeaderMenu',275,276),(120,113,NULL,NULL,'is_Authorizate',277,278),(121,113,NULL,NULL,'getMethod',279,280),(122,113,NULL,NULL,'ajaxVariablesInit',281,282),(123,113,NULL,NULL,'installBase',283,284),(124,113,NULL,NULL,'syncACL',285,286),(125,113,NULL,NULL,'install_acl',287,288),(126,1,NULL,NULL,'Users',298,369),(127,126,NULL,NULL,'getConditionsRootGroup',299,300),(128,126,NULL,NULL,'getConditionsRootUser',301,302),(129,126,NULL,NULL,'admin_initDB',303,304),(130,126,NULL,NULL,'paramFilters',305,306),(131,126,NULL,NULL,'get_index',307,308),(132,126,NULL,NULL,'admin_index',309,310),(133,126,NULL,NULL,'post_add',311,312),(134,126,NULL,NULL,'admin_add',313,314),(135,126,NULL,NULL,'get_edit',315,316),(136,126,NULL,NULL,'post_edit',317,318),(137,126,NULL,NULL,'admin_edit',319,320),(138,126,NULL,NULL,'admin_delete',321,322),(139,126,NULL,NULL,'admin_deletemulti',323,324),(140,126,NULL,NULL,'post_resetpassword',325,326),(141,126,NULL,NULL,'admin_resetpassword',327,328),(142,126,NULL,NULL,'admin_home',329,330),(143,126,NULL,NULL,'get_login',331,332),(144,126,NULL,NULL,'post_login',333,334),(145,126,NULL,NULL,'admin_login',335,336),(146,126,NULL,NULL,'admin_logout',337,338),(147,126,NULL,NULL,'recordsforpage',339,340),(148,126,NULL,NULL,'filterConfig',341,342),(149,126,NULL,NULL,'isRoot',343,344),(150,126,NULL,NULL,'setSidebarMenu',345,346),(151,126,NULL,NULL,'setHeaderMenu',347,348),(152,126,NULL,NULL,'is_Authorizate',349,350),(153,126,NULL,NULL,'getMethod',351,352),(154,126,NULL,NULL,'ajaxVariablesInit',353,354),(155,126,NULL,NULL,'installBase',355,356),(156,126,NULL,NULL,'syncACL',357,358),(157,126,NULL,NULL,'install_acl',359,360),(158,1,NULL,NULL,'AclExtras',370,371),(159,1,NULL,NULL,'Companies',372,425),(160,159,NULL,NULL,'paramFilters',373,374),(161,159,NULL,NULL,'get_index',375,376),(162,159,NULL,NULL,'admin_index',377,378),(163,159,NULL,NULL,'post_add',379,380),(164,159,NULL,NULL,'get_add',381,382),(165,159,NULL,NULL,'admin_add',383,384),(166,159,NULL,NULL,'get_edit',385,386),(167,159,NULL,NULL,'post_edit',387,388),(168,159,NULL,NULL,'admin_edit',389,390),(169,159,NULL,NULL,'admin_delete',391,392),(170,159,NULL,NULL,'admin_deletemulti',393,394),(171,159,NULL,NULL,'recordsforpage',395,396),(172,159,NULL,NULL,'filterConfig',397,398),(173,159,NULL,NULL,'isRoot',399,400),(174,159,NULL,NULL,'setSidebarMenu',401,402),(175,159,NULL,NULL,'setHeaderMenu',403,404),(176,159,NULL,NULL,'is_Authorizate',405,406),(177,159,NULL,NULL,'getMethod',407,408),(178,159,NULL,NULL,'ajaxVariablesInit',409,410),(179,159,NULL,NULL,'installBase',411,412),(180,159,NULL,NULL,'syncACL',413,414),(181,159,NULL,NULL,'install_acl',415,416),(190,1,NULL,NULL,'Corporations',426,479),(191,190,NULL,NULL,'paramFilters',427,428),(192,190,NULL,NULL,'get_index',429,430),(193,190,NULL,NULL,'admin_index',431,432),(194,190,NULL,NULL,'post_add',433,434),(195,190,NULL,NULL,'get_add',435,436),(196,190,NULL,NULL,'admin_add',437,438),(197,190,NULL,NULL,'get_edit',439,440),(198,190,NULL,NULL,'post_edit',441,442),(199,190,NULL,NULL,'admin_edit',443,444),(200,190,NULL,NULL,'admin_delete',445,446),(201,190,NULL,NULL,'admin_deletemulti',447,448),(202,190,NULL,NULL,'recordsforpage',449,450),(203,190,NULL,NULL,'filterConfig',451,452),(204,190,NULL,NULL,'isRoot',453,454),(205,190,NULL,NULL,'setSidebarMenu',455,456),(206,190,NULL,NULL,'setHeaderMenu',457,458),(207,190,NULL,NULL,'is_Authorizate',459,460),(208,190,NULL,NULL,'getMethod',461,462),(209,190,NULL,NULL,'ajaxVariablesInit',463,464),(210,190,NULL,NULL,'installBase',465,466),(211,190,NULL,NULL,'syncACL',467,468),(212,190,NULL,NULL,'install_acl',469,470),(213,1,NULL,NULL,'Welcome',480,513),(214,213,NULL,NULL,'admin_index',481,482),(215,213,NULL,NULL,'recordsforpage',483,484),(216,213,NULL,NULL,'filterConfig',485,486),(217,213,NULL,NULL,'isRoot',487,488),(218,213,NULL,NULL,'setSidebarMenu',489,490),(219,213,NULL,NULL,'setHeaderMenu',491,492),(220,213,NULL,NULL,'is_Authorizate',493,494),(221,213,NULL,NULL,'getMethod',495,496),(222,213,NULL,NULL,'ajaxVariablesInit',497,498),(223,213,NULL,NULL,'installBase',499,500),(224,213,NULL,NULL,'syncACL',501,502),(225,213,NULL,NULL,'install_acl',503,504),(226,2,NULL,NULL,'getlocalesValidates',47,48),(227,2,NULL,NULL,'readWithLocale',49,50),(228,2,NULL,NULL,'getlocales',51,52),(229,2,NULL,NULL,'validationLocale',53,54),(230,25,NULL,NULL,'getlocalesValidates',101,102),(231,25,NULL,NULL,'readWithLocale',103,104),(232,25,NULL,NULL,'getlocales',105,106),(233,25,NULL,NULL,'validationLocale',107,108),(234,159,NULL,NULL,'getlocalesValidates',417,418),(235,159,NULL,NULL,'readWithLocale',419,420),(236,159,NULL,NULL,'getlocales',421,422),(237,159,NULL,NULL,'validationLocale',423,424),(238,190,NULL,NULL,'getlocalesValidates',471,472),(239,190,NULL,NULL,'readWithLocale',473,474),(240,190,NULL,NULL,'getlocales',475,476),(241,190,NULL,NULL,'validationLocale',477,478),(242,48,NULL,NULL,'getlocalesValidates',149,150),(243,48,NULL,NULL,'readWithLocale',151,152),(244,48,NULL,NULL,'getlocales',153,154),(245,48,NULL,NULL,'validationLocale',155,156),(246,68,NULL,NULL,'getlocalesValidates',203,204),(247,68,NULL,NULL,'readWithLocale',205,206),(248,68,NULL,NULL,'getlocales',207,208),(249,68,NULL,NULL,'validationLocale',209,210),(250,91,NULL,NULL,'getlocalesValidates',255,256),(251,91,NULL,NULL,'readWithLocale',257,258),(252,91,NULL,NULL,'getlocales',259,260),(253,91,NULL,NULL,'validationLocale',261,262),(254,113,NULL,NULL,'getlocalesValidates',289,290),(255,113,NULL,NULL,'readWithLocale',291,292),(256,113,NULL,NULL,'getlocales',293,294),(257,113,NULL,NULL,'validationLocale',295,296),(258,126,NULL,NULL,'getlocalesValidates',361,362),(259,126,NULL,NULL,'readWithLocale',363,364),(260,126,NULL,NULL,'getlocales',365,366),(261,126,NULL,NULL,'validationLocale',367,368),(262,213,NULL,NULL,'getlocalesValidates',505,506),(263,213,NULL,NULL,'readWithLocale',507,508),(264,213,NULL,NULL,'getlocales',509,510),(265,213,NULL,NULL,'validationLocale',511,512);
/*!40000 ALTER TABLE `acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `actions`
--

DROP TABLE IF EXISTS `actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `order` int(2) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_actions_categories1_idx` (`category_id`),
  CONSTRAINT `fk_actions_categories1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actions`
--

LOCK TABLES `actions` WRITE;
/*!40000 ALTER TABLE `actions` DISABLE KEYS */;
INSERT INTO `actions` VALUES (29,'Bienvenido','/admin/welcome/index',0,13,'2015-10-14 15:51:54','2015-10-14 15:51:54'),(30,'CategorÃ­as : Listado','/admin/categories/index',0,16,'2015-10-14 15:52:17','2015-10-14 15:52:17'),(31,'Corporaciones : Listado','/admin/corporations/index',0,10,'2015-10-14 15:52:37','2015-10-14 15:52:37'),(32,'Empresas : Listado','/admin/companies/index',0,11,'2015-10-14 15:52:59','2015-10-14 15:52:59'),(33,'Funciones : Listado','/admin/actions/index',0,17,'2015-10-14 15:53:55','2015-10-14 15:53:55'),(34,'Grupos : Listado','/admin/groups/index',0,14,'2015-10-14 15:54:19','2015-10-14 15:54:19'),(35,'Modulos : Listado','/admin/modules/index',0,15,'2015-10-14 15:54:40','2015-10-14 15:54:40'),(36,'Permisos : ACL','/admin/groupactions/acl',1,18,'2015-10-14 15:55:11','2015-10-14 15:55:11'),(37,'Permisos : Listado','/admin/groupactions/index',0,18,'2015-10-14 15:55:33','2015-10-14 15:55:33'),(38,'Usuarios : Listado','/admin/users/index',0,12,'2015-10-14 15:55:58','2015-10-14 15:55:58'),(39,'Usuarios : Sync','/admin/users/initDB',1,12,'2015-10-14 15:56:20','2015-10-14 15:56:20');
/*!40000 ALTER TABLE `actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros`
--

DROP TABLE IF EXISTS `aros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_aros_lft_rght` (`lft`,`rght`),
  KEY `idx_aros_alias` (`alias`),
  KEY `idx_aros_model_foreign_key` (`model`,`foreign_key`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros`
--

LOCK TABLES `aros` WRITE;
/*!40000 ALTER TABLE `aros` DISABLE KEYS */;
INSERT INTO `aros` VALUES (1,NULL,'Group',1,NULL,1,6),(2,1,'User',1,NULL,2,3),(3,NULL,'Module',1,NULL,7,8),(4,NULL,'Category',1,NULL,9,10),(5,NULL,'Category',2,NULL,11,12),(6,NULL,'Category',3,NULL,13,14),(7,NULL,'Category',4,NULL,15,16),(8,NULL,'Category',5,NULL,17,18),(9,NULL,'Category',6,NULL,19,20),(10,NULL,'Module',2,NULL,21,22),(11,NULL,'Category',7,NULL,23,24),(12,NULL,'Company',1,NULL,25,26),(16,1,'User',3,NULL,4,5),(17,NULL,'Group',2,NULL,27,32),(20,NULL,'Module',3,NULL,33,34),(21,17,'User',6,NULL,28,29),(22,NULL,'Group',3,NULL,35,42),(23,NULL,'Company',4,NULL,43,44),(24,22,'User',7,NULL,36,37),(25,22,'User',8,NULL,38,39),(26,NULL,'Module',4,NULL,45,46),(27,NULL,'Category',8,NULL,47,48),(28,NULL,'Corporation',0,NULL,49,50),(30,NULL,'Corporation',3,NULL,51,52),(31,22,'User',9,NULL,40,41),(32,NULL,'Group',4,NULL,53,60),(33,32,'User',10,NULL,54,55),(34,NULL,'Corporation',4,NULL,61,62),(36,NULL,'Company',6,NULL,63,64),(37,NULL,'Company',7,NULL,65,66),(39,32,'User',11,NULL,56,57),(40,32,'User',12,NULL,58,59),(41,NULL,'Module',5,NULL,67,68),(42,NULL,'Category',9,NULL,69,70),(43,17,'User',13,NULL,30,31),(44,NULL,'Module',6,NULL,71,72),(45,NULL,'Module',7,NULL,73,74),(46,NULL,'Module',8,NULL,75,76),(48,NULL,'Group',5,NULL,77,80),(49,NULL,'Group',6,NULL,81,86),(50,NULL,'Group',7,NULL,87,92),(51,NULL,'Group',8,NULL,93,94),(52,NULL,'Module',10,NULL,95,96),(53,NULL,'Module',11,NULL,97,98),(54,NULL,'Module',12,NULL,99,100),(55,NULL,'Module',13,NULL,101,102),(56,NULL,'Module',14,NULL,103,104),(57,NULL,'Category',10,NULL,105,106),(58,NULL,'Category',11,NULL,107,108),(59,NULL,'Category',12,NULL,109,110),(60,NULL,'Category',13,NULL,111,112),(61,NULL,'Category',14,NULL,113,114),(62,NULL,'Category',15,NULL,115,116),(63,NULL,'Category',16,NULL,117,118),(64,NULL,'Category',17,NULL,119,120),(65,NULL,'Category',18,NULL,121,122),(66,48,'User',14,NULL,78,79),(67,NULL,'Corporation',5,NULL,123,124),(68,NULL,'Company',9,NULL,125,126),(69,NULL,'Company',10,NULL,127,128),(70,49,'User',15,NULL,82,83),(71,50,'User',16,NULL,88,89),(73,NULL,'Corporation',7,NULL,129,130),(74,NULL,'Company',11,NULL,131,132),(76,49,'User',17,NULL,84,85),(77,50,'User',18,NULL,90,91),(79,NULL,'Group',9,NULL,133,138),(80,79,'User',19,NULL,134,135),(81,79,'User',20,NULL,136,137);
/*!40000 ALTER TABLE `aros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`),
  KEY `aco_id` (`aco_id`),
  CONSTRAINT `aros_acos_ibfk_1` FOREIGN KEY (`aro_id`) REFERENCES `aros` (`id`),
  CONSTRAINT `aros_acos_ibfk_2` FOREIGN KEY (`aco_id`) REFERENCES `acos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=543 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aros_acos`
--

LOCK TABLES `aros_acos` WRITE;
/*!40000 ALTER TABLE `aros_acos` DISABLE KEYS */;
INSERT INTO `aros_acos` VALUES (1,2,1,'1','1','1','1'),(2,1,1,'1','1','1','1'),(3,17,126,'1','1','1','1'),(4,17,159,'-1','-1','-1','-1'),(5,22,142,'1','1','1','1'),(6,22,143,'1','1','1','1'),(7,22,144,'1','1','1','1'),(8,22,145,'1','1','1','1'),(9,22,146,'1','1','1','1'),(10,22,147,'1','1','1','1'),(11,22,148,'1','1','1','1'),(12,22,149,'1','1','1','1'),(13,22,150,'1','1','1','1'),(14,22,151,'1','1','1','1'),(15,22,152,'1','1','1','1'),(16,22,153,'1','1','1','1'),(17,22,154,'1','1','1','1'),(18,17,190,'-1','-1','-1','-1'),(19,17,191,'-1','-1','-1','-1'),(20,17,192,'-1','-1','-1','-1'),(21,17,193,'-1','-1','-1','-1'),(22,17,194,'-1','-1','-1','-1'),(23,17,195,'-1','-1','-1','-1'),(24,17,196,'-1','-1','-1','-1'),(25,17,197,'-1','-1','-1','-1'),(26,17,198,'-1','-1','-1','-1'),(27,17,199,'-1','-1','-1','-1'),(28,17,200,'-1','-1','-1','-1'),(29,17,201,'-1','-1','-1','-1'),(30,17,202,'-1','-1','-1','-1'),(31,17,203,'-1','-1','-1','-1'),(32,17,204,'-1','-1','-1','-1'),(33,17,205,'-1','-1','-1','-1'),(34,17,206,'-1','-1','-1','-1'),(35,17,207,'-1','-1','-1','-1'),(36,17,208,'-1','-1','-1','-1'),(37,17,209,'-1','-1','-1','-1'),(38,17,210,'-1','-1','-1','-1'),(39,17,211,'-1','-1','-1','-1'),(40,17,212,'-1','-1','-1','-1'),(41,32,126,'1','1','1','1'),(42,32,127,'1','1','1','1'),(43,32,128,'1','1','1','1'),(44,32,129,'1','1','1','1'),(45,32,130,'1','1','1','1'),(46,32,131,'1','1','1','1'),(47,32,132,'1','1','1','1'),(48,32,133,'1','1','1','1'),(49,32,134,'1','1','1','1'),(50,32,135,'1','1','1','1'),(51,32,136,'1','1','1','1'),(52,32,137,'1','1','1','1'),(53,32,138,'1','1','1','1'),(54,32,139,'1','1','1','1'),(55,32,140,'1','1','1','1'),(56,32,141,'1','1','1','1'),(57,32,142,'1','1','1','1'),(58,32,143,'1','1','1','1'),(59,32,144,'1','1','1','1'),(60,32,145,'1','1','1','1'),(61,32,146,'1','1','1','1'),(62,32,147,'1','1','1','1'),(63,32,148,'1','1','1','1'),(64,32,149,'1','1','1','1'),(65,32,150,'1','1','1','1'),(66,32,151,'1','1','1','1'),(67,32,152,'1','1','1','1'),(68,32,153,'1','1','1','1'),(69,32,154,'1','1','1','1'),(70,32,155,'1','1','1','1'),(71,32,156,'1','1','1','1'),(72,32,157,'1','1','1','1'),(73,32,158,'1','1','1','1'),(75,32,159,'1','1','1','1'),(76,32,160,'1','1','1','1'),(77,32,161,'1','1','1','1'),(78,32,162,'1','1','1','1'),(79,32,163,'1','1','1','1'),(80,32,164,'1','1','1','1'),(81,32,165,'1','1','1','1'),(82,32,166,'1','1','1','1'),(83,32,167,'1','1','1','1'),(84,32,168,'1','1','1','1'),(85,32,169,'1','1','1','1'),(86,32,170,'1','1','1','1'),(87,32,171,'1','1','1','1'),(88,32,172,'1','1','1','1'),(89,32,173,'1','1','1','1'),(90,32,174,'1','1','1','1'),(91,32,175,'1','1','1','1'),(92,32,176,'1','1','1','1'),(93,32,177,'1','1','1','1'),(94,32,178,'1','1','1','1'),(95,32,179,'1','1','1','1'),(96,32,180,'1','1','1','1'),(97,32,181,'1','1','1','1'),(98,32,190,'1','1','1','1'),(99,32,191,'1','1','1','1'),(100,32,192,'1','1','1','1'),(101,32,193,'1','1','1','1'),(102,32,194,'1','1','1','1'),(103,32,195,'1','1','1','1'),(104,32,196,'1','1','1','1'),(105,32,197,'1','1','1','1'),(106,32,198,'1','1','1','1'),(107,32,199,'1','1','1','1'),(108,32,200,'1','1','1','1'),(109,32,202,'1','1','1','1'),(110,32,201,'1','1','1','1'),(111,32,203,'1','1','1','1'),(112,32,204,'1','1','1','1'),(113,32,205,'1','1','1','1'),(114,32,206,'1','1','1','1'),(115,32,207,'1','1','1','1'),(116,32,208,'1','1','1','1'),(117,32,209,'1','1','1','1'),(118,32,210,'1','1','1','1'),(119,32,211,'1','1','1','1'),(120,32,212,'1','1','1','1'),(123,17,213,'1','1','1','1'),(124,22,213,'1','1','1','1'),(125,32,213,'1','1','1','1'),(126,32,214,'1','1','1','1'),(127,22,214,'1','1','1','1'),(128,17,214,'1','1','1','1'),(129,17,215,'1','1','1','1'),(130,22,215,'1','1','1','1'),(131,32,215,'1','1','1','1'),(132,32,216,'1','1','1','1'),(133,22,216,'1','1','1','1'),(134,17,216,'1','1','1','1'),(135,17,217,'1','1','1','1'),(136,22,217,'1','1','1','1'),(137,32,217,'1','1','1','1'),(138,32,218,'1','1','1','1'),(139,22,218,'1','1','1','1'),(140,17,218,'1','1','1','1'),(141,17,219,'1','1','1','1'),(142,22,219,'1','1','1','1'),(143,32,219,'1','1','1','1'),(144,32,220,'1','1','1','1'),(145,22,220,'1','1','1','1'),(146,17,220,'1','1','1','1'),(147,17,221,'1','1','1','1'),(148,22,221,'1','1','1','1'),(149,32,221,'1','1','1','1'),(150,32,222,'1','1','1','1'),(151,22,222,'1','1','1','1'),(152,17,222,'1','1','1','1'),(153,17,223,'1','1','1','1'),(154,22,223,'1','1','1','1'),(155,32,223,'1','1','1','1'),(156,32,224,'1','1','1','1'),(157,22,224,'1','1','1','1'),(158,17,224,'1','1','1','1'),(159,17,225,'1','1','1','1'),(160,22,225,'1','1','1','1'),(161,32,225,'1','1','1','1'),(162,17,160,'-1','-1','-1','-1'),(163,17,161,'-1','-1','-1','-1'),(164,17,162,'-1','-1','-1','-1'),(165,17,163,'-1','-1','-1','-1'),(166,17,164,'-1','-1','-1','-1'),(167,17,165,'-1','-1','-1','-1'),(168,17,166,'-1','-1','-1','-1'),(169,17,167,'-1','-1','-1','-1'),(170,17,168,'-1','-1','-1','-1'),(171,17,169,'-1','-1','-1','-1'),(172,17,170,'-1','-1','-1','-1'),(173,17,171,'-1','-1','-1','-1'),(174,17,172,'-1','-1','-1','-1'),(175,17,173,'-1','-1','-1','-1'),(176,17,175,'-1','-1','-1','-1'),(177,17,176,'-1','-1','-1','-1'),(178,17,174,'-1','-1','-1','-1'),(179,17,177,'-1','-1','-1','-1'),(180,17,178,'-1','-1','-1','-1'),(181,17,179,'-1','-1','-1','-1'),(182,17,180,'-1','-1','-1','-1'),(183,17,181,'-1','-1','-1','-1'),(184,22,140,'1','1','1','1'),(185,22,141,'1','1','1','1'),(186,48,1,'1','1','1','1'),(187,48,2,'1','1','1','1'),(188,48,3,'1','1','1','1'),(189,48,4,'1','1','1','1'),(190,48,5,'1','1','1','1'),(191,48,6,'1','1','1','1'),(192,48,7,'1','1','1','1'),(193,48,8,'1','1','1','1'),(194,48,9,'1','1','1','1'),(195,48,10,'1','1','1','1'),(196,48,11,'1','1','1','1'),(197,48,12,'1','1','1','1'),(198,48,13,'1','1','1','1'),(199,48,14,'1','1','1','1'),(200,48,15,'1','1','1','1'),(201,48,16,'1','1','1','1'),(202,48,17,'1','1','1','1'),(203,48,18,'1','1','1','1'),(204,48,19,'1','1','1','1'),(205,48,20,'1','1','1','1'),(206,48,21,'1','1','1','1'),(207,48,22,'1','1','1','1'),(208,48,23,'1','1','1','1'),(209,48,24,'1','1','1','1'),(210,48,25,'1','1','1','1'),(211,48,26,'1','1','1','1'),(212,48,27,'1','1','1','1'),(213,48,28,'1','1','1','1'),(214,48,29,'1','1','1','1'),(215,48,30,'1','1','1','1'),(216,48,31,'1','1','1','1'),(217,48,32,'1','1','1','1'),(218,48,33,'1','1','1','1'),(219,48,34,'1','1','1','1'),(220,48,35,'1','1','1','1'),(221,48,36,'1','1','1','1'),(222,48,37,'1','1','1','1'),(223,48,38,'1','1','1','1'),(224,48,39,'1','1','1','1'),(225,48,40,'1','1','1','1'),(226,48,41,'1','1','1','1'),(227,48,42,'1','1','1','1'),(228,48,43,'1','1','1','1'),(229,48,44,'1','1','1','1'),(230,48,45,'1','1','1','1'),(231,48,46,'1','1','1','1'),(232,48,47,'1','1','1','1'),(233,48,48,'1','1','1','1'),(234,48,49,'1','1','1','1'),(235,48,50,'1','1','1','1'),(236,48,51,'1','1','1','1'),(237,48,52,'1','1','1','1'),(238,48,53,'1','1','1','1'),(239,48,54,'1','1','1','1'),(240,48,55,'1','1','1','1'),(241,48,56,'1','1','1','1'),(242,48,57,'1','1','1','1'),(243,48,58,'1','1','1','1'),(244,48,59,'1','1','1','1'),(245,48,60,'1','1','1','1'),(246,48,61,'1','1','1','1'),(247,48,62,'1','1','1','1'),(248,48,63,'1','1','1','1'),(249,48,64,'1','1','1','1'),(250,48,65,'1','1','1','1'),(251,48,66,'1','1','1','1'),(252,48,67,'1','1','1','1'),(253,48,68,'1','1','1','1'),(254,48,69,'1','1','1','1'),(255,48,70,'1','1','1','1'),(256,48,71,'1','1','1','1'),(257,48,72,'1','1','1','1'),(258,48,73,'1','1','1','1'),(259,48,74,'1','1','1','1'),(260,48,75,'1','1','1','1'),(261,48,76,'1','1','1','1'),(262,48,77,'1','1','1','1'),(263,48,78,'1','1','1','1'),(264,48,79,'1','1','1','1'),(265,48,80,'1','1','1','1'),(266,48,81,'1','1','1','1'),(267,48,82,'1','1','1','1'),(268,48,83,'1','1','1','1'),(269,48,84,'1','1','1','1'),(270,48,85,'1','1','1','1'),(271,48,86,'1','1','1','1'),(272,48,87,'1','1','1','1'),(273,48,88,'1','1','1','1'),(274,48,89,'1','1','1','1'),(275,48,90,'1','1','1','1'),(276,48,91,'1','1','1','1'),(277,48,92,'1','1','1','1'),(280,48,93,'1','1','1','1'),(281,48,94,'1','1','1','1'),(282,48,95,'1','1','1','1'),(283,48,96,'1','1','1','1'),(284,48,97,'1','1','1','1'),(285,48,98,'1','1','1','1'),(286,48,99,'1','1','1','1'),(287,48,100,'1','1','1','1'),(288,48,101,'1','1','1','1'),(289,48,102,'1','1','1','1'),(290,48,103,'1','1','1','1'),(291,48,104,'1','1','1','1'),(292,48,105,'1','1','1','1'),(293,48,106,'1','1','1','1'),(294,48,107,'1','1','1','1'),(295,48,108,'1','1','1','1'),(296,48,109,'1','1','1','1'),(297,48,110,'1','1','1','1'),(298,48,111,'1','1','1','1'),(299,48,112,'1','1','1','1'),(300,48,113,'1','1','1','1'),(301,48,114,'1','1','1','1'),(302,48,115,'1','1','1','1'),(303,48,116,'1','1','1','1'),(304,48,117,'1','1','1','1'),(305,48,118,'1','1','1','1'),(306,48,119,'1','1','1','1'),(307,48,120,'1','1','1','1'),(308,48,121,'1','1','1','1'),(309,48,122,'1','1','1','1'),(310,48,123,'1','1','1','1'),(311,48,124,'1','1','1','1'),(312,48,125,'1','1','1','1'),(313,48,126,'1','1','1','1'),(314,48,127,'1','1','1','1'),(315,48,128,'1','1','1','1'),(316,48,129,'1','1','1','1'),(317,48,130,'1','1','1','1'),(318,48,131,'1','1','1','1'),(319,48,132,'1','1','1','1'),(320,48,133,'1','1','1','1'),(321,48,134,'1','1','1','1'),(322,48,135,'1','1','1','1'),(323,48,136,'1','1','1','1'),(324,48,137,'1','1','1','1'),(325,48,138,'1','1','1','1'),(326,48,139,'1','1','1','1'),(327,48,140,'1','1','1','1'),(328,48,141,'1','1','1','1'),(329,48,142,'1','1','1','1'),(330,48,143,'1','1','1','1'),(331,48,144,'1','1','1','1'),(332,48,145,'1','1','1','1'),(333,48,146,'1','1','1','1'),(334,48,147,'1','1','1','1'),(335,48,148,'1','1','1','1'),(336,48,149,'1','1','1','1'),(337,48,150,'1','1','1','1'),(338,48,151,'1','1','1','1'),(339,48,152,'1','1','1','1'),(340,48,153,'1','1','1','1'),(341,48,154,'1','1','1','1'),(342,48,155,'1','1','1','1'),(343,48,156,'1','1','1','1'),(344,48,157,'1','1','1','1'),(345,48,158,'1','1','1','1'),(346,48,159,'1','1','1','1'),(347,48,160,'1','1','1','1'),(348,48,161,'1','1','1','1'),(349,48,162,'1','1','1','1'),(350,48,163,'1','1','1','1'),(351,48,164,'1','1','1','1'),(352,48,165,'1','1','1','1'),(353,48,166,'1','1','1','1'),(354,48,167,'1','1','1','1'),(355,48,168,'1','1','1','1'),(356,48,169,'1','1','1','1'),(357,48,170,'1','1','1','1'),(358,48,171,'1','1','1','1'),(359,48,172,'1','1','1','1'),(360,48,173,'1','1','1','1'),(361,48,174,'1','1','1','1'),(362,48,175,'1','1','1','1'),(363,48,176,'1','1','1','1'),(364,48,177,'1','1','1','1'),(365,48,178,'1','1','1','1'),(366,48,179,'1','1','1','1'),(367,48,180,'1','1','1','1'),(368,48,181,'1','1','1','1'),(369,48,190,'1','1','1','1'),(370,48,191,'1','1','1','1'),(371,48,192,'1','1','1','1'),(372,48,193,'1','1','1','1'),(373,48,194,'1','1','1','1'),(374,48,195,'1','1','1','1'),(375,48,196,'1','1','1','1'),(376,48,197,'1','1','1','1'),(377,48,198,'1','1','1','1'),(378,48,199,'1','1','1','1'),(379,48,200,'1','1','1','1'),(380,48,201,'1','1','1','1'),(381,48,202,'1','1','1','1'),(382,48,203,'1','1','1','1'),(383,48,204,'1','1','1','1'),(384,48,205,'1','1','1','1'),(385,48,206,'1','1','1','1'),(386,48,207,'1','1','1','1'),(387,48,208,'1','1','1','1'),(388,48,209,'1','1','1','1'),(389,48,210,'1','1','1','1'),(390,48,211,'1','1','1','1'),(391,48,212,'1','1','1','1'),(392,48,213,'1','1','1','1'),(393,48,214,'1','1','1','1'),(394,48,215,'1','1','1','1'),(395,48,216,'1','1','1','1'),(396,48,217,'1','1','1','1'),(397,48,218,'1','1','1','1'),(398,48,219,'1','1','1','1'),(399,48,220,'1','1','1','1'),(400,48,221,'1','1','1','1'),(401,48,222,'1','1','1','1'),(402,48,223,'1','1','1','1'),(403,48,224,'1','1','1','1'),(404,48,225,'1','1','1','1'),(405,49,213,'1','1','1','1'),(406,50,213,'1','1','1','1'),(407,51,213,'1','1','1','1'),(408,51,214,'1','1','1','1'),(409,50,214,'1','1','1','1'),(410,49,214,'1','1','1','1'),(411,49,215,'1','1','1','1'),(412,50,215,'1','1','1','1'),(413,51,215,'1','1','1','1'),(414,51,216,'1','1','1','1'),(415,50,216,'1','1','1','1'),(416,49,216,'1','1','1','1'),(417,49,217,'1','1','1','1'),(418,50,217,'1','1','1','1'),(419,51,217,'1','1','1','1'),(420,51,218,'1','1','1','1'),(421,50,218,'1','1','1','1'),(422,49,218,'1','1','1','1'),(423,49,219,'1','1','1','1'),(424,50,219,'1','1','1','1'),(425,51,219,'1','1','1','1'),(426,49,225,'1','1','1','1'),(427,50,225,'1','1','1','1'),(428,51,225,'1','1','1','1'),(429,51,224,'1','1','1','1'),(430,50,224,'1','1','1','1'),(431,49,224,'1','1','1','1'),(432,49,223,'1','1','1','1'),(433,50,223,'1','1','1','1'),(434,51,222,'1','1','1','1'),(435,50,222,'1','1','1','1'),(436,49,222,'1','1','1','1'),(437,49,221,'1','1','1','1'),(438,49,220,'1','1','1','1'),(439,51,220,'1','1','1','1'),(440,50,220,'1','1','1','1'),(441,51,221,'1','1','1','1'),(442,50,221,'1','1','1','1'),(443,51,223,'1','1','1','1'),(444,49,141,'1','1','1','1'),(445,50,141,'1','1','1','1'),(446,51,141,'1','1','1','1'),(447,51,140,'1','1','1','1'),(448,50,140,'1','1','1','1'),(449,49,140,'1','1','1','1'),(450,49,142,'1','1','1','1'),(451,50,142,'1','1','1','1'),(452,51,142,'1','1','1','1'),(453,49,143,'1','1','1','1'),(454,50,143,'1','1','1','1'),(455,51,143,'1','1','1','1'),(456,50,144,'1','1','1','1'),(457,51,144,'1','1','1','1'),(458,49,144,'1','1','1','1'),(459,49,145,'1','1','1','1'),(460,50,145,'1','1','1','1'),(461,51,146,'1','1','1','1'),(462,51,145,'1','1','1','1'),(463,50,146,'1','1','1','1'),(464,49,146,'1','1','1','1'),(465,79,126,'1','1','1','1'),(466,79,127,'1','1','1','1'),(467,79,128,'1','1','1','1'),(468,79,129,'1','1','1','1'),(469,79,130,'1','1','1','1'),(470,79,131,'1','1','1','1'),(471,79,132,'1','1','1','1'),(472,79,133,'1','1','1','1'),(473,79,134,'1','1','1','1'),(474,79,135,'1','1','1','1'),(475,79,136,'1','1','1','1'),(476,79,137,'1','1','1','1'),(477,79,138,'1','1','1','1'),(478,79,139,'1','1','1','1'),(479,79,140,'1','1','1','1'),(480,79,141,'1','1','1','1'),(481,79,142,'1','1','1','1'),(482,79,143,'1','1','1','1'),(483,79,144,'1','1','1','1'),(484,79,145,'1','1','1','1'),(485,79,146,'1','1','1','1'),(486,79,147,'1','1','1','1'),(487,79,148,'1','1','1','1'),(488,79,149,'1','1','1','1'),(489,79,150,'1','1','1','1'),(490,79,151,'1','1','1','1'),(491,79,152,'1','1','1','1'),(492,79,153,'1','1','1','1'),(493,79,154,'1','1','1','1'),(494,79,155,'1','1','1','1'),(495,79,156,'1','1','1','1'),(496,79,157,'1','1','1','1'),(497,79,159,'1','1','1','1'),(498,79,181,'1','1','1','1'),(499,79,190,'1','1','1','1'),(500,79,212,'1','1','1','1'),(501,79,211,'1','1','1','1'),(502,79,210,'1','1','1','1'),(503,79,209,'1','1','1','1'),(504,79,208,'1','1','1','1'),(505,79,207,'1','1','1','1'),(506,79,206,'1','1','1','1'),(507,79,205,'1','1','1','1'),(508,79,160,'1','1','1','1'),(509,79,161,'1','1','1','1'),(510,79,162,'1','1','1','1'),(511,79,163,'1','1','1','1'),(512,79,164,'1','1','1','1'),(513,79,165,'1','1','1','1'),(514,79,166,'1','1','1','1'),(515,79,167,'1','1','1','1'),(516,79,168,'1','1','1','1'),(517,79,169,'1','1','1','1'),(518,79,170,'1','1','1','1'),(519,79,171,'1','1','1','1'),(520,79,172,'1','1','1','1'),(521,79,173,'1','1','1','1'),(522,79,174,'1','1','1','1'),(523,79,175,'1','1','1','1'),(524,79,176,'1','1','1','1'),(525,79,177,'1','1','1','1'),(526,79,178,'1','1','1','1'),(527,79,179,'1','1','1','1'),(528,79,180,'1','1','1','1'),(529,79,204,'1','1','1','1'),(530,79,203,'1','1','1','1'),(531,79,202,'1','1','1','1'),(532,79,201,'1','1','1','1'),(533,79,200,'1','1','1','1'),(534,79,199,'1','1','1','1'),(535,79,198,'1','1','1','1'),(536,79,197,'1','1','1','1'),(537,79,196,'1','1','1','1'),(538,79,195,'1','1','1','1'),(539,79,194,'1','1','1','1'),(540,79,193,'1','1','1','1'),(541,79,192,'1','1','1','1'),(542,79,191,'1','1','1','1');
/*!40000 ALTER TABLE `aros_acos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `order` int(2) NOT NULL,
  `module_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_categories_modules_idx` (`module_id`),
  CONSTRAINT `fk_categories_modules` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (10,'Administrar Corporaciones',0,12,'2015-10-14 15:48:38','2015-10-14 15:48:38'),(11,'Administrar Empresas',0,13,'2015-10-14 15:48:51','2015-10-14 15:48:51'),(12,'Administrar Usuarios',0,14,'2015-10-14 15:49:02','2015-10-14 15:49:02'),(13,'Bienvenid@',0,10,'2015-10-14 15:49:13','2015-10-14 15:49:13'),(14,'Grupos',0,11,'2015-10-14 15:49:53','2015-10-14 15:49:53'),(15,'Modulos',1,11,'2015-10-14 15:50:07','2015-10-14 15:50:07'),(16,'Categorias',2,11,'2015-10-14 15:50:31','2015-10-14 15:50:31'),(17,'Funciones',3,11,'2015-10-14 15:50:48','2015-10-14 15:50:48'),(18,'Permisos',5,11,'2015-10-14 15:51:07','2015-10-14 15:51:07');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  `corporation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (11,'Musmanni','files/logos/1938087a26f6d40f41f3a3663d1e5a629bc5b4.jpg','PanaderÃ­a y pastelerÃ­a','66665555','La direcciÃ³n','2015-10-14 19:45:53','2015-10-14 19:38:08',7);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corporations`
--

DROP TABLE IF EXISTS `corporations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corporations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `modified` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corporations`
--

LOCK TABLES `corporations` WRITE;
/*!40000 ALTER TABLE `corporations` DISABLE KEYS */;
INSERT INTO `corporations` VALUES (7,'Florida Ice & Farm Company ','files/logos/1908238643fa3c4b5e99478020c2911474a97d.gif','Es una empresa de bebidas y alimentos','22224444','San JosÃ©','2015-10-14 19:10:11','2015-10-14 19:08:23');
/*!40000 ALTER TABLE `corporations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groupactions`
--

DROP TABLE IF EXISTS `groupactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groupactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_group_action` (`group_id`,`action_id`),
  KEY `fk_groupactions_groups1_idx` (`group_id`),
  KEY `fk_groupactions_actions1_idx` (`action_id`),
  CONSTRAINT `fk_groupactions_actions1` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_groupactions_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupactions`
--

LOCK TABLES `groupactions` WRITE;
/*!40000 ALTER TABLE `groupactions` DISABLE KEYS */;
INSERT INTO `groupactions` VALUES (19,5,30,'2015-10-14 15:57:08','2015-10-14 15:57:08'),(20,5,31,'2015-10-14 15:57:13','2015-10-14 15:57:13'),(21,5,32,'2015-10-14 15:57:27','2015-10-14 15:57:27'),(22,5,33,'2015-10-14 15:57:33','2015-10-14 15:57:33'),(23,5,34,'2015-10-14 15:57:39','2015-10-14 15:57:39'),(24,5,35,'2015-10-14 15:57:50','2015-10-14 15:57:50'),(25,5,36,'2015-10-14 15:57:55','2015-10-14 15:57:55'),(26,5,37,'2015-10-14 15:58:01','2015-10-14 15:58:01'),(27,5,38,'2015-10-14 15:58:09','2015-10-14 15:58:09'),(28,5,39,'2015-10-14 15:58:15','2015-10-14 15:58:15'),(29,6,29,'2015-10-14 15:58:25','2015-10-14 15:58:25'),(32,7,29,'2015-10-14 15:59:24','2015-10-14 15:59:24'),(34,8,29,'2015-10-14 15:59:39','2015-10-14 15:59:39'),(35,9,31,'2015-10-15 21:50:35','2015-10-15 21:50:35'),(36,9,32,'2015-10-15 21:50:41','2015-10-15 21:50:41'),(37,9,38,'2015-10-15 21:50:54','2015-10-15 21:50:54');
/*!40000 ALTER TABLE `groupactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (5,'Root','2015-10-14 15:46:28','2015-10-14 15:46:28'),(6,'Administrador Corporativo','2015-10-14 15:46:38','2015-10-14 15:46:38'),(7,'Administrador','2015-10-14 15:46:48','2015-10-14 15:46:48'),(8,'Perfil Usuario Normal','2015-10-14 15:46:56','2015-10-14 15:46:56'),(9,'Administrador CSS','2015-10-15 21:50:04','2015-10-15 21:50:04');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `i18n`
--

DROP TABLE IF EXISTS `i18n`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `i18n` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`foreign_key`,`field`,`locale`,`model`),
  KEY `locale` (`locale`),
  KEY `model` (`model`),
  KEY `row_id` (`foreign_key`),
  KEY `field` (`field`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `i18n`
--

LOCK TABLES `i18n` WRITE;
/*!40000 ALTER TABLE `i18n` DISABLE KEYS */;
INSERT INTO `i18n` VALUES (9,'esp','Group',5,'name','Root'),(10,'eng','Group',5,'name','Root'),(11,'esp','Group',6,'name','Administrador Corporativo'),(12,'eng','Group',6,'name','Corporation Administrator'),(13,'esp','Group',7,'name','Administrador'),(14,'eng','Group',7,'name','Administrator'),(15,'esp','Group',8,'name','Perfil Usuario Normal'),(16,'eng','Group',8,'name','Normal User Profile'),(17,'esp','Module',10,'name','Bienvenid@'),(18,'eng','Module',10,'name','Welcome'),(19,'esp','Module',11,'name','Sistema Base'),(20,'eng','Module',11,'name','Base System'),(21,'esp','Module',12,'name','Corporaciones'),(22,'eng','Module',12,'name','Corporations'),(23,'esp','Module',13,'name','Empresas'),(24,'eng','Module',13,'name','Companies'),(25,'esp','Module',14,'name','Usuarios'),(26,'eng','Module',14,'name','Users'),(27,'esp','Category',10,'name','Administrar Corporaciones'),(28,'eng','Category',10,'name','Corporations Management'),(29,'esp','Category',11,'name','Administrar Empresas'),(30,'eng','Category',11,'name','Companies Management'),(31,'esp','Category',12,'name','Administrar Usuarios'),(32,'eng','Category',12,'name','User Management'),(33,'esp','Category',13,'name','Bienvenid@'),(34,'eng','Category',13,'name','Welcome'),(35,'esp','Category',14,'name','Grupos'),(36,'eng','Category',14,'name','Groups'),(37,'esp','Category',15,'name','Modulos'),(38,'eng','Category',15,'name','Modules'),(39,'esp','Category',16,'name','Categorias'),(40,'eng','Category',16,'name','Categories'),(41,'esp','Category',17,'name','Funciones'),(42,'eng','Category',17,'name','Functions'),(43,'esp','Category',18,'name','Permisos'),(44,'eng','Category',18,'name','Permission'),(45,'esp','Action',29,'name','Bienvenido'),(46,'eng','Action',29,'name','Welcome'),(47,'esp','Action',30,'name','CategorÃ­as : Listado'),(48,'eng','Action',30,'name','Categories : List'),(49,'esp','Action',31,'name','Corporaciones : Listado'),(50,'eng','Action',31,'name','Corporations : List'),(51,'esp','Action',32,'name','Empresas : Listado'),(52,'eng','Action',32,'name','Companies : List'),(53,'esp','Action',33,'name','Funciones : Listado'),(54,'eng','Action',33,'name','Functions : List'),(55,'esp','Action',34,'name','Grupos : Listado'),(56,'eng','Action',34,'name','Groups : List'),(57,'esp','Action',35,'name','Modulos : Listado'),(58,'eng','Action',35,'name','Modules : List'),(59,'esp','Action',36,'name','Permisos : ACL'),(60,'eng','Action',36,'name','Permissions : ACL'),(61,'esp','Action',37,'name','Permisos : Listado'),(62,'eng','Action',37,'name','Permissions : List'),(63,'esp','Action',38,'name','Usuarios : Listado'),(64,'eng','Action',38,'name','Users : List'),(65,'esp','Action',39,'name','Usuarios : Sync'),(66,'eng','Action',39,'name','Users : Sync'),(67,'esp','Corporation',7,'description','Es una empresa de bebidas y alimentos'),(68,'eng','Corporation',7,'description','A food and drinks company'),(69,'esp','Corporation',7,'address','San JosÃ©'),(70,'eng','Corporation',7,'address','Saint Joseph.'),(71,'esp','Company',11,'description','PanaderÃ­a y pastelerÃ­a'),(72,'eng','Company',11,'description','Bakery '),(73,'esp','Company',11,'address','La direcciÃ³n'),(74,'eng','Company',11,'address','The address'),(83,'esp','Group',9,'name','Administrador CSS'),(84,'eng','Group',9,'name','CSS Administrator');
/*!40000 ALTER TABLE `i18n` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `masters`
--

DROP TABLE IF EXISTS `masters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `masters`
--

LOCK TABLES `masters` WRITE;
/*!40000 ALTER TABLE `masters` DISABLE KEYS */;
/*!40000 ALTER TABLE `masters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `order` int(2) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (10,'Bienvenid@','icon-leaf',0,'2015-10-14 15:47:20','2015-10-15 21:36:23'),(11,'Sistema Base','icon-check',1,'2015-10-14 15:47:35','2015-10-15 18:43:52'),(12,'Corporaciones','icon-leaf',2,'2015-10-14 15:47:44','2015-10-15 18:45:28'),(13,'Empresas','icon-tint',4,'2015-10-14 15:47:59','2015-10-15 18:45:42'),(14,'Usuarios','icon-user',6,'2015-10-14 15:48:10','2015-10-15 17:46:09');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `corporation_id` int(11) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` char(40) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (14,'Lisandro','MartÃ­nez','lisandrom@gmail.com',NULL,NULL,'lisandrom','a56547131290384e62cd0937e9a1281c3e10d824',5,'2015-10-14 16:22:29','2015-10-15 21:47:22'),(17,'Pedro','Perez','pedrop@red111.net',NULL,7,'pedrop','253e6edb8139392550fff418bac6265d0d2cc934',6,'2015-10-14 19:49:27','2015-10-15 21:48:24'),(18,'MarÃ­a','Perez','mariap@red111.net',11,NULL,'mariap','253e6edb8139392550fff418bac6265d0d2cc934',7,'2015-10-14 19:49:50','2015-10-15 21:48:46'),(19,'Daniela','Prieto','danielap@siderysbsn.com',NULL,NULL,'danielap','253e6edb8139392550fff418bac6265d0d2cc934',9,'2015-10-15 21:58:04','2015-10-15 21:58:04'),(20,'Juan','Fernandez','juanf@siderysbsn.com',NULL,NULL,'juanf','253e6edb8139392550fff418bac6265d0d2cc934',9,'2015-10-15 21:59:22','2015-10-15 21:59:22');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `values`
--

DROP TABLE IF EXISTS `values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `values` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `optional` varchar(255) NOT NULL,
  `master_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_values_masters1_idx` (`master_id`),
  CONSTRAINT `fk_values_masters1` FOREIGN KEY (`master_id`) REFERENCES `masters` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `values`
--

LOCK TABLES `values` WRITE;
/*!40000 ALTER TABLE `values` DISABLE KEYS */;
/*!40000 ALTER TABLE `values` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-16  9:53:30
