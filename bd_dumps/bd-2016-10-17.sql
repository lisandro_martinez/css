ALTER TABLE `siriarte_css`.`indicators`
  ADD COLUMN `threshold_red_condition` varchar(255) NULL DEFAULT NULL;
ALTER TABLE `siriarte_css`.`indicators`
  ADD COLUMN `threshold_red_value` float NULL DEFAULT NULL;
ALTER TABLE `siriarte_css`.`indicators`
  ADD COLUMN `threshold_yellow_condition` varchar(255) NULL DEFAULT NULL;
ALTER TABLE `siriarte_css`.`indicators`
  ADD COLUMN `threshold_yellow_value` float NULL DEFAULT NULL;
ALTER TABLE `siriarte_css`.`indicators`
  ADD COLUMN `threshold_green_condition` varchar(255) NULL DEFAULT NULL;
ALTER TABLE `siriarte_css`.`indicators`
  ADD COLUMN `threshold_green_value` float NULL DEFAULT NULL;